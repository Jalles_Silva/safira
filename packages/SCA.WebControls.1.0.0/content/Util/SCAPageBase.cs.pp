using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using SQFramework.Core;
using SQFramework.Web;
using SCA.Servicos.Spec.Servicos;
using SCA.WebControls;
using System.Web.UI;
using SLA.Servicos.Spec.DataTransferObjects;
using SCA.Dominio.DataTransferObject;
using System.Net;
using System.Configuration;
using System.Web.Security;

namespace $rootnamespace$.Util
{
    public class SCAPageBase : SQFramework.Web.PageBase
    {
        private const string PORTAL_SISTEMAS = "~/Site/PortalSistemas.aspx";

        public new T GetService<T>(string config)
        {
            return this.ServiceLocator.GetService<T>(config, "antt");
        }

        public new T GetService<T>(string config, Dictionary<string, object> namedParameters)
        {
            return this.ServiceLocator.GetService<T>(config, namedParameters, "antt");
        }

        public ISCAService ObterSCAService()
        {
            return GetService<ISCAService>(SCA.Comum.Constantes.Servicos.SCAService, SCAApplicationContext.ObterParametrosServico());
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.LoadComplete += new EventHandler(PageBase_LoadComplete);

            if (MessageBox != null)
                MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);

            if (!IsPostBack)
            {
                this.ObterInformacoesUsuario();
                this.AlterarComportamentoBotoes();
            }
        }

        protected void AlterarComportamentoBotoes()
        {
            var LinkButtonSalvar = WebHelper.GetControlRecursively(this, "LinkButtonSalvar") as Utilitarios.Web.Controls.LinkButton;

            if (LinkButtonSalvar != null && this.Action == Actions.View)
                LinkButtonSalvar.Visible = false;

            var LinkButtonNovaConsulta = WebHelper.GetControlRecursively(this, "LinkButtonNovaConsulta") as Utilitarios.Web.Controls.LinkButton;

            if (LinkButtonNovaConsulta != null && this.Action == Actions.View)
            {
                LinkButtonNovaConsulta.Text = "Voltar";
                LinkButtonNovaConsulta.CssClass = "e-btnVoltar";
            }
        }

        private void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            if (e != null && e.Command == "AcessoNegadoSCA")
            {
                WebHelper.Redirect(PORTAL_SISTEMAS);
            }
        }

        protected void PageBase_LoadComplete(object sender, EventArgs e)
        {
            this.AplicarPermissoesFuncionalidades();
        }

        [Browsable(true)]
        [DefaultValue(true)]
        public bool VerificarPermissoes
        {
            get
            {
                bool verificarPermissoes = true;

                if (ViewState["VerificarPermissoes"] != null)
                    verificarPermissoes = Convert.ToBoolean(ViewState["VerificarPermissoes"]);

                return verificarPermissoes;
            }

            set
            {
                ViewState["VerificarPermissoes"] = value;
            }
        }

        private void ObterInformacoesUsuario()
        {
            try
            {
                if (Context.User == null || Context.User.Identity == null || !Context.User.Identity.IsAuthenticated)
                    return;

                if (SCAApplicationContext.Usuario == null)
                {
                    var identity = Context.User.Identity as FormsIdentity;

                    if (identity != null && !string.IsNullOrEmpty(identity.Ticket.UserData))
                    {
                        var retorno = ObterSCAService().ObterInformacoesUsuarioLogado(identity.Ticket.UserData);

                        if (retorno.Usuario != null)
                        {
                            SCAApplicationContext.Usuario = retorno.Usuario;

                            if (retorno.Permissoes != null)
                                SCAApplicationContext.Permissoes = retorno.Permissoes;

                            SCAApplicationContext.AdicionarUsuarioLogado();
                        }
                        else
                        {
                            WebHelper.Logoff();
                            return;
                        }
                    }
                }

                if (SCAApplicationContext.Permissoes == null)
                {
                    var retorno = ObterSCAService().ObterPermissoesUsuarioSistema();

                    if (retorno != null)
                        SCAApplicationContext.Permissoes = retorno;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void AplicarPermissoesFuncionalidades()
        {
            if (Context.User == null || Context.User.Identity == null || !Context.User.Identity.IsAuthenticated)
                return;

            bool usuarioMaster = false;

            if (SCAApplicationContext.Usuario != null)
                usuarioMaster = SCAApplicationContext.Usuario.Master;

            if (usuarioMaster || !this.VerificarPermissoes)
                return;

            if (SCAApplicationContext.Permissoes != null &&
                SCAApplicationContext.Permissoes.PermissoesFuncionalidades != null &&
                SCAApplicationContext.Permissoes.PermissoesFuncionalidades.Count > 0)
            {
                var permissoes = SCAApplicationContext.Permissoes.PermissoesFuncionalidades;

                var permissaoPagina = UrlPagina.IndexOf("?") > 0
                    ? permissoes.Where(p => p.Key.ToLower() == UrlPagina.ToLower().Substring(0, UrlPagina.IndexOf("?"))).FirstOrDefault()
                    : permissoes.Where(p => p.Key.ToLower() == UrlPagina.ToLower()).FirstOrDefault();

                if ((permissaoPagina.Key != null && !permissaoPagina.Value) || permissaoPagina.Key == null)
                {
                    if (MessageBox != null)
                    {
                        if (SCAApplicationContext.Permissoes.Cadastrador != null)
                        {
                            MessageBox.ShowErrorMessage(string.Format("Acesso negado!{0}Contate o administrador do sistema:{0}{1}{0}{2}",
                                Environment.NewLine, SCAApplicationContext.Permissoes.Cadastrador.Nome,
                                SCAApplicationContext.Permissoes.Cadastrador.Email), "AcessoNegadoSCA");
                        }
                        else
                        {
                            MessageBox.ShowErrorMessage(string.Format("Acesso negado!{0}Contate o administrador do sistema.",
                                Environment.NewLine), "AcessoNegadoSCA");
                        }
                    }
                    else
                        WebHelper.Redirect(PORTAL_SISTEMAS);
                }

                foreach (KeyValuePair<string, bool> permissao in permissoes)
                {
                    if (permissao.Value)
                        continue;

                    var controls = WebHelper.GetControlsRecursively(this, permissao.Key);

                    foreach (var control in controls)
                        control.Visible = permissao.Value;
                }
            }
            else
                WebHelper.Redirect(PORTAL_SISTEMAS);
        }

        public bool PossuiPermissaoPagina(string EnderecoPagina)
        {
            bool possuiPermissao = false;

            if (SCAApplicationContext.Usuario != null)
                possuiPermissao = SCAApplicationContext.Usuario.Master;

            if (possuiPermissao ||
                SCAApplicationContext.Permissoes == null ||
                SCAApplicationContext.Permissoes.PermissoesFuncionalidades == null ||
                SCAApplicationContext.Permissoes.PermissoesFuncionalidades.Count == 0) return possuiPermissao;

            Dictionary<string, bool> permissoes = SCAApplicationContext.Permissoes.PermissoesFuncionalidades;

            var permissaoPagina = EnderecoPagina.IndexOf("?") > 0
                ? permissoes.Where(p => p.Key.ToLower() == this.UrlPagina.ToLower().Substring(0, EnderecoPagina.IndexOf("?"))).FirstOrDefault()
                : permissoes.Where(p => p.Key.ToLower() == this.UrlPagina.ToLower()).FirstOrDefault();

            possuiPermissao = !string.IsNullOrEmpty(permissaoPagina.Key) && permissaoPagina.Value;

            return possuiPermissao;
        }

        public DTORegistrarAuditoriaEntrada ObtemObjetoAuditoriaSLA()
        {
            var auditoria = new DTORegistrarAuditoriaEntrada()
            {
                DescricaoMaquina = Environment.MachineName
            };

            var usuario = SCAApplicationContext.Usuario;

            if (usuario != null)
            {
                auditoria.CodigoUsuario = usuario.Codigo;
                auditoria.DescricaoIdSessao = usuario.IdentificadorAutenticacao.ToString();
                auditoria.DescricaoIP = usuario.Ip;
            }

            string paginaAtual = UrlPagina.IndexOf("?") > 0 ? UrlPagina.ToLower().Substring(0, UrlPagina.IndexOf("?")) : UrlPagina.ToLower();
            var funcionalidade = ObterSCAService().ObterFuncionalidadePorNomeControle(paginaAtual);

            if (funcionalidade != null)
            {
                auditoria.CodigoFuncionalidade = funcionalidade.Codigo;

                if (funcionalidade.GrupoFuncionalidade != null)
                    auditoria.CodigoSistema = funcionalidade.GrupoFuncionalidade.CodigoSistema;
            }

            string siglaSistema = ConfigurationManager.AppSettings["Sistema"].ToString();

            if (SCAApplicationContext.Permissoes != null && SCAApplicationContext.Permissoes.Sistemas != null && !string.IsNullOrWhiteSpace(siglaSistema))
            {
                var sistema = SCAApplicationContext.Permissoes.Sistemas.FirstOrDefault(s => s.Sigla == siglaSistema);

                if (sistema != null)
                {
                    auditoria.CodigoSistema = sistema.Codigo;

                    if (sistema.Superintencia != null)
                        auditoria.CodigoSuperintendencia = sistema.Superintencia.Codigo;
                }
            }

            return auditoria;
        }

        public DTORegistrarAuditoriaEntrada ObtemObjetoAuditoriaSLA(string identificadorUsuario)
        {
            SCA.Dominio.DataTransferObject.DTOUsuario usuario = ObterSCAService().ObterUsuarioPorIdentificador(identificadorUsuario);

            if (usuario != null)
            {
                var sistema = ObterSCAService().ObterSistemas().FirstOrDefault(s => s.Sigla == ConfigurationManager.AppSettings["Sistema"].ToString());

                var superitendencia = sistema.Superintendencia;

                string paginaAtual = UrlPagina.IndexOf("?") > 0 ? UrlPagina.ToLower().Substring(0, UrlPagina.IndexOf("?")) : UrlPagina.ToLower();

                DTOFuncionalidadeCompleto funcionalidade = ObterSCAService().ObterFuncionalidadePorNomeControle(paginaAtual);

                string machineName = Dns.GetHostName();

                IPAddress[] localIPs = Dns.GetHostAddresses(machineName);

                return new DTORegistrarAuditoriaEntrada()
                {
                    CodigoFuncionalidade = funcionalidade.Codigo,
                    CodigoSistema = sistema.Codigo,
                    CodigoSuperintendencia = superitendencia.Codigo,
                    CodigoUsuario = usuario.Codigo,
                    DescricaoIdSessao = "N/A",
                    DescricaoIP = localIPs.First(p => p.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToString(),
                    DescricaoMaquina = machineName
                };
            }
            else
                return null;
        }
    }
}