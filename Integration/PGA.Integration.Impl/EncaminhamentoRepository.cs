﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class EncaminhamentoRepository : RepositoryBase<Encaminhamento>, IEncaminhamentoRepository<Encaminhamento>
    {
        public PageMessage<Encaminhamento> ListarEncaminhamentosPorAcompanhamento(int codigoSeqAcompanhamento, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<Encaminhamento>();

            criteria.Add(Expression.Eq("acompanhamento.codigoSeqAcompanhamento", codigoSeqAcompanhamento));

            return Page<Encaminhamento>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }

        public PageMessage<Encaminhamento> ListarEncaminhamentosPorTipoUnidadeEStatus(int tipoAcompanhamento, string unidade, int status, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<Encaminhamento>()
                .CreateAlias("acompanhamento", "a")
                .CreateAlias("corporativoUnidade", "c");


            if (tipoAcompanhamento > 0)
                criteria.Add(Expression.Eq("a.tipoAcompanhamento.codigoSeqTipoAcompanhamento", tipoAcompanhamento));

            if (!String.IsNullOrEmpty(unidade) && unidade != "Todas")
                criteria.Add(Expression.Eq("c.descricaoUnidade", (unidade.Contains("Inativa") ? unidade.Replace(" - Inativa", "") : unidade)));

            if (status > 0)
                criteria.Add(Expression.Eq("statusEncaminhamento.codigoSeqStatusEncaminhamento", status));

            return Page<Encaminhamento>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}
