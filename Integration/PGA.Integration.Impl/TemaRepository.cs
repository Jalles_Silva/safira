﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class TemaRepository : RepositoryBase<Tema>, ITemaRepository<Tema>
    {
        public PageMessage<Tema> ListarTemasPorPerspectivasESituacao(int perspectiva, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<Tema>();

            if (perspectiva > 0)
                criteria.Add(Expression.Eq("perspectiva.codigoSeqPerspectiva", perspectiva));

            if (!String.IsNullOrEmpty(ativo))
                criteria.Add(Expression.Eq("ativo", Boolean.Parse(ativo)));

            return Page<Tema>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}