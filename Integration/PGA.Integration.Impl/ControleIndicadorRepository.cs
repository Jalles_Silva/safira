﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class ControleIndicadorRepository : RepositoryBase<ControleIndicador>, IControleIndicadorRepository<ControleIndicador>
    {
        public PageMessage<ControleIndicador> ListarControlesPorIndicador(int codigoIndicador, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<ControleIndicador>()
                .CreateAlias("indicador", "i")
                .Add(Expression.Eq("i.codigoSeqIndicador", codigoIndicador));

            return Page<ControleIndicador>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}