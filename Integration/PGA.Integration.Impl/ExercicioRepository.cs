﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;
using PGA.Common;

namespace PGA.Integration.Impl
{
    public class ExercicioRepository : RepositoryBase<Exercicio>, IExercicioRepository<Exercicio>
    {
        public PageMessage<Exercicio> ListarExercicios(int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<Exercicio>();
            return Page<Exercicio>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }

        public Exercicio ObterExercicioPorAno(Int16 ano)
        {
            var criteria = DetachedCriteria.For<Exercicio>()
             .Add(Expression.Eq("ano", ano))
             .SetMaxResults(1);
            return Get<Exercicio>(criteria);
        }

        public Exercicio ObterExercicioAtual()
        {
            int[] codigosFaseAcompanhamento = new[] {(int)FaseEnum.AcompanhamentoPrimeiroTrimestre, 
                (int)FaseEnum.AcompanhamentoSegundoTrimestre, 
                    (int)FaseEnum.AcompanhamentoTerceiroTrimestre, 
                        (int)FaseEnum.AcompanhamentoQuartoTrimestre ,
                            (int)FaseEnum.Encerrado
            };

            var criteria = DetachedCriteria.For<Exercicio>()
                .Add(Expression.In("fase.codigoSeqFase", codigosFaseAcompanhamento))
                .AddOrder(Order.Desc("ano"))
                .SetMaxResults(1);
            return Get<Exercicio>(criteria);
        }

        public Exercicio ObterExercicioEmPlanejamento()
        {
            var criteria = DetachedCriteria.For<Exercicio>()
             .Add(Expression.Eq("fase.codigoSeqFase", (int)FaseEnum.Planejamento));
            return Get<Exercicio>(criteria);
        }

        public Exercicio ObterUltimoExercicioEmPlanejamento()
        {
            var criteria = DetachedCriteria.For<Exercicio>()
             .Add(Expression.Eq("fase.codigoSeqFase", (int)FaseEnum.Planejamento))
             .AddOrder(Order.Desc("ano"))
             .SetMaxResults(1);
            return Get<Exercicio>(criteria);
        }

        public Exercicio ObterExercicioEmAcompanhamento()
        {
            int[] codigosFaseAcompanhamento = new [] {(int)FaseEnum.AcompanhamentoPrimeiroTrimestre, 
                (int)FaseEnum.AcompanhamentoSegundoTrimestre, 
                    (int)FaseEnum.AcompanhamentoTerceiroTrimestre, 
                        (int)FaseEnum.AcompanhamentoQuartoTrimestre 
            };
            var criteria = DetachedCriteria.For<Exercicio>()
             .Add(Expression.In("fase.codigoSeqFase", codigosFaseAcompanhamento));
            return Get<Exercicio>(criteria);
        }

        public IList<Exercicio> ListarExercicioEmAcompanhamento()
        {
            int[] codigosFaseAcompanhamento = new[] {(int)FaseEnum.AcompanhamentoPrimeiroTrimestre, 
                (int)FaseEnum.AcompanhamentoSegundoTrimestre, 
                    (int)FaseEnum.AcompanhamentoTerceiroTrimestre, 
                        (int)FaseEnum.AcompanhamentoQuartoTrimestre ,
                            (int)FaseEnum.Planejamento
            };
            var criteria = DetachedCriteria.For<Exercicio>()
                .Add(Expression.In("fase.codigoSeqFase", codigosFaseAcompanhamento));
            return List<Exercicio>(criteria);
        }

        public IList<Exercicio> ListarExercicioEmPlanejamento()
        {
            var criteria = DetachedCriteria.For<Exercicio>()
             .Add(Expression.Eq("fase.codigoSeqFase", (int)FaseEnum.Planejamento));
            return List<Exercicio>(criteria);
        }

        public IList<Exercicio> ListarExercicioExcetoPlanejamento()
        {
            int[] codigosFaseAcompanhamento = new[] {(int)FaseEnum.AcompanhamentoPrimeiroTrimestre, 
                (int)FaseEnum.AcompanhamentoSegundoTrimestre, 
                    (int)FaseEnum.AcompanhamentoTerceiroTrimestre, 
                        (int)FaseEnum.AcompanhamentoQuartoTrimestre,
                            (int)FaseEnum.PreEncerramento,
                                (int)FaseEnum.Encerrado
            };
            var criteria = DetachedCriteria.For<Exercicio>()
                .Add(Expression.In("fase.codigoSeqFase", codigosFaseAcompanhamento));
            return List<Exercicio>(criteria);
        }

        public IList<Exercicio> ListarAnoExercicio()
        {
            var criteria = DetachedCriteria.For<Exercicio>()
                .AddOrder(Order.Desc("ano"));
            return List<Exercicio>(criteria);
        }

        public Exercicio ObterFaseExercicioPorAno(Int16 ano)
        {
            var criteria = DetachedCriteria.For<Exercicio>()
                .Add(Expression.Eq("ano", ano));

            return Get<Exercicio>(criteria);
        }
    }
}