﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class IniciativaObjetivoRepository : RepositoryBase<IniciativaObjetivo>, IIniciativaObjetivoRepository<IniciativaObjetivo>
    {
        public PageMessage<IniciativaObjetivo> ListarObjetivosPorIniciativa(int CodigoSeqIniciativa, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<IniciativaObjetivo>();

            criteria.Add(Expression.Eq("iniciativa.codigoSeqIniciativa", CodigoSeqIniciativa));

            return Page<IniciativaObjetivo>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}
