﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class FaseRepository : RepositoryBase<Fase>, IFaseRepository<Fase>
    {
        public Fase ObterProximaFase(Int16 ordemFaseAtual)
        {
            var criteria = DetachedCriteria.For<Fase>()
             .Add(Expression.Gt("ordem", ordemFaseAtual))
             .AddOrder(Order.Asc("ordem"))
             .SetMaxResults(1);
            return Get<Fase>(criteria);
        }
    }
}