﻿using NHibernate.Criterion;
using NHibernate.Transform;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using PGA.Integration.Spec.ValueObjects;
using SQFramework.Spring.Data.Hibernate;
using System.Collections;
using System.Collections.Generic;

namespace PGA.Integration.Impl
{
    public class RelatorioConsubstanciadoRepository : RepositoryBase<RelatorioConsubstanciado>, IRelatorioConsubstanciadoRepository<RelatorioConsubstanciado>
    {
        public IList<RelatorioConsubstanciado> ListarRelatorioConsubstanciado(int codigoSeqExercicio)
        {
            var criteria = DetachedCriteria.For<RelatorioConsubstanciado>();
            criteria.Add(Expression.Eq("exercicio.codigoSeqExercicio", codigoSeqExercicio));

            return List<RelatorioConsubstanciado>(criteria);
        }

        public IList<VOObjetivos> ListarOjetivoEstrategico(int codTipoInciativa)
        {
            var sql = @" SELECT CD_SEQ_OBJETIVO as CodSeqObjetivo , DS_OBJETIVO as DescricaoObjetivo FROM (
                            SELECT DISTINCT O.CD_SEQ_OBJETIVO, O.DS_OBJETIVO, I.CD_TIPO_INICIATIVA
                             FROM TB_OBJETIVO O
		                            INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
		                            INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PE
		                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                            INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
                            WHERE  O.CD_TIPO_OBJETIVO = 4
                            UNION
                            SELECT DISTINCT O.CD_SEQ_OBJETIVO, O.DS_OBJETIVO, I.CD_TIPO_INICIATIVA
                             FROM TB_OBJETIVO O
		                            INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
		                            INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PPA
		                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                            INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
                            WHERE  O.CD_TIPO_OBJETIVO = 4) AS TB_OBJETIVOS {0}";

            string sqlWhere = "WHERE 1=1";

            if (codTipoInciativa > 0)
                sqlWhere += " AND CD_TIPO_INICIATIVA = " + codTipoInciativa;

            var result = Session.CreateSQLQuery(string.Format(sql, sqlWhere)).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var listaObjetivos = new List<VOObjetivos>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;
                    var objetivo = new VOObjetivos()
                    {
                        CodSeqObjetivo = asHash["CodSeqObjetivo"].ToString(),
                        DescricaoObjetivo = asHash["DescricaoObjetivo"].ToString()
                    };

                    listaObjetivos.Add(objetivo);
                }
            }

            return listaObjetivos;
        }

        public IList<VOObjetivos> ListarOjetivoEstrategicoEMissaoAdministrativa()
        {
            var sql = @" SELECT CONVERT(VARCHAR, CD_SEQ_OBJETIVO) + '|' + CONVERT(VARCHAR, CD_TIPO_OBJETIVO) AS CodSeqObjetivo, 
                                   DS_OBJETIVO                          AS DescricaoObjetivo 
                            FROM   (SELECT DISTINCT CD_SEQ_OBJETIVO, DS_OBJETIVO, 1 CD_TIPO_OBJETIVO FROM   TB_OBJETIVO 
                                    UNION 
                                    SELECT DISTINCT CD_SEQ_INICIATIVA CD_SEQ_OBJETIVO, DS_INICIATIVA DS_OBJETIVO, 2 CD_TIPO_OBJETIVO 
                                    FROM   TB_INICIATIVA) AS OjetivoEstrategicoEMissaoAdministrativa";

            var result = Session.CreateSQLQuery(sql).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var listaObjetivos = new List<VOObjetivos>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;
                    var objetivo = new VOObjetivos()
                    {
                        CodSeqObjetivo = asHash["CodSeqObjetivo"].ToString(),
                        DescricaoObjetivo = asHash["DescricaoObjetivo"].ToString()
                    };

                    listaObjetivos.Add(objetivo);
                }
            }

            return listaObjetivos;
        }

        public IList<VOResultadosAlcancadosPorExercicio> ListarResultadosAlcancadosPorExercicio(int AnoExercicio)
        {
            string sql = @"SELECT DS_OBJETIVO_INICIATIVA as DescricaoObjetivoIniciativa, DS_RESULTADO_ALCANCADO as ResultadoAlcancadoRegistrado,
                                CD_SEQ_EXERCICIO as CodSeqExercicio, NU_ANO_EXERCICIO as AnoExcercicio, CD_UNIDADE as CodigoUnidade,
                                DS_UNIDADE as DescricaoUnidade, CD_SEQ_RESULTADO_ALCANCADO as CodSeqResultadoAlcancado,
                                DS_RESULTADO_ALCANCADO + ' ('+ DS_UNIDADE +')' ResultadoAlcancadoRegistradoUnidade FROM (
                            SELECT DISTINCT 'Objetivo estratégico - ' + O.DS_OBJETIVO DS_OBJETIVO_INICIATIVA,  R.DS_RESULTADO_ALCANCADO, E.CD_SEQ_EXERCICIO,  E.NU_ANO_EXERCICIO  , C.DS_UNIDADE, C.CD_UNIDADE , R.CD_SEQ_RESULTADO_ALCANCADO
                             FROM TB_META M 
									INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
									INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
									INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
                                    INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
                                    INNER JOIN RL_INICIATIVA_OBJETIVO RI ON M.CD_INICIATIVA_PE = RI.CD_SEQ_INICIATIVA
                                    INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA
                                    INNER JOIN TB_OBJETIVO O ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
                                    LEFT JOIN TB_RESULTADO_ALCANCADO R ON O.CD_SEQ_OBJETIVO = R.CD_SEQ_OBJETIVO 
                                        AND E.CD_SEQ_EXERCICIO = R.CD_SEQ_EXERCICIO
                                        AND C.CD_UNIDADE = R.CD_UNIDADE
                            WHERE O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 1 
                            UNION
                            SELECT DISTINCT 'Objetivo estratégico - ' + O.DS_OBJETIVO DS_OBJETIVO_INICIATIVA, R.DS_RESULTADO_ALCANCADO, E.CD_SEQ_EXERCICIO, E.NU_ANO_EXERCICIO  , C.DS_UNIDADE , C.CD_UNIDADE, R.CD_SEQ_RESULTADO_ALCANCADO
                                    FROM TB_META M
									INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
									INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
									INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
                                    INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
                                    INNER JOIN RL_INICIATIVA_OBJETIVO RI ON M.CD_INICIATIVA_PPA = RI.CD_SEQ_INICIATIVA
                                    INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA
                                    INNER JOIN TB_OBJETIVO O ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
                                    LEFT JOIN TB_RESULTADO_ALCANCADO R ON  O.CD_SEQ_OBJETIVO = R.CD_SEQ_OBJETIVO 
                                        AND E.CD_SEQ_EXERCICIO = R.CD_SEQ_EXERCICIO
                                        AND C.CD_UNIDADE = R.CD_UNIDADE
                            WHERE O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 2 
                            UNION
                            SELECT DISTINCT 'Missão Institucional - ' + I.DS_INICIATIVA DS_OBJETIVO_INICIATIVA, R.DS_RESULTADO_ALCANCADO, E.CD_SEQ_EXERCICIO, E.NU_ANO_EXERCICIO , C.DS_UNIDADE ,C.CD_UNIDADE, R.CD_SEQ_RESULTADO_ALCANCADO
                             FROM TB_META M
									INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
									INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
									INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
                                    INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
                                    INNER JOIN TB_INICIATIVA I ON M.CD_INICIATIVA_MISSAO = I.CD_SEQ_INICIATIVA
                                    LEFT JOIN TB_RESULTADO_ALCANCADO R ON  I.CD_SEQ_INICIATIVA = R.CD_SEQ_INICIATIVA 
                                        AND E.CD_SEQ_EXERCICIO = R.CD_SEQ_EXERCICIO 
                                        AND C.CD_UNIDADE = R.CD_UNIDADE
                            WHERE I.CD_TIPO_INICIATIVA = 3 ) AS ResultadoAlcancado {0} ORDER BY DS_OBJETIVO_INICIATIVA ASC , NU_ANO_EXERCICIO ASC";

            string sqlWhere = @" WHERE 1 = 1 
                                    AND DS_RESULTADO_ALCANCADO IS NOT NULL";

            if (AnoExercicio > 0)
                sqlWhere += " AND NU_ANO_EXERCICIO = " + AnoExercicio;

            var result = Session.CreateSQLQuery(string.Format(sql, sqlWhere)).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var listaResultadosAlcancados = new List<VOResultadosAlcancadosPorExercicio>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;
                    var resultadoAlcancado = new VOResultadosAlcancadosPorExercicio()
                    {
                        DescricaoObjetivoIniciativa = (string)asHash["DescricaoObjetivoIniciativa"],
                        ResultadoAlcancadoRegistrado = (string)asHash["ResultadoAlcancadoRegistrado"],
                        CodSeqExercicio = (int)asHash["CodSeqExercicio"],
                        AnoExcercicio = int.Parse(asHash["AnoExcercicio"].ToString()),
                        CodigoUnidade = (int)asHash["CodigoUnidade"],
                        DescricaoUnidade = (string)asHash["DescricaoUnidade"],
                        CodSeqResultadoAlcancado = asHash["CodSeqResultadoAlcancado"] != null ? int.Parse(asHash["CodSeqResultadoAlcancado"].ToString()) : 0,
                        ResultadoAlcancadoRegistradoUnidade = (string)asHash["ResultadoAlcancadoRegistradoUnidade"]
                    };

                    listaResultadosAlcancados.Add(resultadoAlcancado);
                }
            }

            return listaResultadosAlcancados;
        }

        public IList<VORelatorioObjetivoEstrategico> ListarObjetivoEstrategicoPE(int anoExercicio, int codSeqObjectivo)
        {
            var sql = @"SELECT	  CD_SEQ_OBJETIVO CodSeqIniciativa
		                        , CD_SEQ_META CodSeqMeta 
		                        , CD_SEQ_INDICADOR CodSeqIndicador
		                        , CD_SEQ_ACAO CodSeqAcao
		                        , DS_UNIDADE DescricaoUnicade
								, PERCENTUAL_EXECUCAO PercentualExecucao
								, RESULTADO_INDICADOR ResultadoIndicador  FROM (
                                        SELECT DISTINCT O.CD_SEQ_OBJETIVO, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, CD_TIPO_INICIATIVA, 0 PERCENTUAL_EXECUCAO , 0 RESULTADO_INDICADOR
                                            FROM TB_OBJETIVO O
	                                            INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
	                                            INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PE
	                                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                            INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                            INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
						                        INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
					                        WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 1 
				                        UNION
                                        SELECT DISTINCT O.CD_SEQ_OBJETIVO, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, II.CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, CD_TIPO_INICIATIVA, 0 PERCENTUAL_EXECUCAO,
											CASE WHEN II.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR)
												 WHEN II.CD_TIPO_RESULTADO = 2 THEN 
											CASE WHEN (SELECT COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) > 0 THEN 
													  (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) ELSE 0 END END AS RESULTADO_INDICADOR
                                            FROM TB_OBJETIVO O
	                                            INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
	                                            INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PE
	                                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                            INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                            INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
						                        INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
						                        INNER JOIN TB_INDICADOR II ON M.CD_SEQ_META = II.CD_META  AND (CD_SEQ_STATUS_INDICADOR IS NULL OR CD_SEQ_STATUS_INDICADOR = 1)
					                        WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 1
											GROUP BY O.CD_SEQ_OBJETIVO, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, II.CD_SEQ_INDICADOR, C.DS_UNIDADE, I.CD_TIPO_INICIATIVA, II.CD_TIPO_RESULTADO
				                        UNION
                                        SELECT DISTINCT O.CD_SEQ_OBJETIVO, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, A.CD_SEQ_ACAO, C.DS_UNIDADE, CD_TIPO_INICIATIVA, 
											(SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO AS PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR
                                            FROM TB_OBJETIVO O
	                                            INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
	                                            INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PE
	                                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                            INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                            INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
						                        INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
						                        INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
					                        WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 1) TB_RESULTADO {0} ORDER BY DS_UNIDADE";

            string sqlWhere = @" WHERE 1 = 1 ";

            if (anoExercicio > 0)
                sqlWhere += " AND NU_ANO_EXERCICIO = " + anoExercicio;

            if (codSeqObjectivo > 0)
                sqlWhere += " AND CD_SEQ_OBJETIVO = " + codSeqObjectivo;

            var result = Session.CreateSQLQuery(string.Format(sql, sqlWhere)).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var listaObjetivoEstrategicoDetalhado = new List<VORelatorioObjetivoEstrategico>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;

                    var objetivoEstrategico = new VORelatorioObjetivoEstrategico()
                    {
                        CodSeqIniciativa = asHash["CodSeqIniciativa"] != null ? int.Parse(asHash["CodSeqIniciativa"].ToString()) : int.Parse("0"),
                        CodSeqMeta = asHash["CodSeqMeta"] != null ? int.Parse(asHash["CodSeqMeta"].ToString()) : int.Parse("0"),
                        CodSeqIndicador = asHash["CodSeqIndicador"] != null ? int.Parse(asHash["CodSeqIndicador"].ToString()) : int.Parse("0"),
                        CodSeqAcao = asHash["CodSeqAcao"] != null ? int.Parse(asHash["CodSeqAcao"].ToString()) : int.Parse("0"),
                        DescricaoUnicade = asHash["DescricaoUnicade"].ToString(),
                        PercentualExecucao = asHash["PercentualExecucao"] != null ? decimal.Parse(asHash["PercentualExecucao"].ToString()) : decimal.Parse("0"),
                        ResultadoIndicador = asHash["ResultadoIndicador"] != null ? decimal.Parse(asHash["ResultadoIndicador"].ToString()) : decimal.Parse("0")
                    };

                    listaObjetivoEstrategicoDetalhado.Add(objetivoEstrategico);
                }
            }

            return listaObjetivoEstrategicoDetalhado;
        }

        public IList<VORelatorioObjetivoEstrategico> ListarObjetivoEstrategicoPPA(int anoExercicio, int codSeqObjectivo)
        {
            var sql = @"SELECT	  CD_SEQ_OBJETIVO CodSeqIniciativa
		                        , CD_SEQ_META CodSeqMeta 
		                        , CD_SEQ_INDICADOR CodSeqIndicador
		                        , CD_SEQ_ACAO CodSeqAcao
		                        , DS_UNIDADE DescricaoUnicade
								, PERCENTUAL_EXECUCAO PercentualExecucao
								, RESULTADO_INDICADOR ResultadoIndicador  FROM (
                                  SELECT DISTINCT O.CD_SEQ_OBJETIVO, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, CD_TIPO_INICIATIVA, 0 PERCENTUAL_EXECUCAO , 0 RESULTADO_INDICADOR
                                            FROM TB_OBJETIVO O
	                                            INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
	                                            INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PPA
	                                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                            INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                            INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
						                        INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
					                        WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 2 
				                        UNION
                                        SELECT DISTINCT O.CD_SEQ_OBJETIVO, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, II.CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, CD_TIPO_INICIATIVA, 0 PERCENTUAL_EXECUCAO,
											CASE WHEN II.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR)
												 WHEN II.CD_TIPO_RESULTADO = 2 THEN 
											CASE WHEN (SELECT COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) > 0 THEN 
													  (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) ELSE 0 END END AS RESULTADO_INDICADOR
                                            FROM TB_OBJETIVO O
	                                            INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
	                                            INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PPA
	                                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                            INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                            INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
						                        INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
						                        INNER JOIN TB_INDICADOR II ON M.CD_SEQ_META = II.CD_META AND (CD_SEQ_STATUS_INDICADOR IS NULL OR CD_SEQ_STATUS_INDICADOR = 1)
					                        WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 2  
											GROUP BY O.CD_SEQ_OBJETIVO, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, II.CD_SEQ_INDICADOR, C.DS_UNIDADE, I.CD_TIPO_INICIATIVA, II.CD_TIPO_RESULTADO
				                        UNION
                                        SELECT DISTINCT O.CD_SEQ_OBJETIVO, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, A.CD_SEQ_ACAO, C.DS_UNIDADE, CD_TIPO_INICIATIVA, 
											(SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO AS PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR
                                            FROM TB_OBJETIVO O
	                                            INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
	                                            INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PPA
	                                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                            INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                            INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
						                        INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
						                        INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
					                        WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 2) TB_RESULTADO {0} ORDER BY DS_UNIDADE";

            string sqlWhere = @" WHERE 1 = 1 ";

            if (anoExercicio > 0)
                sqlWhere += " AND NU_ANO_EXERCICIO = " + anoExercicio;

            if (codSeqObjectivo > 0)
                sqlWhere += " AND CD_SEQ_OBJETIVO = " + codSeqObjectivo;

            var result = Session.CreateSQLQuery(string.Format(sql, sqlWhere)).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var listaObjetivoEstrategicoDetalhado = new List<VORelatorioObjetivoEstrategico>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;

                    var objetivoEstrategico = new VORelatorioObjetivoEstrategico()
                    {
                        CodSeqIniciativa = asHash["CodSeqIniciativa"] != null ? int.Parse(asHash["CodSeqIniciativa"].ToString()) : int.Parse("0"),
                        CodSeqMeta = asHash["CodSeqMeta"] != null ? int.Parse(asHash["CodSeqMeta"].ToString()) : int.Parse("0"),
                        CodSeqIndicador = asHash["CodSeqIndicador"] != null ? int.Parse(asHash["CodSeqIndicador"].ToString()) : int.Parse("0"),
                        CodSeqAcao = asHash["CodSeqAcao"] != null ? int.Parse(asHash["CodSeqAcao"].ToString()) : int.Parse("0"),
                        DescricaoUnicade = asHash["DescricaoUnicade"].ToString(),
                        PercentualExecucao = asHash["PercentualExecucao"] != null ? decimal.Parse(asHash["PercentualExecucao"].ToString()) : decimal.Parse("0"),
                        ResultadoIndicador = asHash["ResultadoIndicador"] != null ? decimal.Parse(asHash["ResultadoIndicador"].ToString()) : decimal.Parse("0")
                    };

                    listaObjetivoEstrategicoDetalhado.Add(objetivoEstrategico);
                }
            }

            return listaObjetivoEstrategicoDetalhado;
        }

        public IList<VORelatorioObjetivoEstrategico> ListarMeta(int anoExercicio, int codTipoMeta)
        {
            var sql = @"SELECT	  CD_SEQ_META CodSeqMeta 
		                            , CD_SEQ_INDICADOR CodSeqIndicador
		                            , CD_SEQ_ACAO CodSeqAcao
		                            , DS_UNIDADE DescricaoUnicade
		                            , PERCENTUAL_EXECUCAO PercentualExecucao
		                            , RESULTADO_INDICADOR ResultadoIndicador
									, DS_TIPO_META DescricaoTipo
                                FROM (
                                SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_META, TM.DS_TIPO_META
                                FROM TB_META M
	                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                INNER JOIN TB_CORPORATIVO_UNIDADE C ON M.CD_UNIDADE = C.CD_UNIDADE 
									INNER JOIN TB_TIPO_META TM ON M.CD_TIPO_META = TM.CD_SEQ_TIPO_META
                                UNION
                                SELECT DISTINCT M.CD_SEQ_META , I.CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO,
	                                CASE WHEN I.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = I.CD_SEQ_INDICADOR)
									        WHEN I.CD_TIPO_RESULTADO = 2 THEN 
							        CASE WHEN (SELECT COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = I.CD_SEQ_INDICADOR) > 0 THEN 
										        (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = I.CD_SEQ_INDICADOR) ELSE 0 END END AS RESULTADO_INDICADOR, CD_TIPO_META, TM.DS_TIPO_META 
                                FROM TB_INDICADOR I
	                                INNER JOIN TB_META M ON I.CD_META = M.CD_SEQ_META
	                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                INNER JOIN TB_CORPORATIVO_UNIDADE C ON M.CD_UNIDADE = C.CD_UNIDADE 
									INNER JOIN TB_TIPO_META TM ON M.CD_TIPO_META = TM.CD_SEQ_TIPO_META
                                WHERE (CD_SEQ_STATUS_INDICADOR IS NULL OR CD_SEQ_STATUS_INDICADOR = 1)
                                UNION
                                SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_META, A.CD_SEQ_ACAO,C.DS_UNIDADE, E.NU_ANO_EXERCICIO,
	                            (SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO AS PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_META, TM.DS_TIPO_META 
                                FROM TB_ACAO A
	                                INNER JOIN TB_META M ON A.CD_META = M.CD_SEQ_META
	                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                INNER JOIN TB_CORPORATIVO_UNIDADE C ON M.CD_UNIDADE = C.CD_UNIDADE 
									INNER JOIN TB_TIPO_META TM ON M.CD_TIPO_META = TM.CD_SEQ_TIPO_META
                                WHERE (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)) TB_META {0} ORDER BY DS_UNIDADE";

            string sqlWhere = @" WHERE 1 = 1 ";

            if (anoExercicio > 0)
                sqlWhere += " AND NU_ANO_EXERCICIO = " + anoExercicio;

            if (codTipoMeta > 0)
                sqlWhere += " AND CD_TIPO_META = " + codTipoMeta;

            var result = Session.CreateSQLQuery(string.Format(sql, sqlWhere)).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var listaMeta = new List<VORelatorioObjetivoEstrategico>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;

                    var meta = new VORelatorioObjetivoEstrategico()
                    {
                        CodSeqMeta = asHash["CodSeqMeta"] != null ? int.Parse(asHash["CodSeqMeta"].ToString()) : int.Parse("0"),
                        CodSeqIndicador = asHash["CodSeqIndicador"] != null ? int.Parse(asHash["CodSeqIndicador"].ToString()) : int.Parse("0"),
                        CodSeqAcao = asHash["CodSeqAcao"] != null ? int.Parse(asHash["CodSeqAcao"].ToString()) : int.Parse("0"),
                        DescricaoUnicade = asHash["DescricaoUnicade"].ToString(),
                        PercentualExecucao = asHash["PercentualExecucao"] != null ? decimal.Parse(asHash["PercentualExecucao"].ToString()) : decimal.Parse("0"),
                        ResultadoIndicador = asHash["ResultadoIndicador"] != null ? decimal.Parse(asHash["ResultadoIndicador"].ToString()) : decimal.Parse("0"),
                        DescricaoTipo = asHash["DescricaoTipo"].ToString()
                    };

                    listaMeta.Add(meta);
                }
            }

            return listaMeta;
        }

        public IList<VORelatorioObjetivoEstrategico> ListarInstrumentos(int anoExercicio, int codTipoInstrumento)
        {
            var sql = @"SELECT	  CD_SEQ_META CodSeqMeta 
		                        , CD_SEQ_INDICADOR CodSeqIndicador
		                        , CD_SEQ_ACAO CodSeqAcao
		                        , DS_UNIDADE DescricaoUnicade
		                        , PERCENTUAL_EXECUCAO PercentualExecucao
		                        , RESULTADO_INDICADOR ResultadoIndicador
								, DS_TIPO_INSTRUMENTO DescricaoTipo
                            FROM (
                            SELECT * FROM (
                            SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_INSTRUMENTO, TI.DS_TIPO_INSTRUMENTO
                                FROM TB_INSTRUMENTO I
	                                    INNER JOIN TB_META M ON I.CD_SEQ_INSTRUMENTO = M.CD_GESTAO_RISCO
			                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
										INNER JOIN TB_TIPO_INSTRUMENTO TI ON I.CD_TIPO_INSTRUMENTO = TI.COD_SEQ_TIPO_INSTRUMENTO
                            UNION
                            SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_INSTRUMENTO, TI.DS_TIPO_INSTRUMENTO
                                FROM TB_INSTRUMENTO I
	                                    INNER JOIN TB_META M ON I.CD_SEQ_INSTRUMENTO = M.CD_AGENDA_REGULATORIA
			                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
										INNER JOIN TB_TIPO_INSTRUMENTO TI ON I.CD_TIPO_INSTRUMENTO = TI.COD_SEQ_TIPO_INSTRUMENTO
                            UNION
                            SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_INSTRUMENTO, TI.DS_TIPO_INSTRUMENTO
                                FROM TB_INSTRUMENTO I
	                                    INNER JOIN TB_META M ON I.CD_SEQ_INSTRUMENTO = M.CD_DESBUROCRATIZACAO
			                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
										INNER JOIN TB_TIPO_INSTRUMENTO TI ON I.CD_TIPO_INSTRUMENTO = TI.COD_SEQ_TIPO_INSTRUMENTO
                            UNION
                            SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_INSTRUMENTO, TI.DS_TIPO_INSTRUMENTO
                                FROM TB_INSTRUMENTO I
	                                    INNER JOIN TB_META M ON I.CD_SEQ_INSTRUMENTO = M.CD_INTEGRIDADE
			                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
										INNER JOIN TB_TIPO_INSTRUMENTO TI ON I.CD_TIPO_INSTRUMENTO = TI.COD_SEQ_TIPO_INSTRUMENTO) AS META
                            UNION
                            SELECT * FROM (
                            SELECT DISTINCT M.CD_SEQ_META, II.CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 
	                            CASE WHEN II.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR)
			                            WHEN II.CD_TIPO_RESULTADO = 2 THEN 
	                            CASE WHEN (SELECT COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) > 0 THEN 
				                            (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) ELSE 0 END END AS RESULTADO_INDICADOR, CD_TIPO_INSTRUMENTO, TI.DS_TIPO_INSTRUMENTO
                                FROM TB_INSTRUMENTO I
	                                    INNER JOIN TB_META M ON I.CD_SEQ_INSTRUMENTO = M.CD_GESTAO_RISCO
			                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
			                            INNER JOIN TB_INDICADOR II ON M.CD_SEQ_META = II.CD_META AND (CD_SEQ_STATUS_INDICADOR IS NULL OR CD_SEQ_STATUS_INDICADOR = 1)
										INNER JOIN TB_TIPO_INSTRUMENTO TI ON I.CD_TIPO_INSTRUMENTO = TI.COD_SEQ_TIPO_INSTRUMENTO
                            UNION
                            SELECT DISTINCT M.CD_SEQ_META, II.CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 
	                            CASE WHEN II.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR)
			                            WHEN II.CD_TIPO_RESULTADO = 2 THEN 
	                            CASE WHEN (SELECT COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) > 0 THEN 
				                            (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) ELSE 0 END END AS RESULTADO_INDICADOR, CD_TIPO_INSTRUMENTO, TI.DS_TIPO_INSTRUMENTO
                                FROM TB_INSTRUMENTO I
	                                    INNER JOIN TB_META M ON I.CD_SEQ_INSTRUMENTO = M.CD_AGENDA_REGULATORIA
			                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
			                            INNER JOIN TB_INDICADOR II ON M.CD_SEQ_META = II.CD_META AND (CD_SEQ_STATUS_INDICADOR IS NULL OR CD_SEQ_STATUS_INDICADOR = 1)
										INNER JOIN TB_TIPO_INSTRUMENTO TI ON I.CD_TIPO_INSTRUMENTO = TI.COD_SEQ_TIPO_INSTRUMENTO
                            UNION
                            SELECT DISTINCT M.CD_SEQ_META, II.CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 
	                            CASE WHEN II.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR)
			                            WHEN II.CD_TIPO_RESULTADO = 2 THEN 
	                            CASE WHEN (SELECT COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) > 0 THEN 
				                            (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) ELSE 0 END END AS RESULTADO_INDICADOR, CD_TIPO_INSTRUMENTO, TI.DS_TIPO_INSTRUMENTO
                                FROM TB_INSTRUMENTO I
	                                    INNER JOIN TB_META M ON I.CD_SEQ_INSTRUMENTO = M.CD_DESBUROCRATIZACAO
			                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
			                            INNER JOIN TB_INDICADOR II ON M.CD_SEQ_META = II.CD_META AND (CD_SEQ_STATUS_INDICADOR IS NULL OR CD_SEQ_STATUS_INDICADOR = 1)
										INNER JOIN TB_TIPO_INSTRUMENTO TI ON I.CD_TIPO_INSTRUMENTO = TI.COD_SEQ_TIPO_INSTRUMENTO
                            UNION
                            SELECT DISTINCT M.CD_SEQ_META, II.CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 
	                            CASE WHEN II.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR)
			                            WHEN II.CD_TIPO_RESULTADO = 2 THEN 
	                            CASE WHEN (SELECT COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) > 0 THEN 
				                            (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) ELSE 0 END END AS RESULTADO_INDICADOR, CD_TIPO_INSTRUMENTO, TI.DS_TIPO_INSTRUMENTO
                                FROM TB_INSTRUMENTO I
	                                    INNER JOIN TB_META M ON I.CD_SEQ_INSTRUMENTO = M.CD_INTEGRIDADE
			                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
			                            INNER JOIN TB_INDICADOR II ON M.CD_SEQ_META = II.CD_META AND (CD_SEQ_STATUS_INDICADOR IS NULL OR CD_SEQ_STATUS_INDICADOR = 1)
										INNER JOIN TB_TIPO_INSTRUMENTO TI ON I.CD_TIPO_INSTRUMENTO = TI.COD_SEQ_TIPO_INSTRUMENTO) AS INDICADOR
                            UNION
                            SELECT * FROM (
                            SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, A.CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 
	                            (SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO AS PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_INSTRUMENTO, TI.DS_TIPO_INSTRUMENTO
                                FROM TB_INSTRUMENTO I
	                                    INNER JOIN TB_META M ON I.CD_SEQ_INSTRUMENTO = M.CD_GESTAO_RISCO
			                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
			                            INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
										INNER JOIN TB_TIPO_INSTRUMENTO TI ON I.CD_TIPO_INSTRUMENTO = TI.COD_SEQ_TIPO_INSTRUMENTO
                            UNION
                            SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, A.CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 
	                            (SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO AS PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_INSTRUMENTO, TI.DS_TIPO_INSTRUMENTO
                                FROM TB_INSTRUMENTO I
	                                    INNER JOIN TB_META M ON I.CD_SEQ_INSTRUMENTO = M.CD_AGENDA_REGULATORIA
			                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
			                            INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
										INNER JOIN TB_TIPO_INSTRUMENTO TI ON I.CD_TIPO_INSTRUMENTO = TI.COD_SEQ_TIPO_INSTRUMENTO
                            UNION
                            SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, A.CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO,
	                            (SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO AS PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_INSTRUMENTO, TI.DS_TIPO_INSTRUMENTO
                                FROM TB_INSTRUMENTO I
	                                    INNER JOIN TB_META M ON I.CD_SEQ_INSTRUMENTO = M.CD_DESBUROCRATIZACAO
			                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
			                            INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
										INNER JOIN TB_TIPO_INSTRUMENTO TI ON I.CD_TIPO_INSTRUMENTO = TI.COD_SEQ_TIPO_INSTRUMENTO
                            UNION
                            SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, A.CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 
	                            (SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO AS PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_INSTRUMENTO, TI.DS_TIPO_INSTRUMENTO
                                FROM TB_INSTRUMENTO I
	                                    INNER JOIN TB_META M ON I.CD_SEQ_INSTRUMENTO = M.CD_INTEGRIDADE
			                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
			                            INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
										INNER JOIN TB_TIPO_INSTRUMENTO TI ON I.CD_TIPO_INSTRUMENTO = TI.COD_SEQ_TIPO_INSTRUMENTO) ACAO) AS INSTRUMENTO {0} ORDER BY DS_UNIDADE";

            string sqlWhere = @" WHERE 1 = 1 ";

            if (anoExercicio > 0)
                sqlWhere += " AND NU_ANO_EXERCICIO = " + anoExercicio;

            if (codTipoInstrumento > 0)
                sqlWhere += " AND CD_TIPO_INSTRUMENTO = " + codTipoInstrumento;

            var result = Session.CreateSQLQuery(string.Format(sql, sqlWhere)).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var listaInstrumento = new List<VORelatorioObjetivoEstrategico>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;

                    var instrumento = new VORelatorioObjetivoEstrategico()
                    {
                        CodSeqMeta = asHash["CodSeqMeta"] != null ? int.Parse(asHash["CodSeqMeta"].ToString()) : int.Parse("0"),
                        CodSeqIndicador = asHash["CodSeqIndicador"] != null ? int.Parse(asHash["CodSeqIndicador"].ToString()) : int.Parse("0"),
                        CodSeqAcao = asHash["CodSeqAcao"] != null ? int.Parse(asHash["CodSeqAcao"].ToString()) : int.Parse("0"),
                        DescricaoUnicade = asHash["DescricaoUnicade"].ToString(),
                        PercentualExecucao = asHash["PercentualExecucao"] != null ? decimal.Parse(asHash["PercentualExecucao"].ToString()) : decimal.Parse("0"),
                        ResultadoIndicador = asHash["ResultadoIndicador"] != null ? decimal.Parse(asHash["ResultadoIndicador"].ToString()) : decimal.Parse("0"),
                        DescricaoTipo = asHash["DescricaoTipo"].ToString()
                    };

                    listaInstrumento.Add(instrumento);
                }
            }

            return listaInstrumento;
        }

        public IList<VORelatorioObjetivoEstrategico> ListarIniciativa(int anoExercicio, int codTipoIniciativa)
        {
            var sql = @"SELECT	  CD_SEQ_META CodSeqMeta 
		                        , CD_SEQ_INDICADOR CodSeqIndicador
		                        , CD_SEQ_ACAO CodSeqAcao
		                        , DS_UNIDADE DescricaoUnicade
		                        , PERCENTUAL_EXECUCAO PercentualExecucao
		                        , RESULTADO_INDICADOR ResultadoIndicador
								, DS_TIPO_INICIATIVA DescricaoTipo FROM(
                                    SELECT * FROM (
                                    SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_INICIATIVA, TI.DS_TIPO_INICIATIVA
	                                    FROM TB_INICIATIVA I
		                                    INNER JOIN TB_META M ON I.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PE
		                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
		                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
		                                    INNER JOIN TB_TIPO_INICIATIVA TI ON I.CD_TIPO_INICIATIVA = TI.COD_SEQ_TIPO_INICIATIVA
                                    UNION
                                    SELECT DISTINCT M.CD_SEQ_META, II.CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 
	                                    CASE WHEN II.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR)
			                                    WHEN II.CD_TIPO_RESULTADO = 2 THEN 
	                                    CASE WHEN (SELECT COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) > 0 THEN 
				                                    (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) ELSE 0 END END AS RESULTADO_INDICADOR, CD_TIPO_INICIATIVA, TI.DS_TIPO_INICIATIVA
	                                    FROM TB_INICIATIVA I
		                                    INNER JOIN TB_META M ON I.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PE
		                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
		                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
		                                    INNER JOIN TB_INDICADOR II ON M.CD_SEQ_META = II.CD_META AND (CD_SEQ_STATUS_INDICADOR IS NULL OR CD_SEQ_STATUS_INDICADOR = 1)
		                                    INNER JOIN TB_TIPO_INICIATIVA TI ON I.CD_TIPO_INICIATIVA = TI.COD_SEQ_TIPO_INICIATIVA
                                    UNION
                                    SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, A.CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 
	                                    (SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO AS PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_INICIATIVA, TI.DS_TIPO_INICIATIVA
	                                    FROM TB_INICIATIVA I
		                                    INNER JOIN TB_META M ON I.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PE
		                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
		                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
		                                    INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
		                                    INNER JOIN TB_TIPO_INICIATIVA TI ON I.CD_TIPO_INICIATIVA = TI.COD_SEQ_TIPO_INICIATIVA) INICIATIVA_PE
                                    UNION
                                    SELECT * FROM (
                                    SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_INICIATIVA, TI.DS_TIPO_INICIATIVA
	                                    FROM TB_INICIATIVA I
		                                    INNER JOIN TB_META M ON I.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PPA
		                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
		                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
		                                    INNER JOIN TB_TIPO_INICIATIVA TI ON I.CD_TIPO_INICIATIVA = TI.COD_SEQ_TIPO_INICIATIVA
                                    UNION
                                    SELECT DISTINCT M.CD_SEQ_META, II.CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 
	                                    CASE WHEN II.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR)
			                                    WHEN II.CD_TIPO_RESULTADO = 2 THEN 
	                                    CASE WHEN (SELECT COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) > 0 THEN 
				                                    (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) ELSE 0 END END AS RESULTADO_INDICADOR, CD_TIPO_INICIATIVA, TI.DS_TIPO_INICIATIVA
	                                    FROM TB_INICIATIVA I
		                                    INNER JOIN TB_META M ON I.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PPA
		                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
		                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
		                                    INNER JOIN TB_INDICADOR II ON M.CD_SEQ_META = II.CD_META AND (CD_SEQ_STATUS_INDICADOR IS NULL OR CD_SEQ_STATUS_INDICADOR = 1)
		                                    INNER JOIN TB_TIPO_INICIATIVA TI ON I.CD_TIPO_INICIATIVA = TI.COD_SEQ_TIPO_INICIATIVA
                                    UNION
                                    SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, A.CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 
	                                    (SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO AS PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_INICIATIVA, TI.DS_TIPO_INICIATIVA
	                                    FROM TB_INICIATIVA I
		                                    INNER JOIN TB_META M ON I.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PPA
		                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
		                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
		                                    INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
		                                    INNER JOIN TB_TIPO_INICIATIVA TI ON I.CD_TIPO_INICIATIVA = TI.COD_SEQ_TIPO_INICIATIVA) INICIATIVA_PPA
                                    UNION 
                                    SELECT * FROM (
                                    SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_INICIATIVA, TI.DS_TIPO_INICIATIVA
	                                    FROM TB_INICIATIVA I
		                                    INNER JOIN TB_META M ON I.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_MISSAO
		                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
		                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
		                                    INNER JOIN TB_TIPO_INICIATIVA TI ON I.CD_TIPO_INICIATIVA = TI.COD_SEQ_TIPO_INICIATIVA
                                    UNION
                                    SELECT DISTINCT M.CD_SEQ_META, II.CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 0 PERCENTUAL_EXECUCAO, 
	                                    CASE WHEN II.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR)
			                                    WHEN II.CD_TIPO_RESULTADO = 2 THEN 
	                                    CASE WHEN (SELECT COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) > 0 THEN 
				                                    (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) ELSE 0 END END AS RESULTADO_INDICADOR, CD_TIPO_INICIATIVA, TI.DS_TIPO_INICIATIVA
	                                    FROM TB_INICIATIVA I
		                                    INNER JOIN TB_META M ON I.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_MISSAO
		                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
		                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
		                                    INNER JOIN TB_INDICADOR II ON M.CD_SEQ_META = II.CD_META AND (CD_SEQ_STATUS_INDICADOR IS NULL OR CD_SEQ_STATUS_INDICADOR = 1)
		                                    INNER JOIN TB_TIPO_INICIATIVA TI ON I.CD_TIPO_INICIATIVA = TI.COD_SEQ_TIPO_INICIATIVA
                                    UNION
                                    SELECT DISTINCT M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, A.CD_SEQ_ACAO, C.DS_UNIDADE, E.NU_ANO_EXERCICIO, 
	                                    (SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO AS PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, CD_TIPO_INICIATIVA, TI.DS_TIPO_INICIATIVA
	                                    FROM TB_INICIATIVA I
		                                    INNER JOIN TB_META M ON I.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_MISSAO
		                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
		                                    INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
		                                    INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
		                                    INNER JOIN TB_TIPO_INICIATIVA TI ON I.CD_TIPO_INICIATIVA = TI.COD_SEQ_TIPO_INICIATIVA) INICIATIVA_MISSAO) INICIATIVA {0} ORDER BY DS_UNIDADE";

            string sqlWhere = @" WHERE 1 = 1 ";

            if (anoExercicio > 0)
                sqlWhere += " AND NU_ANO_EXERCICIO = " + anoExercicio;

            if (codTipoIniciativa > 0)
                sqlWhere += " AND CD_TIPO_INICIATIVA = " + codTipoIniciativa;

            var result = Session.CreateSQLQuery(string.Format(sql, sqlWhere)).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var listaInstrumento = new List<VORelatorioObjetivoEstrategico>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;

                    var instrumento = new VORelatorioObjetivoEstrategico()
                    {
                        CodSeqMeta = asHash["CodSeqMeta"] != null ? int.Parse(asHash["CodSeqMeta"].ToString()) : int.Parse("0"),
                        CodSeqIndicador = asHash["CodSeqIndicador"] != null ? int.Parse(asHash["CodSeqIndicador"].ToString()) : int.Parse("0"),
                        CodSeqAcao = asHash["CodSeqAcao"] != null ? int.Parse(asHash["CodSeqAcao"].ToString()) : int.Parse("0"),
                        DescricaoUnicade = asHash["DescricaoUnicade"].ToString(),
                        PercentualExecucao = asHash["PercentualExecucao"] != null ? decimal.Parse(asHash["PercentualExecucao"].ToString()) : decimal.Parse("0"),
                        ResultadoIndicador = asHash["ResultadoIndicador"] != null ? decimal.Parse(asHash["ResultadoIndicador"].ToString()) : decimal.Parse("0"),
                        DescricaoTipo = asHash["DescricaoTipo"].ToString()
                    };

                    listaInstrumento.Add(instrumento);
                }
            }

            return listaInstrumento;
        }

        public IList<VOObjetivosDetalhado> ListarObjetivosEstrategicoDetalhado(int anoExercicio, int codObjetivo, int codTipo)
        {
            var sql = @"SELECT   CD_SEQ_OBJETIVO CodSeqObjetivo
		                        , CD_SEQ_META CodSeqMeta
		                        , DS_OBJETIVO DescricaoObjetivo
		                        , NU_ANO_EXERCICIO  AnoExercicio
								, DS_UNIDADE DescricaoUnidade
		                        , CD_TIPO CodTipo
		                        FROM (SELECT O.CD_SEQ_OBJETIVO, M.CD_SEQ_META, O.DS_OBJETIVO, NU_ANO_EXERCICIO, C.DS_UNIDADE, 1 CD_TIPO  
		                                        FROM TB_OBJETIVO O
			                                        INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
			                                        INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PE
			                                        INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                                        INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                                        INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
			                                        INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
			                                        INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META
			                                        INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
			                                        INNER JOIN TB_CORPORATIVO_UNIDADE C ON M.CD_UNIDADE = C.CD_UNIDADE
	                                        WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 1
                                      UNION
                                      SELECT O.CD_SEQ_OBJETIVO, M.CD_SEQ_META, O.DS_OBJETIVO, NU_ANO_EXERCICIO, C.DS_UNIDADE,1 CD_TIPO
		                                    FROM TB_OBJETIVO O
			                                    INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
			                                    INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PPA
			                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
			                                    INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
			                                    INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META
			                                    INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
			                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON M.CD_UNIDADE = C.CD_UNIDADE
	                                    WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 2
                                      UNION
                                      SELECT I.CD_SEQ_INICIATIVA CD_SEQ_OBJETIVO,  M.CD_SEQ_META,  I.DS_INICIATIVA DS_OBJETIVO , NU_ANO_EXERCICIO, C.DS_UNIDADE,2 CD_TIPO
		                                    FROM TB_INICIATIVA I
			                                    INNER JOIN TB_META M ON I.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_MISSAO
			                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
			                                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
			                                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
			                                    INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META
			                                    INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
			                                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON M.CD_UNIDADE = C.CD_UNIDADE
	                                    WHERE  I.CD_TIPO_INICIATIVA = 3) TB_OBJ {0}";

            string sqlWhere = @" WHERE 1 = 1 ";

            if (anoExercicio > 0)
                sqlWhere += " AND NU_ANO_EXERCICIO = " + anoExercicio;

            if (codObjetivo > 0)
                sqlWhere += " AND CD_SEQ_OBJETIVO = " + codObjetivo;

            if (codTipo > 0)
                sqlWhere += " AND CD_TIPO = " + codTipo;

            var result = Session.CreateSQLQuery(string.Format(sql, sqlWhere)).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var lista = new List<VOObjetivosDetalhado>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;

                    var objetivo = new VOObjetivosDetalhado()
                    {
                        CodSeqObjetivo = asHash["CodSeqObjetivo"] != null ? int.Parse(asHash["CodSeqObjetivo"].ToString()) : int.Parse("0"),
                        CodSeqMeta = asHash["CodSeqMeta"] != null ? int.Parse(asHash["CodSeqMeta"].ToString()) : int.Parse("0"),
                        DescricaoObjetivo = asHash["DescricaoObjetivo"].ToString(),
                        DescricaoUnidade = asHash["DescricaoUnidade"].ToString(),
                        AnoExercicio = asHash["AnoExercicio"] != null ? int.Parse(asHash["AnoExercicio"].ToString()) : int.Parse("0"),
                        CodTipo = asHash["CodTipo"] != null ? int.Parse(asHash["CodTipo"].ToString()) : int.Parse("0")
                    };

                    lista.Add(objetivo);
                }
            }

            return lista;
        }

        public IList<VORecursosTotais> ObterValorDeRecurso(int codSeqMeta, int anoExercicio)
        {
            var sql = @"SELECT 
	                      CD_SEQ_INSTRUMENTO CodSeqInstrumento
	                    , CD_SEQ_META CodSeqMenta
	                    , NU_ANO_EXERCICIO AnoExercicio
	                    , TOTAL_RECURSO_ESTIMADO TotalRecursoEstimado                 
	                    , TOTAL_RECURSO_ORCAMENTARIO_UTILIZADO TotalRecursoOrcamentarioUtilizado
	                    FROM (
                    SELECT DISTINCT
		                    INST.CD_SEQ_INSTRUMENTO , 
		                    M.CD_SEQ_META, 
		                    E.NU_ANO_EXERCICIO, 
		                    C.DS_UNIDADE,
		                    (SELECT SUM(VL_RECURSO) FROM TB_RECURSO WHERE CD_ACAO = A.CD_SEQ_ACAO AND CD_TIPO_RECURSO = 7) AS TOTAL_RECURSO_ESTIMADO,
		                    (SELECT SUM(VL_RECURSO_FINANCEIRO) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) AS TOTAL_RECURSO_ORCAMENTARIO_UTILIZADO
	                    FROM TB_INSTRUMENTO INST
		                    INNER JOIN TB_META M ON INST.CD_SEQ_INSTRUMENTO = M.CD_GESTAO_RISCO
		                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                    INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
		                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON M.CD_UNIDADE = C.CD_UNIDADE
		                    LEFT JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META
                    UNION
                    SELECT DISTINCT
		                    INST.CD_SEQ_INSTRUMENTO , 
		                    M.CD_SEQ_META, 
		                    E.NU_ANO_EXERCICIO, 
		                    C.DS_UNIDADE,
		                    (SELECT SUM(VL_RECURSO) FROM TB_RECURSO WHERE CD_ACAO = A.CD_SEQ_ACAO AND CD_TIPO_RECURSO = 7) AS TOTAL_RECURSO_ESTIMADO,
		                    (SELECT SUM(VL_RECURSO_FINANCEIRO) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) AS TOTAL_RECURSO_ORCAMENTARIO_UTILIZADO
	                    FROM TB_INSTRUMENTO INST
		                    INNER JOIN TB_META M ON INST.CD_SEQ_INSTRUMENTO = M.CD_AGENDA_REGULATORIA
		                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                    INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
		                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON M.CD_UNIDADE = C.CD_UNIDADE
		                    LEFT JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META
                    UNION
                    SELECT DISTINCT
		                    INST.CD_SEQ_INSTRUMENTO , 
		                    M.CD_SEQ_META, 
		                    E.NU_ANO_EXERCICIO, 
		                    C.DS_UNIDADE,
		                    (SELECT SUM(VL_RECURSO) FROM TB_RECURSO WHERE CD_ACAO = A.CD_SEQ_ACAO AND CD_TIPO_RECURSO = 7) AS TOTAL_RECURSO_ESTIMADO,
		                    (SELECT SUM(VL_RECURSO_FINANCEIRO) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) AS TOTAL_RECURSO_ORCAMENTARIO_UTILIZADO
	                    FROM TB_INSTRUMENTO INST
		                    INNER JOIN TB_META M ON INST.CD_SEQ_INSTRUMENTO = M.CD_DESBUROCRATIZACAO
		                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                    INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
		                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON M.CD_UNIDADE = C.CD_UNIDADE
		                    LEFT JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META
                    UNION
                    SELECT DISTINCT
		                    INST.CD_SEQ_INSTRUMENTO , 
		                    M.CD_SEQ_META, 
		                    E.NU_ANO_EXERCICIO, 
		                    C.DS_UNIDADE,
		                    (SELECT SUM(VL_RECURSO) FROM TB_RECURSO WHERE CD_ACAO = A.CD_SEQ_ACAO AND CD_TIPO_RECURSO = 7) AS TOTAL_RECURSO_ESTIMADO ,
		                    (SELECT SUM(VL_RECURSO_FINANCEIRO) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) AS TOTAL_RECURSO_ORCAMENTARIO_UTILIZADO
	                    FROM TB_INSTRUMENTO INST
		                    INNER JOIN TB_META M ON INST.CD_SEQ_INSTRUMENTO = M.CD_INTEGRIDADE
		                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                    INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
		                    INNER JOIN TB_CORPORATIVO_UNIDADE C ON M.CD_UNIDADE = C.CD_UNIDADE
		                    LEFT JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META) R {0}";


            string sqlWhere = @" WHERE 1 = 1 ";

            if (codSeqMeta > 0)
                sqlWhere += " AND CD_SEQ_META = " + codSeqMeta;

            if (anoExercicio > 0)
                sqlWhere += " AND NU_ANO_EXERCICIO = " + anoExercicio;


            var result = Session.CreateSQLQuery(string.Format(sql, sqlWhere)).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var lista = new List<VORecursosTotais>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;

                    var objetivo = new VORecursosTotais()
                    {
                        TotalRecursoEstimado = asHash["TotalRecursoEstimado"] != null ? decimal.Parse(asHash["TotalRecursoEstimado"].ToString()) : decimal.Parse("0"),
                        TotalRecursoOrcamentarioUtilizado = asHash["TotalRecursoOrcamentarioUtilizado"] != null ? decimal.Parse(asHash["TotalRecursoOrcamentarioUtilizado"].ToString()) : decimal.Parse("0")
                    };

                    lista.Add(objetivo);
                }
            }

            return lista;
        }

        public IList<VOObjetivoEstrategicoTotalizadorDetalhadado> ListarObjetivoEstrategicoTotal(int anoExercicio, int codObjetivo, int codTipo)
        {
            var sql = @"SELECT	 DS_UNIDADE DescricaoUnidade
		                        ,DS_OBJETIVO DescricaoObjetivo
		                        ,CD_SEQ_OBJETIVO CodSeqObjetivo
		                        ,CD_SEQ_INICIATIVA CodSeqIniciativa
		                        ,RECURSO_PREVISTO RecursoPrevisto
		                        ,REPORTE_RECURSO_UTILIZADO ReporteRecursoUtilizado  
                        FROM (
                        SELECT DISTINCT O.CD_SEQ_OBJETIVO, O.DS_OBJETIVO,
			                        CASE WHEN (SELECT COUNT(CD_SEQ_RECURSO) FROM TB_RECURSO WHERE CD_ACAO = A.CD_SEQ_ACAO) IS NOT NULL THEN
						                        (SELECT COUNT(CD_SEQ_RECURSO) FROM TB_RECURSO WHERE CD_ACAO = A.CD_SEQ_ACAO) ELSE 0 END AS RECURSO_PREVISTO,
			                        CASE WHEN (SELECT COUNT(CD_SEQ_CONTROLE_ACAO) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) IS NOT NULL THEN
						                        (SELECT COUNT(CD_SEQ_CONTROLE_ACAO) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) ELSE 0 END AS REPORTE_RECURSO_UTILIZADO,
			                        E.NU_ANO_EXERCICIO , 1 CD_TIPO, C.DS_UNIDADE, I.CD_SEQ_INICIATIVA
                            FROM TB_OBJETIVO O
	                            INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
	                            INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PE
	                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                            INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
		                        INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
		                        INNER JOIN TB_CORPORATIVO_UNIDADE C ON M.CD_UNIDADE = C.CD_UNIDADE
		                        INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
	                        WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 1
                        UNION
                        SELECT DISTINCT O.CD_SEQ_OBJETIVO, O.DS_OBJETIVO,
			                        CASE WHEN (SELECT COUNT(CD_SEQ_RECURSO) FROM TB_RECURSO WHERE CD_ACAO = A.CD_SEQ_ACAO) IS NOT NULL THEN
						                        (SELECT COUNT(CD_SEQ_RECURSO) FROM TB_RECURSO WHERE CD_ACAO = A.CD_SEQ_ACAO) ELSE 0 END AS RECURSO_PREVISTO,
			                        CASE WHEN (SELECT COUNT(CD_SEQ_CONTROLE_ACAO) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) IS NOT NULL THEN
						                        (SELECT COUNT(CD_SEQ_CONTROLE_ACAO) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) ELSE 0 END AS REPORTE_RECURSO_UTILIZADO,
			                        E.NU_ANO_EXERCICIO , 1 CD_TIPO, C.DS_UNIDADE, I.CD_SEQ_INICIATIVA
                            FROM TB_OBJETIVO O
	                            INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
	                            INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PPA
	                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                            INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
		                        INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
		                        INNER JOIN TB_CORPORATIVO_UNIDADE C ON M.CD_UNIDADE = C.CD_UNIDADE
		                        INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
	                        WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 2
                        UNION
                        SELECT DISTINCT I.CD_SEQ_INICIATIVA CD_SEQ_OBJETIVO, I.DS_INICIATIVA DS_OBJETIVO,
			                        CASE WHEN (SELECT COUNT(CD_SEQ_RECURSO) FROM TB_RECURSO WHERE CD_ACAO = A.CD_SEQ_ACAO) IS NOT NULL THEN
						                        (SELECT COUNT(CD_SEQ_RECURSO) FROM TB_RECURSO WHERE CD_ACAO = A.CD_SEQ_ACAO) ELSE 0 END AS RECURSO_PREVISTO,
			                        CASE WHEN (SELECT COUNT(CD_SEQ_CONTROLE_ACAO) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) IS NOT NULL THEN
						                        (SELECT COUNT(CD_SEQ_CONTROLE_ACAO) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) ELSE 0 END AS REPORTE_RECURSO_UTILIZADO,
			                        E.NU_ANO_EXERCICIO , 2 CD_TIPO, C.DS_UNIDADE, I.CD_SEQ_INICIATIVA
	                        FROM TB_INICIATIVA I
		                        INNER JOIN TB_META M ON I.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_MISSAO
		                        INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                        INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
		                        INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
		                        INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
		                        INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
		                        INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
	                        WHERE  I.CD_TIPO_INICIATIVA = 3) RECURSO {0}";

            string sqlWhere = "WHERE 1=1";

            if (anoExercicio > 0)
                sqlWhere += " AND NU_ANO_EXERCICIO = " + anoExercicio;

            if (codObjetivo > 0)
                sqlWhere += " AND CD_SEQ_OBJETIVO = " + codObjetivo;

            if (codTipo > 0)
                sqlWhere += " AND CD_TIPO = " + codTipo;

            var result = Session.CreateSQLQuery(string.Format(sql, sqlWhere)).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var lista = new List<VOObjetivoEstrategicoTotalizadorDetalhadado>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;

                    var objetivo = new VOObjetivoEstrategicoTotalizadorDetalhadado()
                    {
                        DescricaoUnidade = (string)asHash["DescricaoUnidade"],
                        DescricaoObjetivo = (string)asHash["DescricaoObjetivo"],
                        CodSeqObjetivo = (int)asHash["CodSeqObjetivo"],
                        CodSeqIniciativa = (int)asHash["CodSeqIniciativa"],
                        RecursoPrevisto = (int)asHash["RecursoPrevisto"],
                        ReporteRecursoUtilizado = (int)asHash["ReporteRecursoUtilizado"]
                    };

                    lista.Add(objetivo);
                }
            }

            return lista;
        }

        public IList<VORelatorioObjetivoEstrategico> ListarObjetivoEstrategico(int anoExercicio)
        {
            var sql = @"SELECT	  CD_SEQ_META CodSeqMeta 
		                                , CD_SEQ_INDICADOR CodSeqIndicador
		                                , CD_SEQ_ACAO CodSeqAcao
		                                , DS_UNIDADE DescricaoUnicade
		                                , PERCENTUAL_EXECUCAO PercentualExecucao
		                                , RESULTADO_INDICADOR ResultadoIndicador
		                                , DS_OBJETIVO DescricaoTipo
                                FROM (
                                SELECT * FROM (
                                SELECT DISTINCT I.CD_SEQ_INICIATIVA, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, 0 PERCENTUAL_EXECUCAO , 0 RESULTADO_INDICADOR, O.DS_OBJETIVO
                                FROM TB_OBJETIVO O
	                                INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
	                                INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PE
	                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
                                WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 1
                                UNION
                                SELECT DISTINCT I.CD_SEQ_INICIATIVA, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, 0 PERCENTUAL_EXECUCAO , 0 RESULTADO_INDICADOR, O.DS_OBJETIVO
                                FROM TB_OBJETIVO O
	                                INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
	                                INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PPA
	                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
                                WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 2
                                UNION
                                SELECT DISTINCT I.CD_SEQ_INICIATIVA, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, 0 PERCENTUAL_EXECUCAO , 0 RESULTADO_INDICADOR, I.DS_INICIATIVA DS_OBJETIVO
                                FROM TB_INICIATIVA I
	                                INNER JOIN TB_META M ON I.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_MISSAO
	                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
	                                INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
                                WHERE  I.CD_TIPO_INICIATIVA = 3) METAS
                                UNION
                                SELECT * FROM (
                                SELECT DISTINCT I.CD_SEQ_INICIATIVA, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, II.CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, 0 PERCENTUAL_EXECUCAO , 
	                                CASE WHEN II.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR)
			                                WHEN II.CD_TIPO_RESULTADO = 2 THEN 
	                                CASE WHEN (SELECT COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) > 0 THEN 
				                                (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) ELSE 0 END END AS RESULTADO_INDICADOR, O.DS_OBJETIVO
                                FROM TB_OBJETIVO O
	                                INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
	                                INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PE
	                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
	                                INNER JOIN TB_INDICADOR II ON M.CD_SEQ_META = II.CD_META AND (CD_SEQ_STATUS_INDICADOR IS NULL OR CD_SEQ_STATUS_INDICADOR = 1)
                                WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 1
                                UNION
                                SELECT DISTINCT I.CD_SEQ_INICIATIVA, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, II.CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, 0 PERCENTUAL_EXECUCAO , 
	                                CASE WHEN II.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR)
			                                WHEN II.CD_TIPO_RESULTADO = 2 THEN 
	                                CASE WHEN (SELECT COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) > 0 THEN 
				                                (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) ELSE 0 END END AS RESULTADO_INDICADOR, O.DS_OBJETIVO
                                FROM TB_OBJETIVO O
	                                INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
	                                INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PPA
	                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
	                                INNER JOIN TB_INDICADOR II ON M.CD_SEQ_META = II.CD_META AND (CD_SEQ_STATUS_INDICADOR IS NULL OR CD_SEQ_STATUS_INDICADOR = 1)
                                WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 2
                                UNION
                                SELECT DISTINCT I.CD_SEQ_INICIATIVA, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, 0 CD_SEQ_ACAO, C.DS_UNIDADE, 0 PERCENTUAL_EXECUCAO , 
	                                CASE WHEN II.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR)
			                                WHEN II.CD_TIPO_RESULTADO = 2 THEN 
	                                CASE WHEN (SELECT COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) > 0 THEN 
				                                (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = II.CD_SEQ_INDICADOR) ELSE 0 END END AS RESULTADO_INDICADOR, I.DS_INICIATIVA DS_OBJETIVO
                                FROM TB_INICIATIVA I
	                                INNER JOIN TB_META M ON I.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_MISSAO
	                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
	                                INNER JOIN TB_INDICADOR II ON M.CD_SEQ_META = II.CD_META AND (CD_SEQ_STATUS_INDICADOR IS NULL OR CD_SEQ_STATUS_INDICADOR = 1)
                                WHERE  I.CD_TIPO_INICIATIVA = 3) INDICADOR
                                UNION
                                SELECT * FROM (
                                SELECT DISTINCT I.CD_SEQ_INICIATIVA, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, A.CD_SEQ_ACAO, C.DS_UNIDADE, 
	                                (SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO PERCENTUAL_EXECUCAO , 0 RESULTADO_INDICADOR, O.DS_OBJETIVO
                                FROM TB_OBJETIVO O
	                                INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
	                                INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PE
	                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
	                                INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
                                WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 1
                                UNION
                                SELECT DISTINCT I.CD_SEQ_INICIATIVA, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, A.CD_SEQ_ACAO, C.DS_UNIDADE, 
	                                (SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO PERCENTUAL_EXECUCAO , 0 RESULTADO_INDICADOR, O.DS_OBJETIVO
                                FROM TB_OBJETIVO O
	                                INNER JOIN RL_INICIATIVA_OBJETIVO RI ON RI.CD_SEQ_OBJETIVO = O.CD_SEQ_OBJETIVO
	                                INNER JOIN TB_META M ON RI.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_PPA
	                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                                INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                INNER JOIN TB_INICIATIVA I ON RI.CD_SEQ_INICIATIVA = I.CD_SEQ_INICIATIVA 
	                                INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
                                WHERE  O.CD_TIPO_OBJETIVO = 4 AND I.CD_TIPO_INICIATIVA = 2
                                UNION
                                SELECT DISTINCT I.CD_SEQ_INICIATIVA, E.NU_ANO_EXERCICIO, M.CD_SEQ_META, 0 CD_SEQ_INDICADOR, A.CD_SEQ_ACAO, C.DS_UNIDADE,	
	                                (SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO PERCENTUAL_EXECUCAO, 0 RESULTADO_INDICADOR, I.DS_INICIATIVA DS_OBJETIVO
                                FROM TB_INICIATIVA I
	                                INNER JOIN TB_META M ON I.CD_SEQ_INICIATIVA = M.CD_INICIATIVA_MISSAO
	                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META
	                                INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
	                                INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                INNER JOIN TB_CORPORATIVO_UNIDADE C ON C.CD_UNIDADE = M.CD_UNIDADE
	                                INNER JOIN TB_EXERCICIO E ON E.CD_SEQ_EXERCICIO = M.CD_EXERCICIO
	                                INNER JOIN TB_ACAO A ON M.CD_SEQ_META = A.CD_META AND (CD_SEQ_STATUS_ACAO IS NULL OR CD_SEQ_STATUS_ACAO = 1)
                                WHERE  I.CD_TIPO_INICIATIVA = 3) ACAO) OBJETIVO {0} ORDER BY DS_UNIDADE";

            string sqlWhere = " WHERE 1 = 1 ";

            if (anoExercicio > 0)
                sqlWhere += " AND NU_ANO_EXERCICIO = " + anoExercicio;

            var result = Session.CreateSQLQuery(string.Format(sql, sqlWhere)).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var listaObjetivoEstrategico = new List<VORelatorioObjetivoEstrategico>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;

                    var objetivoEstrategico = new VORelatorioObjetivoEstrategico()
                    {
                        CodSeqMeta = asHash["CodSeqMeta"] != null ? int.Parse(asHash["CodSeqMeta"].ToString()) : int.Parse("0"),
                        CodSeqIndicador = asHash["CodSeqIndicador"] != null ? int.Parse(asHash["CodSeqIndicador"].ToString()) : int.Parse("0"),
                        CodSeqAcao = asHash["CodSeqAcao"] != null ? int.Parse(asHash["CodSeqAcao"].ToString()) : int.Parse("0"),
                        DescricaoUnicade = asHash["DescricaoUnicade"].ToString(),
                        PercentualExecucao = asHash["PercentualExecucao"] != null ? decimal.Parse(asHash["PercentualExecucao"].ToString()) : decimal.Parse("0"),
                        ResultadoIndicador = asHash["ResultadoIndicador"] != null ? decimal.Parse(asHash["ResultadoIndicador"].ToString()) : decimal.Parse("0"),
                        DescricaoTipo = asHash["DescricaoTipo"].ToString()
                    };

                    listaObjetivoEstrategico.Add(objetivoEstrategico);
                }
            }

            return listaObjetivoEstrategico;
        }

        public IList<VOResultadoIndicadorAcao> ListaResultadoIndicador(int codigoMeta)
        {
            var sql = @"SELECT * FROM   
		                (SELECT CI.VL_INDICADOR AS UltimoValor, 
		                (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM   TB_CONTROLE_INDICADOR WHERE  CD_INDICADOR = I.CD_SEQ_INDICADOR) AS ValorTotalInformado , 
			                CASE WHEN I.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM   TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = I.CD_SEQ_INDICADOR) WHEN I.CD_TIPO_RESULTADO = 2 THEN 
			                CASE WHEN (SELECT COUNT(1) FROM   TB_CONTROLE_INDICADOR WHERE  CD_INDICADOR = I.CD_SEQ_INDICADOR) > 0 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = I.CD_SEQ_INDICADOR) 
				                ELSE 0 END END AS ResultadoIndicador, 
                               V.CD_SEQ_TIPO_VALOR AS CodSeqTipoValor, 
                               M.CD_SEQ_META AS CodMeta,
                               I.NU_VALORALVO as ValorAlvo
                        FROM   TB_INDICADOR I 
                               INNER JOIN DBO.TB_META M ON I.CD_META = M.CD_SEQ_META 
                               INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM   TB_SITUACAO_META GROUP  BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = M.CD_SEQ_META 
                               INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META AND S.CD_STATUS_META != 4 
                               LEFT JOIN TB_TIPO_VALOR V ON I.CD_TIPO_VALOR = V.CD_SEQ_TIPO_VALOR 
                               INNER JOIN TB_TIPO_RESULTADO R ON I.CD_TIPO_RESULTADO = R.CD_SEQ_TIPO_RESULTADO 
                               INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO 
                               INNER JOIN TB_CORPORATIVO_UNIDADE UC ON M.CD_UNIDADE = UC.CD_UNIDADE 
                               LEFT JOIN (SELECT CD_INDICADOR, DT_INDICADOR, CD_SEQ_CONTROLE_INDICADOR AS CODIGO, ROW_NUMBER() 
				                     OVER(PARTITION BY CD_INDICADOR ORDER BY DT_INDICADOR DESC, DT_CADASTRO DESC) AS RANK_ID FROM TB_CONTROLE_INDICADOR) CIATUAL ON CIATUAL.CD_INDICADOR = I.CD_SEQ_INDICADOR AND CIATUAL.RANK_ID = 1 
                               LEFT JOIN DBO.TB_CONTROLE_INDICADOR CI  ON CIATUAL.CODIGO = CI.CD_SEQ_CONTROLE_INDICADOR 
                        WHERE  S.CD_STATUS_META = 3 
                               AND E.CD_FASE <> 1) AS T {0}";

            string sqlWhere = @" WHERE 1 = 1 ";

            if (codigoMeta > 0)
                sqlWhere += " AND CodMeta = " + codigoMeta;

            var result = Session.CreateSQLQuery(string.Format(sql, sqlWhere)).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var lista = new List<VOResultadoIndicadorAcao>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;

                    var indicador = new VOResultadoIndicadorAcao()
                    {
                        ResultadoIndicador = asHash["ResultadoIndicador"] != null ? decimal.Parse(asHash["ResultadoIndicador"].ToString()) : decimal.Parse("0"),
                        ValorAlvo = asHash["ValorAlvo"] != null ? decimal.Parse(asHash["ValorAlvo"].ToString()) : decimal.Parse("0"),
                        CodSeqTipoValor = asHash["CodSeqTipoValor"] != null ? int.Parse(asHash["CodSeqTipoValor"].ToString()) : int.Parse("0")
                    };

                    lista.Add(indicador);
                }
            }

            return lista;
        }

        public IList<VOResultadoIndicadorAcao> ListaResultadoAtividade(int codigoMeta)
        {
            var sql = @"select ca.VL_PERCENTUAL_REPORTE as PercentualExecucao
                            FROM TB_ACAO A
                            INNER JOIN DBO.TB_META M ON A.CD_META=M.CD_SEQ_META
                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL 
                                ON STATUSATUAL.CD_META = M.CD_SEQ_META
                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
                            INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                        INNER JOIN TB_CORPORATIVO_UNIDADE UC ON M.CD_UNIDADE = UC.CD_UNIDADE
                            LEFT JOIN (SELECT CD_ACAO, MAX(CD_SEQ_CONTROLE_ACAO) AS CODIGO FROM TB_CONTROLE_ACAO GROUP BY CD_ACAO) CAATUAL ON CAATUAL.CD_ACAO = A.CD_SEQ_ACAO
                            LEFT JOIN DBO.TB_CONTROLE_ACAO CA ON CAATUAL.CODIGO = CA.CD_SEQ_CONTROLE_ACAO
                            WHERE S.CD_STATUS_META = 3
                            AND E.CD_FASE <> 1 {0}";

            string sqlWhere = "";

            if (codigoMeta > 0)
                sqlWhere += " AND M.CD_SEQ_META = " + codigoMeta;

            var result = Session.CreateSQLQuery(string.Format(sql, sqlWhere)).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var lista = new List<VOResultadoIndicadorAcao>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;

                    var indicador = new VOResultadoIndicadorAcao()
                    {
                        PercentualExecucao = asHash["PercentualExecucao"] != null ? int.Parse(asHash["PercentualExecucao"].ToString()) : int.Parse("0")
                    };

                    lista.Add(indicador);
                }
            }

            return lista;
        }
    }
}