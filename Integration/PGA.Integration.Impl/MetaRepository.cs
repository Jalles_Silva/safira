﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;
using PGA.Integration.Spec.ValueObjects;
using PGA.Common;

namespace PGA.Integration.Impl
{
    public class MetaRepository : RepositoryBase<Meta>, IMetaRepository<Meta>
    {
        public PageMessage<VOMeta> ListarMetasPorUnidadeMeta(string Unidade, int AnoExercicio, int MetaInterna, int StatusMeta, int startIndex, int pageSize)
        {
            string sql = @"select {0} 
                from dbo.TB_META m 
                inner join dbo.TB_CORPORATIVO_UNIDADE u on m.CD_UNIDADE = u.CD_UNIDADE
                inner join (select CD_META, max(CD_SEQ_SITUACAO_META) as codigo from TB_SITUACAO_META group by CD_META) statusAtual 
                    on statusAtual.CD_META = m.CD_SEQ_META
                inner join TB_SITUACAO_META s on statusAtual.codigo = s.CD_SEQ_SITUACAO_META
                inner join TB_STATUS_META st on st.CD_SEQ_STATUS_META = s.CD_STATUS_META
                inner join TB_EXERCICIO E on m.CD_EXERCICIO = e.CD_SEQ_EXERCICIO
                inner join TB_TIPO_META T on m.CD_TIPO_META = T.CD_SEQ_TIPO_META
                {1} {2}";

            string sqlWhere = "WHERE 1 = 1 ";

            string retorno = @" m.CD_SEQ_META as CodSeqMeta, 
                                m.DS_META as DescricaoMeta,
                                st.CD_SEQ_STATUS_META as CodigoStatusMeta,
                                st.DS_STATUS as DescricaoStatusMeta,
                                CASE WHEN u.ST_ATIVO = 0 THEN
									u.DS_UNIDADE + ' - Inativa'
								ELSE
									u.DS_UNIDADE
								END AS DescricaoUnidade,
                                e.NU_ANO_EXERCICIO as AnoExercicio, 
                                u.CD_UNIDADE as CodigoUnidade,
                                e.CD_FASE as CodigoFaseExercicio,
                                m.ST_INTERNA as StatusMetaInterna,
                                m.CD_INICIATIVA_PE as CodigoSeqIniciativaPE,
                                t.DS_TIPO_META as DescricaoTipoMeta,
                                m.DS_RESULTADO_ESPERADO as DescricaoResultadoEsperado";

            if (!String.IsNullOrEmpty(Unidade) && Unidade != "Todas")
                sqlWhere += " and u.DS_UNIDADE = '" + (Unidade.Contains("Inativa") ? Unidade.Replace(" - Inativa", "") : Unidade) + "'";

            if (AnoExercicio > 0)
                sqlWhere += " and e.NU_ANO_EXERCICIO = " + AnoExercicio;

            if (MetaInterna > 0)
            {
                if (MetaInterna == 1)
                    sqlWhere += " AND m.ST_INTERNA = 0";
                else
                    sqlWhere += " AND m.ST_INTERNA = 1";
            }

            if (StatusMeta > 0)
            {
                sqlWhere += "AND s.CD_STATUS_META = " + StatusMeta;
            }


            NHibernate.ISQLQuery sqlQuery = Session.CreateSQLQuery(String.Format(sql, retorno, sqlWhere, " order by u.DS_UNIDADE asc, m.DS_META asc "));
            NHibernate.ISQLQuery sqlQueryCount = Session.CreateSQLQuery(String.Format(sql, "count(1)", sqlWhere, " "));

            PageMessage pageMessage = new PageMessage();
            pageMessage.StartIndex = startIndex;
            pageMessage.PageSize = pageSize;

            return Page<VOMeta>(sqlQuery, sqlQueryCount, pageMessage);
        }

        public PageMessage<VOMeta> ListarMetasPorUnidade(int codigoUnidade, int AnoExercicio, int startIndex, int pageSize)
        {
            string sql = @"select {0} 
                from dbo.TB_META m 
                inner join dbo.TB_CORPORATIVO_UNIDADE u on m.CD_UNIDADE = u.CD_UNIDADE
                inner join (select CD_META, max(CD_SEQ_SITUACAO_META) as codigo from TB_SITUACAO_META group by CD_META) statusAtual 
                    on statusAtual.CD_META = m.CD_SEQ_META
                inner join TB_SITUACAO_META s on statusAtual.codigo = s.CD_SEQ_SITUACAO_META
                inner join TB_STATUS_META st on st.CD_SEQ_STATUS_META = s.CD_STATUS_META
                inner join TB_EXERCICIO E on m.CD_EXERCICIO = e.CD_SEQ_EXERCICIO
                {1} {2}";

            string sqlWhere = "WHERE 1 = 1 ";

            string retorno = @" m.CD_SEQ_META as CodSeqMeta, 
                                m.DS_META as DescricaoMeta,
                                st.CD_SEQ_STATUS_META as CodigoStatusMeta,
                                st.DS_STATUS as DescricaoStatusMeta,
                                u.DS_UNIDADE as DescricaoUnidade,
                                e.NU_ANO_EXERCICIO as AnoExercicio, 
                                u.CD_UNIDADE as CodigoUnidade,
                                e.CD_FASE as CodigoFaseExercicio ";

            if (codigoUnidade > 0)
                sqlWhere += " and m.CD_UNIDADE = " + codigoUnidade;

            if (AnoExercicio > 0)
                sqlWhere += " and e.NU_ANO_EXERCICIO = " + AnoExercicio;


            NHibernate.ISQLQuery sqlQuery = Session.CreateSQLQuery(String.Format(sql, retorno, sqlWhere, " order by u.DS_UNIDADE asc, m.DS_META asc "));
            NHibernate.ISQLQuery sqlQueryCount = Session.CreateSQLQuery(String.Format(sql, "count(1)", sqlWhere, " "));

            PageMessage pageMessage = new PageMessage();
            pageMessage.StartIndex = startIndex;
            pageMessage.PageSize = pageSize;

            return Page<VOMeta>(sqlQuery, sqlQueryCount, pageMessage);
        }

        public IList<Meta> ListarMetaSimplificadaPorUnidadeEAno(int codigoUnidade, short ano)
        {
            var criteria = DetachedCriteria.For<Meta>()
                .CreateAlias("exercicio", "e");

            if (codigoUnidade > 0)
                criteria.Add(Expression.Eq("corporativoUnidade.codigoUnidade", codigoUnidade));

            if (ano > 0)
                criteria.Add(Expression.Eq("e.ano", ano));

            return List<Meta>(criteria);
        }

        public PageMessage<VOMetaRelatorio> ListaGlobalMeta(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, bool metaAdministrativa,
            bool metaFiscalizacao, bool metaRegulacao, string unidade, int AnoExercicio, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize)
        {
            string sqlWhereInterno = string.Empty;

            if (MetaInterna == 1)
                sqlWhereInterno += " AND META.ST_INTERNA = 0";
            else if (MetaInterna == 2)
                sqlWhereInterno += " AND META.ST_INTERNA = 1";

            if (TipoInstrumento > 0 && TipoInstrumento != 5)
            {
                sqlWhereInterno += " AND TI.COD_SEQ_TIPO_INSTRUMENTO =" + TipoInstrumento;
            }
            else if (TipoInstrumento == 5)
            {
                sqlWhereInterno += " AND META.ST_INICIATIVA_ESTRATEGICA = 1 ";
            }

            string sql = @"SELECT AREA AS DESCRICAOAREA,
	                            (SELECT COUNT(DISTINCT META.CD_SEQ_META) 
	                            FROM TB_META META
	                            INNER JOIN TB_SITUACAO_META SITUACAO    ON SITUACAO.CD_META = META.CD_SEQ_META
	                            INNER JOIN TB_STATUS_META STA           ON SITUACAO.CD_STATUS_META = STA.CD_SEQ_STATUS_META
                                INNER JOIN TB_EXERCICIO EXE             ON META.CD_EXERCICIO = EXE.CD_SEQ_EXERCICIO
                                LEFT JOIN TB_INSTRUMENTO INST ON INST.CD_SEQ_INSTRUMENTO = META.CD_AGENDA_REGULATORIA
							    OR INST.CD_SEQ_INSTRUMENTO = META.CD_DESBUROCRATIZACAO OR INST.CD_SEQ_INSTRUMENTO = META.CD_GESTAO_RISCO OR INST.CD_SEQ_INSTRUMENTO = META.CD_INTEGRIDADE		
							    LEFT JOIN TB_TIPO_INSTRUMENTO TI ON TI.COD_SEQ_TIPO_INSTRUMENTO = INST.CD_TIPO_INSTRUMENTO
	                            WHERE STA.CD_SEQ_STATUS_META = 3
                                AND EXE.NU_ANO_EXERCICIO = {7}" + sqlWhereInterno +
                                @"AND META.CD_UNIDADE = TB_META_GLOBAL.CD_UNIDADE_METAS ) AS QDTMETASCADASTRADAS,
	                            SUM(ALINHAMENTO_PE) AS QTDMETASALINHADASPE, 
	                            SUM(ALINHAMENTO_PPA) AS QTDMETASALINHADASPPA, 
	                            SUM(ALINHAMENTO_MISSAO_INSTITUCIONAL) AS QTDMETASALINHADASMI,
	                            SUM(META_ADMINISTRATIVA) AS QTDMETASADMINISTRATIVAS,
	                            SUM(META_FISCALIZACAO) AS QTDMETASFISCALIZACOES,
	                            SUM(META_REGULACAO) AS QTDMETASREGULACOES,
                                SUM(META_ST_INTERNA) AS QTDMETASINTERNAS,
                                '{1}' AS VERALINHADASPE,
                                '{2}' AS VERALINHADASPPA,
                                '{3}' AS VERALINHADASMI,
                                '{4}' AS VERADMINISTRATIVAS,
                                '{5}' AS VERFISCALIZACOES,
                                '{6}' AS VERREGULACOES,
                                 {7} AS ANOEXCERCICIO,
                                 NULL AS TIPOMETA,
                                 NULL AS TIPOALINHAMENTO,
                                 NULL AS DESCRICAOMETA,
                                 NULL AS CODIGOUNIDADE,
                                NULL AS MetaInterna
                        FROM (SELECT DISTINCT
	                        CASE WHEN UN.ST_ATIVO = 0 THEN
								UN.DS_UNIDADE + ' - Inativa'
							ELSE
								UN.DS_UNIDADE
							END AS AREA, 
                            UN.CD_UNIDADE as CD_UNIDADE_METAS,
	                        CASE WHEN M.CD_INICIATIVA_PE IS NOT NULL THEN 1 ELSE 0 END ALINHAMENTO_PE ,
	                        CASE WHEN M.CD_INICIATIVA_PPA IS NOT NULL THEN 1 ELSE 0 END ALINHAMENTO_PPA,
	                        CASE WHEN M.CD_INICIATIVA_MISSAO IS NOT NULL THEN 1 ELSE 0 END ALINHAMENTO_MISSAO_INSTITUCIONAL,
	                        CASE WHEN TM.DS_TIPO_META = 'ADMINISTRATIVA' THEN 1 ELSE 0 END META_ADMINISTRATIVA,
	                        CASE WHEN TM.DS_TIPO_META = 'DE FISCALIZAÇÃO' THEN 1 ELSE 0 END META_FISCALIZACAO,
	                        CASE WHEN TM.DS_TIPO_META = 'DE REGULAÇÃO' THEN 1 ELSE 0 END META_REGULACAO,
                            CASE WHEN M.ST_INTERNA = 1 THEN 1 ELSE 0 END META_ST_INTERNA
                        FROM TB_META M
	                        INNER JOIN TB_CORPORATIVO_UNIDADE UN ON M.CD_UNIDADE = UN.CD_UNIDADE
	                        INNER JOIN TB_TIPO_META TM ON M.CD_TIPO_META = TM.CD_SEQ_TIPO_META
                            INNER JOIN TB_EXERCICIO EX ON M.CD_EXERCICIO = EX.CD_SEQ_EXERCICIO
                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL 
								ON STATUSATUAL.CD_META = M.CD_SEQ_META
							INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
							INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3
                            LEFT JOIN TB_INSTRUMENTO INST ON INST.CD_SEQ_INSTRUMENTO = M.CD_AGENDA_REGULATORIA
							OR INST.CD_SEQ_INSTRUMENTO = M.CD_DESBUROCRATIZACAO OR INST.CD_SEQ_INSTRUMENTO = M.CD_GESTAO_RISCO OR INST.CD_SEQ_INSTRUMENTO = M.CD_INTEGRIDADE		
							LEFT JOIN TB_TIPO_INSTRUMENTO TI ON TI.COD_SEQ_TIPO_INSTRUMENTO = INST.CD_TIPO_INSTRUMENTO
                            {0}) AS TB_META_GLOBAL 
                        GROUP BY AREA, CD_UNIDADE_METAS";

            string sqlWhere = "WHERE 1 = 1 ";

            if (!String.IsNullOrEmpty(unidade) && unidade != "Todas")
                sqlWhere += " AND UN.DS_UNIDADE = '" + (unidade.Contains("Inativa") ? unidade.Replace(" - Inativa", "") : unidade) + "'";

            if (AnoExercicio > 0)
                sqlWhere += " AND EX.NU_ANO_EXERCICIO = " + AnoExercicio;

            if (codigoTipoMeta > 0)
                sqlWhere += " AND TM.CD_SEQ_TIPO_META = " + codigoTipoMeta;

            if (alinhadasPE == true && alinhadasPPA == false && alinhadasMI == false)
                sqlWhere += " AND M.CD_INICIATIVA_PE IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == true && alinhadasMI == false)
                sqlWhere += " AND M.CD_INICIATIVA_PPA IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == false && alinhadasMI == true)
                sqlWhere += " AND M.CD_INICIATIVA_MISSAO IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == true && alinhadasMI == true)
            {
                sqlWhere += " AND (M.CD_INICIATIVA_PPA IS NOT NULL";
                sqlWhere += " OR M.CD_INICIATIVA_MISSAO IS NOT NULL)";
            }
            else if (alinhadasPE == true && alinhadasPPA == true && alinhadasMI == false)
            {
                sqlWhere += " AND (M.CD_INICIATIVA_PE IS NOT NULL";
                sqlWhere += " OR M.CD_INICIATIVA_PPA IS NOT NULL)";
            }
            else if (alinhadasPE == true && alinhadasPPA == false && alinhadasMI == true)
            {
                sqlWhere += " AND (M.CD_INICIATIVA_PE IS NOT NULL";
                sqlWhere += " OR M.CD_INICIATIVA_MISSAO IS NOT NULL)";
            }

            if (MetaInterna > 0)
            {
                if (MetaInterna == 1)
                    sqlWhere += " AND M.ST_INTERNA = 0";
                else
                    sqlWhere += " AND M.ST_INTERNA = 1";
            }
            if (TipoInstrumento > 0 && TipoInstrumento != 5)
            {
                sqlWhere += " AND TI.COD_SEQ_TIPO_INSTRUMENTO =" + TipoInstrumento;
            }
            else if (TipoInstrumento == 5)
            {
                sqlWhere += " AND M.ST_INICIATIVA_ESTRATEGICA = 1 ";
            }

            NHibernate.ISQLQuery sqlQuery = Session.CreateSQLQuery(String.Format(sql, sqlWhere,
                 alinhadasPE, alinhadasPPA, alinhadasMI, metaAdministrativa, metaFiscalizacao, metaRegulacao, AnoExercicio, MetaInterna));
            NHibernate.ISQLQuery sqlQueryCount = Session.CreateSQLQuery("SELECT COUNT(1) FROM ( " + String.Format(sql, sqlWhere, alinhadasPE
                , alinhadasPPA, alinhadasMI, metaAdministrativa, metaFiscalizacao, metaRegulacao, AnoExercicio, MetaInterna) + " ) AS TOTAL_METAL");

            PageMessage pageMessage = new PageMessage();
            pageMessage.StartIndex = startIndex;
            pageMessage.PageSize = pageSize;

            return Page<VOMetaRelatorio>(sqlQuery, sqlQueryCount, pageMessage);
        }

        public PageMessage<VOMetaRelatorio> ListaDetalheMeta(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI,
            int codigoTipoMeta, string Unidade, int AnoExercicio, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize)
        {
            string sql = @"SELECT DISTINCT {0}
                                    FROM TB_META M
                                INNER JOIN TB_CORPORATIVO_UNIDADE UN ON M.CD_UNIDADE = UN.CD_UNIDADE
                                INNER JOIN TB_TIPO_META TM ON M.CD_TIPO_META = TM.CD_SEQ_TIPO_META
                                INNER JOIN TB_EXERCICIO EX ON M.CD_EXERCICIO = EX.CD_SEQ_EXERCICIO
                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL 
								    ON STATUSATUAL.CD_META = M.CD_SEQ_META
							    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
							    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
                                LEFT JOIN TB_INSTRUMENTO INST ON INST.CD_SEQ_INSTRUMENTO = M.CD_AGENDA_REGULATORIA
							    OR INST.CD_SEQ_INSTRUMENTO = M.CD_DESBUROCRATIZACAO OR INST.CD_SEQ_INSTRUMENTO = M.CD_GESTAO_RISCO OR INST.CD_SEQ_INSTRUMENTO = M.CD_INTEGRIDADE		
							    LEFT JOIN TB_TIPO_INSTRUMENTO TI ON TI.COD_SEQ_TIPO_INSTRUMENTO = INST.CD_TIPO_INSTRUMENTO
                                {1}";

            string retorno = @" M.CD_UNIDADE AS CODIGOUNIDADE,
                            CASE WHEN UN.ST_ATIVO = 0 THEN
								UN.DS_UNIDADE + ' - Inativa'
							ELSE
								UN.DS_UNIDADE
							END AS DESCRICAOAREA, 
                            TM.DS_TIPO_META AS TIPOMETA,
                            CASE WHEN M.CD_INICIATIVA_PE IS NOT NULL AND M.CD_INICIATIVA_PPA IS NOT NULL THEN 'PE/PPA'
                            WHEN M.CD_INICIATIVA_PE IS NOT NULL AND M.CD_INICIATIVA_PPA IS NULL THEN 'PE'
                            WHEN M.CD_INICIATIVA_PE IS NULL AND M.CD_INICIATIVA_PPA IS NOT NULL THEN 'PPA'
                            ELSE 'MISSÃO INSTITUCIONAL' END TIPOALINHAMENTO,
                            M.DS_META AS DESCRICAOMETA,
                            {0} AS ANOEXCERCICIO, 
                            NULL AS QDTMETASCADASTRADAS, 
                            NULL AS QTDMETASALINHADASPE, 
                            NULL AS QTDMETASALINHADASPPA, 
                            NULL AS QTDMETASALINHADASMI, 
                            NULL AS QTDMETASADMINISTRATIVAS,
                            NULL AS QTDMETASFISCALIZACOES, 
                            NULL AS QTDMETASREGULACOES, 
                            NULL AS VERALINHADASPE, 
                            NULL AS VERALINHADASPPA, 
                            NULL AS VERALINHADASMI, 
                            NULL AS VERADMINISTRATIVAS, 
                            NULL AS VERFISCALIZACOES, 
                            NULL AS VERREGULACOES,
                            NULL AS QTDMETASINTERNAS,
							CASE WHEN M.ST_INTERNA = 1  THEN 'SIM' ELSE 'NÃO' END  AS MetaInterna ";

            string sqlWhere = "WHERE 1 = 1 ";

            if (!String.IsNullOrEmpty(Unidade) && Unidade != "Todas")
                sqlWhere += " AND UN.DS_UNIDADE = '" + (Unidade.Contains("Inativa") ? Unidade.Replace(" - Inativa", "") : Unidade) + "'";

            if (AnoExercicio > 0)
                sqlWhere += " AND EX.NU_ANO_EXERCICIO = " + AnoExercicio;

            if (codigoTipoMeta > 0)
                sqlWhere += " AND TM.CD_SEQ_TIPO_META = " + codigoTipoMeta;

            if (alinhadasPE == true && alinhadasPPA == false && alinhadasMI == false)
                sqlWhere += " AND M.CD_INICIATIVA_PE IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == true && alinhadasMI == false)
                sqlWhere += " AND M.CD_INICIATIVA_PPA IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == false && alinhadasMI == true)
                sqlWhere += " AND M.CD_INICIATIVA_MISSAO IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == true && alinhadasMI == true)
            {
                sqlWhere += " AND (M.CD_INICIATIVA_PPA IS NOT NULL";
                sqlWhere += " OR M.CD_INICIATIVA_MISSAO IS NOT NULL)";
            }
            else if (alinhadasPE == true && alinhadasPPA == true && alinhadasMI == false)
            {
                sqlWhere += " AND (M.CD_INICIATIVA_PE IS NOT NULL";
                sqlWhere += " OR M.CD_INICIATIVA_PPA IS NOT NULL)";
            }
            else if (alinhadasPE == true && alinhadasPPA == false && alinhadasMI == true)
            {
                sqlWhere += " AND (M.CD_INICIATIVA_PE IS NOT NULL";
                sqlWhere += " OR M.CD_INICIATIVA_MISSAO IS NOT NULL)";
            }

            if (MetaInterna > 0)
            {
                if (MetaInterna == 1)
                    sqlWhere += " AND M.ST_INTERNA = 0";
                else
                    sqlWhere += " AND M.ST_INTERNA = 1";
            }
            if (TipoInstrumento > 0 && TipoInstrumento != 5)
            {
                sqlWhere += " AND TI.COD_SEQ_TIPO_INSTRUMENTO =" + TipoInstrumento;
            }
            else if (TipoInstrumento == 5)
            {
                sqlWhere += " AND M.ST_INICIATIVA_ESTRATEGICA = 1 ";
            }

            NHibernate.ISQLQuery sqlQuery = Session.CreateSQLQuery(String.Format(sql, String.Format(retorno, AnoExercicio), sqlWhere) + " ORDER BY 1");
            NHibernate.ISQLQuery sqlQueryCount = Session.CreateSQLQuery(String.Format(sql, "COUNT (1) ", sqlWhere));

            PageMessage pageMessage = new PageMessage();
            pageMessage.StartIndex = startIndex;
            pageMessage.PageSize = pageSize;

            return Page<VOMetaRelatorio>(sqlQuery, sqlQueryCount, pageMessage);
        }

        public IList<Meta> ListarMetasAcompanhamento(string unidade, short ano, int codigoTipoMeta, bool alinhamnetoPE, bool alinhamnetoPPA, bool alinhamnetoMI, int codigoTipoInstrumento, int metaInterna)
        {
            var criteria = DetachedCriteria.For<Meta>()
                .CreateAlias("exercicio", "e")
                .CreateAlias("corporativoUnidade", "c");

            // [codigoTipoInstrumento]
            //- 0: <Todos>
            //- 1: Agenda Regulatória
            //- 2: Desburocratização
            //- 3: Gestão de Riscos
            //- 4: Integridade
            //- 5: Iniciativa Estratégica

            switch (codigoTipoInstrumento)
            {
                case 1:
                    criteria.Add(Expression.IsNotNull("agendaRegulatoria"));
                    break;
                case 2:
                    criteria.Add(Expression.IsNotNull("desburocratizacao"));
                    break;
                case 3:
                    criteria.Add(Expression.IsNotNull("gestaoDeRiscos"));
                    break;
                case 4:
                    criteria.Add(Expression.IsNotNull("integridade"));
                    break;
                case 5:
                    criteria.Add(Expression.Eq("statusIniciativaEstrategica", true));
                    break;
            }

            if (alinhamnetoPE)
                criteria.Add(Expression.Eq("statusAlinhamentoPe", true));

            if (alinhamnetoMI)
                criteria.Add(Expression.Eq("statusAlinhamentoMissaoInstitucional", true));

            if (alinhamnetoPPA)
                criteria.Add(Expression.Eq("statusAlinhamentoPpa", true));

            if (!String.IsNullOrEmpty(unidade) && unidade != "Todas")
                criteria.Add(Expression.Eq("c.descricaoUnidade", (unidade.Contains("Inativa") ? unidade.Replace(" - Inativa", "") : unidade)));

            if (ano > 0)
                criteria.Add(Expression.Eq("e.ano", ano));

            if (codigoTipoMeta > 0)
                criteria.Add(Expression.Eq("tipoMeta.codigoSeqTipoMeta", codigoTipoMeta));

            if (metaInterna == 1)
                criteria.Add(Expression.Eq("statusMetaInterna", false));
            else if (metaInterna == 2)
                criteria.Add(Expression.Eq("statusMetaInterna", true));
            criteria.CreateAlias("situacoes", "s");
            criteria.Add(Expression.Eq("s.statusMeta.codigoSeqStatusMeta", 3));

            criteria.AddOrder(Order.Asc("c.descricaoUnidade"));

            return List<Meta>(criteria);
        }
    }
}