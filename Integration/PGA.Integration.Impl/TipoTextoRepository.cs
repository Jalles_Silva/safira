﻿using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class TipoTextoRepository : RepositoryBase<TipoTexto>, ITipoTextoRepository<TipoTexto>
    {
    }
}