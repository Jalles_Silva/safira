﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class TipoIniciativaRepository : RepositoryBase<TipoIniciativa>, ITipoIniciativaRepository<TipoIniciativa>
    {
    }
}
