﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class IniciativaRepository : RepositoryBase<Iniciativa>, IIniciativaRepository<Iniciativa>
    {
        public IList<Iniciativa> ListarIniciativaPorTipoEUnidadeAtivos(int tipoIniciativa, int codigoUnidade)
        {
            var criteria = DetachedCriteria.For<Iniciativa>();
            
            if (tipoIniciativa > 0)
                criteria.Add(Expression.Eq("tipoIniciativa.codigoSeqTipoIniciativa", tipoIniciativa));

            if (codigoUnidade > 0)
                criteria.Add(Expression.Eq("corporativoUnidade.codigoUnidade", codigoUnidade));

            criteria.Add(Expression.Eq("ativo", true));

            return List<Iniciativa>(criteria);
        }

        public IList<Iniciativa> ListarTiposIniciativaFiltro(int tipoIniciativa)
        {
            var criteria = DetachedCriteria.For<Iniciativa>();

            if (tipoIniciativa > 0)
                criteria.Add(Expression.Eq("tipoIniciativa.codigoSeqTipoIniciativa", tipoIniciativa));

            criteria.Add(Expression.Eq("ativo", true));

            return List<Iniciativa>(criteria);
        }

        public PageMessage<Iniciativa> ListarIniciativaPorTipoEUnidade(int tipoIniciativa, string unidade, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<Iniciativa>()
                .CreateAlias("corporativoUnidade", "c");

            if (tipoIniciativa > 0)
                criteria.Add(Expression.Eq("tipoIniciativa.codigoSeqTipoIniciativa", tipoIniciativa));

            if (!String.IsNullOrEmpty(unidade) && unidade != "Todas")
                criteria.Add(Expression.Eq("c.descricaoUnidade", (unidade.Contains("Inativa") ? unidade.Replace(" - Inativa", "") : unidade)));

            if (!String.IsNullOrEmpty(ativo))
                criteria.Add(Expression.Eq("ativo", Boolean.Parse(ativo)));

            return Page<Iniciativa>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}