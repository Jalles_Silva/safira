﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class TipoMetaRepository : RepositoryBase<TipoMeta>, ITipoMetaRepository<TipoMeta>
    {
        public PageMessage<TipoMeta> ListarTipoMetas(int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<TipoMeta>();

            return Page<TipoMeta>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}