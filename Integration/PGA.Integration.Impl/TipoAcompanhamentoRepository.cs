﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class TipoAcompanhamentoRepository : RepositoryBase<TipoAcompanhamento>, ITipoAcompanhamentoRepository<TipoAcompanhamento>
    {
        public PageMessage<TipoAcompanhamento> ListarTiposAcompanhamentoPorTipoESituacao(string tipoAcompanhamento, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<TipoAcompanhamento>();

            if (!String.IsNullOrEmpty(tipoAcompanhamento))
                criteria.Add(Expression.Like("descricaoTipoAcompanhamento", tipoAcompanhamento, MatchMode.Anywhere));

            if (!String.IsNullOrEmpty(ativo))
                criteria.Add(Expression.Eq("ativo", Boolean.Parse(ativo)));

            return Page<TipoAcompanhamento>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}
