﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;
using PGA.Integration.Spec.ValueObjects;

namespace PGA.Integration.Impl
{
    public class IndicadorRepository : RepositoryBase<Indicador>, IIndicadorRepository<Indicador>
    {
        public PageMessage<Indicador> ListarIndicadoresPorMeta(int codigoSeqMeta, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<Indicador>();
            criteria.Add(Expression.Eq("meta.codSeqMeta", codigoSeqMeta));

            return Page<Indicador>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }

        public PageMessage<VOIndicador> ListarIndicadoresPorFiltros(VOFiltroControle filtro)
        {
            var sql = @"select {0} from
                        (select 
                            i.CD_SEQ_INDICADOR as CodigoSeqIndicador, 
                            i.NO_INDICADOR as NoIndicador,
                            i.NU_VALORALVO as ValorAlvo,
                            i.DT_DATAALVO as DataAlvo,
                            m.DS_META as DescricaoMeta,
                            ci.DT_INDICADOR as DataUltimoValor,
                            ci.VL_INDICADOR as UltimoValor,
                            (select isnull(sum(VL_INDICADOR), 0) from TB_CONTROLE_INDICADOR where CD_INDICADOR = i.CD_SEQ_INDICADOR) as ValorTotalInformado,
                            CASE   
                                WHEN i.CD_TIPO_RESULTADO = 1 THEN 
						                (select isnull(sum(VL_INDICADOR), 0) from TB_CONTROLE_INDICADOR where CD_INDICADOR = i.CD_SEQ_INDICADOR)
                                WHEN i.CD_TIPO_RESULTADO = 2 THEN 
						             CASE   
			                            WHEN (select COUNT(1) from TB_CONTROLE_INDICADOR where CD_INDICADOR = i.CD_SEQ_INDICADOR) > 0 THEN 
					                          (select isnull(sum(VL_INDICADOR), 0) / COUNT(1) from TB_CONTROLE_INDICADOR where CD_INDICADOR = i.CD_SEQ_INDICADOR)
			                            ElSE
				                            0
			                            END
                            END AS ResultadoIndicador,
                            v.CD_SEQ_TIPO_VALOR as CodSeqTipoValor,
		                    e.NU_ANO_EXERCICIO as AnoExercicio,
		                    CASE WHEN uc.ST_ATIVO = 0 THEN
								uc.DS_UNIDADE + ' - Inativa'
							ELSE
								uc.DS_UNIDADE
							END AS UnidadeCorporativa,
                            e.CD_FASE as CodigoFaseExercicio,
                            uc.CD_UNIDADE as CodigoUnidade
                        from TB_INDICADOR i
                        inner join dbo.TB_META m on i.CD_META=m.CD_SEQ_META
                        inner join (select CD_META, max(CD_SEQ_SITUACAO_META) as codigo from TB_SITUACAO_META group by CD_META) statusAtual 
                            on statusAtual.CD_META = m.CD_SEQ_META
                        inner join TB_SITUACAO_META s on statusAtual.codigo = s.CD_SEQ_SITUACAO_META AND S.CD_STATUS_META != 4
                        left join TB_TIPO_VALOR v ON i.CD_TIPO_VALOR = v.CD_SEQ_TIPO_VALOR
                        inner join TB_TIPO_RESULTADO r on i.CD_TIPO_RESULTADO = r.CD_SEQ_TIPO_RESULTADO
                        inner join TB_EXERCICIO E on m.CD_EXERCICIO = e.CD_SEQ_EXERCICIO
                        inner join TB_CORPORATIVO_UNIDADE uc on m.CD_UNIDADE = uc.CD_UNIDADE
                        left join (select CD_INDICADOR, DT_INDICADOR, CD_SEQ_CONTROLE_INDICADOR as codigo,
	                               Row_Number() OVER(PARTITION BY cD_Indicador ORDER BY DT_Indicador DESC, Dt_Cadastro DESC) AS Rank_ID 
                                   from TB_CONTROLE_INDICADOR) ciAtual on ciAtual.CD_INDICADOR = i.CD_SEQ_INDICADOR AND ciAtual.Rank_ID = 1
                        left join dbo.TB_CONTROLE_INDICADOR ci on ciAtual.codigo = ci.CD_SEQ_CONTROLE_INDICADOR
                        where s.CD_STATUS_META = 3  
                        and e.CD_FASE <> 1
                        and (:CodigoMeta = 0 OR m.CD_SEQ_META = :CodigoMeta) 
                        and (:Unidade = 'Todas' OR uc.DS_UNIDADE = :Unidade)
                        and (:AnoExercicio = 0 OR e.NU_ANO_EXERCICIO = :AnoExercicio)
                        ) as t
                        where (:Status = 'Todos'
	                            OR (:Status = 'Atingido' AND (t.ResultadoIndicador >= t.ValorAlvo))
                                OR (:Status = 'AtingidoComAtraso' AND (t.ResultadoIndicador >= t.ValorAlvo) AND t.DataUltimoValor > t.DataAlvo)
	                            OR (:Status = 'AtingidoNoPrazo' AND (t.ResultadoIndicador >= t.ValorAlvo) AND t.DataUltimoValor <= t.DataAlvo)
	                            OR (:Status = 'NaoAtingido' AND ((t.ResultadoIndicador < t.ValorAlvo) OR UltimoValor IS NULL))
	                            OR (:Status = 'NaoAtingidoEmAtraso' AND (t.ResultadoIndicador < t.ValorAlvo) AND (t.DataAlvo < GETDATE() OR t.DataAlvo < t.DataUltimoValor))
	                            OR (:Status = 'NaoAtingidoAindaNoPrazo'AND (t.ResultadoIndicador < t.ValorAlvo) AND (t.DataAlvo > GETDATE() OR t.DataAlvo > t.DataUltimoValor)))
                        {1} ";

            PageMessage dadosPaginacao = new PageMessage();
            dadosPaginacao.StartIndex = filtro.StartIndex;
            dadosPaginacao.PageSize = filtro.PageSize;

            NHibernate.ISQLQuery sqlQuery = Session.CreateSQLQuery(String.Format(sql, "*", "order by t.UnidadeCorporativa, t.DescricaoMeta, t.NoIndicador"));
            NHibernate.ISQLQuery sqlQueryCount = Session.CreateSQLQuery(String.Format(sql, "count(1)", ""));

            sqlQuery.SetParameter("CodigoMeta", filtro.CodigoMeta);
            sqlQuery.SetParameter("Unidade", (filtro.Unidade.Contains("Inativa") ? filtro.Unidade.Replace(" - Inativa", "") : filtro.Unidade));
            sqlQuery.SetParameter("Status", filtro.Status);
            sqlQuery.SetParameter("AnoExercicio", filtro.AnoExercicio);

            sqlQueryCount.SetParameter("CodigoMeta", filtro.CodigoMeta);
            sqlQueryCount.SetParameter("Unidade", (filtro.Unidade.Contains("Inativa") ? filtro.Unidade.Replace(" - Inativa", "") : filtro.Unidade));
            sqlQueryCount.SetParameter("Status", filtro.Status);
            sqlQueryCount.SetParameter("AnoExercicio", filtro.AnoExercicio);


            return Page<VOIndicador>(sqlQuery, sqlQueryCount, dadosPaginacao);
        }

        public PageMessage<VOIndicadorRelatorio> ListaGlobalIndicador(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize)
        {
            string sqlWhereInterno = string.Empty;
            if (MetaInterna == 1)
                sqlWhereInterno += " AND META.ST_INTERNA = 0";
            else if (MetaInterna == 2)
                sqlWhereInterno += " AND META.ST_INTERNA = 1";

            if (TipoInstrumento > 0 && TipoInstrumento != 5)
            {
                sqlWhereInterno += " AND TI.COD_SEQ_TIPO_INSTRUMENTO =" + TipoInstrumento;
            }
            else if (TipoInstrumento == 5)
            {
                sqlWhereInterno += " AND META.ST_INICIATIVA_ESTRATEGICA = 1 ";
            }
            string sql = @"SELECT {0} FROM (SELECT DS_UNIDADE AS DescricaoArea,
	                            CD_UNIDADE_INDICATIVO AS CodigoUnidade,
                                CD_SEQ_META AS CodigoMeta,
	                            (SELECT COUNT(1) FROM TB_META META
							    INNER JOIN TB_EXERCICIO EXE ON META.CD_EXERCICIO = EXE.CD_SEQ_EXERCICIO
                                LEFT JOIN TB_INSTRUMENTO INST ON INST.CD_SEQ_INSTRUMENTO = META.CD_AGENDA_REGULATORIA
							        OR INST.CD_SEQ_INSTRUMENTO = META.CD_DESBUROCRATIZACAO OR INST.CD_SEQ_INSTRUMENTO = META.CD_GESTAO_RISCO OR INST.CD_SEQ_INSTRUMENTO = META.CD_INTEGRIDADE		
							        LEFT JOIN TB_TIPO_INSTRUMENTO TI ON TI.COD_SEQ_TIPO_INSTRUMENTO = INST.CD_TIPO_INSTRUMENTO
							    WHERE META.CD_UNIDADE = CD_UNIDADE_INDICATIVO
							    AND EXE.NU_ANO_EXERCICIO = 2019" + sqlWhereInterno + @") AS TotalMetasCadastradas,
	                            (SELECT COUNT(1) FROM TB_ACAO A 
	                                    INNER JOIN TB_META M ON A.CD_META = M.CD_SEQ_META INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO 
	                            WHERE CD_UNIDADE = CD_UNIDADE_INDICATIVO AND E.NU_ANO_EXERCICIO = NU_ANO_EXERCICIO_INDICADOR) AS IndicadoresCadastradosExercicio,
	                            SUM(INDICADOR_CADASTRADO_PERIODO) AS IndicadoresCadastradasPeriodo,
	                            SUM(ISNULL(RESULTADOINDICADOR,0)) AS ResultadoIndicador,
	                            SUM(ISNULL(NU_VALORALVO,0)) AS ValorAlvo,
	                            SUM(INDICADOR_PRAZO_PERIODO) AS IndicadoresPrazoPeriodo,
	                            SUM(INDICADOR_ALIMENTADOS_PERIODO) AS IndicadoresAlimentadosPeriodo,
                                SUM(META_ST_INTERNA) AS QtdMetasInternas
	                            , {1} AnoExercicio
                                , NULL CodigoSeqIndicador
                                , NULL DescricaoIndicador
                                , NULL DescricaoMeta
                                , NULL LinhaBase
                                , NULL MesReferencia
                                , NULL DataAlvo
                                , NULL DataUltimaAtualizacao
                                , NULL CodigoTipoValor
                                , NULL MetaInterna
                                , NULL ObjetivoEstrategico
                            FROM  (SELECT DISTINCT
		                            CASE WHEN UN.ST_ATIVO = 0 THEN
								        UN.DS_UNIDADE + ' - Inativa'
							        ELSE
								        UN.DS_UNIDADE
							        END AS DS_UNIDADE, 
                                    M.CD_SEQ_META,
                                    M.CD_UNIDADE AS CD_UNIDADE_INDICATIVO,
		                            I.CD_SEQ_INDICADOR,
		                            I.DT_DATAALVO,
		                            CONVERT(DATETIME, '01-01-' + CONVERT(VARCHAR,YEAR(I.DT_DATAALVO)),103) DT_INICIO_INDICADOR,
		                            CASE WHEN I.DT_DATAALVO 
			                            BETWEEN DATEADD(HOUR,-1,TB_FASE_PERIODO.DT_INICIO_PERIODO) AND TB_FASE_PERIODO.DT_FIM_PERIODO THEN 
				                            TB_FASE_PERIODO.DT_FIM_PERIODO ELSE NULL END DT_FIM_INDICADOR,
		                            TB_FASE_PERIODO.DT_INICIO_PERIODO,
		                            TB_FASE_PERIODO.DT_FIM_PERIODO,
		                            E.NU_ANO_EXERCICIO AS NU_ANO_EXERCICIO_INDICADOR,
		                            CASE WHEN (CONVERT(DATETIME, '01-01-' + CONVERT(VARCHAR,YEAR(I.DT_DATAALVO)),103) 
			                            BETWEEN TB_FASE_PERIODO.DT_INICIO_PERIODO AND TB_FASE_PERIODO.DT_FIM_PERIODO) OR
			                            ((CASE WHEN I.DT_DATAALVO  BETWEEN DATEADD(HOUR,-1,TB_FASE_PERIODO.DT_INICIO_PERIODO) AND TB_FASE_PERIODO.DT_FIM_PERIODO THEN 
				                            TB_FASE_PERIODO.DT_FIM_PERIODO ELSE NULL END) BETWEEN TB_FASE_PERIODO.DT_INICIO_PERIODO AND TB_FASE_PERIODO.DT_FIM_PERIODO AND I.CD_SEQ_STATUS_INDICADOR = 1) 
					                            THEN 1 ELSE 0 END AS INDICADOR_CADASTRADO_PERIODO,
		                            (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = I.CD_SEQ_INDICADOR) AS VALORTOTALINFORMADO,
			                            CASE WHEN I.CD_TIPO_RESULTADO = 1 THEN (SELECT ISNULL(SUM(VL_INDICADOR), 0) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = I.CD_SEQ_INDICADOR)
			                            WHEN I.CD_TIPO_RESULTADO = 2 THEN CASE   
			                            WHEN (SELECT COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = I.CD_SEQ_INDICADOR AND I.cd_seq_status_indicador = 1) > 0 THEN 
				                            (SELECT ISNULL(SUM(VL_INDICADOR), 0) / COUNT(1) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = I.CD_SEQ_INDICADOR AND I.CD_SEQ_STATUS_INDICADOR = 1) ELSE 0 END 
		                            END AS RESULTADOINDICADOR,
		                            I.NU_VALORALVO,
		                            CASE WHEN I.DT_DATAALVO BETWEEN TB_FASE_PERIODO.DT_INICIO_PERIODO AND TB_FASE_PERIODO.DT_FIM_PERIODO THEN 1 ELSE 0 END AS INDICADOR_PRAZO_PERIODO,
		                            CASE WHEN (SELECT MAX(DT_CADASTRO) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = I.CD_SEQ_INDICADOR) 
			                            BETWEEN TB_FASE_PERIODO.DT_INICIO_PERIODO AND TB_FASE_PERIODO.DT_FIM_PERIODO THEN 1 ELSE 0 END AS INDICADOR_ALIMENTADOS_PERIODO,
                                    CASE WHEN M.ST_INTERNA = 1 THEN 1 ELSE 0 END META_ST_INTERNA
	                            FROM TB_INDICADOR I
		                            INNER JOIN TB_META M ON I.CD_META = M.CD_SEQ_META
		                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL 
			                            ON STATUSATUAL.CD_META = M.CD_SEQ_META
		                            INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META AND S.CD_STATUS_META != 4
		                            INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
                                    LEFT JOIN TB_INSTRUMENTO INST ON INST.CD_SEQ_INSTRUMENTO = M.CD_AGENDA_REGULATORIA
							        OR INST.CD_SEQ_INSTRUMENTO = M.CD_DESBUROCRATIZACAO OR INST.CD_SEQ_INSTRUMENTO = M.CD_GESTAO_RISCO OR INST.CD_SEQ_INSTRUMENTO = M.CD_INTEGRIDADE		
							        LEFT JOIN TB_TIPO_INSTRUMENTO TI ON TI.COD_SEQ_TIPO_INSTRUMENTO = INST.CD_TIPO_INSTRUMENTO
		                            INNER JOIN TB_TIPO_META TM ON M.CD_TIPO_META = TM.CD_SEQ_TIPO_META
		                            INNER JOIN TB_CORPORATIVO_UNIDADE UN ON M.CD_UNIDADE = UN.CD_UNIDADE
		                            INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO,
		                            (SELECT D.CD_SEQ_FASE AS CD_FASE_PERIODO,
		                            (CASE WHEN D.NU_MES_INICIO > 0 THEN CONVERT(DATETIME, '01 - ' + 
			                            (CASE WHEN D.NU_MES_INICIO < 12 THEN CONVERT(VARCHAR,D.NU_MES_INICIO ) ELSE '01' END) + '-' + 
			                            (CASE WHEN D.NU_MES_INICIO < 12 THEN CONVERT(VARCHAR,{1}) ELSE CONVERT(VARCHAR,{1} + 1) END), 103) END) AS DT_INICIO_PERIODO,
		                            (CASE WHEN D.NU_MES_FIM > 0 THEN CONVERT(DATETIME, '01 - ' + 
			                            (CASE WHEN D.NU_MES_FIM < 12 THEN CONVERT(VARCHAR,D.NU_MES_FIM ) ELSE '01' END) + '-' + 
			                            (CASE WHEN D.NU_MES_FIM < 12 THEN CONVERT(VARCHAR,{1}) ELSE CONVERT(VARCHAR,{1} + 1) END), 103) END) AS DT_FIM_PERIODO
			                            FROM TB_DESCRICAO_FASE D {2}) AS TB_FASE_PERIODO 
			                            {3}) AS TB_INDICADORES {4}
		                            GROUP BY DS_UNIDADE, CD_SEQ_META, CD_UNIDADE_INDICATIVO, NU_ANO_EXERCICIO_INDICADOR) AS TB_GLOBAL_INDICADOR";

            string sqlWhere = "WHERE 1 = 1 ";
            string sqlWhere2 = "WHERE 1 = 1 AND DT_FIM_INDICADOR IS NOT NULL";
            string sqlWhere3 = "WHERE 1 = 1 ";

            if (alinhadasPE == true && alinhadasPPA == false && alinhadasMI == false)
                sqlWhere3 += " AND M.CD_INICIATIVA_PE IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == true && alinhadasMI == false)
                sqlWhere3 += " AND M.CD_INICIATIVA_PPA IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == false && alinhadasMI == true)
                sqlWhere3 += " AND M.CD_INICIATIVA_MISSAO IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == true && alinhadasMI == true)
            {
                sqlWhere3 += " AND (M.CD_INICIATIVA_PPA IS NOT NULL";
                sqlWhere3 += " OR M.CD_INICIATIVA_MISSAO IS NOT NULL)";
            }
            else if (alinhadasPE == true && alinhadasPPA == true && alinhadasMI == false)
            {
                sqlWhere3 += " AND (M.CD_INICIATIVA_PE IS NOT NULL";
                sqlWhere3 += " OR M.CD_INICIATIVA_PPA IS NOT NULL)";
            }
            else if (alinhadasPE == true && alinhadasPPA == false && alinhadasMI == true)
            {
                sqlWhere3 += " AND (M.CD_INICIATIVA_PE IS NOT NULL";
                sqlWhere3 += " OR M.CD_INICIATIVA_MISSAO IS NOT NULL)";
            }

            if (!String.IsNullOrEmpty(Unidade) && Unidade != "Todas")
                sqlWhere3 += " AND UN.DS_UNIDADE LIKE '" + (Unidade.Contains("Inativa") ? Unidade.Replace(" - Inativa", "") : Unidade) + "%'";

            if (AnoExercicio > 0)
                sqlWhere3 += " AND E.NU_ANO_EXERCICIO = " + AnoExercicio;

            if (codigoTipoMeta > 0)
                sqlWhere3 += " AND TM.CD_SEQ_TIPO_META = " + codigoTipoMeta;

            if (codigoPeriodo > 0)
            {
                sqlWhere += " AND CD_SEQ_FASE = " + codigoPeriodo;
                sqlWhere2 += @" AND (DT_INICIO_INDICADOR BETWEEN DT_INICIO_PERIODO AND DT_FIM_PERIODO OR
		                    DT_FIM_INDICADOR BETWEEN DT_INICIO_PERIODO AND DT_FIM_PERIODO)";
            }

            if (MetaInterna > 0)
            {
                if (MetaInterna == 1)
                    sqlWhere3 += " AND M.ST_INTERNA = 0";
                else
                    sqlWhere3 += " AND M.ST_INTERNA = 1";
            }

            if (TipoInstrumento > 0 && TipoInstrumento != 5)
            {
                sqlWhere3 += " AND TI.COD_SEQ_TIPO_INSTRUMENTO =" + TipoInstrumento;
            }
            else if (TipoInstrumento == 5)
            {
                sqlWhere3 += " AND M.ST_INICIATIVA_ESTRATEGICA = 1 ";
            }

            NHibernate.ISQLQuery sqlQuery = Session.CreateSQLQuery(String.Format(sql, " * ", AnoExercicio, sqlWhere, sqlWhere3, sqlWhere2));
            NHibernate.ISQLQuery sqlQueryCount = Session.CreateSQLQuery(String.Format(sql, " COUNT (1) ", AnoExercicio, sqlWhere, sqlWhere3, sqlWhere2));

            PageMessage pageMessage = new PageMessage();
            pageMessage.StartIndex = startIndex;
            pageMessage.PageSize = pageSize;

            return Page<VOIndicadorRelatorio>(sqlQuery, sqlQueryCount, pageMessage);
        }

        public PageMessage<VOIndicadorRelatorio> ListaDetalheIndicador(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize)
        {
            string sql = @"SELECT {0} FROM 
                            (SELECT DISTINCT *  FROM 
                            (SELECT 
                                I.CD_SEQ_INDICADOR AS CodigoSeqIndicador
                                ,CASE WHEN U.ST_ATIVO = 0 THEN
								    U.DS_UNIDADE + ' - Inativa'
							    ELSE
								    U.DS_UNIDADE
							    END AS DescricaoArea
	                            ,M.CD_UNIDADE AS CodigoUnidade
                                ,M.CD_SEQ_META AS CodigoMeta
	                            ,M.DS_META AS DescricaoMeta
                                ,I.DS_INDICADOR AS DescricaoIndicador    
	                            ,I.NU_LINHABASE AS LinhaBase
	                            ,I.DT_MESREFERENCIA AS MesReferencia
	                            ,I.NU_VALORALVO AS ValorAlvo
	                            ,I.DT_DATAALVO as DataAlvo
	                            , (SELECT MAX(DT_CADASTRO) FROM TB_CONTROLE_INDICADOR WHERE CD_INDICADOR = I.CD_SEQ_INDICADOR) AS DataUltimaAtualizacao
                                ,CASE 
		                            WHEN i.CD_TIPO_RESULTADO = 1 THEN  (select isnull(sum(VL_INDICADOR), 0) from TB_CONTROLE_INDICADOR 
                                        where CD_INDICADOR = i.CD_SEQ_INDICADOR)
		                            WHEN i.CD_TIPO_RESULTADO = 2 THEN  CASE 
                                            WHEN (select COUNT(1) from TB_CONTROLE_INDICADOR where CD_INDICADOR = i.CD_SEQ_INDICADOR) > 0 THEN 
		                            (select isnull(sum(VL_INDICADOR), 0) / COUNT(1) from TB_CONTROLE_INDICADOR where CD_INDICADOR = i.CD_SEQ_INDICADOR) 
                                            ElSE 0 END END AS ResultadoIndicador,
	                            NULL AS TotalMetasCadastradas,
                                NULL AS IndicadoresCadastradosExercicio,
                                NULL AS IndicadoresCadastradasPeriodo,
                                NULL AS IndicadoresPrazoPeriodo,
                                NULL AS IndicadoresAlimentadosPeriodo,
                                NULL AS QtdMetasInternas,
                                NULL AS MetaInterna
                                ,{1} AnoExercicio
                                , i.CD_TIPO_VALOR CodigoTipoValor
                                , I.ST_OBJETIVO_ESTRATEGICO AS ObjetivoEstrategico
	                            FROM TB_INDICADOR I
	                            INNER JOIN TB_META M ON I.CD_META = M.CD_SEQ_META
                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL 
								    ON STATUSATUAL.CD_META = M.CD_SEQ_META
							    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
							    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                            INNER JOIN TB_TIPO_META TM ON M.CD_TIPO_META = TM.CD_SEQ_TIPO_META
                                LEFT JOIN TB_INSTRUMENTO INST ON INST.CD_SEQ_INSTRUMENTO = M.CD_AGENDA_REGULATORIA
							    OR INST.CD_SEQ_INSTRUMENTO = M.CD_DESBUROCRATIZACAO OR INST.CD_SEQ_INSTRUMENTO = M.CD_GESTAO_RISCO OR INST.CD_SEQ_INSTRUMENTO = M.CD_INTEGRIDADE		
							    LEFT JOIN TB_TIPO_INSTRUMENTO TI ON TI.COD_SEQ_TIPO_INSTRUMENTO = INST.CD_TIPO_INSTRUMENTO
	                            INNER JOIN TB_CORPORATIVO_UNIDADE U ON M.CD_UNIDADE = U.CD_UNIDADE
	                            INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                            INNER JOIN TB_DESCRICAO_FASE D ON E.CD_FASE = D.CD_SEQ_FASE,
                                (SELECT D.CD_SEQ_FASE AS CD_FASE_PERIODO,
	                                (CASE WHEN D.NU_MES_INICIO > 0 THEN CONVERT(DATETIME, '01 - ' + 
	                                (CASE WHEN D.NU_MES_INICIO < 12 THEN CONVERT(VARCHAR,D.NU_MES_INICIO ) ELSE '01' END) + '-' + 
	                                (CASE WHEN D.NU_MES_INICIO < 12 THEN CONVERT(VARCHAR,{1}) ELSE CONVERT(VARCHAR,{1} + 1) END), 103) END) AS DT_INICIO_PERIODO,
	                                (CASE WHEN D.NU_MES_FIM > 0 THEN CONVERT(DATETIME, '01 - ' + 
	                                (CASE WHEN D.NU_MES_FIM < 12 THEN CONVERT(VARCHAR,D.NU_MES_FIM ) ELSE '01' END) + '-' + 
	                                (CASE WHEN D.NU_MES_FIM < 12 THEN CONVERT(VARCHAR,{1}) ELSE CONVERT(VARCHAR,{1} + 1) END), 103) END) AS DT_FIM_PERIODO
	                                FROM TB_DESCRICAO_FASE D {2} ) AS TB_FASE_PERIODO 
                                {3}) AS TB_INDICADOR_DETALHE) AS TB_DETALHE";

            string sqlWhere = "WHERE 1 = 1  AND I.CD_SEQ_STATUS_INDICADOR = 1";
            string sqlWhere2 = "WHERE 1 = 1 ";

            if (alinhadasPE == true && alinhadasPPA == false && alinhadasMI == false)
                sqlWhere += " AND M.CD_INICIATIVA_PE IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == true && alinhadasMI == false)
                sqlWhere += " AND M.CD_INICIATIVA_PPA IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == false && alinhadasMI == true)
                sqlWhere += " AND M.CD_INICIATIVA_MISSAO IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == true && alinhadasMI == true)
            {
                sqlWhere += " AND (M.CD_INICIATIVA_PPA IS NOT NULL";
                sqlWhere += " OR M.CD_INICIATIVA_MISSAO IS NOT NULL)";
            }
            else if (alinhadasPE == true && alinhadasPPA == true && alinhadasMI == false)
            {
                sqlWhere += " AND (M.CD_INICIATIVA_PE IS NOT NULL";
                sqlWhere += " OR M.CD_INICIATIVA_PPA IS NOT NULL)";
            }
            else if (alinhadasPE == true && alinhadasPPA == false && alinhadasMI == true)
            {
                sqlWhere += " AND (M.CD_INICIATIVA_PE IS NOT NULL";
                sqlWhere += " OR M.CD_INICIATIVA_MISSAO IS NOT NULL)";
            }

            if (!String.IsNullOrEmpty(Unidade) && Unidade != "Todas")
                sqlWhere += " AND U.DS_UNIDADE = '" + (Unidade.Contains("Inativa") ? Unidade.Replace(" - Inativa", "") : Unidade) + "'";

            if (AnoExercicio > 0)
                sqlWhere += " AND E.NU_ANO_EXERCICIO = " + AnoExercicio;

            if (codigoTipoMeta > 0)
                sqlWhere += " AND TM.CD_SEQ_TIPO_META = " + codigoTipoMeta;

            if (codigoPeriodo > 0)
            {
                sqlWhere2 += " AND D.CD_SEQ_FASE = " + codigoPeriodo;
                sqlWhere += @"AND ((CONVERT(DATETIME, '01-01-' + CONVERT(VARCHAR,YEAR(I.DT_DATAALVO)),103)) BETWEEN TB_FASE_PERIODO.DT_INICIO_PERIODO AND TB_FASE_PERIODO.DT_FIM_PERIODO OR
		             (CASE WHEN I.DT_DATAALVO 
				        BETWEEN DATEADD(HOUR,-1,TB_FASE_PERIODO.DT_INICIO_PERIODO) AND TB_FASE_PERIODO.DT_FIM_PERIODO THEN 
					        TB_FASE_PERIODO.DT_FIM_PERIODO ELSE NULL END) BETWEEN TB_FASE_PERIODO.DT_INICIO_PERIODO AND TB_FASE_PERIODO.DT_FIM_PERIODO)";
            }

            if (MetaInterna > 0)
            {
                if (MetaInterna == 1)
                    sqlWhere += " AND M.ST_INTERNA = 0";
                else
                    sqlWhere += " AND M.ST_INTERNA = 1";
            }

            if (TipoInstrumento > 0 && TipoInstrumento != 5)
            {
                sqlWhere += " AND TI.COD_SEQ_TIPO_INSTRUMENTO =" + TipoInstrumento;
            }
            else if (TipoInstrumento == 5)
            {
                sqlWhere += " AND M.ST_INICIATIVA_ESTRATEGICA = 1 ";
            }

            NHibernate.ISQLQuery sqlQuery = Session.CreateSQLQuery(String.Format(sql, " * ", AnoExercicio, sqlWhere2, sqlWhere) + " ORDER BY DescricaoArea");
            NHibernate.ISQLQuery sqlQueryCount = Session.CreateSQLQuery(String.Format(sql, " COUNT (1) ", AnoExercicio, sqlWhere2, sqlWhere));

            PageMessage pageMessage = new PageMessage();
            pageMessage.StartIndex = startIndex;
            pageMessage.PageSize = pageSize;

            return Page<VOIndicadorRelatorio>(sqlQuery, sqlQueryCount, pageMessage);
        }
    }
}