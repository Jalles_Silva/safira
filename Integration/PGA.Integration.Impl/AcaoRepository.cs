﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;
using PGA.Integration.Spec.ValueObjects;
using NHibernate.Transform;

namespace PGA.Integration.Impl
{
    public class AcaoRepository : RepositoryBase<Acao>, IAcaoRepository<Acao>
    {
        public PageMessage<Acao> ListarAcoesPorMeta(int codigoSeqMeta, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<Acao>();
            criteria.Add(Expression.Eq("meta.codSeqMeta", codigoSeqMeta));

            return Page<Acao>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }

        public PageMessage<VOAcao> ListarAcoesPorFiltros(VOFiltroControle filtro)
        {
            var a = @"a.CD_SEQ_ACAO as CodigoSeqAcao, 
                                a.DS_ATIVIDADE_ACAO as DescricaoAtividadeAcao,
                                a.DS_ESTRATEGIA_ACAO as DescricaoEstrategiaAcao,
                                a.DT_INICIO_ACAO as DataInicioAcao,
                                a.DT_FIM_ACAO as DataFimAcao,
                                m.DS_META as DescricaoMeta,
                                ca.DT_REPORTE as DataUltimoReporte,
                                ca.VL_PERCENTUAL_REPORTE as PercentualExecucao,
		                        e.NU_ANO_EXERCICIO as AnoExercicio,
		                        CASE WHEN uc.ST_ATIVO = 0 THEN
									uc.DS_UNIDADE + ' - Inativa'
								ELSE
									uc.DS_UNIDADE
								END AS UnidadeCorporativa,
                                e.CD_FASE as CodigoFaseExercicio,
                                uc.CD_UNIDADE as CodigoUnidade";

            var sql = @"select 
                              {0}                           
                        from TB_ACAO a
                        inner join dbo.TB_META m on a.CD_META=m.CD_SEQ_META
                        inner join (select CD_META, max(CD_SEQ_SITUACAO_META) as codigo from TB_SITUACAO_META group by CD_META) statusAtual 
                            on statusAtual.CD_META = m.CD_SEQ_META
                        inner join TB_SITUACAO_META s on statusAtual.codigo = s.CD_SEQ_SITUACAO_META
                        inner join TB_EXERCICIO E on m.CD_EXERCICIO = e.CD_SEQ_EXERCICIO
	                    inner join TB_CORPORATIVO_UNIDADE uc on m.CD_UNIDADE = uc.CD_UNIDADE
                        left join (select CD_ACAO, max(CD_SEQ_CONTROLE_ACAO) as codigo from TB_CONTROLE_ACAO group by CD_ACAO) caAtual on caAtual.CD_ACAO = a.CD_SEQ_ACAO
                        left join dbo.TB_CONTROLE_ACAO ca on caAtual.codigo = ca.CD_SEQ_CONTROLE_ACAO
                        where s.CD_STATUS_META = 3
                        and e.CD_FASE <> 1
                        and (:CodigoMeta = 0 OR m.CD_SEQ_META = :CodigoMeta) 
                        and (:Unidade = 'Todas' OR uc.DS_UNIDADE = :Unidade)
                        and (:AnoExercicio = 0 OR e.NU_ANO_EXERCICIO = :AnoExercicio) 
                        and (:Status = 'Todas'
	                        OR (:Status = 'Concluida' AND (ca.VL_PERCENTUAL_REPORTE = 100))
	                        OR (:Status = 'ConcluidaComAtraso' AND (ca.VL_PERCENTUAL_REPORTE = 100) and ca.DT_REPORTE > a.DT_FIM_ACAO)
	                        OR (:Status = 'ConcluidaNoPrazo' AND (ca.VL_PERCENTUAL_REPORTE = 100) and ca.DT_REPORTE <= a.DT_FIM_ACAO)
	                        OR (:Status = 'NaoConcluida' AND ((ca.VL_PERCENTUAL_REPORTE <> 100) OR ca.VL_PERCENTUAL_REPORTE IS NULL))
	                        OR (:Status = 'NaoConcluidaEmAtraso' AND (ca.VL_PERCENTUAL_REPORTE <> 100) and a.DT_FIM_ACAO < getdate())
	                        OR (:Status = 'NaoConcluidaAindaNoPrazo' AND (ca.VL_PERCENTUAL_REPORTE <> 100) and a.DT_FIM_ACAO >= getdate()))
                        {1} ";


            PageMessage dadosPaginacao = new PageMessage();
            dadosPaginacao.StartIndex = filtro.StartIndex;
            dadosPaginacao.PageSize = filtro.PageSize;

            NHibernate.ISQLQuery sqlQuery = Session.CreateSQLQuery(String.Format(sql, a, "order by uc.DS_UNIDADE, m.DS_META, a.DS_ATIVIDADE_ACAO"));
            NHibernate.ISQLQuery sqlQueryCount = Session.CreateSQLQuery(String.Format(sql, "count(1)", ""));

            sqlQuery.SetParameter("CodigoMeta", filtro.CodigoMeta);
            sqlQuery.SetParameter("Unidade", (filtro.Unidade.Contains("Inativa") ? filtro.Unidade.Replace(" - Inativa", "") : filtro.Unidade));
            sqlQuery.SetParameter("Status", filtro.Status);
            sqlQuery.SetParameter("AnoExercicio", filtro.AnoExercicio);

            sqlQueryCount.SetParameter("CodigoMeta", filtro.CodigoMeta);
            sqlQueryCount.SetParameter("Unidade", (filtro.Unidade.Contains("Inativa") ? filtro.Unidade.Replace(" - Inativa", "") : filtro.Unidade));
            sqlQueryCount.SetParameter("Status", filtro.Status);
            sqlQueryCount.SetParameter("AnoExercicio", filtro.AnoExercicio);


            return Page<VOAcao>(sqlQuery, sqlQueryCount, dadosPaginacao);

        }

        public PageMessage<VOAcaoRelatorio> ListaGlobalAtividade(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize)
        {
            string sqlWhereInterno = string.Empty;

            if (MetaInterna == 1)
                sqlWhereInterno += " AND META.ST_INTERNA = 0";
            else if (MetaInterna == 2)
                sqlWhereInterno += " AND META.ST_INTERNA = 1";

            if (TipoInstrumento > 0 && TipoInstrumento != 5)
            {
                sqlWhereInterno += " AND TI.COD_SEQ_TIPO_INSTRUMENTO =" + TipoInstrumento;
            }
            else if (TipoInstrumento == 5)
            {
                sqlWhereInterno += " AND META.ST_INICIATIVA_ESTRATEGICA = 1 ";
            }

            string sql = @"SELECT {0} FROM (SELECT DS_UNIDADE AS DescricaoArea,
                            (SELECT COUNT(1) FROM TB_META META
							INNER JOIN TB_EXERCICIO EXE ON META.CD_EXERCICIO = EXE.CD_SEQ_EXERCICIO 
                            LEFT JOIN TB_INSTRUMENTO INST ON INST.CD_SEQ_INSTRUMENTO = META.CD_AGENDA_REGULATORIA
							OR INST.CD_SEQ_INSTRUMENTO = META.CD_DESBUROCRATIZACAO OR INST.CD_SEQ_INSTRUMENTO = META.CD_GESTAO_RISCO OR INST.CD_SEQ_INSTRUMENTO = META.CD_INTEGRIDADE		
							LEFT JOIN TB_TIPO_INSTRUMENTO TI ON TI.COD_SEQ_TIPO_INSTRUMENTO = INST.CD_TIPO_INSTRUMENTO
							WHERE META.CD_UNIDADE = CD_UNIDADE_ACAO
							AND EXE.NU_ANO_EXERCICIO = 2019" + sqlWhereInterno + @") AS TotalMetasCadastradas,
                            (SELECT COUNT(1) FROM TB_ACAO A 
	                            INNER JOIN TB_META M ON A.CD_META = M.CD_SEQ_META INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
                                LEFT JOIN TB_INSTRUMENTO INST ON INST.CD_SEQ_INSTRUMENTO = M.CD_AGENDA_REGULATORIA
							    OR INST.CD_SEQ_INSTRUMENTO = M.CD_DESBUROCRATIZACAO OR INST.CD_SEQ_INSTRUMENTO = M.CD_GESTAO_RISCO OR INST.CD_SEQ_INSTRUMENTO = M.CD_INTEGRIDADE		
							    LEFT JOIN TB_TIPO_INSTRUMENTO TI ON TI.COD_SEQ_TIPO_INSTRUMENTO = INST.CD_TIPO_INSTRUMENTO
	                            WHERE M.CD_UNIDADE = CD_UNIDADE_ACAO AND E.NU_ANO_EXERCICIO = NU_ANO_EXERCICIO_ACAO AND A.CD_SEQ_STATUS_ACAO = 1) AS AtividadesCadastradasExercicio,
                                SUM(ATIVIDADE_CADASTRADA_PERIODO) AS AtividadesCadastradasPeriodo,
                                SUM(ATIVIDADE_CONCLUIDA_PERIODO) AS AtividadesConcluidas,
                                (SUM(PERCENTUAL_EXECUCAO)/COUNT(1)) AS PercentualExecucao,
                                (SELECT COUNT(1) FROM TB_ACAO A 
		                            INNER JOIN TB_META M ON A.CD_META = M.CD_SEQ_META 
		                            INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO 
                                    LEFT JOIN TB_INSTRUMENTO INST ON INST.CD_SEQ_INSTRUMENTO = M.CD_AGENDA_REGULATORIA
							        OR INST.CD_SEQ_INSTRUMENTO = M.CD_DESBUROCRATIZACAO OR INST.CD_SEQ_INSTRUMENTO = M.CD_GESTAO_RISCO OR INST.CD_SEQ_INSTRUMENTO = M.CD_INTEGRIDADE		
							LEFT JOIN TB_TIPO_INSTRUMENTO TI ON TI.COD_SEQ_TIPO_INSTRUMENTO = INST.CD_TIPO_INSTRUMENTO
	                            WHERE M.CD_UNIDADE = CD_UNIDADE_ACAO AND E.NU_ANO_EXERCICIO = NU_ANO_EXERCICIO_ACAO AND A.CD_SEQ_STATUS_ACAO = 1) AS AtividadesConcluidasPeriodo,
                                SUM(ATIVIDADE_PRAZO) AS AtividadesPeriodoPrazo,
                                SUM(TOTAL_RECURSO_FINANCEIRO) AS RecursoFinanceiro,
                                SUM(TOTAL_RECURSO_ESTIMADO) AS RecursoEstimado,
                                SUM(TOTAL_RECURSO_ORCAMENTARIO_UTILIZADO) AS RecursoOrcamentarioUtilizado,
                                SUM(META_ST_INTERNA) AS QtdMetasInternas,
                                {1} AnoExercicio,
                                NULL CodigoSeqAcao,
                                NULL CodigoMeta,
                                NULL DescricaoMeta,
                                NULL DescricaoAtividade,
                                NULL DescricaoEstrategia,
                                NULL DataInicioAtividade,
                                NULL DataFimAtividade,
                                NULL DataUltimaAtualizacao,
	                            NULL CodigoUnidade,
	                            NULL AtividadesRecursoFinanceiro,
                                NULL MetaInterna
                                FROM (SELECT 
                                CASE WHEN UN.ST_ATIVO = 0 THEN
									UN.DS_UNIDADE + ' - Inativa'
								ELSE
									UN.DS_UNIDADE
								END AS DS_UNIDADE, 
                                M.CD_UNIDADE AS CD_UNIDADE_ACAO,
	                            E.CD_FASE AS CD_FASE_ACAO,
	                            TM.CD_SEQ_TIPO_META AS CD_TIPO_META_ACAO,
                                E.NU_ANO_EXERCICIO AS NU_ANO_EXERCICIO_ACAO,
                                CASE WHEN (A.DT_INICIO_ACAO BETWEEN TB_FASE_PERIODO.DT_INICIO_PERIODO AND TB_FASE_PERIODO.DT_FIM_PERIODO) OR
		                            (A.DT_FIM_ACAO BETWEEN TB_FASE_PERIODO.DT_INICIO_PERIODO AND TB_FASE_PERIODO.DT_FIM_PERIODO) THEN 1 ELSE 0 END AS ATIVIDADE_CADASTRADA_PERIODO,
                                CASE WHEN (SELECT SUM(VL_PERCENTUAL_REPORTE) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO AND 
		                            ((A.DT_INICIO_ACAO BETWEEN TB_FASE_PERIODO.DT_INICIO_PERIODO AND TB_FASE_PERIODO.DT_FIM_PERIODO) OR
		                            (A.DT_FIM_ACAO BETWEEN TB_FASE_PERIODO.DT_INICIO_PERIODO AND TB_FASE_PERIODO.DT_FIM_PERIODO))) = 100 THEN 1 ELSE 0 END AS ATIVIDADE_CONCLUIDA_PERIODO,
                                CASE WHEN (SELECT SUM(VL_PERCENTUAL_REPORTE) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO AND 
		                            ((A.DT_FIM_ACAO BETWEEN TB_FASE_PERIODO.DT_INICIO_PERIODO AND TB_FASE_PERIODO.DT_FIM_PERIODO))) <= 100 THEN 1 ELSE 0 END AS ATIVIDADE_PRAZO,
                            (SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO AS PERCENTUAL_EXECUCAO,
                            A.CD_SEQ_ACAO,
                            CASE WHEN M.ST_INTERNA = 1 THEN 1 ELSE 0 END META_ST_INTERNA,
                            (SELECT SUM(CASE WHEN VL_RECURSO_FINANCEIRO > 0 THEN 1 ELSE 1 END) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) AS TOTAL_RECURSO_FINANCEIRO,
                            (SELECT SUM(VL_RECURSO) FROM TB_RECURSO WHERE CD_ACAO = A.CD_SEQ_ACAO AND CD_TIPO_RECURSO = 7) AS TOTAL_RECURSO_ESTIMADO ,
                            (SELECT SUM(VL_RECURSO_FINANCEIRO) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) AS TOTAL_RECURSO_ORCAMENTARIO_UTILIZADO
                                FROM TB_ACAO A
	                            INNER JOIN TB_META M ON A.CD_META = M.CD_SEQ_META
                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL 
								    ON STATUSATUAL.CD_META = M.CD_SEQ_META
							    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
							    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3                                 
                                INNER JOIN TB_TIPO_META TM ON M.CD_TIPO_META = TM.CD_SEQ_TIPO_META
                                LEFT JOIN TB_INSTRUMENTO INST ON INST.CD_SEQ_INSTRUMENTO = M.CD_AGENDA_REGULATORIA
							OR INST.CD_SEQ_INSTRUMENTO = M.CD_DESBUROCRATIZACAO OR INST.CD_SEQ_INSTRUMENTO = M.CD_GESTAO_RISCO OR INST.CD_SEQ_INSTRUMENTO = M.CD_INTEGRIDADE		
							LEFT JOIN TB_TIPO_INSTRUMENTO TI ON TI.COD_SEQ_TIPO_INSTRUMENTO = INST.CD_TIPO_INSTRUMENTO
	                            INNER JOIN TB_CORPORATIVO_UNIDADE UN ON M.CD_UNIDADE = UN.CD_UNIDADE
	                            INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
	                            , (SELECT D.CD_SEQ_FASE AS CD_FASE_PERIODO,
	                                (CASE WHEN D.NU_MES_INICIO > 0 THEN CONVERT(DATETIME, '01 - ' + 
		                                (CASE WHEN D.NU_MES_INICIO < 12 THEN CONVERT(VARCHAR,D.NU_MES_INICIO ) ELSE '01' END) + '-' + 
		                                (CASE WHEN D.NU_MES_INICIO < 12 THEN CONVERT(VARCHAR,{1}) ELSE CONVERT(VARCHAR,{1} + 1) END), 103) END) AS DT_INICIO_PERIODO,
	                                (CASE WHEN D.NU_MES_FIM > 0 THEN CONVERT(DATETIME, '01 - ' + 
		                                (CASE WHEN D.NU_MES_FIM < 12 THEN CONVERT(VARCHAR,D.NU_MES_FIM ) ELSE '01' END) + '-' + 
		                                (CASE WHEN D.NU_MES_FIM < 12 THEN CONVERT(VARCHAR,{1}) ELSE CONVERT(VARCHAR,{1} + 1) END), 103) END) AS DT_FIM_PERIODO
	                                FROM TB_DESCRICAO_FASE D {2}) AS TB_FASE_PERIODO  {3}) AS TB_ATIVIDADE {4} 
                            GROUP BY DS_UNIDADE, CD_UNIDADE_ACAO, NU_ANO_EXERCICIO_ACAO) AS TB_ATIVIDADE_GLOBAL";

            string sqlWhere = "WHERE 1 = 1 ";
            string sqlWhere2 = "WHERE 1 = 1 ";
            string sqlWhere3 = "WHERE 1 = 1 AND D.NU_MES_INICIO > 0 AND D.NU_MES_FIM > 0";

            if (alinhadasPE == true && alinhadasPPA == false && alinhadasMI == false)
                sqlWhere2 += " AND M.CD_INICIATIVA_PE IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == true && alinhadasMI == false)
                sqlWhere2 += " AND M.CD_INICIATIVA_PPA IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == false && alinhadasMI == true)
                sqlWhere2 += " AND M.CD_INICIATIVA_MISSAO IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == true && alinhadasMI == true)
            {
                sqlWhere2 += " AND (M.CD_INICIATIVA_PPA IS NOT NULL";
                sqlWhere2 += " OR M.CD_INICIATIVA_MISSAO IS NOT NULL)";
            }
            else if (alinhadasPE == true && alinhadasPPA == true && alinhadasMI == false)
            {
                sqlWhere2 += " AND (M.CD_INICIATIVA_PE IS NOT NULL";
                sqlWhere2 += " OR M.CD_INICIATIVA_PPA IS NOT NULL)";
            }
            else if (alinhadasPE == true && alinhadasPPA == false && alinhadasMI == true)
            {
                sqlWhere2 += " AND (M.CD_INICIATIVA_PE IS NOT NULL";
                sqlWhere2 += " OR M.CD_INICIATIVA_MISSAO IS NOT NULL)";
            }

            if (!String.IsNullOrEmpty(Unidade) && Unidade != "Todas")
                sqlWhere += " AND DS_UNIDADE LIKE '" + (Unidade.Contains("Inativa") ? Unidade.Replace(" - Inativa", "") : Unidade) + "%'";

            if (AnoExercicio > 0)
                sqlWhere += " AND NU_ANO_EXERCICIO_ACAO = " + AnoExercicio;

            if (codigoPeriodo > 0)
            {
                sqlWhere3 += " AND D.CD_SEQ_FASE = " + codigoPeriodo;
                sqlWhere2 += @" AND (A.DT_INICIO_ACAO BETWEEN TB_FASE_PERIODO.DT_INICIO_PERIODO AND TB_FASE_PERIODO.DT_FIM_PERIODO OR
							        A.DT_FIM_ACAO BETWEEN TB_FASE_PERIODO.DT_INICIO_PERIODO AND TB_FASE_PERIODO.DT_FIM_PERIODO)";
            }

            if (codigoTipoMeta > 0)
                sqlWhere2 += " AND TM.CD_SEQ_TIPO_META = " + codigoTipoMeta;

            if (MetaInterna > 0)
            {
                if (MetaInterna == 1)
                    sqlWhere2 += " AND M.ST_INTERNA = 0";
                else
                    sqlWhere2 += " AND M.ST_INTERNA = 1";
            }

            if (TipoInstrumento > 0 && TipoInstrumento != 5)
            {
                sqlWhere2 += " AND TI.COD_SEQ_TIPO_INSTRUMENTO =" + TipoInstrumento;
            }
            else if (TipoInstrumento == 5)
            {
                sqlWhere2 += " AND M.ST_INICIATIVA_ESTRATEGICA = 1 ";
            }

            NHibernate.ISQLQuery sqlQuery = Session.CreateSQLQuery(String.Format(sql, "*", AnoExercicio, sqlWhere3 ,sqlWhere2, sqlWhere));
            NHibernate.ISQLQuery sqlQueryCount = Session.CreateSQLQuery(String.Format(sql, "COUNT(1)", AnoExercicio, sqlWhere3, sqlWhere2, sqlWhere));

            PageMessage pageMessage = new PageMessage();
            pageMessage.StartIndex = startIndex;
            pageMessage.PageSize = pageSize;

            return Page<VOAcaoRelatorio>(sqlQuery, sqlQueryCount, pageMessage);
        }


        public PageMessage<VOAcaoRelatorio> ListaDetalheAtividade(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize)
        {
            string sql = @"SELECT {0} FROM (SELECT 
                                A.CD_SEQ_ACAO AS CodigoSeqAcao,
                                CASE WHEN UN.ST_ATIVO = 0 THEN
									UN.DS_UNIDADE + ' - Inativa'
								ELSE
									UN.DS_UNIDADE
								END AS DescricaoArea,
                                M.CD_SEQ_META AS CodigoMeta,
                                M.CD_UNIDADE AS CodigoUnidade,
                                M.DS_META AS DescricaoMeta,
                                A.DS_ATIVIDADE_ACAO AS DescricaoAtividade,
                                A.DS_ESTRATEGIA_ACAO AS DescricaoEstrategia,
                                ISNULL((SELECT TOP 1 VL_PERCENTUAL_REPORTE FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO ORDER BY CD_SEQ_CONTROLE_ACAO DESC),0) AS PercentualExecucao,
                                CASE WHEN (SELECT SUM(VL_RECURSO) FROM TB_RECURSO WHERE CD_ACAO = A.CD_SEQ_ACAO AND CD_TIPO_RECURSO = 7) IS NOT NULL THEN 
	                            (SELECT SUM(VL_RECURSO) FROM TB_RECURSO WHERE CD_ACAO = A.CD_SEQ_ACAO AND CD_TIPO_RECURSO = 7) ELSE 0 END AS RecursoEstimado,
                                CASE WHEN (SELECT SUM(VL_RECURSO_FINANCEIRO) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) IS NOT NULL THEN
	                            (SELECT SUM(VL_RECURSO_FINANCEIRO) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) ELSE 0 END AS RecursoOrcamentarioUtilizado,
                                A.DT_INICIO_ACAO AS DataInicioAtividade,
                                A.DT_FIM_ACAO  AS DataFimAtividade,
                                (SELECT MAX(DT_REPORTE) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO) AS DataUltimaAtualizacao,
                                NULL TotalMetasCadastradas,
                                NULL AtividadesCadastradasExercicio,
                                NULL AtividadesCadastradasPeriodo,
                                NULL AtividadesConcluidasPeriodo,
                                CASE WHEN ((SELECT SUM(VL_PERCENTUAL_REPORTE)/COUNT(1) FROM TB_CONTROLE_ACAO WHERE CD_ACAO = A.CD_SEQ_ACAO)/A.NU_PESO) = 100 
	                            THEN 1 ELSE 0 END AS AtividadesConcluidas,
                                NULL AtividadesPeriodoPrazo,
                                NULL AtividadesRecursoFinanceiro,
                                NULL RecursoFinanceiro,
                                NULL QtdMetasInternas,
                                NULL MetaInterna,
                                {1} AnoExercicio
                                FROM TB_ACAO A INNER JOIN TB_META M ON A.CD_META = M.CD_SEQ_META
                                
                                INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL 
								    ON STATUSATUAL.CD_META = M.CD_SEQ_META
							    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META 
							    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3                                 
                                LEFT JOIN TB_INSTRUMENTO INST ON INST.CD_SEQ_INSTRUMENTO = M.CD_AGENDA_REGULATORIA
							    OR INST.CD_SEQ_INSTRUMENTO = M.CD_DESBUROCRATIZACAO OR INST.CD_SEQ_INSTRUMENTO = M.CD_GESTAO_RISCO OR INST.CD_SEQ_INSTRUMENTO = M.CD_INTEGRIDADE		
							    LEFT JOIN TB_TIPO_INSTRUMENTO TI ON TI.COD_SEQ_TIPO_INSTRUMENTO = INST.CD_TIPO_INSTRUMENTO
	                            INNER JOIN TB_TIPO_META TM ON M.CD_TIPO_META = TM.CD_SEQ_TIPO_META
	                            INNER JOIN TB_CORPORATIVO_UNIDADE UN ON M.CD_UNIDADE = UN.CD_UNIDADE
	                            INNER JOIN TB_EXERCICIO E ON M.CD_EXERCICIO = E.CD_SEQ_EXERCICIO
				                ,(SELECT CONVERT(DATETIME,'01 -' + CONVERT(VARCHAR,NU_MES_INICIO) + '-' + CONVERT(VARCHAR,{1}),103) AS DT_INICIO_PERIODO, 
					                CONVERT(DATETIME,'01 -' + CONVERT(VARCHAR, (CASE WHEN NU_MES_FIM < 12 THEN NU_MES_FIM ELSE 1 END)) 
					                + '-' + CONVERT(VARCHAR, (CASE WHEN NU_MES_FIM < 12 THEN {1} ELSE {1} + 1 END)),103) AS DT_FIM_PERIODO
					                FROM TB_DESCRICAO_FASE 
				                {2}) AS TB_PERIODO {3}
                                GROUP BY UN.DS_UNIDADE, UN.ST_ATIVO, M.CD_SEQ_META, M.CD_UNIDADE, A.DS_ATIVIDADE_ACAO,
		                            M.DS_META, A.DS_ESTRATEGIA_ACAO, A.CD_SEQ_ACAO,
		                            A.NU_PESO, A.DT_INICIO_ACAO, A.DT_FIM_ACAO) AS TB_ACAO_DETALHADA";

            string sqlWhere = "WHERE 1 = 1  AND A.CD_SEQ_STATUS_ACAO = 1";
            string sqlWhere2 = "WHERE 1 = 1 AND (NU_MES_INICIO > 0 and NU_MES_FIM > 0) ";

            if (alinhadasPE == true && alinhadasPPA == false && alinhadasMI == false)
                sqlWhere += " AND M.CD_INICIATIVA_PE IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == true && alinhadasMI == false)
                sqlWhere += " AND M.CD_INICIATIVA_PPA IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == false && alinhadasMI == true)
                sqlWhere += " AND M.CD_INICIATIVA_MISSAO IS NOT NULL";
            else if (alinhadasPE == false && alinhadasPPA == true && alinhadasMI == true)
            {
                sqlWhere += " AND (M.CD_INICIATIVA_PPA IS NOT NULL";
                sqlWhere += " OR M.CD_INICIATIVA_MISSAO IS NOT NULL)";
            }
            else if (alinhadasPE == true && alinhadasPPA == true && alinhadasMI == false)
            {
                sqlWhere += " AND (M.CD_INICIATIVA_PE IS NOT NULL";
                sqlWhere += " OR M.CD_INICIATIVA_PPA IS NOT NULL)";
            }
            else if (alinhadasPE == true && alinhadasPPA == false && alinhadasMI == true)
            {
                sqlWhere += " AND (M.CD_INICIATIVA_PE IS NOT NULL";
                sqlWhere += " OR M.CD_INICIATIVA_MISSAO IS NOT NULL)";
            }

            if (!String.IsNullOrEmpty(Unidade) && Unidade != "Todas")
                sqlWhere += " AND DS_UNIDADE = '" + (Unidade.Contains("Inativa") ? Unidade.Replace(" - Inativa", "") : Unidade) + "'";

            if (AnoExercicio > 0)
                sqlWhere += " AND E.NU_ANO_EXERCICIO = " + AnoExercicio;

            if (codigoTipoMeta > 0)
                sqlWhere += " AND TM.CD_SEQ_TIPO_META = " + codigoTipoMeta;

            if (codigoPeriodo > 0)
            {
                sqlWhere2 += " AND CD_SEQ_FASE = " + codigoPeriodo;
                sqlWhere += @"AND (A.DT_INICIO_ACAO BETWEEN DT_INICIO_PERIODO AND DT_FIM_PERIODO OR
						    A.DT_FIM_ACAO BETWEEN DT_INICIO_PERIODO AND DT_FIM_PERIODO)";
            }

            if (MetaInterna > 0)
            {
                if (MetaInterna == 1)
                    sqlWhere += " AND M.ST_INTERNA = 0";
                else
                    sqlWhere += " AND M.ST_INTERNA = 1";
            }

            if (TipoInstrumento > 0 && TipoInstrumento != 5)
            {
                sqlWhere += " AND TI.COD_SEQ_TIPO_INSTRUMENTO =" + TipoInstrumento;
            }
            else if (TipoInstrumento == 5)
            {
                sqlWhere += " AND M.ST_INICIATIVA_ESTRATEGICA = 1 ";
            }

            NHibernate.ISQLQuery sqlQuery = Session.CreateSQLQuery(String.Format(sql, " * ", AnoExercicio, sqlWhere2, sqlWhere) + " ORDER BY DescricaoArea, DescricaoMeta");
            NHibernate.ISQLQuery sqlQueryCount = Session.CreateSQLQuery(String.Format(sql, " COUNT (1) ", AnoExercicio, sqlWhere2, sqlWhere));

            PageMessage pageMessage = new PageMessage();
            pageMessage.StartIndex = startIndex;
            pageMessage.PageSize = pageSize;

            return Page<VOAcaoRelatorio>(sqlQuery, sqlQueryCount, pageMessage);
        }
    }
}