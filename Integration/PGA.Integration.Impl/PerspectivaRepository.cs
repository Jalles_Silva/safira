﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class PerspectivaRepository : RepositoryBase<Perspectiva>, IPerspectivaRepository<Perspectiva>
    {
        public PageMessage<Perspectiva> ListarPerspectivasPorSituacao(string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<Perspectiva>();

            if (!String.IsNullOrEmpty(ativo))
                criteria.Add(Expression.Eq("ativo", Boolean.Parse(ativo)));

            return Page<Perspectiva>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}