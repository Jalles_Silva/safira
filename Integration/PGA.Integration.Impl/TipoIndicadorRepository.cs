﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class TipoIndicadorRepository : RepositoryBase<TipoIndicador>, ITipoIndicadorRepository<TipoIndicador>
    {
        public PageMessage<TipoIndicador> ListarTipoIndicadores(int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<TipoIndicador>();

            return Page<TipoIndicador>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}