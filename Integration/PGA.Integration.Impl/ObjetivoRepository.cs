﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class ObjetivoRepository : RepositoryBase<Objetivo>, IObjetivoRepository<Objetivo>
    {
        public PageMessage<Objetivo> ListarObjetivosPorTipo(int tipoObjetivo, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<Objetivo>();

            if (tipoObjetivo > 0)
                criteria.Add(Expression.Eq("tipoObjetivo.codigoSeqTipoObjetivo", tipoObjetivo));

            if (!String.IsNullOrEmpty(ativo))
                criteria.Add(Expression.Eq("ativo", Boolean.Parse(ativo)));

            return Page<Objetivo>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}