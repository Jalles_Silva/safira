﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class ControleAcaoRepository : RepositoryBase<ControleAcao>, IControleAcaoRepository<ControleAcao>
    {
        public PageMessage<ControleAcao> ListarControlesPorAcao(int codigoAcao, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<ControleAcao>()
                .CreateAlias("acao", "a")
                .Add(Expression.Eq("a.codigoSeqAcao", codigoAcao));

            return Page<ControleAcao>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}