﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class TipoValorRepository : RepositoryBase<TipoValor>, ITipoValorRepository<TipoValor>
    {
        public PageMessage<TipoValor> ListarTipoValores(int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<TipoValor>();

            return Page<TipoValor>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}