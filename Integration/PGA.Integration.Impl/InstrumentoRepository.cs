﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class InstrumentoRepository : RepositoryBase<Instrumento>, IInstrumentoRepository<Instrumento>
    {
        public PageMessage<Instrumento> ListarInstrumentoPorTipoEUnidade(int tipoInstrumento, int codigoUnidade, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<Instrumento>();

            if (tipoInstrumento > 0)
                criteria.Add(Expression.Eq("tipoInstrumento.codigoSeqTipoInstrumento", tipoInstrumento));

            if (codigoUnidade > 0)
                criteria.Add(Expression.Eq("corporativoUnidade.codigoUnidade", codigoUnidade));
            else if (codigoUnidade < 0)
                criteria.Add(Expression.IsNull("corporativoUnidade"));

            if (!String.IsNullOrEmpty(ativo))
                criteria.Add(Expression.Eq("ativo", Boolean.Parse(ativo)));

            return Page<Instrumento>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}
