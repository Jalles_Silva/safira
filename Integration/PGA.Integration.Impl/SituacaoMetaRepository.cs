﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;
using PGA.Integration.Spec.ValueObjects;

namespace PGA.Integration.Impl
{
    public class SituacaoMetaRepository : RepositoryBase<Domain.Entities.SituacaoMeta>, ISituacaoMetaRepository<SituacaoMeta>
    {
        public IList<SituacaoMeta> GetSituacaoMeta(int codSeqMeta)
        {
            try
            {

                var criteria = DetachedCriteria.For<SituacaoMeta>()
                .CreateAlias("meta", "m");

                criteria.Add(Expression.Eq("m.codSeqMeta", codSeqMeta));
                criteria.AddOrder(Order.Desc("codigoSeqSituacaoMeta"));
                
                return List<SituacaoMeta>(criteria);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
  
    }
}