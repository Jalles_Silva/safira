﻿using System.Collections.Generic;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;
using PGA.Integration.Spec.ValueObjects;
using NHibernate;
using System.Collections;
using NHibernate.Transform;

namespace PGA.Integration.Impl
{
    public class ResultadoAlcancadoRepository : RepositoryBase<ResultadoAlcancado>, IResultadoAlcancadoRepository<ResultadoAlcancado>
    {
        public PageMessage<VOResultadosAlcancados> ListarResultadosAlcancados(int codigoUnidade, int AnoExercicio, int startIndex, int pageSize)
        {
            string sql = @"SELECT {0} FROM (
                            SELECT DISTINCT 'Objetivo estratégico' DS_TIPO, OPE.DS_OBJETIVO DS_OBJETIVO_INICIATIVA, 
                                    EPE.NU_ANO_EXERCICIO , CUPE.DS_UNIDADE, CUPE.CD_UNIDADE , OPE.CD_SEQ_OBJETIVO CD_SEQ_OBJETIVO_INICIATIVA, 1 CD_TIPO,
                                    CASE WHEN RAPE.CD_SEQ_EXERCICIO IS NOT NULL THEN 'Sim' ELSE 'Não' END DS_RESULTADO_ALCANCADO_REGISTRADO,
                                    RAPE.CD_SEQ_RESULTADO_ALCANCADO, FPE.CD_SEQ_FASE, MPE.CD_SEQ_META
	                            FROM TB_META MPE 
                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = MPE.CD_SEQ_META
									INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
									INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
                                    INNER JOIN TB_CORPORATIVO_UNIDADE CUPE ON CUPE.CD_UNIDADE = MPE.CD_UNIDADE
	                                INNER JOIN TB_EXERCICIO EPE ON MPE.CD_EXERCICIO = EPE.CD_SEQ_EXERCICIO
									INNER JOIN TB_DESCRICAO_FASE FPE ON FPE.CD_SEQ_FASE = EPE.CD_FASE
	                                INNER JOIN RL_INICIATIVA_OBJETIVO RIPE ON MPE.CD_INICIATIVA_PE = RIPE.CD_SEQ_INICIATIVA
	                                INNER JOIN TB_INICIATIVA IPE ON RIPE.CD_SEQ_INICIATIVA = IPE.CD_SEQ_INICIATIVA
	                                INNER JOIN TB_OBJETIVO OPE ON RIPE.CD_SEQ_OBJETIVO = OPE.CD_SEQ_OBJETIVO
                                    LEFT JOIN TB_RESULTADO_ALCANCADO RAPE ON OPE.CD_SEQ_OBJETIVO = RAPE.CD_SEQ_OBJETIVO 
										AND EPE.CD_SEQ_EXERCICIO = RAPE.CD_SEQ_EXERCICIO
										AND CUPE.CD_UNIDADE = RAPE.CD_UNIDADE
                                WHERE OPE.CD_TIPO_OBJETIVO = 4 AND IPE.CD_TIPO_INICIATIVA = 1 
                            UNION
                            SELECT DISTINCT 'Objetivo estratégico' DS_TIPO, OPPA.DS_OBJETIVO DS_OBJETIVO_INICIATIVA, 
                                    EPPA.NU_ANO_EXERCICIO  , CUPA.DS_UNIDADE , CUPA.CD_UNIDADE, OPPA.CD_SEQ_OBJETIVO CD_SEQ_OBJETIVO_INICIATIVA, 1 CD_TIPO,
									CASE WHEN RAPPA.CD_SEQ_EXERCICIO IS NOT NULL THEN 'Sim' ELSE 'Não' END DS_RESULTADO_ALCANCADO_REGISTRADO,
                                    RAPPA.CD_SEQ_RESULTADO_ALCANCADO, FPPA.CD_SEQ_FASE, MPPA.CD_SEQ_META
	                                FROM TB_META MPPA
                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = MPPA.CD_SEQ_META
									INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
									INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                INNER JOIN TB_CORPORATIVO_UNIDADE CUPA ON CUPA.CD_UNIDADE = MPPA.CD_UNIDADE
	                                INNER JOIN TB_EXERCICIO EPPA ON MPPA.CD_EXERCICIO = EPPA.CD_SEQ_EXERCICIO
								    INNER JOIN TB_DESCRICAO_FASE FPPA ON FPPA.CD_SEQ_FASE = EPPA.CD_FASE
	                                INNER JOIN RL_INICIATIVA_OBJETIVO RIPA ON MPPA.CD_INICIATIVA_PPA = RIPA.CD_SEQ_INICIATIVA
	                                INNER JOIN TB_INICIATIVA IPPA ON RIPA.CD_SEQ_INICIATIVA = IPPA.CD_SEQ_INICIATIVA
	                                INNER JOIN TB_OBJETIVO OPPA ON RIPA.CD_SEQ_OBJETIVO = OPPA.CD_SEQ_OBJETIVO
                                    LEFT JOIN TB_RESULTADO_ALCANCADO RAPPA ON OPPA.CD_SEQ_OBJETIVO = RAPPA.CD_SEQ_OBJETIVO 
										AND EPPA.CD_SEQ_EXERCICIO = RAPPA.CD_SEQ_EXERCICIO
										AND CUPA.CD_UNIDADE = RAPPA.CD_UNIDADE
                            WHERE OPPA.CD_TIPO_OBJETIVO = 4 AND IPPA.CD_TIPO_INICIATIVA = 2 
                            UNION
                            SELECT DISTINCT 'Missão Institucional' DS_TIPO, MI.DS_INICIATIVA DS_OBJETIVO_INICIATIVA, 
                                    EMI.NU_ANO_EXERCICIO , CUMI.DS_UNIDADE ,CUMI.CD_UNIDADE, MI.CD_SEQ_INICIATIVA CD_SEQ_OBJETIVO_INICIATIVA, 2 CD_TIPO,
									CASE WHEN RAMI.CD_SEQ_INICIATIVA IS NOT NULL THEN 'Sim' ELSE 'Não' END DS_RESULTADO_ALCANCADO_REGISTRADO,
                                    RAMI.CD_SEQ_RESULTADO_ALCANCADO, FMI.CD_SEQ_FASE, MMI.CD_SEQ_META
	                                FROM TB_META MMI
                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = MMI.CD_SEQ_META
									INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
									INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
	                                INNER JOIN TB_CORPORATIVO_UNIDADE CUMI ON CUMI.CD_UNIDADE = MMI.CD_UNIDADE
	                                INNER JOIN TB_EXERCICIO EMI ON MMI.CD_EXERCICIO = EMI.CD_SEQ_EXERCICIO
								    INNER JOIN TB_DESCRICAO_FASE FMI ON FMI.CD_SEQ_FASE = EMI.CD_FASE
	                                INNER JOIN TB_INICIATIVA MI ON MMI.CD_INICIATIVA_MISSAO = MI.CD_SEQ_INICIATIVA
                                    LEFT JOIN TB_RESULTADO_ALCANCADO RAMI ON  MI.CD_SEQ_INICIATIVA = RAMI.CD_SEQ_INICIATIVA 
										AND EMI.CD_SEQ_EXERCICIO = RAMI.CD_SEQ_EXERCICIO 
										AND CUMI.CD_UNIDADE = RAMI.CD_UNIDADE
                            WHERE MI.CD_TIPO_INICIATIVA = 3 ) AS ResultadoAlcancado {1} {2}";

            string sqlWhere = "WHERE 1 = 1 ";

            string retorno = @" CD_SEQ_OBJETIVO_INICIATIVA as CodSeqObjetivoIniciativa, 
                                CD_TIPO as CodTipo,
                                DS_TIPO as DescricaoTipo, 
                                DS_OBJETIVO_INICIATIVA as DescricaoObjetivoIniciativa,
                                NU_ANO_EXERCICIO as AnoExcercicio,
                                DS_UNIDADE as DescricaoUnidade,
                                CD_UNIDADE as CodigoUnidade,
                                DS_RESULTADO_ALCANCADO_REGISTRADO AS ResultadoAlcancadoRegistrado,
                                CD_SEQ_RESULTADO_ALCANCADO as CodSeqResultadoAlcancado,
                                CD_SEQ_FASE as CodFase,
                                CD_SEQ_META as CodMeta";

            if (codigoUnidade > 0)
                sqlWhere += " AND CD_UNIDADE = " + codigoUnidade;

            if (AnoExercicio > 0)
                sqlWhere += " AND NU_ANO_EXERCICIO = " + AnoExercicio;


            ISQLQuery sqlQuery = Session.CreateSQLQuery(string.Format(sql, retorno, sqlWhere, " ORDER BY DS_UNIDADE ASC , DS_OBJETIVO_INICIATIVA ASC "));
            ISQLQuery sqlQueryCount = Session.CreateSQLQuery(string.Format(sql, "count(1)", sqlWhere, " "));

            PageMessage pageMessage = new PageMessage();
            pageMessage.StartIndex = startIndex;
            pageMessage.PageSize = pageSize;

            return Page<VOResultadosAlcancados>(sqlQuery, sqlQueryCount, pageMessage);
        }

        public PageMessage<VOMetaResultadosAlcancados> ListaMetasPorObjetivoIniciativa(int codObjetivoIniciativa, short anoExercicio, int codTipo, int codUnidade, int startIndex, int pageSize)
        {
            string sql = @"	SELECT {0} FROM 
								(SELECT DISTINCT OPE.CD_SEQ_OBJETIVO CD_SEQ_OBJETIVO_INICIATIVA, EPE.NU_ANO_EXERCICIO, MPE.CD_UNIDADE, 
                                           MPE.CD_SEQ_META, MPE.DS_META, TPPE.DS_TIPO_META, MPE.DS_RESULTADO_ESPERADO, 
                                           CASE WHEN MPE.ST_INTERNA = 1 THEN 'Sim' ELSE 'Não' END ST_CONSTANTE_PGA, 1 CD_TIPO, 
                                           CASE WHEN MPE.CD_INICIATIVA_PE IS NOT NULL AND MPE.CD_INICIATIVA_PPA IS NOT NULL THEN 'PPA/PE' 
					                            WHEN MPE.CD_INICIATIVA_PE IS NOT NULL AND MPE.CD_INICIATIVA_PPA IS NULL THEN 'PE' 
					                            WHEN MPE.CD_INICIATIVA_MISSAO IS NOT NULL THEN 'PPA' END DS_ALINHAMENTO
                                    FROM   TB_META MPE 
                                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = MPE.CD_SEQ_META
									        INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
									        INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
                                            INNER JOIN TB_TIPO_META TPPE ON MPE.CD_TIPO_META = TPPE.CD_SEQ_TIPO_META 
                                            INNER JOIN TB_CORPORATIVO_UNIDADE CUPE ON CUPE.CD_UNIDADE = MPE.CD_UNIDADE 
                                            INNER JOIN TB_EXERCICIO EPE ON MPE.CD_EXERCICIO = EPE.CD_SEQ_EXERCICIO 
                                            INNER JOIN RL_INICIATIVA_OBJETIVO RIPE ON MPE.CD_INICIATIVA_PE = RIPE.CD_SEQ_INICIATIVA 
                                            INNER JOIN TB_INICIATIVA IPE ON RIPE.CD_SEQ_INICIATIVA = IPE.CD_SEQ_INICIATIVA 
                                            INNER JOIN TB_OBJETIVO OPE ON RIPE.CD_SEQ_OBJETIVO = OPE.CD_SEQ_OBJETIVO 
                                    WHERE  OPE.CD_TIPO_OBJETIVO = 4 AND IPE.CD_TIPO_INICIATIVA = 1 
                                    UNION 
                                    SELECT DISTINCT OPPA.CD_SEQ_OBJETIVO CD_SEQ_OBJETIVO_INICIATIVA, EPPA.NU_ANO_EXERCICIO, MPPA.CD_UNIDADE, 
                                           MPPA.CD_SEQ_META, MPPA.DS_META, TPPPA.DS_TIPO_META, MPPA.DS_RESULTADO_ESPERADO, 
                                           CASE WHEN MPPA.ST_INTERNA = 1 THEN 'Sim' ELSE 'Não'END ST_CONSTANTE_PGA, 1 CD_TIPO, 
                                           CASE WHEN MPPA.CD_INICIATIVA_PE IS NOT NULL AND MPPA.CD_INICIATIVA_PPA IS NOT NULL THEN 'PPA/PE' 
					                            WHEN MPPA.CD_INICIATIVA_PE IS NOT NULL AND MPPA.CD_INICIATIVA_PPA IS NULL THEN 'PE' 
					                            WHEN MPPA.CD_INICIATIVA_MISSAO IS NOT NULL THEN 'PPA' END DS_ALINHAMENTO
                                    FROM   TB_META MPPA 
                                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = MPPA.CD_SEQ_META
									        INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
									        INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
                                            INNER JOIN TB_TIPO_META TPPPA ON MPPA.CD_TIPO_META = TPPPA.CD_SEQ_TIPO_META 
                                            INNER JOIN TB_CORPORATIVO_UNIDADE CUPA ON CUPA.CD_UNIDADE = MPPA.CD_UNIDADE 
                                            INNER JOIN TB_EXERCICIO EPPA ON MPPA.CD_EXERCICIO = EPPA.CD_SEQ_EXERCICIO 
                                            INNER JOIN RL_INICIATIVA_OBJETIVO RIPA ON MPPA.CD_INICIATIVA_PPA = RIPA.CD_SEQ_INICIATIVA 
                                            INNER JOIN TB_INICIATIVA IPPA ON RIPA.CD_SEQ_INICIATIVA = IPPA.CD_SEQ_INICIATIVA 
                                            INNER JOIN TB_OBJETIVO OPPA ON RIPA.CD_SEQ_OBJETIVO = OPPA.CD_SEQ_OBJETIVO 
                                    WHERE  OPPA.CD_TIPO_OBJETIVO = 4 AND IPPA.CD_TIPO_INICIATIVA = 2 
                                    UNION 
									SELECT DISTINCT MI.CD_SEQ_INICIATIVA CD_SEQ_OBJETIVO_INICIATIVA, EMI.NU_ANO_EXERCICIO, MMI.CD_UNIDADE, 
                                           MMI.CD_SEQ_META, MMI.DS_META, TPMMI.DS_TIPO_META, MMI.DS_RESULTADO_ESPERADO, 
                                           CASE WHEN MMI.ST_INTERNA = 1 THEN 'Sim' ELSE 'Não' END ST_CONSTANTE_PGA, 2 CD_TIPO, 
                                           CASE WHEN MMI.CD_INICIATIVA_PE IS NOT NULL AND MMI.CD_INICIATIVA_PPA IS NOT NULL THEN 'PPA/PE' 
					                            WHEN MMI.CD_INICIATIVA_PE IS NOT NULL AND MMI.CD_INICIATIVA_PPA IS NULL THEN 'PE' 
					                            WHEN MMI.CD_INICIATIVA_MISSAO IS NOT NULL THEN 'PPA' END DS_ALINHAMENTO
                                    FROM   TB_META MMI 
                                            INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = MMI.CD_SEQ_META
									        INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
									        INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
                                            INNER JOIN TB_TIPO_META TPMMI ON MMI.CD_TIPO_META = TPMMI.CD_SEQ_TIPO_META 
                                            INNER JOIN TB_CORPORATIVO_UNIDADE CUMI ON CUMI.CD_UNIDADE = MMI.CD_UNIDADE 
                                            INNER JOIN TB_EXERCICIO EMI ON MMI.CD_EXERCICIO = EMI.CD_SEQ_EXERCICIO 
                                            INNER JOIN TB_INICIATIVA MI ON MMI.CD_INICIATIVA_MISSAO = MI.CD_SEQ_INICIATIVA 
                                    WHERE  MI.CD_TIPO_INICIATIVA = 3) AS ResultadoAlcancado {1} ";

            string retorno = @" CD_SEQ_META as CodigoMeta,
                                DS_META as DescricaoMeta, 
                                CD_TIPO as TipoMeta,
                                DS_TIPO_META as DescricaoTipo, 
                                DS_RESULTADO_ESPERADO as ResultadoEsperadoMeta,
                                ST_CONSTANTE_PGA as ContantePGA,
								DS_ALINHAMENTO as DescricaoAlinhamento,
                                NULL as Executado,
                                NULL as DesempenhoIndicador ";

            string sqlWhere = "WHERE 1 = 1 ";

            if (codObjetivoIniciativa > 0)
                sqlWhere += " AND CD_SEQ_OBJETIVO_INICIATIVA = " + codObjetivoIniciativa;

            if (anoExercicio > 0)
                sqlWhere += " AND NU_ANO_EXERCICIO = " + anoExercicio;

            if (codTipo > 0)
                sqlWhere += " AND CD_TIPO = " + codTipo;

            if (codUnidade > 0)
                sqlWhere += " AND CD_UNIDADE = " + codUnidade;

            ISQLQuery sqlQuery = Session.CreateSQLQuery(string.Format(sql, retorno, sqlWhere, "GROUP BY CD_SEQ_META, DS_META, CD_TIPO, DS_TIPO_META, DS_RESULTADO_ESPERADO, ST_CONSTANTE_PGA, DS_ALINHAMENTO ORDER BY DS_TIPO_META ASC"));
            ISQLQuery sqlQueryCount = Session.CreateSQLQuery(string.Format(sql, "count(1)", sqlWhere, ""));

            PageMessage pageMessage = new PageMessage();
            pageMessage.StartIndex = startIndex;
            pageMessage.PageSize = pageSize;

            return Page<VOMetaResultadosAlcancados>(sqlQuery, sqlQueryCount, pageMessage);
        }

        public IList<VOResultadosAlcancados> ListarResultadosAlcancadosPorUnidade(int codigoUnidade, int AnoExercicio)
        {
            string sql = @"SELECT NU_ANO_EXERCICIO as AnoExcercicio, DS_UNIDADE as DescricaoUnidade, CD_UNIDADE as CodigoUnidade, CD_SEQ_RESULTADO_ALCANCADO as CodSeqResultadoAlcancado 
                            FROM (SELECT DISTINCT 'Objetivo estratégico' DS_TIPO,  EPE.NU_ANO_EXERCICIO , CUPE.DS_UNIDADE, CUPE.CD_UNIDADE , RAPE.CD_SEQ_RESULTADO_ALCANCADO
	                            FROM TB_META MPE 
                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = MPE.CD_SEQ_META
                                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
                                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
                                    INNER JOIN TB_CORPORATIVO_UNIDADE CUPE ON CUPE.CD_UNIDADE = MPE.CD_UNIDADE
                                    INNER JOIN TB_EXERCICIO EPE ON MPE.CD_EXERCICIO = EPE.CD_SEQ_EXERCICIO
                                    INNER JOIN RL_INICIATIVA_OBJETIVO RIPE ON MPE.CD_INICIATIVA_PE = RIPE.CD_SEQ_INICIATIVA
                                    INNER JOIN TB_INICIATIVA IPE ON RIPE.CD_SEQ_INICIATIVA = IPE.CD_SEQ_INICIATIVA
                                    INNER JOIN TB_OBJETIVO OPE ON RIPE.CD_SEQ_OBJETIVO = OPE.CD_SEQ_OBJETIVO
                                    LEFT JOIN TB_RESULTADO_ALCANCADO RAPE ON OPE.CD_SEQ_OBJETIVO = RAPE.CD_SEQ_OBJETIVO 
										AND EPE.CD_SEQ_EXERCICIO = RAPE.CD_SEQ_EXERCICIO
										AND CUPE.CD_UNIDADE = RAPE.CD_UNIDADE
                                WHERE OPE.CD_TIPO_OBJETIVO = 4 AND IPE.CD_TIPO_INICIATIVA = 1 
                            UNION
                            SELECT DISTINCT 'Objetivo estratégico' DS_TIPO, EPPA.NU_ANO_EXERCICIO  , CUPA.DS_UNIDADE , CUPA.CD_UNIDADE, RAPPA.CD_SEQ_RESULTADO_ALCANCADO
	                            FROM TB_META MPPA
                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = MPPA.CD_SEQ_META
                                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
                                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
                                    INNER JOIN TB_CORPORATIVO_UNIDADE CUPA ON CUPA.CD_UNIDADE = MPPA.CD_UNIDADE
                                    INNER JOIN TB_EXERCICIO EPPA ON MPPA.CD_EXERCICIO = EPPA.CD_SEQ_EXERCICIO
                                    INNER JOIN RL_INICIATIVA_OBJETIVO RIPA ON MPPA.CD_INICIATIVA_PPA = RIPA.CD_SEQ_INICIATIVA
                                    INNER JOIN TB_INICIATIVA IPPA ON RIPA.CD_SEQ_INICIATIVA = IPPA.CD_SEQ_INICIATIVA
                                    INNER JOIN TB_OBJETIVO OPPA ON RIPA.CD_SEQ_OBJETIVO = OPPA.CD_SEQ_OBJETIVO
                                    LEFT JOIN TB_RESULTADO_ALCANCADO RAPPA ON OPPA.CD_SEQ_OBJETIVO = RAPPA.CD_SEQ_OBJETIVO 
										AND EPPA.CD_SEQ_EXERCICIO = RAPPA.CD_SEQ_EXERCICIO
										AND CUPA.CD_UNIDADE = RAPPA.CD_UNIDADE
                            WHERE OPPA.CD_TIPO_OBJETIVO = 4 AND IPPA.CD_TIPO_INICIATIVA = 2 
                            UNION
                            SELECT DISTINCT 'Missão Institucional' DS_TIPO, EMI.NU_ANO_EXERCICIO , CUMI.DS_UNIDADE ,CUMI.CD_UNIDADE, RAMI.CD_SEQ_RESULTADO_ALCANCADO
	                            FROM TB_META MMI
                                    INNER JOIN (SELECT CD_META, MAX(CD_SEQ_SITUACAO_META) AS CODIGO FROM TB_SITUACAO_META GROUP BY CD_META) STATUSATUAL ON STATUSATUAL.CD_META = MMI.CD_SEQ_META
                                    INNER JOIN TB_SITUACAO_META S ON STATUSATUAL.CODIGO = S.CD_SEQ_SITUACAO_META
                                    INNER JOIN TB_STATUS_META ST ON ST.CD_SEQ_STATUS_META = S.CD_STATUS_META AND ST.CD_SEQ_STATUS_META = 3 
                                    INNER JOIN TB_CORPORATIVO_UNIDADE CUMI ON CUMI.CD_UNIDADE = MMI.CD_UNIDADE
                                    INNER JOIN TB_EXERCICIO EMI ON MMI.CD_EXERCICIO = EMI.CD_SEQ_EXERCICIO
                                    INNER JOIN TB_INICIATIVA MI ON MMI.CD_INICIATIVA_MISSAO = MI.CD_SEQ_INICIATIVA
                                    LEFT JOIN TB_RESULTADO_ALCANCADO RAMI ON MI.CD_SEQ_INICIATIVA = RAMI.CD_SEQ_INICIATIVA 
										AND EMI.CD_SEQ_EXERCICIO = RAMI.CD_SEQ_EXERCICIO 
										AND CUMI.CD_UNIDADE = RAMI.CD_UNIDADE
                            WHERE MI.CD_TIPO_INICIATIVA = 3 ) AS ResultadoAlcancado {0} ORDER BY CD_SEQ_RESULTADO_ALCANCADO DESC";

            string sqlWhere = "WHERE 1 = 1 ";

            if (codigoUnidade > 0)
                sqlWhere += " AND CD_UNIDADE = " + codigoUnidade;

            if (AnoExercicio > 0)
                sqlWhere += " AND NU_ANO_EXERCICIO = " + AnoExercicio;

            var result = Session.CreateSQLQuery(string.Format(sql, sqlWhere)).SetResultTransformer(Transformers.AliasToEntityMap).List();

            var listaResultadosAlcancados = new List<VOResultadosAlcancados>();
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    var asHash = item as Hashtable;
                    var resultadoAlcancado = new VOResultadosAlcancados()
                    {
                        AnoExcercicio = int.Parse(asHash["AnoExcercicio"].ToString()),
                        DescricaoUnidade = (string)asHash["DescricaoUnidade"],
                        CodigoUnidade = (int)asHash["CodigoUnidade"],
                        CodSeqResultadoAlcancado = asHash["CodSeqResultadoAlcancado"] != null ? int.Parse(asHash["CodSeqResultadoAlcancado"].ToString()) : 0
                    };

                    listaResultadosAlcancados.Add(resultadoAlcancado);
                }
            }

            return listaResultadosAlcancados;
        }
    }
}