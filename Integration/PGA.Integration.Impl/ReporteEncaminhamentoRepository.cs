﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class ReporteEncaminhamentoRepository : RepositoryBase<ReporteEncaminhamento>, IReporteEncaminhamentoRepository<ReporteEncaminhamento>
    {
        public PageMessage<ReporteEncaminhamento> ListarReportesPorEncaminhamentos(int codigoSeqEncaminhamento, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<ReporteEncaminhamento>();

            criteria.Add(Expression.Eq("encaminhamento.codigoSeqEncaminhamento", codigoSeqEncaminhamento));

            return Page<ReporteEncaminhamento>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}
