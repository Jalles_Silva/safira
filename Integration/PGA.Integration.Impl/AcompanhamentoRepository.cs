﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class AcompanhamentoRepository : RepositoryBase<Acompanhamento>, IAcompanhamentoRepository<Acompanhamento>
    {
        public PageMessage<Acompanhamento> ListarAcompanhamentosPorTipoAnoEStatus(int tipoAcompanhamento, short ano, int status, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var criteria = DetachedCriteria.For<Acompanhamento>()
                .CreateAlias("exercicio", "e");

            if (tipoAcompanhamento > 0)
                criteria.Add(Expression.Eq("tipoAcompanhamento.codigoSeqTipoAcompanhamento", tipoAcompanhamento));

            if (status > 0)
                criteria.Add(Expression.Eq("statusAcompanhamento.codigoSeqStatusAcompanhamento", status));

            if (ano > 0)
                criteria.Add(Expression.Eq("e.ano", ano));

            return Page<Acompanhamento>(criteria, startIndex, pageSize, orderProperty, orderAscending);
        }
    }
}
