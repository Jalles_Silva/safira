﻿using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Data.Pagging;
using SQFramework.Spring.Data.Hibernate;

namespace PGA.Integration.Impl
{
    public class CorporativoUnidadeRepository : RepositoryBase<CorporativoUnidade>, ICorporativoUnidadeRepository<CorporativoUnidade>
    {
        public IList<CorporativoUnidade> ListarCorporativoUnidadeAtivo()
        {
            var criteria = DetachedCriteria.For<CorporativoUnidade>();
            criteria.Add(Expression.Eq("ativo", true));
            criteria.AddOrder(Order.Asc("descricaoUnidade"));

            return List<CorporativoUnidade>(criteria);
        }

        public IList<CorporativoUnidade> ListarCorporativoUnidadePorExercicio(short ano)
        {
            var criteria = DetachedCriteria.For<CorporativoUnidade>("u")
                .CreateAlias("u.metas", "m")
                .CreateAlias("m.exercicio", "e")
                .AddOrder(Order.Asc("u.descricaoUnidade"));

            if (ano > 0)
                criteria.Add(Expression.Eq("e.ano", ano));

            criteria.SetResultTransformer(new NHibernate.Transform.DistinctRootEntityResultTransformer());

            return List<CorporativoUnidade>(criteria);
        }

        public IList<CorporativoUnidade> ListarCorporativoUnidadePorCodigos(List<int> codigos)
        {
            var criteria = DetachedCriteria.For<CorporativoUnidade>()
                .Add(Expression.In("codigoUnidade", codigos));

            return List<CorporativoUnidade>(criteria);
        }
    }
}