﻿using PGA.Domain.Entities;
using PGA.Integration.Spec;
using SQFramework.Spring.Data.Hibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Impl
{
    public class TipoAnexoRepository : RepositoryBase<TipoAnexo>, ITipoAnexoRepository<TipoAnexo>
    {
    }
}
