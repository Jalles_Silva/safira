﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;

namespace PGA.Integration.Spec
{
    [ObjectMap("IniciativaObjetivoRepository", true)]
    public interface IIniciativaObjetivoRepository<T> : IRepositoryBase<T>
    {
        PageMessage<T> ListarObjetivosPorIniciativa(int CodigoSeqIniciativa, int startIndex, int pageSize, string orderProperty, bool orderAscending);
    }
}
