﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;
using PGA.Integration.Spec.ValueObjects;

namespace PGA.Integration.Spec
{
    [ObjectMap("AcaoRepository", true)]
    public interface IAcaoRepository<T> : IRepositoryBase<T>
    {
        PageMessage<T> ListarAcoesPorMeta(int codigoSeqMeta, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        PageMessage<VOAcao> ListarAcoesPorFiltros(VOFiltroControle filtro);

        PageMessage<VOAcaoRelatorio> ListaGlobalAtividade(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize);

        PageMessage<VOAcaoRelatorio> ListaDetalheAtividade(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize);
    }
}