﻿using SQFramework.Spring;
using SQFramework.Data;
using System.Collections.Generic;
using PGA.Integration.Spec.ValueObjects;

namespace PGA.Integration.Spec
{
    [ObjectMap("RelatorioConsubstanciadoRepository", true)]
    public interface IRelatorioConsubstanciadoRepository<T> : IRepositoryBase<T>
    {
        IList<T> ListarRelatorioConsubstanciado(int codigoSeqExercicio);

        IList<VOResultadosAlcancadosPorExercicio> ListarResultadosAlcancadosPorExercicio(int anoExercicio);

        IList<VOObjetivos> ListarOjetivoEstrategicoEMissaoAdministrativa();

        IList<VOObjetivoEstrategicoTotalizadorDetalhadado> ListarObjetivoEstrategicoTotal(int anoExercicio, int codObjetivo, int codTipo);

        IList<VORelatorioObjetivoEstrategico> ListarObjetivoEstrategico(int anoExercicio);

        IList<VORelatorioObjetivoEstrategico> ListarObjetivoEstrategicoPE(int anoExercicio, int codSeqObjectivo);

        IList<VORelatorioObjetivoEstrategico> ListarObjetivoEstrategicoPPA(int anoExercicio, int codSeqObjectivo);

        IList<VORelatorioObjetivoEstrategico> ListarMeta(int anoExercicio, int codTipoMeta);

        IList<VORelatorioObjetivoEstrategico> ListarInstrumentos(int anoExercicio, int codTipoInstrumento);

        IList<VORelatorioObjetivoEstrategico> ListarIniciativa(int anoExercicio, int codTipoIniciativa);

        IList<VOObjetivos> ListarOjetivoEstrategico(int codTipoInciativa);

        IList<VOObjetivosDetalhado> ListarObjetivosEstrategicoDetalhado(int anoExercicio, int codObjetivo, int codTipo);

        IList<VORecursosTotais> ObterValorDeRecurso(int codSeqMeta, int anoExercicio);

        IList<VOResultadoIndicadorAcao> ListaResultadoIndicador(int codigoMeta);

        IList<VOResultadoIndicadorAcao> ListaResultadoAtividade(int codigoMeta);
    }
}