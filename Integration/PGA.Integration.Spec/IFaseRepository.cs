﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;

namespace PGA.Integration.Spec
{
    [ObjectMap("FaseRepository", true)]
    public interface IFaseRepository<T> : IRepositoryBase<T>
    {
        T ObterProximaFase(Int16 ordemFaseAtual);
    }
}