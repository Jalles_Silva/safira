﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;

namespace PGA.Integration.Spec
{
    [ObjectMap("CorporativoUnidadeRepository", true)]
    public interface ICorporativoUnidadeRepository<T> : IRepositoryBase<T>
    {
        IList<T> ListarCorporativoUnidadeAtivo();
        IList<T> ListarCorporativoUnidadePorExercicio(short ano);
        IList<T> ListarCorporativoUnidadePorCodigos(List<int> codigos);
    }
}