﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;

namespace PGA.Integration.Spec
{
    [ObjectMap("PerspectivaRepository", true)]
    public interface IPerspectivaRepository<T> : IRepositoryBase<T>
    {
        PageMessage<T> ListarPerspectivasPorSituacao(string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending);
    }
}