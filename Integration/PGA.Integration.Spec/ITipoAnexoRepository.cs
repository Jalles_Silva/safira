﻿using SQFramework.Data;
using SQFramework.Spring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Spec
{
    [ObjectMap("TipoAnexoRepository", true)]
    public interface ITipoAnexoRepository<T> : IRepositoryBase<T>
    {
    }
}
