﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;

namespace PGA.Integration.Spec
{
    [ObjectMap("ExercicioRepository", true)]
    public interface IExercicioRepository<T> : IRepositoryBase<T>
    {
        PageMessage<T> ListarExercicios(int startIndex, int pageSize, string orderProperty, bool orderAscending);

        T ObterExercicioPorAno(Int16 ano);
        T ObterExercicioAtual();
        T ObterExercicioEmPlanejamento();
        T ObterUltimoExercicioEmPlanejamento();
        T ObterExercicioEmAcompanhamento();
        IList<T> ListarExercicioEmAcompanhamento();
        IList<T> ListarExercicioEmPlanejamento();
        IList<T> ListarExercicioExcetoPlanejamento();
        IList<T> ListarAnoExercicio();
        T ObterFaseExercicioPorAno(Int16 ano);
    }
}