﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;

namespace PGA.Integration.Spec
{
    [ObjectMap("EncaminhamentoRepository", true)]
    public interface IEncaminhamentoRepository<T> : IRepositoryBase<T>
    {
        PageMessage<T> ListarEncaminhamentosPorAcompanhamento(int codigoSeqAcompanhamento, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        PageMessage<T> ListarEncaminhamentosPorTipoUnidadeEStatus(int tipoAcompanhamento, string unidade, int status, int startIndex, int pageSize, string orderProperty, bool orderAscending);
    }
}
