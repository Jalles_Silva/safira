﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;

namespace PGA.Integration.Spec
{
    [ObjectMap("TipoMetaRepository", true)]
    public interface ITipoMetaRepository<T> : IRepositoryBase<T>
    {
        PageMessage<T> ListarTipoMetas(int startIndex, int pageSize, string orderProperty, bool orderAscending);
    }
}