﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;

namespace PGA.Integration.Spec
{
    [ObjectMap("AcompanhamentoRepository", true)]
    public interface IAcompanhamentoRepository<T> : IRepositoryBase<T>
    {
        PageMessage<T> ListarAcompanhamentosPorTipoAnoEStatus(int tipoAcompanhamento, short ano, int status, int startIndex, int pageSize, string orderProperty, bool orderAscending);
    }
}
