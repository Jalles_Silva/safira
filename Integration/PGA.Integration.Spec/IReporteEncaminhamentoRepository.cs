﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;

namespace PGA.Integration.Spec
{
    [ObjectMap("ReporteEncaminhamentoRepository", true)]
    public interface IReporteEncaminhamentoRepository<T> : IRepositoryBase<T>
    {
        PageMessage<T> ListarReportesPorEncaminhamentos(int codigoSeqEncaminhamento, int startIndex, int pageSize, string orderProperty, bool orderAscending);
    }
}
