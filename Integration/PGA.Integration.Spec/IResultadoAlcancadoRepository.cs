﻿using System.Collections.Generic;
using SQFramework.Spring;
using SQFramework.Data;
using SQFramework.Data.Pagging;
using PGA.Integration.Spec.ValueObjects;

namespace PGA.Integration.Spec
{
    [ObjectMap("ResultadoAlcancadoRepository", true)]
    public interface IResultadoAlcancadoRepository<T> : IRepositoryBase<T>
    {
        PageMessage<VOResultadosAlcancados> ListarResultadosAlcancados(int codigoUnidade, int anoExercicio, int startIndex, int pageSize);

        PageMessage<VOMetaResultadosAlcancados> ListaMetasPorObjetivoIniciativa(int codObjetivoIniciativa, short anoExercicio, int codTipo, int codUnidade, int startIndex, int pageSize);

        IList<VOResultadosAlcancados> ListarResultadosAlcancadosPorUnidade(int codigoUnidade, int AnoExercicio);
    }
}