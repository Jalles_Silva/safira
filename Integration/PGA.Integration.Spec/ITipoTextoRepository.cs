﻿using SQFramework.Data;
using SQFramework.Spring;

namespace PGA.Integration.Spec
{
    [ObjectMap("TipoTextoRepository", true)]
    public interface ITipoTextoRepository<T> : IRepositoryBase<T>
    {

    }
}
