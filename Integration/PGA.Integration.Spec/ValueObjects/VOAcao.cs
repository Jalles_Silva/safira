﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Spec.ValueObjects
{
    [Serializable()]
    public class VOAcao
    {
        public int CodigoSeqAcao { get; set; }
        public string DescricaoAtividadeAcao { get; set; }
        public string DescricaoEstrategiaAcao { get; set; }
        public DateTime? DataInicioAcao { get; set; }
        public DateTime? DataFimAcao { get; set; }
        public string DescricaoMeta { get; set; }
        public DateTime? DataUltimoReporte { get; set; }
        public int? PercentualExecucao { get; set; }
        public int? AnoExercicio { get; set; }
        public string UnidadeCorporativa { get; set; }
        public int CodigoFaseExercicio { get; set; }
        public int CodigoUnidade { get; set; }
    }
}
