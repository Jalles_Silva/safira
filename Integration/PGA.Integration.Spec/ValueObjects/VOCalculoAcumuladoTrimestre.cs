﻿using PGA.Common;

namespace PGA.Integration.Spec.ValueObjects
{
    public class VOCalculoAcumuladoTrimestre
    {
        public EnumTipoResultado TipoResultado { get; set; }
        public int NumeroTrimestre { get; set; }
        public int TotalItens { get; set; }
        public decimal ValorAcumulado { get; set; }        
        public decimal ValorAlvo { get; set; }

        public string NomeTrimestre
        {
            get
            {
                return string.Format("{0}º Trimestre", NumeroTrimestre);
            }
        }

        public decimal ValorFinal
        {
            get
            {
                // O "TotalItens > 0" evita o erro aritimético de divisão por zero
                return TipoResultado == EnumTipoResultado.Media && TotalItens > 0
                    ? ValorAcumulado / TotalItens
                    : ValorAcumulado;
            }
        }

        public decimal SomatorioFinal { get; set; }

        public VOCalculoAcumuladoTrimestre(EnumTipoResultado tipo, int numeroTrimestre, decimal valorAlvo)
        {
            TipoResultado = tipo;
            NumeroTrimestre = numeroTrimestre;
            TotalItens = 0;
            ValorAcumulado = 0.0m;
            ValorAlvo = valorAlvo;
            SomatorioFinal = 0;
        }
    }
}
