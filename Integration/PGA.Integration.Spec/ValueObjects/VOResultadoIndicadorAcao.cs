﻿namespace PGA.Integration.Spec.ValueObjects
{
    public class VOResultadoIndicadorAcao
    {
        public decimal ResultadoIndicador { get; set; }
        public int CodSeqTipoValor { get; set; }
        public decimal ValorAlvo { get; set; }
        public int PercentualExecucao { get; set; }
    }
}
