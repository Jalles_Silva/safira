﻿namespace PGA.Integration.Spec.ValueObjects
{
    public class VOObjetivosDetalhado
    {
        public int CodSeqObjetivo { get; set; }

        public int CodSeqMeta { get; set; }

        public string DescricaoObjetivo { get; set; }

        public string DescricaoUnidade { get; set; }

        public int AnoExercicio { get; set; }

        public int CodTipo { get; set; }
    }
}
