﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Spec.ValueObjects
{
    public class VOMetaRelatorio
    {
        public string DescricaoArea { get; set; }
        public int CodigoUnidade { get; set; }
        public int AnoExcercicio { get; set; }
        public int QdtMetasCadastradas { get; set; }
        public int QtdMetasAlinhadasPE { get; set; }
        public int QtdMetasAlinhadasPPA { get; set; }
        public int QtdMetasAlinhadasMI { get; set; }
        public int QtdMetasAdministrativas { get; set; }
        public int QtdMetasFiscalizacoes { get; set; }
        public int QtdMetasRegulacoes { get; set; }
        public bool VerAlinhadasPE { get; set; }
        public bool VerAlinhadasPPA { get; set; }
        public bool VerAlinhadasMI { get; set; }
        public bool VerAdministrativas { get; set; }
        public bool VerFiscalizacoes { get; set; }
        public bool VerRegulacoes { get; set; }
        public string TipoMeta { get; set; }
        public string TipoAlinhamento { get; set; }
        public string DescricaoMeta { get; set; }
        public int? QtdMetasInternas { get; set; }
        public string MetaInterna { get; set; }
    }
}
