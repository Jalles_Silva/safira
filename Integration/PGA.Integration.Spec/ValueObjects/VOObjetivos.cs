﻿namespace PGA.Integration.Spec.ValueObjects
{
    public class VOObjetivos
    {
        public string CodSeqObjetivo { get; set; }
        public string DescricaoObjetivo { get; set; }
    }
}
