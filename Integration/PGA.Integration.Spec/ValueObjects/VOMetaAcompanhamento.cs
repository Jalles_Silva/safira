﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Spec.ValueObjects
{
    public class VOMetaAcompanhamento
    {
        public int CodigoMeta { get; set; }
        public string DescricaoMeta { get; set; }
        public string NomeArea { get; set; }
        public string NomeGestorEstrategico { get; set; }
        public string NomeCoordenador { get; set; }
        public string CorStatusGestaoRisco { get; set; }
        public string CorStatusAgendaRegulatoria { get; set; }
        public string CorStatusDesburocratizacao { get; set; }
        public string CorStatusIntegridade { get; set; }
        public string CorStatusIniciativaEstrategica { get; set; }
        public string CorStatusOutros { get; set; }
        public string StatusMetaInterna { get; set; }

        public List<VOIndicadorAcompanhamento> Indicadores { get; set; }

        public List<VOAcaoAcompanhamento> Acoes { get; set; }
    }
}