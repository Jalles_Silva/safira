﻿namespace PGA.Integration.Spec.ValueObjects
{
    public class VORelatorioConsubstanciadoDetalhado
    {
        public int CodSeqIniciativa { get; set; }
        public int AnoExercicio { get; set; }
        public int CodSeqMeta { get; set; }
        public int CodSeqIndicador { get; set; }
        public int CodSeqAcao { get; set; }
        public string DescricaoUnicade { get; set; }
    }
}
