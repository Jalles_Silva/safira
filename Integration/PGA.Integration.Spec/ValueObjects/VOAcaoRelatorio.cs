﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Spec.ValueObjects
{
    [Serializable()]
    public class VOAcaoRelatorio
    {
        public int CodigoSeqAcao { get; set; }
        public string DescricaoArea { get; set; }
        public int CodigoUnidade { get; set; }
        public int CodigoMeta { get; set; }
        public string DescricaoMeta { get; set; }
        public string DescricaoAtividade { get; set; }
        public string DescricaoEstrategia { get; set; }
        public int PercentualExecucao { get; set; }
        public decimal RecursoEstimado { get; set; }
        public decimal RecursoOrcamentarioUtilizado { get; set; }
        public DateTime? DataInicioAtividade { get; set; }
        public DateTime? DataFimAtividade { get; set; }
        public DateTime? DataUltimaAtualizacao { get; set; }

        public int TotalMetasCadastradas { get; set; }
        public int AtividadesCadastradasExercicio { get; set; }
        public int AtividadesCadastradasPeriodo { get; set; }
        public int AtividadesConcluidas { get; set; }
        public int AtividadesPeriodoPrazo { get; set; }
        public int AtividadesRecursoFinanceiro { get; set; }
        public int AnoExercicio { get; set; }
        public decimal RecursoFinanceiro { get; set; }
        public int AtividadesConcluidasPeriodo { get; set; }
        public int? QtdMetasInternas { get; set; }
        public string MetaInterna { get; set; }
    }
}
