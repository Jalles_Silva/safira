﻿using System.Collections.Generic;

namespace PGA.Integration.Spec.ValueObjects
{
    public class VORelatorioAcompanhamento
    {
        public string Periodo { get; set; }
        public string Exercicio { get; set; }
        public string TipoMetas { get; set; }
        public string TipoAlinhamentos { get; set; }
        public string TipoInstrumentos { get; set; }

        public List<VOMetaAcompanhamento> Metas { get; set; }

        public VORelatorioAcompanhamento()
        {
            Metas = new List<VOMetaAcompanhamento>();
        }
    }
}
