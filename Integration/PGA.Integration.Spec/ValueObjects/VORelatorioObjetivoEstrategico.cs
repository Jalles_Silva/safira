﻿namespace PGA.Integration.Spec.ValueObjects
{
    public class VORelatorioObjetivoEstrategico
    {
        public int CodSeqIniciativa { get; set; }
        public int CodSeqMeta { get; set; }
        public int CodSeqIndicador { get; set; }
        public int CodSeqAcao { get; set; }
        public string DescricaoUnicade { get; set; }
        public decimal PercentualExecucao { get; set; }
        public decimal ResultadoIndicador { get; set; }
        public string DescricaoTipo { get; set; }
    }
}
