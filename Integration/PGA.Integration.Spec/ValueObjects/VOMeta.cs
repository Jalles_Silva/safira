﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Spec.ValueObjects
{
    public class VOMeta
    {
        public int CodSeqMeta { get; set; }
        public string DescricaoMeta { get; set; }
        public string CodigoStatusMeta { get; set; }
        public string DescricaoStatusMeta { get; set; }
        public string DescricaoUnidade { get; set; }
        public string DescricaoTipoMeta { get; set; }
        public string DescricaoResultadoEsperado { get; set; }
        public int AnoExercicio { get; set; }
        public int CodigoUnidade { get; set; }
        public int CodigoFaseExercicio { get; set; }
        public bool StatusMetaInterna { get; set; }
        public int CodigoSeqIniciativaPE { get; set; }
    }
}
