﻿namespace PGA.Integration.Spec.ValueObjects
{
    public class VOObjetivoEstrategicoTotalizadorDetalhadado
    {
        public string DescricaoUnidade { get; set; }
        public string DescricaoObjetivo { get; set; }
        public int CodSeqObjetivo { get; set; }
        public int CodSeqIniciativa { get; set; }
        public int RecursoPrevisto { get; set; }
        public int ReporteRecursoUtilizado { get; set; }
    }
}
