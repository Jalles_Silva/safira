﻿namespace PGA.Integration.Spec.ValueObjects
{
    public class VOResultadosAlcancados
    {
        public int CodSeqObjetivoIniciativa { get; set; }
        public int CodTipo { get; set; }
        public string DescricaoTipo { get; set; }
        public string DescricaoObjetivoIniciativa { get; set; }
        public int AnoExcercicio { get; set; }
        public string DescricaoUnidade { get; set; }
        public int CodigoUnidade { get; set; }
        public string ResultadoAlcancadoRegistrado { get; set; }
        public int CodSeqResultadoAlcancado { get; set; }
        public int CodFase { get; set; }
        public int CodMeta { get; set; }
    }
}
