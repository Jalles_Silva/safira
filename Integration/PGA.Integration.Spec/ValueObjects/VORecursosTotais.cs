﻿namespace PGA.Integration.Spec.ValueObjects
{
    public class VORecursosTotais
    {
        public decimal TotalRecursoEstimado { get; set; }

        public decimal TotalRecursoOrcamentarioUtilizado { get; set; }
    }
}
