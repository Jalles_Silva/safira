﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Spec.ValueObjects
{
    [Serializable()]
    public class VOFiltroControle
    {
        public int CodigoMeta { get; set; }
        public int CodigoUnidade { get; set; }
        public string Unidade { get; set; }
        public string Status { get; set; }
        public int StartIndex { get; set; }
        public int PageSize { get; set; }
        public string OrderProperty { get; set; }
        public bool OrderAscending { get; set; }
        public int AnoExercicio { get; set; }
    }
}
