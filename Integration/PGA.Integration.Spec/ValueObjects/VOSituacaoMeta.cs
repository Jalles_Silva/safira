﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Spec.ValueObjects
{
    public class VOSituacaoMeta
    {
        public int codigoSeqSituacaoMeta { get; set; }
        public string noUsuario { get; set; }
        public int codSeqMeta { get; set; }
        public int codStatusMeta { get; set; }
        public DateTime dataAlteracao { get; set; }
        public string descricaoJustificativaCancelamento { get; set; }
        public int? codigoSeqAnexo { get; set; }


    }
}
