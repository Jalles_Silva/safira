﻿
namespace PGA.Integration.Spec.ValueObjects
{
    public class VOMetaResultadosAlcancados
    {
        public int CodigoMeta { get; set; }
        public string DescricaoMeta { get; set; }
        public string TipoMeta { get; set; }
        public string DescricaoTipo { get; set; }
        public string ResultadoEsperadoMeta { get; set; }
        public string ContantePGA { get; set; }
        public string DescricaoAlinhamento { get; set; }
        public string Executado { get; set; }
        public string DesempenhoIndicador { get; set; }
    }
}
