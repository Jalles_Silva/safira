﻿using System;

namespace PGA.Integration.Spec.ValueObjects
{
    public class VOComentario
    {
        public DateTime Data { get; set; }
        public string Fato { get; set; }
        public string Causa { get; set; }
        public string Acao { get; set; }
    }
}
