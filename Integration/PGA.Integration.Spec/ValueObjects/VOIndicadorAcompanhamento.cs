﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Spec.ValueObjects
{
    public class VOIndicadorAcompanhamento
    {
        public int CodigoIndicador { get; set; }
        public string DescricaoIndicador { get; set; }
        public string Status { get; set; }
        public string CorStatus { get; set; }
        public decimal ValorAlvo { get; set; }
        public string ValorAlvoFormatado { get; set; }
		public string ValorPeriodoFormatado { get; set; }
		public int ValorAcumuladoPeriodo { get; set; }
        public string ValorAcumuladoPeriodoFormatado { get; set; }
        public List<VOComentario> Comentarios { get; set; }
        public List<VOCalculoAcumuladoTrimestre> DadosGrafico { get; set; }
        public string DataAlvo { get; set; }
        public string MesReferencia { get; set; }
        public string TipoResultado { get; set; }
        public string PularPagina { get; set; }
        public VOIndicadorAcompanhamento()
        {
            Comentarios = new List<VOComentario>();
            DadosGrafico = new List<VOCalculoAcumuladoTrimestre>();
        }
    }
}
