﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Spec.ValueObjects
{
    public class VOIndicador
    {
        public int CodigoSeqIndicador { get; set; }
        public string NoIndicador { get; set; }
        public decimal ValorAlvo { get; set; }
        public DateTime DataAlvo { get; set; }
        public string DescricaoMeta { get; set; }
        public DateTime? DataUltimoValor { get; set; }
        public int? UltimoValor { get; set; }
        public decimal ValorTotalInformado { get; set; }
        public int? CodSeqTipoValor { get; set; }
        public int? AnoExercicio { get; set; }
        public string UnidadeCorporativa { get; set; }
        public int CodigoFaseExercicio { get; set; }
        public int CodigoUnidade { get; set; }
        public decimal ResultadoIndicador { get; set; }
    }
}
