﻿namespace PGA.Integration.Spec.ValueObjects
{
    public class VOResultadosAlcancadosPorExercicio
    {
        public string DescricaoObjetivoIniciativa { get; set; }
        public string ResultadoAlcancadoRegistrado { get; set; }
        public int CodSeqExercicio { get; set; }
        public int AnoExcercicio { get; set; }
        public int CodigoUnidade { get; set; }
        public string DescricaoUnidade { get; set; }
        public int CodSeqResultadoAlcancado { get; set; }
        public string ResultadoAlcancadoRegistradoUnidade { get; set; }
    }
}
