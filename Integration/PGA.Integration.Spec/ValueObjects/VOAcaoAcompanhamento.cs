﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Spec.ValueObjects
{
    public class VOAcaoAcompanhamento
    {
        public VOAcaoAcompanhamento()
        {
            Comentarios = new List<VOComentario>();
        }
        public int CodigoAcao { get; set; }
        public string DescricaoAcao { get; set; }
        public DateTime? DataInicio { get; set; }
        public DateTime? DataFim { get; set; }
        public decimal Percentual { get; set; }
        public string Status { get; set; }
        public string CorStatus { get; set; }
        public List<VOComentario> Comentarios { get; set; }

        public string DataInicioFormatado
        {
            get
            {
                return DataInicio.HasValue
                    ? DataInicio.Value.ToString("dd/MM/yyyy")
                    : string.Empty;
            }
        }

        public string DataFimFormatado
        {
            get
            {
                return DataFim.HasValue
                    ? DataFim.Value.ToString("dd/MM/yyyy")
                    : string.Empty;
            }
        }
    }
}
