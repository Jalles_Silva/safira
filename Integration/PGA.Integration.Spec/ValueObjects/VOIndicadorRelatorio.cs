﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Spec.ValueObjects
{
    public class VOIndicadorRelatorio
    {
        public int CodigoSeqIndicador { get; set; }
        public string DescricaoArea { get; set; }
        public int CodigoUnidade { get; set; }
        public int TotalMetasCadastradas { get; set; }
        public int IndicadoresCadastradosExercicio { get; set; }
        public int IndicadoresCadastradasPeriodo { get; set; }
        public decimal ResultadoIndicador { get; set; }
        public decimal ValorAlvo { get; set; }
        public int IndicadoresPrazoPeriodo { get; set; }
        public int IndicadoresAlimentadosPeriodo { get; set; }
        public int AnoExercicio { get; set; }
        public string DescricaoIndicador { get; set; }
        public int CodigoMeta { get; set; }
        public string DescricaoMeta { get; set; }
        public string LinhaBase { get; set; }
        public DateTime MesReferencia { get; set; }
        public DateTime DataAlvo { get; set; }
        public DateTime? DataUltimaAtualizacao { get; set; }
        public int CodigoTipoValor { get; set; }
        public int? QtdMetasInternas { get; set; }
        public string MetaInterna { get; set; }
        public bool ObjetivoEstrategico { get; set; }
    }
}
