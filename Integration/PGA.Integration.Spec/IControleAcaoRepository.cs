﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;

namespace PGA.Integration.Spec
{
    [ObjectMap("ControleAcaoRepository", true)]
    public interface IControleAcaoRepository<T> : IRepositoryBase<T>
    {
        PageMessage<T> ListarControlesPorAcao(int codigoAcao, int startIndex, int pageSize, string orderProperty, bool orderAscending);
    }
}