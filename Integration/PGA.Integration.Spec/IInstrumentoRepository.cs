﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;

namespace PGA.Integration.Spec
{
    [ObjectMap("InstrumentoRepository", true)]
    public interface IInstrumentoRepository<T> : IRepositoryBase<T>
    {
        PageMessage<T> ListarInstrumentoPorTipoEUnidade(int tipoInstrumento, int codigoUnidade, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending);
    }
}
