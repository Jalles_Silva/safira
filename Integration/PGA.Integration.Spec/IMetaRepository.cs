﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;
using PGA.Integration.Spec.ValueObjects;

namespace PGA.Integration.Spec
{
    [ObjectMap("MetaRepository", true)]
    public interface IMetaRepository<T> : IRepositoryBase<T>
    {
        PageMessage<VOMeta> ListarMetasPorUnidadeMeta(string Unidade, int AnoExercicio, int MetaInterna, int StatusMeta, int startIndex, int pageSize);

        PageMessage<VOMeta> ListarMetasPorUnidade(int codigoUnidade, int AnoExercicio, int startIndex, int pageSize);

        IList<T> ListarMetaSimplificadaPorUnidadeEAno(int codigoUnidade, short ano);

        PageMessage<VOMetaRelatorio> ListaGlobalMeta(bool VerAlinhadasPE, bool VerAlinhadasPPA, bool VerAlinhadasMI, bool VerAdministratiVas,
            bool VerFiscalizacoes, bool VerRegulacoes, string Unidade, int AnoExercicio, int CodigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize);

        PageMessage<VOMetaRelatorio> ListaDetalheMeta(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int codigoTipoMeta,
            string Unidade, int AnoExercicio, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize);

        IList<T> ListarMetasAcompanhamento(string unidade, short ano, int codigoTipoMeta, bool alinhamnetoPE, bool alinhamnetoPPA, bool alinhamnetoMI, int codigoTipoInstrumento, int metaInterna);
    }
}