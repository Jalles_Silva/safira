﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;

namespace PGA.Integration.Spec
{
    [ObjectMap("TipoIndicadorRepository", true)]
    public interface ITipoIndicadorRepository<T> : IRepositoryBase<T>
    {
        PageMessage<T> ListarTipoIndicadores(int startIndex, int pageSize, string orderProperty, bool orderAscending);
    }
}