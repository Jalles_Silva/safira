﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;
using PGA.Integration.Spec.ValueObjects;

namespace PGA.Integration.Spec
{
    [ObjectMap("IndicadorRepository", true)]
    public interface IIndicadorRepository<T> : IRepositoryBase<T>
    {
        PageMessage<T> ListarIndicadoresPorMeta(int codigoSeqMeta, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        PageMessage<VOIndicador> ListarIndicadoresPorFiltros(VOFiltroControle filtro);

        PageMessage<VOIndicadorRelatorio> ListaGlobalIndicador(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize);

        PageMessage<VOIndicadorRelatorio> ListaDetalheIndicador(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize);
    }
}