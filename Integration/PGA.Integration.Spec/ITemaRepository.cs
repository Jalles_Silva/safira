﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;

namespace PGA.Integration.Spec
{
    [ObjectMap("TemaRepository", true)]
    public interface ITemaRepository<T> : IRepositoryBase<T>
    {
        PageMessage<T> ListarTemasPorPerspectivasESituacao(int perspectiva, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending);
    }
}