﻿using SQFramework.Data;
using SQFramework.Spring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Spec
{
    [ObjectMap("ProcessoAnexoRepository", true)]
    public interface IProcessoAnexoRepository<T> : IRepositoryBase<T>
    {
    }
}