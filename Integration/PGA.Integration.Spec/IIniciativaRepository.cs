﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring;
using SQFramework.Spring.Domain;
using SQFramework.Data;
using SQFramework.Data.Pagging;
using PGA.Integration.Spec.ValueObjects;

namespace PGA.Integration.Spec
{
    [ObjectMap("IniciativaRepository", true)]
    public interface IIniciativaRepository<T> : IRepositoryBase<T>
    {
        IList<T> ListarIniciativaPorTipoEUnidadeAtivos(int tipoIniciativa, int codigoUnidade);

        PageMessage<T> ListarIniciativaPorTipoEUnidade(int tipoIniciativa, string unidade, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        IList<T> ListarTiposIniciativaFiltro(int tipoIniciativa);
    }
}