﻿using SQFramework.Data;
using SQFramework.Data.Pagging;
using SQFramework.Spring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Integration.Spec
{
    [ObjectMap("ProcessoRepository", true)]
    public interface IProcessoRepository<T> : IRepositoryBase<T>
    {
        PageMessage<T> ListarProcessos(int startIndex, int pageSize, string orderProperty, bool orderAscending);

        IEnumerable<T> ObterNomeProcesso(String pNome);
        IEnumerable<T> ObterProcessoSituacao(bool pSituacao);
        IEnumerable<T> ObterProcessoArea(String pArea);
    }
}