﻿Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginReq);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endReq);
var showProgressAttribute = 'ShowProgress';

function beginReq(sender, args) {
    if (DivProgress != null && DivProgress != 'undefined') {

        var showProgress = true;

        if (args != null && args._postBackElement != null && args._postBackElement != 'undefined') {

            var attribute = args._postBackElement.getAttribute(showProgressAttribute);

            if (attribute != null && attribute != 'undefined')
                showProgress = attribute == 'true';
        }

        if (showProgress) {
            var progress = $('#' + DivProgress);

            if (progress._element) {
                progress._element.style.zIndex = 999999;
            }

            progress.modal();

            if (args == null && sender == null) {
                verificarDownloadFinalizado();
            }
        }
    }
}

function verificarDownloadFinalizado() {
    setTimeout(function () {
        var valor = getCookie("fileDownloadToken");

        if (valor != null) {
            endReq(null, null);
            eraseCookie("fileDownloadToken");
        } else {
            verificarDownloadFinalizado();
        }
    }, 2000);
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}
function eraseCookie(name) {
    createCookie(name, "", -1);
}

function getCookie(c_name) {
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1) {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1) {
        c_value = null;
    }
    else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start, c_end));
    }
    return c_value;
}

function endReq(sender, args) {
    if (DivProgress != null && DivProgress != 'undefined') {
        $('#' + DivProgress).modal('hide');
    }

    if (UpdateProgress != null && UpdateProgress != 'undefined') {
        document.getElementById(UpdateProgress).style.display = 'none';
    }
}

function SetShowProgress(timerId, value) {

    var timer = document.getElementById(timerId);

    if (timer != null && timer != 'undefined')
        timer.setAttribute(showProgressAttribute, value);
}

function showProgressJavaScript() {
    document.getElementById(DivProgress).style.display = '';
    document.getElementById(UpdateProgress).style.display = '';

    beginReq(null, null);
}