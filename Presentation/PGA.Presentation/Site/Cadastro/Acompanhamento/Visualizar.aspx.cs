﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using SQFramework.Web.Report;

namespace PGA.Presentation.Site.Cadastro.Acompanhamento
{
    public partial class Visualizar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqAcompanhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqAcompanhamento").ToInt32();
            }
        }

        private int TipoAcompanhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoAcompanhamento").ToInt32();
            }
        }

        private int AnoExercicio
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("AnoExercicio").ToInt32();
            }
        }

        private int Status
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Status").ToInt32();
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
            lnkBaixarAnexo.Click += new EventHandler(lnkBaixarAnexo_Click);

            lnkReportPDF.Click += new EventHandler(lnkReportPDF_Click);
            lnkReportExcel.Click += new EventHandler(lnkReportExcel_Click);
            lnkReportWord.Click += new EventHandler(lnkReportWord_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Cadastro/Acompanhamento/Consultar.aspx?TipoAcompanhamento={0}&AnoExercicio={1}&Status={2}&PageIndex={3}", TipoAcompanhamento, AnoExercicio, Status, PageIndex));
        }

        protected void lnkBaixarAnexo_Click(object sender, EventArgs e)
        {
            var lnk = (LinkButton)sender;

            WebHelper.DownloadFile(System.IO.File.ReadAllBytes(lnk.Attributes["CaminhoAnexo"]), lblAnexo.Text);
        }

        protected void lnkReportPDF_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReport(ReportViewerHelper.ReportType.PDF);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void lnkReportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReport(ReportViewerHelper.ReportType.Excel);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void lnkReportWord_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReport(ReportViewerHelper.ReportType.Word);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion

        #region [Methods]

        private void CarregarEncaminhamentos()
        {
            try
            {
                var reportes = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarEncaminhamentosDoAcompanhamento(CodigoSeqAcompanhamento);

                rptEncaminhamentos.DataSource = reportes;
                rptEncaminhamentos.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqAcompanhamento > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterAcompanhamento(CodigoSeqAcompanhamento);

                    if (registro.DataAcompanhamento != null)
                        lblDataAcompanhamento.Text = registro.DataAcompanhamento.ToShortDateString();

                    if (registro.Exercicio != null)
                        lblAnoExercicio.Text = registro.Exercicio.Ano.ToString();

                    if (registro.TipoAcompanhamento != null)
                        lblTipoAcompanhamento.Text = registro.TipoAcompanhamento.DescricaoTipoAcompanhamento;

                    if (registro.Anexo != null)
                    {
                        lblAnexo.Text = registro.Anexo.DescricaoAnexo;
                        lnkBaixarAnexo.Attributes["CaminhoAnexo"] = registro.Anexo.CaminhoAnexo;
                    }
                    else
                    {
                        lnkBaixarAnexo.Text = "Sem Anexo";
                        lnkBaixarAnexo.Enabled = false;
                    }

                    if (registro.StatusAcompanhamento != null)
                        lblStatusAcompanhamento.Text = registro.StatusAcompanhamento.DescricaoStatusAcompanhamento;

                    CarregarEncaminhamentos();
                }
                else
                    WebHelper.Redirect("~/Site/Cadastro/Acompanhamento/Consultar.aspx");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void ExportReport(ReportViewerHelper.ReportType reportType)
        {
            try
            {
                byte[] bytes = this.ObterPGAService().ExportReportRelatorioAcompanhamentos(CodigoSeqAcompanhamento, reportType);

                if (bytes != null)
                {
                    string filename = "RelatorioAcompanhamento";

                    switch (reportType)
                    {
                        case ReportViewerHelper.ReportType.Excel:
                            filename += ".xls";
                            break;
                        case ReportViewerHelper.ReportType.PDF:
                            filename += ".pdf";
                            break;
                        case ReportViewerHelper.ReportType.Word:
                        default:
                            filename += ".doc";
                            break;
                    }

                    WebHelper.DownloadFile(bytes, filename);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}