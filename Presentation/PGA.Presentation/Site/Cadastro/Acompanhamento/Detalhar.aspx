﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Detalhar.aspx.cs" Inherits="PGA.Presentation.Site.Cadastro.Acompanhamento.Detalhar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnSalvar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cadastro Acompanhamento do SAFIRA</h3>
                    </div>
                    <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6">
                                        <div class="form-group">
                                            <asp:Label ID="lblTipoAcompanhamento" AssociatedControlID="drpTipoAcompanhamento" Text="Acompanhamento do SAFIRA *" runat="server" />
                                            <asp:DropDownList ID="drpTipoAcompanhamento" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-1 col-sm-1">
                                        <div class="form-group">
                                            <asp:Label ID="lblAnoExercicio" AssociatedControlID="drpAnoExercicio" Text="Exercício" runat="server" />
                                            <asp:DropDownList ID="drpAnoExercicio" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group">
                                            <asp:Label ID="lblDataAcompanhamento" AssociatedControlID="txtDataAcompanhamento" Text="Data do Acompanhamento" runat="server" />
                                            <asp:TextBox ID="txtDataAcompanhamento" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group">
                                            <asp:UpdatePanel ID="updPanel" runat="server">
                                                <ContentTemplate>
                                                    <asp:Label Text="Anexo" runat="server" AssociatedControlID="lnkBaixarAnexo" />
                                                    <asp:FileUpload ID="flpAnexo" runat="server" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div style="margin-top:-10px">
                                            <asp:LinkButton ID="lnkBaixarAnexo" ToolTip="Realizar o download do arquivo" runat="server" CaminhoAnexo="">
                                                <asp:Label ID="lblAnexo" runat="server" />   <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="lnkDeletarAnexo" ToolTip="Excluir arquivo do Acompanhamento" runat="server">
                                                <i aria-hidden="true" class="glyphicon glyphicon-remove" style="color:red;"></i>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:LinkButton ID="btnEncaminhamento" runat="server" SkinID="btnEncaminhamento" CausesValidation="false" />
                                <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                <asp:LinkButton ID="btnSalvar" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="Salvar" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtDataAcompanhamento.ClientID %>").datepicker();
            $("#<%= txtDataAcompanhamento.ClientID %>").mask("99/99/9999");
        });
    </script>
</asp:Content>
