﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;

namespace PGA.Presentation.Site.Cadastro.Acompanhamento
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqAcompanhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqAcompanhamento").ToInt32();
            }
        }

        private int TipoAcompanhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoAcompanhamento").ToInt32();
            }
        }

        private int AnoExercicio
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("AnoExercicio").ToInt32();
            }
        }

        private int Status
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Status").ToInt32();
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
            lnkBaixarAnexo.Click += new EventHandler(lnkBaixarAnexo_Click);
            lnkDeletarAnexo.Click += new EventHandler(lnkDeletarAnexo_Click);
            btnEncaminhamento.Click += new EventHandler(btnEncaminhamento_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarTipoAcompanhamento();
                    CarregarAnoExercicio();
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Voltar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        WebHelper.Redirect(String.Format("~/Site/Cadastro/Acompanhamento/Consultar.aspx?TipoAcompanhamento={0}&AnoExercicio={1}&Status={2}&PageIndex={3}", TipoAcompanhamento, AnoExercicio, Status, PageIndex));
                    }
                    break;
                case "Detalhar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        var CodSeqAcompanhamento = e.Parameters["CodSeqAcompanhamento"];
                        WebHelper.Redirect(String.Format("~/Site/Cadastro/Acompanhamento/Detalhar.aspx?CodigoSeqAcompanhamento={0}&TipoAcompanhamento={1}&AnoExercicio={2}&Status={3}&PageIndex={4}", CodSeqAcompanhamento, TipoAcompanhamento, AnoExercicio, Status, PageIndex));
                    }
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            Validate();

            if (IsValid)
            {
                if (String.IsNullOrEmpty(drpTipoAcompanhamento.SelectedValue))
                {
                    MessageBox.ShowInformationMessage("Favor informar o Acompanhamento do SAFIRA.", "Erro");
                    return;
                }

                if (String.IsNullOrEmpty(txtDataAcompanhamento.Text))
                {
                    MessageBox.ShowInformationMessage("Favor informar a Data do Acompanhamento.", "Erro");
                    return;
                }

                Salvar();
            }
        }

        protected void btnEncaminhamento_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Acompanhar/Encaminhamento/Detalhar.aspx?CodigoSeqAcompanhamento={0}&TipoAcompanhamento={1}&AnoExercicio={2}&Status={3}&PageIndex={4}", CodigoSeqAcompanhamento, TipoAcompanhamento, AnoExercicio, Status, PageIndex));
        }

        protected void lnkBaixarAnexo_Click(object sender, EventArgs e)
        {
            var lnk = (LinkButton)sender;
            
            WebHelper.DownloadFile(System.IO.File.ReadAllBytes(lnk.Attributes["CaminhoAnexo"]), lblAnexo.Text);
        }

        protected void lnkDeletarAnexo_Click(object sender, EventArgs e)
        {
            this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarAnexoAcompanhamento(CodigoSeqAcompanhamento);
            MessageBox.ShowInformationMessage("Anexo excluído com sucesso!");

            lnkBaixarAnexo.Visible = lnkDeletarAnexo.Visible = false;
            flpAnexo.Visible = true;
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Voltar",
                "Deseja retornar para a página de consulta (dados não salvos serão perdidos)?", MessageBoxButtons.YesNo, "Voltar");
        }

        #endregion

        #region [Methods]

        private void CarregarTipoAcompanhamento()
        {
            drpTipoAcompanhamento.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTiposAcompanhamento();
            drpTipoAcompanhamento.DataTextField = "DescricaoTipoAcompanhamento";
            drpTipoAcompanhamento.DataValueField = "CodigoSeqTipoAcompanhamento";
            drpTipoAcompanhamento.DataBind();
            drpTipoAcompanhamento.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void CarregarAnoExercicio()
        {
            if (UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador)
                drpAnoExercicio.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarExercicios().OrderByDescending(i => i.Ano);
            else if (UsuarioConsultor)
                drpAnoExercicio.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarExercicioExcetoPlanejamento().OrderByDescending(i => i.Ano);

            drpAnoExercicio.DataTextField = "Ano";
            drpAnoExercicio.DataValueField = "CodigoSeqExercicio";
            drpAnoExercicio.DataBind();
        }

        private void Salvar()
        {
            try
            {
                DTOAcompanhamento registro = new DTOAcompanhamento();
                registro.CodigoSeqAcompanhamento = CodigoSeqAcompanhamento;
                registro.DataAcompanhamento = DateTime.Parse(txtDataAcompanhamento.Text);

                var exercicio = new DTOExercicio();
                exercicio.CodigoSeqExercicio = drpAnoExercicio.SelectedValue.ToInt32();
                registro.Exercicio = exercicio;

                var tipo = new DTOTipoAcompanhamento();
                tipo.CodigoSeqTipoAcompanhamento = drpTipoAcompanhamento.SelectedValue.ToInt32();
                registro.TipoAcompanhamento = tipo;

                registro.StatusAcompanhamento = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterStatusAcompanhamento(Common.EnumStatusAcompanhamento.Pendente);

                if (flpAnexo.HasFile)
                {
                    var anexo = new DTOAnexo();

                    anexo.DescricaoAnexo = flpAnexo.FileName;
                    anexo.Conteudo = flpAnexo.FileBytes;
                    anexo.CaminhoAnexo = System.Configuration.ConfigurationManager.AppSettings["diretorioAnexosAcompanhamento"];

                    registro.Anexo = anexo;
                }

                var CodSeqAcompanhamento = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarAcompanhamento(registro);

                var param = new Dictionary<string, object>();
                param.Add("CodSeqAcompanhamento", CodSeqAcompanhamento);

                MessageBox.ShowInformationMessage("Registro salvo com sucesso.", "Detalhar", param);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqAcompanhamento > 0)
                {
                    drpTipoAcompanhamento.Enabled = false;
                    btnEncaminhamento.Visible = true;

                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterAcompanhamento(CodigoSeqAcompanhamento);

                    txtDataAcompanhamento.Text = registro.DataAcompanhamento.ToShortDateString();

                    if (registro.Exercicio != null)
                        drpAnoExercicio.SelectedValue = registro.Exercicio.CodigoSeqExercicio.ToString();

                    if (registro.TipoAcompanhamento != null)
                        drpTipoAcompanhamento.SelectedValue = registro.TipoAcompanhamento.CodigoSeqTipoAcompanhamento.ToString();

                    if (registro.Anexo != null)
                    {
                        lblAnexo.Text = registro.Anexo.DescricaoAnexo;
                        lnkBaixarAnexo.Attributes["CaminhoAnexo"] = registro.Anexo.CaminhoAnexo;
                        lnkBaixarAnexo.Visible = lnkDeletarAnexo.Visible = true;
                        flpAnexo.Visible = false;
                    }
                    else
                    {
                        lnkBaixarAnexo.Visible = lnkDeletarAnexo.Visible = false;
                        flpAnexo.Visible = true;
                    }
                }
                else
                {
                    lnkBaixarAnexo.Visible = lnkDeletarAnexo.Visible = false;
                    flpAnexo.Visible = true;
                    drpTipoAcompanhamento.Enabled = true;
                    btnEncaminhamento.Visible = false;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}