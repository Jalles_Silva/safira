﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Consultar.aspx.cs" Inherits="PGA.Presentation.Site.Cadastro.Acompanhamento.Consultar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Pesquisar Acompanhamento</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoAcompanhamento" AssociatedControlID="drpTipoAcompanhamento" Text="Acompanhamento do SAFIRA" runat="server" />
                                    <asp:DropDownList ID="drpTipoAcompanhamento" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-1 col-sm-1">
                                <div class="form-group">
                                    <asp:Label ID="lblAnoExercicio" AssociatedControlID="drpAnoExercicio" Text="Exercício" runat="server" />
                                    <asp:DropDownList ID="drpAnoExercicio" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblStatus" AssociatedControlID="rblStatus" Text="Status" runat="server" />
                                    <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal" CssClass="radioListHoriz">
                                        <asp:ListItem Value="0" Text="Todos" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Pendente"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Concluído"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-12">
                                    <asp:LinkButton ID="btnPesquisar" runat="server" SkinID="btnPesquisar" CausesValidation="false" />
                                    <asp:HyperLink ID="btnLimpar" runat="server" SkinID="btnLimpar" CausesValidation="false"
                                        NavigateUrl="~/Site/Cadastro/Acompanhamento/Consultar.aspx" />
                                    <asp:HyperLink ID="btnNovo" runat="server" SkinID="btnNovo" CausesValidation="false"
                                        NavigateUrl="~/Site/Cadastro/Acompanhamento/Detalhar.aspx" />
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="upPnlResultadoConsulta" runat="server">
                            <ContentTemplate>
                                <div class="area-table">
                                    <asp:GridView ID="grdAcompanhamento" runat="server" AutoGenerateColumns="False" EmptyDataText="Nenhum registro encontrado." OnRowCommand="grdAcompanhamento_RowCommand">
                                        <EmptyDataTemplate>Nenhum registro encontrado.</EmptyDataTemplate>
                                        <AlternatingRowStyle CssClass="alternate" />
                                        <Columns>
                                            <asp:BoundField HeaderText="Acompanhamento do SAFIRA" DataField="TipoAcompanhamento.DescricaoTipoAcompanhamento" />
                                            <asp:BoundField HeaderText="Exercício" DataField="Exercicio.Ano" ItemStyle-Width="80px" />
                                            <asp:BoundField HeaderText="Data do Acompanhamento" DataField="DataAcompanhamento" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="170px" />
                                            <asp:BoundField HeaderText="Status" DataField="StatusAcompanhamento.DescricaoStatusAcompanhamento" ItemStyle-Width="100px" />
                                            <asp:TemplateField HeaderText="Ações" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkDetalhar" runat="server" CommandArgument='<%# Eval("CodigoSeqAcompanhamento") %>'
                                                        CommandName="Detalhar" CausesValidation="false" CssClass="glyphicon glyphicon-pencil"
                                                        Visible='<%# Convert.ToInt32(Eval("StatusAcompanhamento.CodigoSeqStatusAcompanhamento")) == 2 ? false : true %>'/>
                                                    <asp:LinkButton ID="lnkVisualizar" runat="server" Target="_self" CommandArgument='<%# Eval("CodigoSeqAcompanhamento") %>'
                                                        CommandName="Visuzalizar" CausesValidation="false" CssClass="glyphicon glyphicon-search" />
                                                    <asp:LinkButton ID="lnkExcluir" runat="server" CommandArgument='<%# Eval("CodigoSeqAcompanhamento") %>'
                                                        CommandName="Excluir" CausesValidation="false" CssClass="glyphicon glyphicon-remove"
                                                        Visible='<%# Convert.ToInt32(Eval("StatusAcompanhamento.CodigoSeqStatusAcompanhamento")) == 2 ? false : true %>'/>
                                                    <asp:LinkButton ID="lnkConcluir" runat="server" CommandArgument='<%# Eval("CodigoSeqAcompanhamento") %>'
                                                        CommandName="Concluir" CausesValidation="false" CssClass="glyphicon glyphicon-ok"
                                                        ToolTip='<%# Convert.ToBoolean(Eval("AptoConclusao")) == false ? "Não é possível concluir o Acompanhamento por existir(em) Encaminhamento(s) pendente(s)": "Acompanhamento apto para a conclusão" %>'
                                                        Style='<%# Convert.ToInt32(Eval("StatusAcompanhamento.CodigoSeqStatusAcompanhamento")) == 2 ? "visibility:hidden;": Convert.ToBoolean(Eval("AptoConclusao")) == false ? "color:red;": "color:green;" %>'/>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <div class="paginator">
                                    <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
