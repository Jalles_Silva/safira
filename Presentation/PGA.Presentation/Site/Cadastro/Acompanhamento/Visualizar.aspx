﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Visualizar.aspx.cs" Inherits="PGA.Presentation.Site.Cadastro.Acompanhamento.Visualizar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Consulta/Validação</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>Acompanhamento do SAFIRA</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-1 col-sm-1">
                                        <div class="form-group">
                                            <asp:Label Text="EXERCÍCIO: " runat="server" />
                                            <asp:Label ID="lblAnoExercicio" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="ACOMPANHAMENTO: " runat="server" />
                                            <asp:Label ID="lblTipoAcompanhamento" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group">
                                            <asp:Label Text="DATA DO ACOMPANHAMENTO: " runat="server" />
                                            <asp:Label ID="lblDataAcompanhamento" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-1 col-sm-1">
                                        <div class="form-group">
                                            <asp:Label Text="STATUS: " runat="server" />
                                            <asp:Label ID="lblStatusAcompanhamento" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6">
                                        <div>
                                            <asp:Label Text="ANEXO: " runat="server" />
                                            <asp:LinkButton ID="lnkBaixarAnexo" ToolTip="Realizar o download do arquivo" runat="server" CaminhoAnexo="">
                                                <asp:Label ID="lblAnexo" runat="server" />   <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>Encaminhamentos</strong></h3>
                            </div>

                            <asp:Repeater ID="rptEncaminhamentos" runat="server">
                                <ItemTemplate>
                                    <div style="margin:10px 0 15px 10px;">
                                        <div class="row">
                                            <div class="col-xs-1 col-sm-1">
                                                <div>
                                                    <asp:Label Text="ÁREA: " runat="server" />
                                                    <asp:Label ID="lblArea" Text='<%#Eval("CorporativoUnidade.DescricaoUnidade")%>' runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div>
                                                    <asp:Label Text="META: " runat="server" />
                                                    <asp:Label ID="lblMeta" Text='<%#Eval("Meta.DescricaoMeta")%>' runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div>
                                                    <asp:Label Text="ENCAMINHAMENTO: " runat="server" />
                                                    <asp:Label ID="lblEncaminhamento" Text='<%#Eval("DescricaoEncaminhamento")%>' runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-2 col-sm-2">
                                                <div>
                                                    <asp:Label Text="PRAZO DE ATENDIMENTO: " runat="server" />
                                                    <asp:Label ID="lblPrazoEncaminhamento" Text='<%#Eval("PrazoAtendimento", "{0:dd/MM/yyyy}")%>' runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div>
                                                    <asp:Label Text="STATUS: " runat="server" />
                                                    <asp:Label ID="lblStatusEncaminhamento" Text='<%#Eval("StatusEncaminhamento.DescricaoStatusEncaminhamento")%>' runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <div style="margin:5px 0 5px 5px;">
                                        <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." />
                                    </div>
                                </FooterTemplate>
                            </asp:Repeater>

                        </div>
                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <div class="btn-group" style="margin-top:-5px;">
                                    <button type="button" class="btn btn-antt dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i> Exportar <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><asp:LinkButton ID="lnkReportPDF" runat="server" CausesValidation="false" Text="PDF" /></li>
                                        <li><asp:LinkButton ID="lnkReportExcel" runat="server" CausesValidation="false" Text="Excel" /></li>
                                        <li><asp:LinkButton ID="lnkReportWord" runat="server" CausesValidation="false" Text="Word" /></li>
                                    </ul>
                                </div>
                                <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
