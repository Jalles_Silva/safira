﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;

namespace PGA.Presentation.Site.Cadastro.Acompanhamento
{
    public partial class Consultar : CustomPageBase
    {
        #region [Properties]

        private int TipoAcompanhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoAcompanhamento").ToInt32();
            }
        }

        private int AnoExercicio
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("AnoExercicio").ToInt32();
            }
        }

        private int Status
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Status").ToInt32();
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            btnPesquisar.Click += BtnPesquisar_Click;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarTipoAcompanhamento();
                    CarregarAnoExercicio();

                    if (TipoAcompanhamento > 0)
                        drpTipoAcompanhamento.SelectedValue = TipoAcompanhamento.ToString();
                    if (AnoExercicio > 0)
                        drpAnoExercicio.SelectedValue = AnoExercicio.ToString();
                    if (Status > 0)
                        rblStatus.SelectedValue = Status.ToString();
                    if (PageIndex > 0)
                        ConsultarDados(PageIndex - 1);
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Remover":
                    if (e.Result == MessageBoxResult.Yes)
                        Remover(e.Parameters["CodigoSeqAcompanhamento"].ToInt32());
                    break;
                case "Concluir":
                    if (e.Result == MessageBoxResult.Yes)
                        Concluir(e.Parameters["CodigoSeqAcompanhamento"].ToInt32());
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            ConsultarDados(e.NewPage - 1);
        }

        private void BtnPesquisar_Click(object sender, EventArgs e)
        {
            ConsultarDados(0);
        }

        protected void grdAcompanhamento_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Excluir":
                    {
                        var parametros = new Dictionary<string, object>();
                        parametros.Add("CodigoSeqAcompanhamento", e.CommandArgument);

                        MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente excluir o registro?",
                            "Deseja realmente excluir o registro?", MessageBoxButtons.YesNo, "Remover", parametros);
                    }
                    break;
                case "Concluir":
                    {
                        var encaminhamentos = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarEncaminhamentosDoAcompanhamento(e.CommandArgument.ToInt32());

                        if (encaminhamentos.Where(x => x.StatusEncaminhamento.CodigoSeqStatusEncaminhamento == (int)Common.EnumStatusAcompanhamento.Pendente).Count() == 0)
                        {
                            var parametros = new Dictionary<string, object>();
                            parametros.Add("CodigoSeqAcompanhamento", e.CommandArgument);

                            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente concluir o aconpanhamento?",
                            "Deseja realmente concluir o aconpanhamento?", MessageBoxButtons.YesNo, "Concluir", parametros);
                        }
                        else
                            MessageBox.ShowErrorMessage("Não é possível concluir o Acompanhamento por existir(em) Encaminhamento(s) pendente(s)");
                    }
                    break;
                case "Detalhar":
                    WebHelper.Redirect(String.Format("~/Site/Cadastro/Acompanhamento/Detalhar.aspx?CodigoSeqAcompanhamento={0}&TipoAcompanhamento={1}&AnoExercicio={2}&Status={3}&PageIndex={4}", e.CommandArgument, drpTipoAcompanhamento.SelectedValue, drpAnoExercicio.SelectedValue, rblStatus.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                case "Visuzalizar":
                    WebHelper.Redirect(String.Format("~/Site/Cadastro/Acompanhamento/Visualizar.aspx?CodigoSeqAcompanhamento={0}&TipoAcompanhamento={1}&AnoExercicio={2}&Status={3}&PageIndex={4}", e.CommandArgument, drpTipoAcompanhamento.SelectedValue, drpAnoExercicio.SelectedValue, rblStatus.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region [Methods]

        private void CarregarTipoAcompanhamento()
        {
            drpTipoAcompanhamento.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTiposAcompanhamento();
            drpTipoAcompanhamento.DataTextField = "DescricaoTipoAcompanhamento";
            drpTipoAcompanhamento.DataValueField = "CodigoSeqTipoAcompanhamento";
            drpTipoAcompanhamento.DataBind();
            drpTipoAcompanhamento.Items.Insert(0, new ListItem("Todos", "0"));
        }

        private void CarregarAnoExercicio()
        {
            if (UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador)
                drpAnoExercicio.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarExercicios().OrderByDescending(i => i.Ano);
            else if (UsuarioConsultor)
                drpAnoExercicio.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarExercicioExcetoPlanejamento().OrderByDescending(i => i.Ano);

            drpAnoExercicio.DataTextField = "Ano";
            drpAnoExercicio.DataValueField = "Ano";
            drpAnoExercicio.DataBind();
            drpAnoExercicio.Items.Insert(0, new ListItem("Todos", "0"));
        }

        private void ConsultarDados(int pageIndex)
        {
            Validate();

            if (IsValid)
            {
                try
                {
                    var acompanhamentos = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarAcompanhamentosPorTipoAnoEStatus(drpTipoAcompanhamento.SelectedValue.ToInt32(), drpAnoExercicio.SelectedValue.ToShort(), rblStatus.SelectedValue.ToInt32(), pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize, "CodigoSeqAcompanhamento", true);

                    foreach (var item in acompanhamentos)
                    {
                        var encaminhamentos = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarEncaminhamentosDoAcompanhamento(item.CodigoSeqAcompanhamento);

                        if (encaminhamentos.Where(e => e.StatusEncaminhamento.CodigoSeqStatusEncaminhamento == (int)Common.EnumStatusAcompanhamento.Pendente).Count() == 0)
                            item.AptoConclusao = true;
                    }

                    grdAcompanhamento.DataSource = acompanhamentos;
                    grdAcompanhamento.DataBind();

                    ucPaginatorConsulta.Visible = (acompanhamentos.RowsCount > 0);
                    ucPaginatorConsulta.TotalRecords = acompanhamentos.RowsCount;
                    ucPaginatorConsulta.PageIndex = pageIndex + 1;
                    ucPaginatorConsulta.DataBind();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        private void Remover(int CodigoSeqAcompanhamento)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarAcompanhamento(CodigoSeqAcompanhamento);
                MessageBox.ShowInformationMessage("Registro excluido com sucesso!");
                ConsultarDados(ucPaginatorConsulta.PageIndex - 1);
            }
            catch
            {
                MessageBox.ShowErrorMessage("Não foi possível excluir o registro devido ao fato de ele já ter vinculação.");
            }
        }

        private void Concluir(int CodigoSeqAcompanhamento)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").AlterarStatusAcompanhamento(CodigoSeqAcompanhamento, (int)Common.EnumStatusAcompanhamento.Concluido);
                MessageBox.ShowInformationMessage("Acompanhamento concluído com sucesso!");
                ConsultarDados(ucPaginatorConsulta.PageIndex - 1);
            }
            catch (Exception e)
            {
                MessageBox.ShowErrorMessage(e.Message);
            }
        }

        #endregion
    }
}