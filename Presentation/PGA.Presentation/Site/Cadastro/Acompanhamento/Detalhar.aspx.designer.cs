﻿//------------------------------------------------------------------------------
// <gerado automaticamente>
//     Este código foi gerado por uma ferramenta.
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for recriado
// </gerado automaticamente>
//------------------------------------------------------------------------------

namespace PGA.Presentation.Site.Cadastro.Acompanhamento
{


    public partial class Detalhar
    {

        /// <summary>
        /// Controle Panel.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel;

        /// <summary>
        /// Controle lblTipoAcompanhamento.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTipoAcompanhamento;

        /// <summary>
        /// Controle drpTipoAcompanhamento.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList drpTipoAcompanhamento;

        /// <summary>
        /// Controle lblAnoExercicio.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblAnoExercicio;

        /// <summary>
        /// Controle drpAnoExercicio.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList drpAnoExercicio;

        /// <summary>
        /// Controle lblDataAcompanhamento.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblDataAcompanhamento;

        /// <summary>
        /// Controle txtDataAcompanhamento.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtDataAcompanhamento;

        /// <summary>
        /// Controle updPanel.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel updPanel;

        /// <summary>
        /// Controle flpAnexo.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.FileUpload flpAnexo;

        /// <summary>
        /// Controle lnkBaixarAnexo.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton lnkBaixarAnexo;

        /// <summary>
        /// Controle lblAnexo.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblAnexo;

        /// <summary>
        /// Controle lnkDeletarAnexo.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton lnkDeletarAnexo;

        /// <summary>
        /// Controle btnEncaminhamento.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton btnEncaminhamento;

        /// <summary>
        /// Controle btnVoltar.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton btnVoltar;

        /// <summary>
        /// Controle btnSalvar.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton btnSalvar;
    }
}
