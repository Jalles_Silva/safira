﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using System.Web.Security;
using SCA.WebControls;
using PGA.Presentation.Util;

namespace PGA.Presentation.Site.MasterPage
{
    public partial class Site : CustomMasterPageBase
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            lnkPortalSistemas.Click += new EventHandler(lnkPortalSistemas_Click);
            lnkSair.Click += new EventHandler(lnkSair_Click);
            Load += new EventHandler(Site_Load);
            menu.MenuItemDataBound += menu_MenuItemDataBound;

            if (PageBase != null)
                PageBase.MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
        }

        void menu_MenuItemDataBound(object sender, MenuEventArgs e)
        {
            if (e.Item.NavigateUrl.EndsWith("pdf"))
                e.Item.Target = "_blank";
        }

        protected void Site_Load(object sender, EventArgs e)
        {
            if (SCAApplicationContext.Usuario == null)
            {
                phPortal.Visible = false;
                litBoasVindas.Text = string.Empty;
            }
            else
            {
                phPortal.Visible = true;
                litBoasVindas.Text = string.Format("Bem - vindo(a) {0}", SCAApplicationContext.Usuario.Identificador != "usuario.sistemico" ? SCAApplicationContext.Usuario.Nome : "");
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            if (e != null)
            {
                switch (e.Command)
                {
                    case "Sair":
                        if (e.Result == MessageBoxResult.Yes)
                            WebHelper.LogoffWithNewRedirection("~/Default.aspx");

                        break;
                    case "Portal":
                        if (e.Result == MessageBoxResult.Yes)
                        {
                            string[] enderecoPortal = FormsAuthentication.LoginUrl.Split(new char[] { '/' });
                            WebHelper.Redirect("/" + enderecoPortal[1] + "/Site/PortalSistemas.aspx");
                        }

                        break;
                }
            }
        }

        public bool PopUp
        {
            get { return divTopoPopUp.Visible; }
            set
            {
                divTopoPadrao.Visible = !value;
                divTopoPopUp.Visible = !value;
                divFooter.Visible = !value;
                divHeader.Visible = !value;
                menu.Visible = !value;
            }
        }

        protected void lnkPortalSistemas_Click(object sender, EventArgs e)
        {
            PageBase.MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Porta de Sistemas", "Deseja realmente sair para o portal de sistemas?", MessageBoxButtons.YesNo, "Portal");
        }

        protected void lnkSair_Click(object sender, EventArgs e)
        {
            PageBase.MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Sair", "Deseja terminar a sessão?", MessageBoxButtons.YesNo, "Sair");
        }
    }
}