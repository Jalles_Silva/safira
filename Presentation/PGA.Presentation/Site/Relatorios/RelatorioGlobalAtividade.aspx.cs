﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using SQFramework.Web.Report;
using SQFramework.Core.Reflection;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using PGA.Common;

namespace PGA.Presentation.Site.Relatorios
{
    public partial class RelatorioGlobalAtividade : CustomPageBase
    {
        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnPesquisar.Click += new EventHandler(btnPesquisar_Click);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            gdvResultadoConsulta.Sorting += new GridViewSortEventHandler(gdvResultadoConsulta_Sorting);

            btnExportarExcel.Click += new EventHandler(btnExportarExcel_Click);
            lnkReportPDF.Click += new EventHandler(lnkReportPDF_Click);
            lnkReportExcel.Click += new EventHandler(lnkReportExcel_Click);
            lnkReportWord.Click += new EventHandler(lnkReportWord_Click);

            ddlAnoExercicio.SelectedIndexChanged += ddlAnoExercicio_SelectedIndexChanged;
        }

        void ddlAnoExercicio_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarArea();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (UsuarioConsultor || UsuarioCadastrador || UsuarioAdministrador)
                {
                    if (!IsPostBack)
                        CarregarControles();
                }
                else
                    WebHelper.LogoffWithNewRedirection("~/Default.aspx");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReportSimples(ReportViewerHelper.ReportType.Excel);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }
        protected void lnkReportPDF_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReport(ReportViewerHelper.ReportType.PDF);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void lnkReportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReport(ReportViewerHelper.ReportType.Excel);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void lnkReportWord_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReport(ReportViewerHelper.ReportType.Word);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Recarregar":
                    if (e.Result == MessageBoxResult.OK)
                        btnPesquisar_Click(btnPesquisar, null);
                    break;
            }
        }

        protected void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                ConsultarDados(0, gdvResultadoConsulta.Columns[0].SortExpression, true);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void gdvResultadoConsulta_Sorting(object sender, GridViewSortEventArgs e)
        {
            bool orderAscending = true;

            if (gdvResultadoConsulta.Attributes["SortExpression"] == e.SortExpression)
                orderAscending = !gdvResultadoConsulta.Attributes["SortAscending"].ToBoolean();

            ConsultarDados(ucPaginatorConsulta.PageIndex - 1, e.SortExpression, orderAscending);
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            ConsultarDados(e.NewPage - 1, gdvResultadoConsulta.Attributes["SortExpression"], gdvResultadoConsulta.Attributes["SortAscending"].ToBoolean());
        }

        #endregion

        #region [Methods]

        private bool alinhamnetoPE = false;
        private bool alinhamnetoPPA = false;
        private bool alinhamnetoMI = false;

        private void ConsultarDados(int pageIndex, string sortExpression, bool sortAscending)
        {
            try
            {
                BuscarParamentros();
                if (ValidaParametros())
                {
                    var dadosPesquisados = this.ObterPGAService()
                        .ListaGlobalAtividade(alinhamnetoPE, alinhamnetoPPA, alinhamnetoMI, ddlAnoExercicio.SelectedValue.ToInt32(),
                        ddlArea.SelectedItem.Text, ddlPeriodo.SelectedValue.ToInt32(), ddlDescricaoMeta.SelectedValue.ToInt32(), ddlMetaInterna.SelectedValue.ToInt32(),
                         ddlTipoInstrumento.SelectedValue.ToInt32(), pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize);

                    btnExportar.Visible = (dadosPesquisados.RowsCount > 0);
                    gdvResultadoConsulta.Visible = true;
                    gdvResultadoConsulta.DataSource = dadosPesquisados.Entities.ToList();
                    gdvResultadoConsulta.DataBind();
                    gdvResultadoConsulta.Attributes["SortExpression"] = sortExpression;
                    gdvResultadoConsulta.Attributes["SortAscending"] = sortAscending.ToString();
                }
                else
                {
                    btnExportar.Visible = false;
                    gdvResultadoConsulta.Visible = false;
                    MessageBox.ShowInformationMessage("Favor selecionar um tipo de alinhamento");
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarControles()
        {
            CarregarExercicio();
            CarregarArea();
            CarregarTipoMetas();
            CarregarPeriodo();
            CarregarTipoInstrumento();
        }

        private void CarregarExercicio()
        {
            ddlAnoExercicio.DataSource = this.ObterPGAService().ListarExercicios();
            ddlAnoExercicio.DataTextField = "Ano";
            ddlAnoExercicio.DataValueField = "Ano";
            ddlAnoExercicio.DataBind();

            var ExercicioAtual = this.ObterPGAService().ObterExercicioAtual();
            ddlAnoExercicio.SelectedValue = this.ObterPGAService().ObterExercicioAtual() != null ? ExercicioAtual.Ano.ToString() : DateTime.Now.Year.ToString();
        }

        private void CarregarArea()
        {
            ddlArea.DataSource = this.ObterPGAService().ListarCorporativoUnidadePorExercicio(ddlAnoExercicio.SelectedValue.ToInt32());
            ddlArea.DataTextField = "DescricaoUnidade";
            ddlArea.DataValueField = "CodigoUnidade";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("Todas", ""));
        }

        private void CarregarPeriodo()
        {
            ddlPeriodo.DataSource = this.ObterPGAService().ListarFase()
                    .Where(c => c.CodigoSeqFase == (int)FasePeriodo.PrimeiroTrimestre ||
                                c.CodigoSeqFase == (int)FasePeriodo.SegundoTrimestre ||
                                c.CodigoSeqFase == (int)FasePeriodo.TerceiroTrimestre ||
                                c.CodigoSeqFase == (int)FasePeriodo.QuartoTrimestre);
            ddlPeriodo.DataTextField = "DescricaoFaseAlterada";
            ddlPeriodo.DataValueField = "CodigoSeqFase";
            ddlPeriodo.DataBind();
            ddlPeriodo.Items.Insert(0, new ListItem("Acumulado no Exercício", ""));
        }

        private void CarregarTipoMetas()
        {
            ddlDescricaoMeta.DataSource = this.ObterPGAService().ListarTipoMetas();
            ddlDescricaoMeta.DataTextField = "DescricaoTipoMeta";
            ddlDescricaoMeta.DataValueField = "CodigoSeqTipoMeta";
            ddlDescricaoMeta.DataBind();
            ddlDescricaoMeta.Items.Insert(0, new ListItem("Todos", ""));
        }
        private void CarregarTipoInstrumento()
        {
            ddlTipoInstrumento.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTiposInstrumento();
            ddlTipoInstrumento.DataTextField = "DescricaoTipoInstrumento";
            ddlTipoInstrumento.DataValueField = "CodigoSeqTipoInstrumento";
            ddlTipoInstrumento.DataBind();
            ddlTipoInstrumento.Items.Insert(0, new ListItem("Todos", "0"));
        }
        private void BuscarParamentros()
        {
            foreach (ListItem item in rbtTipoAlinhamento.Items)
            {
                if (item.Selected)
                {
                    if (item.Value.ToInt32() == (int)TiposAlinhamento.Alinhamento_PE) alinhamnetoPE = true;
                    if (item.Value.ToInt32() == (int)TiposAlinhamento.Alinhamento_PPA) alinhamnetoPPA = true;
                    if (item.Value.ToInt32() == (int)TiposAlinhamento.Alinhamento_MI) alinhamnetoMI = true;
                }
            }
        }

        private bool ValidaParametros()
        {
            bool retorno = false;
            retorno = (alinhamnetoPE || alinhamnetoPPA || alinhamnetoMI);
            return retorno;
        }

        public void ExportReport(ReportViewerHelper.ReportType reportType)
        {
            try
            {
                BuscarParamentros();
                if (ValidaParametros())
                {
                    string orderProperty = (gdvResultadoConsulta.Attributes["SortExpression"] ?? gdvResultadoConsulta.Columns[0].SortExpression);
                    bool orderAscending = (gdvResultadoConsulta.Attributes["SortAscending"] ?? "True").ToBoolean();

                    byte[] bytes = this.ObterPGAService().ExportReportRelatorioGlobalAtividade(alinhamnetoPE, alinhamnetoPPA, alinhamnetoMI,
                        ddlAnoExercicio.SelectedValue.ToInt32(), ddlArea.SelectedItem.Text, ddlPeriodo.SelectedValue.ToInt32(),
                        ddlDescricaoMeta.SelectedValue.ToInt32(), ddlMetaInterna.SelectedValue.ToInt32(), ddlTipoInstrumento.SelectedValue.ToInt32(), reportType, orderProperty, orderAscending);

                    if (bytes != null)
                    {
                        string filename = "RelatorioGlobalAtividade";

                        switch (reportType)
                        {
                            case ReportViewerHelper.ReportType.Excel:
                                filename += ".xls";
                                break;
                            case ReportViewerHelper.ReportType.PDF:
                                filename += ".pdf";
                                break;
                            case ReportViewerHelper.ReportType.Word:
                            default:
                                filename += ".doc";
                                break;
                        }

                        WebHelper.DownloadFile(bytes, filename);
                    }
                }
                else
                    MessageBox.ShowInformationMessage("Favor selecionar um tipo de alinhamento");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }
        public void ExportReportSimples(ReportViewerHelper.ReportType reportType)
        {
            try
            {
                BuscarParamentros();
                if (ValidaParametros())
                {
                    string orderProperty = (gdvResultadoConsulta.Attributes["SortExpression"] ?? gdvResultadoConsulta.Columns[0].SortExpression);
                    bool orderAscending = (gdvResultadoConsulta.Attributes["SortAscending"] ?? "True").ToBoolean();

                    byte[] bytes = this.ObterPGAService().ExportReportRelatorioGlobalAtividadeSimples(alinhamnetoPE, alinhamnetoPPA, alinhamnetoMI,
                        ddlAnoExercicio.SelectedValue.ToInt32(), ddlArea.SelectedItem.Text, ddlPeriodo.SelectedValue.ToInt32(),
                        ddlDescricaoMeta.SelectedValue.ToInt32(), ddlMetaInterna.SelectedValue.ToInt32(), ddlTipoInstrumento.SelectedValue.ToInt32(), reportType, orderProperty, orderAscending);

                    if (bytes != null)
                    {
                        string filename = "RelatorioGlobalAtividadeExtracaoExcel.xls";

                        WebHelper.DownloadFile(bytes, filename);
                    }
                }
                else
                    MessageBox.ShowInformationMessage("Favor selecionar um tipo de alinhamento");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }
        #endregion
    }
}