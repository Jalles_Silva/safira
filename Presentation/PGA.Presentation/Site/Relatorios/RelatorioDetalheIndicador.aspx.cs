﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using SQFramework.Web.Report;
using SQFramework.Core.Reflection;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SQFramework.Data.Pagging;
using PGA.Common;

namespace PGA.Presentation.Site.Relatorios
{
    public partial class RelatorioDetalheIndicador : CustomPageBase
    {
        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnPesquisar.Click += new EventHandler(btnPesquisar_Click);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);

            btnExportarExcel.Click += new EventHandler(btnExportarExcel_Click);
            lnkReportPDF.Click += new EventHandler(lnkReportPDF_Click);
            lnkReportExcel.Click += new EventHandler(lnkReportExcel_Click);
            lnkReportWord.Click += new EventHandler(lnkReportWord_Click);

            ddlAnoExercicio.SelectedIndexChanged += ddlAnoExercicio_SelectedIndexChanged;
        }

        void ddlAnoExercicio_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarArea();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (UsuarioPublico && !(UsuarioConsultor || UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador))
                {
                    ddlTipoInstrumento.Visible = false;
                    ddlMetaInterna.Visible = false;
                    lblTipoInstrumento.Visible = false;
                    lblMetaInterna.Visible = false;
                }
                else
                {
                    ddlTipoInstrumento.Visible = true;
                    ddlMetaInterna.Visible = true;
                    lblTipoInstrumento.Visible = true;
                    lblMetaInterna.Visible = true;
                }
                if (UsuarioConsultor || UsuarioPublico || UsuarioCadastrador || UsuarioAdministrador)
                {
                    if (!IsPostBack)
                        CarregarControles();
                }
                else
                    WebHelper.LogoffWithNewRedirection("~/Default.aspx");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }
        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReportSimples(ReportViewerHelper.ReportType.Excel);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }
        protected void lnkReportPDF_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReport(ReportViewerHelper.ReportType.PDF);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void lnkReportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReport(ReportViewerHelper.ReportType.Excel);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void lnkReportWord_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReport(ReportViewerHelper.ReportType.Word);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            try
            {
                switch (e.Command)
                {
                    case "Recarregar":
                        if (e.Result == MessageBoxResult.OK)
                            btnPesquisar_Click(btnPesquisar, null);
                        break;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                ConsultarDados(0);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            try
            {
                ConsultarDados(e.NewPage - 1);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion

        #region [Methods]

        private bool alinhamnetoPE = false;
        private bool alinhamnetoPPA = false;
        private bool alinhamnetoMI = false;

        private void BuscarParamentros()
        {
            foreach (ListItem item in rbtTipoAlinhamento.Items)
            {
                if (item.Selected)
                {
                    if (item.Value.ToInt32() == (int)TiposAlinhamento.Alinhamento_PE) alinhamnetoPE = true;
                    if (item.Value.ToInt32() == (int)TiposAlinhamento.Alinhamento_PPA) alinhamnetoPPA = true;
                    if (item.Value.ToInt32() == (int)TiposAlinhamento.Alinhamento_MI) alinhamnetoMI = true;
                }
            }
        }

        private bool ValidaParametros()
        {
            bool retorno = false;
            retorno = (alinhamnetoPE || alinhamnetoPPA || alinhamnetoMI);
            return retorno;
        }

        private void ConsultarDados(int pageIndex)
        {
            try
            {
                BuscarParamentros();

                if (ValidaParametros())
                {

                    PageMessage<DTOIndicadorRelatorio> dadosPesquisados = this.ObterPGAService().ListaDetalheIndicador(alinhamnetoPE, alinhamnetoPPA, alinhamnetoMI,
                        ddlAnoExercicio.SelectedValue.ToInt32(), ddlArea.SelectedItem.Text, ddlPeriodo.SelectedValue.ToInt32(),
                        ddlDescricaoMeta.SelectedValue.ToInt32(), (UsuarioPublico && !(UsuarioConsultor || UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador)) ? 2 : ddlMetaInterna.SelectedValue.ToInt32(),
                         ddlTipoInstrumento.SelectedValue.ToInt32(), pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize);

                    List<DTOIndicadorRelatorio> source = dadosPesquisados.Entities.ToList();

                    var metas = source.Select(i => new { DescricaoMeta = i.DescricaoMeta, UnidadeCorporativa = i.DescricaoArea }).Distinct().ToList();

                    Dictionary<string, IList<DTOIndicadorRelatorio>> lista = new Dictionary<string, IList<DTOIndicadorRelatorio>>();

                    foreach (var item in metas)
                    {
                        lista.Add(item.UnidadeCorporativa + " - " + item.DescricaoMeta, source.Where(i => i.DescricaoArea == item.UnidadeCorporativa
                            && i.DescricaoMeta == item.DescricaoMeta).ToList());
                    }

                    rptIndicador.DataSource = lista;

                    rptIndicador.DataBind();

                    btnExportar.Visible = (dadosPesquisados.RowsCount > 0);
                    rptIndicador.Visible = true;

                    ucPaginatorConsulta.Visible = (dadosPesquisados.RowsCount > 0);
                    ucPaginatorConsulta.TotalRecords = dadosPesquisados.RowsCount;
                    ucPaginatorConsulta.PageIndex = pageIndex + 1;
                    ucPaginatorConsulta.DataBind();
                }
                else
                {
                    btnExportar.Visible = false;
                    rptIndicador.Visible = false;
                    ucPaginatorConsulta.Visible = false;
                    MessageBox.ShowInformationMessage("Favor selecionar um tipo de alinhamento");
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarControles()
        {
            CarregarExercicio();
            CarregarArea();
            CarregarTipoMetas();
            CarregarPeriodo();
            CarregarTipoInstrumento();
        }

        private void CarregarExercicio()
        {
            ddlAnoExercicio.DataSource = this.ObterPGAService().ListarExercicios();
            ddlAnoExercicio.DataTextField = "Ano";
            ddlAnoExercicio.DataValueField = "Ano";
            ddlAnoExercicio.DataBind();

            var ExercicioAtual = this.ObterPGAService().ObterExercicioAtual();
            ddlAnoExercicio.SelectedValue = this.ObterPGAService().ObterExercicioAtual() != null ? ExercicioAtual.Ano.ToString() : DateTime.Now.Year.ToString();
        }

        private void CarregarArea()
        {
            ddlArea.DataSource = this.ObterPGAService().ListarCorporativoUnidadePorExercicio(ddlAnoExercicio.SelectedValue.ToInt32());
            ddlArea.DataTextField = "DescricaoUnidade";
            ddlArea.DataValueField = "CodigoUnidade";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("Todas", ""));
        }

        private void CarregarTipoMetas()
        {
            ddlDescricaoMeta.DataSource = this.ObterPGAService().ListarTipoMetas();
            ddlDescricaoMeta.DataTextField = "DescricaoTipoMeta";
            ddlDescricaoMeta.DataValueField = "CodigoSeqTipoMeta";
            ddlDescricaoMeta.DataBind();
            ddlDescricaoMeta.Items.Insert(0, new ListItem("Todos", ""));
        }

        private void CarregarPeriodo()
        {
            ddlPeriodo.DataSource = this.ObterPGAService().ListarFase()
                    .Where(c => c.CodigoSeqFase == (int)FasePeriodo.PrimeiroTrimestre ||
                                c.CodigoSeqFase == (int)FasePeriodo.SegundoTrimestre ||
                                c.CodigoSeqFase == (int)FasePeriodo.TerceiroTrimestre ||
                                c.CodigoSeqFase == (int)FasePeriodo.QuartoTrimestre);
            ddlPeriodo.DataTextField = "DescricaoFaseAlterada";
            ddlPeriodo.DataValueField = "CodigoSeqFase";
            ddlPeriodo.DataBind();
            ddlPeriodo.Items.Insert(0, new ListItem("Acumulado no Exercício", ""));
        }
        private void CarregarTipoInstrumento()
        {
            ddlTipoInstrumento.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTiposInstrumento();
            ddlTipoInstrumento.DataTextField = "DescricaoTipoInstrumento";
            ddlTipoInstrumento.DataValueField = "CodigoSeqTipoInstrumento";
            ddlTipoInstrumento.DataBind();
            ddlTipoInstrumento.Items.Insert(0, new ListItem("Todos", "0"));
        }
        public void ExportReport(ReportViewerHelper.ReportType reportType)
        {
            try
            {
                BuscarParamentros();
                if (ValidaParametros())
                {
                    byte[] bytes = this.ObterPGAService().ExportReportRelatorioDetalheIndicador(alinhamnetoPE, alinhamnetoPPA, alinhamnetoMI, ddlAnoExercicio.SelectedValue.ToInt32(),
                        ddlArea.SelectedItem.Text, ddlPeriodo.SelectedValue.ToInt32(), ddlDescricaoMeta.SelectedValue.ToInt32(),
                        (UsuarioPublico && !(UsuarioConsultor || UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador)) ? 2 : ddlMetaInterna.SelectedValue.ToInt32(),
                        ddlTipoInstrumento.SelectedValue.ToInt32(), reportType,
                            "", true);

                    if (bytes != null)
                    {
                        string filename = "RelatorioDetalheIndicador";

                        switch (reportType)
                        {
                            case ReportViewerHelper.ReportType.Excel:
                                filename += ".xls";
                                break;
                            case ReportViewerHelper.ReportType.PDF:
                                filename += ".pdf";
                                break;
                            case ReportViewerHelper.ReportType.Word:
                            default:
                                filename += ".doc";
                                break;
                        }

                        WebHelper.DownloadFile(bytes, filename);
                    }
                }
                else
                    MessageBox.ShowInformationMessage("Favor selecionar um tipo de alinhamento");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }
        public void ExportReportSimples(ReportViewerHelper.ReportType reportType)
        {
            try
            {
                BuscarParamentros();
                if (ValidaParametros())
                {
                    byte[] bytes = this.ObterPGAService().ExportReportRelatorioDetalheIndicadorSimples(alinhamnetoPE, alinhamnetoPPA, alinhamnetoMI, ddlAnoExercicio.SelectedValue.ToInt32(),
                        ddlArea.SelectedItem.Text, ddlPeriodo.SelectedValue.ToInt32(), ddlDescricaoMeta.SelectedValue.ToInt32(),
                        (UsuarioPublico && !(UsuarioConsultor || UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador)) ? 2 : ddlMetaInterna.SelectedValue.ToInt32(),
                        ddlTipoInstrumento.SelectedValue.ToInt32(), reportType,
                            "", true);

                    if (bytes != null)
                    {
                        string filename = "RelatorioDetalheIndicadorExtracaoDados.xls";


                        WebHelper.DownloadFile(bytes, filename);
                    }
                }
                else
                    MessageBox.ShowInformationMessage("Favor selecionar um tipo de alinhamento");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }
        #endregion
    }
}