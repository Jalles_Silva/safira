﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using SQFramework.Web.Report;
using SQFramework.Core.Reflection;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using PGA.Common;
using SQFramework.Data.Pagging;

namespace PGA.Presentation.Site.Relatorios
{
    public partial class RelatorioDetalheMeta : CustomPageBase
    {
        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnPesquisar.Click += new EventHandler(btnPesquisar_Click);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            
            btnExportarExcel.Click += new EventHandler(btnExportarExcel_Click);
            lnkReportPDF.Click += new EventHandler(lnkReportPDF_Click);
            lnkReportExcel.Click += new EventHandler(lnkReportExcel_Click);
            lnkReportWord.Click += new EventHandler(lnkReportWord_Click);

            ddlAnoExercicio.SelectedIndexChanged += ddlAnoExercicio_SelectedIndexChanged;
        }

        void ddlAnoExercicio_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarArea();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (UsuarioPublico && !(UsuarioConsultor || UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador))
                {
                    ddlTipoInstrumento.Visible = false;
                    ddlMetaInterna.Visible = false;
                    lblTipoInstrumento.Visible = false;
                    lblMetaInterna.Visible = false;
                }
                else
                {
                    ddlTipoInstrumento.Visible = true;
                    ddlMetaInterna.Visible = true;
                    lblTipoInstrumento.Visible = true;
                    lblMetaInterna.Visible = true;
                }
                if (UsuarioConsultor || UsuarioPublico || UsuarioCadastrador || UsuarioAdministrador)
                {
                    if (!IsPostBack)
                        CarregarControles();
                }
                else
                    WebHelper.LogoffWithNewRedirection("~/Default.aspx");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReportSimples(ReportViewerHelper.ReportType.Excel);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }
        protected void lnkReportPDF_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReport(ReportViewerHelper.ReportType.PDF);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void lnkReportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReport(ReportViewerHelper.ReportType.Excel);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void lnkReportWord_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReport(ReportViewerHelper.ReportType.Word);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            try
            {
                switch (e.Command)
                {
                    case "Recarregar":
                        if (e.Result == MessageBoxResult.OK)
                            btnPesquisar_Click(btnPesquisar, null);
                        break;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                ConsultarDados(0);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            try
            {
                ConsultarDados(e.NewPage - 1);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion

        #region [Methods]

        private bool alinhamnetoPE = false;
        private bool alinhamnetoPPA = false;
        private bool alinhamnetoMI = false;
        private bool metaAdministrativa = false;
        private bool metaFiscalizacao = false;
        private bool metaRegulacao = false;

        private void BuscarParamentros()
        {
            foreach (ListItem item in rbtTipoAlinhamento.Items)
            {
                if (item.Selected)
                {
                    if (item.Value.ToInt32() == (int)TiposAlinhamento.Alinhamento_PE) alinhamnetoPE = true;
                    if (item.Value.ToInt32() == (int)TiposAlinhamento.Alinhamento_PPA) alinhamnetoPPA = true;
                    if (item.Value.ToInt32() == (int)TiposAlinhamento.Alinhamento_MI) alinhamnetoMI = true;
                }
            }

            metaAdministrativa = (ddlDescricaoMeta.SelectedItem.Text == TiposDeMeta.Administrativa.GetDescription()) || (ddlDescricaoMeta.SelectedItem.Text == TiposDeMeta.Todos.GetDescription());
            metaFiscalizacao = (ddlDescricaoMeta.SelectedItem.Text == TiposDeMeta.De_Fiscalizacao.GetDescription()) || (ddlDescricaoMeta.SelectedItem.Text == TiposDeMeta.Todos.GetDescription());
            metaRegulacao = (ddlDescricaoMeta.SelectedItem.Text == TiposDeMeta.De_Regulacao.GetDescription()) || (ddlDescricaoMeta.SelectedItem.Text == TiposDeMeta.Todos.GetDescription());
        }

        private bool ValidaParametros()
        {
            bool retorno = false;
            retorno = (alinhamnetoPE || alinhamnetoPPA || alinhamnetoMI);
            return retorno;
        }

        private void ConsultarDados(int pageIndex)
        {
            try
            {
                BuscarParamentros();

                if (ValidaParametros())
                {
                    PageMessage<DTOMetaRelatorio> dadosPesquisados = this.ObterPGAService().ListaDetalheMeta(alinhamnetoPE, alinhamnetoPPA, alinhamnetoMI,
                        ddlDescricaoMeta.SelectedValue.ToInt32(), ddlArea.SelectedItem.Text, ddlAnoExercicio.SelectedValue.ToInt32(), 
                        ddlMetaInterna.SelectedValue.ToInt32(), ddlTipoInstrumento.SelectedValue.ToInt32(),
                        pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize);

                    List<DTOMetaRelatorio> source = dadosPesquisados.Entities.ToList();

                    List<string> unidades = source.Select(i => i.DescricaoArea).Distinct().ToList();
                    Dictionary<string, IList<DTOMetaRelatorio>> lista = new Dictionary<string, IList<DTOMetaRelatorio>>();
                    List<DTOMetaRelatorio> listaPublica = new List<DTOMetaRelatorio>();

                    if (UsuarioPublico && !(UsuarioConsultor || UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador))
                    {
                        foreach (var item in source)
                        {
                            if (item.MetaInterna == "SIM")
                            {
                                listaPublica.Add(item);
                            }
                        }
                    }
                    else
                    {
                        listaPublica = source;
                    }

                    foreach (var item in unidades)
                    {
                        lista.Add(item, listaPublica.Where(i => i.DescricaoArea == item).OrderBy(x => x.DescricaoMeta).ToList());
                    }

                    

                    rptMetas.DataSource = lista;
                    rptMetas.DataBind();

                    btnExportar.Visible = (dadosPesquisados.RowsCount > 0);
                    rptMetas.Visible = true;

                    ucPaginatorConsulta.Visible = (dadosPesquisados.RowsCount > 0);
                    ucPaginatorConsulta.TotalRecords = dadosPesquisados.RowsCount;
                    ucPaginatorConsulta.PageIndex = pageIndex + 1;
                    ucPaginatorConsulta.DataBind();
                }
                else
                {
                    btnExportar.Visible = false;
                    rptMetas.Visible = false;
                    ucPaginatorConsulta.Visible = false;
                    MessageBox.ShowInformationMessage("Favor selecionar um tipo de alinhamento");
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarControles()
        {
            CarregarExercicio();
            CarregarArea();
            CarregarTipoMetas();
            CarregarTipoInstrumento();
            CarregarPeriodo();
        }

        private void CarregarExercicio()
        {
            ddlAnoExercicio.DataSource = this.ObterPGAService().ListarExercicios();
            ddlAnoExercicio.DataTextField = "Ano";
            ddlAnoExercicio.DataValueField = "Ano";
            ddlAnoExercicio.DataBind();

            var ExercicioAtual = this.ObterPGAService().ObterExercicioAtual();
            ddlAnoExercicio.SelectedValue = this.ObterPGAService().ObterExercicioAtual() != null ? ExercicioAtual.Ano.ToString() : DateTime.Now.Year.ToString();
        }

        private void CarregarArea()
        {
            ddlArea.DataSource = this.ObterPGAService().ListarCorporativoUnidadePorExercicio(ddlAnoExercicio.SelectedValue.ToInt32());
            ddlArea.DataTextField = "DescricaoUnidade";
            ddlArea.DataValueField = "CodigoUnidade";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("Todas", ""));
        }

        private void CarregarTipoMetas()
        {
            ddlDescricaoMeta.DataSource = this.ObterPGAService().ListarTipoMetas();
            ddlDescricaoMeta.DataTextField = "DescricaoTipoMeta";
            ddlDescricaoMeta.DataValueField = "CodigoSeqTipoMeta";
            ddlDescricaoMeta.DataBind();
            ddlDescricaoMeta.Items.Insert(0, new ListItem("Todos", ""));
        }
        private void CarregarTipoInstrumento()
        {
            ddlTipoInstrumento.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTiposInstrumento();
            ddlTipoInstrumento.DataTextField = "DescricaoTipoInstrumento";
            ddlTipoInstrumento.DataValueField = "CodigoSeqTipoInstrumento";
            ddlTipoInstrumento.DataBind();
            ddlTipoInstrumento.Items.Insert(0, new ListItem("Todos", "0"));
        }

        private void CarregarPeriodo()
        {
            ddlPeriodo.DataSource = this.ObterPGAService().ListarFase()
                    .Where(c => c.CodigoSeqFase == (int)FasePeriodo.PrimeiroTrimestre ||
                                c.CodigoSeqFase == (int)FasePeriodo.SegundoTrimestre ||
                                c.CodigoSeqFase == (int)FasePeriodo.TerceiroTrimestre ||
                                c.CodigoSeqFase == (int)FasePeriodo.QuartoTrimestre);
            ddlPeriodo.DataTextField = "DescricaoFaseAlterada";
            ddlPeriodo.DataValueField = "CodigoSeqFase";
            ddlPeriodo.DataBind();
            ddlPeriodo.Items.Insert(0, new ListItem("Acumulado no Exercício", ""));
        }
        public void ExportReport(ReportViewerHelper.ReportType reportType)
        {
            try
            {
                BuscarParamentros();

                // Caso seja usuário público o valor da Meta Interna será sempre o mesmo = SIM, caso não recebe o valor da DDL
                int metaInterna = 0;
                
                if (UsuarioPublico && !(UsuarioConsultor || UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador))
                    metaInterna = 2;
                else
                    metaInterna = ddlMetaInterna.SelectedValue.ToInt32();

                if (ValidaParametros())
                {
                    byte[] bytes = this.ObterPGAService().ExportReportRelatorioDetalheMeta(alinhamnetoPE, alinhamnetoPPA, alinhamnetoMI,
                            ddlDescricaoMeta.SelectedValue.ToInt32(), ddlArea.SelectedItem.Text, ddlAnoExercicio.SelectedValue.ToInt32(), 
                            metaInterna, ddlTipoInstrumento.SelectedValue.ToInt32(), reportType, "", true);

                    if (bytes != null)
                    {
                        string filename = "RelatorioDetalheMeta";

                        switch (reportType)
                        {
                            case ReportViewerHelper.ReportType.Excel:
                                filename += ".xls";
                                break;
                            case ReportViewerHelper.ReportType.PDF:
                                filename += ".pdf";
                                break;
                            case ReportViewerHelper.ReportType.Word:
                            default:
                                filename += ".doc";
                                break;
                        }

                        WebHelper.DownloadFile(bytes, filename);
                    }
                }
                else
                    MessageBox.ShowInformationMessage("Favor selecionar um tipo de alinhamento");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void ExportReportSimples(ReportViewerHelper.ReportType reportType)
        {
            try
            {
                // Caso seja usuário público o valor da Meta Interna será sempre o mesmo = SIM, caso não recebe o valor da DDL
                int metaInterna = 0;

                if (UsuarioPublico && !(UsuarioConsultor || UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador))
                    metaInterna = 2;
                else
                    metaInterna = ddlMetaInterna.SelectedValue.ToInt32();
                BuscarParamentros();
                if (ValidaParametros())
                {
                    byte[] bytes = this.ObterPGAService().ExportReportRelatorioDetalheMetaSimples(alinhamnetoPE, alinhamnetoPPA, alinhamnetoMI,
                            ddlDescricaoMeta.SelectedValue.ToInt32(), ddlArea.SelectedItem.Text, ddlAnoExercicio.SelectedValue.ToInt32(),
                            metaInterna, ddlTipoInstrumento.SelectedValue.ToInt32(), reportType, "", true);

                    if (bytes != null)
                    {
                        string filename = "RelatorioDetalheMetaExtracaoDados.xls";

                        
                        WebHelper.DownloadFile(bytes, filename);
                    }
                }
                else
                    MessageBox.ShowInformationMessage("Favor selecionar um tipo de alinhamento");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}