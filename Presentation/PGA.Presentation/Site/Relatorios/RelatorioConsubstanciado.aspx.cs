﻿using ClosedXML.Excel;
using PGA.Common;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using SQFramework.Core;
using SQFramework.Web;
using SQFramework.Web.Report;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PGA.Presentation.Site.Relatorios
{
    public partial class RelatorioConsubstanciado : CustomPageBase
    {
        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            btnSalvarSumario.Click += new EventHandler(btnSalvarSumario_Click);
            btnSalvarApresentacao.Click += new EventHandler(btnSalvarApresentacao_Click);
            btnSalvarPlanoEstrategico.Click += new EventHandler(btnSalvarPlanoEstrategico_Click);
            btnExportarExcel.Click += new EventHandler(btnExportarExcel_Click);
            lnkReportPDF.Click += new EventHandler(lnkReportPDF_Click);
            lnkReportWord.Click += new EventHandler(lnkReportWord_Click);

            ddlAnoExercicio.SelectedIndexChanged += ddlAnoExercicio_SelectedIndexChanged;
            ddlObjetivoEstrategicoPE.SelectedIndexChanged += ddlObjetivoEstrategicoPE_SelectedIndexChanged;
            ddlObjetivoEstrategicoPPA.SelectedIndexChanged += ddlObjetivoEstrategicoPPA_SelectedIndexChanged;
            ddlObjetivoEstrategicoPeDetalhado.SelectedIndexChanged += ddlObjetivoEstrategicoPeDetalhado_SelectedIndexChanged;
            ddlObjetivoEstrategicoPPaDetalhado.SelectedIndexChanged += ddlObjetivoEstrategicoPPaDetalhado_SelectedIndexChanged;
            ddlInstrumentos.SelectedIndexChanged += ddlInstrumentos_SelectedIndexChanged;
            ddlInstrumentoDetalhado.SelectedIndexChanged += ddlInstrumentoDetalhado_SelectedIndexChanged;
            ddlAlinhamentos.SelectedIndexChanged += ddlAlinhamentos_SelectedIndexChanged;
            ddlAlinhamentoDetalhado.SelectedIndexChanged += ddlAlinhamentoDetalhado_SelectedIndexChanged;
            ddlTipoMeta.SelectedIndexChanged += ddlTipoMeta_SelectedIndexChanged;
            ddlTipoMetaDetalhado.SelectedIndexChanged += ddlTipoMetaDetalhado_SelectedIndexChanged;
            ddlObjetivoEstrategico.SelectedIndexChanged += ddlObjetivoEstrategico_SelectedIndexChanged;
            ddlObjetivoEstrategicoDetalhe.SelectedIndexChanged += ddlObjetivoEstrategicoDetalhe_SelectedIndexChanged;
        }

        private void ddlObjetivoEstrategicoPE_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarGrids();
            CarregarGridsDetalhadadas();
        }

        private void ddlObjetivoEstrategicoPeDetalhado_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarGridsDetalhadadas();
        }

        private void ddlObjetivoEstrategicoPPA_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarGrids();
            CarregarGridsDetalhadadas();
        }

        private void ddlObjetivoEstrategicoPPaDetalhado_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarGridsDetalhadadas();
        }

        private void ddlInstrumentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarGrids();
            CarregarGridsDetalhadadas();
        }

        private void ddlInstrumentoDetalhado_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarGridsDetalhadadas();
        }

        private void ddlAlinhamentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarGrids();
            CarregarGridsDetalhadadas();
        }

        private void ddlAlinhamentoDetalhado_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarGridsDetalhadadas();
        }

        private void ddlTipoMeta_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarGrids();
            CarregarGridsDetalhadadas();
        }

        private void ddlTipoMetaDetalhado_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarGridsDetalhadadas();
        }

        private void ddlObjetivoEstrategico_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarGrids();
            CarregarGridsDetalhadadas();
        }

        private void ddlObjetivoEstrategicoDetalhe_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarGridsDetalhadadas();
        }

        private void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "SalvarSumarioExecutivo":
                    if (e.Result == MessageBoxResult.Yes)
                        Salvar(EnumTipoTexto.SumarioExecutivo, hdfCodigoSeqSumarioExecutivo.Value, txtDescSumarioExecutivo.Text);
                    break;
                case "SalvarApresentacao":
                    if (e.Result == MessageBoxResult.Yes)
                        Salvar(EnumTipoTexto.Apresentacao, hdfCodigoSeqApresentacao.Value, txtDescApresentacao.Text);
                    break;
                case "SalvarPlanoEstrategicoPlanoGestaoAnual":
                    if (e.Result == MessageBoxResult.Yes)
                        Salvar(EnumTipoTexto.PlanoEstrategicoPlanoGestaoAnual, hdfCodigoSeqPlanoEstrategicoPlanoGestaoAnual.Value, txtDescPlanoEstrategico.Text);
                    break;
                case "FecharModal":
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", string.Format("$('#modal{0}').modal('hide');", e.Parameters["TipoTexto"]), true);
                    break;
            }
        }

        private void ddlAnoExercicio_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarControles(true);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                    CarregarControles(false);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void btnSalvarSumario_Click(object sender, EventArgs e)
        {
            PreSalvar(EnumTipoTexto.SumarioExecutivo, txtDescSumarioExecutivo.Text);
        }

        protected void btnSalvarApresentacao_Click(object sender, EventArgs e)
        {
            PreSalvar(EnumTipoTexto.Apresentacao, txtDescApresentacao.Text);
        }

        protected void btnSalvarPlanoEstrategico_Click(object sender, EventArgs e)
        {
            PreSalvar(EnumTipoTexto.PlanoEstrategicoPlanoGestaoAnual, txtDescPlanoEstrategico.Text);
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                GerarExportacaoExcel();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void lnkReportPDF_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReport(ReportViewerHelper.ReportType.PDF);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void lnkReportWord_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReport(ReportViewerHelper.ReportType.Word);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion

        #region [Methods]
        private void CarregarControles(bool evento)
        {
            if (!evento)
                CarregarExercicio();

            LimparCampos();
            CarregarCombos();
            CarregarGrids();
            CarregarGridsDetalhadadas();
            CarregarRegistro();
        }

        private void CarregarGrids()
        {
            GridAreas();
            OcultarComponentesTela(Convert.ToInt32(hdfUnidades.Value) > 0);

            if (Convert.ToInt32(hdfUnidades.Value) > 0)
            {
                GridResultadoAlcancado();
                GridTotalObjetivoEstrategicoPE();
                GridTotalObjetivoEstrategicoPPA();
                GridTotalMeta();
                GridTotalInstrumentos();
                GridTotalAlinhamentos();
                GridTotalTipoMeta();
                GridTotalObjetivoEstrategico();
            }
        }

        public void CarregarGridsDetalhadadas()
        {
            GridDetalheObjetivoEstrategicoPE();
            GridDetalheObjetivoEstrategicoPPA();
            GridDetalheMeta();
            GridDetalheInstrumentos();
            GridDetalheAlinhamentos();
            GridDetalheTipoMeta();
            GridDetalheObjetivoEstrategico();
        }

        private void OcultarComponentesTela(bool visivel)
        {
            arObjetivoEstrategicoPE.Visible = visivel;
            arObjetivoEstrategicoPPA.Visible = visivel;
            arMetasTotais.Visible = visivel;
            arInstrumentosTotais.Visible = visivel;
            arAlinhamentosTotais.Visible = visivel;
            arTipoMetaTotal.Visible = visivel;
            arObjetivoEstrategico.Visible = visivel;
            btnExportar.Visible = visivel;
            botaoSalvar.Visible = visivel;
        }

        private void CarregarCombos()
        {
            CarregarObjetivoEstrategicoPE();
            CarregarObjetivoEstrategicoPeDetalhado();
            CarregarObjetivoEstrategicoPPA();
            CarregarObjetivoEstrategicoPPaDetalhado();
            CarregarInstrumentos();
            CarregarInstrumentosDetalhado();
            CarregarIniciativa();
            CarregarIniciativaDetalhado();
            CarregarTipoMeta();
            CarregarTipoMetaDetalhado();
            CarregarObjetivoEstrategico();
            CarregarObjetivoEstrategicoDetalhado();
        }

        private void LimparCampos()
        {
            txtDescSumarioExecutivo.Text = string.Empty;
            hdfCodigoSeqSumarioExecutivo.Value = null;
            txtDescApresentacao.Text = string.Empty;
            hdfCodigoSeqApresentacao.Value = null;
            txtDescPlanoEstrategico.Text = string.Empty;
            hdfCodigoSeqPlanoEstrategicoPlanoGestaoAnual.Value = null;
        }

        private void CarregarExercicio()
        {
            try
            {
                ddlAnoExercicio.DataSource = this.ObterPGAService().ListarExercicios();
                ddlAnoExercicio.DataTextField = "Ano";
                ddlAnoExercicio.DataValueField = "Ano";
                ddlAnoExercicio.DataBind();
                ddlAnoExercicio.SelectedValue = this.ObterPGAService().ObterExercicioAtual() != null ? this.ObterPGAService().ObterExercicioAtual().Ano.ToString() : DateTime.Now.Year.ToString();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarObjetivoEstrategicoPE()
        {
            try
            {
                ddlObjetivoEstrategicoPE.DataSource = this.ObterPGAService().ListarOjetivoEstrategico((int)TiposAlinhamento.Alinhamento_PE).OrderBy(x => x.DescricaoObjetivo);
                ddlObjetivoEstrategicoPE.DataTextField = "DescricaoObjetivo";
                ddlObjetivoEstrategicoPE.DataValueField = "CodSeqObjetivo";
                ddlObjetivoEstrategicoPE.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarObjetivoEstrategicoPeDetalhado()
        {
            try
            {
                ddlObjetivoEstrategicoPeDetalhado.DataSource = this.ObterPGAService().ListarOjetivoEstrategico((int)TiposAlinhamento.Alinhamento_PE).OrderBy(x => x.DescricaoObjetivo);
                ddlObjetivoEstrategicoPeDetalhado.DataTextField = "DescricaoObjetivo";
                ddlObjetivoEstrategicoPeDetalhado.DataValueField = "CodSeqObjetivo";
                ddlObjetivoEstrategicoPeDetalhado.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarObjetivoEstrategicoPPA()
        {
            try
            {
                ddlObjetivoEstrategicoPPA.DataSource = this.ObterPGAService().ListarOjetivoEstrategico((int)TiposAlinhamento.Alinhamento_PPA).OrderBy(x => x.DescricaoObjetivo);
                ddlObjetivoEstrategicoPPA.DataTextField = "DescricaoObjetivo";
                ddlObjetivoEstrategicoPPA.DataValueField = "CodSeqObjetivo";
                ddlObjetivoEstrategicoPPA.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarObjetivoEstrategicoPPaDetalhado()
        {
            try
            {
                ddlObjetivoEstrategicoPPaDetalhado.DataSource = this.ObterPGAService().ListarOjetivoEstrategico((int)TiposAlinhamento.Alinhamento_PPA).OrderBy(x => x.DescricaoObjetivo);
                ddlObjetivoEstrategicoPPaDetalhado.DataTextField = "DescricaoObjetivo";
                ddlObjetivoEstrategicoPPaDetalhado.DataValueField = "CodSeqObjetivo";
                ddlObjetivoEstrategicoPPaDetalhado.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarObjetivoEstrategico()
        {
            try
            {
                ddlObjetivoEstrategico.DataSource = this.ObterPGAService().ListarOjetivoEstrategicoEMissaoAdministrativa();
                ddlObjetivoEstrategico.DataTextField = "DescricaoObjetivo";
                ddlObjetivoEstrategico.DataValueField = "CodSeqObjetivo";
                ddlObjetivoEstrategico.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarObjetivoEstrategicoDetalhado()
        {
            try
            {
                ddlObjetivoEstrategicoDetalhe.DataSource = this.ObterPGAService().ListarOjetivoEstrategicoEMissaoAdministrativa();
                ddlObjetivoEstrategicoDetalhe.DataTextField = "DescricaoObjetivo";
                ddlObjetivoEstrategicoDetalhe.DataValueField = "CodSeqObjetivo";
                ddlObjetivoEstrategicoDetalhe.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarInstrumentos()
        {
            try
            {
                ddlInstrumentos.DataSource = this.ObterPGAService().ListarTiposInstrumento();
                ddlInstrumentos.DataTextField = "DescricaoTipoInstrumento";
                ddlInstrumentos.DataValueField = "CodigoSeqTipoInstrumento";
                ddlInstrumentos.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarInstrumentosDetalhado()
        {
            try
            {
                ddlInstrumentoDetalhado.DataSource = this.ObterPGAService().ListarTiposInstrumento();
                ddlInstrumentoDetalhado.DataTextField = "DescricaoTipoInstrumento";
                ddlInstrumentoDetalhado.DataValueField = "CodigoSeqTipoInstrumento";
                ddlInstrumentoDetalhado.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarIniciativa()
        {
            try
            {
                ddlAlinhamentos.DataSource = this.ObterPGAService().ListarTiposIniciativa();
                ddlAlinhamentos.DataTextField = "DescricaoTipoIniciativa";
                ddlAlinhamentos.DataValueField = "CodigoSeqTipoIniciativa";
                ddlAlinhamentos.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarIniciativaDetalhado()
        {
            try
            {
                ddlAlinhamentoDetalhado.DataSource = this.ObterPGAService().ListarTiposIniciativa();
                ddlAlinhamentoDetalhado.DataTextField = "DescricaoTipoIniciativa";
                ddlAlinhamentoDetalhado.DataValueField = "CodigoSeqTipoIniciativa";
                ddlAlinhamentoDetalhado.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarTipoMeta()
        {
            try
            {
                ddlTipoMeta.DataSource = this.ObterPGAService().ListarTipoMetas();
                ddlTipoMeta.DataTextField = "DescricaoTipoMeta";
                ddlTipoMeta.DataValueField = "CodigoSeqTipoMeta";
                ddlTipoMeta.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarTipoMetaDetalhado()
        {
            try
            {
                ddlTipoMetaDetalhado.DataSource = this.ObterPGAService().ListarTipoMetas();
                ddlTipoMetaDetalhado.DataTextField = "DescricaoTipoMeta";
                ddlTipoMetaDetalhado.DataValueField = "CodigoSeqTipoMeta";
                ddlTipoMetaDetalhado.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridAreas()
        {
            try
            {
                var areasLista = this.ObterPGAService().ListarCorporativoResultadosAlcancados(ddlAnoExercicio.SelectedValue.ToInt32());
                hdfUnidades.Value = areasLista.Count().ToString();

                if (areasLista.Count() > 0)
                {
                    HtmlTableRow tr = new HtmlTableRow();

                    foreach (var item in areasLista)
                    {
                        HtmlTableCell td = new HtmlTableCell();
                        td.InnerText = item.DescricaoUnidade;
                        td.Style.Add("background-color", item.ResultadosInformados ? "#D8EADE" : "#F9F585");
                        tr.Controls.Add(td);
                    }

                    tbAreas.Rows.Add(tr);
                }
                else
                {
                    HtmlTableRow tr = new HtmlTableRow();
                    HtmlTableCell td = new HtmlTableCell();
                    td.InnerText = "Resultado não encontrado";
                    tr.Controls.Add(td);
                    tbAreas.Rows.Add(tr);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridResultadoAlcancado()
        {
            try
            {
                tbResultadoAlcancado.Visible = true;

                var listaResultadoAlcancado = this.ObterPGAService().ListarResultadosAlcancadosPorExercicio(ddlAnoExercicio.SelectedValue.ToInt32());

                if (listaResultadoAlcancado.Count() > 0)
                {
                    List<string> descricaoObjetivoIniciativa = listaResultadoAlcancado.Select(i => i.DescricaoObjetivoIniciativa).Distinct().ToList();

                    foreach (var itemObjetivoIniciativa in descricaoObjetivoIniciativa)
                    {
                        TableHeaderRow headerRow1 = new TableHeaderRow();
                        TableHeaderCell headerTableCell = new TableHeaderCell();
                        headerTableCell.Text = itemObjetivoIniciativa;
                        headerRow1.Cells.Add(headerTableCell);
                        tbResultadoAlcancado.Rows.Add(headerRow1);

                        foreach (var item in listaResultadoAlcancado.Where(x=> x.DescricaoObjetivoIniciativa == itemObjetivoIniciativa))
                        {
                            TableHeaderRow headerRow2 = new TableHeaderRow();
                            TableCell tableCell2 = new TableCell();
                            tableCell2.Text = item.DescricaoUnidade;
                            tableCell2.Style.Add("background-color", "#D8EADE");
                            headerRow2.Cells.Add(tableCell2);
                            tbResultadoAlcancado.Rows.Add(headerRow2);

                            TableRow tableRow = new TableRow();
                            TableCell tableCell3 = new TableCell();
                            tableCell3.Text = item.ResultadoAlcancadoRegistrado;
                            tableCell3.Style.Add("padding", "30px");
                            tableRow.Cells.Add(tableCell3);
                            tbResultadoAlcancado.Rows.Add(tableRow);
                        }
                    }
                }
                else
                {
                    TableRow tableRow = new TableRow();
                    TableCell tableCell = new TableCell();
                    tableCell.Text = "Resultado não encontrado";
                    tableRow.Cells.Add(tableCell);
                    tbResultadoAlcancado.Rows.Add(tableRow);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridTotalObjetivoEstrategicoPE()
        {
            try
            {
                var dadosPesquisados = this.ObterPGAService().ListarObjetivoEstrategicoPE(ddlAnoExercicio.SelectedValue.ToInt32(), ddlObjetivoEstrategicoPE.SelectedValue.ToInt32());
                tbObjetivoEstrategicoPE.Rows.Add(CriarTabelaRow(ContabilizarTotal(dadosPesquisados)));
                CentralizarCelula(tbObjetivoEstrategicoPE);
                ddlObjetivoEstrategicoPeDetalhado.SelectedValue = ddlObjetivoEstrategicoPE.SelectedValue;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridDetalheObjetivoEstrategicoPE()
        {
            try
            {
                foreach (var item in ContabilizarTotalDetalhado(this.ObterPGAService().ListarObjetivoEstrategicoPE(ddlAnoExercicio.SelectedValue.ToInt32(), ddlObjetivoEstrategicoPeDetalhado.SelectedValue.ToInt32())))
                {
                    var tableRow = CriarTabelaRow(item);
                    tbObjetivosPeDetalhado.Rows.Add(tableRow);
                }

                CentralizarCelula(tbObjetivosPeDetalhado);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridTotalObjetivoEstrategicoPPA()
        {
            try
            {
                var dadosPesquisados = this.ObterPGAService().ListarObjetivoEstrategicoPPA(ddlAnoExercicio.SelectedValue.ToInt32(), ddlObjetivoEstrategicoPPA.SelectedValue.ToInt32());
                tbObjetivoEstrategicoPPA.Rows.Add(CriarTabelaRow(ContabilizarTotal(dadosPesquisados)));
                CentralizarCelula(tbObjetivoEstrategicoPPA);
                ddlObjetivoEstrategicoPPaDetalhado.SelectedValue = ddlObjetivoEstrategicoPPA.SelectedValue;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridDetalheObjetivoEstrategicoPPA()
        {
            try
            {
                foreach (var item in ContabilizarTotalDetalhado(this.ObterPGAService().ListarObjetivoEstrategicoPPA(ddlAnoExercicio.SelectedValue.ToInt32(), ddlObjetivoEstrategicoPPaDetalhado.SelectedValue.ToInt32())))
                {
                    var tableRow = CriarTabelaRow(item);
                    tbObjetivosPPaDetalhado.Rows.Add(tableRow);
                }

                CentralizarCelula(tbObjetivosPPaDetalhado);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridTotalMeta()
        {
            try
            {
                var dadosPesquisados = this.ObterPGAService().ListarMeta(ddlAnoExercicio.SelectedValue.ToInt32(), 0);
                tbMetasTotais.Rows.Add(CriarTabelaMetaRow(ContabilizarTotal(dadosPesquisados)));
                CentralizarCelula(tbMetasTotais);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridDetalheMeta()
        {
            try
            {
                var dadosPesquisados = this.ObterPGAService().ListarMeta(ddlAnoExercicio.SelectedValue.ToInt32(), 0);
                foreach (var item in ContabilizarTotalDetalhado(dadosPesquisados))
                {
                    var tableRow = CriarTabelaMetaRow(item);
                    tbMetasDetalhado.Rows.Add(tableRow);
                }

                CentralizarCelula(tbMetasDetalhado);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridTotalInstrumentos()
        {
            try
            {
                var dadosPesquisados = this.ObterPGAService().ListarInstrumentos(ddlAnoExercicio.SelectedValue.ToInt32(), ddlInstrumentos.SelectedValue.ToInt32());
                tbInstrumentosTotais.Rows.Add(CriarTabelaRow(ContabilizarTotal(dadosPesquisados)));
                CentralizarCelula(tbInstrumentosTotais);
                ddlInstrumentoDetalhado.SelectedValue = ddlInstrumentos.SelectedValue;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridDetalheInstrumentos()
        {
            try
            {
                var dadosPesquisados = this.ObterPGAService().ListarInstrumentos(ddlAnoExercicio.SelectedValue.ToInt32(), ddlInstrumentoDetalhado.SelectedValue.ToInt32());
                foreach (var item in ContabilizarTotalDetalhado(dadosPesquisados))
                {
                    var tableRow = CriarTabelaRow(item);
                    tbInstrumentoDetalhado.Rows.Add(tableRow);
                }

                CentralizarCelula(tbInstrumentoDetalhado);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridTotalAlinhamentos()
        {
            try
            {
                var dadosPesquisados = this.ObterPGAService().ListarIniciativa(ddlAnoExercicio.SelectedValue.ToInt32(), ddlAlinhamentos.SelectedValue.ToInt32());
                tbAlinhamentosTotais.Rows.Add(CriarTabelaRow(ContabilizarTotal(dadosPesquisados)));
                CentralizarCelula(tbAlinhamentosTotais);
                ddlAlinhamentoDetalhado.SelectedValue = ddlAlinhamentos.SelectedValue;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridDetalheAlinhamentos()
        {
            try
            {
                var dadosPesquisados = this.ObterPGAService().ListarIniciativa(ddlAnoExercicio.SelectedValue.ToInt32(), ddlAlinhamentoDetalhado.SelectedValue.ToInt32());
                foreach (var item in ContabilizarTotalDetalhado(dadosPesquisados))
                {
                    var tableRow = CriarTabelaRow(item);
                    tbAlinhamentoDetalhado.Rows.Add(tableRow);
                }

                CentralizarCelula(tbAlinhamentoDetalhado);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridTotalTipoMeta()
        {
            try
            {
                var dadosPesquisados = this.ObterPGAService().ListarMeta(ddlAnoExercicio.SelectedValue.ToInt32(), ddlTipoMeta.SelectedValue.ToInt32());
                tbTipoMetaTotal.Rows.Add(CriarTabelaRow(ContabilizarTotal(dadosPesquisados)));
                CentralizarCelula(tbTipoMetaTotal);
                ddlTipoMetaDetalhado.SelectedValue = ddlTipoMeta.SelectedValue;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridDetalheTipoMeta()
        {
            try
            {
                var dadosPesquisados = this.ObterPGAService().ListarMeta(ddlAnoExercicio.SelectedValue.ToInt32(), ddlTipoMetaDetalhado.SelectedValue.ToInt32());
                foreach (var item in ContabilizarTotalDetalhado(dadosPesquisados))
                {
                    var tableRow = CriarTabelaRow(item);
                    tbTipoMetaDetalhado.Rows.Add(tableRow);
                }

                CentralizarCelula(tbTipoMetaDetalhado);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridTotalObjetivoEstrategico()
        {
            try
            {
                string[] parametro = ddlObjetivoEstrategico.SelectedValue.Split('|');
                int codObjetivo = int.Parse(parametro[0]);
                int codTipo = int.Parse(parametro[1]);

                var dadosPesquisados = this.ObterPGAService().ListarObjetivoEstrategicoTotal(ddlAnoExercicio.SelectedValue.ToInt32(), codObjetivo, codTipo);
                tbObjetivoEstrategico.Rows.Add(CriarTabelaRowObjetivoEstrategico(ContabilizarObjetivoEstrategico(dadosPesquisados)));
                CentralizarCelulaTotalizador(tbObjetivoEstrategico);

                ddlObjetivoEstrategicoDetalhe.SelectedValue = ddlObjetivoEstrategico.SelectedValue;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void GridDetalheObjetivoEstrategico()
        {
            try
            {
                string[] parametro = ddlObjetivoEstrategicoDetalhe.SelectedValue.Split('|');
                int codObjetivo = int.Parse(parametro[0]);
                int codTipo = int.Parse(parametro[1]);

                var dadosPesquisados = this.ObterPGAService().ListarObjetivoEstrategicoTotal(ddlAnoExercicio.SelectedValue.ToInt32(), codObjetivo, codTipo);
                foreach (var item in ContabilizarObjetivoEstrategicoPorUnidade(dadosPesquisados))
                {
                    var tableRow = CriarTabelaRowObjetivoEstrategico(item);
                    tbObjetivoEstrategicoDetalhe.Rows.Add(tableRow);
                }

                CentralizarCelulaTotalizador(tbObjetivoEstrategicoDetalhe);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                var exercicio = this.ObterPGAService().ObterFaseExercicioPorAno(ddlAnoExercicio.SelectedValue.ToInt16());

                foreach (var item in this.ObterPGAService().ListarRelatorioConsubstanciado(exercicio.CodigoSeqExercicio))
                {
                    if (item.TipoTexto.CodigoSeqTipoTexto == (int)EnumTipoTexto.SumarioExecutivo)
                    {
                        txtDescSumarioExecutivo.Text = item.DescricaoRelatorioConsubstanciado;
                        hdfCodigoSeqSumarioExecutivo.Value = item.CodigoSeqRelatorioConsubstanciado.ToString();
                    }
                    if (item.TipoTexto.CodigoSeqTipoTexto == (int)EnumTipoTexto.Apresentacao)
                    {
                        txtDescApresentacao.Text = item.DescricaoRelatorioConsubstanciado;
                        hdfCodigoSeqApresentacao.Value = item.CodigoSeqRelatorioConsubstanciado.ToString();
                    }
                    if (item.TipoTexto.CodigoSeqTipoTexto == (int)EnumTipoTexto.PlanoEstrategicoPlanoGestaoAnual)
                    {
                        txtDescPlanoEstrategico.Text = item.DescricaoRelatorioConsubstanciado;
                        hdfCodigoSeqPlanoEstrategicoPlanoGestaoAnual.Value = item.CodigoSeqRelatorioConsubstanciado.ToString();
                    }
                }

                btnSalvarSumario.Visible = !(exercicio.Fase.CodigoSeqFase == (int)FaseEnum.Encerrado);
                btnSalvarApresentacao.Visible = !(exercicio.Fase.CodigoSeqFase == (int)FaseEnum.Encerrado);
                btnSalvarPlanoEstrategico.Visible = !(exercicio.Fase.CodigoSeqFase == (int)FaseEnum.Encerrado);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private DTORelatorioConsubstanciadoDetalhado ContabilizarTotal(IList<DTORelatorioObjetivoEstrategico> dadosPesquisados)
        {
            var dto = new DTORelatorioConsubstanciadoDetalhado()
            {
                DescricaoUnidade = "ANTT"
            };

            List<int> meta = dadosPesquisados.Select(i => i.CodSeqMeta).Distinct().ToList();

            foreach (var cod in meta)
            {
                if (cod > 0)
                {
                    ++dto.TotalMetas;

                    decimal resultadoIndicador = dadosPesquisados.Where(i => i.CodSeqMeta == cod).Sum(x => x.ResultadoIndicador);
                    decimal percentualExecucao = dadosPesquisados.Where(i => i.CodSeqMeta == cod).Sum(x => x.PercentualExecucao);

                    if (resultadoIndicador.Equals(100) && percentualExecucao.Equals(100))
                        ++dto.CemMetas;
                    else if ((resultadoIndicador >= 90 && resultadoIndicador < 100) && (percentualExecucao >= 90 && percentualExecucao < 100))
                        ++dto.NoventaMetas;
                }
            }

            List<int> acao = dadosPesquisados.Select(i => i.CodSeqAcao).Distinct().ToList();

            foreach (var cod in acao)
            {
                if (cod > 0)
                {
                    ++dto.TotalAtividades;

                    decimal percentualExecucao = dadosPesquisados.Where(i => i.CodSeqAcao == cod).Sum(x => x.PercentualExecucao);

                    if (percentualExecucao.Equals(100))
                        ++dto.CemAtividades;
                    else if (percentualExecucao >= 90 && percentualExecucao < 100)
                        ++dto.NoventaAtividades;
                }
            }

            List<int> indicador = dadosPesquisados.Select(i => i.CodSeqIndicador).Distinct().ToList();

            foreach (var cod in indicador)
            {
                if (cod > 0)
                {
                    ++dto.TotalIndicadores;

                    decimal resultadoIndicador = dadosPesquisados.Where(i => i.CodSeqIndicador == cod).Sum(x => x.ResultadoIndicador);

                    if (resultadoIndicador.Equals(100))
                        ++dto.CemIndicadores;
                    else if (resultadoIndicador >= 90 && resultadoIndicador < 100)
                        ++dto.NoventaIndicadores;
                }
            }

            return dto;
        }

        private DTOObjetivoEstrategicoDetalhado ContabilizarObjetivoEstrategico(IList<DTOObjetivoEstrategicoTotalizadorDetalhadado> dadosPesquisados)
        {
            var dtoRelatorio = new DTOObjetivoEstrategicoDetalhado()
            {
                DescricaoUnidade = "ANTT",
                RecursosPrevistosAssociados = dadosPesquisados.Sum(x => x.TotalRecursoEstimado),
                RecursosPrevistosRoteados = dadosPesquisados.Sum(x => x.TotalRecursoEstimadoTotal),
                RecursosUtilizadosAssociados = dadosPesquisados.Sum(x => x.TotalRecursoOrcamentarioUtilizado),
                RecursosUtilizadosRoteados = dadosPesquisados.Sum(x => x.TotalRecursoOrcamentarioUtilizadoTotal)
            };

            return dtoRelatorio;
        }

        private IList<DTOObjetivoEstrategicoDetalhado> ContabilizarObjetivoEstrategicoDetalhado(IList<DTOObjetivoEstrategicoTotalizadorDetalhadado> dadosPesquisados)
        {
            IList<DTOObjetivoEstrategicoDetalhado> lista = new List<DTOObjetivoEstrategicoDetalhado>();
            List<string> objetivos = dadosPesquisados.Select(i => i.DescricaoObjetivo).Distinct().ToList();

            if (objetivos.Count().Equals(0))
            {
                DTOObjetivoEstrategicoDetalhado dtoRelatorio = new DTOObjetivoEstrategicoDetalhado();
                dtoRelatorio.DescricaoObjetivo = "-";
                lista.Add(dtoRelatorio);
            }
            else
            {
                foreach (var itemDescricaoObjetivo in objetivos.Distinct())
                {
                    foreach (var item in dadosPesquisados.Where(x => x.DescricaoObjetivo == itemDescricaoObjetivo))
                    {
                        DTOObjetivoEstrategicoDetalhado dtoRelatorio = new DTOObjetivoEstrategicoDetalhado()
                        {
                            DescricaoObjetivo = itemDescricaoObjetivo,
                            DescricaoUnidade = item.DescricaoUnidade,
                            RecursosPrevistosAssociados = dadosPesquisados.Where(x => x.DescricaoObjetivo == itemDescricaoObjetivo).Sum(x => x.TotalRecursoEstimado),
                            RecursosPrevistosRoteados = dadosPesquisados.Where(x => x.DescricaoObjetivo == itemDescricaoObjetivo).Sum(x => x.TotalRecursoEstimadoTotal),
                            RecursosUtilizadosAssociados = dadosPesquisados.Where(x => x.DescricaoObjetivo == itemDescricaoObjetivo).Sum(x => x.TotalRecursoOrcamentarioUtilizado),
                            RecursosUtilizadosRoteados = dadosPesquisados.Where(x => x.DescricaoObjetivo == itemDescricaoObjetivo).Sum(x => x.TotalRecursoOrcamentarioUtilizadoTotal)
                        };

                        lista.Add(dtoRelatorio);
                    }
                }
            }

            return lista;
        }

        private IList<DTOObjetivoEstrategicoDetalhado> ContabilizarObjetivoEstrategicoPorUnidade(IList<DTOObjetivoEstrategicoTotalizadorDetalhadado> dadosPesquisados)
        {
            IList<DTOObjetivoEstrategicoDetalhado> lista = new List<DTOObjetivoEstrategicoDetalhado>();
            List<string> unidades = dadosPesquisados.Select(i => i.DescricaoUnidade).Distinct().ToList();

            if (unidades.Count().Equals(0))
            {
                DTOObjetivoEstrategicoDetalhado dtoRelatorio = new DTOObjetivoEstrategicoDetalhado();
                dtoRelatorio.DescricaoUnidade = "-";
                lista.Add(dtoRelatorio);
            }
            else
            {
                foreach (var itemDescricaoUnidade in unidades.Distinct())
                {
                    DTOObjetivoEstrategicoDetalhado dtoRelatorio = new DTOObjetivoEstrategicoDetalhado()
                    {
                        DescricaoUnidade = itemDescricaoUnidade,
                        RecursosPrevistosAssociados = dadosPesquisados.Where(x => x.DescricaoUnidade == itemDescricaoUnidade).Sum(x => x.TotalRecursoEstimado),
                        RecursosPrevistosRoteados = dadosPesquisados.Where(x => x.DescricaoUnidade == itemDescricaoUnidade).Sum(x => x.TotalRecursoEstimadoTotal),
                        RecursosUtilizadosAssociados = dadosPesquisados.Where(x => x.DescricaoUnidade == itemDescricaoUnidade).Sum(x => x.TotalRecursoOrcamentarioUtilizado),
                        RecursosUtilizadosRoteados = dadosPesquisados.Where(x => x.DescricaoUnidade == itemDescricaoUnidade).Sum(x => x.TotalRecursoOrcamentarioUtilizadoTotal)
                    };

                    lista.Add(dtoRelatorio);
                }
            }
            return lista;
        }

        private IList<DTORelatorioConsubstanciadoDetalhado> ContabilizarTotalDetalhado(IList<DTORelatorioObjetivoEstrategico> dadosPesquisados)
        {
            IList<DTORelatorioConsubstanciadoDetalhado> lista = new List<DTORelatorioConsubstanciadoDetalhado>();
            List<string> unidades = dadosPesquisados.Select(i => i.DescricaoUnicade).Distinct().ToList();

            if (unidades.Count().Equals(0))
            {
                DTORelatorioConsubstanciadoDetalhado dtoRelatorio = new DTORelatorioConsubstanciadoDetalhado();
                dtoRelatorio.DescricaoUnidade = "-";
                lista.Add(dtoRelatorio);
            }
            else
            {
                foreach (var itemUnidade in unidades)
                {
                    DTORelatorioConsubstanciadoDetalhado dto = new DTORelatorioConsubstanciadoDetalhado();

                    dto.DescricaoUnidade = itemUnidade;
                    dto.DescricaoTipo = dadosPesquisados.Where(x => x.DescricaoUnicade == itemUnidade).Select(i => i.DescricaoTipo).FirstOrDefault();

                    List<int> meta = dadosPesquisados.Where(x => x.DescricaoUnicade == itemUnidade).Select(i => i.CodSeqMeta).Distinct().ToList();

                    foreach (var cod in meta)
                    {
                        if (cod > 0)
                        {
                            ++dto.TotalMetas;

                            decimal resultadoIndicador = dadosPesquisados.Where(i => i.CodSeqMeta == cod && i.DescricaoUnicade == itemUnidade).Sum(x => x.ResultadoIndicador);
                            decimal percentualExecucao = dadosPesquisados.Where(i => i.CodSeqMeta == cod && i.DescricaoUnicade == itemUnidade).Sum(x => x.PercentualExecucao);

                            if (resultadoIndicador == 100 && percentualExecucao == 100)
                                ++dto.CemMetas;
                            else if ((resultadoIndicador >= 90 && resultadoIndicador < 100) && (percentualExecucao >= 90 && percentualExecucao < 100))
                                ++dto.NoventaMetas;
                        }
                    }

                    List<int> acao = dadosPesquisados.Where(i => i.DescricaoUnicade == itemUnidade).Select(i => i.CodSeqAcao).Distinct().ToList();

                    foreach (var cod in acao)
                    {
                        if (cod > 0)
                        {
                            ++dto.TotalAtividades;

                            decimal percentualExecucao = dadosPesquisados.Where(i => i.CodSeqAcao == cod && i.DescricaoUnicade == itemUnidade).Sum(x => x.PercentualExecucao);

                            if (percentualExecucao == 100)
                                ++dto.CemAtividades;
                            else if (percentualExecucao >= 90 && percentualExecucao < 100)
                                ++dto.NoventaAtividades;
                        }
                    }

                    List<int> indicador = dadosPesquisados.Where(i => i.DescricaoUnicade == itemUnidade).Select(i => i.CodSeqIndicador).Distinct().ToList();

                    foreach (var cod in indicador)
                    {
                        if (cod > 0)
                        {
                            ++dto.TotalIndicadores;

                            decimal resultadoIndicador = dadosPesquisados.Where(i => i.CodSeqIndicador == cod && i.DescricaoUnicade == itemUnidade).Sum(x => x.ResultadoIndicador);

                            if (resultadoIndicador == 100)
                                ++dto.CemIndicadores;
                            else if (resultadoIndicador >= 90 && resultadoIndicador < 100)
                                ++dto.NoventaIndicadores;
                        }
                    }

                    lista.Add(dto);
                }
            }

            return lista;
        }

        private TableRow CriarTabelaRow(DTORelatorioConsubstanciadoDetalhado item)
        {
            TableRow tableRow = new TableRow();

            TableCell cellDescricaoUnidade = new TableCell();
            cellDescricaoUnidade.HorizontalAlign = HorizontalAlign.Center;
            cellDescricaoUnidade.Text = item.DescricaoUnidade;
            tableRow.Cells.Add(cellDescricaoUnidade);

            TableCell cellTotalMetas = new TableCell();
            cellTotalMetas.HorizontalAlign = HorizontalAlign.Center;
            cellTotalMetas.Text = item.TotalMetas.ToString();
            tableRow.Cells.Add(cellTotalMetas);

            TableCell cell100Metas = new TableCell();
            cell100Metas.HorizontalAlign = HorizontalAlign.Center;
            cell100Metas.Text = item.CemMetas.ToString();
            tableRow.Cells.Add(cell100Metas);

            TableCell cell90Metas = new TableCell();
            cell90Metas.HorizontalAlign = HorizontalAlign.Center;
            cell90Metas.Text = item.NoventaMetas.ToString();
            tableRow.Cells.Add(cell90Metas);

            TableCell cellTotalIndicadores = new TableCell();
            cellTotalIndicadores.HorizontalAlign = HorizontalAlign.Center;
            cellTotalIndicadores.Text = item.TotalIndicadores.ToString();
            tableRow.Cells.Add(cellTotalIndicadores);

            TableCell cell100Indicadores = new TableCell();
            cell100Indicadores.HorizontalAlign = HorizontalAlign.Center;
            cell100Indicadores.Text = item.CemIndicadores.ToString();
            tableRow.Cells.Add(cell100Indicadores);

            TableCell cell90Indicadores = new TableCell();
            cell90Indicadores.HorizontalAlign = HorizontalAlign.Center;
            cell90Indicadores.Text = item.NoventaIndicadores.ToString();
            tableRow.Cells.Add(cell90Indicadores);

            TableCell cellTotalAtividades = new TableCell();
            cellTotalAtividades.HorizontalAlign = HorizontalAlign.Center;
            cellTotalAtividades.Text = item.TotalAtividades.ToString();
            tableRow.Cells.Add(cellTotalAtividades);

            TableCell cell100Atividades = new TableCell();
            cell100Atividades.HorizontalAlign = HorizontalAlign.Center;
            cell100Atividades.Text = item.CemAtividades.ToString();
            tableRow.Cells.Add(cell100Atividades);

            TableCell cell90Atividades = new TableCell();
            cell90Atividades.HorizontalAlign = HorizontalAlign.Center;
            cell90Atividades.Text = item.NoventaAtividades.ToString();
            tableRow.Cells.Add(cell90Atividades);

            return tableRow;
        }

        private TableRow CriarTabelaMetaRow(DTORelatorioConsubstanciadoDetalhado item)
        {
            TableRow tableRow = new TableRow();

            TableCell cellDescricaoUnidade = new TableCell();
            cellDescricaoUnidade.HorizontalAlign = HorizontalAlign.Center;
            cellDescricaoUnidade.Text = item.DescricaoUnidade;
            tableRow.Cells.Add(cellDescricaoUnidade);

            TableCell cellTotalMetas = new TableCell();
            cellTotalMetas.HorizontalAlign = HorizontalAlign.Center;
            cellTotalMetas.Text = item.TotalMetas.ToString();
            tableRow.Cells.Add(cellTotalMetas);

            TableCell cellTotalIndicadores = new TableCell();
            cellTotalIndicadores.HorizontalAlign = HorizontalAlign.Center;
            cellTotalIndicadores.Text = item.TotalIndicadores.ToString();
            tableRow.Cells.Add(cellTotalIndicadores);

            TableCell cellTotalAtividades = new TableCell();
            cellTotalAtividades.HorizontalAlign = HorizontalAlign.Center;
            cellTotalAtividades.Text = item.TotalAtividades.ToString();
            tableRow.Cells.Add(cellTotalAtividades);

            TableCell cell100Metas = new TableCell();
            cell100Metas.HorizontalAlign = HorizontalAlign.Center;
            cell100Metas.Text = item.CemMetas.ToString();
            tableRow.Cells.Add(cell100Metas);

            TableCell cell100Atividades = new TableCell();
            cell100Atividades.HorizontalAlign = HorizontalAlign.Center;
            cell100Atividades.Text = item.CemAtividades.ToString();
            tableRow.Cells.Add(cell100Atividades);

            TableCell cell100Indicadores = new TableCell();
            cell100Indicadores.HorizontalAlign = HorizontalAlign.Center;
            cell100Indicadores.Text = item.CemIndicadores.ToString();
            tableRow.Cells.Add(cell100Indicadores);

            TableCell cell90Metas = new TableCell();
            cell90Metas.HorizontalAlign = HorizontalAlign.Center;
            cell90Metas.Text = item.NoventaMetas.ToString();
            tableRow.Cells.Add(cell90Metas);

            TableCell cell90Indicadores = new TableCell();
            cell90Indicadores.HorizontalAlign = HorizontalAlign.Center;
            cell90Indicadores.Text = item.NoventaIndicadores.ToString();
            tableRow.Cells.Add(cell90Indicadores);

            TableCell cell90Atividades = new TableCell();
            cell90Atividades.HorizontalAlign = HorizontalAlign.Center;
            cell90Atividades.Text = item.NoventaAtividades.ToString();
            tableRow.Cells.Add(cell90Atividades);

            return tableRow;
        }

        private TableRow CriarTabelaRowObjetivoEstrategico(DTOObjetivoEstrategicoDetalhado item)
        {
            TableRow tableRow = new TableRow();

            TableCell cellDescricaoUnidade = new TableCell();
            cellDescricaoUnidade.HorizontalAlign = HorizontalAlign.Center;
            cellDescricaoUnidade.Text = item.DescricaoUnidade;
            tableRow.Cells.Add(cellDescricaoUnidade);

            TableCell cellRecursosPrevistosAssociados = new TableCell();
            cellRecursosPrevistosAssociados.HorizontalAlign = HorizontalAlign.Center;
            cellRecursosPrevistosAssociados.Text = Math.Round(item.RecursosPrevistosAssociados, 0).ToString();
            tableRow.Cells.Add(cellRecursosPrevistosAssociados);

            TableCell cellRecursosPrevistosRoteados = new TableCell();
            cellRecursosPrevistosRoteados.HorizontalAlign = HorizontalAlign.Center;
            cellRecursosPrevistosRoteados.Text = Math.Round(item.RecursosPrevistosRoteados, 0).ToString();
            tableRow.Cells.Add(cellRecursosPrevistosRoteados);

            TableCell cellRecursosUtilizadosAssociados = new TableCell();
            cellRecursosUtilizadosAssociados.HorizontalAlign = HorizontalAlign.Center;
            cellRecursosUtilizadosAssociados.Text = Math.Round(item.RecursosUtilizadosAssociados, 0).ToString();
            tableRow.Cells.Add(cellRecursosUtilizadosAssociados);

            TableCell cellRecursosUtilizadosRoteados = new TableCell();
            cellRecursosUtilizadosRoteados.HorizontalAlign = HorizontalAlign.Center;
            cellRecursosUtilizadosRoteados.Text = Math.Round(item.RecursosUtilizadosRoteados, 0).ToString();
            tableRow.Cells.Add(cellRecursosUtilizadosRoteados);

            return tableRow;
        }

        private void CentralizarCelula(Table table)
        {
            try
            {
                table.Rows[2].Cells[0].Attributes.Add("style", "text-align:center !important;");
                table.Rows[2].Cells[1].Attributes.Add("style", "text-align:center !important;");
                table.Rows[2].Cells[2].Attributes.Add("style", "text-align:center !important;");
                table.Rows[2].Cells[3].Attributes.Add("style", "text-align:center !important;");
                table.Rows[2].Cells[4].Attributes.Add("style", "text-align:center !important;");
                table.Rows[2].Cells[5].Attributes.Add("style", "text-align:center !important;");
                table.Rows[2].Cells[6].Attributes.Add("style", "text-align:center !important;");
                table.Rows[2].Cells[7].Attributes.Add("style", "text-align:center !important;");
                table.Rows[2].Cells[8].Attributes.Add("style", "text-align:center !important;");
                table.Rows[2].Cells[9].Attributes.Add("style", "text-align:center !important;");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CentralizarCelulaTotalizador(Table table)
        {
            try
            {
                table.Rows[1].Cells[0].Attributes.Add("style", "text-align:center !important;");
                table.Rows[2].Cells[0].Attributes.Add("style", "text-align:center !important;");
                table.Rows[2].Cells[1].Attributes.Add("style", "text-align:center !important;");
                table.Rows[2].Cells[2].Attributes.Add("style", "text-align:center !important;");
                table.Rows[2].Cells[3].Attributes.Add("style", "text-align:center !important;");
                table.Rows[2].Cells[4].Attributes.Add("style", "text-align:center !important;");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void PreSalvar(EnumTipoTexto tipoTexto, string descricaoResultadosAlcancados)
        {
            try
            {
                Validate();

                if (IsValid)
                {
                    if (string.IsNullOrEmpty(descricaoResultadosAlcancados))
                    {
                        MessageBox.ShowInformationMessage(string.Format("Favor informar o {0}.", tipoTexto.GetDescription()), "Erro");
                        return;
                    }

                    MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Salvar",
                        "Deseja realmente salvar?", MessageBoxButtons.YesNo, "Salvar" + tipoTexto);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void Salvar(EnumTipoTexto tipoTexto, string codigoSeqRelatorioConsubstanciado, string descricaoResultadosAlcancados)
        {
            try
            {
                DTORelatorioConsubstanciado registro = new DTORelatorioConsubstanciado()
                {
                    Exercicio = new DTOExercicio() { Ano = ddlAnoExercicio.SelectedValue.ToShort() },
                    TipoTexto = new DTOTipoTexto() { CodigoSeqTipoTexto = (int)tipoTexto }
                };

                registro.CodigoSeqRelatorioConsubstanciado = !string.IsNullOrEmpty(codigoSeqRelatorioConsubstanciado) ? int.Parse(codigoSeqRelatorioConsubstanciado) : 0;
                registro.DescricaoRelatorioConsubstanciado = descricaoResultadosAlcancados;

                this.ObterPGAService().SalvarRelatorioConsubstanciado(registro);

                var param = new Dictionary<string, object>();
                param.Add("TipoTexto", tipoTexto);

                MessageBox.ShowInformationMessage("Registro salvo com sucesso.", "FecharModal", param);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void GerarExportacaoExcel()
        {
            try
            {
                var watch = new Stopwatch();
                watch.Start();
                DataSet ds = GetDataSet();

                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(ds);
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Buffer = true;
                    HttpContext.Current.Response.Charset = "";
                    HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + "RelatorioConsubstanciadoAnual" + ".xlsx");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public DataSet GetDataSet()
        {
            DataSet ds = new DataSet();
            DataTable dtTableObjetivoEstrategicoPE = new DataTable();
            dtTableObjetivoEstrategicoPE = GetDataTable(ContabilizarTotalDetalhado(this.ObterPGAService().ListarObjetivoEstrategicoPE(ddlAnoExercicio.SelectedValue.ToInt32(), 0)), null);
            dtTableObjetivoEstrategicoPE.TableName = "Por Objetivos Estratégicos PE";
            ds.Tables.Add(dtTableObjetivoEstrategicoPE);

            DataTable dtTableObjetivoEstrategicoPPA = new DataTable();
            dtTableObjetivoEstrategicoPPA = GetDataTable(ContabilizarTotalDetalhado(this.ObterPGAService().ListarObjetivoEstrategicoPPA(ddlAnoExercicio.SelectedValue.ToInt32(), 0)), null);
            dtTableObjetivoEstrategicoPPA.TableName = "Por Objetivos Estratégicos PPA";
            ds.Tables.Add(dtTableObjetivoEstrategicoPPA);

            DataTable dtTableMetas = new DataTable();
            dtTableMetas = GetDataTable(ContabilizarTotalDetalhado(this.ObterPGAService().ListarMeta(ddlAnoExercicio.SelectedValue.ToInt32(), 0)), null);
            dtTableMetas.TableName = "Por Metas";
            ds.Tables.Add(dtTableMetas);

            DataTable dtTableInstrumentos = new DataTable();
            dtTableInstrumentos = GetDataTable(ContabilizarTotalDetalhado(this.ObterPGAService().ListarInstrumentos(ddlAnoExercicio.SelectedValue.ToInt32(), 0)), "Por Instrumentos");
            dtTableInstrumentos.TableName = "Por Instrumentos";
            ds.Tables.Add(dtTableInstrumentos);

            DataTable dtTableAlinhamento = new DataTable();
            dtTableAlinhamento = GetDataTable(ContabilizarTotalDetalhado(this.ObterPGAService().ListarIniciativa(ddlAnoExercicio.SelectedValue.ToInt32(), 0)), "Por Alinhamento");
            dtTableAlinhamento.TableName = "Por Alinhamento";
            ds.Tables.Add(dtTableAlinhamento);

            DataTable dtTablePorTipo = new DataTable();
            dtTablePorTipo = GetDataTable(ContabilizarTotalDetalhado(this.ObterPGAService().ListarMeta(ddlAnoExercicio.SelectedValue.ToInt32(), 0)), "Por Tipo");
            dtTablePorTipo.TableName = "Por Tipo";
            ds.Tables.Add(dtTablePorTipo);

            DataTable dtTablePorRecurso = new DataTable();
            dtTablePorRecurso = GetDataTableRecurso(ContabilizarObjetivoEstrategicoDetalhado(this.ObterPGAService().ListarObjetivoEstrategicoTotal(ddlAnoExercicio.SelectedValue.ToInt32(), 0, 0)));
            dtTablePorRecurso.TableName = "Por Recursos";
            ds.Tables.Add(dtTablePorRecurso);

            return ds;
        }

        private DataTable GetDataTable(IList<DTORelatorioConsubstanciadoDetalhado> list, string descricaoTipo)
        {
            DataTable dt = new DataTable();
            DataColumn column;
            DataRow row;

            if (!string.IsNullOrEmpty(descricaoTipo))
            {
                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = descricaoTipo;
                dt.Columns.Add(column);
            }

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "Áreas";
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Metas TOTAL";
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Metas 100%";
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Metas +90%";
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Indicadores TOTAL";
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Indicadores 100%";
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Indicadores +90%";
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Atividades TOTAL";
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Atividades 100%";
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Atividades +90%";
            dt.Columns.Add(column);

            foreach (var item in list)
            {
                row = dt.NewRow();

                if (!string.IsNullOrEmpty(descricaoTipo))
                    row[descricaoTipo] = item.DescricaoTipo;

                row["Áreas"] = item.DescricaoUnidade;
                row["Metas TOTAL"] = item.TotalMetas;
                row["Metas 100%"] = item.CemMetas;
                row["Metas +90%"] = item.NoventaMetas;
                row["Indicadores TOTAL"] = item.TotalIndicadores;
                row["Indicadores 100%"] = item.CemIndicadores;
                row["Indicadores +90%"] = item.NoventaIndicadores;
                row["Atividades TOTAL"] = item.TotalAtividades;
                row["Atividades 100%"] = item.CemAtividades;
                row["Atividades +90%"] = item.NoventaAtividades;

                dt.Rows.Add(row);
            }

            return dt;
        }

        private DataTable GetDataTableRecurso(IList<DTOObjetivoEstrategicoDetalhado> list)
        {
            DataTable dt = new DataTable();
            DataColumn column;
            DataRow row;

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "Objetivo Estratégico";
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "Áreas";
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Recursos Previstos associados às Metas que contribuem para o Objetivo";
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Recursos Previstos rateados para alocação ao Objetivo";
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Recursos Utilizados associados às Metas que contribuem para o Objetivo";
            dt.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Recursos Utilizados rateados para alocação ao Objetivo";
            dt.Columns.Add(column);

            foreach (var item in list.OrderBy(x => x.DescricaoUnidade))
            {
                row = dt.NewRow();

                row["Objetivo Estratégico"] = item.DescricaoObjetivo;
                row["Áreas"] = item.DescricaoUnidade;
                row["Recursos Previstos associados às Metas que contribuem para o Objetivo"] = item.RecursosPrevistosAssociados;
                row["Recursos Previstos rateados para alocação ao Objetivo"] = item.RecursosPrevistosRoteados;
                row["Recursos Utilizados associados às Metas que contribuem para o Objetivo"] = item.RecursosUtilizadosAssociados;
                row["Recursos Utilizados rateados para alocação ao Objetivo"] = item.RecursosUtilizadosRoteados;

                dt.Rows.Add(row);
            }

            return dt;
        }

        public void ExportReport(ReportViewerHelper.ReportType reportType)
        {
            try
            {
                byte[] bytes = this.ObterPGAService().ExportReportRelatorioConsubstanciadoAnual(ddlAnoExercicio.SelectedValue.ToInt32(), reportType);

                if (bytes != null)
                {
                    string filename = "RelatorioConsubstanciadoAnual";

                    switch (reportType)
                    {
                        case ReportViewerHelper.ReportType.PDF:
                            filename += ".pdf";
                            break;
                        case ReportViewerHelper.ReportType.Word:
                        default:
                            filename += ".doc";
                            break;
                    }

                    WebHelper.DownloadFile(bytes, filename);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}