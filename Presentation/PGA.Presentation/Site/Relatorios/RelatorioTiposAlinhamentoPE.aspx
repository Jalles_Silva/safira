﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="RelatorioTiposAlinhamentoPE.aspx.cs" Inherits="PGA.Presentation.Site.Relatorios.RelatorioTiposAlinhamentoPE" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnPesquisar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Emitir Relatório de Projetos / Iniciativas</h3>
                    </div>
                   <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblAnoExercicio" AssociatedControlID="lblAnoExercicio" Text="Exercício" runat="server" />
                                    <asp:DropDownList ID="ddlAnoExercicio" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="drpArea" Text="Área" runat="server" />
                                    <asp:DropDownList ID="drpArea" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:HiddenField ID="hdfAreaUsuarioLogado" runat="server" />
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoInstrumento" AssociatedControlID="ddlTipoInstrumento" Text="Tipo de Instrumento" runat="server" />
                                    <asp:DropDownList ID="ddlTipoInstrumento" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblMetaInterna" AssociatedControlID="ddlMetaInterna" Text="Constante no PGA" runat="server" />
                                    <asp:DropDownList ID="ddlMetaInterna" runat="server" CssClass="form-control">
                                        <asp:ListItem Selected="True" Value="0">Todos</asp:ListItem>
                                        <asp:ListItem Value="2">Sim</asp:ListItem>
                                        <asp:ListItem Value="1">Não</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdfMetaInterna" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoIniciativa" AssociatedControlID="drpTipoIniciativa" Text="Projeto / Iniciativa" runat="server" />
                                    <asp:DropDownList ID="drpTipoIniciativa" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="" Text="Todos" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="P" Text="Projeto"></asp:ListItem>
                                        <asp:ListItem Value="I" Text="Iniciativa"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblObjetivo" AssociatedControlID="drpObjetivo" Text="Objetivo Estratégico" runat="server" />
                                    <asp:DropDownList ID="drpObjetivo" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoMeta" AssociatedControlID="ddlDescricaoMeta" Text="Tipo de Meta" runat="server" />
                                    <asp:DropDownList ID="ddlDescricaoMeta" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblSituacao" AssociatedControlID="rblSituacao" Text="Situação" runat="server" />
                                    <asp:RadioButtonList ID="rblSituacao" runat="server" RepeatDirection="Horizontal" CssClass="radioListHoriz">
                                        <asp:ListItem Value="" Text="Todos" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="True" Text="Ativo"></asp:ListItem>
                                        <asp:ListItem Value="False" Text="Inativo"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:LinkButton ID="btnPesquisar" runat="server" SkinID="btnPesquisar" CausesValidation="false"
                                        ValidationGroup="Pesquisar" />
                                </div>
                            </div>
                        </div>
                         <div class="area-table">
                            <asp:Repeater ID="rptAlinhamentosPE" runat="server">
                                <HeaderTemplate>
                                    <table>
                                        <tr runat="server">
                                            <th>Projeto / Iniciativa</th>
                                            <th>Identificação</th>
                                            <th>Situação</th>
                                            <th>Documentação</th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td colspan="4" style="background-color: #C5D8CC !important;">
                                            <asp:Label ID="lblNomeUnidade" runat="server" Text='<%#Eval("Key") %>' />
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="rptAtividadePorMetaEUnidade" runat="server" DataSource='<%#Eval("Value") %>'>
                                        <ItemTemplate>
                                            <tr class="<%# (Container.ItemIndex % 2 == 0 ? "" : "alternate") %>">
                                                <td style="width: 10%" align="left">
                                                    <asp:Label ID="lblIniciativaProjeto" runat="server" Text='<%# Eval("TpIniciativaProjeto") == null ? "Indefinido" : ((string)Eval("TpIniciativaProjeto") == "I" ? "Iniciativa" : "Projeto") %>' />
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblAlinhamento" runat="server" Text='<%#Eval("DescricaoIniciativa") %>' />
                                                </td>
                                                <td style="width: 60px; text-align:center;">
                                                    <asp:Label ID="lblSituacao" runat="server" Text='<%# (bool)Eval("Ativo") == true ? "Ativo" : "Inativo" %>' />
                                                </td>
                                                <td style="width: 25%">
                                                    <asp:LinkButton ID="lnkBaixarAnexo" Text='<%# Eval("Anexo") != null ? Eval("Anexo.DescricaoAnexo") : "Sem Anexo" %>' ToolTip="Realizar o download do arquivo" runat="server" OnClick="lnkBaixarAnexo_Click" CaminhoAnexo='<%# Eval("Anexo") != null ? Eval("Anexo.CaminhoAnexo") : "" %>' Enabled='<%# Eval("Anexo") != null %>' />
                                                </td>
                                            </tr>
                                            <tr class="<%# (Container.ItemIndex % 2 == 0 ? "normal" : "alternate") %>">
                                                <td colspan="4">
                                                    <div onclick='toggleDiv(this, iDivObjetivos<%#Eval("CodigoSeqIniciativa") %>, divObjetivos<%#Eval("CodigoSeqIniciativa") %>);' class='<%# ((IList<PGA.Services.Spec.DataTransferObjects.DTOIniciativaObjetivo>)Eval("Objetivos")).Count == 0 ? "divZerada" : "divFechada" %>'> <i id='iDivObjetivos<%#Eval("CodigoSeqIniciativa") %>' aria-hidden="true" class='<%# ((IList<PGA.Services.Spec.DataTransferObjects.DTOIniciativaObjetivo>)Eval("Objetivos")).Count == 0 ? "glyphicon glyphicon-ban-circle" : "glyphicon glyphicon-plus" %>'></i> Objetivos Estratégicos</div>
                                                    <div id='divObjetivos<%#Eval("CodigoSeqIniciativa") %>' style="display:none">
                                                        <asp:GridView ID="grdObjetivos" runat="server" AutoGenerateColumns="False" DataSource='<%#Eval("Objetivos") %>'
                                                            EmptyDataText="Nenhum registro encontrado." Style="margin-top:10px">
                                                            <EmptyDataTemplate>Nenhum objetivo encontrado.</EmptyDataTemplate>
                                                            <AlternatingRowStyle CssClass="alternate" />
                                                            <RowStyle CssClass="normal" />
                                                            <Columns>
                                                                <asp:BoundField HeaderText="Objetivo Estratégico" DataField="Objetivo.NomeObjetivo" />
                                                                <asp:TemplateField HeaderText="Porcentagem" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="8%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPorcentagem" runat="server" Text='<%# string.Format("{0:N2}", Convert.ToDecimal(Eval("PercentualParticipacao"))) %>'></asp:Label>%
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="<%# (Container.ItemIndex % 2 == 0 ? "normal" : "alternate") %>">
                                                <td colspan="4">
                                                    <div onclick='toggleDiv(this, iDivMetas<%#Eval("CodigoSeqIniciativa") %>, divMetas<%#Eval("CodigoSeqIniciativa") %>);' class='<%# ((IList<PGA.Services.Spec.DataTransferObjects.DTOMeta>)Eval("Metas")).Count == 0 ? "divZerada" : "divFechada" %>'> <i id='iDivMetas<%#Eval("CodigoSeqIniciativa") %>' aria-hidden="true" class='<%# ((IList<PGA.Services.Spec.DataTransferObjects.DTOMeta>)Eval("Metas")).Count == 0 ? "glyphicon glyphicon-ban-circle" : "glyphicon glyphicon-plus" %>'></i> Metas</div>
                                                    <div id='divMetas<%#Eval("CodigoSeqIniciativa") %>' style="display:none">
                                                        <asp:Repeater ID="rptMetas" runat="server" DataSource='<%#Eval("Metas") %>'>
                                                            <HeaderTemplate>
                                                                <table Style="margin-top:10px">
                                                                    <tr runat="server">
                                                                        <th>Meta</th>
                                                                        <th>Exercício</th>
                                                                        <th>Tipo Meta</th>
                                                                        <th>Resultados Esperados</th>
                                                                        <th>Status</th>
                                                                    </tr>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                    <tr class="<%# (Container.ItemIndex % 2 == 0 ? "normal" : "alternate") %>">
                                                                        <td>
                                                                            <asp:Label ID="lblNomeMeta" runat="server" Text='<%#Eval("DescricaoMeta") %>' />
                                                                        </td>
                                                                        <td style="width: 60px">
                                                                            <asp:Label ID="lblAnoExercicio" runat="server" Text='<%#Eval("AnoExercicio") %>' />
                                                                        </td>
                                                                        <td style="width: 100px">
                                                                            <asp:Label ID="lblTipoMeta" runat="server" Text='<%#Eval("DescricaoTipoMeta") %>' />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblResultadosEsperados" runat="server" Text='<%#Eval("DescricaoResultadoEsperado") %>' />
                                                                        </td>
                                                                        <td style="width: 80px">
                                                                            <asp:Label ID="lblStatusMeta" runat="server" Text='<%#Eval("DescricaoStatusMeta") %>' />
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="<%# (Container.ItemIndex % 2 == 0 ? "normal" : "alternate") %>">
                                                                        <td colspan="5">
                                                                            <div onclick='toggleDiv(this, iDivAcoes<%#Eval("CodSeqMeta") %>, divAcoes<%#Eval("CodSeqMeta") %>);' class='<%# ((IList<PGA.Services.Spec.DataTransferObjects.DTOAcaoRelatorio>)Eval("Acoes")).Count == 0 ? "divZerada" : "divFechada" %>'> <i id='iDivAcoes<%#Eval("CodSeqMeta") %>' aria-hidden="true" class='<%# ((IList<PGA.Services.Spec.DataTransferObjects.DTOAcaoRelatorio>)Eval("Acoes")).Count == 0 ? "glyphicon glyphicon-ban-circle" : "glyphicon glyphicon-plus" %>'></i> Atividades</div>
                                                                            <div id='divAcoes<%#Eval("CodSeqMeta") %>' style="display:none">
                                                                                <asp:GridView ID="gdvAcoes" runat="server" Style="margin-top:10px"
                                                                                    AutoGenerateColumns="False" DataSource='<%#Eval("Acoes") %>'
                                                                                    EmptyDataText="Nenhum registro encontrado.">
                                                                                    <EmptyDataTemplate>Nenhuma atividade encontrada.</EmptyDataTemplate>
                                                                                    <AlternatingRowStyle CssClass="alternate" />
                                                                                    <RowStyle CssClass="normal" />
                                                                                    <Columns>
                                                                                        <asp:BoundField HeaderText="Atividade" DataField="DescricaoAtividade" />
                                                                                        <asp:BoundField HeaderText="Recurso Estimado" DataField="RecursoEstimadoMascara" ItemStyle-Width="80px" />
                                                                                        <asp:BoundField HeaderText="Recurso Utilizado" DataField="RecursoOrcamentarioUtilizadoMascara" ItemStyle-Width="80px" />
                                                                                        <asp:BoundField HeaderText="Data Início" DataField="DataInicioAtividade" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="60px" />
                                                                                        <asp:BoundField HeaderText="Data Fim" DataField="DataFimAtividade" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="60px" />
                                                                                        <asp:BoundField HeaderText="Percentual Executado" DataField="PercentualExecucaoMascara" ItemStyle-Width="60px" />
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="<%# (Container.ItemIndex % 2 == 0 ? "normal" : "alternate") %>">
                                                                        <td colspan="5">
                                                                            <div onclick='toggleDiv(this, iDivIndicadores<%#Eval("CodSeqMeta") %>, divIndicadores<%#Eval("CodSeqMeta") %>);' class='<%# ((IList<PGA.Services.Spec.DataTransferObjects.DTOIndicadorRelatorio>)Eval("Indicadores")).Count == 0 ? "divZerada" : "divFechada" %>'> <i id='iDivIndicadores<%#Eval("CodSeqMeta") %>' aria-hidden="true" class='<%# ((IList<PGA.Services.Spec.DataTransferObjects.DTOIndicadorRelatorio>)Eval("Indicadores")).Count == 0 ? "glyphicon glyphicon-ban-circle" : "glyphicon glyphicon-plus" %>'></i> Indicadores</div>
                                                                            <div id='divIndicadores<%#Eval("CodSeqMeta") %>' style="display:none">
                                                                                <asp:GridView ID="gdvIndicadores" runat="server" Style="margin-top:10px"
                                                                                    Width="100%" AutoGenerateColumns="False" DataSource='<%#Eval("Indicadores") %>'
                                                                                    EmptyDataText="Nenhum registro encontrado.">
                                                                                    <EmptyDataTemplate>Nenhum indicador encontrado.</EmptyDataTemplate>
                                                                                    <AlternatingRowStyle CssClass="alternate" />
                                                                                    <RowStyle CssClass="normal" />
                                                                                    <Columns>
                                                                                        <asp:BoundField HeaderText="Indicador" DataField="DescricaoIndicador" />
                                                                                        <asp:BoundField HeaderText="Valor Alvo" DataField="ValorAlvoMascara" ItemStyle-Width="80px" />
                                                                                        <asp:BoundField HeaderText="Data Alvo" DataField="DataAlvo" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="60px" />
                                                                                        <asp:TemplateField HeaderText="Objetivo Estratégico" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                   <%# (bool)Eval("ObjetivoEstrategico") == true ? "Sim" : "Não" %>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField HeaderText="Resultado em Relação ao Alvo" ItemStyle-Width="100px" DataField="ResultadoIndicadorValorAlvo" />
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                    <tr>
                                                                        <td colspan="5">
                                                                            <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhuma Meta encontrada." /></td>
                                                                    </tr>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td colspan="9">
                                            <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." /></td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="paginator">
                            <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="row pull-right" id="btnExportar" runat="server" visible="false">
                                    <div class="btn-group">
                                        <div class="col-xs-6 col-sm-6">
                                            <div class="form-group">
                                                <asp:LinkButton type="button" ID="btnExportarExcel" runat="server" style="width: auto" class="btn btn-antt" aria-haspopup="true">
                                                    <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>Extração Excel
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">
        function toggleDiv(Div, iDiv, divFilha) {
            if (Div.getAttribute("class") != "divZerada") {
                if (divFilha.style.display == "none") {
                    divFilha.style = "display:block";
                    Div.setAttribute("class", "divAberta");
                    iDiv.setAttribute("class", "glyphicon glyphicon-minus");
                }
                else {
                    divFilha.style = "display:none";
                    Div.setAttribute("class", "divFechada");
                    iDiv.setAttribute("class", "glyphicon glyphicon-plus");
                }
            }
        }
    </script>
</asp:Content>
