﻿<%@ Page Title="SAFIRA - Consultar RelatorioGlobalMeta" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false"
    CodeBehind="RelatorioGlobalMeta.aspx.cs" Inherits="PGA.Presentation.Site.Relatorios.RelatorioGlobalMeta"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnPesquisar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Emitir Relatório Global de Metas</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblAnoExercicio" AssociatedControlID="lblAnoExercicio" Text="Exercício" runat="server" />
                                    <asp:DropDownList ID="ddlAnoExercicio" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="ddlArea" Text="Área" runat="server" />
                                    <asp:DropDownList ID="ddlArea" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:HiddenField ID="hdfAreaUsuarioLogado" runat="server" />
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoInstrumento" AssociatedControlID="ddlTipoInstrumento" Text="Tipo de Instrumento" runat="server" />
                                    <asp:DropDownList ID="ddlTipoInstrumento" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblMetaInterna" AssociatedControlID="ddlMetaInterna" Text="Constante no PGA" runat="server" />
                                    <asp:DropDownList ID="ddlMetaInterna" runat="server" CssClass="form-control">
                                        <asp:ListItem Selected="True" Value="0">Todos</asp:ListItem>
                                        <asp:ListItem Value="2">Sim</asp:ListItem>
                                        <asp:ListItem Value="1">Não</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdfMetaInterna" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblPeriodo" AssociatedControlID="ddlPeriodo" Text="Período" runat="server" />
                                    <asp:DropDownList ID="ddlPeriodo" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoMeta" AssociatedControlID="ddlDescricaoMeta" Text="Tipo de Meta" runat="server" />
                                    <asp:DropDownList ID="ddlDescricaoMeta" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoAlinhamento" AssociatedControlID="rbtTipoAlinhamento" Text="Tipo de Alinhamento" runat="server" />
                                    <asp:CheckBoxList runat="server" ID="rbtTipoAlinhamento" RepeatDirection="Horizontal" CssClass="radioListHoriz">
                                        <asp:ListItem Text="&nbsp;Alinhamento PE&nbsp;&nbsp;&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="&nbsp;Alinhamento PPA&nbsp;&nbsp;&nbsp;" Value="2" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="&nbsp;Alinhamento Missão Institucional&nbsp;&nbsp;&nbsp;" Value="3" Selected="True"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-xs-5 col-sm-5">
                                <fieldset>
                                    <legend>Detalhar</legend>
                                    <asp:RadioButtonList ID="rblDetalhar" runat="server" OnSelectedIndexChanged="rblDetalhar_SelectedIndexChanged" AutoPostBack="true" CssClass="radioListHoriz">
                                        <asp:ListItem Text="&nbsp;Por tipo de Alinhamento e tipo de Meta" Value="1" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="&nbsp;Por tipo de Meta" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="&nbsp;Por tipo de Alinhamento" Value="2"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </fieldset>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:LinkButton ID="btnPesquisar" runat="server" SkinID="btnPesquisar" CausesValidation="false"
                                        ValidationGroup="Pesquisar" />
                                </div>
                            </div>
                        </div>
                        <div class="area-table">
                            <asp:GridView ID="gdvResultadoConsulta" runat="server" AllowSorting="True"
                                Width="100%" AutoGenerateColumns="False" OnRowDataBound="gdvResultadoConsulta_OnRowDataBound"
                                EmptyDataText="Nenhum registro encontrado.">
                                <EmptyDataTemplate>Nenhum registro encontrado.</EmptyDataTemplate>
                                <AlternatingRowStyle CssClass="alternate" />
                                <Columns>
                                    <asp:BoundField HeaderText="Área" DataField="DescricaoArea" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="12%" />
                                    <asp:BoundField HeaderText="Metas Cadastradas" DataField="QdtMetasCadastradas" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Metas Alinhadas ao PE" DataField="QtdMetasAlinhadasPE" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Metas Alinhadas ao PPA" DataField="QtdMetasAlinhadasPPA" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Metas Alinhadas à Missão Institucional" DataField="QtdMetasAlinhadasMI" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Metas Administrativa" DataField="QtdMetasAdministrativas" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Metas de Fiscalização" DataField="QtdMetasFiscalizacoes" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Metas de Regulação" DataField="QtdMetasRegulacoes" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField HeaderText="Total de Metas Constante no PGA" DataField="QtdMetasInternas" ItemStyle-HorizontalAlign="Center" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="paginator" runat="server" visible="false">
                            <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="100" />
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="row pull-right" id="btnExportar" runat="server" visible="false">
                                    <div class="btn-group">
                                        <div class="col-xs-6 col-sm-6">
                                            <div class="form-group">
                                                <asp:LinkButton type="button" ID="btnExportarExcel" runat="server" style="width: auto" class="btn btn-antt" aria-haspopup="true">
                                                    <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>Extração Excel
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6">
                                            <button type="button" class="btn btn-antt dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>Exportar <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <asp:LinkButton ID="lnkReportPDF" runat="server" CausesValidation="false" Text="PDF" /></li>
                                                <li>
                                                    <asp:LinkButton ID="lnkReportExcel" runat="server" CausesValidation="false" Text="Excel" /></li>
                                                <li>
                                                    <asp:LinkButton ID="lnkReportWord" runat="server" CausesValidation="false" Text="Word" /></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
