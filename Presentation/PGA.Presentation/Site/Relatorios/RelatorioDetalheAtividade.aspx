﻿<%@ Page Title="SAFIRA - Consultar RelatorioDetalheAtividade" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false"
    CodeBehind="RelatorioDetalheAtividade.aspx.cs" Inherits="PGA.Presentation.Site.Relatorios.RelatorioDetalheAtividade" 
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnPesquisar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Emitir Relatório Detalhado de Atividades</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblAnoExercicio" AssociatedControlID="lblAnoExercicio" Text="Exercício" runat="server" />
                                    <asp:DropDownList ID="ddlAnoExercicio" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="ddlArea" Text="Área" runat="server" />
                                    <asp:DropDownList ID="ddlArea" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:HiddenField ID="hdfAreaUsuarioLogado" runat="server" />
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoInstrumento" AssociatedControlID="ddlTipoInstrumento" Text="Tipo de Instrumento" runat="server" />
                                    <asp:DropDownList ID="ddlTipoInstrumento" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblMetaInterna" AssociatedControlID="ddlMetaInterna" Text="Constante no PGA" runat="server" />
                                    <asp:DropDownList ID="ddlMetaInterna" runat="server" CssClass="form-control">
                                        <asp:ListItem Selected="True" Value="0">Todos</asp:ListItem>
                                        <asp:ListItem Value="2">Sim</asp:ListItem>
                                        <asp:ListItem Value="1">Não</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdfMetaInterna" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblPeriodo" AssociatedControlID="ddlPeriodo" Text="Período" runat="server" />
                                    <asp:DropDownList ID="ddlPeriodo" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoMeta" AssociatedControlID="ddlDescricaoMeta" Text="Tipo de Meta" runat="server" />
                                    <asp:DropDownList ID="ddlDescricaoMeta" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoAlinhamento" AssociatedControlID="rbtTipoAlinhamento" Text="Tipo de Alinhamento" runat="server" />
                                    <asp:CheckBoxList runat="server" ID="rbtTipoAlinhamento" RepeatDirection="Horizontal" CssClass="radioListHoriz">
                                        <asp:ListItem Text="&nbsp;Alinhamento PE&nbsp;&nbsp;&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="&nbsp;Alinhamento PPA&nbsp;&nbsp;&nbsp;" Value="2" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="&nbsp;Alinhamento Missão Institucional&nbsp;&nbsp;&nbsp;" Value="3" Selected="True"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:LinkButton ID="btnPesquisar" runat="server" SkinID="btnPesquisar" CausesValidation="false"
                                        ValidationGroup="Pesquisar" />
                                </div>
                            </div>
                        </div>
                        <div class="area-table">
                            <asp:Repeater ID="rptAcao" runat="server">
                                <HeaderTemplate>
                                    <table>
                                        <tr runat="server">
                                            <th colspan="2">Atividade</th>
                                            <th>Estratégia</th>
                                            <th>Percentual de Execução</th>
                                            <th>Recurso Orçamentário Estimado</th>
                                            <th>Recurso Utilizado Estimado</th>
                                            <th>Início da Atividade</th>
                                            <th>Última Atualização</th>
                                            <th>Fim da Atividade</th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td colspan="9" style="background-color: #C5D8CC !important; width: 50px;">
                                            <asp:Label ID="lblNomeUnidade" runat="server" Text='<%#Eval("Key") %>' />
                                        </td>
                                    </tr>
                                    <asp:Repeater ID="rptAtividadePorMetaEUnidade" runat="server" DataSource='<%#Eval("Value") %>'>
                                        <ItemTemplate>
                                            <tr class="<%# (Container.ItemIndex % 2 == 0 ? "" : "alternate") %>">
                                                <td style="width: 5px">
                                                    <asp:HiddenField ID="hdfNomeUnidade" runat="server" Value='<%#Eval("DescricaoArea") %>' />
                                                </td>
                                                <td style="width: 30%" align="left">
                                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("DescricaoAtividade") %>' />
                                                </td>
                                                <td style="width: 50%" align="left">
                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("DescricaoEstrategia") %>' />
                                                </td>
                                                <td style="width: 5%" align="center">
                                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("PercentualExecucaoMascara") %>' />
                                                </td>
                                                <td style="width: 5%" align="center">
                                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("RecursoEstimadoMascara") %>' />
                                                </td>
                                                <td style="width: 5%" align="center">
                                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("RecursoOrcamentarioUtilizadoMascara") %>' />
                                                </td>
                                                <td style="width: 5%" align="center">
                                                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("DataInicioAtividade", "{0:dd/MM/yyyy}") %>'/>
                                                </td>
                                                <td style="width: 5%" align="center">
                                                    <asp:Label ID="Label8" runat="server" Text='<%#Eval("DataUltimaAtualizacao", "{0:dd/MM/yyyy}") %>' />
                                                </td>
                                                <td style="width: 5%" align="center">
                                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("DataFimAtividade", "{0:dd/MM/yyyy}") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr>
                                        <td colspan="9">
                                            <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." /></td>
                                    </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="paginator">
                            <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="row pull-right" id="btnExportar" runat="server" visible="false">
                                    <div class="btn-group">
                                        <div class="col-xs-6 col-sm-6">
                                            <div class="form-group">
                                                <asp:LinkButton type="button" ID="btnExportarExcel" runat="server" style="width: auto" class="btn btn-antt" aria-haspopup="true">
                                                    <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>Extração Excel
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-6">
                                            <button type="button" class="btn btn-antt dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>Exportar <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <asp:LinkButton ID="lnkReportPDF" runat="server" CausesValidation="false" Text="PDF" /></li>
                                                <li>
                                                    <asp:LinkButton ID="lnkReportExcel" runat="server" CausesValidation="false" Text="Excel" /></li>
                                                <li>
                                                    <asp:LinkButton ID="lnkReportWord" runat="server" CausesValidation="false" Text="Word" /></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>