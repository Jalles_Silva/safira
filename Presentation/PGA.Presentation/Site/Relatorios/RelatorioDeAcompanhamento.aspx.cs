﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PGA.Presentation.Util;
using PGA.Common;
using SQFramework.Web;
using SQFramework.Core;
using Utilitarios.Enums;
using PGA.Services.Spec.DataTransferObjects;

namespace PGA.Presentation.Site.Relatorios
{
    public partial class RelatorioDeAcompanhamento : CustomPageBase
    {
        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnGerarRelatorio.Click += new EventHandler(btnGerarRelatorio_Click);

            ddlAnoExercicio.SelectedIndexChanged += ddlAnoExercicio_SelectedIndexChanged;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (UsuarioConsultor || UsuarioCadastrador || UsuarioCadastrador || UsuarioAdministrador)
                {
                    if (!IsPostBack)
                        CarregarControles();
                }
                else
                    WebHelper.LogoffWithNewRedirection("~/Default.aspx");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        void ddlAnoExercicio_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarArea();
        }


        protected void btnGerarRelatorio_Click(object sender, EventArgs e)
        {
            try
            {
                bool periodoAcumularo = false;

                if (ddlPeriodo.SelectedItem != null)
                {
                    periodoAcumularo = ddlPeriodo.SelectedItem.Text.Contains("Acumulado");
                }

                byte[] relatorio = this.ObterPGAService().GerarRelatorioAcompanhamento(
                    ddlAnoExercicio.SelectedValue.ToInt32(),
                    ddlArea.SelectedItem.Text,
                    Math.Abs(ddlPeriodo.SelectedValue.ToInt32()),
                    periodoAcumularo,
                    ddlDescricaoMeta.SelectedValue.ToInt32(), 
                    alinhamnetoPE, 
                    alinhamnetoPPA, 
                    alinhamnetoMI,
                    ddlTipoInstrumento.SelectedValue.ToInt32(),
                    ddlMetaInterna.SelectedValue.ToInt32()
                );

                if (relatorio != null)
                {
                    WebHelper.DownloadFile(relatorio, "RelatorioDeAcompanhamento.pdf");
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion

        #region [Methods]


        private bool alinhamnetoPE = false;
        private bool alinhamnetoPPA = false;
        private bool alinhamnetoMI = false;
        private bool metaAdministrativa = false;
        private bool metaFiscalizacao = false;
        private bool metaRegulacao = false;
        private void CarregarControles()
        {
            CarregarExercicio();
            CarregarArea();
            CarregarTipoMetas();
            CarregarPeriodo();
            CarregarInstrumentos();
        }
        private void BuscarParamentros()
        {
            foreach (ListItem item in rbtTipoAlinhamento.Items)
            {
                if (item.Selected)
                {
                    if (item.Value.ToInt32() == (int)TiposAlinhamento.Alinhamento_PE) alinhamnetoPE = true;
                    if (item.Value.ToInt32() == (int)TiposAlinhamento.Alinhamento_PPA) alinhamnetoPPA = true;
                    if (item.Value.ToInt32() == (int)TiposAlinhamento.Alinhamento_MI) alinhamnetoMI = true;
                }
            }

            metaAdministrativa = (ddlDescricaoMeta.SelectedItem.Text == TiposDeMeta.Administrativa.GetDescription()) || (ddlDescricaoMeta.SelectedItem.Text == TiposDeMeta.Todos.GetDescription());
            metaFiscalizacao = (ddlDescricaoMeta.SelectedItem.Text == TiposDeMeta.De_Fiscalizacao.GetDescription()) || (ddlDescricaoMeta.SelectedItem.Text == TiposDeMeta.Todos.GetDescription());
            metaRegulacao = (ddlDescricaoMeta.SelectedItem.Text == TiposDeMeta.De_Regulacao.GetDescription()) || (ddlDescricaoMeta.SelectedItem.Text == TiposDeMeta.Todos.GetDescription());
        }
        private void CarregarExercicio()
        {
            ddlAnoExercicio.DataSource = this.ObterPGAService().ListarExercicios();
            ddlAnoExercicio.DataTextField = "Ano";
            ddlAnoExercicio.DataValueField = "Ano";
            ddlAnoExercicio.DataBind();

            var ExercicioAtual = this.ObterPGAService().ObterExercicioAtual();
            ddlAnoExercicio.SelectedValue = this.ObterPGAService().ObterExercicioAtual() != null ? ExercicioAtual.Ano.ToString() : DateTime.Now.Year.ToString();
        }

        private void CarregarArea()
        {
            ddlArea.DataSource = this.ObterPGAService().ListarCorporativoUnidadePorExercicio(ddlAnoExercicio.SelectedValue.ToInt32());
            ddlArea.DataTextField = "DescricaoUnidade";
            ddlArea.DataValueField = "CodigoUnidade";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("Todas", ""));
        }

        private void CarregarTipoMetas()
        {
            ddlDescricaoMeta.DataSource = this.ObterPGAService().ListarTipoMetas();
            ddlDescricaoMeta.DataTextField = "DescricaoTipoMeta";
            ddlDescricaoMeta.DataValueField = "CodigoSeqTipoMeta";
            ddlDescricaoMeta.DataBind();
            ddlDescricaoMeta.Items.Insert(0, new ListItem("Todos", ""));
        }

        private void CarregarPeriodo()
        {
            var ds = new List<DTOFase>()
            {
                new DTOFase { CodigoSeqFase = 1, DescricaoFase = "1º Trimestre" },
                new DTOFase { CodigoSeqFase = 2, DescricaoFase = "2º Trimestre " },
                new DTOFase { CodigoSeqFase = -2, DescricaoFase = "2º Trimestre (Acumulado)" },
                new DTOFase { CodigoSeqFase = 3, DescricaoFase = "3º Trimestre" },
                new DTOFase { CodigoSeqFase = -3, DescricaoFase = "3º Trimestre (Acumulado)" },
                new DTOFase { CodigoSeqFase = 4, DescricaoFase = "4º Trimestre" },

                new DTOFase { CodigoSeqFase = 0, DescricaoFase = "Acumulado no Exercício" },
            };

            ddlPeriodo.DataSource = ds.OrderBy(o => o.DescricaoFase).ToList();
            ddlPeriodo.DataTextField = "DescricaoFaseAlterada";
            ddlPeriodo.DataValueField = "CodigoSeqFase";
            ddlPeriodo.SelectedValue = "0"; // Acumulado no Exercício
            ddlPeriodo.DataBind();
        }

        private void CarregarInstrumentos()
        {
            ddlTipoInstrumento.DataSource = new List<object>()
            {
                new { CodigoTipoInstrumento = 0,  DescricaoTipoInstrumento = "Todos" },
                new { CodigoTipoInstrumento = 1,  DescricaoTipoInstrumento = "Agenda Regulatória" },
                new { CodigoTipoInstrumento = 2,  DescricaoTipoInstrumento = "Desburocratização" },
                new { CodigoTipoInstrumento = 3,  DescricaoTipoInstrumento = "Gestão de Riscos" },
                new { CodigoTipoInstrumento = 4,  DescricaoTipoInstrumento = "Integridade" },
                new { CodigoTipoInstrumento = 5,  DescricaoTipoInstrumento = "Iniciativa Estratégica" }
            };

            ddlTipoInstrumento.DataTextField = "DescricaoTipoInstrumento";
            ddlTipoInstrumento.DataValueField = "CodigoTipoInstrumento";
            ddlTipoInstrumento.DataBind();
        }

        #endregion
    }
}