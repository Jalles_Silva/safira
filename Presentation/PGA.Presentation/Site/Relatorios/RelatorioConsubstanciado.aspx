﻿<%@ Page Title="SAFIRA - Relatório / Cadastro Anual Consubstanciado" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false" CodeBehind="RelatorioConsubstanciado.aspx.cs"
    Inherits="PGA.Presentation.Site.Relatorios.RelatorioConsubstanciado" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Relatório / Cadastro Anual Consubstanciado</h3>
                    </div>
                    <div class="panel-body">
                        <asp:HiddenField ID="hdfCodigoSeqSumarioExecutivo" runat="server" />
                        <asp:HiddenField ID="hdfCodigoSeqApresentacao" runat="server" />
                        <asp:HiddenField ID="hdfCodigoSeqPlanoEstrategicoPlanoGestaoAnual" runat="server" />
                        <asp:HiddenField ID="hdfUnidades" runat="server" />
                        <div class="col-xs-1 col-sm-1">
                            <div class="form-group">
                                <asp:Label ID="lblAnoExercicio" AssociatedControlID="lblAnoExercicio" Text="Ano" runat="server" />
                                <br />
                                <br />
                                <asp:DropDownList ID="ddlAnoExercicio" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-xs-11 col-sm-11">
                            <div class="area-table">
                                <asp:Image ID="imgVerde" runat="server" ImageUrl="~/App_Themes/Default/images/imgVerdade.png" Width="14px" />
                                <asp:Label ID="lblResutadosInformados" runat="server"><strong>Resutados Informados</strong></asp:Label>
                                &nbsp;
                                <asp:Image ID="imgAmarelo" runat="server" ImageUrl="~/App_Themes/Default/images/imgAmarelo.png" Width="14px" />
                                <asp:Label ID="lblResutadosPendentes" runat="server"><strong>Resutados Pendentes</strong></asp:Label>

                                <table border="1" id="tbAreas" runat="server"></table>
                            </div>
                        </div>
                        <div class="row" runat="server" id="botaoSalvar">
                            <div class="col-xs-12">
                                &nbsp;&nbsp;
                                <asp:LinkButton ID="btnSumario" runat="server" SkinID="btnSumario" CausesValidation="false" data-toggle="modal" data-target="#modalSumarioExecutivo" />
                                <asp:LinkButton ID="btnApresentacao" runat="server" SkinID="btnApresentacao" CausesValidation="false" data-toggle="modal" data-target="#modalApresentacao" />
                                <asp:LinkButton ID="btnPlanoEstrategico" runat="server" SkinID="btnPlanoEstrategico" CausesValidation="false" data-toggle="modal" data-target="#modalPlanoEstrategicoPlanoGestaoAnual" />
                                <asp:LinkButton ID="btnResultadosAlcancados" runat="server" SkinID="btnResultadosAlcancados" CausesValidation="false" data-toggle="modal" data-target="#modalResultadosAlcancados" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="upPnlObjetivoEstrategicoPE" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="area-table" runat="server" id="arObjetivoEstrategicoPE">
                                            <asp:Table ID="tbObjetivoEstrategicoPE" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center" Width="80%">
                                                <asp:TableHeaderRow>
                                                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left" Text="Por Objetivo Estratégico PE" BackColor="#CCCCCC" />
                                                    <asp:TableCell ColumnSpan="7" HorizontalAlign="Left" BackColor="#CCCCCC">
                                                        <asp:DropDownList ID="ddlObjetivoEstrategicoPE" runat="server" CssClass="form-control" AutoPostBack="true" BackColor="#CCCCCC" />
                                                    </asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Right" BackColor="#CCCCCC">
                                                        <asp:LinkButton ID="lnkObjetivoPE" runat="server" data-toggle="modal" data-target="#modalObjetivosPE">
                                                            <i aria-hidden="true" class="glyphicon glyphicon-th-list" style="margin-bottom:10px;"></i>
                                                        </asp:LinkButton>
                                                    </asp:TableCell>
                                                </asp:TableHeaderRow>
                                                <asp:TableHeaderRow>
                                                    <asp:TableCell BackColor="#D8EADE" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Metas" BackColor="#D8EADE" Width="40px" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Indicadores" BackColor="#D8EADE" Width="40px" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Atividades" BackColor="#D8EADE" Width="40px" />
                                                </asp:TableHeaderRow>
                                                <asp:TableHeaderRow>
                                                    <asp:TableHeaderCell Width="100px" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                </asp:TableHeaderRow>
                                            </asp:Table>
                                            <div style="text-align: center">
                                                <strong>Nota:</strong> Uma mesma Meta, Indicador e Atividade podem contribuir para mais um Objetivo, assim pode haver duplicação no<br>
                                                somatório dos valores associados a cada Objetivo Estratégico
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="upPnlObjetivoEstrategicoPPA" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="area-table" runat="server" id="arObjetivoEstrategicoPPA">
                                            <asp:Table ID="tbObjetivoEstrategicoPPA" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center" Width="80%">
                                                <asp:TableHeaderRow>
                                                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left" Text="Por Objetivo Estratégico PPA" BackColor="#CCCCCC" />
                                                    <asp:TableCell ColumnSpan="7" HorizontalAlign="Left" BackColor="#CCCCCC">
                                                        <asp:DropDownList ID="ddlObjetivoEstrategicoPPA" runat="server" CssClass="form-control" AutoPostBack="true" BackColor="#CCCCCC" />
                                                    </asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Right" BackColor="#CCCCCC">
                                                        <asp:LinkButton ID="lnkObjetivoPPA" runat="server" data-toggle="modal" data-target="#modalObjetivosPPA">
                                                            <i aria-hidden="true" class="glyphicon glyphicon-th-list" style="margin-bottom:10px;"></i>
                                                        </asp:LinkButton>
                                                    </asp:TableCell>
                                                </asp:TableHeaderRow>
                                                <asp:TableHeaderRow>
                                                    <asp:TableCell BackColor="#D8EADE" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Metas" BackColor="#D8EADE" Width="40px" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Indicadores" BackColor="#D8EADE" Width="40px" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Atividades" BackColor="#D8EADE" Width="40px" />
                                                </asp:TableHeaderRow>
                                                <asp:TableHeaderRow>
                                                    <asp:TableHeaderCell Width="100px" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                </asp:TableHeaderRow>
                                            </asp:Table>
                                            <div style="text-align: center">
                                                <strong>Nota:</strong> Uma mesma Meta, Indicador e Atividade podem contribuir para mais um Objetivo, assim pode haver duplicação no<br>
                                                somatório dos valores associados a cada Objetivo Estratégico
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="upPnlMetas" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="area-table" runat="server" id="arMetasTotais">
                                            <asp:Table ID="tbMetasTotais" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center" Width="80%">
                                                <asp:TableHeaderRow>
                                                    <asp:TableCell ColumnSpan="9" HorizontalAlign="Left" Text="Por Metas" BackColor="#CCCCCC" />
                                                    <asp:TableCell HorizontalAlign="Right" BackColor="#CCCCCC">
                                                        <asp:LinkButton ID="lnkMetasTotais" runat="server" data-toggle="modal" data-target="#modalMetas">
                                                            <i aria-hidden="true" class="glyphicon glyphicon-th-list" style="margin-bottom:10px;"></i>
                                                        </asp:LinkButton>
                                                    </asp:TableCell>
                                                </asp:TableHeaderRow>
                                                <asp:TableHeaderRow>
                                                    <asp:TableCell BackColor="#D8EADE" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Total" BackColor="#D8EADE" Width="40px" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="100%" BackColor="#D8EADE" Width="40px" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="+90%" BackColor="#D8EADE" Width="40px" />
                                                </asp:TableHeaderRow>
                                                <asp:TableHeaderRow>
                                                    <asp:TableHeaderCell Width="100px" />
                                                    <asp:TableHeaderCell Text="Meta" />
                                                    <asp:TableHeaderCell Text="Indicadores" />
                                                    <asp:TableHeaderCell Text="Atividades" />
                                                    <asp:TableHeaderCell Text="Meta" />
                                                    <asp:TableHeaderCell Text="Indicadores" />
                                                    <asp:TableHeaderCell Text="Atividades" />
                                                    <asp:TableHeaderCell Text="Meta" />
                                                    <asp:TableHeaderCell Text="Indicadores" />
                                                    <asp:TableHeaderCell Text="Atividades" />
                                                </asp:TableHeaderRow>
                                            </asp:Table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="upPnlInstrumentos" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="area-table" runat="server" id="arInstrumentosTotais">
                                            <asp:Table ID="tbInstrumentosTotais" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center" Width="80%">
                                                <asp:TableHeaderRow>
                                                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left" Text="Por Instrumentos" BackColor="#CCCCCC" />
                                                    <asp:TableCell ColumnSpan="7" HorizontalAlign="Left" BackColor="#CCCCCC">
                                                        <asp:DropDownList ID="ddlInstrumentos" runat="server" CssClass="form-control" AutoPostBack="true" BackColor="#CCCCCC" />
                                                    </asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Right" BackColor="#CCCCCC">
                                                        <asp:LinkButton ID="lnkInstrumentos" runat="server" data-toggle="modal" data-target="#modalInstrumentos">
                                                            <i aria-hidden="true" class="glyphicon glyphicon-th-list" style="margin-bottom:10px;"></i>
                                                        </asp:LinkButton>
                                                    </asp:TableCell>
                                                </asp:TableHeaderRow>
                                                <asp:TableHeaderRow>
                                                    <asp:TableCell BackColor="#D8EADE" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Metas" BackColor="#D8EADE" Width="40px" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Indicadores" BackColor="#D8EADE" Width="40px" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Atividades" BackColor="#D8EADE" Width="40px" />
                                                </asp:TableHeaderRow>
                                                <asp:TableHeaderRow>
                                                    <asp:TableHeaderCell Width="100px" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                </asp:TableHeaderRow>
                                            </asp:Table>
                                            <div style="text-align: center">
                                                <strong>Nota:</strong> Uma mesma Meta, Indicador e Atividade podem contribuir para mais um Instrumento, assim pode haver duplicação no<br>
                                                somatório dos valores associados a cada Instrumento
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="upPnlAlinhamentos" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="area-table" runat="server" id="arAlinhamentosTotais">
                                            <asp:Table ID="tbAlinhamentosTotais" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center" Width="80%">
                                                <asp:TableHeaderRow>
                                                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left" Text="Por Alinhamento" BackColor="#CCCCCC" />
                                                    <asp:TableCell ColumnSpan="7" HorizontalAlign="Left" BackColor="#CCCCCC">
                                                        <asp:DropDownList ID="ddlAlinhamentos" runat="server" CssClass="form-control" AutoPostBack="true" BackColor="#CCCCCC" />
                                                    </asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Right" BackColor="#CCCCCC">
                                                        <asp:LinkButton ID="lnkAlinhamentos" runat="server" data-toggle="modal" data-target="#modalAlinhamentos">
                                                            <i aria-hidden="true" class="glyphicon glyphicon-th-list" style="margin-bottom:10px;"></i>
                                                        </asp:LinkButton>
                                                    </asp:TableCell>
                                                </asp:TableHeaderRow>
                                                <asp:TableHeaderRow>
                                                    <asp:TableCell BackColor="#D8EADE" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Metas" BackColor="#D8EADE" Width="40px" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Indicadores" BackColor="#D8EADE" Width="40px" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Atividades" BackColor="#D8EADE" Width="40px" />
                                                </asp:TableHeaderRow>
                                                <asp:TableHeaderRow>
                                                    <asp:TableHeaderCell Width="100px" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                </asp:TableHeaderRow>
                                            </asp:Table>
                                            <div style="text-align: center">
                                                <strong>Nota:</strong> Uma mesma Meta, Indicador e Atividade podem contribuir para mais um Alinhamento, assim pode haver duplicação no<br>
                                                somatório dos valores associados a cada Alinhamento
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="upPnlTipo" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="area-table" runat="server" id="arTipoMetaTotal">
                                            <asp:Table ID="tbTipoMetaTotal" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center" Width="80%">
                                                <asp:TableHeaderRow>
                                                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left" Text="Por Tipo" BackColor="#CCCCCC" />
                                                    <asp:TableCell ColumnSpan="7" HorizontalAlign="Left" BackColor="#CCCCCC">
                                                        <asp:DropDownList ID="ddlTipoMeta" runat="server" CssClass="form-control" AutoPostBack="true" BackColor="#CCCCCC" />
                                                    </asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Right" BackColor="#CCCCCC">
                                                        <asp:LinkButton ID="lnkTipoMeta" runat="server" data-toggle="modal" data-target="#modalTipoMeta">
                                                            <i aria-hidden="true" class="glyphicon glyphicon-th-list" style="margin-bottom:10px;"></i>
                                                        </asp:LinkButton>
                                                    </asp:TableCell>
                                                </asp:TableHeaderRow>
                                                <asp:TableHeaderRow>
                                                    <asp:TableCell BackColor="#D8EADE" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Metas" BackColor="#D8EADE" Width="40px" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Indicadores" BackColor="#D8EADE" Width="40px" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Atividades" BackColor="#D8EADE" Width="40px" />
                                                </asp:TableHeaderRow>
                                                <asp:TableHeaderRow>
                                                    <asp:TableHeaderCell Width="100px" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                    <asp:TableHeaderCell Text="Total" />
                                                    <asp:TableHeaderCell Text="100%" />
                                                    <asp:TableHeaderCell Text="+90%" />
                                                </asp:TableHeaderRow>
                                            </asp:Table>
                                            <div style="text-align: center">
                                                <strong>Nota:</strong> Uma mesma Meta, Indicador e Atividade podem contribuir para mais um Tipo de Meta, assim pode haver duplicação no<br>
                                                somatório dos valores associados a cada Tipo de Meta
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="upPnlObjetivoEstrategico" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="area-table" runat="server" id="arObjetivoEstrategico">
                                            <asp:Table ID="tbObjetivoEstrategico" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center" Width="80%">
                                                <asp:TableHeaderRow>
                                                    <asp:TableCell HorizontalAlign="Left" Text="Objetivo Estratégico" BackColor="#CCCCCC" />
                                                    <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" BackColor="#CCCCCC">
                                                        <asp:DropDownList ID="ddlObjetivoEstrategico" runat="server" CssClass="form-control" AutoPostBack="true" BackColor="#CCCCCC" />
                                                    </asp:TableCell>
                                                    <asp:TableCell HorizontalAlign="Right" BackColor="#CCCCCC">
                                                        <asp:LinkButton ID="lnkObjetivoEstrategico" runat="server" data-toggle="modal" data-target="#modalObjetivoEstrategico">
                                                            <i aria-hidden="true" class="glyphicon glyphicon-th-list" style="margin-bottom:10px;"></i>
                                                        </asp:LinkButton>
                                                    </asp:TableCell>
                                                </asp:TableHeaderRow>
                                                <asp:TableHeaderRow>
                                                    <asp:TableHeaderCell Width="150px" />
                                                    <asp:TableHeaderCell Text="Recursos Previstos associados ás Metas que contribuem para o objetivo" />
                                                    <asp:TableHeaderCell Text="Recursos Previstos rateados para alocação ao objetivo" />
                                                    <asp:TableHeaderCell Text="Recursos Utilizados associados às Metas que contribuem para o objetivo" />
                                                    <asp:TableHeaderCell Text="Recursos Utilizados rateados para alocação ao objetivo" />
                                                </asp:TableHeaderRow>
                                            </asp:Table>
                                            <div style="text-align: center">
                                                <strong>Nota:</strong> Os recursos são distruibuídos aos Objetivos de acordo com o valor total alocado para a Meta, a qual contruibuir para<br>
                                                mais de um Objetivo, assim não há como mensurar o montante diretamente alocado a cada Objetivo Estratégico, havendo um<br>
                                                superdimensionamento nos valores não reatados. O rateio é realizado com base na proporção dos montantes associoados às<br>
                                                Metas que contruibuem para o Objetivo Estratégico. 
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <br />
                        <div class="row pull-right" id="btnExportar" runat="server">
                            <div class="form-group">
                                <div class="col-xs-6 col-sm-6">
                                    <div class="form-group">
                                        <asp:LinkButton type="button" ID="btnExportarExcel" runat="server" Style="width: auto" class="btn btn-antt" aria-haspopup="true">
                                            <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>&nbsp;Extração Excel
                                        </asp:LinkButton>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-antt dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>&nbsp;Exportar <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <asp:LinkButton ID="lnkReportPDF" runat="server" CausesValidation="false" Text="PDF" /></li>
                                            <li>
                                                <asp:LinkButton ID="lnkReportWord" runat="server" CausesValidation="false" Text="Word" /></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalSumarioExecutivo" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Incluir Sumário Executivo do Relatório</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <asp:TextBox ID="txtDescSumarioExecutivo" Rows="12" runat="server" CssClass="form-control" MaxLength="8000" TextMode="MultiLine" />
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <asp:UpdatePanel ID="updBotoesSumarioExecutivo" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="btnSalvarSumario" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="SalvarSumarioExecutivo" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalApresentacao" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Incluir Apresentação Do Relatório</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <asp:TextBox ID="txtDescApresentacao" Rows="12" runat="server" CssClass="form-control" MaxLength="8000" TextMode="MultiLine" />
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <asp:UpdatePanel ID="updBotoesApresentacao" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="btnSalvarApresentacao" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="SalvarApresentacao" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalPlanoEstrategicoPlanoGestaoAnual" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Incluir descrição acerca Plano Estratégico e Plano de Gestão Anual</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <asp:TextBox ID="txtDescPlanoEstrategico" Rows="12" runat="server" CssClass="form-control" MaxLength="8000" TextMode="MultiLine" />
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <asp:UpdatePanel ID="updBotoesPlanoEstrategico" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="btnSalvarPlanoEstrategico" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="SalvarPlanoEstrategico" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalResultadosAlcancados" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Visualizar Resultados Alcançados</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:UpdatePanel ID="upPnlRelatorioConsubstanciado" runat="server">
                                        <ContentTemplate>
                                            <div class="popup-table">
                                                <asp:Table ID="tbResultadoAlcancado" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalObjetivosPE" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Detalhe dos Totalizadores Por Objetivo Estratégico PE</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:UpdatePanel ID="upPnlObjetivosPeDetalhadoPE" runat="server">
                                        <ContentTemplate>
                                            <div class="popup-table">
                                                <asp:Table ID="tbObjetivosPeDetalhado" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center">
                                                    <asp:TableHeaderRow>
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" Text="Por Objetivo Estratégico" BackColor="#CCCCCC" />
                                                        <asp:TableCell ColumnSpan="7" HorizontalAlign="Left" BackColor="#CCCCCC">
                                                            <asp:DropDownList ID="ddlObjetivoEstrategicoPeDetalhado" runat="server" CssClass="form-control" AutoPostBack="true" BackColor="#CCCCCC" />
                                                        </asp:TableCell>
                                                    </asp:TableHeaderRow>
                                                    <asp:TableHeaderRow>
                                                        <asp:TableCell BackColor="#D8EADE" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Metas" BackColor="#D8EADE" Width="40px" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Indicadores" BackColor="#D8EADE" Width="40px" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Atividades" BackColor="#D8EADE" Width="40px" />
                                                    </asp:TableHeaderRow>
                                                    <asp:TableHeaderRow>
                                                        <asp:TableHeaderCell Text="Áreas" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                    </asp:TableHeaderRow>
                                                </asp:Table>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalObjetivosPPA" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Detalhe dos Totalizadores Por Objetivo Estratégico PPA</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:UpdatePanel ID="upPnlObjetivosPeDetalhadoPPA" runat="server">
                                        <ContentTemplate>
                                            <div class="popup-table">
                                                <asp:Table ID="tbObjetivosPPaDetalhado" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center">
                                                    <asp:TableHeaderRow>
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" Text="Por Objetivo Estratégico" BackColor="#CCCCCC" />
                                                        <asp:TableCell ColumnSpan="7" HorizontalAlign="Left" BackColor="#CCCCCC">
                                                            <asp:DropDownList ID="ddlObjetivoEstrategicoPPaDetalhado" runat="server" CssClass="form-control" AutoPostBack="true" BackColor="#CCCCCC" />
                                                        </asp:TableCell>
                                                    </asp:TableHeaderRow>
                                                    <asp:TableHeaderRow>
                                                        <asp:TableCell BackColor="#D8EADE" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Metas" BackColor="#D8EADE" Width="40px" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Indicadores" BackColor="#D8EADE" Width="40px" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Atividades" BackColor="#D8EADE" Width="40px" />
                                                    </asp:TableHeaderRow>
                                                    <asp:TableHeaderRow>
                                                        <asp:TableHeaderCell Text="Áreas" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                    </asp:TableHeaderRow>
                                                </asp:Table>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalMetas" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Detalhe dos Totalizadores Por Meta</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:UpdatePanel ID="upPnlDetalheMetas" runat="server">
                                        <ContentTemplate>
                                            <div class="popup-table">
                                                <asp:Table ID="tbMetasDetalhado" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center">
                                                    <asp:TableHeaderRow>
                                                        <asp:TableCell ColumnSpan="10" HorizontalAlign="Left" Text="Por Metas" BackColor="#CCCCCC" />
                                                    </asp:TableHeaderRow>
                                                    <asp:TableHeaderRow>
                                                        <asp:TableCell BackColor="#D8EADE" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Total" BackColor="#D8EADE" Width="40px" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="100%" BackColor="#D8EADE" Width="40px" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="+90%" BackColor="#D8EADE" Width="40px" />
                                                    </asp:TableHeaderRow>
                                                    <asp:TableHeaderRow>
                                                        <asp:TableHeaderCell Width="75px" />
                                                        <asp:TableHeaderCell Text="Meta" />
                                                        <asp:TableHeaderCell Text="Indicadores" />
                                                        <asp:TableHeaderCell Text="Atividades" />
                                                        <asp:TableHeaderCell Text="Meta" />
                                                        <asp:TableHeaderCell Text="Indicadores" />
                                                        <asp:TableHeaderCell Text="Atividades" />
                                                        <asp:TableHeaderCell Text="Meta" />
                                                        <asp:TableHeaderCell Text="Indicadores" />
                                                        <asp:TableHeaderCell Text="Atividades" />
                                                    </asp:TableHeaderRow>
                                                </asp:Table>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalInstrumentos" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Detalhe dos Totalizadores Por Instrumento</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:UpdatePanel ID="upPnlDetalheInstrumentos" runat="server">
                                        <ContentTemplate>
                                            <div class="popup-table">
                                                <asp:Table ID="tbInstrumentoDetalhado" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center">
                                                    <asp:TableHeaderRow>
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" Text="Por Instrumentos" BackColor="#CCCCCC" />
                                                        <asp:TableCell ColumnSpan="7" HorizontalAlign="Left" BackColor="#CCCCCC">
                                                            <asp:DropDownList ID="ddlInstrumentoDetalhado" runat="server" CssClass="form-control" AutoPostBack="true" BackColor="#CCCCCC" />
                                                        </asp:TableCell>
                                                    </asp:TableHeaderRow>
                                                    <asp:TableHeaderRow>
                                                        <asp:TableCell BackColor="#D8EADE" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Metas" BackColor="#D8EADE" Width="40px" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Indicadores" BackColor="#D8EADE" Width="40px" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Atividades" BackColor="#D8EADE" Width="40px" />
                                                    </asp:TableHeaderRow>
                                                    <asp:TableHeaderRow>
                                                        <asp:TableHeaderCell Width="100px" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                    </asp:TableHeaderRow>
                                                </asp:Table>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAlinhamentos" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Detalhe dos Totalizadores Por Alinhamento</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:UpdatePanel ID="upPnlDetalheAlinhamentos" runat="server">
                                        <ContentTemplate>
                                            <div class="popup-table">
                                                <asp:Table ID="tbAlinhamentoDetalhado" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center">
                                                    <asp:TableHeaderRow>
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" Text="Por Alinhamento" BackColor="#CCCCCC" />
                                                        <asp:TableCell ColumnSpan="7" HorizontalAlign="Left" BackColor="#CCCCCC">
                                                            <asp:DropDownList ID="ddlAlinhamentoDetalhado" runat="server" CssClass="form-control" AutoPostBack="true" BackColor="#CCCCCC" />
                                                        </asp:TableCell>
                                                    </asp:TableHeaderRow>
                                                    <asp:TableHeaderRow>
                                                        <asp:TableCell BackColor="#D8EADE" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Metas" BackColor="#D8EADE" Width="40px" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Indicadores" BackColor="#D8EADE" Width="40px" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Atividades" BackColor="#D8EADE" Width="40px" />
                                                    </asp:TableHeaderRow>
                                                    <asp:TableHeaderRow>
                                                        <asp:TableHeaderCell Width="100px" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                    </asp:TableHeaderRow>
                                                </asp:Table>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalTipoMeta" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Detalhe dos Totalizadores Por Tipo</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:UpdatePanel ID="upPnlDetalheMeta" runat="server">
                                        <ContentTemplate>
                                            <div class="popup-table">
                                                <asp:Table ID="tbTipoMetaDetalhado" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center">
                                                    <asp:TableHeaderRow>
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Left" Text="Por Tipo" BackColor="#CCCCCC" />
                                                        <asp:TableCell ColumnSpan="7" HorizontalAlign="Left" BackColor="#CCCCCC">
                                                            <asp:DropDownList ID="ddlTipoMetaDetalhado" runat="server" CssClass="form-control" AutoPostBack="true" BackColor="#CCCCCC" />
                                                        </asp:TableCell>
                                                    </asp:TableHeaderRow>
                                                    <asp:TableHeaderRow>
                                                        <asp:TableCell BackColor="#D8EADE" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Metas" BackColor="#D8EADE" Width="40px" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Indicadores" BackColor="#D8EADE" Width="40px" />
                                                        <asp:TableCell ColumnSpan="3" HorizontalAlign="Center" Text="Atividades" BackColor="#D8EADE" Width="40px" />
                                                    </asp:TableHeaderRow>
                                                    <asp:TableHeaderRow>
                                                        <asp:TableHeaderCell Width="100px" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                        <asp:TableHeaderCell Text="Total" />
                                                        <asp:TableHeaderCell Text="100%" />
                                                        <asp:TableHeaderCell Text="+90%" />
                                                    </asp:TableHeaderRow>
                                                </asp:Table>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalObjetivoEstrategico" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Detalhe dos Totalizadores Por Objetivo Estratégico</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:UpdatePanel ID="upPnlDetalheObjetivoEstrategico" runat="server">
                                        <ContentTemplate>
                                            <div class="popup-table">
                                                <asp:Table ID="tbObjetivoEstrategicoDetalhe" runat="server" CellPadding="10" GridLines="Both" HorizontalAlign="Center">
                                                    <asp:TableHeaderRow>
                                                        <asp:TableCell HorizontalAlign="Left" Text="Objetivo Estratégico" BackColor="#CCCCCC" />
                                                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="4" BackColor="#CCCCCC">
                                                            <asp:DropDownList ID="ddlObjetivoEstrategicoDetalhe" runat="server" CssClass="form-control" AutoPostBack="true" BackColor="#CCCCCC" />
                                                        </asp:TableCell>
                                                    </asp:TableHeaderRow>
                                                    <asp:TableHeaderRow>
                                                        <asp:TableHeaderCell Text="Áreas" Width="150px" />
                                                        <asp:TableHeaderCell Text="Recursos Previstos associados ás Metas que contribuem para o objetivo" />
                                                        <asp:TableHeaderCell Text="Recursos Previstos rateados para alocação ao objetivo" />
                                                        <asp:TableHeaderCell Text="Recursos Utilizados associados às Metas que contribuem para o objetivo" />
                                                        <asp:TableHeaderCell Text="Recursos Utilizados rateados para alocação ao objetivo" />
                                                    </asp:TableHeaderRow>
                                                </asp:Table>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
