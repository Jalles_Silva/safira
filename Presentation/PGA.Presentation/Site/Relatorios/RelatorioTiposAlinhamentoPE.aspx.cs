﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using SQFramework.Web.Report;
using SQFramework.Core.Reflection;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SQFramework.Data.Pagging;
using PGA.Common;
using System.Diagnostics;
using System.Data;
using ClosedXML.Excel;
using System.IO;

namespace PGA.Presentation.Site.Relatorios
{
    public partial class RelatorioTiposAlinhamentoPE : CustomPageBase
    {
        #region [Properties]

        private Dictionary<string, IList<DTOIniciativa>> AlinhamentosPE
        {
            get
            {
                return (Dictionary<string, IList<DTOIniciativa>>)Session["AlinhamentosPE"];
            }
            set
            {
                Session["AlinhamentosPE"] = value;
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnPesquisar.Click += new EventHandler(btnPesquisar_Click);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);

            btnExportarExcel.Click += new EventHandler(btnExportarExcel_Click);

            ddlAnoExercicio.SelectedIndexChanged += ddlAnoExercicio_SelectedIndexChanged;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (UsuarioPublico && !(UsuarioConsultor || UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador))
                {
                    ddlTipoInstrumento.Visible = false;
                    ddlMetaInterna.Visible = false;
                    lblTipoInstrumento.Visible = false;
                    lblMetaInterna.Visible = false;
                }
                else
                {
                    ddlTipoInstrumento.Visible = true;
                    ddlMetaInterna.Visible = true;
                    lblTipoInstrumento.Visible = true;
                    lblMetaInterna.Visible = true;
                }
                if (UsuarioConsultor || UsuarioPublico || UsuarioCadastrador || UsuarioAdministrador)
                {
                    if (!IsPostBack)
                        CarregarControles();
                }
                else
                    WebHelper.LogoffWithNewRedirection("~/Default.aspx");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        void ddlAnoExercicio_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarArea();
        }

        protected void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                ConsultarDados(0);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void lnkBaixarAnexo_Click(object sender, EventArgs e)
        {
            var lnk = (LinkButton)sender;

            WebHelper.DownloadFile(System.IO.File.ReadAllBytes(lnk.Attributes["CaminhoAnexo"]), lnk.Text);
        }

        protected void btnExportarExcel_Click(object sender, EventArgs e)
        {
            try
            {
                ExportReportExcel();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            try
            {
                switch (e.Command)
                {
                    case "Recarregar":
                        if (e.Result == MessageBoxResult.OK)
                            btnPesquisar_Click(btnPesquisar, null);
                        break;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            try
            {
                ConsultarDados(e.NewPage - 1);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion

        #region [Methods]

        private void CarregarControles()
        {
            AlinhamentosPE = new Dictionary<string, IList<DTOIniciativa>>();
            CarregarExercicio();
            CarregarArea();
            CarregarTipoMetas();
            CarregarObjetivos();
            CarregarTipoInstrumento();
        }

        private void CarregarExercicio()
        {
            ddlAnoExercicio.DataSource = this.ObterPGAService().ListarExercicios();
            ddlAnoExercicio.DataTextField = "Ano";
            ddlAnoExercicio.DataValueField = "Ano";
            ddlAnoExercicio.DataBind();

            var ExercicioAtual = this.ObterPGAService().ObterExercicioAtual();
            ddlAnoExercicio.SelectedValue = this.ObterPGAService().ObterExercicioAtual() != null ? ExercicioAtual.Ano.ToString() : DateTime.Now.Year.ToString();
        }

        private void CarregarArea()
        {
            drpArea.DataSource = this.ObterPGAService().ListarCorporativoUnidadePorExercicio(ddlAnoExercicio.SelectedValue.ToInt32());
            drpArea.DataTextField = "DescricaoUnidade";
            drpArea.DataValueField = "CodigoUnidade";
            drpArea.DataBind();
            drpArea.Items.Insert(0, new ListItem("Todas", ""));
        }

        private void CarregarTipoMetas()
        {
            ddlDescricaoMeta.DataSource = this.ObterPGAService().ListarTipoMetas();
            ddlDescricaoMeta.DataTextField = "DescricaoTipoMeta";
            ddlDescricaoMeta.DataValueField = "CodigoSeqTipoMeta";
            ddlDescricaoMeta.DataBind();
            ddlDescricaoMeta.Items.Insert(0, new ListItem("Todos", ""));
        }

        private void CarregarObjetivos()
        {
            drpObjetivo.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarObjetivosPorTipo(EnumTipoObjetivo.Objetivos);
            drpObjetivo.DataTextField = "NomeObjetivo";
            drpObjetivo.DataValueField = "CodigoSeqObjetivo";
            drpObjetivo.DataBind();
            drpObjetivo.Items.Insert(0, new ListItem("Todos", "0"));
        }

        private void CarregarTipoInstrumento()
        {
            ddlTipoInstrumento.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTiposInstrumento();
            ddlTipoInstrumento.DataTextField = "DescricaoTipoInstrumento";
            ddlTipoInstrumento.DataValueField = "CodigoSeqTipoInstrumento";
            ddlTipoInstrumento.DataBind();
            ddlTipoInstrumento.Items.Insert(0, new ListItem("Todos", "0"));
        }

        private void ConsultarDados(int pageIndex)
        {
            try
            {
                var iniciativas = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarIniciativaPorTipoEUnidade(1, drpArea.SelectedItem.Text, rblSituacao.SelectedValue, -1, -1, "CorporativoUnidade.DescricaoUnidade", true);

                List<DTOIniciativa> source = iniciativas.Entities.Where(i => (drpTipoIniciativa.SelectedValue != "" ? i.TpIniciativaProjeto == drpTipoIniciativa.SelectedValue : true) || i.TpIniciativaProjeto == null).ToList();

                List<string> unidades = source.Select(i => i.CorporativoUnidade.DescricaoUnidade).Distinct().ToList();
                Dictionary<string, IList<DTOIniciativa>> lista = new Dictionary<string, IList<DTOIniciativa>>();
                AlinhamentosPE = new Dictionary<string, IList<DTOIniciativa>>();

                foreach (var item in unidades)
                {
                    var incts = source.Where(i => i.CorporativoUnidade.DescricaoUnidade == item).OrderBy(x => x.DescricaoIniciativa).ToList();

                    foreach (var inct in incts)
                    {
                        var objetivos = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarObjetivosPorIniciativa(inct.CodigoSeqIniciativa, -1, -1, "", true);

                        inct.Objetivos = objetivos.Entities.ToList();

                        var metas = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarMetasPorUnidadeMeta(drpArea.SelectedItem.Text, ddlAnoExercicio.SelectedValue.ToInt32(), ddlMetaInterna.SelectedValue.ToInt32(), 0, -1, -1);

                        var ms = metas.Entities.Where(m => m.CodigoSeqIniciativaPE == inct.CodigoSeqIniciativa).ToList();

                        foreach (var m in ms)
                        {
                            var acoes = this.ObterPGAService().ListaDetalheAtividade(true, true, true, ddlAnoExercicio.SelectedValue.ToInt32(), drpArea.SelectedItem.Text, 0, ddlDescricaoMeta.SelectedValue.ToInt32(),
                                (UsuarioPublico && !(UsuarioConsultor || UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador)) ? 2 : ddlMetaInterna.SelectedValue.ToInt32(), ddlTipoInstrumento.SelectedValue.ToInt32(), -1, -1);

                            m.Acoes = acoes.Entities.Where(a => a.CodigoMeta == m.CodSeqMeta).ToList();

                            var indicadores = this.ObterPGAService().ListaDetalheIndicador(true, true, true, ddlAnoExercicio.SelectedValue.ToInt32(), drpArea.SelectedItem.Text, 0, ddlDescricaoMeta.SelectedValue.ToInt32(),
                                (UsuarioPublico && !(UsuarioConsultor || UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador)) ? 2 : ddlMetaInterna.SelectedValue.ToInt32(), ddlTipoInstrumento.SelectedValue.ToInt32(), -1, -1);

                            m.Indicadores = indicadores.Entities.Where(i => i.CodigoMeta == m.CodSeqMeta).ToList();
                        }

                        inct.Metas = ms;
                    }

                    if (drpObjetivo.SelectedValue != "0")
                    {
                        incts = incts.Where(i => i.Objetivos.Where(o => o.Objetivo.CodigoSeqObjetivo == drpObjetivo.SelectedValue.ToInt32()).Count() != 0).ToList();

                        if (incts.Count > 0)
                            lista.Add(item, incts);
                    }
                    else
                        lista.Add(item, incts);
                }

                AlinhamentosPE = lista;

                var incs = new Dictionary<string, IList<DTOIniciativa>>();
                var p = pageIndex * 3;

                for (int i = p; i < p + 3; i++)
                {
                    if (AlinhamentosPE.Count <= i)
                        break;

                    incs.Add(AlinhamentosPE.Keys.ElementAt(i), AlinhamentosPE.Values.ElementAt(i));
                }

                rptAlinhamentosPE.DataSource = incs;
                rptAlinhamentosPE.DataBind();

                btnExportar.Visible = (AlinhamentosPE.Count > 0);

                ucPaginatorConsulta.Visible = (AlinhamentosPE.Count > 0);
                ucPaginatorConsulta.TotalRecords = AlinhamentosPE.Count;
                ucPaginatorConsulta.PageIndex = pageIndex + 1;
                ucPaginatorConsulta.PageSize = 3;
                ucPaginatorConsulta.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void ExportReportExcel()
        {
            try
            {
                var watch = new Stopwatch();
                watch.Start();
                DataSet ds = GetDataSet();

                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(ds);
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Buffer = true;
                    HttpContext.Current.Response.Charset = "";
                    HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=RelatorioProjetosIniciativas.xlsx");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public DataSet GetDataSet()
        {
            DataSet ds = new DataSet();
            string nmCol, tpCol;

            DataTable dtProjetosIniciativas = new DataTable();
            DataTable dtObjetivos = new DataTable();
            DataTable dtMetas = new DataTable();
            DataTable dtAtividades = new DataTable();
            DataTable dtIndicadores = new DataTable();

            dtProjetosIniciativas.TableName = "Projetos_Iniciativas";
            dtObjetivos.TableName = "Objetivos Estratégicos";
            dtMetas.TableName = "Metas";
            dtAtividades.TableName = "Atividades";
            dtIndicadores.TableName = "Indicadores";

            #region [colunas]

            nmCol = "Área";
            tpCol = "System.String";
            dtProjetosIniciativas.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtObjetivos.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtMetas.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtAtividades.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtIndicadores.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Código Projeto/Iniciativa";
            tpCol = "System.Int32";
            dtProjetosIniciativas.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtObjetivos.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtMetas.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtAtividades.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtIndicadores.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Tipo";
            tpCol = "System.String";
            dtProjetosIniciativas.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtObjetivos.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtMetas.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtAtividades.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtIndicadores.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Projeto/Iniciativa";
            tpCol = "System.String";
            dtProjetosIniciativas.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtObjetivos.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtMetas.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtAtividades.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtIndicadores.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Situação";
            tpCol = "System.String";
            dtProjetosIniciativas.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtObjetivos.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtMetas.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtAtividades.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtIndicadores.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Código Objetivo";
            tpCol = "System.Int32";
            dtObjetivos.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Objetivo";
            tpCol = "System.String";
            dtObjetivos.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Porcentagem";
            tpCol = "System.String";
            dtObjetivos.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Código Meta";
            tpCol = "System.Int32";
            dtMetas.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtAtividades.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtIndicadores.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Meta";
            tpCol = "System.String";
            dtMetas.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtAtividades.Columns.Add(AdicionarColuna(nmCol, tpCol));
            dtIndicadores.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Exercício";
            tpCol = "System.String";
            dtMetas.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Tipo Meta";
            tpCol = "System.String";
            dtMetas.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Resultados Esperados";
            tpCol = "System.String";
            dtMetas.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Status";
            tpCol = "System.String";
            dtMetas.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Código Atividade";
            tpCol = "System.Int32";
            dtAtividades.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Atividade";
            tpCol = "System.String";
            dtAtividades.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Recurso Estimado";
            tpCol = "System.String";
            dtAtividades.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Recurso Utilizado";
            tpCol = "System.String";
            dtAtividades.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Data Início";
            tpCol = "System.String";
            dtAtividades.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Data Fim";
            tpCol = "System.String";
            dtAtividades.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Percentual Executado";
            tpCol = "System.String";
            dtAtividades.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Código Indicador";
            tpCol = "System.Int32";
            dtIndicadores.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Indicador";
            tpCol = "System.String";
            dtIndicadores.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Valor Alvo";
            tpCol = "System.String";
            dtIndicadores.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Data Alvo";
            tpCol = "System.String";
            dtIndicadores.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Objetivo Estratégico";
            tpCol = "System.String";
            dtIndicadores.Columns.Add(AdicionarColuna(nmCol, tpCol));

            nmCol = "Resultado em Relação ao Alvo";
            tpCol = "System.String";
            dtIndicadores.Columns.Add(AdicionarColuna(nmCol, tpCol));

            #endregion

            #region [linhas]

            foreach (var u in AlinhamentosPE)
            {
                foreach (var incs in u.Value)
                {
                    var row = dtProjetosIniciativas.NewRow();

                    row["Área"] = u.Key;
                    row["Código Projeto/Iniciativa"] = incs.CodigoSeqIniciativa;
                    row["Tipo"] = incs.TpIniciativaProjeto == "I" ? "Iniciativa" : "Projeto";
                    row["Projeto/Iniciativa"] = incs.DescricaoIniciativa;
                    row["Situação"] = incs.Ativo ? "Ativo" : "Inativo";

                    dtProjetosIniciativas.Rows.Add(row);

                    foreach (var objs in incs.Objetivos)
                    {
                        var rowObj = dtObjetivos.NewRow();

                        rowObj["Área"] = u.Key;
                        rowObj["Código Projeto/Iniciativa"] = incs.CodigoSeqIniciativa;
                        rowObj["Tipo"] = incs.TpIniciativaProjeto == "I" ? "Iniciativa" : "Projeto";
                        rowObj["Projeto/Iniciativa"] = incs.DescricaoIniciativa;
                        rowObj["Situação"] = incs.Ativo ? "Ativo" : "Inativo";
                        rowObj["Código Objetivo"] = objs.CodigoSeqIniciativaObjetivo;
                        rowObj["Objetivo"] = objs.Objetivo.NomeObjetivo;
                        rowObj["Porcentagem"] = objs.PercentualParticipacao;

                        dtObjetivos.Rows.Add(rowObj);
                    }

                    foreach (var meta in incs.Metas)
                    {
                        var rowMet = dtMetas.NewRow();

                        rowMet["Área"] = u.Key;
                        rowMet["Código Projeto/Iniciativa"] = incs.CodigoSeqIniciativa;
                        rowMet["Tipo"] = incs.TpIniciativaProjeto == "I" ? "Iniciativa" : "Projeto";
                        rowMet["Projeto/Iniciativa"] = incs.DescricaoIniciativa;
                        rowMet["Situação"] = incs.Ativo ? "Ativo" : "Inativo";
                        rowMet["Código Meta"] = meta.CodSeqMeta;
                        rowMet["Meta"] = meta.DescricaoMeta;
                        rowMet["Exercício"] = meta.AnoExercicio;
                        rowMet["Tipo Meta"] = meta.DescricaoTipoMeta;
                        rowMet["Resultados Esperados"] = meta.DescricaoResultadoEsperado;
                        rowMet["Status"] = meta.DescricaoStatusMeta;

                        dtMetas.Rows.Add(rowMet);

                        foreach (var acao in meta.Acoes)
                        {
                            var rowAca = dtAtividades.NewRow();

                            rowAca["Área"] = u.Key;
                            rowAca["Código Projeto/Iniciativa"] = incs.CodigoSeqIniciativa;
                            rowAca["Tipo"] = incs.TpIniciativaProjeto == "I" ? "Iniciativa" : "Projeto";
                            rowAca["Projeto/Iniciativa"] = incs.DescricaoIniciativa;
                            rowAca["Situação"] = incs.Ativo ? "Ativo" : "Inativo";
                            rowAca["Código Meta"] = meta.CodSeqMeta;
                            rowAca["Meta"] = meta.DescricaoMeta;
                            rowAca["Código Atividade"] = acao.CodigoSeqAcao;
                            rowAca["Atividade"] = acao.DescricaoAtividade;
                            rowAca["Recurso Estimado"] = acao.RecursoEstimadoMascara;
                            rowAca["Recurso Utilizado"] = acao.RecursoOrcamentarioUtilizadoMascara;
                            rowAca["Data Início"] = acao.DataInicioAtividade.HasValue ? acao.DataInicioAtividade.Value.ToShortDateString() : null;
                            rowAca["Data Fim"] = acao.DataFimAtividade.HasValue ? acao.DataFimAtividade.Value.ToShortDateString() : null;
                            rowAca["Percentual Executado"] = acao.PercentualExecucaoMascara;

                            dtAtividades.Rows.Add(rowAca);
                        }

                        foreach (var indc in meta.Indicadores)
                        {
                            var rowInd = dtIndicadores.NewRow();

                            rowInd["Área"] = u.Key;
                            rowInd["Código Projeto/Iniciativa"] = incs.CodigoSeqIniciativa;
                            rowInd["Tipo"] = incs.TpIniciativaProjeto == "I" ? "Iniciativa" : "Projeto";
                            rowInd["Projeto/Iniciativa"] = incs.DescricaoIniciativa;
                            rowInd["Situação"] = incs.Ativo ? "Ativo" : "Inativo";
                            rowInd["Código Meta"] = meta.CodSeqMeta;
                            rowInd["Meta"] = meta.DescricaoMeta;
                            rowInd["Código Indicador"] = indc.CodigoSeqIndicador;
                            rowInd["Indicador"] = indc.DescricaoIndicador;
                            rowInd["Valor Alvo"] = indc.ValorAlvoMascara;
                            rowInd["Data Alvo"] = indc.DataAlvo.ToShortDateString();
                            rowInd["Objetivo Estratégico"] = indc.ObjetivoEstrategico ? "Sim" : "Não";
                            rowInd["Resultado em Relação ao Alvo"] = indc.ResultadoIndicadorValorAlvo;

                            dtIndicadores.Rows.Add(rowInd);
                        }
                    }
                }
            }

            #endregion

            ds.Tables.Add(dtProjetosIniciativas);
            ds.Tables.Add(dtObjetivos);
            ds.Tables.Add(dtMetas);
            ds.Tables.Add(dtAtividades);
            ds.Tables.Add(dtIndicadores);

            return ds;
        }

        private DataColumn AdicionarColuna(String nome, String tipo)
        {
            var column = new DataColumn();
            column.ColumnName = nome;
            column.DataType = Type.GetType(tipo);

            return column;
        }
        #endregion
    }
}