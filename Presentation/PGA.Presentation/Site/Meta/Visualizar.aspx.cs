﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SQFramework.Web;
using SQFramework.Core;
using PGA.Common;
using SCA.WebControls;
using System.Diagnostics;

namespace PGA.Presentation.Site.Meta
{
    public partial class Visualizar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqMetaParameter
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqMeta").ToInt32();
            }
        }

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private int AnoExercicio
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("AnoExercicio").ToInt32();
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        private DTOAnexo Anexo
        {
            get { return (DTOAnexo)Session["anexo"]; }
            set { Session["anexo"] = value; }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);

            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
            btnAprovarMeta2741.Click += new EventHandler(btnAprovarMeta2741_Click);
            btnEnviarMeta2743.Click += btnEnviarMeta2743_Click;
            btnRejeitarMeta2742.Click += btnRejeitarMeta2742_Click;
            btnRetornarParaCadastro2744.Click += btnRetornarParaCadastro2744_Click;
            btnCancelar2746.Click += btnCancelar2746_Click;
            btnRetornarParaValidada2745.Click += btnRetornarParaValidada2745_Click;
            lnkDeletarAnexo.Click += new EventHandler(lnkDeletarAnexo_Click);
            lnkBaixarAnexo.Click += new EventHandler(lnkBaixarAnexo_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    this.Anexo = null;
                    CarregarControles();
                }
                catch (Exception ex)
                {

                    HandleException(ex);
                }
            }

        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "RetornarParaCadastro":
                    if (e.Result == MessageBoxResult.Yes)
                        AlterarStatusMeta((int)StatusMetaEnum.EmCadastro);
                    break;
                case "EnviarMeta":
                    if (e.Result == MessageBoxResult.Yes)
                        AlterarStatusMeta((int)StatusMetaEnum.EmValidacao);
                    break;
                case "RejeitarMeta":
                    if (e.Result == MessageBoxResult.Yes)
                        AlterarStatusMeta((int)StatusMetaEnum.EmCadastro);
                    break;
                case "AprovarMeta":
                    if (e.Result == MessageBoxResult.Yes)
                        AlterarStatusMeta((int)StatusMetaEnum.Validada);
                    break;
                case "CancelarMeta":
                    if (e.Result == MessageBoxResult.Yes)
                        AlterarStatusMeta((int)StatusMetaEnum.Cancelada);
                    break;
                case "RetornarParaValidada":
                    if (e.Result == MessageBoxResult.Yes)
                        AlterarStatusMeta((int)StatusMetaEnum.Validada);
                    break;
                case "Voltar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                        Voltar();
                    break;
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Anexo = null;
            Voltar();
        }

        protected void btnRetornarParaCadastro2744_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Retornar Para Cadastro",
                "Deseja retornar a meta para cadastro?", MessageBoxButtons.YesNo, "RetornarParaCadastro");
        }

        protected void btnEnviarMeta2743_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Enviar Meta",
               "Confirma o envio para validação do gestor? Após esta operação, só será possível fazer ajustes se o gestor rejeitar o cadastro da meta.", MessageBoxButtons.YesNo, "EnviarMeta");
        }

        protected void btnRejeitarMeta2742_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Rejeitar Meta",
               "Confirma o retorno para ajustes do cadastro desta meta?", MessageBoxButtons.YesNo, "RejeitarMeta");
        }

        protected void btnAprovarMeta2741_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Aprovar Meta",
               "Confirma a aprovação do cadastro desta meta? Após esta operação, não será mais possível qualquer ajuste de dados.", MessageBoxButtons.YesNo, "AprovarMeta");
        }
        protected void btnRetornarParaValidada2745_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Retornar para Validada",
              "Deseja retornar essa meta para o status de Validada?", MessageBoxButtons.YesNo, "RetornarParaValidada");
        }
        protected void btnCancelar2746_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Cancelar Meta",
              "Confirma o cancelamento desta meta?", MessageBoxButtons.YesNo, "CancelarMeta");
        }

        protected void btnAnexar_Click(object sender, EventArgs e)
        {
            this.Anexo = null;

            if (flpAnexo.HasFile)
            {
                var anexo = new DTOAnexo();

                anexo.DescricaoAnexo = flpAnexo.FileName;
                anexo.Conteudo = flpAnexo.FileBytes;
                anexo.CaminhoAnexo = System.Configuration.ConfigurationManager.AppSettings["diretorioAnexosMeta"];

                this.Anexo = anexo;

                lblAnexo.Text = this.Anexo.DescricaoAnexo;
                lnkBaixarAnexo.Visible = lnkDeletarAnexo.Visible = true;

                updPanelAnexo.Visible =
                btnAnexar.Visible = false;
            }
        }

        protected void lnkBaixarAnexo_Click(object sender, EventArgs e)
        {
            var lnk = (LinkButton)sender;

            WebHelper.DownloadFile(System.IO.File.ReadAllBytes(lnk.Attributes["CaminhoAnexo"]), lblAnexo.Text);
        }

        protected void lnkDeletarAnexo_Click(object sender, EventArgs e)
        {
            this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarAnexoMeta(CodigoSeqMetaParameter);

            this.Anexo = null;

            lnkBaixarAnexo.Visible = lnkDeletarAnexo.Visible = false;
            updPanelAnexo.Visible = btnAnexar.Visible = true;

            MessageBox.ShowInformationMessage("Anexo excluído com sucesso!");
        }

        #endregion

        #region [Methods]

        private void CarregarControles()
        {
            if (CodigoSeqMetaParameter == 0)
                WebHelper.Redirect("~/Site/Meta/Consultar.aspx");

            DTOMetaDetalhada meta = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterMetaDetalhada(CodigoSeqMetaParameter);

            if (meta == null)
                WebHelper.Redirect("~/Site/Meta/Consultar.aspx");

            txtArea.Text = meta.DescricaoUnidade;
            txtDescricaoMeta.Text = meta.DescricaoMeta;
            txtDescricaoMetaInterna.Text = meta.StatusMetaInterna == "True" ? "Sim" : "Não";
            txtDescricaoJustificativa.Text = meta.DescricaoJustificativa;

            if (meta.IniciativaPe != null && meta.IniciativaPe.CodigoSeqIniciativa > 0)
            {
                txtAlinhamentoPE.Text = meta.IniciativaPe.DescricaoIniciativa;
                CarregarObjetivos(meta.IniciativaPe.CodigoSeqIniciativa, ref rptObjetivosPe);
            }
            else
            {
                divAlinhamentoPE.Visible = false;
                divObjetivoEstrategicoPe.Visible = false;
            }

            if (meta.IniciativaPpa != null && meta.IniciativaPpa.CodigoSeqIniciativa > 0)
            {
                txtAlinhamentoPPA.Text = meta.IniciativaPpa.DescricaoIniciativa;
                CarregarObjetivos(meta.IniciativaPpa.CodigoSeqIniciativa, ref rptObjetivosPpa);
            }
            else
            {
                divAlinhamentoPPA.Visible = false;
                divObjetivoEstrategicoPpa.Visible = false;
            }

            if (meta.IniciativaMissao != null && meta.IniciativaMissao.CodigoSeqIniciativa > 0)
                txtAlinhamentoMissaoInstitucional.Text = meta.IniciativaMissao.DescricaoIniciativa;
            else
                divAlinhamentoMissaoInstitucional.Visible = false;

            divGestaoDeRiscos.Visible = meta.GestaoDeRiscos != null;
            if (meta.GestaoDeRiscos != null)
                txtGestaoDeRiscos.Text = meta.GestaoDeRiscos.DescricaoInstrumento;

            divAgendaRegulatoria.Visible = meta.AgendaRegulatoria != null;
            if (meta.AgendaRegulatoria != null)
                txtAgendaRegulatoria.Text = meta.AgendaRegulatoria.DescricaoInstrumento;

            divDesburocratizacao.Visible = meta.Desburocratizacao != null;
            if (meta.Desburocratizacao != null)
                txtDesburocratizacao.Text = meta.Desburocratizacao.DescricaoInstrumento;

            divIntegridade.Visible = meta.Integridade != null;
            if (meta.Integridade != null)
                txtIntegridade.Text = meta.Integridade.DescricaoInstrumento;

            lblNenhumInstrumentoSelecionado.Visible = !divGestaoDeRiscos.Visible && !divAgendaRegulatoria.Visible && !divDesburocratizacao.Visible && !divIntegridade.Visible;

            txtTipoMeta.Text = meta.TipoMeta.DescricaoTipoMeta;
            txtResultadosEsperados.Text = meta.DescricaoResultadoEsperado;
            txtPremissas.Text = meta.DescricaoPremissa;
            txtCoordenacao.Text = meta.NoCoordenacao;
            txtRestricoes.Text = meta.DescricaoRestricao;

            //sociedade e usuário
            divSociedadeUsuarioBeneficio.Visible = !String.IsNullOrEmpty(meta.DescricaoSociedadeUsuarioBeneficio);
            if (divSociedadeUsuarioBeneficio.Visible)
                txtSociedadeUsuarioBeneficio.Text = meta.DescricaoSociedadeUsuarioBeneficio;

            divSociedadeUsuarioImpacto.Visible = !String.IsNullOrEmpty(meta.DescricaoSociedadeUsuarioImpacto);
            if (divSociedadeUsuarioImpacto.Visible)
                txtSociedadeUsuarioImpacto.Text = meta.DescricaoSociedadeUsuarioImpacto;

            divSociedadeUsuario.Visible = divSociedadeUsuarioBeneficio.Visible || divSociedadeUsuarioImpacto.Visible;

            //institucional
            divInstitucionalBeneficio.Visible = !String.IsNullOrEmpty(meta.DescricaoInstitucionalBeneficio);
            if (divInstitucionalBeneficio.Visible)
                txtInstitucionalBeneficio.Text = meta.DescricaoInstitucionalBeneficio;

            divInstitucionalImpacto.Visible = !String.IsNullOrEmpty(meta.DescricaoInstitucionalImpacto);
            if (divInstitucionalImpacto.Visible)
                txtInstitucionalImpacto.Text = meta.DescricaoInstitucionalImpacto;

            divInstitucional.Visible = divInstitucionalBeneficio.Visible || divInstitucionalImpacto.Visible;

            //governo
            divGovernoBeneficio.Visible = !String.IsNullOrEmpty(meta.DescricaoGovernoBeneficio);
            if (divGovernoBeneficio.Visible)
                txtGovernoBeneficio.Text = meta.DescricaoGovernoBeneficio;

            divGovernoImpacto.Visible = !String.IsNullOrEmpty(meta.DescricaoGovernoImpacto);
            if (divGovernoImpacto.Visible)
                txtGovernoImpacto.Text = meta.DescricaoGovernoImpacto;

            divGoverno.Visible = divGovernoBeneficio.Visible || divGovernoImpacto.Visible;

            //setor regulado
            divSetorReguladoBeneficio.Visible = !String.IsNullOrEmpty(meta.DescricaoSetorReguladoBeneficio);
            if (divSetorReguladoBeneficio.Visible)
                txtSetorReguladoBeneficio.Text = meta.DescricaoSetorReguladoBeneficio;

            divSetorReguladoImpacto.Visible = !String.IsNullOrEmpty(meta.DescricaoSetorReguladoImpacto);
            if (divSetorReguladoImpacto.Visible)
                txtSetorReguladoImpacto.Text = meta.DescricaoSetorReguladoImpacto;

            divSetorRegulado.Visible = divSetorReguladoBeneficio.Visible || divSetorReguladoImpacto.Visible;

            if (meta.Indicadores != null)
            {
                rptIndicadores.DataSource = meta.Indicadores;
                rptIndicadores.DataBind();
            }

            if (meta.Acoes != null)
            {
                rptAcoes.DataSource = meta.Acoes.OrderBy(i => i.DataInicioAcao);
                rptAcoes.DataBind();
            }

            if (UsuarioAdministrador && (meta.StatusMeta.CodigoSeqStatusMeta == (int)PGA.Common.StatusMetaEnum.Cancelada) || (meta.StatusMeta.CodigoSeqStatusMeta == (int)PGA.Common.StatusMetaEnum.Validada))
            {
                divAnexo.Visible = true;

                txtJustificativaCancelamento.Text = meta.SituacaoMeta.DescricaoJustificativaCancelamento;

                if (meta.SituacaoMeta.Anexo != null)
                {
                    lblAnexo.Text = meta.SituacaoMeta.Anexo.DescricaoAnexo;
                    lnkBaixarAnexo.Attributes["CaminhoAnexo"] = meta.SituacaoMeta.Anexo.CaminhoAnexo;
                    lnkBaixarAnexo.Visible = lnkDeletarAnexo.Visible = true;
                    updPanelAnexo.Visible =
                    btnAnexar.Visible = false;
                }
                else
                {
                    lnkBaixarAnexo.Visible = lnkDeletarAnexo.Visible = false;
                    updPanelAnexo.Visible =
                    btnAnexar.Visible = true;
                }

                if (meta.StatusMeta.CodigoSeqStatusMeta == (int)PGA.Common.StatusMetaEnum.Cancelada)
                {
                    if (meta.SituacaoMeta.Anexo == null)
                    {
                        lnkBaixarAnexo.Visible = true;
                        lnkBaixarAnexo.Enabled = false;
                        lblAnexo.Text = "Sem anexo";
                    }

                    txtJustificativaCancelamento.Enabled = lnkDeletarAnexo.Enabled = false;
                    updPanelAnexo.Visible = btnAnexar.Visible = false;
                }
            }

            bool exercicioFechado = meta.Exercicio.Fase.CodigoSeqFase == (int)FaseEnum.PreEncerramento || meta.Exercicio.Fase.CodigoSeqFase == (int)FaseEnum.Encerrado;

            btnEnviarMeta2743.Visible = meta.StatusMeta.CodigoSeqStatusMeta == (int)StatusMetaEnum.EmCadastro && (!exercicioFechado || (exercicioFechado && UsuarioAdministrador));
            btnAprovarMeta2741.Visible = meta.StatusMeta.CodigoSeqStatusMeta == (int)StatusMetaEnum.EmValidacao && (!exercicioFechado || (exercicioFechado && UsuarioAdministrador));
            btnRejeitarMeta2742.Visible = meta.StatusMeta.CodigoSeqStatusMeta == (int)StatusMetaEnum.EmValidacao && (!exercicioFechado || (exercicioFechado && UsuarioAdministrador));
            btnRetornarParaValidada2745.Visible = meta.StatusMeta.CodigoSeqStatusMeta == (int)StatusMetaEnum.Cancelada && (!exercicioFechado || (exercicioFechado && UsuarioAdministrador));
            btnCancelar2746.Visible = meta.StatusMeta.CodigoSeqStatusMeta == (int)StatusMetaEnum.Validada && (!exercicioFechado || (exercicioFechado && UsuarioAdministrador));
            btnRetornarParaCadastro2744.Visible = meta.StatusMeta.CodigoSeqStatusMeta == (int)StatusMetaEnum.Validada && (!exercicioFechado || (exercicioFechado && UsuarioAdministrador));
        }

        private void CarregarObjetivos(int CodigoSeqIniciativa, ref Repeater rptObjetivos)
        {
            try
            {
                var objetivos = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarObjetivosPorIniciativa(CodigoSeqIniciativa, 0, int.MaxValue, "", true);

                rptObjetivos.DataSource = objetivos;
                rptObjetivos.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public void Voltar()
        {
            WebHelper.Redirect(String.Format("~/Site/Meta/Consultar.aspx?AnoExercicio={0}&Unidade={1}&PageIndex={2}", AnoExercicio, Unidade, PageIndex));
        }

        public void AlterarStatusMeta(int codigoStatus)
        {
            this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").AlterarStatusMeta(CodigoSeqMetaParameter, codigoStatus, SCAApplicationContext.Usuario.Nome, txtJustificativaCancelamento.Text, this.Anexo);

            MessageBox.ShowInformationMessage("Registro salvo com sucesso.", "Voltar");
        }

        #endregion
    }
}