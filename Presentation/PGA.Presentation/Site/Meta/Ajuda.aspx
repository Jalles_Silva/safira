﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Ajuda.aspx.cs" Inherits="PGA.Presentation.Site.Meta.Ajuda" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <a href="#MetaConsulta"></a>
            <div class="row" id="MetaConsulta">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Ajuda - Lista de Metas</strong></h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Nesta tela devem ser descritas as metas a serem atingidas para que a unidade alcance os objetivos traçados para 2017.
                        </p>
                        <p>
                            Cada meta deve ser, sempre que possível: quantificável, desafiadora, gerenciável, exequível, desdobrada e voltada para o usuário final.
                        </p>
                        <p>
                            Para o estabelecimento das metas deve-se ter como referência os objetivos do Mapa Estratégico da ANTT, os macro indicadores estratégicos e as iniciativas estratégicas.
                        </p>
                        <p>
                            As metas devem estar relacionadas aos processos finalísticos e/ou de gestão da ANTT
                        </p>
                        <p>
                            Para incluir uma nova META, clique no botão <strong>&quot;Novo&quot;</strong>.
                        </p>
                        <p>
                            As metas já cadastradas aparecem na tabela e podem ser alteradas ou excluídas.
                        </p>
                        <p>
                            <strong>Atenção:</strong> ao excluir uma meta, as ações e os indicadores cadastrados para esta meta também são excluídos e esta ação não tem como ser desfeita!
                        </p>
                        <p>
                            Para cada meta devem ser cadastrados indicadores e as atividades do plano de ação. Para isto, clique no botão <strong>&quot;+&quot;</strong> ao lado de cada meta.
                        </p>
                        <p>
                            O botão <strong>&quot+&quot</strong> aparecerá em <strong>vermelho</strong> quando não existirem indicadores e/ou atividades cadastradas.
                        </p>
                        <p>
                            O botão <strong>&quot+&quot</strong> aparecerá em <strong>verde</strong> quando não existirem indicadores e/ou atividades cadastradas.
                        </p>
                    </div>
                </div>
            </div>
            <a href="#MetaEdicao"></a>
            <div class="row" id="MetaEdicao">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Ajuda - Edição/Inclusão de Metas</strong></h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            <strong>DESCRIÇÃO DA META:</strong> descreva cada uma das metas a serem atingidas para que a unidade alcance o objetivo traçado, devendo, sempre que possível, ser quantificada, desafiadora, gerenciável, exequível, desdobrada e voltada para o usuário final. Para o estabelecimento das metas deve-se ter como referência os objetivos do Mapa Estratégico da ANTT, seus macro indicadores estratégicos e suas iniciativas estratégicas. Essas metas devem estar relacionadas aos processos finalísticos e/ou de gestão da ANTT.<br />
                            <br />
                            São <strong>exemplos</strong> de descrição de meta:<br />
                            a) Aumentar em 30% o número de fiscalizações da ANTT no ano de 2017, sendo 15% (1500) de fiscalizações de cargas e 15% (1500) de fiscalizações de passageiros.
        <br />
                            b) Elevar de 80% para 100% a satisfação dos cidadãos usuários dos serviços da ANTT, até dezembro de 2017.
        <br />
                            c) Aumentar o percentual de cumprimento da Agenda Regulatória de 35% para 100%, até dezembro de 2017.
                        </p>
                        <p>
                            <strong>ALINHAMENTO:</strong> toda meta deve estar alinhada ao PE, ao PPA, e à Missão Institucional.
                        </p>
                        <p>
                            <strong>ALINHAMENTO PE:</strong> caso esta opção seja selecionada, deve-se escolher uma das iniciativas estratégicas/objetivos estratégicos do PE (Planejamento Estratégico).
                        </p>
                        <p>
                            <strong>ALINHAMENTO PPA:</strong> caso esta opção seja selecionada, deve-se escolher um dos objetivos do PPA (Planejamento Plurianual).
                        </p>
                         <p>
                            <strong>ALINHAMENTO Missão Institucional:</strong> esta opção será selecionada quando não for identificado alinhamento com os objetivos/iniciativas e/ou PPA, mas atende a requisitos da missão institucional.
                        </p>
                        <p>
                            <strong>TIPO DA META:</strong> neste campo deverá ser selecionada a opção a partir do foco da meta descrita.<br />
                            <strong>Exemplos:</strong>
                            <br />
                            a) &quot;De Fiscalização&quot;:<br />
                            Aumentar em 30% o número de fiscalizações da ANTT no ano de 2017, sendo 15% de fiscalizações de cargas e 15% de fiscalizações de passageiros.<br />
                            <br />
                            b) &quot;De Regulação&quot;:<br />
                            Aumentar o percentual de cumprimento da Agenda Regulatória de 35% para 100%, até dezembro de 2017.<br />
                            <br />
                            c) &quot;Administrativa&quot;<br />
                            Renovar 50% (1000) do parque tecnológico da ANTT, por meio da aquisição de 500 computadores, até dezembro de 2017.
                            <br />
                        </p>
                        <p>
                            <strong>RESULTADOS ESPERADOS:</strong> neste campo deverão ser elencados os resultados que se espera alcançar com a execução da meta.
                            <br />
                            <strong>Exemplo:</strong><br />
                            Meta: Desdobrar o Mapa Estratégico em 100% das unidades da ANTT, até dezembro de 2017<br />
                            Resultados Esperados:<br />
                            - Efetividade no cumprimento dos prazos de execução;
                            <br />
                            - Alinhamento do foco de atuação;<br />
                            - Impacto positivo na imagem do órgão;<br />
                            - Aumento da satisfação do ente regulado;
                            <br />
                            - Elevação do desempenho global da ANTT.
                        </p>
                        <p>
                            <strong>PREMISSAS OU RESTRIÇÕES:</strong> Premissas são hipóteses ou pressupostos ligados ao projeto, meta ou ação, assumidos como verdadeiros, sem necessidade de comprovação. Restrições são fatores internos ou externos que podem comprometer o projeto, ação ou meta e podem dificultar o seu gerenciamento.
                        </p>
                        <p>
                            <strong>COORDENAÇÃO:</strong> Neste campo deverá ser inserido o nome do gestor e/ou responsável pela meta.
                        </p>
                    </div>
                </div>
            </div>
            <a href="#AcaoConsulta"></a>
            <div class="row" id="AcaoConsulta">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Ajuda - Plano de Ação</strong></h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Esta é a lista de atividades, inspiradas na metodologia 5w2h, que serão desenvolvidas para o atingimento da meta.
                        </p>
                        <p>
                            Para incluir uma ação, clique no botão <strong>&#39;Novo&#39;</strong>.
                        </p>
                        <p>
                            Para excluir ou alterar, escolha uma das opções ao lado de cada meta.
                        </p>
                    </div>
                </div>
            </div>
            <a href="#AcaoEdicao"></a>
            <div class="row" id="AcaoEdicao">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Ajuda - Inclusão/Edição de atividades</strong></h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            <strong>ATIVIDADE:</strong> O que será feito.
                        </p>
                        <p>
                            <strong>ESTRATÉGIA:</strong> Como será feito.
                        </p>
                        <p>
                            <strong>RESPONSÁVEL:</strong> Quem (ator ou atores) irá viabilizar a atividade.
                        </p>
                        <p>
                            <strong>PESO:</strong> Um valor de um a cinco que representa a importância (a seu critério: valor, prazo ou esforço) de cada atividade em relação às demais. Este peso será utilizado para o cálculo do percentual geral de execução do plano de ação da meta.
                        </p>
                        <p>
                            <strong>Exemplo:</strong> Um plano ação tem três atividades, sendo que as duas primeiras estão concluídas (100%) e a terceira não (0%) -> Se todas tiverem o mesmo peso, o percentual de execução do plano de ação da meta é 66,66%. No entanto, se as duas primeiras têm peso 1 e a terceira peso 2, o percentual de execução do plano de ação da meta será de 50%.
                        </p>
                        <p>
                            <strong>DATA INÍCIO</strong> e <strong>DATA FIM:</strong> Quando será iniciada e concluída a atividade.
                        </p>
                        <p>
                            <strong>RECURSOS:</strong> Insumos Logísticos, Tecnológicos, Humanos ou Financeiros necessários à execução da atividade.
                        </p>
                    </div>
                </div>
            </div>
            <a href="#IndicadorConsulta"></a>
            <div class="row" id="IndicadorConsulta">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Ajuda - Lista de Indicadores de Contribuição</strong></h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Esta tela apresenta os Indicadores de Contribuição da Meta.
                        </p>
                        <p>
                            Para incluir um indicador, clique no botão <strong>&quot;Novo&quot;</strong>.
                        </p>
                        <p>
                            Para <strong>alterar</strong> ou <strong>excluir</strong> um indicador já cadastrado, escolha uma das opções ao lado de cada indicador.
                        </p>
                    </div>
                </div>
            </div>
            <a href="#IndicadorEdicao"></a>
            <div class="row"id="IndicadorEdicao">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Ajuda - Inclusão/Edição de Indicadores de Contribuição</strong></h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            Um&nbsp; Indicador de Contribuição tem por objetivo medir o desempenho da meta, bem como identificar sua contribuição ao alcance dos objetivos estratégicos projetados no Mapa da ANTT (BSC).
                        </p>
                        <p>
                            <strong>INDICADOR:</strong> neste campo deverá ser inserido o nome do indicador de contribuição da meta.
                        </p>
                        <p>
                            <strong>DESCRIÇÂO:</strong> neste campo deverá ser detalhado o que este indicador mede, reflete ou avalia e as principais formas de utilização dos dados.<br />
                            Exemplos:
                             <br />
                            a) Este indicador mede o percentual das fiscalizações de cargas rodoviárias realizadas pela ANTT em relação ao total de fiscalizações de cargas rodoviárias planejadas.
            <br />
                            b) O indicador mede o grau de atendimento as competências, em termos de proficiência e dimensionamento, identificadas como necessárias para cada função mapeada das áreas.
                        </p>
                        <p>
                            <strong>LINHA DE BASE e MÊS DE REFERÊNCIA:</strong> aqui deverá ser informado o valor do indicador e o mês de referência, para se saber o referencial de partida da medição e a evolução deste ao longo do tempo. Pode ser número ou percentual.
            <br />
                            Exemplo:
            <br />
                            a) 10.000 e 12/2016 (que seria, por exemplo, o número de fiscalizações realizadas pela ANTT em 2016, apurado em 12/2016, a ser utilizado como base para este indicador de 2017).<br />
                            b) 15% e 01/2017 (que seria, por exemplo, o número de rodovias sem pavimentação asfáltica em 01/2017 a ser utilizado como linha de base para este indicador de contribuição da meta.
                        </p>
                        <p>
                            <strong>VALOR ALVO e DATA ALVO:</strong> aqui deverá ser informado o valor para o indicador que pretende atingir e quando se pretende atingir este valor. Deve seguir o mesmo formato da linha de base e é muito importante que seja coerente com a descrição textual da meta.
            <br />
                            Exemplo:
            <br />
                            a) 12.000 e 12/2017 (supondo que sua meta seja aumentar em 20% até o final de 2017 o número de fiscalizações realizadas pela ANTT).<br />
                            b) 7,5% e 09/2017 (comparando com o exemplo 'b' da linha de base, estes seriam os valores caso sua meta fosse, por exemplo, 'reduzir em 50% o percentual de rodovias sem pavimentação asfáltica até setembro de 2017).
                        </p>
                        <p>
                            <strong>FÓRMULA:</strong> neste campo deverá ser inserido o método de cálculo do indicador, com definição precisa dos elementos que o compõem, numerador e denominador.
            <br />
                            Exemplos:<br />
                            a)&nbsp; (&#39;Quantitativo de Competências Existentes&#39; vezes o &#39;Grau de Proficiência da Competência&#39;) dividido por (&#39;Quantitativo de Competências Necessárias&#39; vezes o &#39;Grau de Proficiência da Competência&#39;)<br />
                            b) (&#39;Número de Servidores com Titulação&#39; vezes 100) dividido por &#39;Número Total de Servidores&#39;
                        </p>
                        <p>
                            <strong>TIPOS DE INDICADORES:</strong>
                        </p>
                        <p>
                            Indicadores de <strong>QUALIDADE (EFICÁCIA):</strong> Referem-se à capacidade da organização de satisfazer as necessidades das partes interessadas e tem foco nos resultados. Devem representar os atributos relativos aos produtos e serviços prestados pela organização e que são relevantes para os cidadãos/clientes, sociedade, servidores/funcionários, governo e outros. Também entendidos como o grau de alcance das metas programadas, em um determinado período de tempo, independentemente da utilização dos recursos empregados. Este conceito pode ser aplicado a diferentes contextos, a depender do que queremos medir. É fundamental saber quais são os requisitos do cidadão/cliente, principalmente em relação aos impactos dos resultados.<br />
                            <strong>Exemplo:</strong>
            <br />
                            Resultado: as pessoas sentem-se razoavelmente seguras e, portanto, relativamente satisfeitas ao perceberem a presença de policiais pelas ruas (a meta anunciada pelo governante foi de colocar nas ruas 90% do efetivo de policiais em função burocrática nos quartéis).<br />
                            Impacto: mais satisfeitas e seguras irão sentir-se ao perceberem que os índices de violência se reduziram sensivelmente.
                        </p>
                        <p>
                            Indicadores de <strong>EFICIÊNCIA (PRODUTIVIDADE):</strong> Referem-se à utilização dos recursos disponíveis para a produção dos produtos ou execução dos serviços da organização; é o meio de fazer certo um processo correto de boa qualidade, em curto prazo, com o menor número de erros e otimizando os recursos.<br />
                            <strong>Exemplo:</strong>
            <br />
                            Meta: Reduzir em 20% o custo da cesta básica, até dez 2015.
            <br />
                            Indicador: Percentual de redução de custo (Recurso: Financeiro)
            <br />
                            Fórmula: (&#39;Custo do ano anterior&#39; menos &#39;custo do ano corrente&#39;) dividido por &#39;Custo do ano anterior&#39;)
                        </p>
                        <p>
                            Distinção entre Eficiência e Eficácia:
                        </p>
                        <table width="90%" border="1px">
                            <tr>
                                <th>INDICADOR</th>
                                <th>EFICIÊNCIA</th>
                                <th>EFICÁCIA</th>
                            </tr>
                            <tr>
                                <td>Dizem respeito</td>
                                <td>ao modo de utilizar os recursos disponíveis</td>
                                <td>à satisfação dos cidadãos/clientes</td>
                            </tr>
                               <tr>
                                <td>Medem</td>
                                <td>a eficiência dos processos</td>
                                <td>a eficácia e o impacto dos processos</td>
                            </tr>
                               <tr>
                                <td>Tem foco</td>
                                <td>no esforço</td>
                                <td>nos resultados</td>
                            </tr>
                               <tr>
                                <td>Indicam</td>
                                <td>como fazer</td>
                                <td>o que fazer</td>
                            </tr>
                               <tr>
                                <td>Ensinam</td>
                                <td>fazer certo as coisas</td>
                                <td>fazer as coisas certas</td>
                            </tr>
                        </table>
                        <p>
                            &nbsp;
                        </p>
                        <p>
                            Indicadores de <strong>CRESCIMENTO E INOVAÇÃO:</strong> Medem quão bem o processo utiliza seus recursos de forma a fazer a Organização crescer e disponibilizar novos serviços aos cidadãos/clientes.
                        </p>
                        <p>
                            Indicadores de <strong>SATISFAÇÃO:</strong> Quantificam a percepção dos cidadãos/clientes em relação ao produto e/ou serviço fornecido/disponibilizado pelo processo.
                        </p>
                        <p>
                            Indicadores de <strong>CONFORMIDADE:</strong> Quantificam a conformidade do serviço em relação a limites de aceitação estabelecidos para seus requisitos.
                        </p>
                        <p>
                            Indicadores de <strong>ECONOMICIDADE:</strong> Dizem respeito à minimização dos custos de uma atividade, sem comprometimento dos padrões de qualidade. Referem-se à capacidade de uma instituição de gerir adequadamente os recursos financeiros à sua disposição. De acordo com o Tribunal de Contas da União, estes indicadores permitem também medir o custo dos insumos e os recursos alocados para a atividade.
                        </p>
                        <p>
                            Indicadores de <strong>EFETIVIDADE:</strong> Definem a capacidade de se transformar uma realidade (impacto) a partir do objetivo estabelecido e sua continuidade ao longo do tempo.
                        </p>
                        <p>
                            Indicadores de <strong>RESULTADOS:</strong> Utilizados para avaliar se a meta foi alcançada ou não e, portanto, são medidos ao final do prazo estabelecido para a meta.
                        </p>
                        <p>
                            Indicadores de <strong>ALERTA:</strong> Utilizados durante o processo de monitoramento da meta para avaliar a evolução dos resultados obtidos e orientar o redirecionamento das ações. Algumas vezes os indicadores de resultado e de alerta são os mesmos, o que varia é a periodicidade de medição.
                            <br />
                            <strong>Exemplo:</strong>
                            <br />
                            Meta: atingir 95% de alunos aprovados por ano, até 2015.
                            <br />
                            Indicador de resultado: índice de alunos aprovados por ano.
                            <br />
                            Indicador de alerta: percentual de alunos com notas abaixo da média no bimestre.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

</asp:Content>
