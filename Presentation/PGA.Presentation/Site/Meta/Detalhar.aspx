﻿<%@ Page Title="SAFIRA - Meta" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false"
    CodeBehind="Detalhar.aspx.cs" Inherits="PGA.Presentation.Site.Meta.Detalhar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnSalvar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Meta</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="drpArea" Text="Área*" runat="server" />
                                    <asp:LinkButton ID="lnkAjudaArea" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaArea" ForeColor="#034623" />
                                    <asp:DropDownList ID="drpArea" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvArea" runat="server" Display="None" ControlToValidate="drpArea" ErrorMessage="Favor informar a Área." ValidationGroup="Salvar" />
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblAnoExercicio" AssociatedControlID="lblAnoExercicio" Text="Exercício*" runat="server" />
                                    <asp:LinkButton ID="lnkAjudaExercicio" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaExercicio" ForeColor="#034623" />
                                    <asp:DropDownList ID="ddlAnoExercicio" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvAnoExercicio" runat="server" Display="None" ControlToValidate="ddlAnoExercicio" ErrorMessage="Favor informar o Exercício." ValidationGroup="Salvar" />
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <br/>
                                    <br/>
                                    <asp:CheckBox ID="chkMetaInterna" runat="server" Text="Constante no PGA" CssClass="checkListHoriz" AutoPostBack="true" Checked="true"/>
                                    <asp:LinkButton ID="lnkAjudaMetaInterna" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaMetaInterna" ForeColor="#034623" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoMeta" AssociatedControlID="txtDescricaoMeta" Text="Descrição*" runat="server" />
                                    <asp:LinkButton ID="lnkAjudaDescricao" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaDescricaoMeta" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDescricaoMeta" runat="server" CssClass="form-control" MaxLength="1000" TextMode="MultiLine" />
                                    <asp:RequiredFieldValidator ID="rfvDescricaoMeta" runat="server" Display="None" ControlToValidate="txtDescricaoMeta" ErrorMessage="Favor informar a Descrição." ValidationGroup="Salvar" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoJustificativa" AssociatedControlID="txtDescricaoJustificativa" Text="Justificativa*" runat="server" />
                                    <asp:LinkButton ID="lnkAjudaJustificativa" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaJustificativa" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDescricaoJustificativa" runat="server" CssClass="form-control" TextMode="MultiLine"/>
                                    <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate = "txtDescricaoJustificativa" ID="RegularExpressionValidator1" ValidationExpression = "^[\s\S]{0,4000}$" runat="server" ErrorMessage="Máximo de 4000 caracteres permitidos."></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="rfvDescricaoJustificativa" runat="server" Display="None" ControlToValidate="txtDescricaoJustificativa" ErrorMessage="Favor informar a Justificativa." ValidationGroup="Salvar" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3">
                                <div class="form-group">
                                    <asp:CheckBox ID="chkAlinhamentoPe" runat="server" Text="Alinhamento PE " OnCheckedChanged="chkAlinhamentoPe_CheckedChanged" AutoPostBack="true" CssClass="checkListHoriz" />
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaAlinhamentoPE" ForeColor="#034623" />
                                </div>
                            </div>
                            <div class="col-xs-9 col-sm-9">
                                <div class="form-group">
                                    <asp:DropDownList ID="drpAlinhamentoPE" runat="server" CssClass="form-control" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="drpAlinhamentoPE_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <asp:LinkButton ID="lnkObjetivoPE" ToolTip="Visualizar o(s) objetivo(s) estratégico(s) do Alinhamento PE selecionado" runat="server" data-toggle="modal" data-target="#modalObjetivosPE" Visible="false">
                                    <strong>Objetivo(s) Estratégico(s)</strong>   <i aria-hidden="true" class="glyphicon glyphicon-th-list" style="margin-bottom:10px;"></i>
                                </asp:LinkButton>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3">
                                <div class="form-group">
                                    <asp:CheckBox ID="chkAlinhamentoPpa" runat="server" Text="Alinhamento PPA " OnCheckedChanged="chkAlinhamentoPpa_CheckedChanged" AutoPostBack="true" CssClass="checkListHoriz" />
                                    <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaAlinhamentoPPA" ForeColor="#034623" />
                                </div>
                            </div>
                            <div class="col-xs-9 col-sm-9">
                                <div class="form-group">
                                    <asp:DropDownList ID="drpAlinhamentoPPA" runat="server" CssClass="form-control" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="drpAlinhamentoPPA_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <asp:LinkButton ID="lnkObjetivoPPA" ToolTip="Visualizar o(s) objetivo(s) estratégico(s) do Alinhamento PPA selecionado" runat="server" data-toggle="modal" data-target="#modalObjetivosPPA" Visible="false">
                                    <strong>Objetivo(s) Estratégico(s)</strong>   <i aria-hidden="true" class="glyphicon glyphicon-th-list" style="margin-bottom:10px;"></i>
                                </asp:LinkButton>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-3 col-sm-3">
                                <div class="form-group">
                                    <asp:CheckBox ID="chkAlinhamentoMissao" runat="server" Text="Alinhamento Missão Institucional" OnCheckedChanged="chkAlinhamentoMissao_CheckedChanged" AutoPostBack="true" CssClass="checkListHoriz" />
                                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaAlinhamentoMissaoInstitucional" ForeColor="#034623" />
                                </div>
                            </div>
                            <div class="col-xs-9 col-sm-9">
                                <div class="form-group">
                                    <asp:DropDownList ID="drpAlinhamentoMissao" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblInstrumentosMeta" AssociatedControlID="lnkAjudaInstrumentos" Text="Instrumentos" runat="server" />
                                    <asp:LinkButton ID="lnkAjudaInstrumentos" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaInstrumentosMeta" ForeColor="#034623" />
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-8 col-sm-8">
                            <div class="row">
                                <div class="col-xs-3 col-sm-3">
                                    <div class="form-group">
                                        <asp:CheckBox ID="chkGestaoDeRiscos" runat="server" Text="Gestão de Riscos " AutoPostBack="true" CssClass="checkListHoriz" />
                                    </div>
                                </div>
                                <div class="col-xs-9 col-sm-9">
                                    <div class="form-group">
                                        <asp:DropDownList ID="drpGestaoDeRiscos" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 col-sm-3">
                                    <div class="form-group">
                                        <asp:CheckBox ID="chkAgendaRegulatoria" runat="server" Text="Agenda Regulatória " AutoPostBack="true" CssClass="checkListHoriz" />
                                    </div>
                                </div>
                                <div class="col-xs-9 col-sm-9">
                                    <div class="form-group">
                                        <asp:DropDownList ID="drpAgendaRegulatoria" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 col-sm-3">
                                    <div class="form-group">
                                        <asp:CheckBox ID="chkDesburocratizacao" runat="server" Text="Desburocratização " AutoPostBack="true" CssClass="checkListHoriz" />
                                    </div>
                                </div>
                                <div class="col-xs-9 col-sm-9">
                                    <div class="form-group">
                                        <asp:DropDownList ID="drpDesburocratizacao" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-3 col-sm-3">
                                    <div class="form-group">
                                        <asp:CheckBox ID="chkIntegridade" runat="server" Text="Integridade " AutoPostBack="true" CssClass="checkListHoriz" />
                                    </div>
                                </div>
                                <div class="col-xs-9 col-sm-9">
                                    <div class="form-group">
                                        <asp:DropDownList ID="drpIntegridade" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4">

                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <asp:CheckBox ID="chkStatusIniciativaEstrategica" runat="server" Text="Iniciativa Estratégica" Enabled="false" CssClass="checkListHoriz"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-3 col-sm-3">
                                    <div class="form-group">
                                        <asp:CheckBox ID="chkStatusOutros" runat="server" Text="Outros " AutoPostBack="true" CssClass="checkListHoriz" />
                                    </div>
                                </div>
                                <div class="col-xs-9 col-sm-9">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtOutros" runat="server" CssClass="form-control" MaxLength="500" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoMeta" AssociatedControlID="ddlTipoMeta" Text="Tipo*" runat="server" />
                                    <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaTipoMeta" ForeColor="#034623" />
                                    <asp:DropDownList ID="ddlTipoMeta" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvTipoMeta" runat="server" Display="None" InitialValue="" ControlToValidate="ddlTipoMeta" ErrorMessage="Favor informar o Tipo." ValidationGroup="Salvar" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoResultadoEsperado" AssociatedControlID="txtDescricaoResultadoEsperado" Text="Resultados Esperados" runat="server" />
                                    <asp:LinkButton ID="LinkButton5" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaResultados" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDescricaoResultadoEsperado" runat="server" CssClass="form-control" MaxLength="4000" TextMode="MultiLine" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoPremissa" AssociatedControlID="txtDescricaoPremissa" Text="Premissas" runat="server" />
                                    <asp:LinkButton ID="LinkButton6" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaPremissas" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDescricaoPremissa" runat="server" CssClass="form-control" MaxLength="4000" TextMode="MultiLine" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="Label1" AssociatedControlID="txtDescricaoRestricao" Text="Restrições" runat="server" />
                                    <asp:LinkButton ID="LinkButton7" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaRestricoes" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDescricaoRestricao" runat="server" CssClass="form-control" MaxLength="4000" TextMode="MultiLine" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblNoCoordenacao" AssociatedControlID="txtNoCoordenacao" Text="Coordenação*" runat="server" />
                                    <asp:LinkButton ID="LinkButton8" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaCoordenacao" ForeColor="#034623" />
                                    <asp:TextBox ID="txtNoCoordenacao" runat="server" CssClass="form-control" MaxLength="500" />
                                    <asp:RequiredFieldValidator ID="rfvNoCoordenacao" runat="server" Display="None" ControlToValidate="txtNoCoordenacao" ErrorMessage="Favor informar a Coordenação." ValidationGroup="Salvar" />
                                </div>
                            </div>
                        </div>

                        <br />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title">Sociedade/Usuário</h5>
                            </div>
                            <div class="panel-body">
                                <div class="col-xs-6 col-sm-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblSociedadeUsuarioBeneficio" AssociatedControlID="txtSociedadeUsuarioBeneficio" Text="Benefício" runat="server" />
                                        <asp:LinkButton ID="lnkSociedadeUsuarioBeneficio" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaBeneficioMeta" ForeColor="#034623" />
                                        <asp:TextBox ID="txtSociedadeUsuarioBeneficio" runat="server" CssClass="form-control" MaxLength="2000" TextMode="MultiLine" />
                                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate = "txtSociedadeUsuarioBeneficio" ID="RegularExpressionValidator2" ValidationExpression = "^[\s\S]{0,2000}$" runat="server" ErrorMessage="Máximo de 2000 caracteres permitidos."></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblSociedadeUsuarioImpacto" AssociatedControlID="txtSociedadeUsuarioImpacto" Text="Impacto" runat="server" />
                                        <asp:LinkButton ID="lnkSociedadeUsuarioImpacto" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaImpactoMeta" ForeColor="#034623" />
                                        <asp:TextBox ID="txtSociedadeUsuarioImpacto" runat="server" CssClass="form-control" MaxLength="2000" TextMode="MultiLine" />
                                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate = "txtSociedadeUsuarioImpacto" ID="RegularExpressionValidator3" ValidationExpression = "^[\s\S]{0,2000}$" runat="server" ErrorMessage="Máximo de 2000 caracteres permitidos."></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Institucional</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-xs-6 col-sm-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblInstitucionalBeneficio" AssociatedControlID="txtInstitucionalBeneficio" Text="Benefício" runat="server" />
                                        <asp:LinkButton ID="lnkInstitucionalBeneficio" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaBeneficioMeta" ForeColor="#034623" />
                                        <asp:TextBox ID="txtInstitucionalBeneficio" runat="server" CssClass="form-control" MaxLength="2000" TextMode="MultiLine" />
                                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate = "txtInstitucionalBeneficio" ID="RegularExpressionValidator4" ValidationExpression = "^[\s\S]{0,2000}$" runat="server" ErrorMessage="Máximo de 2000 caracteres permitidos."></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblInstitucionalImpacto" AssociatedControlID="txtInstitucionalImpacto" Text="Impacto" runat="server" />
                                        <asp:LinkButton ID="lnkInstitucionalImpacto" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaImpactoMeta" ForeColor="#034623" />
                                        <asp:TextBox ID="txtInstitucionalImpacto" runat="server" CssClass="form-control" MaxLength="2000" TextMode="MultiLine" />
                                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate = "txtInstitucionalImpacto" ID="RegularExpressionValidator5" ValidationExpression = "^[\s\S]{0,2000}$" runat="server" ErrorMessage="Máximo de 2000 caracteres permitidos."></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Governo</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-xs-6 col-sm-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblGovernoBeneficio" AssociatedControlID="txtGovernoBeneficio" Text="Benefício" runat="server" />
                                        <asp:LinkButton ID="lnkGovernoBeneficio" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaBeneficioMeta" ForeColor="#034623" />
                                        <asp:TextBox ID="txtGovernoBeneficio" runat="server" CssClass="form-control" MaxLength="2000" TextMode="MultiLine" />
                                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate = "txtGovernoBeneficio" ID="RegularExpressionValidator6" ValidationExpression = "^[\s\S]{0,2000}$" runat="server" ErrorMessage="Máximo de 2000 caracteres permitidos."></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblGovernoImpacto" AssociatedControlID="txtGovernoImpacto" Text="Impacto" runat="server" />
                                        <asp:LinkButton ID="lnkGovernoImpacto" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaImpactoMeta" ForeColor="#034623" />
                                        <asp:TextBox ID="txtGovernoImpacto" runat="server" CssClass="form-control" MaxLength="2000" TextMode="MultiLine" />
                                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate = "txtGovernoImpacto" ID="RegularExpressionValidator7" ValidationExpression = "^[\s\S]{0,2000}$" runat="server" ErrorMessage="Máximo de 2000 caracteres permitidos."></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Setor Regulado</h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-xs-6 col-sm-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblSetorReguladoBeneficio" AssociatedControlID="txtSetorReguladoBeneficio" Text="Benefício" runat="server" />
                                        <asp:LinkButton ID="lnkSetorReguladoBeneficio" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaBeneficioMeta" ForeColor="#034623" />
                                        <asp:TextBox ID="txtSetorReguladoBeneficio" runat="server" CssClass="form-control" MaxLength="2000" TextMode="MultiLine" />
                                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate = "txtSetorReguladoBeneficio" ID="RegularExpressionValidator8" ValidationExpression = "^[\s\S]{0,2000}$" runat="server" ErrorMessage="Máximo de 2000 caracteres permitidos."></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6">
                                    <div class="form-group">
                                        <asp:Label ID="lblSetorReguladoImpacto" AssociatedControlID="txtSetorReguladoImpacto" Text="Impacto" runat="server" />
                                        <asp:LinkButton ID="lnkSetorReguladoImpacto" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaImpactoMeta" ForeColor="#034623" />
                                        <asp:TextBox ID="txtSetorReguladoImpacto" runat="server" CssClass="form-control" MaxLength="2000" TextMode="MultiLine" />
                                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate = "txtSetorReguladoImpacto" ID="RegularExpressionValidator9" ValidationExpression = "^[\s\S]{0,2000}$" runat="server" ErrorMessage="Máximo de 2000 caracteres permitidos."></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="updBotoes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnIndicador" runat="server" SkinID="btnIndicador" CausesValidation="false" Visible="false" />
                                        <asp:LinkButton ID="btnAtividade" runat="server" SkinID="btnAtividade" CausesValidation="false" Visible="false" />
                                        <asp:LinkButton ID="btnSalvar" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="Salvar" />
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaArea" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Área</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Área da meta.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaExercicio" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Exercício</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Corresponde ao ano em curso da Meta.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          <div id="modalAjudaMetaInterna" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Exercício</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Indica se a meta é interna ao PGA.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaDescricaoMeta" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Descrição da Meta</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Descreva cada uma das metas a serem atingidas para que a unidade alcance o objetivo traçado, devendo, sempre que possível, ser quantificada, desafiadora, gerenciável, exequível, desdobrada e voltada para o usuário final. Para o estabelecimento das metas deve-se ter como referência os objetivos do Mapa Estratégico da ANTT, seus macro indicadores estratégicos e suas iniciativas estratégicas. Essas metas devem estar relacionadas aos processos finalísticos e/ou de gestão da ANTT.
                                        <br />
                                        <br />
                                        São exemplos de descrição de meta:
                                        <br />
                                        a) Aumentar em 30% o número de fiscalizações da ANTT no ano de 2017, sendo 15% (1500) de fiscalizações de cargas e 15% (1500) de fiscalizações de passageiros. 
                                        <br />
                                        b) Elevar de 80% para 100% a satisfação dos cidadãos usuários dos serviços da ANTT, até dezembro de 2017. 
                                        <br />
                                        c) Aumentar o percentual de cumprimento da Agenda Regulatória de 35% para 100%, até dezembro de 2017.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaJustificativa" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Descrição da Justificativa</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Informe o problema ou a oportunidade (necessidade) que justifica as razões da ação ser realizada. Citar ações anteriores, se for o caso.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaAlinhamentoPE" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alinhamento PE</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Caso esta opção seja selecionada, deve-se escolher uma das iniciativas estratégicas/objetivos estratégicos do PE (Planejamento Estratégico).
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaAlinhamentoPPA" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alinhamento PPA</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Caso esta opção seja selecionada, deve-se escolher um dos objetivos do PPA (Planejamento Plurianual).
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaAlinhamentoMissaoInstitucional" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alinhamento Missão Institucional</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Esta opção será selecionada quando não for identificado alinhamento com os objetivos/iniciativas e/ou PPA, mas atende a requisitos da missão institucional.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaInstrumentosMeta" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Instrumentos</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Informe os intrumentos a serem utilizados para o cumprimento da meta.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaTipoMeta" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tipo Meta</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Neste campo deverá ser selecionada a opção a partir do foco da meta descrita.
                                        Exemplos: 
                                        a) "De Fiscalização":
                                        Aumentar em 30% o número de fiscalizações da ANTT no ano de 2017, sendo 15% de fiscalizações de cargas e 15% de fiscalizações de passageiros.

                                        b) "De Regulação":
                                        Aumentar o percentual de cumprimento da Agenda Regulatória de 35% para 100%, até dezembro de 2017.

                                        c) "Administrativa"
                                        Renovar 50% (1000) do parque tecnológico da ANTT, por meio da aquisição de 500 computadores, até dezembro de 2017. 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaResultados" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Resultados Esperados</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Neste campo deverão ser elencados os resultados que se espera alcançar com a execução da meta. 
                                        Exemplo: 
                                        Meta: Desdobrar o Mapa Estratégico em 100% das unidades da ANTT, até dezembro de 2017. 
                                        Resultados Esperados: 
                                        - Eficiência e eficácia no alcance dos resultados projetados; 
                                        - Alinhamento do foco de atuação; 
                                        - Impacto positivo na imagem do órgão; 
                                        - Aumento da satisfação do ente regulado; 
                                        - Elevação do desempenho global da ANTT.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaPremissas" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Premissas</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Premissas são hipóteses ou pressupostos ligados ao projeto, meta ou ação, assumidos como verdadeiros, sem necessidade de comprovação.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaRestricoes" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Restrições</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Restrições são fatores internos ou externos que podem comprometer o projeto, ação ou meta e podem dificultar o seu gerenciamento.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaCoordenacao" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Coordenação</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Neste campo deverá ser inserido o nome do gestor e/ou responsável pela meta.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaBeneficioMeta" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Benefício</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Significa o quanto a meta/ação propiciou de valor a sociedade, aos cidadãos – usuários e a organização.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaImpactoMeta" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Impacto</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Significa o quanto a meta/ação programada conseguiu transformar a realidade a partir do planejado. Pode ser positivo ou negativo.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalObjetivosPE" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Objetivo(s) Alinhamento PE</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="Objetivo(s) Estratégico(s): " runat="server" />
                                            
                                            <asp:Repeater ID="rptObjetivosPE" runat="server">
                                                <ItemTemplate>
                                                    <div style="margin:10px 0 15px 15px;">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-12">
                                                                <div>
                                                                    <asp:Label Text="OBJETIVO ESTRATÉGICO: " runat="server" />
                                                                    <asp:Label ID="lblObjetivoPe" Text='<%#Eval("Objetivo.nomeObjetivo")%>' runat="server" />
                                                                </div>
                                                                <div>
                                                                    <asp:Label Text="PERCENTUAL DE PARTICIPAÇÃO: " runat="server" />
                                                                    <asp:Label ID="lblParticipacaoPe" Text='<%#Eval("PercentualParticipacao")%>' runat="server" />%
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div style="margin:5px 0 5px 5px;">
                                                        <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:Repeater>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalObjetivosPPA" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Objetivo(s) Alinhamento PPA</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="Objetivo(s) Estratégico(s): " runat="server" />
                                            
                                            <asp:Repeater ID="rptObjetivosPPA" runat="server">
                                                <ItemTemplate>
                                                    <div style="margin:10px 0 15px 15px;">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-12">
                                                                <div>
                                                                    <asp:Label Text="OBJETIVO ESTRATÉGICO: " runat="server" />
                                                                    <asp:Label ID="lblObjetivoPpa" Text='<%#Eval("Objetivo.nomeObjetivo")%>' runat="server" />
                                                                </div>
                                                                <div>
                                                                    <asp:Label Text="PERCENTUAL DE PARTICIPAÇÃO: " runat="server" />
                                                                    <asp:Label ID="lblParticipacaoPpa" Text='<%#Eval("PercentualParticipacao")%>' runat="server" />%
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div style="margin:5px 0 5px 5px;">
                                                        <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:Repeater>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>





    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtDescricaoMeta.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 250) {
                    $("#<%= txtDescricaoMeta.ClientID %>").val(text.substring(0, 250));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtDescricaoMeta.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 250) {
                    $("#<%= txtDescricaoMeta.ClientID %>").val(text.substring(0, 250));
                    return false;
                }
                else {
                    return true;
                }
            });
        });
    </script>
</asp:Content>
