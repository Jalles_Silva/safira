﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false" CodeBehind="Visualizar.aspx.cs" Inherits="PGA.Presentation.Site.Meta.Visualizar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Consulta/Validação</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>Meta</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-4 col-sm-4">
                                        <div class="form-group">
                                            <asp:Label ID="lblArea" Text="ÁREA: " runat="server" />
                                            <asp:Label ID="txtArea" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblDescricaoMeta" Text="DESCRIÇÃO: " runat="server" />
                                            <asp:Label ID="txtDescricaoMeta" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblDescricaoMetaInterna" Text="Constante no PGA: " runat="server" />
                                            <asp:Label ID="txtDescricaoMetaInterna" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblDescricaoJustificativa" Text="JUSTIFICATIVA: " runat="server" />
                                            <asp:Label ID="txtDescricaoJustificativa" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div id="divAlinhamentoPE" class="row" runat="server">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblAlinhamentoPE" Text="ALINHAMENTO PE: " runat="server" />
                                            <asp:Label ID="txtAlinhamentoPE" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div id="divObjetivoEstrategicoPe" class="row" runat="server">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="OBJETIVO(S) ESTRATÉGICO(S): " runat="server" />

                                            <asp:Repeater ID="rptObjetivosPe" runat="server">
                                                <ItemTemplate>
                                                    <div style="margin: 10px 0 15px 15px;">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-12">
                                                                <div>
                                                                    <asp:Label Text="OBJETIVO ESTRATÉGICO: " runat="server" />
                                                                    <asp:Label ID="lblObjetivoPe" Text='<%#Eval("Objetivo.nomeObjetivo")%>' runat="server" />
                                                                </div>
                                                                <div>
                                                                    <asp:Label Text="PERCENTUAL DE PARTICIPAÇÃO: " runat="server" />
                                                                    <asp:Label ID="lblParticipacaoPe" Text='<%#Eval("PercentualParticipacao")%>' runat="server" />%
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div style="margin: 5px 0 5px 5px;">
                                                        <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:Repeater>

                                        </div>
                                    </div>
                                </div>
                                <div id="divAlinhamentoPPA" class="row" runat="server">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblAlinhamentoPPA" Text="ALINHAMENTO PPA: " runat="server" />
                                            <asp:Label ID="txtAlinhamentoPPA" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div id="divObjetivoEstrategicoPpa" class="row" runat="server">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="OBJETIVO(S) ESTRATÉGICO(S): " runat="server" />

                                            <asp:Repeater ID="rptObjetivosPpa" runat="server">
                                                <ItemTemplate>
                                                    <div style="margin: 10px 0 15px 15px;">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-12">
                                                                <div>
                                                                    <asp:Label Text="OBJETIVO ESTRATÉGICO: " runat="server" />
                                                                    <asp:Label ID="lblObjetivoPpa" Text='<%#Eval("Objetivo.nomeObjetivo")%>' runat="server" />
                                                                </div>
                                                                <div>
                                                                    <asp:Label Text="PERCENTUAL DE PARTICIPAÇÃO: " runat="server" />
                                                                    <asp:Label ID="lblParticipacaoPpa" Text='<%#Eval("PercentualParticipacao")%>' runat="server" />%
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div style="margin: 5px 0 5px 5px;">
                                                        <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:Repeater>

                                        </div>
                                    </div>
                                </div>

                                <div id="divIntrumentos" class="row" runat="server">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblInstrumentos" Text="INSTRUMENTOS: " runat="server" />
                                            <asp:Label ID="lblNenhumInstrumentoSelecionado" Text="Nenhum instrumento selecionado. " runat="server" />
                                        </div>
                                    </div>


                                    <div class="col-xs-10 col-md-offset-2">

                                        <div id="divGestaoDeRiscos" class="row" runat="server">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblGestaoDeRiscos" Text="GESTÃO DE RISCOS: " runat="server" />
                                                    <asp:Label ID="txtGestaoDeRiscos" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divAgendaRegulatoria" class="row" runat="server">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblAgendaRegulatoria" Text="AGENDA REGULATÓRIA: " runat="server" />
                                                    <asp:Label ID="txtAgendaRegulatoria" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divDesburocratizacao" class="row" runat="server">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblDesburocratizacao" Text="DESBUROCRATIZAÇÃO: " runat="server" />
                                                    <asp:Label ID="txtDesburocratizacao" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divIntegridade" class="row" runat="server">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblIntegridade" Text="INTEGRIDADE: " runat="server" />
                                                    <asp:Label ID="txtIntegridade" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="divAlinhamentoMissaoInstitucional" class="row" runat="server">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblAlinhamentoMissaoInstitucional" Text="ALINHAMENTO MISSÃO INSTITUCIONAL: " runat="server" />
                                            <asp:Label ID="txtAlinhamentoMissaoInstitucional" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblTipoMeta" Text="TIPO DA META: " runat="server" />
                                            <asp:Label ID="txtTipoMeta" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblResultadosEsperados" Text="RESULTADOS ESPERADOS: " runat="server" />
                                            <asp:Label ID="txtResultadosEsperados" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblPremissas" Text="PREMISSAS: " runat="server" />
                                            <asp:Label ID="txtPremissas" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblRestricoes" Text="RESTRIÇÕES: " runat="server" />
                                            <asp:Label ID="txtRestricoes" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblCoordenacao" Text="COORDENAÇÃO: " runat="server" />
                                            <asp:Label ID="txtCoordenacao" runat="server" />
                                        </div>
                                    </div>
                                </div>

                                <div id="divSociedadeUsuario" class="row" runat="server">
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group">
                                            <asp:Label ID="lblSociedadeUsuario" Text="SOCIEDADE/USUÁRIO: " runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-xs-10 col-sm-10">
                                        <div class="form-group">
                                            <div id="divSociedadeUsuarioBeneficio" class="row" runat="server">
                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="form-group">
                                                        <asp:Label ID="lblSociedadeUsuarioBeneficio" Text="BENEFÍCIO: " runat="server" />
                                                        <asp:Label ID="txtSociedadeUsuarioBeneficio" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divSociedadeUsuarioImpacto" class="row" runat="server">
                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="form-group">
                                                        <asp:Label ID="lblSociedadeUsuarioImpacto" Text="IMPACTO: " runat="server" />
                                                        <asp:Label ID="txtSociedadeUsuarioImpacto" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="divInstitucional" class="row" runat="server">
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group">
                                            <asp:Label ID="lblInstitucional" Text="INSTITUCIONAL: " runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-xs-10 col-sm-10">
                                        <div class="form-group">
                                            <div id="divInstitucionalBeneficio" class="row" runat="server">
                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="form-group">
                                                        <asp:Label ID="lblInstitucionalBeneficio" Text="BENEFÍCIO: " runat="server" />
                                                        <asp:Label ID="txtInstitucionalBeneficio" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divInstitucionalImpacto" class="row" runat="server">
                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="form-group">
                                                        <asp:Label ID="lblInstitucionalImpacto" Text="IMPACTO: " runat="server" />
                                                        <asp:Label ID="txtInstitucionalImpacto" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="divGoverno" class="row" runat="server">
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group">
                                            <asp:Label ID="lblGoverno" Text="GOVERNO: " runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-xs-10 col-sm-10">
                                        <div class="form-group">
                                            <div id="divGovernoBeneficio" class="row" runat="server">
                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="form-group">
                                                        <asp:Label ID="lblGovernoBeneficio" Text="BENEFÍCIO: " runat="server" />
                                                        <asp:Label ID="txtGovernoBeneficio" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divGovernoImpacto" class="row" runat="server">
                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="form-group">
                                                        <asp:Label ID="lblGovernoImpacto" Text="IMPACTO: " runat="server" />
                                                        <asp:Label ID="txtGovernoImpacto" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="divSetorRegulado" class="row" runat="server">
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group">
                                            <asp:Label ID="lblSetorRegulado" Text="SETOR REGULADO: " runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-xs-10 col-sm-10">
                                        <div class="form-group">
                                            <div id="divSetorReguladoBeneficio" class="row" runat="server">
                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="form-group">
                                                        <asp:Label ID="lblSetorReguladoBeneficio" Text="BENEFÍCIO: " runat="server" />
                                                        <asp:Label ID="txtSetorReguladoBeneficio" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divSetorReguladoImpacto" class="row" runat="server">
                                                <div class="col-xs-12 col-sm-12">
                                                    <div class="form-group">
                                                        <asp:Label ID="lblSetorReguladoImpacto" Text="IMPACTO: " runat="server" />
                                                        <asp:Label ID="txtSetorReguladoImpacto" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>Indicadores</strong></h3>
                            </div>
                            <div class="panel-body">
                                <asp:Repeater ID="rptIndicadores" runat="server">
                                    <ItemTemplate>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblIndicador" Text="INDICADOR: " runat="server" />
                                                    <asp:Label ID="txtIndicador" runat="server" Text='<%# Eval("NoIndicador") %>' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblDescricaoIndicador" Text="DESCRIÇÃO: " runat="server" />
                                                    <asp:Label ID="txtDescricaoIndicador" runat="server" Text='<%# Eval("DescricaoIndicador") %>' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblLinhaBase" Text="LINHA DE BASE: " runat="server" />
                                                    <asp:Label ID="txtLinhaBase" runat="server" Text='<%# Eval("ValorLinhaBase") %>' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblValorAlvo" Text="VALOR ALVO: " runat="server" />
                                                    <asp:Label ID="txtValorAlvo" runat="server" Text='<%# Eval("ValorAlvo") %>' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblDataAlvo" Text="DATA ALVO: " runat="server" />
                                                    <asp:Label ID="txtDataAlvo" runat="server" Text='<%# String.Format("{0:dd/MM/yyyy}", Eval("DataAlvo")) %>' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblMesReferencia" Text="MÊS DE REFERÊNCIA: " runat="server" />
                                                    <asp:Label ID="txtMesReferencia" runat="server" Text='<%# String.Format("{0:y}", Eval("MesReferencia")) %>' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblFormula" Text="FÓRMULA: " runat="server" />
                                                    <asp:Label ID="txtFormula" runat="server" Text='<%# Eval("DescricaoFormula") %>' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblTipoIndicador" Text="TIPO DE INDICADOR: " runat="server" />
                                                    <asp:Label ID="txtTipoIndicador" runat="server" Text='<%# Eval("DescricaoTipoIndicador") %>' />
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <SeparatorTemplate>
                                        <br />
                                    </SeparatorTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>Plano de Ação</strong></h3>
                            </div>
                            <div class="panel-body">
                                <asp:Repeater ID="rptAcoes" runat="server">
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblAtividade" Text="ATIVIDADE: " runat="server" />
                                                    <asp:Label ID="txtAtividade" runat="server" Text='<%# Eval("DescricaoAtividadeAcao") %>' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblEstrategia" Text="ESTRATÉGIA: " runat="server" />
                                                    <asp:Label ID="txtEstrategia" runat="server" Text='<%# Eval("DescricaoEstrategiaAcao") %>' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblResponsavel" Text="RESPONSÁVEL: " runat="server" />
                                                    <asp:Label ID="txtResponsavel" runat="server" Text='<%# Eval("DescricaoResponsavel") %>' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblPeso" Text="PESO: " runat="server" />
                                                    <asp:Label ID="txtPeso" runat="server" Text='<%# Eval("Peso") %>' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-3 col-sm-3">
                                                <div class="form-group">
                                                    <asp:Label ID="lblInicio" Text="INÍCIO: " runat="server" />
                                                    <asp:Label ID="txtInicio" runat="server" Text='<%# Eval("DataInicioAcao") != null ? Convert.ToDateTime(Eval("DataInicioAcao")).ToString("dd/MM/yyyy") : String.Empty %>' />
                                                </div>
                                            </div>
                                            <div class="col-xs-9 col-sm-9">
                                                <div class="form-group">
                                                    <asp:Label ID="lblFim" Text="FIM: " runat="server" />
                                                    <asp:Label ID="txtFim" runat="server" Text='<%# Eval("DataFimAcao") != null ? Convert.ToDateTime(Eval("DataFimAcao")).ToString("dd/MM/yyyy") : String.Empty %>' />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-2 col-sm-2">
                                                <div class="form-group">
                                                    <asp:Label ID="lblRecursos" Text="RECURSOS: " runat="server" />
                                                </div>
                                            </div>
                                            <div class="col-xs-10 col-sm-10">
                                                <div class="form-group">
                                                    <asp:Repeater ID="rptRecursos" runat="server" DataSource='<%# Eval("Recursos") %>'>
                                                        <ItemTemplate>
                                                            <div class="col-xs-1 col-sm-1">
                                                                <asp:Label ID="txtQuantidadeRecurso" runat="server" Text='<%# Eval("QuantidadeRecurso") %>' />
                                                            </div>
                                                            <div class="col-xs-4 col-sm-4">
                                                                <asp:Label ID="txtDescricaoRecurso" runat="server" Text='<%# Eval("DescricaoRecurso") %>' />
                                                            </div>
                                                            <div class="col-xs-7 col-sm-7">
                                                                <asp:Label ID="txtTipoRecurso" runat="server" Text='<%# Eval("DescricaoTipoRecurso") %>' />
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <SeparatorTemplate>
                                        <br />
                                    </SeparatorTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div id="divAnexo" runat="server" visible="false" class="form-group row">
                            <div class="col-xs-4 col-sm-4">
                                <asp:Label ID="lblJustificativaCancelamento" Text="Justificativa do Cancelamento: " runat="server" />
                                <asp:TextBox ID="txtJustificativaCancelamento" CssClass="form-control" runat="server" TextMode="MultiLine" />
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <asp:Label Text="Anexo" runat="server" />
                                <br />

                                <asp:UpdatePanel ID="updPanelAnexo" runat="server">
                                    <ContentTemplate>
                                        <asp:FileUpload ID="flpAnexo" runat="server" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:Button ID="btnAnexar" Text="Anexar" runat="server" OnClick="btnAnexar_Click" />

                                <asp:LinkButton ID="lnkBaixarAnexo" ToolTip="Realizar o download do arquivo" runat="server" CaminhoAnexo="">
                                    <asp:Label ID="lblAnexo" Text="Sem anexo" runat="server" />   <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkDeletarAnexo" ToolTip="Excluir arquivo do Cancelamento" runat="server">
                                    <i class="glyphicon glyphicon-remove" style="color:red;"></i>
                                </asp:LinkButton>
                            </div>

                        </div>
                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:LinkButton ID="btnEnviarMeta2743" runat="server" SkinID="btnEnviarParaValidacao" CausesValidation="false" />
                                <asp:LinkButton ID="btnAprovarMeta2741" runat="server" SkinID="btnAprovarCadastro" CausesValidation="false" />
                                <asp:LinkButton ID="btnRejeitarMeta2742" runat="server" SkinID="btnRejeitarParaAjustes" CausesValidation="false" />
                                <asp:LinkButton ID="btnCancelar2746" runat="server" SkinID="btnCancelar" CausesValidation="false" />
                                <asp:LinkButton ID="btnRetornarParaValidada2745" runat="server" SkinID="btnRetornarParaValidada" CausesValidation="false" Visible="true" />
                                <asp:LinkButton ID="btnRetornarParaCadastro2744" runat="server" SkinID="btnRetornarParaCadastro" CausesValidation="false" />
                                <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
