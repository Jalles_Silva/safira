﻿<%@ Page Title="SAFIRA - Consultar Meta" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false"
    CodeBehind="Consultar.aspx.cs" Inherits="PGA.Presentation.Site.Meta.Consultar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Metas 
                            <asp:LinkButton ID="lnkAjudaMeta" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaMeta" ForeColor="#034623" />
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblAnoExercicio" AssociatedControlID="lblAnoExercicio" Text="Exercício" runat="server" />
                                    <asp:DropDownList ID="ddlAnoExercicio" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="ddlArea" Text="Área" runat="server" />
                                    <asp:DropDownList ID="ddlArea" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:HiddenField ID="hdfAreaUsuarioLogado" runat="server" />
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblMetaInterna" AssociatedControlID="ddlMetaInterna" Text="Constante no PGA" runat="server" />
                                    <asp:DropDownList ID="ddlMetaInterna" runat="server" CssClass="form-control">
                                        <asp:ListItem Selected="True" Value="0">Todos</asp:ListItem>
                                        <asp:ListItem Value="2">Sim</asp:ListItem>
                                        <asp:ListItem Value="1">Não</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdfMetaInterna" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-4 col-sm-4">
                                    <div class="form-group">
                                        <asp:Label ID="lblStatusMeta" AssociatedControlID="ddlStatusMeta" Text="Status da Meta" runat="server" />
                                        <asp:DropDownList ID="ddlStatusMeta" runat="server" CssClass="form-control"></asp:DropDownList>
                                        <asp:HiddenField ID="hdfStatusMeta" runat="server" />
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-6">
                                    <div class="form-group">
                                        <br/>
                                        <asp:LinkButton ID="btnPesquisar" runat="server" SkinID="btnPesquisar" CausesValidation="false" />
                                        <asp:HyperLink ID="btnLimpar" runat="server" SkinID="btnLimpar" CausesValidation="false"
                                            NavigateUrl="~/Site/Meta/Consultar.aspx" />
                                        <asp:HyperLink ID="btnNovo" runat="server" SkinID="btnNovo" CausesValidation="false"
                                            NavigateUrl="~/Site/Meta/Detalhar.aspx" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="upPnlResultadoConsulta" runat="server">
                        <ContentTemplate>
                            <div class="area-table">
                                <asp:Repeater ID="rptMetas" runat="server">
                                    <HeaderTemplate>
                                        <table>
                                            <tr runat="server">
                                                <th colspan="2">Metas</th>
                                                <th>Exercício</th>
                                                <th>Status</th>
                                                <th>Constante no PGA</th>
                                                <th>Ações</th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td colspan="6" style="background-color: #C5D8CC !important;">
                                                <asp:Label ID="lblNomeUnidade" runat="server" Text='<%#Eval("Key") %>' />
                                            </td>
                                        </tr>
                                        <asp:Repeater ID="rptMetasPorUnidade" OnItemDataBound="rptMetas_ItemDataBound" OnItemCommand="rptMetasPorUnidade_ItemCommand" runat="server" DataSource='<%#Eval("Value") %>'>
                                            <ItemTemplate>
                                                <tr class="<%# (Container.ItemIndex % 2 == 0 ? "" : "alternate") %>">
                                                    <td style="width: 50px;">
                                                        <asp:HiddenField ID="hdfNomeUnidade" runat="server" Value='<%#Eval("CodigoUnidade") %>' />
                                                        <asp:HiddenField ID="hdfCodigoStatusMeta" runat="server" Value='<%#Eval("CodigoStatusMeta") %>' />
                                                        <asp:HiddenField ID="hdfCodigoFaseExercicio" runat="server" Value='<%#Eval("CodigoFaseExercicio") %>' />
                                                        <asp:HiddenField ID="hdfStatusMetaInterna" runat="server" Value='<%#Eval("StatusMetaInterna") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblNomeMeta" runat="server" Text='<%#Eval("DescricaoMeta") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblAnoExercicio" runat="server" Text='<%#Eval("AnoExercicio") %>' />
                                                    </td>
                                                    <td style="width: 100px">
                                                        <asp:Label ID="lblStatusMeta" runat="server" Text='<%#Eval("DescricaoStatusMeta") %>' />
                                                    </td>
                                                    <td style="width: 100px">
                                                        <asp:Label ID="lblMetaInterna" runat="server" Text='<%#Convert.ToInt32(Eval("StatusMetaInterna")) == 0 ? "Não" : "Sim" %>' />
                                                    </td>
                                                    <td style="width: 90px; justify-content: right; text-align: right;">
                                                        <asp:LinkButton ID="lnkDetalhar" runat="server" Target="_self" CommandArgument='<%# Eval("CodSeqMeta") %>'
                                                            CommandName="Detalhar" CausesValidation="false" CssClass="glyphicon glyphicon-pencil" />

                                                        <asp:LinkButton ID="lnkExcluir" runat="server" CommandArgument='<%# Eval("CodSeqMeta") %>'
                                                            CommandName="Excluir" CausesValidation="false" CssClass="glyphicon glyphicon-remove" />

                                                        <asp:LinkButton ID="lnkVisualizar" runat="server" Target="_self" CommandArgument='<%# Eval("CodSeqMeta") %>'
                                                            CommandName="Visuzalizar" CausesValidation="false" CssClass="glyphicon glyphicon-search" />

                                                        <asp:LinkButton ID="lnkCancelar" runat="server" Target="_self" CommandArgument='<%# Eval("CodSeqMeta") %>'
                                                            CommandName="CancelarMeta" CausesValidation="false" CssClass="glyphicon glyphicon-circle-arrow-right" Style="color:orange;" />

                                                        <asp:LinkButton ID="lnkValidar" runat="server" Target="_self" CommandArgument='<%# Eval("CodSeqMeta") %>'
                                                            CommandName="Visuzalizar" CausesValidation="false" CssClass="glyphicon glyphicon-circle-arrow-right"
                                                            Style='<%# Convert.ToInt32(Eval("CodigoStatusMeta")) == 0 ? "visibility:hidden;": ((PGA.Common.StatusMetaEnum)Convert.ToInt32(Eval("CodigoStatusMeta")) == PGA.Common.StatusMetaEnum.EmCadastro) ? "color:red;": 
                                                                        ((PGA.Common.StatusMetaEnum)Convert.ToInt32(Eval("CodigoStatusMeta")) == PGA.Common.StatusMetaEnum.EmValidacao) ? "color:yellow;" : "color:green;" %>' />

                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <tr>
                                            <td colspan="6">
                                                <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." /></td>
                                        </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="paginator">
                                <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        </div>
        <div id="modalAjudaMeta" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Lista de Metas</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <p>Nesta tela devem ser descritas as metas a serem atingidas para que a unidade alcance os objetivos traçados para 2017.</p>
                                        <p>Cada meta deve ser, sempre que possível: quantificável, desafiadora, gerenciável, exequível, desdobrada e voltada para o usuário final.</p>
                                        <p>Para o estabelecimento das metas deve-se ter como referência os objetivos do Mapa Estratégico da ANTT, os macro indicadores estratégicos e as iniciativas estratégicas.</p>
                                        <p>As metas devem estar relacionadas aos processos finalísticos e/ou de gestão da ANTT.</p>
                                        <p>Para incluir uma nova META, clique no botão "Novo".</p>
                                        <p>As metas já cadastradas aparecem na tabela e podem ser alteradas ou excluídas.</p>
                                        <p>Atenção: ao excluir uma meta, as ações e os indicadores cadastrados para esta meta também são excluídos e esta ação não tem como ser desfeita!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
