﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using PGA.Common;

namespace PGA.Presentation.Site.Meta
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodSeqMetaParameter
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodSeqMeta").ToInt32();
            }
        }

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private int AnoExercicio
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("AnoExercicio").ToInt32();
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            drpArea.SelectedIndexChanged += drpArea_SelectedIndexChanged;
            chkAlinhamentoPe.CheckedChanged += chkAlinhamentoPe_CheckedChanged;
            chkAlinhamentoPpa.CheckedChanged += chkAlinhamentoPpa_CheckedChanged;
            chkAlinhamentoMissao.CheckedChanged += chkAlinhamentoMissao_CheckedChanged;
            chkGestaoDeRiscos.CheckedChanged += chkGestaoDeRiscos_CheckedChanged;
            chkAgendaRegulatoria.CheckedChanged += chkAgendaRegulatoria_CheckedChanged;
            chkDesburocratizacao.CheckedChanged += chkDesburocratizacao_CheckedChanged;
            chkIntegridade.CheckedChanged += chkIntegridade_CheckedChanged;
            chkStatusIniciativaEstrategica.Checked = chkAlinhamentoPe.Checked;
            txtOutros.Enabled = false;
            chkStatusOutros.CheckedChanged += chkStatusOutros_CheckedChanged;
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
            btnAtividade.Click += new EventHandler(btnAtividade_Click);
            btnIndicador.Click += new EventHandler(btnIndicador_Click);
        }

        protected void btnAtividade_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Meta/Acao/Consultar.aspx?CodigoSeqMeta={0}&AnoExercicio={1}&Unidade={2}&PageIndex={3}", CodSeqMetaParameter, AnoExercicio, Unidade, PageIndex));
        }

        protected void btnIndicador_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Meta/Indicador/Consultar.aspx?CodigoSeqMeta={0}&AnoExercicio={1}&Unidade={2}&PageIndex={3}", CodSeqMetaParameter, AnoExercicio, Unidade, PageIndex));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarControles();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Voltar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        WebHelper.Redirect(String.Format("~/Site/Meta/Consultar.aspx?AnoExercicio={0}&Unidade={1}&PageIndex={2}", AnoExercicio, Unidade, PageIndex));
                    }
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
                case "Detalhar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        var codSeqMeta = e.Parameters["codSeqMeta"];
                        WebHelper.Redirect(String.Format("~/Site/Meta/Detalhar.aspx?CodSeqMeta={0}&AnoExercicio={1}&Unidade={2}&PageIndex={3}", codSeqMeta, AnoExercicio, Unidade, PageIndex));
                    }
                    break;
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Voltar",
                "Deseja retornar para a página de consulta (dados não salvos serão perdidos)?", MessageBoxButtons.YesNo, "Voltar");
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            PreSalvar();
        }

        protected void drpArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarIniciativas();
            CarregarInstrumentos();
        }

        protected void drpAlinhamentoPE_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarObjetivoIniciativa(ref drpAlinhamentoPE, ref lnkObjetivoPE, ref rptObjetivosPE);
        }

        protected void drpAlinhamentoPPA_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarObjetivoIniciativa(ref drpAlinhamentoPPA, ref lnkObjetivoPPA, ref rptObjetivosPPA);
        }

        protected void chkAlinhamentoPe_CheckedChanged(object sender, EventArgs e)
        {
            drpAlinhamentoPE.SelectedValue = "";
            drpAlinhamentoPE.Enabled = chkAlinhamentoPe.Checked;
            chkStatusIniciativaEstrategica.Checked = chkAlinhamentoPe.Checked;
            //chkStatusIniciativaEstrategica.Enabled = chkAlinhamentoPe.Checked;
            lnkObjetivoPE.Visible = false;
        }

        protected void chkAlinhamentoPpa_CheckedChanged(object sender, EventArgs e)
        {
            drpAlinhamentoPPA.SelectedValue = "";
            drpAlinhamentoPPA.Enabled = chkAlinhamentoPpa.Checked;
            lnkObjetivoPPA.Visible = false;
        }

        protected void chkAlinhamentoMissao_CheckedChanged(object sender, EventArgs e)
        {
            drpAlinhamentoMissao.SelectedValue = "";
            drpAlinhamentoMissao.Enabled = chkAlinhamentoMissao.Checked;
        }

        protected void chkGestaoDeRiscos_CheckedChanged(object sender, EventArgs e)
        {
            drpGestaoDeRiscos.SelectedValue = "";
            drpGestaoDeRiscos.Enabled = chkGestaoDeRiscos.Checked;
        }

        protected void chkAgendaRegulatoria_CheckedChanged(object sender, EventArgs e)
        {
            drpAgendaRegulatoria.SelectedValue = "";
            drpAgendaRegulatoria.Enabled = chkAgendaRegulatoria.Checked;
        }

        protected void chkDesburocratizacao_CheckedChanged(object sender, EventArgs e)
        {
            drpDesburocratizacao.SelectedValue = "";
            drpDesburocratizacao.Enabled = chkDesburocratizacao.Checked;
        }

        protected void chkIntegridade_CheckedChanged(object sender, EventArgs e)
        {
            drpIntegridade.SelectedValue = "";
            drpIntegridade.Enabled = chkIntegridade.Checked;
        }

        protected void chkStatusOutros_CheckedChanged(object sender, EventArgs e)
        {
            txtOutros.Text = String.Empty;
            txtOutros.Enabled = chkStatusOutros.Checked;
        }

        #endregion

        #region [Methods]

        private void CarregarControles()
        {
            try
            {
                CarregarTipoMeta();
                CarregarUnidade();
                CarregarAnoExercicio();
                CarregarInstrumentos();
                CarregarRegistro();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarIniciativas()
        {
            CarregarIniciativa(TiposAlinhamento.Alinhamento_PE, ref drpAlinhamentoPE);
            CarregarIniciativa(TiposAlinhamento.Alinhamento_PPA, ref drpAlinhamentoPPA);
            CarregarIniciativa(TiposAlinhamento.Alinhamento_MI, ref drpAlinhamentoMissao);
            CarregarObjetivoIniciativa(ref drpAlinhamentoPE, ref lnkObjetivoPE, ref rptObjetivosPE);
            CarregarObjetivoIniciativa(ref drpAlinhamentoPPA, ref lnkObjetivoPPA, ref rptObjetivosPPA);
        }

        private void CarregarInstrumentos()
        {
            CarregarInstrumento(TiposInstrumento.AgendaRegulatoria, ref drpAgendaRegulatoria);
            CarregarInstrumento(TiposInstrumento.Desburocratizacao, ref drpDesburocratizacao);
            CarregarInstrumento(TiposInstrumento.GestaoRiscos, ref drpGestaoDeRiscos);
            CarregarInstrumento(TiposInstrumento.Integridade, ref drpIntegridade);
        }

        private void CarregarUnidade()
        {
            drpArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarCorporativoUnidadeAtivo();
            drpArea.DataTextField = "DescricaoUnidade";
            drpArea.DataValueField = "CodigoUnidade";
            drpArea.DataBind();
            drpArea.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void CarregarAnoExercicio()
        {
            if (CodSeqMetaParameter > 0)
            {
                ddlAnoExercicio.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarExercicios();
                ddlAnoExercicio.DataTextField = "Ano";
                ddlAnoExercicio.DataValueField = "CodigoSeqExercicio";
                ddlAnoExercicio.DataBind();
                ddlAnoExercicio.Items.Insert(0, new ListItem("Selecione", ""));
            }
            else
            {
                if (UsuarioAdministrador)
                {
                    ddlAnoExercicio.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarExercicios().Where(i => i.Fase.CodigoSeqFase != (int)FaseEnum.Encerrado);
                    ddlAnoExercicio.DataTextField = "Ano";
                    ddlAnoExercicio.DataValueField = "CodigoSeqExercicio";
                    ddlAnoExercicio.DataBind();
                    ddlAnoExercicio.Items.Insert(0, new ListItem("Selecione", ""));
                }
                else if (UsuarioCadastrador)
                {
                    ddlAnoExercicio.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarExercicioEmPlanejamento();
                    ddlAnoExercicio.DataTextField = "Ano";
                    ddlAnoExercicio.DataValueField = "CodigoSeqExercicio";
                    ddlAnoExercicio.DataBind();
                    ddlAnoExercicio.Items.Insert(0, new ListItem("Selecione", ""));
                }
                else
                {
                    btnSalvar.Visible = false;
                }
            }
        }

        private void CarregarTipoMeta()
        {
            ddlTipoMeta.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTipoMetas();
            ddlTipoMeta.DataTextField = "DescricaoTipoMeta";
            ddlTipoMeta.DataValueField = "CodigoSeqTipoMeta";
            ddlTipoMeta.DataBind();
            ddlTipoMeta.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void CarregarIniciativa(TiposAlinhamento tipoAlinhamento, ref DropDownList drpAlinhamento)
        {
            drpAlinhamento.Items.Clear();

            if (!String.IsNullOrEmpty(drpArea.SelectedValue))
            {
                drpAlinhamento.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarIniciativaPorTipoEUnidadeAtivos(tipoAlinhamento, drpArea.SelectedValue.ToInt32());
                drpAlinhamento.DataTextField = "DescricaoIniciativa";
                drpAlinhamento.DataValueField = "CodigoSeqIniciativa";
                drpAlinhamento.DataBind();
            }

            drpAlinhamento.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void CarregarObjetivoIniciativa(ref DropDownList drpAlinhamento, ref LinkButton lnkObetivo, ref Repeater rptObjetivos)
        {
            lnkObetivo.Visible = false;

            if (!String.IsNullOrEmpty(drpAlinhamento.SelectedValue))
            {
                var objetivos = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarObjetivosPorIniciativa(drpAlinhamento.SelectedValue.ToInt32(), 0, int.MaxValue, "", true);

                if (objetivos.Count() > 0)
                {
                    rptObjetivos.DataSource = objetivos;
                    rptObjetivos.DataBind();

                    lnkObetivo.Visible = true;
                }
            }
        }

        private void CarregarInstrumento(TiposInstrumento tipoInstrumento, ref DropDownList drpInstrumento)
        {
            drpInstrumento.Items.Clear();

            if (!String.IsNullOrEmpty(drpArea.SelectedValue))
            {
                drpInstrumento.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarInstrumentoPorTipoEUnidadeAtivos(tipoInstrumento, drpArea.SelectedValue.ToInt32());
                drpInstrumento.DataTextField = "DescricaoInstrumento";
                drpInstrumento.DataValueField = "CodigoSeqInstrumento";
                drpInstrumento.DataBind();
            }

            drpInstrumento.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void PreSalvar()
        {
            Validate();

            if (IsValid)
            {
                if (!chkAlinhamentoPe.Checked && !chkAlinhamentoPpa.Checked && !chkAlinhamentoMissao.Checked)
                {
                    MessageBox.ShowInformationMessage("Favor informar ao menos um tipo de Alinhamento, não é possível registrar uma meta sem informar nenhum tipo de Alinhamento.", "Erro");
                    return;
                }

                if (chkAlinhamentoPe.Checked && String.IsNullOrEmpty(drpAlinhamentoPE.SelectedValue))
                {
                    MessageBox.ShowInformationMessage("Favor informar o Alinhamento PE.", "Erro");
                    return;
                }

                if (chkAlinhamentoPpa.Checked && String.IsNullOrEmpty(drpAlinhamentoPPA.SelectedValue))
                {
                    MessageBox.ShowInformationMessage("Favor informar o Alinhamento PPA.", "Erro");
                    return;
                }

                if (chkAlinhamentoMissao.Checked && String.IsNullOrEmpty(drpAlinhamentoMissao.SelectedValue))
                {
                    MessageBox.ShowInformationMessage("Favor informar o Alinhamento Missão Institucional.", "Erro");
                    return;
                }

                if (chkGestaoDeRiscos.Checked && String.IsNullOrEmpty(drpGestaoDeRiscos.SelectedValue))
                {
                    MessageBox.ShowInformationMessage("Favor informar o campo Gestao de Riscos.", "Erro");
                    return;
                }

                if (chkAgendaRegulatoria.Checked && String.IsNullOrEmpty(drpAgendaRegulatoria.SelectedValue))
                {
                    MessageBox.ShowInformationMessage("Favor informar o campo Agenda Regulatória.", "Erro");
                    return;
                }

                if (chkDesburocratizacao.Checked && String.IsNullOrEmpty(drpDesburocratizacao.SelectedValue))
                {
                    MessageBox.ShowInformationMessage("Favor informar o campo Desburocratização", "Erro");
                    return;
                }

                if (chkIntegridade.Checked && String.IsNullOrEmpty(drpIntegridade.SelectedValue))
                {
                    MessageBox.ShowInformationMessage("Favor informar o campo Integridade.", "Erro");
                    return;
                }

                if (chkStatusOutros.Checked && String.IsNullOrEmpty(txtOutros.Text))
                {
                    MessageBox.ShowInformationMessage("Favor informar o campo Outros.", "Erro");
                    return;
                }

                var maxLengthJustificativa = 4000;
                if (txtDescricaoJustificativa.Text.Trim().Length > maxLengthJustificativa)
                {
                    MessageBox.ShowInformationMessage("Campo Justificativa excede o limite de 4000 caracteres.", "Erro");
                    return;
                }

                var maxLengthBeneficioImpacto = 2000;
                if (txtSociedadeUsuarioBeneficio.Text.Trim().Length > maxLengthBeneficioImpacto)
                {
                    MessageBox.ShowInformationMessage("Campo Benefício na área Sociedade/Usuário excede o limite de 2000 caracteres.", "Erro");
                    return;
                }
                
                if (txtSociedadeUsuarioImpacto.Text.Trim().Length > maxLengthBeneficioImpacto)
                {
                    MessageBox.ShowInformationMessage("Campo Impacto na área Sociedade/Usuário excede o limite de 2000 caracteres.", "Erro");
                    return;
                }

                if (txtInstitucionalBeneficio.Text.Trim().Length > maxLengthBeneficioImpacto)
                {
                    MessageBox.ShowInformationMessage("Campo Benefício na área Institucional excede o limite de 2000 caracteres.", "Erro");
                    return;
                }

                if (txtInstitucionalImpacto.Text.Trim().Length > maxLengthBeneficioImpacto)
                {
                    MessageBox.ShowInformationMessage("Campo Impacto na área Institucional excede o limite de 2000 caracteres.", "Erro");
                    return;
                }

                if (txtGovernoBeneficio.Text.Trim().Length > maxLengthBeneficioImpacto)
                {
                    MessageBox.ShowInformationMessage("Campo Benefício na área Governo excede o limite de 2000 caracteres.", "Erro");
                    return;
                }

                if (txtGovernoImpacto.Text.Trim().Length > maxLengthBeneficioImpacto)
                {
                    MessageBox.ShowInformationMessage("Campo Impacto na área Governo excede o limite de 2000 caracteres.", "Erro");
                    return;
                }

                if (txtSetorReguladoBeneficio.Text.Trim().Length > maxLengthBeneficioImpacto)
                {
                    MessageBox.ShowInformationMessage("Campo Benefício na área Setor Regulado excede o limite de 2000 caracteres.", "Erro");
                    return;
                }

                if (txtSetorReguladoImpacto.Text.Trim().Length > maxLengthBeneficioImpacto)
                {
                    MessageBox.ShowInformationMessage("Campo Impacto na área Setor Regulado excede o limite de 2000 caracteres.", "Erro");
                    return;
                }
                Salvar();
            }
        }

        private void Salvar()
        {
            try
            {
                DTOMeta registro = new DTOMeta();
                registro.CodSeqMeta = CodSeqMetaParameter;
                registro.DescricaoMeta = (txtDescricaoMeta.Text.Length > 1000) ? txtDescricaoMeta.Text.Substring(0, 1000) : txtDescricaoMeta.Text;
                registro.DescricaoJustificativa = txtDescricaoJustificativa.Text;
                registro.DescricaoResultadoEsperado = txtDescricaoResultadoEsperado.Text;
                registro.DescricaoPremissa = txtDescricaoPremissa.Text;
                registro.DescricaoRestricao = txtDescricaoRestricao.Text;
                
                registro.CodigoSeqGestaoDeRiscos = drpGestaoDeRiscos.SelectedValue.ToInt32();
                registro.CodigoSeqAgendaRegulatoria = drpAgendaRegulatoria.SelectedValue.ToInt32();
                registro.CodigoSeqDesburocratizacao = drpDesburocratizacao.SelectedValue.ToInt32();
                registro.CodigoSeqIntegridade = drpIntegridade.SelectedValue.ToInt32();

                registro.NoCoordenacao = txtNoCoordenacao.Text;
                registro.CodigoSeqTipoMeta = ddlTipoMeta.SelectedValue.ToInt32();
                registro.CodigoUnidade = drpArea.SelectedValue.ToInt32();
                registro.NomeUsuario = SCAApplicationContext.Usuario.Nome;
                registro.CodigoSeqExercicio = ddlAnoExercicio.SelectedValue.ToInt32();
                registro.StatusMetaInterna = chkMetaInterna.Checked;

                registro.DescricaoOutros = txtOutros.Text;
                registro.DescricaoSociedadeUsuarioBeneficio = txtSociedadeUsuarioBeneficio.Text;
                registro.DescricaoSociedadeUsuarioImpacto = txtSociedadeUsuarioImpacto.Text;
                registro.DescricaoInstitucionalBeneficio = txtInstitucionalBeneficio.Text;
                registro.DescricaoInstitucionalImpacto = txtInstitucionalImpacto.Text;
                registro.DescricaoGovernoBeneficio = txtGovernoBeneficio.Text;
                registro.DescricaoGovernoImpacto = txtGovernoImpacto.Text;
                registro.DescricaoSetorReguladoBeneficio = txtSetorReguladoBeneficio.Text;
                registro.DescricaoSetorReguladoImpacto = txtSetorReguladoImpacto.Text;

                if (!String.IsNullOrEmpty(drpAlinhamentoPE.SelectedValue) && chkAlinhamentoPe.Checked)
                {
                    var iniciativa = new DTOIniciativa();
                    iniciativa.CodigoSeqIniciativa = drpAlinhamentoPE.SelectedValue.ToInt32();
                    registro.IniciativaPe = iniciativa;
                    registro.StatusIniciativaEstrategica = chkStatusIniciativaEstrategica.Checked;
                }

                if (!String.IsNullOrEmpty(drpAlinhamentoPPA.SelectedValue) && chkAlinhamentoPpa.Checked)
                {
                    var iniciativa = new DTOIniciativa();
                    iniciativa.CodigoSeqIniciativa = drpAlinhamentoPPA.SelectedValue.ToInt32();
                    registro.IniciativaPpa = iniciativa;
                }

                if (!String.IsNullOrEmpty(drpAlinhamentoMissao.SelectedValue) && chkAlinhamentoMissao.Checked)
                {
                    var iniciativa = new DTOIniciativa();
                    iniciativa.CodigoSeqIniciativa = drpAlinhamentoMissao.SelectedValue.ToInt32();
                    registro.IniciativaMissao = iniciativa;
                }

                var codSeqMeta = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarMeta(registro);

                var param = new Dictionary<string, object>();
                param.Add("codSeqMeta", codSeqMeta);

                MessageBox.ShowInformationMessage("Registro salvo com sucesso.","Detalhar", param);                
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodSeqMetaParameter > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterMeta(CodSeqMetaParameter);

                    txtDescricaoMeta.Text = registro.DescricaoMeta;
                    txtDescricaoJustificativa.Text = registro.DescricaoJustificativa;
                    txtDescricaoResultadoEsperado.Text = registro.DescricaoResultadoEsperado;
                    txtDescricaoPremissa.Text = registro.DescricaoPremissa;
                    txtDescricaoRestricao.Text = registro.DescricaoRestricao;
                    drpArea.SelectedValue = registro.CorporativoUnidade.CodigoUnidade.ToString();
                    ddlTipoMeta.SelectedValue = registro.CodigoSeqTipoMeta.ToString();
                    ddlAnoExercicio.SelectedValue = registro.Exercicio.CodigoSeqExercicio.ToString();
                    chkMetaInterna.Checked = registro.StatusMetaInterna;

                    txtOutros.Text = registro.DescricaoOutros;
                    chkStatusOutros.Checked = txtOutros.Enabled = !String.IsNullOrEmpty(txtOutros.Text);
                    //chkStatusIniciativaEstrategica.Enabled = registro.IniciativaPe != null;

                    CarregarInstrumentos();

                    if (registro.CodigoSeqGestaoDeRiscos > 0 && drpGestaoDeRiscos.Items.FindByValue(registro.CodigoSeqGestaoDeRiscos.ToString()) != null)
                        drpGestaoDeRiscos.SelectedValue = registro.CodigoSeqGestaoDeRiscos.ToString();
                    if (registro.CodigoSeqAgendaRegulatoria > 0 && drpAgendaRegulatoria.Items.FindByValue(registro.CodigoSeqAgendaRegulatoria.ToString()) != null)
                        drpAgendaRegulatoria.SelectedValue = registro.CodigoSeqAgendaRegulatoria.ToString();
                    if (registro.CodigoSeqDesburocratizacao > 0 && drpDesburocratizacao.Items.FindByValue(registro.CodigoSeqDesburocratizacao.ToString()) != null)
                        drpDesburocratizacao.SelectedValue = registro.CodigoSeqDesburocratizacao.ToString();
                    if (registro.CodigoSeqIntegridade > 0 && drpIntegridade.Items.FindByValue(registro.CodigoSeqIntegridade.ToString()) != null)
                        drpIntegridade.SelectedValue = registro.CodigoSeqIntegridade.ToString();

                    chkGestaoDeRiscos.Checked = !String.IsNullOrEmpty(drpGestaoDeRiscos.Text);
                    chkAgendaRegulatoria.Checked = !String.IsNullOrEmpty(drpAgendaRegulatoria.Text);
                    chkDesburocratizacao.Checked = !String.IsNullOrEmpty(drpDesburocratizacao.Text);
                    chkIntegridade.Checked = !String.IsNullOrEmpty(drpIntegridade.Text);

                    txtSociedadeUsuarioBeneficio.Text = registro.DescricaoSociedadeUsuarioBeneficio;
                    txtSociedadeUsuarioImpacto.Text = registro.DescricaoSociedadeUsuarioImpacto;
                    txtInstitucionalBeneficio.Text = registro.DescricaoInstitucionalBeneficio;
                    txtInstitucionalImpacto.Text = registro.DescricaoInstitucionalImpacto;
                    txtGovernoBeneficio.Text = registro.DescricaoGovernoBeneficio;
                    txtGovernoImpacto.Text = registro.DescricaoGovernoImpacto;
                    txtSetorReguladoBeneficio.Text = registro.DescricaoSetorReguladoBeneficio;
                    txtSetorReguladoImpacto.Text = registro.DescricaoSetorReguladoImpacto;

                    CarregarIniciativas();

                    if (registro.IniciativaPe != null && drpAlinhamentoPE.Items.Contains(new ListItem(registro.IniciativaPe.DescricaoIniciativa, registro.IniciativaPe.CodigoSeqIniciativa.ToString())))
                    {
                        drpAlinhamentoPE.SelectedValue = registro.IniciativaPe.CodigoSeqIniciativa.ToString();
                        chkStatusIniciativaEstrategica.Checked = registro.StatusIniciativaEstrategica != null;
                        CarregarObjetivoIniciativa(ref drpAlinhamentoPE, ref lnkObjetivoPE, ref rptObjetivosPE);
                    }

                    if (registro.IniciativaPpa != null && drpAlinhamentoPPA.Items.Contains(new ListItem(registro.IniciativaPpa.DescricaoIniciativa, registro.IniciativaPpa.CodigoSeqIniciativa.ToString())))
                    {
                        drpAlinhamentoPPA.SelectedValue = registro.IniciativaPpa.CodigoSeqIniciativa.ToString();
                        CarregarObjetivoIniciativa(ref drpAlinhamentoPPA, ref lnkObjetivoPPA, ref rptObjetivosPPA);
                    }

                    if (registro.IniciativaMissao != null && drpAlinhamentoMissao.Items.Contains(new ListItem(registro.IniciativaMissao.DescricaoIniciativa, registro.IniciativaMissao.CodigoSeqIniciativa.ToString())))
                        drpAlinhamentoMissao.SelectedValue = registro.IniciativaMissao.CodigoSeqIniciativa.ToString();

                    chkAlinhamentoPe.Checked = !String.IsNullOrEmpty(drpAlinhamentoPE.Text);
                    chkAlinhamentoPpa.Checked = !String.IsNullOrEmpty(drpAlinhamentoPPA.Text);
                    chkAlinhamentoMissao.Checked = !String.IsNullOrEmpty(drpAlinhamentoMissao.Text);

                    txtNoCoordenacao.Text = registro.NoCoordenacao;

                    btnAtividade.Visible = true;
                    btnIndicador.Visible = true;

                    if (UsuarioAdministrador)
                    {
                        btnSalvar.Visible = !(registro.Exercicio.Fase.CodigoSeqFase == (int)FaseEnum.Encerrado);
                    }
                    else if (UsuarioCadastrador)
                    {
                        btnSalvar.Visible = registro.Exercicio.Fase.CodigoSeqFase == (int)FaseEnum.Planejamento && registro.StatusMeta.CodigoSeqStatusMeta == (int)StatusMetaEnum.EmCadastro;
                    }
                }

                if (!UsuarioAdministrador)
                {
                    if (SCAApplicationContext.Usuario.CodigoSuperintendencia == 0)
                    {
                        MessageBox.ShowConfirmationMessage(MessageBoxType.Error, "Erro",
                             "Não foi possível carregar a superintêndencia, favor logar novamente.", MessageBoxButtons.OK, "Deslogar");
                        return;
                    }

                    drpArea.SelectedValue = SCAApplicationContext.Usuario.CodigoSuperintendencia.ToString();
                    drpArea.Enabled = false;
                    chkMetaInterna.Checked = true;

                    DTOExercicio exercicioPlanejamento = this.ObterPGAService().ListarExercicioEmPlanejamento().FirstOrDefault();
                    if (exercicioPlanejamento != null)
                    {
                        ddlAnoExercicio.SelectedValue = exercicioPlanejamento.CodigoSeqExercicio.ToString();
                    }

                    CarregarIniciativas();
                    CarregarInstrumentos();
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}