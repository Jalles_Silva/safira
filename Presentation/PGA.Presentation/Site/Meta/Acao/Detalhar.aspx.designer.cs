﻿//------------------------------------------------------------------------------
// <gerado automaticamente>
//     Este código foi gerado por uma ferramenta.
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for recriado
// </gerado automaticamente>
//------------------------------------------------------------------------------

namespace PGA.Presentation.Site.Meta.Acao
{


    public partial class Detalhar
    {

        /// <summary>
        /// Controle Panel.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel;

        /// <summary>
        /// Controle lblArea.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblArea;

        /// <summary>
        /// Controle lnkAjudaArea.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton lnkAjudaArea;

        /// <summary>
        /// Controle ddlArea.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlArea;

        /// <summary>
        /// Controle lblDescricaoMeta.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblDescricaoMeta;

        /// <summary>
        /// Controle LinkButton1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton1;

        /// <summary>
        /// Controle txtDescricaoMeta.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtDescricaoMeta;

        /// <summary>
        /// Controle lblDescricaoAtividadeAcao.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblDescricaoAtividadeAcao;

        /// <summary>
        /// Controle LinkButton2.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton2;

        /// <summary>
        /// Controle txtDescricaoAtividadeAcao.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtDescricaoAtividadeAcao;

        /// <summary>
        /// Controle rfvDescricaoAtividadeAcao.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvDescricaoAtividadeAcao;

        /// <summary>
        /// Controle lblDescricaoEstrategiaAcao.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblDescricaoEstrategiaAcao;

        /// <summary>
        /// Controle LinkButton3.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton3;

        /// <summary>
        /// Controle txtDescricaoEstrategiaAcao.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtDescricaoEstrategiaAcao;

        /// <summary>
        /// Controle rfvDescricaoEstrategiaAcao.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvDescricaoEstrategiaAcao;

        /// <summary>
        /// Controle lblDescricaoResponsavel.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblDescricaoResponsavel;

        /// <summary>
        /// Controle LinkButton4.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton4;

        /// <summary>
        /// Controle txtDescricaoResponsavel.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtDescricaoResponsavel;

        /// <summary>
        /// Controle rfvDescricaoResponsavel.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvDescricaoResponsavel;

        /// <summary>
        /// Controle lblDataInicioAcao.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblDataInicioAcao;

        /// <summary>
        /// Controle LinkButton5.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton5;

        /// <summary>
        /// Controle txtDataInicioAcao.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtDataInicioAcao;

        /// <summary>
        /// Controle lblDataFimAcao.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblDataFimAcao;

        /// <summary>
        /// Controle LinkButton6.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton6;

        /// <summary>
        /// Controle txtDataFimAcao.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtDataFimAcao;

        /// <summary>
        /// Controle lblPesoAtividade.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblPesoAtividade;

        /// <summary>
        /// Controle LinkButton7.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton7;

        /// <summary>
        /// Controle ddlPesoAtividade.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlPesoAtividade;

        /// <summary>
        /// Controle rfvPesoAtividade.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvPesoAtividade;

        /// <summary>
        /// Controle LinkButton8.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton8;

        /// <summary>
        /// Controle lblTipoRecurso.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblTipoRecurso;

        /// <summary>
        /// Controle LinkButton9.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton9;

        /// <summary>
        /// Controle ddlTipoRecurso.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlTipoRecurso;

        /// <summary>
        /// Controle rfvTipoRecurso.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvTipoRecurso;

        /// <summary>
        /// Controle lblNomeRecurso.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblNomeRecurso;

        /// <summary>
        /// Controle LinkButton10.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton10;

        /// <summary>
        /// Controle txtNomeRecurso.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtNomeRecurso;

        /// <summary>
        /// Controle rfvNomeRecurso.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvNomeRecurso;

        /// <summary>
        /// Controle lblQuantidadeRecurso.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblQuantidadeRecurso;

        /// <summary>
        /// Controle LinkButton11.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton LinkButton11;

        /// <summary>
        /// Controle txtQuantidadeRecurso.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox txtQuantidadeRecurso;

        /// <summary>
        /// Controle rfvQuantidadeRecurso.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator rfvQuantidadeRecurso;

        /// <summary>
        /// Controle UpdatePanel1.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel UpdatePanel1;

        /// <summary>
        /// Controle btnAdicionar.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton btnAdicionar;

        /// <summary>
        /// Controle upPnlRecursos.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel upPnlRecursos;

        /// <summary>
        /// Controle gdvRecursos.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView gdvRecursos;

        /// <summary>
        /// Controle ucPaginatorConsulta.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::SQFramework.Web.Controls.Paginator ucPaginatorConsulta;

        /// <summary>
        /// Controle updBotoes.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel updBotoes;

        /// <summary>
        /// Controle btnSalvar.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton btnSalvar;

        /// <summary>
        /// Controle btnVoltar.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Modificar a declaração do campo de movimento do arquivo de designer para o arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.LinkButton btnVoltar;
    }
}
