﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using PGA.Common;

namespace PGA.Presentation.Site.Meta.Acao
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqAcaoParameter
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqAcao").ToInt32();
            }
        }

        private int CodigoSeqMetaParameter
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqMeta").ToInt32();
            }
        }

        public List<DTORecurso> Recursos
        {
            set
            {
                ViewState["_Recursos"] = value;
            }

            get
            {
                if (ViewState["_Recursos"] == null)
                {
                    ViewState["_Recursos"] = new List<DTORecurso>();
                }

                return ViewState["_Recursos"] as List<DTORecurso>;
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);

            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            btnAdicionar.Click += new EventHandler(btnAdicionar_Click);
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
            gdvRecursos.RowCommand += gdvRecursos_RowCommand;
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Meta/Acao/Consultar.aspx?CodigoSeqMeta={0}", CodigoSeqMetaParameter));
        }

        protected void btnAdicionar_Click(object sender, EventArgs e)
        {
            Validate();

            if (IsValid)
            {
                DTORecurso recurso = new DTORecurso();
                recurso.CodigoSeqTipoRecurso = ddlTipoRecurso.SelectedValue.ToInt32();
                recurso.DescricaoTipoRecurso = ddlTipoRecurso.SelectedItem.Text;
                recurso.DescricaoRecurso = txtNomeRecurso.Text;
                recurso.QuantidadeRecurso = txtQuantidadeRecurso.Text.ToDecimal();
                Recursos.Add(recurso);
                gdvRecursos.DataSource = Recursos;
                gdvRecursos.DataBind();

                ddlTipoRecurso.SelectedValue = "";
                txtNomeRecurso.Text = String.Empty;
                txtQuantidadeRecurso.Text = String.Empty;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarControles();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Salvar":
                    if (e.Result == MessageBoxResult.Yes)
                        Salvar();
                    break;
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            PreSalvar();
        }

        protected void gdvRecursos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Excluir"))
            {
                string[] campos = e.CommandArgument.ToString().Split('-');
                int codigoTipoRecurso = campos[0].ToInt32();
                string descricao = campos[1];
                decimal quantidade = campos[2].ToDecimal();
                Recursos.RemoveAll(i => (i.CodigoSeqTipoRecurso == codigoTipoRecurso && i.DescricaoRecurso == descricao && i.QuantidadeRecurso == quantidade));
                gdvRecursos.DataSource = Recursos;
                gdvRecursos.DataBind();
            }
        }

        #endregion

        #region [Methods]

        private void CarregarControles()
        {
            try
            {
                if (CodigoSeqMetaParameter == 0 && CodigoSeqAcaoParameter == 0)
                    WebHelper.Redirect("~/Site/Meta/Consultar.aspx");
                CarregarTipoRecurso();
                CarregarRegistro();
                CarregarUnidade();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarUnidade()
        {
            ddlArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarCorporativoUnidadeAtivo();
            ddlArea.DataTextField = "DescricaoUnidade";
            ddlArea.DataValueField = "CodigoUnidade";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void CarregarTipoRecurso()
        {
            ddlTipoRecurso.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTipoRecursos();
            ddlTipoRecurso.DataTextField = "DescricaoTipoRecurso";
            ddlTipoRecurso.DataValueField = "CodigoSeqTipoRecurso";
            ddlTipoRecurso.DataBind();
            ddlTipoRecurso.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void PreSalvar()
        {
            Validate();

            if (IsValid)
            {
                if (Recursos.Count == 0)
                {
                    MessageBox.ShowInformationMessage("Informar os Recursos (não se esqueça de que é necessário clicar em  “+ Adicionar” para cada recurso informado).", "Erro");
                    return;
                }

                if (!ValidaData(txtDataInicioAcao.Text))
                {
                    MessageBox.ShowInformationMessage("Data Inicial inválida", "Erro");
                    return;
                }

                if (!ValidaData(txtDataFimAcao.Text))
                {
                    MessageBox.ShowInformationMessage("Data Final inválida", "Erro");
                    return;
                }

                DateTime? inicio = txtDataInicioAcao.Text.ToNullableDateTime("dd/MM/yyyy", null);
                DateTime? fim = txtDataFimAcao.Text.ToNullableDateTime("dd/MM/yyyy", null);

                if (inicio.HasValue && fim.HasValue && inicio > fim)
                {
                    MessageBox.ShowInformationMessage("Data Inicial deve ser menor que a Data Final.", "Erro");
                    return;
                }

                MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Salvar",
                    "Deseja realmente salvar?", MessageBoxButtons.YesNo, "Salvar");
            }
        }

        private void Salvar()
        {
            try
            {
                DTOAcao registro = new DTOAcao();
                registro.CodigoSeqAcao = CodigoSeqAcaoParameter;
                registro.DescricaoAtividadeAcao = txtDescricaoAtividadeAcao.Text;
                registro.DescricaoEstrategiaAcao = txtDescricaoEstrategiaAcao.Text;
                registro.DescricaoResponsavel = txtDescricaoResponsavel.Text;
                registro.Peso = ddlPesoAtividade.SelectedValue.Equals("Selecione") ? 0 : Convert.ToInt32(ddlPesoAtividade.SelectedValue);
                registro.DataInicioAcao = txtDataInicioAcao.Text.ToNullableDateTime("dd/MM/yyyy", null);
                registro.DataFimAcao = txtDataFimAcao.Text.ToNullableDateTime("dd/MM/yyyy", null);
                registro.CodigoSeqMeta = CodigoSeqMetaParameter;
                registro.Recursos = Recursos;
                registro.StatusAcao = 1;

                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarAcao(registro);

                WebHelper.Redirect(String.Format("~/Site/Meta/Acao/Consultar.aspx?CodigoSeqMeta={0}", CodigoSeqMetaParameter));
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqAcaoParameter > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterAcao(CodigoSeqAcaoParameter);

                    txtDescricaoAtividadeAcao.Text = registro.DescricaoAtividadeAcao;
                    txtDescricaoEstrategiaAcao.Text = registro.DescricaoEstrategiaAcao;
                    txtDescricaoResponsavel.Text = registro.DescricaoResponsavel;
                    ddlPesoAtividade.SelectedValue = registro.Peso.Equals(0) ? "0" : registro.Peso.ToString();
                    if (registro.DataInicioAcao.HasValue)
                        txtDataInicioAcao.Text = registro.DataInicioAcao.Value.ToString("dd/MM/yyyy");
                    if (registro.DataFimAcao.HasValue)
                        txtDataFimAcao.Text = registro.DataFimAcao.Value.ToString("dd/MM/yyyy");
                    txtDescricaoMeta.Text = registro.DescricaoMeta;
                    ddlArea.SelectedValue = registro.CodigoUnidade.ToString();

                    if (UsuarioAdministrador)
                    {
                        btnSalvar.Visible = !(registro.CodigoFaseExercicio == (int)FaseEnum.Encerrado);
                    }
                    else if (UsuarioCadastrador)
                    {
                        btnSalvar.Visible = registro.CodigoFaseExercicio == (int)FaseEnum.Planejamento && registro.StatusMeta.CodigoSeqStatusMeta == (int)StatusMetaEnum.EmCadastro;
                    }

                    Recursos = registro.Recursos.ToList();
                    gdvRecursos.DataSource = registro.Recursos;
                    gdvRecursos.DataBind();
                }
                else if (CodigoSeqMetaParameter > 0)
                {
                    var meta = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterMeta(CodigoSeqMetaParameter);
                    if (meta == null)
                        WebHelper.Redirect("~/Site/Meta/Consultar.aspx");

                    ddlArea.SelectedValue = meta.CorporativoUnidade.CodigoUnidade.ToString();
                    txtDescricaoMeta.Text = meta.DescricaoMeta;
                    if (meta.StatusMeta != null)
                    {
                        if (UsuarioAdministrador)
                        {
                            btnSalvar.Visible = !(meta.CodigoFaseExercicio == (int)FaseEnum.Encerrado);
                        }
                        else if (UsuarioCadastrador)
                        {
                            btnSalvar.Visible = meta.CodigoFaseExercicio == (int)FaseEnum.Planejamento && meta.StatusMeta.CodigoSeqStatusMeta == (int)StatusMetaEnum.EmCadastro;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}