﻿<%@ Page Title="SAFIRA - Acao" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false"
    CodeBehind="Detalhar.aspx.cs" Inherits="PGA.Presentation.Site.Meta.Acao.Detalhar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnSalvar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Atividade</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="ddlArea" Text="Área" runat="server" />
                                    <asp:LinkButton ID="lnkAjudaArea" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaArea" ForeColor="#034623" />
                                    <asp:DropDownList ID="ddlArea" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoMeta" AssociatedControlID="txtDescricaoMeta" Text="Meta" runat="server" />
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaDescricaoMeta" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDescricaoMeta" runat="server" ToolTip="Descrição da Meta" CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoAtividadeAcao" AssociatedControlID="txtDescricaoAtividadeAcao" Text="Descrição da Atividade*" runat="server" />
                                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaDescricaoAtividade" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDescricaoAtividadeAcao" runat="server" ToolTip="DescricaoAtividadeAcao" CssClass="form-control" MaxLength="200" />
                                    <asp:RequiredFieldValidator ID="rfvDescricaoAtividadeAcao" runat="server" Display="None" InitialValue="" ControlToValidate="txtDescricaoAtividadeAcao" ErrorMessage="Por gentileza preencha o campo Atividade obrigatório." ValidationGroup="Salvar" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoEstrategiaAcao" AssociatedControlID="txtDescricaoEstrategiaAcao" Text="Estratégia*" runat="server" />
                                    <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaEstrategia" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDescricaoEstrategiaAcao" runat="server" ToolTip="DescricaoEstrategiaAcao" CssClass="form-control" MaxLength="800" />
                                    <asp:RequiredFieldValidator ID="rfvDescricaoEstrategiaAcao" runat="server" Display="None" InitialValue="" ControlToValidate="txtDescricaoEstrategiaAcao" ErrorMessage="Por gentileza preencha o campo Estratégia obrigatório." ValidationGroup="Salvar" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoResponsavel" AssociatedControlID="txtDescricaoResponsavel" Text="Responsável*" runat="server" />
                                    <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaResponsavel" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDescricaoResponsavel" runat="server" ToolTip="DescricaoResponsavel" CssClass="form-control" MaxLength="200" />
                                    <asp:RequiredFieldValidator ID="rfvDescricaoResponsavel" runat="server" Display="None" InitialValue="" ControlToValidate="txtDescricaoResponsavel" ErrorMessage="Por gentileza preencha o campo Responsável obrigatório." ValidationGroup="Salvar" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblDataInicioAcao" AssociatedControlID="txtDataInicioAcao" Text="Data Início" runat="server" />
                                    <asp:LinkButton ID="LinkButton5" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaDataInicio" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDataInicioAcao" runat="server" ToolTip="Data Início da atividade" CssClass="form-control" />
                                    <asp:CompareValidator ErrorMessage="A data de início deve ser anterior a data final" ControlToValidate="txtDataInicioAcao" ForeColor="Red" Operator="LessThanEqual" Type="Date" ControlToCompare="txtDataFimAcao" runat="server" />
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblDataFimAcao" AssociatedControlID="txtDataFimAcao" Text="Data Fim" runat="server" />
                                    <asp:LinkButton ID="LinkButton6" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaDataFim" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDataFimAcao" runat="server" ToolTip="Data Fim da atividade" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblPesoAtividade" AssociatedControlID="ddlPesoAtividade" Text="Peso*" runat="server" />
                                    <asp:LinkButton ID="LinkButton7" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaPesoAtividade" ForeColor="#034623" />
                                    <asp:DropDownList ID="ddlPesoAtividade" runat="server" CssClass="form-control" ToolTip="Peso da atividade">
                                        <asp:ListItem Text="Selecione" Value="" />
                                        <asp:ListItem Text="1" Value="1" />
                                        <asp:ListItem Text="2" Value="2" />
                                        <asp:ListItem Text="3" Value="3" />
                                        <asp:ListItem Text="4" Value="4" />
                                        <asp:ListItem Text="5" Value="5" />
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvPesoAtividade" runat="server" Display="None" InitialValue="" ControlToValidate="ddlPesoAtividade" ErrorMessage="Por gentileza preencha o campo Peso da Atividade obrigatório." ValidationGroup="Salvar" />
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Recursos
                                    <asp:LinkButton ID="LinkButton8" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaRecursos" ForeColor="#034623" />
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-4 col-sm-4">
                                        <div class="form-group">
                                            <asp:Label ID="lblTipoRecurso" AssociatedControlID="ddlTipoRecurso" Text="Tipo Recurso*" runat="server" />
                                            <asp:LinkButton ID="LinkButton9" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaTipoRecurso" ForeColor="#034623" />
                                            <asp:DropDownList ID="ddlTipoRecurso" runat="server" CssClass="form-control" ToolTip="Tipo de Recurso"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvTipoRecurso" runat="server" Display="None" InitialValue="" ControlToValidate="ddlTipoRecurso" ErrorMessage="Por gentileza preencha o campo Tipo de Recurso obrigatório." ValidationGroup="Adicionar" />
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4">
                                        <div class="form-group">
                                            <asp:Label ID="lblNomeRecurso" AssociatedControlID="txtNomeRecurso" Text="Descrição do Recurso*" runat="server" />
                                            <asp:LinkButton ID="LinkButton10" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaDescricaoRecurso" ForeColor="#034623" />
                                            <asp:TextBox ID="txtNomeRecurso" runat="server" ToolTip="Descrição da Meta" CssClass="form-control" MaxLength="100" />
                                            <asp:RequiredFieldValidator ID="rfvNomeRecurso" runat="server" Display="None" ControlToValidate="txtNomeRecurso" ErrorMessage="Por gentileza preencha o campo Recurso obrigatório." ValidationGroup="Adicionar" />
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4">
                                        <div class="form-group">
                                            <asp:Label ID="lblQuantidadeRecurso" AssociatedControlID="txtQuantidadeRecurso" Text="Quantidade Recurso*" runat="server" />
                                            <asp:LinkButton ID="LinkButton11" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaQuantidadeRecurso" ForeColor="#034623" />
                                            <asp:TextBox ID="txtQuantidadeRecurso" runat="server" ToolTip="Quantidade do Recurso" CssClass="form-control" Style="text-align: right" />
                                            <asp:RequiredFieldValidator ID="rfvQuantidadeRecurso" runat="server" Display="None" ControlToValidate="txtQuantidadeRecurso" ErrorMessage="Por gentileza preencha o campo Quantidade Recurso obrigatório." ValidationGroup="Adicionar" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row pull-right">
                                    <div class="col-xs-12">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:LinkButton ID="btnAdicionar" runat="server" SkinID="btnAdicionar" CausesValidation="false" ValidationGroup="Adicionar" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <asp:UpdatePanel ID="upPnlRecursos" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="area-table">
                                                    <asp:GridView ID="gdvRecursos" runat="server"
                                                        Width="100%" AutoGenerateColumns="False"
                                                        EmptyDataText="Nenhum registro encontrado.">
                                                        <EmptyDataTemplate>Nenhum registro encontrado.</EmptyDataTemplate>
                                                        <AlternatingRowStyle CssClass="alternate" />
                                                        <Columns>
                                                            <asp:BoundField HeaderText="Tipo Recurso" DataField="DescricaoTipoRecurso" SortExpression="DescricaoTipoRecurso" />
                                                            <asp:BoundField HeaderText="Recurso" DataField="DescricaoRecurso" SortExpression="DescricaoRecurso" />
                                                            <asp:TemplateField HeaderText="Quantidade" ItemStyle-HorizontalAlign="Right" SortExpression="QuantidadeRecurso">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblQuantidade" runat="server" Text='<%# string.Format("{0:N2}", Convert.ToDecimal(Eval("QuantidadeRecurso"))) %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Excluir" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkExcluir" runat="server" CommandArgument='<%# String.Format("{0}-{1}-{2}", Eval("CodigoSeqTipoRecurso"), Eval("DescricaoRecurso"), Eval("QuantidadeRecurso")) %>'
                                                                        CommandName="Excluir" CausesValidation="false" CssClass="glyphicon glyphicon-remove" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="50px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div class="paginator">
                                                    <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnAdicionar" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="updBotoes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnSalvar" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="Salvar" />
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaArea" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Área</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Área da meta.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaDescricaoMeta" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Meta</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Descrição da meta.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaDescricaoAtividade" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Atividade</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        O que será feito.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaEstrategia" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Estratégia</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Como será feito.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaResponsavel" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Responsável</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Quem (ator ou atores) irá viabilizar a atividade.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaDataInicio" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Data Início</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Quando será iniciada e concluída a atividade.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaDataFim" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Data Fim</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Quando será iniciada e concluída a atividade.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaPesoAtividade" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Peso</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <p>Representa o valor de um (01) a cinco (05), atribuído para cada atividade do Plano de Ação, a critério da unidade organizacional, envolvendo recurso utilizado na ação, prazo, esforço e atividade direcionada ao cidadão/usuário. Este peso será utilizado para o cálculo do percentual geral de execução do plano de ação da meta.</p>
                                        <p>Exemplo: Um plano de ação tem três atividades, sendo que as duas primeiras estão concluídas (100%) e a terceira não (0%) -> Se todas tiverem o mesmo peso, o percentual de execução do plano de ação da meta é 66,66%. No entanto, se as duas primeiras têm peso 1 e a terceira peso 2, o percentual de execução do plano de ação da meta será de 50%.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaRecursos" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Recursos</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Insumos Logísticos, Tecnológicos, Humanos ou Financeiros necessários à execução da atividade.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaTipoRecurso" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tipo do Recurso</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Tipo do recurso.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaDescricaoRecurso" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Descrição de Recursos</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Descrever o que representa o recurso que será utilizado.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaQuantidadeRecurso" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Quantidade de Recursos</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Inserir o recurso que será utilizado para realizar a atividade.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtDataInicioAcao.ClientID %>").datepicker();
            $("#<%= txtDataInicioAcao.ClientID %>").mask("99/99/9999");

            $("#<%= txtDataFimAcao.ClientID %>").datepicker();
            $("#<%= txtDataFimAcao.ClientID %>").mask("99/99/9999");

            $("#<%= txtQuantidadeRecurso.ClientID %>").mask("000.000.000.000,00", { reverse: true });
        });
    </script>
</asp:Content>
