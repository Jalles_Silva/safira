﻿<%@ Page Title="SAFIRA - Consultar Acao" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false"
    CodeBehind="Consultar.aspx.cs" Inherits="PGA.Presentation.Site.Meta.Acao.Consultar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Consultar Atividades
                            <asp:LinkButton ID="lnkAjudaAtividade" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaAtividade" ForeColor="#034623" />
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-12">
                                    <asp:HyperLink ID="btnNovo" runat="server" SkinID="btnNovo" CausesValidation="false" />
                                    <asp:LinkButton ID="btnConcluir" runat="server" SkinID="btnConcluir" CausesValidation="false" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="txtDescricaoMeta" runat="server" ToolTip="Descrição da Meta" />
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="upPnlResultadoConsulta" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="area-table">
                                    <asp:GridView ID="gdvResultadoConsulta" runat="server" AllowSorting="True"
                                        Width="100%" AutoGenerateColumns="False"
                                        EmptyDataText="Nenhum registro encontrado.">
                                        <EmptyDataTemplate>Nenhum registro encontrado.</EmptyDataTemplate>
                                        <AlternatingRowStyle CssClass="alternate" />
                                        <Columns>
                                            <asp:BoundField HeaderText="Atividade" DataField="DescricaoAtividadeAcao" SortExpression="DescricaoAtividadeAcao" />
                                            <asp:BoundField HeaderText="Estratégia" DataField="DescricaoEstrategiaAcao" SortExpression="DescricaoEstrategiaAcao" />
                                            <asp:BoundField HeaderText="Responsável" DataField="DescricaoResponsavel" SortExpression="DescricaoResponsavel" />
                                            <asp:BoundField HeaderText="Data Início" DataField="DataInicioAcao" SortExpression="DataInicioAcao" DataFormatString="{0:dd/MM/yyyy}" />
                                            <asp:BoundField HeaderText="Data Fim" DataField="DataFimAcao" SortExpression="DataFimAcao" DataFormatString="{0:dd/MM/yyyy}" />
                                            <asp:TemplateField HeaderText="Status" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                       <%# (int?)Eval("StatusAcao") == 1 ? "Ativo" : "Cancelado" %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Alterar" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="lnkDetalhar" runat="server" Target="_self" NavigateUrl='<%# String.Format("~/Site/Meta/Acao/Detalhar.aspx?CodigoSeqAcao={0}&CodigoSeqMeta={1}", Eval("CodigoSeqAcao"), Eval("CodigoSeqMeta")) %>'
                                                        CommandName="Detalhar" CausesValidation="false" CssClass="glyphicon glyphicon-pencil" />
                                                </ItemTemplate>
                                                <ItemStyle Width="50px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Excluir" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkExcluir" runat="server" CommandArgument='<%# Eval("CodigoSeqAcao") %>'
                                                        CommandName="Excluir" CausesValidation="false" CssClass="glyphicon glyphicon-remove" />
                                                </ItemTemplate>
                                                <ItemStyle Width="50px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cancelar/Ativar" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkCancelar" runat="server" CommandArgument='<%# Eval("CodigoSeqAcao") %>'
                                                        CommandName="Cancelar" CausesValidation="false" CssClass="glyphicon glyphicon-remove-sign"
                                                        Style="color:red;" Visible='<%# (int?)Eval("StatusAcao") == 1 ? true : false  %>'/>
                                                    <asp:LinkButton ID="lnkAtivar" runat="server" CommandArgument='<%# Eval("CodigoSeqAcao") %>'
                                                        CommandName="Ativar" CausesValidation="false" CssClass="glyphicon glyphicon-ok-sign"
                                                        Style="color:green;" Visible='<%# (int?)Eval("StatusAcao") == 1 ? false : true  %>'/>
                                                </ItemTemplate>
                                                <ItemStyle Width="50px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <div class="paginator">
                                    <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
             <div id="modalAjudaAtividade" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Lista de Atividade</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <p>Esta é a lista de atividades, inspiradas na metodologia 5w2h, que serão desenvolvidas para o atingimento da meta.</p>
                                        <p>Para incluir uma ação, clique no botão 'Novo'.</p>
                                        <p>Para excluir ou alterar, escolha uma das opções ao lado de cada meta.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
