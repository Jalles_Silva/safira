﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using PGA.Common;

namespace PGA.Presentation.Site.Meta.Indicador
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqIndicadorParameter
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqIndicador").ToInt32();
            }
        }

        private int CodigoSeqMetaParameter
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqMeta").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
            ddlTipoValor.SelectedIndexChanged += ddlTipoValor_SelectedIndexChanged;
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Meta/Indicador/Consultar.aspx?CodigoSeqMeta={0}", CodigoSeqMetaParameter));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarControles();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Salvar":
                    if (e.Result == MessageBoxResult.Yes)
                        Salvar();
                    break;
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            PreSalvar();
        }

        #endregion

        #region [Methods]

        private void CarregarControles()
        {
            try
            {
                if (CodigoSeqMetaParameter == 0 && CodigoSeqIndicadorParameter == 0)
                    WebHelper.Redirect("~/Site/Meta/Consultar.aspx");
                CarregarTipoIndicador();
                CarregarTipoValor();
                CarregarUnidade();
                CarregarTipoResultado();
                CarregarRegistro();
      
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarUnidade()
        {
            ddlArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarCorporativoUnidadeAtivo();
            ddlArea.DataTextField = "DescricaoUnidade";
            ddlArea.DataValueField = "CodigoUnidade";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void CarregarTipoIndicador()
        {
            ddlTipoIndicador.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTipoIndicadores();
            ddlTipoIndicador.DataTextField = "DescricaoTipoIndicador";
            ddlTipoIndicador.DataValueField = "CodigoSeqTipoIndicador";
            ddlTipoIndicador.DataBind();
            ddlTipoIndicador.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void CarregarTipoValor()
        {
            ddlTipoValor.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTipoValores();
            ddlTipoValor.DataTextField = "DescricaoTipo";
            ddlTipoValor.DataValueField = "CodigoSeqTipoValor";
            ddlTipoValor.DataBind();
            //ddlTipoValor.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void CarregarTipoResultado()
        {
            ddlTipoResultado.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTipoResultado();
            ddlTipoResultado.DataTextField = "Descricao";
            ddlTipoResultado.DataValueField = "Codigo";
            ddlTipoResultado.DataBind();
            ddlTipoResultado.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void PreSalvar()
        {
            Validate();

            DateTime? DataAlvo = txtDataAlvo.Text.ToNullableDateTime("MM/yyyy", null);
            DateTime? MesReferencia = txtMesReferencia.Text.ToNullableDateTime("MM/yyyy", null);


            if (DataAlvo != null && !ValidaData(DataAlvo.ToString()))
            {
                MessageBox.ShowInformationMessage("Data alvo inválida", "Erro");
                return;
            }

            if (MesReferencia != null && !ValidaData(MesReferencia.ToString()))
            {
                MessageBox.ShowInformationMessage("Mês de Referência da Linha de Base inválido", "Erro");
                return;
            }

            if (IsValid)
            {
                MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Salvar",
                    "Deseja realmente salvar?", MessageBoxButtons.YesNo, "Salvar");
            }
        }

        private void Salvar()
        {
            try
            {
                decimal a = txtValorAlvo.Text.ToDecimal();

                DTOIndicador registro = new DTOIndicador();
                registro.CodigoSeqIndicador = CodigoSeqIndicadorParameter;
                registro.NoIndicador = txtNoIndicador.Text;
                registro.DescricaoIndicador = txtDescricaoIndicador.Text;
                registro.DescricaoFormula = txtDescricaoFormula.Text;
                registro.CodigoSeqTipoIndicador = ddlTipoIndicador.SelectedValue.ToInt32();
                registro.CodigoSeqTipoValor = ddlTipoValor.SelectedValue.ToInt32();
                registro.CodigoSeqMeta = CodigoSeqMetaParameter;
                registro.DataAlvo = txtDataAlvo.Text.ToDateTime("MM/yyyy", null);
                registro.ObjetivoEstrategico = Boolean.Parse(rblObjetivo.SelectedValue);
                registro.StatusIndicador = 1;

                if (ddlTipoValor.SelectedValue == "2")
                {
                    registro.ValorAlvo = Convert.ToDecimal(txtValorAlvoPercentual.Text.Replace("%", ""));
                    registro.ValorLinhaBase = Convert.ToDecimal(txtValorLinhabasePercentual.Text.Replace("%", ""));
                }
                else
                {
                    registro.ValorAlvo = txtValorAlvo.Text.ToDecimal();
                    registro.ValorLinhaBase = txtValorLinhabase.Text.ToDecimal();
                }

                registro.MesReferencia = txtMesReferencia.Text.ToDateTime("MM/yyyy", null);
                registro.TipoResultado = new DTOTipoResultado()
                {
                    Codigo = ddlTipoResultado.SelectedValue.ToInt32()
                };

                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarIndicador(registro);

                WebHelper.Redirect(String.Format("~/Site/Meta/Indicador/Consultar.aspx?CodigoSeqMeta={0}", CodigoSeqMetaParameter));
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqIndicadorParameter > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterIndicador(CodigoSeqIndicadorParameter);

                    txtNoIndicador.Text = registro.NoIndicador;
                    txtDescricaoIndicador.Text = registro.DescricaoIndicador;
                    txtValorLinhabase.Text = registro.ValorLinhaBase.ToString();
                    txtDescricaoFormula.Text = registro.DescricaoFormula;
                    ddlTipoIndicador.SelectedValue = registro.CodigoSeqTipoIndicador.ToString();
                    ddlTipoValor.SelectedValue = registro.CodigoSeqTipoValor.ToString();
                    txtDescricaoMeta.Text = registro.DescricaoMeta;
                    ddlArea.SelectedValue = registro.CodigoUnidade.ToString();
                    ddlTipoResultado.SelectedValue = registro.TipoResultado.Codigo.ToString();
                    rblObjetivo.SelectedValue = registro.ObjetivoEstrategico.ToString();

                    if (registro.DataAlvo.HasValue)
                        txtDataAlvo.Text = registro.DataAlvo.Value.ToString("MM/yyyy");

                    string valorTratado = ApresentaValores(registro.ValorAlvo, (int)registro.CodigoSeqTipoValor, false).ToString();
                    string valorTratadoLinhaBase = ApresentaValores(registro.ValorLinhaBase, (int)registro.CodigoSeqTipoValor, false).ToString();

                    if (registro.CodigoSeqTipoValor == 2) //Percentual
                    {
                        ControleValoresPercentual(valorTratado, valorTratadoLinhaBase);
                    }
                    else
                    { //Absoluto
                        ControleValoresAbsouto(valorTratado, valorTratadoLinhaBase);
                    }

                    if (registro.MesReferencia.HasValue)
                        txtMesReferencia.Text = registro.MesReferencia.Value.ToString("MM/yyyy");

                    if (UsuarioAdministrador)
                    {
                        rblObjetivo.Enabled = true;
                        btnSalvar.Visible = !(registro.CodigoFaseExercicio == (int)FaseEnum.Encerrado);
                    }
                    else if (UsuarioCadastrador)
                    {
                        btnSalvar.Visible = registro.CodigoFaseExercicio == (int)FaseEnum.Planejamento && registro.StatusMeta.CodigoSeqStatusMeta == (int)StatusMetaEnum.EmCadastro;
                    }
                }
                else if (CodigoSeqMetaParameter > 0)
                {
                    var meta = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterMeta(CodigoSeqMetaParameter);
                    if (meta == null)
                        WebHelper.Redirect("~/Site/Meta/Consultar.aspx");
                    else
                    {
                        ddlArea.SelectedValue = meta.CorporativoUnidade.CodigoUnidade.ToString();
                        txtDescricaoMeta.Text = meta.DescricaoMeta;
                        if (meta.StatusMeta != null)
                        {
                            if (UsuarioAdministrador)
                            {
                                rblObjetivo.Enabled = true;
                                btnSalvar.Visible = !(meta.CodigoFaseExercicio == (int)FaseEnum.Encerrado);
                            }
                            else if (UsuarioCadastrador)
                            {
                                btnSalvar.Visible = meta.CodigoFaseExercicio == (int)FaseEnum.Planejamento && meta.StatusMeta.CodigoSeqStatusMeta == (int)StatusMetaEnum.EmCadastro;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void ddlTipoValor_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTipoValor.SelectedValue == "1") //Absoluto
            {
                var valorAlvo = Convert.ToDecimal(txtValorAlvoPercentual.Text.Replace("%", "")).ToString();
                var valorLinhaBase = Convert.ToDecimal(txtValorLinhabasePercentual.Text.Replace("%", "")).ToString();

                ControleValoresAbsouto(valorAlvo, valorLinhaBase);
            }
            else if (ddlTipoValor.SelectedValue == "2") //Percentual
            {
                var valorAlvo = (txtValorAlvo.Text.ToDecimal() > (decimal)9999.99 ? (decimal)0 : txtValorAlvo.Text.ToDecimal()).ToString();
                var valorLinhaBase = (txtValorLinhabase.Text.ToDecimal() > (decimal)9999.99 ? (decimal)0 : txtValorLinhabase.Text.ToDecimal()).ToString();

                ControleValoresPercentual(valorAlvo, valorLinhaBase);
            }
            }

        private void ControleValoresAbsouto(string ValorAlvo, string ValorLinhaBase)
        {
            //CampoValorAlvo
            txtValorAlvo.Visible = true;
            txtValorAlvo.Text = ValorAlvo;

            txtValorAlvoPercentual.Visible = false;
            txtValorAlvoPercentual.Text = "0";

            //CampoLinhaBase
            txtValorLinhabase.Visible = true;
            txtValorLinhabase.Text = ValorLinhaBase;

            txtValorLinhabasePercentual.Visible = false;
            txtValorLinhabasePercentual.Text = "0";
        }

        private void ControleValoresPercentual(string ValorAlvo, string ValorLinhaBase)
        {
            //CampoValorAlvo
            txtValorAlvo.Visible = false;
            txtValorAlvo.Text = "0";

            txtValorAlvoPercentual.Visible = true;
            txtValorAlvoPercentual.Text = ValorAlvo;

            //CampoLinhaBase
            txtValorLinhabase.Visible = false;
            txtValorLinhabase.Text = "0";

            txtValorLinhabasePercentual.Visible = true;
            txtValorLinhabasePercentual.Text = ValorLinhaBase;
        }
        #endregion
    }
}