﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using SQFramework.Core.Reflection;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;

namespace PGA.Presentation.Site.Meta.Indicador
{
    public partial class Consultar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqMetaParameter
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqMeta").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnConcluir.Click += new EventHandler(btnConcluir_Click);
            gdvResultadoConsulta.RowCommand += new GridViewCommandEventHandler(gdvResultadoConsulta_RowCommand);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            gdvResultadoConsulta.Sorting += new GridViewSortEventHandler(gdvResultadoConsulta_Sorting);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarControles();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Remover":
                    if (e.Result == MessageBoxResult.Yes)
                        Remover(e.Parameters["CodigoSeqIndicador"].ToInt32());
                    break;

                case "Recarregar":
                    if (e.Result == MessageBoxResult.OK)
                        ConsultarDados(0, "NoIndicador", true);
                    break;
                case "Cancelar":
                    if (e.Result == MessageBoxResult.Yes)
                        Cancelar(e.Parameters["CodigoSeqIndicador"].ToInt32(), e.Parameters["StatusIndicador"].ToInt32());
                    break;
            }
        }

        protected void btnConcluir_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect("~/Site/Meta/Detalhar.aspx?CodSeqMeta=" + CodigoSeqMetaParameter);
        }

        protected void gdvResultadoConsulta_Sorting(object sender, GridViewSortEventArgs e)
        {
            bool orderAscending = true;

            if (gdvResultadoConsulta.Attributes["SortExpression"] == e.SortExpression)
                orderAscending = !gdvResultadoConsulta.Attributes["SortAscending"].ToBoolean();

            ConsultarDados(ucPaginatorConsulta.PageIndex - 1, e.SortExpression, orderAscending);
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            ConsultarDados(e.NewPage - 1, gdvResultadoConsulta.Attributes["SortExpression"], gdvResultadoConsulta.Attributes["SortAscending"].ToBoolean());
        }

        protected void gdvResultadoConsulta_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Excluir"))
            {
                var parametros = new Dictionary<string, object>();
                parametros.Add("CodigoSeqIndicador", e.CommandArgument);

                MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente excluir o registro?",
                    "Deseja realmente excluir o registro?", MessageBoxButtons.YesNo, "Remover", parametros);
            }
            if (e.CommandName.Equals("Cancelar"))
            {
                var parametros = new Dictionary<string, object>();
                parametros.Add("CodigoSeqIndicador", e.CommandArgument);
                parametros.Add("StatusIndicador", e.CommandArgument);

                MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente cancelar o registro?",
                    "Deseja realmente cancelar o registro?", MessageBoxButtons.YesNo, "Cancelar", parametros);
            }
            if (e.CommandName.Equals("Ativar"))
            {
                var parametros = new Dictionary<string, object>();
                parametros.Add("CodigoSeqIndicador", e.CommandArgument);
                parametros.Add("StatusIndicador", e.CommandArgument);

                MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente ativar o registro?",
                    "Deseja realmente ativar o registro?", MessageBoxButtons.YesNo, "Cancelar", parametros);
            }
        }

        #endregion

        #region [Methods]

        private void ConsultarDados(int pageIndex, string sortExpression, bool sortAscending)
        {
            Validate();

            if (IsValid)
            {
                try
                {
                    var dadosPesquisados = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService")
                        .ListarIndicadoresPorMeta(CodigoSeqMetaParameter, pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize, sortExpression, sortAscending);

                    gdvResultadoConsulta.DataSource = dadosPesquisados.Entities.ToList();
                    gdvResultadoConsulta.DataBind();

                    gdvResultadoConsulta.Attributes["SortExpression"] = sortExpression;
                    gdvResultadoConsulta.Attributes["SortAscending"] = sortAscending.ToString();

                    ucPaginatorConsulta.Visible = (dadosPesquisados.RowsCount > 0);
                    ucPaginatorConsulta.TotalRecords = dadosPesquisados.RowsCount;
                    ucPaginatorConsulta.PageIndex = pageIndex + 1;
                    ucPaginatorConsulta.DataBind();
                    upPnlResultadoConsulta.Update();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        private void Remover(int codigoSeqIndicador)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarIndicador(codigoSeqIndicador);

                MessageBox.ShowInformationMessage("Registro excluído com sucesso.", "Recarregar");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void Cancelar(int codigoSeqIndicador, int statusIndicador)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").AlterarStatusIndicador(codigoSeqIndicador, statusIndicador);

                MessageBox.ShowInformationMessage("Registro alterado com sucesso.", "Recarregar");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarControles()
        {
            if (CodigoSeqMetaParameter == 0)
                WebHelper.Redirect("~/Site/Meta/Consultar.aspx");

            var meta = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterMeta(CodigoSeqMetaParameter);
            if (meta == null)
                WebHelper.Redirect("~/Site/Meta/Consultar.aspx");
            txtDescricaoMeta.Text = meta.DescricaoMeta;

            btnNovo.NavigateUrl = String.Format("Detalhar.aspx?CodigoSeqMeta={0}", CodigoSeqMetaParameter);
            ConsultarDados(0, "NoIndicador", true);
        }

        #endregion
    }
}