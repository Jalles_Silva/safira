﻿<%@ Page Title="SAFIRA - Indicador" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false"
    CodeBehind="Detalhar.aspx.cs" Inherits="PGA.Presentation.Site.Meta.Indicador.Detalhar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnSalvar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Indicadores de Contribuição</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="ddlArea" Text="Área" runat="server" />
                                    <asp:LinkButton ID="lnkAjudaArea" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaArea" ForeColor="#034623" />
                                    <asp:DropDownList ID="ddlArea" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoMeta" AssociatedControlID="txtDescricaoMeta" Text="Meta" runat="server" />
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaDescricaoMeta" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDescricaoMeta" runat="server" ToolTip="Descrição da Meta" CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblNoIndicador" AssociatedControlID="txtNoIndicador" Text="Indicador*" runat="server" />
                                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaIndicador" ForeColor="#034623" />
                                    <asp:TextBox ID="txtNoIndicador" runat="server" ToolTip="NoIndicador" CssClass="form-control" MaxLength="500" />
                                    <asp:RequiredFieldValidator ID="rfvNoIndicador" runat="server" Display="None" ControlToValidate="txtNoIndicador" ErrorMessage="Por gentileza preencha o campo Indicador obrigatório." ValidationGroup="Salvar" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoIndicador" AssociatedControlID="txtDescricaoIndicador" Text="Descrição*" runat="server" />
                                    <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaDescricaoIndicador" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDescricaoIndicador" runat="server" ToolTip="DescricaoIndicador" CssClass="form-control" MaxLength="1000" />
                                    <asp:RequiredFieldValidator ID="rfvDescricaoIndicador" runat="server" Display="None" ControlToValidate="txtDescricaoIndicador" ErrorMessage="Por gentileza preencha o campo Descrição obrigatório." ValidationGroup="Salvar" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8 col-sm-8">
                                <div class="form-group">
                                    <asp:Label ID="lblValorLinhabase" AssociatedControlID="txtValorLinhabase" Text="Linha de Base*" runat="server" />
                                    <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaLinhaBase" ForeColor="#034623" />
                                    
                                    <asp:TextBox ID="txtValorLinhabase" runat="server" ToolTip="Valor Linha Base" CssClass="form-control" Style="text-align: right"/>
                                    <asp:RequiredFieldValidator ID="rfvValoLinhabase" runat="server" Display="None" ControlToValidate="txtValorLinhabase" ErrorMessage="Por gentileza preencha o campo Linha de Base obrigatório." ValidationGroup="Salvar" />

                                     <asp:TextBox ID="txtValorLinhabasePercentual" runat="server" ToolTip="Valor Linha Base" CssClass="form-control" Visible="false" Text="0" Style="text-align: right"/>
                                    <asp:RequiredFieldValidator ID="rfvValorLinhabasePercentual" runat="server" Display="None" ControlToValidate="txtValorLinhabasePercentual" ErrorMessage="Por gentileza preencha o campo Linha de Base obrigatório." ValidationGroup="Salvar" />
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblMesReferencia" AssociatedControlID="txtMesReferencia" Text="Mês de Referência da Linha de Base" runat="server" />
                                    <asp:LinkButton ID="LinkButton5" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaMesReferencia" ForeColor="#034623" />
                                    <asp:TextBox ID="txtMesReferencia" runat="server" ToolTip="Mês de Referência" CssClass="form-control mesReferencia" />
                                    <asp:RequiredFieldValidator ID="rfvMesReferencia" runat="server" Display="None" ControlToValidate="txtMesReferencia" ErrorMessage="Por gentileza preencha o campo Mês de Referência obrigatório." ValidationGroup="Salvar" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-10 col-sm-10">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoFormula" AssociatedControlID="txtDescricaoFormula" Text="Fórmula" runat="server" />
                                    <asp:LinkButton ID="LinkButton6" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaFormula" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDescricaoFormula" runat="server" ToolTip="Descricao Fórmula" CssClass="form-control" MaxLength="1000" />
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblObjetivo" AssociatedControlID="rblObjetivo" Text="Indicador Objetivo Estratégico" runat="server" />
                                    <asp:LinkButton ID="lnkObjetivo" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaObjetivo" ForeColor="#034623" />
                                    <asp:RadioButtonList ID="rblObjetivo" runat="server" RepeatDirection="Horizontal" CssClass="radioListHoriz" Enabled="false">
                                        <asp:ListItem Value="True" Text="Sim"></asp:ListItem>
                                        <asp:ListItem Value="False" Text="Não" Selected="True"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblValorAlvo" AssociatedControlID="txtValorAlvo" Text="Valor Alvo" runat="server" />
                                    <asp:LinkButton ID="LinkButton7" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaValorAlvo" ForeColor="#034623" />
                                    
                                    <asp:TextBox ID="txtValorAlvo" runat="server" ToolTip="Valor Alvo" CssClass="form-control" Style="text-align: right"/>
                                    <asp:RequiredFieldValidator ID="rfvValorAlvo" runat="server" Display="None" ControlToValidate="txtValorAlvo" ErrorMessage="Por gentileza preencha o campo Valor Alvo obrigatório." ValidationGroup="Salvar" />

                                    <asp:TextBox ID="txtValorAlvoPercentual" runat="server" ToolTip="Valor Alvo" CssClass="form-control" Visible="false" Text="0" Style="text-align: right" />
                                    <asp:RequiredFieldValidator ID="rfvValorAlvoPercentual" runat="server" Display="None" ControlToValidate="txtValorAlvoPercentual" ErrorMessage="Por gentileza preencha o campo Valor Alvo obrigatório." ValidationGroup="Salvar" />
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDataAlvo" AssociatedControlID="txtDataAlvo" Text="Mês Alvo" runat="server" />
                                    <asp:LinkButton ID="LinkButton8" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaDataAlvo" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDataAlvo" runat="server" ToolTip="Mês Alvo" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="rfvDataAlvo" runat="server" Display="None" ControlToValidate="txtDataAlvo" ErrorMessage="Por gentileza preencha o campo Mês Alvo obrigatório." ValidationGroup="Salvar" />
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoValor" AssociatedControlID="ddlTipoValor" Text="Tipo Valor*" runat="server" />
                                    <asp:LinkButton ID="LinkButton9" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaTipoValor" ForeColor="#034623" />
                                    <asp:DropDownList ID="ddlTipoValor" runat="server" CssClass="form-control" ToolTip="Tipo de Valor" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoIndicador" AssociatedControlID="ddlTipoIndicador" Text="Tipo Indicador*" runat="server" />
                                    <asp:LinkButton ID="LinkButton10" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaTipoIndicador" ForeColor="#034623" />
                                    <asp:DropDownList ID="ddlTipoIndicador" runat="server" CssClass="form-control" ToolTip="Tipo de Indicador"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvTipoIndicador" runat="server" Display="None" InitialValue="" ControlToValidate="ddlTipoIndicador" ErrorMessage="Por gentileza preencha o campo Tipo de Indicador obrigatório." ValidationGroup="Salvar" />
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoResultado" AssociatedControlID="ddlTipoResultado" Text="Tipo de Resultado*" runat="server" />
                                    <asp:LinkButton ID="LinkButton11" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaTipoResultado" ForeColor="#034623" />
                                    <asp:DropDownList ID="ddlTipoResultado" runat="server" CssClass="form-control" ToolTip="Tipo de Resultado"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvTipoResultado" runat="server" Display="None" InitialValue="" ControlToValidate="ddlTipoResultado" ErrorMessage="Por gentileza preencha o campo Tipo de Resultado obrigatório." ValidationGroup="Salvar" />
                                </div>
                            </div>
                        </div>
                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="updBotoes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnSalvar" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="Salvar" />
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaArea" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Área</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Área da meta.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaDescricaoMeta" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Meta</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Descrição da meta.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaIndicador" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Indicador</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Neste campo deverá ser inserido o nome do indicador de contribuição da meta.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaDescricaoIndicador" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Descrição do Indicador</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <p>Neste campo deverá ser detalhado o que este indicador mede, reflete ou avalia e as principais formas de utilização dos dados.</p>
                                        <p>Exemplos: </p>
                                        <p>a) Este indicador mede o percentual das fiscalizações de cargas rodoviárias realizadas pela ANTT em relação ao total de fiscalizações de cargas rodoviárias planejadas. </p>
                                        <p>b) O indicador mede o grau de atendimento as competências, em termos de proficiência e dimensionamento, identificadas como necessárias para cada função mapeada das áreas.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaLinhaBase" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Linha Base</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <p>Aqui deverá ser informado o valor do indicador e o mês de referência, para se saber o referencial de partida da medição e a evolução deste ao longo do tempo. Pode ser número ou percentual. </p>
                                        <p>Exemplo:</p> 
                                        <p>a) 10.000 e 12/2016 (que seria, por exemplo, o número de fiscalizações realizadas pela ANTT em 2016, apurado em 12/2016, a ser utilizado como base para este indicador de 2017).</p>
                                        <p>b) 15% e 01/2017 (que seria, por exemplo, o número de rodovias sem pavimentação asfáltica em 01/2017 a ser utilizado como linha de base para este indicador de contribuição da meta.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaMesReferencia" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Mês de Referência</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <p>Aqui deverá ser informado o valor do indicador e o mês de referência, para se saber o referencial de partida da medição e a evolução deste ao longo do tempo. Pode ser número ou percentual. </p>
                                        <p>Exemplo:</p> 
                                        <p>a) 10.000 e 12/2016 (que seria, por exemplo, o número de fiscalizações realizadas pela ANTT em 2016, apurado em 12/2016, a ser utilizado como base para este indicador de 2017).</p>
                                        <p>b) 15% e 01/2017 (que seria, por exemplo, o número de rodovias sem pavimentação asfáltica em 01/2017 a ser utilizado como linha de base para este indicador de contribuição da meta.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaFormula" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Fórmula</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <p>Neste campo deverá ser inserido o método de cálculo do indicador, com definição precisa dos elementos que o compõem, numerador e denominador. </p>
                                        <p>Exemplos:</p>
                                        <p>a)  ('Quantitativo de Competências Existentes' vezes o 'Grau de Proficiência da Competência') dividido por ('Quantitativo de Competências Necessárias' vezes o 'Grau de Proficiência da Competência')</p>
                                        <p>b) ('Número de Servidores com Titulação' vezes 100) dividido por 'Número Total de Servidores'</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaObjetivo" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Indicador Objetivo Estratégico</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Sinalize qual o objetivo estratégico vinculado ao indicador.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaValorAlvo" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Valor Alvo</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <p>Aqui deverá ser informado o valor para o indicador que pretende atingir e quando se pretende atingir este valor. Deve seguir o mesmo formato da linha de base e é muito importante que seja coerente com a descrição textual da meta. </p>
                                        <p>Exemplo: </p>
                                        <p>a) 12.000 e 12/2017 (supondo que sua meta seja aumentar em 20% até o final de 2017 o o número de fiscalizações realizadas pela ANTT).</p>
                                        <p>b) 7,5% e 09/2017 (comparando com o exemplo 'b' da linha de base, estes seriam os valores caso sua meta fosse, por exemplo, 'reduzir em 50% o percentual de rodovias sem pavimentação asfáltica até setembor de 2017).</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaDataAlvo" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Mês Alvo</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <p>Aqui deverá ser informado o valor para o indicador que pretende atingir e quando se pretende atingir este valor. Deve seguir o mesmo formato da linha de base e é muito importante que seja coerente com a descrição textual da meta. </p>
                                        <p>Exemplo: </p>
                                        <p>a) 12.000 e 12/2017 (supondo que sua meta seja aumentar em 20% até o final de 2017 o o número de fiscalizações realizadas pela ANTT).</p>
                                        <p>b) 7,5% e 09/2017 (comparando com o exemplo 'b' da linha de base, estes seriam os valores caso sua meta fosse, por exemplo, 'reduzir em 50% o percentual de rodovias sem pavimentação asfáltica até setembor de 2017).</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaTipoValor" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tipo Valor</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Tipo do Valor.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaTipoIndicador" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tipos de Indicadores</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <p>
                                                Indicadores de <strong>QUALIDADE (EFICÁCIA):</strong> Referem-se à capacidade da organização de satisfazer as necessidades das partes interessadas e tem foco nos resultados. Devem representar os atributos relativos aos produtos e serviços prestados pela organização e que são relevantes para os cidadãos/clientes, sociedade, servidores/funcionários, governo e outros. Também entendidos como o grau de alcance das metas programadas, em um determinado período de tempo, independentemente da utilização dos recursos empregados. Este conceito pode ser aplicado a diferentes contextos, a depender do que queremos medir. É fundamental saber quais são os requisitos do cidadão/cliente, principalmente em relação aos impactos dos resultados.<br />
                                                <strong>Exemplo:</strong>
                                <br />
                                                Resultado: as pessoas sentem-se razoavelmente seguras e, portanto, relativamente satisfeitas ao perceberem a presença de policiais pelas ruas (a meta anunciada pelo governante foi de colocar nas ruas 90% do efetivo de policiais em função burocrática nos quartéis).<br />
                                                Impacto: mais satisfeitas e seguras irão sentir-se ao perceberem que os índices de violência se reduziram sensivelmente.
                                            </p>
                                            <p>
                                                Indicadores de <strong>EFICIÊNCIA (PRODUTIVIDADE):</strong> Referem-se à utilização dos recursos disponíveis para a produção dos produtos ou execução dos serviços da organização; é o meio de fazer certo um processo correto de boa qualidade, em curto prazo, com o menor número de erros e otimizando os recursos.<br />
                                                <strong>Exemplo:</strong>
                                <br />
                                                Meta: Reduzir em 20% o custo da cesta básica, até dez 2015.
                                <br />
                                                Indicador: Percentual de redução de custo (Recurso: Financeiro)
                                <br />
                                                Fórmula: (&#39;Custo do ano anterior&#39; menos &#39;custo do ano corrente&#39;) dividido por &#39;Custo do ano anterior&#39;)
                                            </p>
                                            <p>
                                                Distinção entre Eficiência e Eficácia:
                                            </p>
                                            <table width="90%" border="1px">
                                                <tr>
                                                    <th>INDICADOR</th>
                                                    <th>EFICIÊNCIA</th>
                                                    <th>EFICÁCIA</th>
                                                </tr>
                                                <tr>
                                                    <td>Dizem respeito</td>
                                                    <td>ao modo de utilizar os recursos disponíveis</td>
                                                    <td>à satisfação dos cidadãos/clientes</td>
                                                </tr>
                                                   <tr>
                                                    <td>Medem</td>
                                                    <td>a eficiência dos processos</td>
                                                    <td>a eficácia e o impacto dos processos</td>
                                                </tr>
                                                   <tr>
                                                    <td>Tem foco</td>
                                                    <td>no esforço</td>
                                                    <td>nos resultados</td>
                                                </tr>
                                                   <tr>
                                                    <td>Indicam</td>
                                                    <td>como fazer</td>
                                                    <td>o que fazer</td>
                                                </tr>
                                                   <tr>
                                                    <td>Ensinam</td>
                                                    <td>fazer certo as coisas</td>
                                                    <td>fazer as coisas certas</td>
                                                </tr>
                                            </table>
                                            <p>
                                                &nbsp;
                                            </p>
                                            <p>
                                                Indicadores de <strong>CRESCIMENTO E INOVAÇÃO:</strong> Medem quão bem o processo utiliza seus recursos de forma a fazer a Organização crescer e disponibilizar novos serviços aos cidadãos/clientes.
                                            </p>
                                            <p>
                                                Indicadores de <strong>SATISFAÇÃO:</strong> Quantificam a percepção dos cidadãos/clientes em relação ao produto e/ou serviço fornecido/disponibilizado pelo processo.
                                            </p>
                                            <p>
                                                Indicadores de <strong>CONFORMIDADE:</strong> Quantificam a conformidade do serviço em relação a limites de aceitação estabelecidos para seus requisitos.
                                            </p>
                                            <p>
                                                Indicadores de <strong>ECONOMICIDADE:</strong> Dizem respeito à minimização dos custos de uma atividade, sem comprometimento dos padrões de qualidade. Referem-se à capacidade de uma instituição de gerir adequadamente os recursos financeiros à sua disposição. De acordo com o Tribunal de Contas da União, estes indicadores permitem também medir o custo dos insumos e os recursos alocados para a atividade.
                                            </p>
                                            <p>
                                                Indicadores de <strong>EFETIVIDADE:</strong> Definem a capacidade de se transformar uma realidade (impacto) a partir do objetivo estabelecido e sua continuidade ao longo do tempo.
                                            </p>
                                            <p>
                                                Indicadores de <strong>RESULTADOS:</strong> Utilizados para avaliar se a meta foi alcançada ou não e, portanto, são medidos ao final do prazo estabelecido para a meta.
                                            </p>
                                            <p>
                                                Indicadores de <strong>ALERTA:</strong> Utilizados durante o processo de monitoramento da meta para avaliar a evolução dos resultados obtidos e orientar o redirecionamento das ações. Algumas vezes os indicadores de resultado e de alerta são os mesmos, o que varia é a periodicidade de medição.
                                                <br />
                                                <strong>Exemplo:</strong>
                                                <br />
                                                Meta: atingir 95% de alunos aprovados por ano, até 2015.
                                                <br />
                                                Indicador de resultado: índice de alunos aprovados por ano.
                                                <br />
                                                Indicador de alerta: percentual de alunos com notas abaixo da média no bimestre.
                                            </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaTipoResultado" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tipo Resultado</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <p>Corresponde a forma de acompanhamento da evolução do indicador de contribuição: </p>
                                        <p>Acumulado: Indicador representado pela soma dos valores informados; </p>
                                        <p>Média: Média dos valores informados para o indicador.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= txtDataAlvo.ClientID %>").datepicker({ dateFormat: "mm/yy" });
            $("#<%= txtDataAlvo.ClientID %>").mask("00/0000", { placeholder: "" });

            $("#<%= txtMesReferencia.ClientID %>").datepicker({ dateFormat: "mm/yy" });
            $("#<%= txtMesReferencia.ClientID %>").mask("00/0000", { placeholder: "" });

            $("#<%= txtValorAlvo.ClientID %>").mask('000.000.000.000,00', { reverse: true });

            $("#<%= txtValorLinhabase.ClientID %>").mask('000.000.000.000,00', { reverse: true });

            $("#<%= txtValorLinhabasePercentual.ClientID %>").mask('0000,00%', { reverse: true, maxlength: true });

          <%--  $("#<%= txtValorLinhabasePercentual.ClientID %>").keyup(function () {
                calculaValorPercentual($(this), $("#<%= txtValorLinhabasePercentual.ClientID %>"));
            });--%>

            $("#<%= txtValorAlvoPercentual.ClientID %>").mask('0000,00%', { reverse: true, maxlength: true });

            $<%--("#<%= txtValorAlvoPercentual.ClientID %>").keyup(function () {
                calculaValorPercentual($(this), $("#<%= txtValorAlvoPercentual.ClientID %>"));
            });--%>
        });

        //function calculaValorPercentual($this, $textBox) {
        //    var text = $this.val();
        //    console.log(text);

        //    if (text != null) {
        //        text = text.replace('%', '');
        //        text = text.indexOf(',') > 0 ? text.split(',')[0] : text;

        //        var decimal = text.indexOf(',') > 0 ? "," + text.split(',')[1] : "";

        //        if (text <= 100) {
        //            $textBox.val();
        //        } else {
        //            if (text.substring(0, 2) > 10) {
        //                $textBox.val(text.substring(0, 2) + decimal + "%");
        //            } else if (text.substring(0, 1) == 1) {
        //                $textBox.val(text.substring(0, 3) + decimal + "%");
        //            } else {
        //                $textBox.val(text.substring(0, 2) + decimal + "%");
        //            }
        //        }
        //            }
        //        }
    </script>
</asp:Content>
