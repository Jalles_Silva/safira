﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using SQFramework.Core.Reflection;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using SQFramework.Data.Pagging;
using PGA.Common;


namespace PGA.Presentation.Site.Meta
{
    public partial class Consultar : CustomPageBase
    {
        #region [Properties]

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private int AnoExercicio
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("AnoExercicio").ToInt32();
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            btnPesquisar.Click += btnPesquisar_Click;

            ddlAnoExercicio.SelectedIndexChanged += ddlAnoExercicio_SelectedIndexChanged;
        }

        void ddlAnoExercicio_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarArea();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CarregarControles();
                HabilitarBotaoNovo();
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Recarregar":
                    if (e.Result == MessageBoxResult.OK)
                        ConsultarDados(0);
                    break;
                case "Remover":
                    if (e.Result == MessageBoxResult.Yes)
                        Remover(e.Parameters["CodSeqMeta"].ToInt32());
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            ConsultarDados(e.NewPage - 1);
        }

        protected void rptMetasPorUnidade_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Excluir":
                    {
                        var parametros = new Dictionary<string, object>();
                        parametros.Add("CodSeqMeta", e.CommandArgument);

                        MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente excluir o registro?",
                            "Deseja realmente excluir o registro?", MessageBoxButtons.YesNo, "Remover", parametros);
                    }
                    break;
                case "Detalhar":
                    WebHelper.Redirect(String.Format("~/Site/Meta/Detalhar.aspx?CodSeqMeta={0}&AnoExercicio={1}&Unidade={2}&PageIndex={3}", e.CommandArgument, ddlAnoExercicio.SelectedValue, ddlArea.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                case "Visuzalizar":
                    WebHelper.Redirect(String.Format("~/Site/Meta/Visualizar.aspx?CodigoSeqMeta={0}&AnoExercicio={1}&Unidade={2}&PageIndex={3}", e.CommandArgument, ddlAnoExercicio.SelectedValue, ddlArea.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                case "CancelarMeta":
                    WebHelper.Redirect(String.Format("~/Site/Meta/Visualizar.aspx?CodigoSeqMeta={0}&AnoExercicio={1}&Unidade={2}&PageIndex={3}", e.CommandArgument, ddlAnoExercicio.SelectedValue, ddlArea.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                default:
                    break;
            }
        }

        protected void btnPesquisar_Click(object sender, EventArgs e)
        {
            ConsultarDados(0);
        }

        #endregion

        #region [Methods]

        private void ConsultarDados(int pageIndex)
        {
            Validate();

            if (IsValid)
            {
                try
                {
                    PageMessage<DTOMeta> dadosPesquisados = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService")
                        .ListarMetasPorUnidadeMeta(ddlArea.SelectedItem.Text, ddlAnoExercicio.SelectedValue.ToInt32(), ddlMetaInterna.SelectedValue.ToInt32(), ddlStatusMeta.SelectedValue.ToInt32(), pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize);

                    List<DTOMeta> source = dadosPesquisados.Entities.ToList();

                    List<string> unidades = source.Select(i => i.DescricaoUnidade).Distinct().ToList();
                    Dictionary<string, IList<DTOMeta>> lista = new Dictionary<string, IList<DTOMeta>>();

                    foreach (var item in unidades)
                    {
                        lista.Add(item, source.Where(i => i.DescricaoUnidade == item).OrderBy(x => x.DescricaoMeta).ToList());
                    }

                    rptMetas.DataSource = lista;
                    rptMetas.DataBind();

                    ucPaginatorConsulta.Visible = (dadosPesquisados.RowsCount > 0);
                    ucPaginatorConsulta.TotalRecords = dadosPesquisados.RowsCount;
                    ucPaginatorConsulta.PageIndex = pageIndex + 1;
                    ucPaginatorConsulta.DataBind();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        public static Control ProcuraControle(Control control, string id)
        {
            if (control == null)
                return null;

            Control ctrl = control.FindControl(id);

            if (ctrl == null)
            {
                foreach (Control child in control.Controls)
                {
                    ctrl = ProcuraControle(child, id);

                    if (ctrl != null)
                        break;
                }
            }
            return ctrl;
        }

        private void HabilitarBotaoNovo()
        {
            IList<DTOExercicio> exercicios = this.ObterPGAService().ListarExercicios();
            if (exercicios.Any(i => i.Fase.CodigoSeqFase == (int)FaseEnum.Planejamento))
            {
                btnNovo.Visible = UsuarioAdministrador || UsuarioCadastrador;
            }
            else
            {
                if (exercicios.Any(i => i.Fase.CodigoSeqFase == (int)FaseEnum.AcompanhamentoPrimeiroTrimestre
                        || i.Fase.CodigoSeqFase == (int)FaseEnum.AcompanhamentoSegundoTrimestre
                        || i.Fase.CodigoSeqFase == (int)FaseEnum.AcompanhamentoTerceiroTrimestre
                        || i.Fase.CodigoSeqFase == (int)FaseEnum.AcompanhamentoQuartoTrimestre))
                    btnNovo.Visible = UsuarioAdministrador;
            }
        }

        private void Remover(int codSeqMeta)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarMeta(codSeqMeta);
                MessageBox.ShowInformationMessage("Registro excluido com sucesso!");
                ConsultarDados(ucPaginatorConsulta.PageIndex - 1);
            }
            catch
            {
                MessageBox.ShowErrorMessage("Não foi possível excluir o registro devido ao fato de ele já ter vinculação.");
            }
        }

        private void CarregarControles()
        {
            if (UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador)
            {
                ddlAnoExercicio.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarExercicios().OrderByDescending(i => i.Ano);
                ddlAnoExercicio.DataTextField = "Ano";
                ddlAnoExercicio.DataValueField = "Ano";
                ddlAnoExercicio.DataBind();
            }
            else if (UsuarioConsultor)
            {
                ddlAnoExercicio.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarExercicioExcetoPlanejamento().OrderByDescending(i => i.Ano);
                ddlAnoExercicio.DataTextField = "Ano";
                ddlAnoExercicio.DataValueField = "Ano";
                ddlAnoExercicio.DataBind();
            }
            CarregarStatusMetas();
            CarregarArea();

            if (UsuarioCadastrador || UsuarioValidador)
            {
                if (SCAApplicationContext.Usuario.CodigoSuperintendencia == 0)
                {
                    MessageBox.ShowConfirmationMessage(MessageBoxType.Error, "Erro",
                         "Não foi possível carregar a superintêndencia, favor logar novamente.", MessageBoxButtons.OK, "Deslogar");
                    return;
                }
                else
                {
                    hdfAreaUsuarioLogado.Value = SCAApplicationContext.Usuario.CodigoSuperintendencia.ToString();
                }
            }
            if (AnoExercicio > 0)
                ddlAnoExercicio.SelectedValue = AnoExercicio.ToString();
            if (Unidade > 0)
                ddlArea.SelectedValue = Unidade.ToString();
            if (PageIndex > 0)
                ConsultarDados(PageIndex - 1);
        }

        private void CarregarArea()
        {
            ddlArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarCorporativoUnidadePorExercicio(ddlAnoExercicio.SelectedValue.ToInt32());
            ddlArea.DataTextField = "DescricaoUnidade";
            ddlArea.DataValueField = "CodigoUnidade";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("Todas", "0"));
        }

        private void CarregarStatusMetas()
        {
            ddlStatusMeta.DataSource = this.ObterPGAService().ListarStatusMetas();
            ddlStatusMeta.DataTextField = "DescricaoStatus";
            ddlStatusMeta.DataValueField = "CodigoSeqStatusMeta";
            ddlStatusMeta.DataBind();
            ddlStatusMeta.Items.Insert(0, new ListItem("Todos", ""));
        }

        protected void rptMetas_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var lnkDetalhar = (LinkButton)ProcuraControle(e.Item, "lnkDetalhar");
            var lnkExcluir = (LinkButton)ProcuraControle(e.Item, "lnkExcluir");
            var lnkValidar = (LinkButton)ProcuraControle(e.Item, "lnkValidar");
            var lnkCancelar = (LinkButton)ProcuraControle(e.Item, "lnkCancelar");
            int codigoUnidade = ((HiddenField)ProcuraControle(e.Item, "hdfNomeUnidade")).Value.ToInt32();
            int codigoStatusMeta = ((HiddenField)ProcuraControle(e.Item, "hdfCodigoStatusMeta")).Value.ToInt32();
            int codigoFaseExercicio = ((HiddenField)ProcuraControle(e.Item, "hdfCodigoFaseExercicio")).Value.ToInt32();

            bool areaUsuarioLogado = hdfAreaUsuarioLogado.Value.ToInt32() == codigoUnidade;

            lnkDetalhar.Visible = ((UsuarioAdministrador && codigoFaseExercicio != (int)FaseEnum.Encerrado)
                                    || (UsuarioCadastrador && areaUsuarioLogado && codigoStatusMeta == (int)StatusMetaEnum.EmCadastro)) && (codigoStatusMeta != (int)StatusMetaEnum.Cancelada);
            lnkExcluir.Visible = ((UsuarioAdministrador && codigoFaseExercicio != (int)FaseEnum.Encerrado)
                                    || (UsuarioCadastrador && areaUsuarioLogado && codigoStatusMeta == (int)StatusMetaEnum.EmCadastro)) && (codigoStatusMeta != (int)StatusMetaEnum.Cancelada);
            lnkValidar.Visible = ((UsuarioAdministrador && codigoFaseExercicio != (int)FaseEnum.Encerrado)
                                    || (UsuarioCadastrador && areaUsuarioLogado && codigoStatusMeta == (int)StatusMetaEnum.EmCadastro)
                                    || (UsuarioValidador && areaUsuarioLogado && codigoStatusMeta == (int)StatusMetaEnum.EmValidacao)) && (codigoStatusMeta != (int)StatusMetaEnum.Cancelada);
            lnkCancelar.Visible = (codigoStatusMeta == (int)StatusMetaEnum.Cancelada);
        }

        #endregion


    }
}