﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Detalhar.aspx.cs" Inherits="PGA.Presentation.Site.Gerenciar.TipoAlinhamento.Detalhar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnSalvar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cadastro Tipo de Alinhamento</h3>
                    </div>
                    <div class="panel-body">
                       <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoAlinhamento" AssociatedControlID="drpTipoAlinhamento" Text="Tipo de Alinhamento" runat="server" />
                                    <asp:DropDownList ID="drpTipoAlinhamento" runat="server" CssClass="form-control" OnSelectedIndexChanged="drpTipoAlinhamento_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="drpArea" Text="Área *" runat="server" />
                                    <asp:DropDownList ID="drpArea" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7 col-sm-7">
                                <div class="form-group">
                                    <asp:Label ID="lblNomeAlinhamento" AssociatedControlID="txtNomeAlinhamento" Text="Projeto / Iniciativa" runat="server" />
                                    <asp:TextBox ID="txtNomeAlinhamento" runat="server" Rows="5" TextMode="MultiLine" MaxLength="500" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div id="divAnexoPE" runat="server" class="col-xs-3 col-sm-3">
                                <div class="form-group">
                                    <asp:UpdatePanel ID="updPanel" runat="server">
                                        <ContentTemplate>
                                            <asp:Label Text="Documentação do Projeto / Iniciativa" runat="server" AssociatedControlID="lnkBaixarAnexo" />
                                            <asp:FileUpload ID="flpAnexo" runat="server" accept=".pdf, .rar, .zip, .7z" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSalvar" />
                                            <asp:PostBackTrigger ControlID="btnAdicionar" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                <div style="margin-top:-10px">
                                    <asp:LinkButton ID="lnkBaixarAnexo" ToolTip="Realizar o download do arquivo" runat="server" CaminhoAnexo="">
                                        <asp:Label ID="lblAnexo" runat="server" />   <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkDeletarAnexo" ToolTip="Excluir arquivo do Acompanhamento" runat="server">
                                        <i aria-hidden="true" class="glyphicon glyphicon-remove" style="color:red;"></i>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div id="divProgramaTematico" runat="server" class="row">
                            <div class="col-xs-7 col-sm-7">
                                <div class="form-group">
                                    <asp:Label ID="lblProgramaTematico" AssociatedControlID="txtProgramaTematico" Text="Programa Temático" runat="server" />
                                    <asp:TextBox ID="txtProgramaTematico" runat="server" Rows="3" TextMode="MultiLine" MaxLength="1000" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div ID="divTipo" runat="server" class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblTipo" AssociatedControlID="rblTipo" Text="Tipo" runat="server" />
                                    <asp:RadioButtonList ID="rblTipo" runat="server" RepeatDirection="Horizontal" CssClass="radioListHoriz">
                                        <asp:ListItem Value="P" Text="Projeto" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="I" Text="Iniciativa"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                        <div id="divObjetivo" runat="server" class="row" style="margin-bottom:15px;">
                            <asp:UpdatePanel ID="upPnlObjetivo" runat="server">
                                <ContentTemplate>
                                    <div class="col-xs-6 col-sm-6">
                                        <div class="form-group">
                                            <asp:HiddenField ID="hdfCodSeqIniciativaObjetivo" runat="server" Value="0" />
                                            <asp:Label ID="lblObjetivo" AssociatedControlID="drpObjetivo" Text="Objetivo Estratégico" runat="server" />
                                            <asp:DropDownList ID="drpObjetivo" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group">
                                            <asp:Label ID="lblPercentual" AssociatedControlID="txtPercentual" Text="Percentual de Participação" runat="server" />
                                            <asp:TextBox ID="txtPercentual" runat="server" CssClass="form-control" Style="text-align: right"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="row pull-right" style="margin-right:25%; margin-top:18px;">
                                        <div class="col-xs-1">
                                            <asp:LinkButton ID="btnAdicionar" runat="server" SkinID="btnAdicionar" CausesValidation="false" /> 
                                        </div>
                                    </div>
                        
                                    <br />
                                    <asp:UpdatePanel ID="upPnlResultadoConsulta" runat="server">
                                        <ContentTemplate>
                                            <div class="area-table col-xs-8">
                                                <asp:Label ID="lblObjetivos" Text="Objetivos" runat="server" CssClass="h4" Visible="false" />
                                                <asp:GridView ID="grdObjetivos" runat="server" AutoGenerateColumns="False" EmptyDataText="Nenhum registro encontrado." OnRowCommand="grdObjetivos_RowCommand" Style="margin-top:10px">
                                                    <EmptyDataTemplate>Nenhum registro encontrado.</EmptyDataTemplate>
                                                    <AlternatingRowStyle CssClass="alternate" />
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Objetivo Estratégico" DataField="Objetivo.nomeObjetivo" />
                                                        <asp:TemplateField HeaderText="Porcentagem" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPorcentagem" runat="server" Text='<%# string.Format("{0:N2}", Convert.ToDecimal(Eval("PercentualParticipacao"))) %>'></asp:Label>%
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Ações" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hdfCodObjetivo" runat="server" Value='<%# Eval("Objetivo.CodigoSeqObjetivo") %>' />
                                                                <asp:LinkButton ID="lnkDetalhar" runat="server" CommandArgument='<%# Eval("codigoSeqIniciativaObjetivo") %>'
                                                                    CommandName="Detalhar" CausesValidation="false" CssClass="glyphicon glyphicon-pencil" />
                                                                <asp:LinkButton ID="lnkExcluir" runat="server" CommandArgument='<%# Eval("codigoSeqIniciativaObjetivo") %>'
                                                                    CommandName="Excluir" CausesValidation="false" CssClass="glyphicon glyphicon-remove" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <div class="paginator col-xs-8">
                                                <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="8" />
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblSituacao" AssociatedControlID="rblSituacao" Text="Situação" runat="server" />
                                    <asp:RadioButtonList ID="rblSituacao" runat="server" RepeatDirection="Horizontal" CssClass="radioListHoriz">
                                        <asp:ListItem Value="True" Text="Ativo" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="False" Text="Inativo"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>

                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="updBotoes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                        <asp:LinkButton ID="btnSalvar" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="Salvar" />                                        
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            FuncaoTela();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function ()
        {
            FuncaoTela();
        });

        function FuncaoTela() {

            $("#<%= txtPercentual.ClientID %>").mask('000,00', { reverse: true });

            $("#<%= txtNomeAlinhamento.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 500) {
                    $("#<%= txtNomeAlinhamento.ClientID %>").val(text.substring(0, 500));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtNomeAlinhamento.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 500) {
                    $("#<%= txtNomeAlinhamento.ClientID %>").val(text.substring(0, 500));
                    return false;
                }
                else {
                    return true;
                }
            });

            $("#<%= txtProgramaTematico.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 1000) {
                    $("#<%= txtProgramaTematico.ClientID %>").val(text.substring(0, 1000));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtProgramaTematico.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 1000) {
                    $("#<%= txtProgramaTematico.ClientID %>").val(text.substring(0, 1000));
                    return false;
                }
                else {
                    return true;
                }
            });
            
        }
    </script>
</asp:Content>
