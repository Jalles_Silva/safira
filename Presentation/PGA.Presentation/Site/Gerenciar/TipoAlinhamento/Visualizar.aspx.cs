﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using PGA.Common;

namespace PGA.Presentation.Site.Gerenciar.TipoAlinhamento
{
    public partial class Visualizar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqIniciativa
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqIniciativa").ToInt32();
            }
        }

        private int TipoAlinhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoAlinhamento").ToInt32();
            }
        }

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
            lnkBaixarAnexo.Click += new EventHandler(lnkBaixarAnexo_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Gerenciar/TipoAlinhamento/Consultar.aspx?TipoAlinhamento={0}&Unidade={1}&Situacao={2}&PageIndex={3}", TipoAlinhamento, Unidade, Situacao, PageIndex));
        }

        protected void lnkBaixarAnexo_Click(object sender, EventArgs e)
        {
            var lnk = (LinkButton)sender;

            WebHelper.DownloadFile(System.IO.File.ReadAllBytes(lnk.Attributes["CaminhoAnexo"]), lblAnexo.Text);
        }

        #endregion

        #region [Methods]

        private void AtualizarPaineis()
        {
            divProgramaTematico.Visible =
            divObjetivo.Visible =
            divAnexoPE.Visible = false;

            switch (lblTipoAlinhamento.Text)
            {
                case "Alinhamento PE":
                    lblNomeAlinhamentoCampo.Text = "INICIATIVA ESTRATÉGICA: ";
                    lblProgramaTematicoCampo.Text = "PORTFÓLIO";
                    divProgramaTematico.Visible =
                    divObjetivo.Visible =
                    divAnexoPE.Visible = true;
                    break;
                case "Alinhamento PPA":
                    lblNomeAlinhamentoCampo.Text = "OBJETIVO PPA: ";
                    lblProgramaTematicoCampo.Text = "PROGRAMA TEMÁTICO";
                    divProgramaTematico.Visible =
                    divObjetivo.Visible = true;
                    break;
                case "Alinhamento Missão Institucional":
                    lblNomeAlinhamentoCampo.Text = "MISSÃO: ";
                    break;
                default:
                    break;
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqIniciativa > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterIniciativa(CodigoSeqIniciativa);

                    lblNomeAlinhamento.Text = registro.DescricaoIniciativa;
                    lblTipoAlinhamento.Text = registro.TipoIniciativa.DescricaoTipoIniciativa;
                    lblSituacao.Text = registro.Ativo ? "Ativo" : "Inativo";

                    if (registro.CorporativoUnidade != null)
                        lblArea.Text = registro.CorporativoUnidade.DescricaoUnidade;
                    else
                        lblArea.Text = "Não cadastrada";

                    if (!String.IsNullOrEmpty(registro.ProgramaTematico))
                        lblProgramaTematico.Text = registro.ProgramaTematico;
                    else
                        lblProgramaTematico.Text = "Não cadastrado";

                    if (registro.Anexo != null)
                    {
                        lblAnexo.Text = registro.Anexo.DescricaoAnexo;
                        lnkBaixarAnexo.Attributes["CaminhoAnexo"] = registro.Anexo.CaminhoAnexo;
                    }
                    else
                    {
                        lnkBaixarAnexo.Text = "Sem Anexo";
                        lnkBaixarAnexo.Enabled = false;
                    }

                    AtualizarPaineis();

                    CarregarObjetivos();
                }
                else
                    WebHelper.Redirect("~/Site/Gerenciar/TipoAlinhamento/Consultar.aspx");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarObjetivos()
        {
            try
            {
                var objetivos = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarObjetivosPorIniciativa(CodigoSeqIniciativa, 0, int.MaxValue, "", true);

                rptObjetivos.DataSource = objetivos;
                rptObjetivos.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}