﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using PGA.Common;

namespace PGA.Presentation.Site.Gerenciar.TipoAlinhamento
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqIniciativa
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqIniciativa").ToInt32();
            }
        }

        private int TipoAlinhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoAlinhamento").ToInt32();
            }
        }

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        private List<DTOIniciativaObjetivo> ObjetivosIniciativa
        {
            get
            {
                return (List<DTOIniciativaObjetivo>)Session["ObjetivosIniciativa"];
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            drpTipoAlinhamento.SelectedIndexChanged += drpTipoAlinhamento_SelectedIndexChanged;
            lnkBaixarAnexo.Click += new EventHandler(lnkBaixarAnexo_Click);
            lnkDeletarAnexo.Click += new EventHandler(lnkDeletarAnexo_Click);
            btnAdicionar.Click += new EventHandler(btnAdicionar_Click);
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    Session["ObjetivosIniciativa"] = new List<DTOIniciativaObjetivo>();
                    CarregarTipoAlinhamento();
                    CarregarArea();
                    CarregarObjetivo();
                    CarregarRegistro();
                    AtualizarPaineis();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Voltar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        WebHelper.Redirect(String.Format("~/Site/Gerenciar/TipoAlinhamento/Consultar.aspx?TipoAlinhamento={0}&Unidade={1}&Situacao={2}&PageIndex={3}", TipoAlinhamento, Unidade, Situacao, PageIndex));
                    }
                    break;
                case "Detalhar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        var CodSeqIniciativa = e.Parameters["CodSeqIniciativa"];
                        WebHelper.Redirect(String.Format("~/Site/Gerenciar/TipoAlinhamento/Detalhar.aspx?CodigoSeqIniciativa={0}&TipoAlinhamento={1}&Unidade={2}&Situacao={3}&PageIndex={4}", CodSeqIniciativa, TipoAlinhamento, Unidade, Situacao, PageIndex));
                    }
                    break;
                case "Remover":
                    if (e.Result == MessageBoxResult.Yes)
                        Remover(e.Parameters["codigoSeqIniciativaObjetivo"].ToInt32());
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void drpTipoAlinhamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            AtualizarPaineis();
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            CarregarObjetivosTipoAlinhamento(e.NewPage - 1);
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            Validate();

            if (IsValid)
            {
                if (Int32.Parse(drpArea.SelectedValue) <= 0)
                {
                    MessageBox.ShowInformationMessage("Favor informar a Área.", "Erro");
                    return;
                }

                if (String.IsNullOrEmpty(txtNomeAlinhamento.Text))
                {
                    MessageBox.ShowInformationMessage("Favor informar o " + lblNomeAlinhamento.Text + " do registro.", "Erro");
                    return;
                }

                decimal v = 0;

                foreach (var item in ObjetivosIniciativa)
                    v += item.PercentualParticipacao;

                if (v > 100)
                {
                    MessageBox.ShowInformationMessage("O Percentual de Participação total não pode ser maior que 100%.", "Erro");
                    return;
                }

                if (flpAnexo.HasFile)
                {
                    var TipoArquivosValidos = "pdf, rar, zip, 7z";
                    string extensao = flpAnexo.FileName.Substring(flpAnexo.FileName.LastIndexOf(".") + 1);

                    if (!TipoArquivosValidos.ToLower().Contains(extensao.ToLower()))
                    {
                        MessageBox.ShowInformationMessage("O tipo de arquivo anexado não é válido, anexe um arquivo do tipo valido e tente novamente.", "Erro");
                        return;
                    }
                }

                Salvar();
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Voltar",
                "Deseja retornar para a página de consulta (dados não salvos serão perdidos)?", MessageBoxButtons.YesNo, "Voltar");
        }

        protected void btnAdicionar_Click(object sender, EventArgs e)
        {
            Validate();

            if (IsValid)
            {
                if (String.IsNullOrEmpty(txtPercentual.Text))
                {
                    MessageBox.ShowInformationMessage("Favor informar o Percentual de Participação do objetivo.", "Erro");
                    return;
                }

                if (drpObjetivo.SelectedValue == "0")
                {
                    MessageBox.ShowInformationMessage("Favor informar Objetivo Estratégico.", "Erro");
                    return;
                }

                if (ObjetivosIniciativa.Where(o => o.Objetivo.CodigoSeqObjetivo == drpObjetivo.SelectedValue.ToInt32()).Count() > 0)
                {
                    MessageBox.ShowInformationMessage("O Objetivo Estratégico já foi informado anteriormente.", "Erro");
                    return;
                }

                var objetivo = new DTOIniciativaObjetivo();
                objetivo.CodigoSeqIniciativaObjetivo = hdfCodSeqIniciativaObjetivo.Value.ToInt32();
                objetivo.Objetivo = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterObjetivo(drpObjetivo.SelectedValue.ToInt32());
                objetivo.PercentualParticipacao = txtPercentual.Text.ToDecimal();

                if (CodigoSeqIniciativa > 0)
                    objetivo.Iniciativa = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterIniciativa(CodigoSeqIniciativa);

                ObjetivosIniciativa.Add(objetivo);

                txtPercentual.Text = "";
                hdfCodSeqIniciativaObjetivo.Value =
                drpObjetivo.SelectedValue = "0";

                CarregarObjetivosTipoAlinhamento(ucPaginatorConsulta.PageIndex - 1);
            }
        }

        protected void grdObjetivos_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Excluir":
                    {
                        var parametros = new Dictionary<string, object>();
                        parametros.Add("codigoSeqIniciativaObjetivo", e.CommandArgument);

                        if (e.CommandArgument.ToInt32() > 0)
                            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente excluir o registro?",
                                "Deseja realmente excluir o registro?", MessageBoxButtons.YesNo, "Remover", parametros);
                        else
                        {
                            var hdfCodObjetivo = (HiddenField)((GridViewRow)((Control)e.CommandSource).NamingContainer).FindControl("hdfCodObjetivo");

                            ObjetivosIniciativa.Remove(ObjetivosIniciativa.FirstOrDefault(o => o.Objetivo.CodigoSeqObjetivo == hdfCodObjetivo.Value.ToInt32()));
                            MessageBox.ShowInformationMessage("Registro excluido com sucesso!");
                            CarregarObjetivosTipoAlinhamento(ucPaginatorConsulta.PageIndex - 1);
                        }
                    }
                    break;
                case "Detalhar":
                    var objetivo = new DTOIniciativaObjetivo();

                    if (e.CommandArgument.ToInt32() > 0)
                        objetivo = ObjetivosIniciativa.FirstOrDefault(o => o.CodigoSeqIniciativaObjetivo == e.CommandArgument.ToInt32());
                    else
                    {
                        var hdfCodObjetivo = (HiddenField)((GridViewRow)((Control)e.CommandSource).NamingContainer).FindControl("hdfCodObjetivo");
                        objetivo = ObjetivosIniciativa.FirstOrDefault(o => o.Objetivo.CodigoSeqObjetivo == hdfCodObjetivo.Value.ToInt32());
                    }

                    CarregarObjetivoTipoAlinhamento(objetivo);
                    CarregarObjetivosTipoAlinhamento(ucPaginatorConsulta.PageIndex - 1);
                    break;
                default:
                    break;
            }
        }

        protected void lnkBaixarAnexo_Click(object sender, EventArgs e)
        {
            var lnk = (LinkButton)sender;

            WebHelper.DownloadFile(System.IO.File.ReadAllBytes(lnk.Attributes["CaminhoAnexo"]), lblAnexo.Text);
        }

        protected void lnkDeletarAnexo_Click(object sender, EventArgs e)
        {
            this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarAnexoIniciativa(CodigoSeqIniciativa);
            MessageBox.ShowInformationMessage("Anexo excluído com sucesso!");

            lnkBaixarAnexo.Visible = lnkDeletarAnexo.Visible = false;
            flpAnexo.Visible = true;
        }

        #endregion

        #region [Methods]

        private void CarregarTipoAlinhamento()
        {
            drpTipoAlinhamento.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTiposIniciativa();
            drpTipoAlinhamento.DataTextField = "DescricaoTipoIniciativa";
            drpTipoAlinhamento.DataValueField = "CodigoSeqTipoIniciativa";
            drpTipoAlinhamento.DataBind();
        }

        private void CarregarArea()
        {
            drpArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarCorporativoUnidadeAtivo();
            drpArea.DataTextField = "DescricaoUnidade";
            drpArea.DataValueField = "CodigoUnidade";
            drpArea.DataBind();
            drpArea.Items.Insert(0, new ListItem("Selecione", "0"));
        }

        private void CarregarObjetivo()
        {
            drpObjetivo.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarObjetivosPorTipo(EnumTipoObjetivo.Objetivos);
            drpObjetivo.DataTextField = "NomeObjetivo";
            drpObjetivo.DataValueField = "CodigoSeqObjetivo";
            drpObjetivo.DataBind();
            drpObjetivo.Items.Insert(0, new ListItem("Selecione", "0"));
        }

        private void AtualizarPaineis()
        {
            divProgramaTematico.Visible =
            divObjetivo.Visible =
            divAnexoPE.Visible =
            divTipo.Visible = false;

            switch (drpTipoAlinhamento.SelectedItem.Text)
            {
                case "Alinhamento PE":
                    lblNomeAlinhamento.Text = "Projeto / Iniciativa";
                    lblProgramaTematico.Text = "Portfólio";
                    divProgramaTematico.Visible =
                    divObjetivo.Visible =
                    divAnexoPE.Visible =
                    divTipo.Visible = true;
                    break;
                case "Alinhamento PPA":
                    lblNomeAlinhamento.Text = "Objetivo PPA";
                    lblProgramaTematico.Text = "Programa Temático";
                    divProgramaTematico.Visible =
                    divObjetivo.Visible = true;
                    break;
                case "Alinhamento Missão Institucional":
                    lblNomeAlinhamento.Text = "Missão";
                    break;
                default:
                    break;
            }
        }

        private void Salvar()
        {
            try
            {
                DTOIniciativa registro = new DTOIniciativa();
                registro.CodigoSeqIniciativa = CodigoSeqIniciativa;
                registro.DescricaoIniciativa = txtNomeAlinhamento.Text;
                registro.Ativo = Boolean.Parse(rblSituacao.SelectedValue);

                if (divProgramaTematico.Visible)
                    registro.ProgramaTematico = txtProgramaTematico.Text;

                if (divTipo.Visible)
                    registro.TpIniciativaProjeto = rblTipo.SelectedValue;

                var tipo = new DTOTipoIniciativa();
                tipo.CodigoSeqTipoIniciativa = drpTipoAlinhamento.SelectedValue.ToInt32();
                registro.TipoIniciativa = tipo;

                var unidade = new DTOCorporativoUnidade();
                unidade.CodigoUnidade = drpArea.SelectedValue.ToInt32();
                registro.CorporativoUnidade = unidade;

                if (flpAnexo.HasFile)
                {
                    var anexo = new DTOAnexo();

                    anexo.DescricaoAnexo = flpAnexo.FileName;
                    anexo.Conteudo = flpAnexo.FileBytes;
                    anexo.CaminhoAnexo = System.Configuration.ConfigurationManager.AppSettings["diretorioAnexosTipoAlinhamento"];

                    registro.Anexo = anexo;
                }

                var CodSeqIniciativa = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarIniciativa(registro);

                foreach (var item in ObjetivosIniciativa)
                {
                    if (item.Iniciativa == null)
                    {
                        registro.CodigoSeqIniciativa = CodSeqIniciativa;
                        item.Iniciativa = registro;
                    }

                    this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarIniciativaObjetivo(item);
                }

                var param = new Dictionary<string, object>();
                param.Add("CodSeqIniciativa", CodSeqIniciativa);

                MessageBox.ShowInformationMessage("Registro salvo com sucesso.", "Detalhar", param);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqIniciativa > 0)
                {
                    drpTipoAlinhamento.Enabled = false;

                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterIniciativa(CodigoSeqIniciativa);

                    txtNomeAlinhamento.Text = registro.DescricaoIniciativa;
                    txtProgramaTematico.Text = registro.ProgramaTematico;
                    rblSituacao.SelectedValue = registro.Ativo.ToString();

                    if (!String.IsNullOrEmpty(registro.TpIniciativaProjeto))
                        rblTipo.SelectedValue = registro.TpIniciativaProjeto;

                    if (registro.TipoIniciativa != null)
                        drpTipoAlinhamento.SelectedValue = registro.TipoIniciativa.CodigoSeqTipoIniciativa.ToString();

                    if (registro.CorporativoUnidade != null && drpArea.Items.Contains(new ListItem(registro.CorporativoUnidade.DescricaoUnidade, registro.CorporativoUnidade.CodigoUnidade.ToString())))
                        drpArea.SelectedValue = registro.CorporativoUnidade.CodigoUnidade.ToString();

                    if (registro.Anexo != null)
                    {
                        lblAnexo.Text = registro.Anexo.DescricaoAnexo;
                        lnkBaixarAnexo.Attributes["CaminhoAnexo"] = registro.Anexo.CaminhoAnexo;
                        lnkBaixarAnexo.Visible = lnkDeletarAnexo.Visible = true;
                        flpAnexo.Visible = false;
                    }
                    else
                    {
                        lnkBaixarAnexo.Visible = lnkDeletarAnexo.Visible = false;
                        flpAnexo.Visible = true;
                    }

                    AtualizarPaineis();

                    CarregarObjetivosTipoAlinhamento(ucPaginatorConsulta.PageIndex - 1);
                }
                else
                {
                    lnkBaixarAnexo.Visible = lnkDeletarAnexo.Visible = false;
                    flpAnexo.Visible = true;
                    drpTipoAlinhamento.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarObjetivosTipoAlinhamento(int pageIndex)
        {
            try
            {
                if (ObjetivosIniciativa.Count == 0)
                {
                    var objetivos = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarObjetivosPorIniciativa(CodigoSeqIniciativa, pageIndex * ucPaginatorConsulta.PageSize, -1, "", true);

                    ObjetivosIniciativa.AddRange(objetivos.Entities);
                }

                var objts = new List<DTOIniciativaObjetivo>();
                var x = pageIndex * 8;

                for (int i = x; i < x + 8; i++)
                {
                    if (ObjetivosIniciativa.Count <= i)
                        break;

                    objts.Add(ObjetivosIniciativa[i]);
                }

                grdObjetivos.DataSource = objts;
                grdObjetivos.DataBind();
                lblObjetivos.Visible = true;

                ucPaginatorConsulta.Visible = (ObjetivosIniciativa.Count > 0);
                ucPaginatorConsulta.TotalRecords = ObjetivosIniciativa.Count;
                ucPaginatorConsulta.PageIndex = pageIndex + 1;
                ucPaginatorConsulta.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarObjetivoTipoAlinhamento(DTOIniciativaObjetivo objetivo)
        {
            try
            {
                if (objetivo != null)
                {
                    hdfCodSeqIniciativaObjetivo.Value = objetivo.CodigoSeqIniciativaObjetivo.ToString();
                    txtPercentual.Text = objetivo.PercentualParticipacao.ToString();

                    if (objetivo.Objetivo != null && drpObjetivo.Items.Contains(new ListItem(objetivo.Objetivo.NomeObjetivo, objetivo.Objetivo.CodigoSeqObjetivo.ToString())))
                        drpObjetivo.SelectedValue = objetivo.Objetivo.CodigoSeqObjetivo.ToString();

                    ObjetivosIniciativa.Remove(objetivo);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void Remover(int codigoSeqIniciativaObjetivo)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarIniciativaObjetivo(codigoSeqIniciativaObjetivo);
                ObjetivosIniciativa.Remove(ObjetivosIniciativa.FirstOrDefault(o => o.CodigoSeqIniciativaObjetivo == codigoSeqIniciativaObjetivo));

                MessageBox.ShowInformationMessage("Registro excluido com sucesso!");
                CarregarObjetivosTipoAlinhamento(ucPaginatorConsulta.PageIndex - 1);
            }
            catch
            {
                MessageBox.ShowErrorMessage("Não foi possível excluir o registro devido ao fato de ele já ter vinculação.");
            }
        }

        #endregion
    }
}