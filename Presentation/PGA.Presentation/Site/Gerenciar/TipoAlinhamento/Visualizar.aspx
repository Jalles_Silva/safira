﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Visualizar.aspx.cs" Inherits="PGA.Presentation.Site.Gerenciar.TipoAlinhamento.Visualizar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Consulta/Validação</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>Alinhamento</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblNomeAlinhamentoCampo" Text="TEMA: " runat="server" />
                                            <asp:Label ID="lblNomeAlinhamento" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="ÁREA: " runat="server" />
                                            <asp:Label ID="lblArea" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="TIPO DE ALINHAMENTO: " runat="server" />
                                            <asp:Label ID="lblTipoAlinhamento" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div id="divProgramaTematico" runat="server" class="row" visible="false">
                                    <div class="col-xs-7 col-sm-7">
                                        <div class="form-group">
                                            <asp:Label ID="lblProgramaTematicoCampo" Text="PROGRAMA TEMÁTICO: " runat="server" />
                                            <asp:Label ID="lblProgramaTematico" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div id="divObjetivo" runat="server" class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="OBJETIVO(S) ESTRATÉGICO(S): " runat="server" />
                                            
                                            <asp:Repeater ID="rptObjetivos" runat="server">
                                                <ItemTemplate>
                                                    <div style="margin:10px 0 15px 15px;">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-12">
                                                                <div>
                                                                    <asp:Label Text="OBJETIVO ESTRATÉGICO: " runat="server" />
                                                                    <asp:Label ID="lblObjetivo" Text='<%#Eval("Objetivo.nomeObjetivo")%>' runat="server" />
                                                                </div>
                                                                <div>
                                                                    <asp:Label Text="PERCENTUAL DE PARTICIPAÇÃO: " runat="server" />
                                                                    <asp:Label ID="lblParticipacao" Text='<%#Eval("PercentualParticipacao")%>' runat="server" />%
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <div style="margin:5px 0 5px 5px;">
                                                        <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." />
                                                    </div>
                                                </FooterTemplate>
                                            </asp:Repeater>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-1 col-sm-1">
                                        <div class="form-group">
                                            <asp:Label Text="SITUAÇÃO: " runat="server" />
                                            <asp:Label ID="lblSituacao" runat="server" />
                                        </div>
                                    </div>
                                    <div id="divAnexoPE" runat="server" class="col-xs-6 col-sm-6">
                                        <div>
                                            <asp:Label Text="ANEXO: " runat="server" />
                                            <asp:LinkButton ID="lnkBaixarAnexo" ToolTip="Realizar o download do arquivo" runat="server" CaminhoAnexo="">
                                                <asp:Label ID="lblAnexo" runat="server" />   <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="updBotoes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
