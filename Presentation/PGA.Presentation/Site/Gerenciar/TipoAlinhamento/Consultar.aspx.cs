﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;

namespace PGA.Presentation.Site.Gerenciar.TipoAlinhamento
{
    public partial class Consultar : CustomPageBase
    {
        #region [Properties]

        private int TipoAlinhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoAlinhamento").ToInt32();
            }
        }

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            btnPesquisar.Click += BtnPesquisar_Click;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarTipoAlinhamento();
                    CarregarArea();

                    if (TipoAlinhamento > 0)
                        drpTipoAlinhamento.SelectedValue = TipoAlinhamento.ToString();
                    if (Unidade > 0)
                        drpArea.SelectedValue = Unidade.ToString();
                    if (!String.IsNullOrEmpty(Situacao))
                        rblSituacao.SelectedValue = Situacao.ToString();
                    if (PageIndex > 0)
                        ConsultarDados(PageIndex - 1);
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Remover":
                    if (e.Result == MessageBoxResult.Yes)
                        Remover(e.Parameters["CodigoSeqIniciativa"].ToInt32());
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            ConsultarDados(e.NewPage - 1);
        }

        private void BtnPesquisar_Click(object sender, EventArgs e)
        {
            ConsultarDados(0);
        }

        protected void rptAlinhamentosPorUnidade_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Excluir":
                    {
                        var parametros = new Dictionary<string, object>();
                        parametros.Add("CodigoSeqIniciativa", e.CommandArgument);

                        MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente excluir o registro?",
                            "Deseja realmente excluir o registro?", MessageBoxButtons.YesNo, "Remover", parametros);
                    }
                    break;
                case "Detalhar":
                    WebHelper.Redirect(String.Format("~/Site/Gerenciar/TipoAlinhamento/Detalhar.aspx?CodigoSeqIniciativa={0}&TiposAlinhamento={1}&Unidade={2}&Situacao={3}&PageIndex={4}", e.CommandArgument, drpTipoAlinhamento.SelectedValue, drpArea.SelectedValue, rblSituacao.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                case "Visuzalizar":
                    WebHelper.Redirect(String.Format("~/Site/Gerenciar/TipoAlinhamento/Visualizar.aspx?CodigoSeqIniciativa={0}&TiposAlinhamento={1}&Unidade={2}&Situacao={3}&PageIndex={4}", e.CommandArgument, drpTipoAlinhamento.SelectedValue, drpArea.SelectedValue, rblSituacao.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region [Methods]

        private void CarregarTipoAlinhamento()
        {
            drpTipoAlinhamento.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTiposIniciativa();
            drpTipoAlinhamento.DataTextField = "DescricaoTipoIniciativa";
            drpTipoAlinhamento.DataValueField = "CodigoSeqTipoIniciativa";
            drpTipoAlinhamento.DataBind();
            drpTipoAlinhamento.Items.Insert(0, new ListItem("Todos", "0"));
        }

        private void CarregarArea()
        {
            drpArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarCorporativoUnidadePorExercicio(0);
            drpArea.DataTextField = "DescricaoUnidade";
            drpArea.DataValueField = "CodigoUnidade";
            drpArea.DataBind();
            drpArea.Items.Insert(0, new ListItem("Todas", "0"));
        }

        private void ConsultarDados(int pageIndex)
        {
            Validate();

            if (IsValid)
            {
                try
                {
                    var iniciativas = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarIniciativaPorTipoEUnidade(drpTipoAlinhamento.SelectedValue.ToInt32(), drpArea.SelectedItem.Text, rblSituacao.SelectedValue, pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize, "CorporativoUnidade.DescricaoUnidade", true);

                    List<DTOIniciativa> source = iniciativas.Entities.ToList();

                    List<string> unidades = source.Select(i => i.CorporativoUnidade.DescricaoUnidade).Distinct().ToList();
                    Dictionary<string, IList<DTOIniciativa>> lista = new Dictionary<string, IList<DTOIniciativa>>();

                    foreach (var item in unidades)
                    {
                        lista.Add(item, source.Where(i => i.CorporativoUnidade.DescricaoUnidade == item).OrderBy(x => x.DescricaoIniciativa).ToList());
                    }

                    rptAlinhamentos.DataSource = lista;
                    rptAlinhamentos.DataBind();

                    ucPaginatorConsulta.Visible = (iniciativas.RowsCount > 0);
                    ucPaginatorConsulta.TotalRecords = iniciativas.RowsCount;
                    ucPaginatorConsulta.PageIndex = pageIndex + 1;
                    ucPaginatorConsulta.DataBind();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        private void Remover(int CodigoSeqIniciativa)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarIniciativa(CodigoSeqIniciativa);
                MessageBox.ShowInformationMessage("Registro excluido com sucesso!");
                ConsultarDados(ucPaginatorConsulta.PageIndex - 1);
            }
            catch
            {
                MessageBox.ShowErrorMessage("Não foi possível excluir o registro devido ao fato de ele já ter vinculação.");
            }
        }

        #endregion
    }
}