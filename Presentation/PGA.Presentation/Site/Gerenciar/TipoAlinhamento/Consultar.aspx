﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Consultar.aspx.cs" Inherits="PGA.Presentation.Site.Gerenciar.TipoAlinhamento.Consultar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Pesquisar Tipos de Alinhamento</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoAlinhamento" AssociatedControlID="drpTipoAlinhamento" Text="Tipo de Alinhamento" runat="server" />
                                    <asp:DropDownList ID="drpTipoAlinhamento" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="drpArea" Text="Área" runat="server" />
                                    <asp:DropDownList ID="drpArea" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblSituacao" AssociatedControlID="rblSituacao" Text="Situação" runat="server" />
                                    <asp:RadioButtonList ID="rblSituacao" runat="server" RepeatDirection="Horizontal" CssClass="radioListHoriz">
                                        <asp:ListItem Value="" Text="Todos" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="True" Text="Ativo"></asp:ListItem>
                                        <asp:ListItem Value="False" Text="Inativo"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-12">
                                    <asp:LinkButton ID="btnPesquisar" runat="server" SkinID="btnPesquisar" CausesValidation="false" />
                                    <asp:HyperLink ID="btnLimpar" runat="server" SkinID="btnLimpar" CausesValidation="false"
                                        NavigateUrl="~/Site/Gerenciar/TipoAlinhamento/Consultar.aspx" />
                                    <asp:HyperLink ID="btnNovo" runat="server" SkinID="btnNovo" CausesValidation="false"
                                        NavigateUrl="~/Site/Gerenciar/TipoAlinhamento/Detalhar.aspx" />
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="upPnlResultadoConsulta" runat="server">
                            <ContentTemplate>
                                <div class="area-table">
                                    <asp:Repeater ID="rptAlinhamentos" runat="server">
                                        <HeaderTemplate>
                                            <table>
                                                <tr>
                                                    <th>Tipo de Alinhamento</th>
                                                    <th>Descrição</th>
                                                    <th>Situação</th>
                                                    <th>Ações</th>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td colspan="4" style="background-color: #C5D8CC !important;">
                                                    <asp:Label ID="lblNomeUnidade" runat="server" Text='<%#Eval("Key") %>' />
                                                </td>
                                            </tr>
                                            <asp:Repeater ID="rptAlinhamentosPorUnidade" OnItemCommand="rptAlinhamentosPorUnidade_ItemCommand" runat="server" DataSource='<%#Eval("Value") %>'>
                                                <ItemTemplate>
                                                    <tr class="<%# (Container.ItemIndex % 2 == 0 ? "" : "alternate") %>">
                                                        <td style="width: 202px;">
                                                            <asp:Label ID="lblTipoAlinhamento" style="margin-left:13px;" runat="server" Text='<%#Eval("TipoIniciativa.DescricaoTipoIniciativa") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblAlinhamento" runat="server" Text='<%#Eval("DescricaoIniciativa") %>' />
                                                        </td>
                                                        <td style="width: 60px; text-align:center;">
                                                            <asp:Label ID="lblSituacao" runat="server" Text='<%# (bool)Eval("Ativo") == true ? "Ativo" : "Inativo" %>' />
                                                        </td>
                                                        <td style="width: 60px">
                                                            <asp:LinkButton ID="lnkDetalhar" runat="server" Target="_self" CommandArgument='<%# Eval("CodigoSeqIniciativa") %>'
                                                                CommandName="Detalhar" CausesValidation="false" CssClass="glyphicon glyphicon-pencil" />

                                                            <asp:LinkButton ID="lnkExcluir" runat="server" CommandArgument='<%# Eval("CodigoSeqIniciativa") %>'
                                                                CommandName="Excluir" CausesValidation="false" CssClass="glyphicon glyphicon-remove" />

                                                            <asp:LinkButton ID="lnkVisualizar" runat="server" Target="_self" CommandArgument='<%# Eval("CodigoSeqIniciativa") %>'
                                                                CommandName="Visuzalizar" CausesValidation="false" CssClass="glyphicon glyphicon-search" />

                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <tr>
                                                <td colspan="4"><asp:Label ID="lblEmptyData" runat="server"  Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." /></td>
                                            </tr>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                                <div class="paginator">
                                    <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
