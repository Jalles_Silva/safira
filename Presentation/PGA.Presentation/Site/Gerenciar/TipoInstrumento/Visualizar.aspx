﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Visualizar.aspx.cs" Inherits="PGA.Presentation.Site.Gerenciar.TipoInstrumento.Visualizar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Consulta/Validação</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>Instrumento</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblNomeInstrumentoCampo" Text="TEMA: " runat="server" />
                                            <asp:Label ID="lblNomeInstrumento" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="ÁREA: " runat="server" />
                                            <asp:Label ID="lblArea" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="TIPO DE INSTRUMENTO: " runat="server" />
                                            <asp:Label ID="lblTipoInstrumento" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div id="divObjetivoEixo" runat="server" class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="EIXO TEMÁTICO: " runat="server" />
                                            <asp:Label ID="lblObjetivoEixo" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblObjetivoCampo" Text="OBJETIVO ESTRATÉGICO: " runat="server" />
                                            <asp:Label ID="lblObjetivo" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="SITUAÇÃO: " runat="server" />
                                            <asp:Label ID="lblSituacao" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="updBotoes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
