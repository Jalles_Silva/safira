﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;

namespace PGA.Presentation.Site.Gerenciar.TipoInstrumento
{
    public partial class Consultar : CustomPageBase
    {
        #region [Properties]

        private int TipoInstrumento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoInstrumento").ToInt32();
            }
        }

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            btnPesquisar.Click += BtnPesquisar_Click;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarTipoInstrumento();
                    CarregarArea();

                    if (TipoInstrumento > 0)
                        drpTipoInstrumento.SelectedValue = TipoInstrumento.ToString();
                    if (Unidade > 0)
                        drpArea.SelectedValue = Unidade.ToString();
                    if (!String.IsNullOrEmpty(Situacao))
                        rblSituacao.SelectedValue = Situacao.ToString();
                    if (PageIndex > 0)
                        ConsultarDados(PageIndex - 1);
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Remover":
                    if (e.Result == MessageBoxResult.Yes)
                        Remover(e.Parameters["CodigoSeqInstrumento"].ToInt32());
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            ConsultarDados(e.NewPage - 1);
        }

        private void BtnPesquisar_Click(object sender, EventArgs e)
        {
            ConsultarDados(0);
        }

        protected void rptInstrumentosPorUnidade_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Excluir":
                    {
                        var parametros = new Dictionary<string, object>();
                        parametros.Add("CodigoSeqInstrumento", e.CommandArgument);

                        MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente excluir o registro?",
                            "Deseja realmente excluir o registro?", MessageBoxButtons.YesNo, "Remover", parametros);
                    }
                    break;
                case "Detalhar":
                    WebHelper.Redirect(String.Format("~/Site/Gerenciar/TipoInstrumento/Detalhar.aspx?CodigoSeqInstrumento={0}&TipoInstrumento={1}&Unidade={2}&Situacao={3}&PageIndex={4}", e.CommandArgument, drpTipoInstrumento.SelectedValue, drpArea.SelectedValue, rblSituacao.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                case "Visuzalizar":
                    WebHelper.Redirect(String.Format("~/Site/Gerenciar/TipoInstrumento/Visualizar.aspx?CodigoSeqInstrumento={0}&TipoInstrumento={1}&Unidade={2}&Situacao={3}&PageIndex={4}", e.CommandArgument, drpTipoInstrumento.SelectedValue, drpArea.SelectedValue, rblSituacao.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region [Methods]

        private void CarregarTipoInstrumento()
        {
            drpTipoInstrumento.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTiposInstrumento();
            drpTipoInstrumento.DataTextField = "DescricaoTipoInstrumento";
            drpTipoInstrumento.DataValueField = "CodigoSeqTipoInstrumento";
            drpTipoInstrumento.DataBind();
            drpTipoInstrumento.Items.Insert(0, new ListItem("Todos", "0"));
        }

        private void CarregarArea()
        {
            drpArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarCorporativoUnidadeAtivo();
            drpArea.DataTextField = "DescricaoUnidade";
            drpArea.DataValueField = "CodigoUnidade";
            drpArea.DataBind();
            drpArea.Items.Insert(0, new ListItem("SEM ÁREA", "-1"));
            drpArea.Items.Insert(0, new ListItem("Todas", "0"));
        }

        private void ConsultarDados(int pageIndex)
        {
            Validate();

            if (IsValid)
            {
                try
                {
                    var instrumentos = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarInstrumentoPorTipoEUnidade(drpTipoInstrumento.SelectedValue.ToInt32(), drpArea.SelectedValue.ToInt32(), rblSituacao.SelectedValue, pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize, "CorporativoUnidade.DescricaoUnidade", false);

                    List<DTOInstrumento> source = instrumentos.Entities.ToList();

                    List<string> unidades = source.Where(u => u.CorporativoUnidade != null).OrderBy(u => u.CorporativoUnidade.DescricaoUnidade).Select(u => u.CorporativoUnidade.DescricaoUnidade).Distinct().ToList();

                    if (instrumentos.RowsCount > 0 && instrumentos.Where(i => i.CorporativoUnidade == null).Count() > 0)
                        unidades.Add("SEM ÁREA CADASTRADA");

                    Dictionary<string, IList<DTOInstrumento>> lista = new Dictionary<string, IList<DTOInstrumento>>();

                    foreach (var item in unidades)
                    {
                        if (item == "SEM ÁREA CADASTRADA")
                            lista.Add(item, source.Where(i => i.CorporativoUnidade == null).OrderBy(x => x.TipoInstrumento.DescricaoTipoInstrumento).ThenBy(x => x.DescricaoInstrumento).ToList());
                        else
                            lista.Add(item, source.Where(i => i.CorporativoUnidade != null && i.CorporativoUnidade.DescricaoUnidade == item).OrderBy(x => x.TipoInstrumento.DescricaoTipoInstrumento).ThenBy(x => x.DescricaoInstrumento).ToList());
                    }

                    rptInstrumentos.DataSource = lista;
                    rptInstrumentos.DataBind();

                    ucPaginatorConsulta.Visible = (instrumentos.RowsCount > 0);
                    ucPaginatorConsulta.TotalRecords = instrumentos.RowsCount;
                    ucPaginatorConsulta.PageIndex = pageIndex + 1;
                    ucPaginatorConsulta.DataBind();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        private void Remover(int CodigoSeqInstrumento)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarInstrumento(CodigoSeqInstrumento);
                MessageBox.ShowInformationMessage("Registro excluido com sucesso!");
                ConsultarDados(ucPaginatorConsulta.PageIndex - 1);
            }
            catch
            {
                MessageBox.ShowErrorMessage("Não foi possível excluir o registro devido ao fato de ele já ter vinculação.");
            }
        }

        #endregion
    }
}