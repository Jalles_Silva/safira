﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Detalhar.aspx.cs" Inherits="PGA.Presentation.Site.Gerenciar.TipoInstrumento.Detalhar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnSalvar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cadastro de Instrumentos</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoInstrumento" AssociatedControlID="drpTipoInstrumento" Text="Tipo de Instrumento *" runat="server" />
                                    <asp:DropDownList ID="drpTipoInstrumento" runat="server" CssClass="form-control" OnSelectedIndexChanged="drpTipoInstrumento_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="drpArea" Text="Area *" runat="server" />
                                    <asp:DropDownList ID="drpArea" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7 col-sm-7">
                                <div class="form-group">
                                    <asp:Label ID="lblNomeInstrumento" AssociatedControlID="txtNomeInstrumento" Text="Tema" runat="server" />
                                    <asp:TextBox ID="txtNomeInstrumento" runat="server" Rows="7" MaxLength="1000" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div id="divObjetivoEixo" runat="server" class="row">
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblObjetivoEixo" AssociatedControlID="drpObjetivoEixo" Text="Eixo Temático *" runat="server" />
                                    <asp:DropDownList ID="drpObjetivoEixo" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblObjetivo" AssociatedControlID="drpObjetivo" Text="Objetivo Estratégico" runat="server" />
                                    <asp:DropDownList ID="drpObjetivo" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblSituacao" AssociatedControlID="rblSituacao" Text="Situação" runat="server" />
                                    <asp:RadioButtonList ID="rblSituacao" runat="server" RepeatDirection="Horizontal" CssClass="radioListHoriz">
                                        <asp:ListItem Value="True" Text="Ativo" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="False" Text="Inativo"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>

                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="updBotoes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                        <asp:LinkButton ID="btnSalvar" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="Salvar" />                                        
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            FuncaoTela();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function ()
        {
            FuncaoTela();
        });

        function FuncaoTela() {

            $("#<%= txtNomeInstrumento.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 1000) {
                    $("#<%= txtNomeInstrumento.ClientID %>").val(text.substring(0, 1000));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtNomeInstrumento.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 1000) {
                    $("#<%= txtNomeInstrumento.ClientID %>").val(text.substring(0, 1000));
                    return false;
                }
                else {
                    return true;
                }
            });
            
        }
    </script>
</asp:Content>
