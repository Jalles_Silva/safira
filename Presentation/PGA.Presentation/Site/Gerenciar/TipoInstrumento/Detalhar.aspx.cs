﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using PGA.Common;

namespace PGA.Presentation.Site.Gerenciar.TipoInstrumento
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqInstrumento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqInstrumento").ToInt32();
            }
        }

        private int TipoInstrumento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoInstrumento").ToInt32();
            }
        }

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            drpTipoInstrumento.SelectedIndexChanged += drpTipoInstrumento_SelectedIndexChanged;
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarTipoInstrumento();
                    CarregarArea();
                    CarregarObjetivo(EnumTipoObjetivo.Objetivos);
                    CarregarObjetivoEixo();
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Voltar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        WebHelper.Redirect(String.Format("~/Site/Gerenciar/TipoInstrumento/Consultar.aspx?TipoInstrumento={0}&Unidade={1}&Situacao={2}&PageIndex={3}", TipoInstrumento, Unidade, Situacao, PageIndex));
                    }
                    break;
                case "Detalhar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        var CodSeqInstrumento = e.Parameters["CodSeqInstrumento"];
                        WebHelper.Redirect(String.Format("~/Site/Gerenciar/TipoInstrumento/Detalhar.aspx?CodigoSeqObjetivo={0}&TipoInstrumento={1}&Unidade={2}&Situacao={3}&PageIndex={4}", CodSeqInstrumento, TipoInstrumento, Unidade, Situacao, PageIndex));
                    }
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void drpTipoInstrumento_SelectedIndexChanged(object sender, EventArgs e)
        {
            AtualizarPaineis();
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            Validate();

            if (IsValid)
            {
                if (Int32.Parse(drpArea.SelectedValue) <= 0)
                {
                    MessageBox.ShowInformationMessage("Favor informar a Área.", "Erro");
                    return;
                }

                if (String.IsNullOrEmpty(txtNomeInstrumento.Text))
                {
                    MessageBox.ShowInformationMessage("Favor informar o " + lblNomeInstrumento.Text + " do registro.", "Erro");
                    return;
                }

                if (divObjetivoEixo.Visible && Int32.Parse(drpObjetivoEixo.SelectedValue) <= 0)
                {
                    MessageBox.ShowInformationMessage("Favor informar o Eixo Temático.", "Erro");
                    return;
                }

                Salvar();
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Voltar",
                "Deseja retornar para a página de consulta (dados não salvos serão perdidos)?", MessageBoxButtons.YesNo, "Voltar");
        }

        #endregion

        #region [Methods]

        private void CarregarTipoInstrumento()
        {
            drpTipoInstrumento.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTiposInstrumento();
            drpTipoInstrumento.DataTextField = "DescricaoTipoInstrumento";
            drpTipoInstrumento.DataValueField = "CodigoSeqTipoInstrumento";
            drpTipoInstrumento.DataBind();
        }

        private void CarregarArea()
        {
            drpArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarCorporativoUnidadeAtivo();
            drpArea.DataTextField = "DescricaoUnidade";
            drpArea.DataValueField = "CodigoUnidade";
            drpArea.DataBind();
            drpArea.Items.Insert(0, new ListItem("Selecione", "0"));
        }

        private void CarregarObjetivo(EnumTipoObjetivo tipo)
        {
            drpObjetivo.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarObjetivosPorTipo(tipo);
            drpObjetivo.DataTextField = "NomeObjetivo";
            drpObjetivo.DataValueField = "CodigoSeqObjetivo";
            drpObjetivo.DataBind();
            drpObjetivo.Items.Insert(0, new ListItem("Selecione", "0"));
        }

        private void CarregarObjetivoEixo()
        {
            drpObjetivoEixo.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarObjetivosPorTipo(EnumTipoObjetivo.Eixos);
            drpObjetivoEixo.DataTextField = "NomeObjetivo";
            drpObjetivoEixo.DataValueField = "CodigoSeqObjetivo";
            drpObjetivoEixo.DataBind();
            drpObjetivoEixo.Items.Insert(0, new ListItem("Selecione", "0"));
        }

        private void AtualizarPaineis()
        {
            divObjetivoEixo.Visible = false;

            switch (drpTipoInstrumento.SelectedItem.Text)
            {
                case "Agenda Regulatória":
                    lblNomeInstrumento.Text = "Tema";
                    lblObjetivo.Text = "Objetivo Estratégico";
                    divObjetivoEixo.Visible = true;
                    CarregarObjetivo(EnumTipoObjetivo.Objetivos);
                    break;
                case "Desburocratização":
                    lblNomeInstrumento.Text = "Ação";
                    lblObjetivo.Text = "Objetivo Estratégico";
                    CarregarObjetivo(EnumTipoObjetivo.Objetivos);
                    break;
                case "Gestão de Risco":
                    lblNomeInstrumento.Text = "Processo";
                    lblObjetivo.Text = "Macroprocesso";
                    CarregarObjetivo(EnumTipoObjetivo.Macroprocessos);
                    break;
                case "Integridade":
                    lblNomeInstrumento.Text = "Ação";
                    lblObjetivo.Text = "Aspecto de Integridade";
                    CarregarObjetivo(EnumTipoObjetivo.Aspectos);
                    break;
                default:
                    break;
            }
        }

        private void Salvar()
        {
            try
            {
                DTOInstrumento registro = new DTOInstrumento();
                registro.CodigoSeqInstrumento = CodigoSeqInstrumento;
                registro.DescricaoInstrumento = txtNomeInstrumento.Text;
                registro.Ativo = Boolean.Parse(rblSituacao.SelectedValue);

                var tipo = new DTOTipoInstrumento();
                tipo.CodigoSeqTipoInstrumento = drpTipoInstrumento.SelectedValue.ToInt32();
                registro.TipoInstrumento = tipo;

                var unidade = new DTOCorporativoUnidade();
                unidade.CodigoUnidade = drpArea.SelectedValue.ToInt32();
                registro.CorporativoUnidade = unidade;

                var objetivo = new DTOObjetivo();
                objetivo.CodigoSeqObjetivo = drpObjetivo.SelectedValue.ToInt32();
                registro.Objetivo = objetivo;

                if (divObjetivoEixo.Visible)
                {
                    var objetivoEixo = new DTOObjetivo();
                    objetivoEixo.CodigoSeqObjetivo = drpObjetivoEixo.SelectedValue.ToInt32();
                    registro.ObjetivoEixoTematico = objetivoEixo;
                }

                var CodSeqInstrumento = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarInstrumento(registro);

                var param = new Dictionary<string, object>();
                param.Add("CodSeqInstrumento", CodSeqInstrumento);

                MessageBox.ShowInformationMessage("Registro salvo com sucesso.", "Detalhar", param);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqInstrumento > 0)
                {
                    drpTipoInstrumento.Enabled = false;

                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterInstrumento(CodigoSeqInstrumento);

                    txtNomeInstrumento.Text = registro.DescricaoInstrumento;
                    rblSituacao.SelectedValue = registro.Ativo.ToString();

                    if (registro.TipoInstrumento != null)
                        drpTipoInstrumento.SelectedValue = registro.TipoInstrumento.CodigoSeqTipoInstrumento.ToString();

                    if (registro.CorporativoUnidade != null && drpArea.Items.Contains(new ListItem(registro.CorporativoUnidade.DescricaoUnidade, registro.CorporativoUnidade.CodigoUnidade.ToString())))
                        drpArea.SelectedValue = registro.CorporativoUnidade.CodigoUnidade.ToString();

                    if (registro.Objetivo != null && drpObjetivo.Items.Contains(new ListItem(registro.Objetivo.NomeObjetivo, registro.Objetivo.CodigoSeqObjetivo.ToString())))
                        drpObjetivo.SelectedValue = registro.Objetivo.CodigoSeqObjetivo.ToString();

                    if (registro.ObjetivoEixoTematico != null && drpObjetivoEixo.Items.Contains(new ListItem(registro.ObjetivoEixoTematico.NomeObjetivo, registro.ObjetivoEixoTematico.CodigoSeqObjetivo.ToString())))
                        drpObjetivoEixo.SelectedValue = registro.ObjetivoEixoTematico.CodigoSeqObjetivo.ToString();

                    AtualizarPaineis();
                }
                else
                    drpTipoInstrumento.Enabled = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}