﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using PGA.Common;

namespace PGA.Presentation.Site.Gerenciar.TipoInstrumento
{
    public partial class Visualizar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqInstrumento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqInstrumento").ToInt32();
            }
        }

        private int TipoInstrumento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoInstrumento").ToInt32();
            }
        }

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }
        
        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Gerenciar/TipoInstrumento/Consultar.aspx?TipoInstrumento={0}&Unidade={1}&Situacao={2}&PageIndex={3}", TipoInstrumento, Unidade, Situacao, PageIndex));
        }

        #endregion

        #region [Methods]

        private void AtualizarPaineis()
        {
            divObjetivoEixo.Visible = false;

            switch (lblTipoInstrumento.Text)
            {
                case "Agenda Regulatória":
                    lblNomeInstrumentoCampo.Text = "TEMA: ";
                    lblObjetivoCampo.Text = "OBJETIVO ESTRATÉGICO: ";
                    divObjetivoEixo.Visible = true;
                    break;
                case "Desburocratização":
                    lblNomeInstrumentoCampo.Text = "AÇÃO: ";
                    lblObjetivoCampo.Text = "OBJETIVO ESTRATÉGICO: ";
                    break;
                case "Gestão de Risco":
                    lblNomeInstrumentoCampo.Text = "PROCESSO: ";
                    lblObjetivoCampo.Text = "MACROPROCESSO: ";
                    break;
                case "Integridade":
                    lblNomeInstrumentoCampo.Text = "AÇÃO: ";
                    lblObjetivoCampo.Text = "ASPECTO DE INTEGRIDADE: ";
                    break;
                default:
                    break;
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqInstrumento > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterInstrumento(CodigoSeqInstrumento);

                    lblNomeInstrumento.Text = registro.DescricaoInstrumento;
                    lblTipoInstrumento.Text = registro.TipoInstrumento.DescricaoTipoInstrumento;
                    lblSituacao.Text = registro.Ativo ? "Ativo" : "Inativo";

                    if (registro.CorporativoUnidade != null)
                        lblArea.Text = registro.CorporativoUnidade.DescricaoUnidade;
                    else
                        lblArea.Text = "Não cadastrada";

                    if (registro.Objetivo != null)
                        lblObjetivo.Text = registro.Objetivo.NomeObjetivo;
                    else
                        lblObjetivo.Text = "Não cadastrado";

                    if (registro.ObjetivoEixoTematico != null)
                        lblObjetivoEixo.Text = registro.ObjetivoEixoTematico.NomeObjetivo;
                    else
                        lblObjetivoEixo.Text = "Não cadastrado";

                    AtualizarPaineis();
                }
                else
                    WebHelper.Redirect("~/Site/Gerenciar/TipoInstrumento/Consultar.aspx");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}