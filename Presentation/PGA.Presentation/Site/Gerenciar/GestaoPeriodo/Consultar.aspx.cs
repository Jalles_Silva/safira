﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using SQFramework.Core.Reflection;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;

namespace PGA.Presentation.Site.Gerenciar.GestaoPeriodo
{
    public partial class Consultar : CustomPageBase
    {
        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            gdvResultadoConsulta.RowCommand += new GridViewCommandEventHandler(gdvResultadoConsulta_RowCommand);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            btnAbrirExercicio.Click += btnAbrirExercicio_Click;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ConsultarDados(0);
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            try
            {
                switch (e.Command)
                {
                    case "AbrirExercicio":
                        if (e.Result == MessageBoxResult.Yes)
                            AbrirExercicio();
                        break;
                    case "PromoverFase":
                        if (e.Result == MessageBoxResult.Yes)
                            PromoverFaseExercicio(e.Parameters["Ano"].ToInt32());
                        break;
                    case "Recarregar":
                        WebHelper.Redirect("~/Site/Gerenciar/GestaoPeriodo/Consultar.aspx");
                        break;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            ConsultarDados(e.NewPage - 1);
        }

        protected void gdvResultadoConsulta_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("PromoverFase"))
                {
                    var parametros = new Dictionary<string, object>();
                    parametros.Add("Ano", e.CommandArgument.ToInt32());
                    
                    GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).NamingContainer;
                    var DescricaoFase = "";

                    if (row != null)
                    {
                        DescricaoFase = row.Cells[1].Text;
                    } 

                    MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Salvar",
                    "ATENÇÃO: Após confirmar essa operação, não será mais possível o " + DescricaoFase + ". Esta alteração NÃO PODERÁ SER DESFEITA!", MessageBoxButtons.YesNo, "PromoverFase", parametros);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void btnAbrirExercicio_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Salvar",
                   "Esta operação não poderá ser desfeita! Tem certeza?", MessageBoxButtons.YesNo, "AbrirExercicio");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion

        #region [Methods]

        private void ConsultarDados(int pageIndex)
        {
            Validate();

            if (IsValid)
            {
                try
                {
                    var dadosPesquisados = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService")
                        .ListarExercicios();

                    gdvResultadoConsulta.DataSource = dadosPesquisados.OrderByDescending(i => i.Ano);
                    gdvResultadoConsulta.DataBind();

                    //ucPaginatorConsulta.Visible = (dadosPesquisados.RowsCount > 0);
                    //ucPaginatorConsulta.TotalRecords = dadosPesquisados.RowsCount;
                    ucPaginatorConsulta.PageIndex = pageIndex + 1;
                    ucPaginatorConsulta.DataBind();
                    upPnlResultadoConsulta.Update();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        private void AbrirExercicio()
        {
            this.ObterPGAService().AbrirExercicio(SCAApplicationContext.Usuario.Nome);
            MessageBox.ShowInformationMessage("Registro salvo com sucesso.", "Recarregar");
        }

        private void PromoverFaseExercicio(int ano)
        {
            this.ObterPGAService().PromoverFaseExercicio(ano, SCAApplicationContext.Usuario.Nome);
            MessageBox.ShowInformationMessage("Registro salvo com sucesso.", "Recarregar");
        }

        #endregion
    }
}