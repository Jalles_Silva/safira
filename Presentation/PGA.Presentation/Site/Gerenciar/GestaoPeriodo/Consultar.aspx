﻿<%@ Page Title="SAFIRA - Consultar GestaoPeriodo" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false"
    CodeBehind="Consultar.aspx.cs" Inherits="PGA.Presentation.Site.Gerenciar.GestaoPeriodo.Consultar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Gestão de Períodos</h3>
                    </div>
                    <div class="panel-body">
                        <asp:UpdatePanel ID="upPnlResultadoConsulta" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="area-table">
                                    <asp:GridView ID="gdvResultadoConsulta" runat="server" AllowSorting="True"
                                        Width="100%" AutoGenerateColumns="False"
                                        EmptyDataText="Nenhum registro encontrado.">
                                        <EmptyDataTemplate>Nenhum registro encontrado.</EmptyDataTemplate>
                                        <AlternatingRowStyle CssClass="alternate" />
                                        <Columns>
                                            <asp:BoundField HeaderText="Exercício" DataField="Ano" />
                                            <asp:BoundField HeaderText="Fase" DataField="Fase.DescricaoFase" />
                                            <asp:TemplateField HeaderText="Ações" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkPromoverFase" runat="server" CommandArgument='<%# Eval("Ano") %>'
                                                        CommandName="PromoverFase" CausesValidation="false" CssClass="glyphicon glyphicon-circle-arrow-right"
                                                        Visible='<%# Convert.ToInt32(Eval("Fase.CodigoSeqFase")) == (int)PGA.Common.FaseEnum.Encerrado ? false : true %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="50px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <div class="paginator">
                                    <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-horizontal row pull-right"">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <asp:LinkButton ID="btnAbrirExercicio" runat="server" SkinID="btnAbrirExercicio" CausesValidation="false"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

</asp:Content>