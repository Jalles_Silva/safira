﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Detalhar.aspx.cs" Inherits="PGA.Presentation.Site.Gerenciar.RegistroBasico.Detalhar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnSalvar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cadastro Registros Básicos</h3>
                    </div>
                    <div class="panel-body">
                       <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoObjetivo" AssociatedControlID="drpTiposObjetivo" Text="Registro Básico" runat="server" />
                                    <asp:DropDownList ID="drpTiposObjetivo" runat="server" CssClass="form-control" OnSelectedIndexChanged="drpTiposObjetivo_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-1 col-sm-1">
                                <div class="form-group">
                                    <asp:Label ID="lblNumObjetivo" AssociatedControlID="txtNumObjetivo" Text="Número" runat="server" />
                                    <asp:TextBox ID="txtNumObjetivo" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblNomeObjetivo" AssociatedControlID="txtNomeObjetivo" Text="Aspecto" runat="server" />
                                    <asp:TextBox ID="txtNomeObjetivo" runat="server" Rows="1" MaxLength="200" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div id="divDescricao" runat="server" class="row">
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoObjetivo" AssociatedControlID="txtDescricaoObjetivo" Text="Descrição" runat="server" />
                                    <asp:TextBox ID="txtDescricaoObjetivo" runat="server" Rows="7" MaxLength="1000" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div id="divPerspectiva" runat="server" class="row" visible="false">
                            <div class="col-xs-5 col-sm-5">
                                <div class="form-group">
                                    <asp:Label ID="lblPerspectiva" AssociatedControlID="drpPerspectiva" Text="Perspectiva" runat="server" />
                                    <asp:DropDownList ID="drpPerspectiva" runat="server" CssClass="form-control" OnSelectedIndexChanged="drpPerspectiva_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div id="divTema" runat="server" class="row" visible="false">
                            <div class="col-xs-5 col-sm-5">
                                <div class="form-group">
                                    <asp:Label ID="lblTema" AssociatedControlID="drpTema" Text="Tema" runat="server" />
                                    <asp:DropDownList ID="drpTema" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblSituacao" AssociatedControlID="rblSituacao" Text="Situação" runat="server" />
                                    <asp:RadioButtonList ID="rblSituacao" runat="server" RepeatDirection="Horizontal" CssClass="radioListHoriz">
                                        <asp:ListItem Value="True" Text="Ativo" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="False" Text="Inativo"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>

                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="updBotoes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                        <asp:LinkButton ID="btnSalvar" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="Salvar" />                                        
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            FuncaoTela();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function ()
        {
            FuncaoTela();
        });

        function FuncaoTela() {

            $("#<%= txtNomeObjetivo.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 200) {
                    $("#<%= txtNomeObjetivo.ClientID %>").val(text.substring(0, 200));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtNomeObjetivo.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 200) {
                    $("#<%= txtNomeObjetivo.ClientID %>").val(text.substring(0, 200));
                    return false;
                }
                else {
                    return true;
                }
            });

            $("#<%= txtDescricaoObjetivo.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 1000) {
                    $("#<%= txtDescricaoObjetivo.ClientID %>").val(text.substring(0, 1000));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtDescricaoObjetivo.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 1000) {
                    $("#<%= txtDescricaoObjetivo.ClientID %>").val(text.substring(0, 1000));
                    return false;
                }
                else {
                    return true;
                }
            });
            
        }
    </script>
</asp:Content>

