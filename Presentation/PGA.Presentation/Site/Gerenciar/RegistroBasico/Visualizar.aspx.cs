﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using PGA.Common;

namespace PGA.Presentation.Site.Gerenciar.RegistroBasico
{
    public partial class Visualizar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqObjetivo
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqObjetivo").ToInt32();
            }
        }

        private int TiposObjetivo
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TiposObjetivo").ToInt32();
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Gerenciar/RegistroBasico/Consultar.aspx?TiposObjetivo={0}&Situacao={1}&PageIndex={2}", TiposObjetivo, Situacao, PageIndex));
        }

        #endregion

        #region [Methods]

        private void AtualizarPaineis()
        {
            divDescricao.Visible = false;
            divTema.Visible = false;
            divPerspectiva.Visible = false;

            switch (lblTipoObjetivo.Text)
            {
                case "Aspectos de Integridade":
                    lblNomeObjetivoCampo.Text = "ASPECTO: ";
                    lblDescricaoObjetivoCampo.Text = "DESCRIÇÃO: ";
                    divDescricao.Visible = true;
                    break;
                case "Eixos Temáticos":
                    lblNomeObjetivoCampo.Text = "DESCRIÇÃO: ";
                    break;
                case "Macroprocessos":
                    lblNomeObjetivoCampo.Text = "MACROPROCESSO: ";
                    lblDescricaoObjetivoCampo.Text = "ABRANGÊNCIA: ";
                    divDescricao.Visible = true;
                    break;
                case "Objetivos Estratégicos":
                    lblNomeObjetivoCampo.Text = "OBJETIVO: ";
                    divTema.Visible = true;
                    divPerspectiva.Visible = true;
                    break;
                default:
                    break;
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqObjetivo > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterObjetivo(CodigoSeqObjetivo);

                    lblNumObjetivo.Text = registro.NuObjetivo;
                    lblNomeObjetivo.Text = registro.NomeObjetivo;
                    lblTipoObjetivo.Text = registro.TipoObjetivo.DescricaoTipoObjetivo;
                    lblSituacao.Text = registro.Ativo ? "Ativo" : "Inativo";

                    if (!String.IsNullOrEmpty(registro.DescricaoObjetivo))
                        lblDescricaoObjetivo.Text = registro.DescricaoObjetivo;
                    else
                        lblDescricaoObjetivo.Text = "Não cadastrado";

                    if (registro.Tema != null)
                        lblTema.Text = registro.Tema.DescricaoTema;

                    if (registro.Tema != null && registro.Tema.Perspectiva != null)
                        lblPerspectiva.Text = registro.Tema.Perspectiva.DescricaoPerspectiva;

                    AtualizarPaineis();
                }
                else
                    WebHelper.Redirect("~/Site/Gerenciar/TipoAlinhamento/Consultar.aspx");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}