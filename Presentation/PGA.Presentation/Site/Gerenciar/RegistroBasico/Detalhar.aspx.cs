﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using PGA.Common;

namespace PGA.Presentation.Site.Gerenciar.RegistroBasico
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqObjetivo
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqObjetivo").ToInt32();
            }
        }

        private int TiposObjetivo
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TiposObjetivo").ToInt32();
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            drpTiposObjetivo.SelectedIndexChanged += drpTiposObjetivo_SelectedIndexChanged;
            drpPerspectiva.SelectedIndexChanged += drpPerspectiva_SelectedIndexChanged;
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarTipoObjetivo();
                    CarregarPerspectiva();
                    CarregarTema(drpPerspectiva.SelectedValue.ToInt32());
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Voltar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        WebHelper.Redirect(String.Format("~/Site/Gerenciar/RegistroBasico/Consultar.aspx?TiposObjetivo={0}&Situacao={1}&PageIndex={2}", TiposObjetivo, Situacao, PageIndex));
                    }
                    break;
                case "Detalhar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        var CodSeqObjetivo = e.Parameters["CodSeqObjetivo"];
                        WebHelper.Redirect(String.Format("~/Site/Gerenciar/RegistroBasico/Detalhar.aspx?CodigoSeqObjetivo={0}&TiposObjetivo={1}&Situacao={2}&PageIndex={3}", CodSeqObjetivo, TiposObjetivo, Situacao, PageIndex));
                    }
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void drpTiposObjetivo_SelectedIndexChanged(object sender, EventArgs e)
        {
            AtualizarPaineis();
        }

        protected void drpPerspectiva_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarTema(drpPerspectiva.SelectedValue.ToInt32());
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            Validate();

            if (IsValid)
            {
                if (String.IsNullOrEmpty(txtNomeObjetivo.Text))
                {
                    MessageBox.ShowInformationMessage("Favor informar o(a) " + lblNomeObjetivo.Text + " do registro.", "Erro");
                    return;
                }

                switch (drpTiposObjetivo.SelectedItem.Text)
                {
                    case "Aspectos de Integridade":
                        if (String.IsNullOrEmpty(txtDescricaoObjetivo.Text))
                        {
                            MessageBox.ShowInformationMessage("Favor informar a Descrição.", "Erro");
                            return;
                        }
                        break;
                    case "Eixos Temáticos":
                        break;
                    case "Macroprocessos":
                        if (String.IsNullOrEmpty(txtDescricaoObjetivo.Text))
                        {
                            MessageBox.ShowInformationMessage("Favor informar a Abrangência.", "Erro");
                            return;
                        }
                        break;
                    case "Objetivos Estratégicos":
                        if (String.IsNullOrEmpty(drpTema.SelectedValue))
                        {
                            MessageBox.ShowInformationMessage("Favor informar o Tema.", "Erro");
                            return;
                        }
                        if (String.IsNullOrEmpty(drpPerspectiva.SelectedValue))
                        {
                            MessageBox.ShowInformationMessage("Favor informar a Perspectiva.", "Erro");
                            return;
                        }
                        break;
                    default:
                        break;
                }

                Salvar();
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Voltar",
                "Deseja retornar para a página de consulta (dados não salvos serão perdidos)?", MessageBoxButtons.YesNo, "Voltar");
        }

        #endregion

        #region [Methods]

        private void CarregarTipoObjetivo()
        {
            drpTiposObjetivo.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTiposObjetivo();
            drpTiposObjetivo.DataTextField = "DescricaoTipoObjetivo";
            drpTiposObjetivo.DataValueField = "CodigoSeqTipoObjetivo";
            drpTiposObjetivo.DataBind();
        }

        private void CarregarPerspectiva()
        {
            drpPerspectiva.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarPerspectivas();
            drpPerspectiva.DataTextField = "DescricaoPerspectiva";
            drpPerspectiva.DataValueField = "CodigoSeqPerspectiva";
            drpPerspectiva.DataBind();
        }

        private void CarregarTema(int perspectiva)
        {
            drpTema.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTemasPorPerspectiva(perspectiva);
            drpTema.DataTextField = "DescricaoTema";
            drpTema.DataValueField = "CodigoSeqTema";
            drpTema.DataBind();
        }

        private void AtualizarPaineis()
        {
            divDescricao.Visible = false;
            divTema.Visible = false;
            divPerspectiva.Visible = false;

            switch (drpTiposObjetivo.SelectedItem.Text)
            {
                case "Aspectos de Integridade":
                    lblNomeObjetivo.Text = "Aspecto";
                    lblDescricaoObjetivo.Text = "Descrição";
                    divDescricao.Visible = true;
                    break;
                case "Eixos Temáticos":
                    lblNomeObjetivo.Text = "Descrição";
                    break;
                case "Macroprocessos":
                    lblNomeObjetivo.Text = "Macroprocesso";
                    lblDescricaoObjetivo.Text = "Abrangência";
                    divDescricao.Visible = true;
                    break;
                case "Objetivos Estratégicos":
                    lblNomeObjetivo.Text = "Objetivo";
                    divTema.Visible = true;
                    divPerspectiva.Visible = true;
                    break;
                default:
                    break;
            }
        }

        private void Salvar()
        {
            try
            {
                DTOObjetivo registro = new DTOObjetivo();
                registro.CodigoSeqObjetivo = CodigoSeqObjetivo;
                registro.NuObjetivo = txtNumObjetivo.Text;
                registro.NomeObjetivo = txtNomeObjetivo.Text;
                registro.Ativo = Boolean.Parse(rblSituacao.SelectedValue);

                if (divDescricao.Visible)
                    registro.DescricaoObjetivo = txtDescricaoObjetivo.Text;

                var tipo = new DTOTipoObjetivo();
                tipo.CodigoSeqTipoObjetivo = drpTiposObjetivo.SelectedValue.ToInt32();
                registro.TipoObjetivo = tipo;

                if(divTema.Visible)
                {
                    var tema = new DTOTema();
                    tema.CodigoSeqTema = drpTema.SelectedValue.ToInt32();
                    registro.Tema = tema;
                }

                var CodSeqObjetivo = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarObjetivo(registro);

                var param = new Dictionary<string, object>();
                param.Add("CodSeqObjetivo", CodSeqObjetivo);

                MessageBox.ShowInformationMessage("Registro salvo com sucesso.", "Detalhar", param);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqObjetivo > 0)
                {
                    drpTiposObjetivo.Enabled = false;

                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterObjetivo(CodigoSeqObjetivo);

                    txtNumObjetivo.Text = registro.NuObjetivo;
                    txtNomeObjetivo.Text = registro.NomeObjetivo;
                    txtDescricaoObjetivo.Text = registro.DescricaoObjetivo;
                    rblSituacao.SelectedValue = registro.Ativo.ToString();

                    if (registro.TipoObjetivo != null)
                        drpTiposObjetivo.SelectedValue = registro.TipoObjetivo.CodigoSeqTipoObjetivo.ToString();

                    if (registro.Tema != null && drpTema.Items.Contains(new ListItem(registro.Tema.DescricaoTema, registro.Tema.CodigoSeqTema.ToString())))
                        drpTema.SelectedValue = registro.Tema.CodigoSeqTema.ToString();

                    if (registro.Tema != null && registro.Tema.Perspectiva != null && drpPerspectiva.Items.Contains(new ListItem(registro.Tema.Perspectiva.DescricaoPerspectiva, registro.Tema.Perspectiva.CodigoSeqPerspectiva.ToString())))
                        drpPerspectiva.SelectedValue = registro.Tema.Perspectiva.CodigoSeqPerspectiva.ToString();

                    AtualizarPaineis();
                }
                else
                    drpTiposObjetivo.Enabled = true;
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}