﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using PGA.Common;

namespace PGA.Presentation.Site.Gerenciar.Tema
{
    public partial class Visualizar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqTema
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqTema").ToInt32();
            }
        }

        private int Perspectiva
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Perspectiva").ToInt32();
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Gerenciar/Tema/Consultar.aspx?Perspectiva={0}&Situacao={1}&PageIndex={2}", Perspectiva, Situacao, PageIndex));
        }

        #endregion

        #region [Methods]

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqTema > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterTema(CodigoSeqTema);

                    lblNumTema.Text = registro.NuTema;
                    lblDescricaoTema.Text = registro.DescricaoTema;
                    lblPerspectiva.Text = registro.Perspectiva.DescricaoPerspectiva;
                    lblSituacao.Text = registro.Ativo ? "Ativo" : "Inativo";
                }
                else
                    WebHelper.Redirect("~/Site/Gerenciar/Perspectiva/Consultar.aspx");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}