﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using PGA.Common;

namespace PGA.Presentation.Site.Gerenciar.Tema
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqTema
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqTema").ToInt32();
            }
        }

        private int Perspectiva
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Perspectiva").ToInt32();
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarPerspectiva();
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Voltar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        WebHelper.Redirect(String.Format("~/Site/Gerenciar/Tema/Consultar.aspx?Perspectiva={0}&Situacao={1}&PageIndex={2}", Perspectiva, Situacao, PageIndex));
                    }
                    break;
                case "Detalhar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        var CodSeqTema = e.Parameters["CodSeqTema"];
                        WebHelper.Redirect(String.Format("~/Site/Gerenciar/Tema/Detalhar.aspx?CodigoSeqTema={0}&Perspectiva={1}&Situacao={2}&PageIndex={3}", CodSeqTema, Perspectiva, Situacao, PageIndex));
                    }
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            Validate();

            if (IsValid)
            {
                if (String.IsNullOrEmpty(txtDescricaoTema.Text))
                {
                    MessageBox.ShowInformationMessage("Favor informar a Tema.", "Erro");
                    return;
                }

                Salvar();
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Voltar",
                "Deseja retornar para a página de consulta (dados não salvos serão perdidos)?", MessageBoxButtons.YesNo, "Voltar");
        }

        #endregion

        #region [Methods]

        private void CarregarPerspectiva()
        {
            drpPerspectiva.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarPerspectivas();
            drpPerspectiva.DataTextField = "DescricaoPerspectiva";
            drpPerspectiva.DataValueField = "CodigoSeqPerspectiva";
            drpPerspectiva.DataBind();
        }

        private void Salvar()
        {
            try
            {
                DTOTema registro = new DTOTema();
                registro.CodigoSeqTema = CodigoSeqTema;
                registro.NuTema = txtNumTema.Text;
                registro.DescricaoTema = txtDescricaoTema.Text;
                registro.Ativo = Boolean.Parse(rblSituacao.SelectedValue);

                var perspectiva = new DTOPerspectiva();
                perspectiva.CodigoSeqPerspectiva = drpPerspectiva.SelectedValue.ToInt32();
                registro.Perspectiva = perspectiva;

                var CodSeqTema = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarTema(registro);

                var param = new Dictionary<string, object>();
                param.Add("CodSeqTema", CodSeqTema);

                MessageBox.ShowInformationMessage("Registro salvo com sucesso.", "Detalhar", param);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqTema > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterTema(CodigoSeqTema);

                    txtNumTema.Text = registro.NuTema;
                    txtDescricaoTema.Text = registro.DescricaoTema;
                    rblSituacao.SelectedValue = registro.Ativo.ToString();

                    if (registro.Perspectiva != null && drpPerspectiva.Items.Contains(new ListItem(registro.Perspectiva.DescricaoPerspectiva, registro.Perspectiva.CodigoSeqPerspectiva.ToString())))
                        drpPerspectiva.SelectedValue = registro.Perspectiva.CodigoSeqPerspectiva.ToString();
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}