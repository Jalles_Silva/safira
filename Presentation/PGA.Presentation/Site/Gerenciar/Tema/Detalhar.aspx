﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Detalhar.aspx.cs" Inherits="PGA.Presentation.Site.Gerenciar.Tema.Detalhar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnSalvar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cadastro Tema</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-1 col-sm-1">
                                <div class="form-group">
                                    <asp:Label ID="lblNumTema" AssociatedControlID="txtNumTema" Text="Número" runat="server" />
                                    <asp:TextBox ID="txtNumTema" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div id="divDescricao" runat="server" class="row">
                            <div class="col-xs-5 col-sm-5">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoTema" AssociatedControlID="txtDescricaoTema" Text="Tema" runat="server" />
                                    <asp:TextBox ID="txtDescricaoTema" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblPerspectiva" AssociatedControlID="drpPerspectiva" Text="Perspectiva" runat="server" />
                                    <asp:DropDownList ID="drpPerspectiva" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblSituacao" AssociatedControlID="rblSituacao" Text="Situação" runat="server" />
                                    <asp:RadioButtonList ID="rblSituacao" runat="server" RepeatDirection="Horizontal" CssClass="radioListHoriz">
                                        <asp:ListItem Value="True" Text="Ativo" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="False" Text="Inativo"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>

                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="updBotoes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                        <asp:LinkButton ID="btnSalvar" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="Salvar" />                                        
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </asp:Panel>
</asp:Content>
