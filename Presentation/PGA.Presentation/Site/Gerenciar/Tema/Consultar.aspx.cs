﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;

namespace PGA.Presentation.Site.Gerenciar.Tema
{
    public partial class Consultar : CustomPageBase
    {
        #region [Properties]

        private int Perspectiva
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Perspectiva").ToInt32();
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            btnPesquisar.Click += BtnPesquisar_Click;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarPerspectiva();

                    if (Perspectiva > 0)
                        drpPerspectiva.SelectedValue = Perspectiva.ToString();
                    if (!String.IsNullOrEmpty(Situacao))
                        rblSituacao.SelectedValue = Situacao.ToString();
                    if (PageIndex > 0)
                        ConsultarDados(PageIndex - 1);
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Remover":
                    if (e.Result == MessageBoxResult.Yes)
                        Remover(e.Parameters["CodigoSeqTema"].ToInt32());
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            ConsultarDados(e.NewPage - 1);
        }

        private void BtnPesquisar_Click(object sender, EventArgs e)
        {
            ConsultarDados(0);
        }

        protected void grdTema_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Excluir":
                    {
                        var parametros = new Dictionary<string, object>();
                        parametros.Add("CodigoSeqTema", e.CommandArgument);

                        MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente excluir o registro?",
                            "Deseja realmente excluir o registro?", MessageBoxButtons.YesNo, "Remover", parametros);
                    }
                    break;
                case "Detalhar":
                    WebHelper.Redirect(String.Format("~/Site/Gerenciar/Tema/Detalhar.aspx?CodigoSeqTema={0}&Perspectiva={1}&Situacao={2}&PageIndex={3}", e.CommandArgument, drpPerspectiva.SelectedValue, rblSituacao.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                case "Visuzalizar":
                    WebHelper.Redirect(String.Format("~/Site/Gerenciar/Tema/Visualizar.aspx?CodigoSeqTema={0}&Perspectiva={1}&Situacao={2}&PageIndex={3}", e.CommandArgument, drpPerspectiva.SelectedValue, rblSituacao.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region [Methods]

        private void CarregarPerspectiva()
        {
            drpPerspectiva.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarPerspectivas();
            drpPerspectiva.DataTextField = "DescricaoPerspectiva";
            drpPerspectiva.DataValueField = "CodigoSeqPerspectiva";
            drpPerspectiva.DataBind();
            drpPerspectiva.Items.Insert(0, new ListItem("Todas", "0"));
        }

        private void ConsultarDados(int pageIndex)
        {
            Validate();

            if (IsValid)
            {
                try
                {
                    var temas = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTemasPorPerspectivasESituacao(drpPerspectiva.SelectedValue.ToInt32(), rblSituacao.SelectedValue, pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize, "CodigoSeqTema", true);

                    grdTema.DataSource = temas;
                    grdTema.DataBind();

                    ucPaginatorConsulta.Visible = (temas.RowsCount > 0);
                    ucPaginatorConsulta.TotalRecords = temas.RowsCount;
                    ucPaginatorConsulta.PageIndex = pageIndex + 1;
                    ucPaginatorConsulta.DataBind();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        private void Remover(int CodigoSeqTema)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarTema(CodigoSeqTema);
                MessageBox.ShowInformationMessage("Registro excluido com sucesso!");
                ConsultarDados(ucPaginatorConsulta.PageIndex - 1);
            }
            catch
            {
                MessageBox.ShowErrorMessage("Não foi possível excluir o registro devido ao fato de ele já ter vinculação.");
            }
        }

        #endregion
    }
}