﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Consultar.aspx.cs" Inherits="PGA.Presentation.Site.Gerenciar.Tema.Consultar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Pesquisar Tema</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblPerspectiva" AssociatedControlID="drpPerspectiva" Text="Perspectiva" runat="server" />
                                    <asp:DropDownList ID="drpPerspectiva" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblSituacao" AssociatedControlID="rblSituacao" Text="Situação" runat="server" />
                                    <asp:RadioButtonList ID="rblSituacao" runat="server" RepeatDirection="Horizontal" CssClass="radioListHoriz">
                                        <asp:ListItem Value="" Text="Todos" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="True" Text="Ativo"></asp:ListItem>
                                        <asp:ListItem Value="False" Text="Inativo"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-12">
                                    <asp:LinkButton ID="btnPesquisar" runat="server" SkinID="btnPesquisar" CausesValidation="false" />
                                    <asp:HyperLink ID="btnLimpar" runat="server" SkinID="btnLimpar" CausesValidation="false"
                                        NavigateUrl="~/Site/Gerenciar/Tema/Consultar.aspx" />
                                    <asp:HyperLink ID="btnNovo" runat="server" SkinID="btnNovo" CausesValidation="false"
                                        NavigateUrl="~/Site/Gerenciar/Tema/Detalhar.aspx" />
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="upPnlResultadoConsulta" runat="server">
                            <ContentTemplate>
                                <div class="area-table">
                                    <asp:GridView ID="grdTema" runat="server" AutoGenerateColumns="False" EmptyDataText="Nenhum registro encontrado." OnRowCommand="grdTema_RowCommand">
                                        <EmptyDataTemplate>Nenhum registro encontrado.</EmptyDataTemplate>
                                        <AlternatingRowStyle CssClass="alternate" />
                                        <Columns>
                                            <asp:BoundField HeaderText="Perspectiva" ItemStyle-Width="250px" DataField="Perspectiva.DescricaoPerspectiva" />
                                            <asp:BoundField HeaderText="Tema" DataField="DescricaoTema" />
                                            <asp:TemplateField HeaderText="Situação" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                       <%# (bool)Eval("Ativo") == true ? "Ativo" : "Inativo" %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Ações" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkDetalhar" runat="server" CommandArgument='<%# Eval("CodigoSeqTema") %>'
                                                        CommandName="Detalhar" CausesValidation="false" CssClass="glyphicon glyphicon-pencil" />
                                                    <asp:LinkButton ID="lnkExcluir" runat="server" CommandArgument='<%# Eval("CodigoSeqTema") %>'
                                                        CommandName="Excluir" CausesValidation="false" CssClass="glyphicon glyphicon-remove" />
                                                    <asp:LinkButton ID="lnkVisualizar" runat="server" Target="_self" CommandArgument='<%# Eval("CodigoSeqTema") %>'
                                                        CommandName="Visuzalizar" CausesValidation="false" CssClass="glyphicon glyphicon-search" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <div class="paginator">
                                    <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
