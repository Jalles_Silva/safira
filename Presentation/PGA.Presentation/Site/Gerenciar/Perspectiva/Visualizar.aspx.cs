﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using PGA.Common;

namespace PGA.Presentation.Site.Gerenciar.Perspectiva
{
    public partial class Visualizar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqPerspectiva
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqPerspectiva").ToInt32();
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Gerenciar/Perspectiva/Consultar.aspx?Situacao={0}&PageIndex={1}", Situacao, PageIndex));
        }

        #endregion

        #region [Methods]

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqPerspectiva > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterPerspectiva(CodigoSeqPerspectiva);

                    lblNumPerspectiva.Text = registro.NuPerspectiva;
                    lblDescricaoPerspectiva.Text = registro.DescricaoPerspectiva;
                    lblSituacao.Text = registro.Ativo ? "Ativo" : "Inativo";
                }
                else
                    WebHelper.Redirect("~/Site/Gerenciar/Perspectiva/Consultar.aspx");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}