﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using PGA.Common;

namespace PGA.Presentation.Site.Gerenciar.Perspectiva
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqPerspectiva
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqPerspectiva").ToInt32();
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Voltar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        WebHelper.Redirect(String.Format("~/Site/Gerenciar/Perspectiva/Consultar.aspx?Situacao={0}&PageIndex={1}", Situacao, PageIndex));
                    }
                    break;
                case "Detalhar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        var CodSeqPerspectiva = e.Parameters["CodSeqPerspectiva"];
                        WebHelper.Redirect(String.Format("~/Site/Gerenciar/Perspectiva/Detalhar.aspx?CodigoSeqPerspectiva={0}&Situacao={1}&PageIndex={2}", CodSeqPerspectiva, Situacao, PageIndex));
                    }
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            Validate();

            if (IsValid)
            {
                if (String.IsNullOrEmpty(txtDescricaoPerspectiva.Text))
                {
                    MessageBox.ShowInformationMessage("Favor informar a Perspectiva.", "Erro");
                    return;
                }

                Salvar();
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Voltar",
                "Deseja retornar para a página de consulta (dados não salvos serão perdidos)?", MessageBoxButtons.YesNo, "Voltar");
        }

        #endregion

        #region [Methods]

        private void Salvar()
        {
            try
            {
                DTOPerspectiva registro = new DTOPerspectiva();
                registro.CodigoSeqPerspectiva = CodigoSeqPerspectiva;
                registro.NuPerspectiva = txtNumPerspectiva.Text;
                registro.DescricaoPerspectiva = txtDescricaoPerspectiva.Text;
                registro.Ativo = Boolean.Parse(rblSituacao.SelectedValue);

                var CodSeqPerspectiva = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarPerspectiva(registro);

                var param = new Dictionary<string, object>();
                param.Add("CodSeqPerspectiva", CodSeqPerspectiva);

                MessageBox.ShowInformationMessage("Registro salvo com sucesso.", "Detalhar", param);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqPerspectiva > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterPerspectiva(CodigoSeqPerspectiva);

                    txtNumPerspectiva.Text = registro.NuPerspectiva;
                    txtDescricaoPerspectiva.Text = registro.DescricaoPerspectiva;
                    rblSituacao.SelectedValue = registro.Ativo.ToString();
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}