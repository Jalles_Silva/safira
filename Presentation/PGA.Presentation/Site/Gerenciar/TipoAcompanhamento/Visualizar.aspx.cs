﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using PGA.Common;

namespace PGA.Presentation.Site.Gerenciar.TipoAcompanhamento
{
    public partial class Visualizar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqTipoAcompanhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqTipoAcompanhamento").ToInt32();
            }
        }

        private string TipoAcompanhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoAcompanhamento");
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Gerenciar/TipoAcompanhamento/Consultar.aspx?TipoAcompanhamento={0}&Situacao={1}&PageIndex={2}", TipoAcompanhamento, Situacao, PageIndex));
        }

        #endregion

        #region [Methods]

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqTipoAcompanhamento > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterTipoAcompanhamento(CodigoSeqTipoAcompanhamento);

                    lblTipoAcompanhamento.Text = registro.DescricaoTipoAcompanhamento;
                    lblSituacao.Text = registro.Ativo ? "Ativo" : "Inativo";
                }
                else
                    WebHelper.Redirect("~/Site/Gerenciar/TipoAcompanhamento/Consultar.aspx");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}