﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;

namespace PGA.Presentation.Site.Gerenciar.TipoAcompanhamento
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqTipoAcompanhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqTipoAcompanhamento").ToInt32();
            }
        }

        private string TipoAcompanhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoAcompanhamento");
            }
        }

        private string Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Voltar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        WebHelper.Redirect(String.Format("~/Site/Gerenciar/TipoAcompanhamento/Consultar.aspx?TipoAcompanhamento={0}&Situacao={1}&PageIndex={2}", TipoAcompanhamento, Situacao, PageIndex));
                    }
                    break;
                case "Detalhar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        var CodSeqTipoAcompanhamento = e.Parameters["CodSeqTipoAcompanhamento"];
                        WebHelper.Redirect(String.Format("~/Site/Gerenciar/TipoAcompanhamento/Detalhar.aspx?CodigoSeqTipoAcompanhamento={0}&TipoAcompanhamento={1}&Situacao={2}&PageIndex={3}", CodSeqTipoAcompanhamento, TipoAcompanhamento, Situacao, PageIndex));
                    }
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            Validate();

            if (IsValid)
            {
                if (String.IsNullOrEmpty(txtTipoAcompanhamento.Text))
                {
                    MessageBox.ShowInformationMessage("Favor informar o Tipo de Acompanhamento do registro.", "Erro");
                    return;
                }

                Salvar();
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Voltar",
                "Deseja retornar para a página de consulta (dados não salvos serão perdidos)?", MessageBoxButtons.YesNo, "Voltar");
        }

        #endregion

        #region [Methods]

        private void Salvar()
        {
            try
            {
                DTOTipoAcompanhamento registro = new DTOTipoAcompanhamento();
                registro.CodigoSeqTipoAcompanhamento = CodigoSeqTipoAcompanhamento;
                registro.DescricaoTipoAcompanhamento = txtTipoAcompanhamento.Text;
                registro.Ativo = Boolean.Parse(rblSituacao.SelectedValue);

                var CodSeqTipoAcompanhamento = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarTipoAcompanhamento(registro);

                var param = new Dictionary<string, object>();
                param.Add("CodSeqTipoAcompanhamento", CodSeqTipoAcompanhamento);

                MessageBox.ShowInformationMessage("Registro salvo com sucesso.", "Detalhar", param);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqTipoAcompanhamento > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterTipoAcompanhamento(CodigoSeqTipoAcompanhamento);

                    txtTipoAcompanhamento.Text = registro.DescricaoTipoAcompanhamento;
                    rblSituacao.SelectedValue = registro.Ativo.ToString();
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}