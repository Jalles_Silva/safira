﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Detalhar.aspx.cs" Inherits="PGA.Presentation.Site.Gerenciar.TipoAcompanhamento.Detalhar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnSalvar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cadastro Tipo Acompanhamento</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-7 col-sm-7">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoAcompanhamento" AssociatedControlID="txtTipoAcompanhamento" Text="Tipo Acompanhamento" runat="server" />
                                    <asp:TextBox ID="txtTipoAcompanhamento" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblSituacao" AssociatedControlID="rblSituacao" Text="Situação" runat="server" />
                                    <asp:RadioButtonList ID="rblSituacao" runat="server" RepeatDirection="Horizontal" CssClass="radioListHoriz">
                                        <asp:ListItem Value="True" Text="Ativo" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="False" Text="Inativo"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>

                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="updBotoes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                        <asp:LinkButton ID="btnSalvar" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="Salvar" />                                        
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            FuncaoTela();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function ()
        {
            FuncaoTela();
        });

        function FuncaoTela() {

            $("#<%= txtTipoAcompanhamento.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 150) {
                    $("#<%= txtTipoAcompanhamento.ClientID %>").val(text.substring(0, 150));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtTipoAcompanhamento.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 150) {
                    $("#<%= txtTipoAcompanhamento.ClientID %>").val(text.substring(0, 150));
                    return false;
                }
                else {
                    return true;
                }
            });
            
        }
    </script>
</asp:Content>
