﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Visualizar.aspx.cs" Inherits="PGA.Presentation.Site.Gerenciar.ResultadosAlcancados.Visualizar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Visualizar Resultados Alcançados (Informações para Relatório Anual Consubstanciado)</h3>
                    </div>
                    <div class="panel-body">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-1 col-sm-1">
                                    <div class="form-group">
                                        <asp:Label ID="lblAnoExercicio" AssociatedControlID="txtAnoExercicio" Text="Ano" runat="server" />
                                        <asp:TextBox ID="txtAnoExercicio" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-1 col-sm-1">
                                    <div class="form-group">
                                        <asp:Label ID="lblArea" AssociatedControlID="txtArea" Text="Área" runat="server" />
                                        <asp:TextBox ID="txtArea" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-xs-10 col-sm-10">
                                    <div class="form-group">
                                        <asp:Label ID="lblObjetivoIniciativa" AssociatedControlID="txtObjetivoIniciativa" Text="Objetivos estratégicos ou Missão" runat="server" />
                                        <asp:TextBox ID="txtObjetivoIniciativa" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="upPnlControleAcao" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12">
                                            <div class="area-table">
                                                <asp:GridView ID="gdvResultadoConsulta" runat="server"
                                                    Width="100%" AutoGenerateColumns="False"
                                                    EmptyDataText="Nenhum registro encontrado.">
                                                    <EmptyDataTemplate>Nenhum registro encontrado.</EmptyDataTemplate>
                                                    <AlternatingRowStyle CssClass="alternate" />
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Meta" DataField="DescricaoMeta" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="30px" />
                                                        <asp:BoundField HeaderText="Tipo da Meta" DataField="DescricaoTipo" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15px" />
                                                        <asp:BoundField HeaderText="Resultado Esperado da Meta" DataField="ResultadoEsperadoMeta" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="30px" />
                                                        <asp:BoundField HeaderText="Constante no PGA" DataField="ContantePGA" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5px" />
                                                        <asp:BoundField HeaderText="Executado" DataField="Executado" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="1px" />
                                                        <asp:BoundField HeaderText="Desempenho Indicador" DataField="DesempenhoIndicador" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="1px" />
                                                        <asp:BoundField HeaderText="Alinhamento" DataField="DescricaoAlinhamento" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="3px" />
                                                        <asp:TemplateField HeaderText="Benefícios/Impactos" ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="1px">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkVisualizar" runat="server" CommandArgument='<%# Eval("CodigoMeta") %>'
                                                                    CommandName="Visualizar" CausesValidation="false" CssClass="glyphicon glyphicon-eye-open" ToolTip="Benefícios Impactos " />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <div class="paginator">
                                                <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12">
                                    <div class="form-group">
                                        <asp:Label ID="lblDescResultadosAlcancados" AssociatedControlID="txtDescResultadosAlcancados" Text="Resultados Alcançados" runat="server" />
                                        <asp:TextBox ID="txtDescResultadosAlcancados" Rows="4" runat="server" CssClass="form-control" TextMode="MultiLine" ReadOnly="true" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="updBotoes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div id="modalBeneficiosImpactados" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="panel-title">
                        <asp:Label ID="lblTitulo" runat="server" Text="" />
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h5 class="panel-title">Sociedade/Usuário</h5>
                        </div>
                        <div class="panel-body">
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblSociedadeUsuarioBeneficio" AssociatedControlID="txtSociedadeUsuarioBeneficio" Text="Benefício" runat="server" />
                                    <asp:TextBox ID="txtSociedadeUsuarioBeneficio" runat="server" CssClass="form-control" TextMode="MultiLine" Enabled="false" />
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblSociedadeUsuarioImpacto" AssociatedControlID="txtSociedadeUsuarioImpacto" Text="Impacto" runat="server" />
                                    <asp:TextBox ID="txtSociedadeUsuarioImpacto" runat="server" CssClass="form-control" TextMode="MultiLine" Enabled="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Institucional</h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblInstitucionalBeneficio" AssociatedControlID="txtInstitucionalBeneficio" Text="Benefício" runat="server" />
                                    <asp:TextBox ID="txtInstitucionalBeneficio" runat="server" CssClass="form-control" TextMode="MultiLine" Enabled="false" />
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblInstitucionalImpacto" AssociatedControlID="txtInstitucionalImpacto" Text="Impacto" runat="server" />
                                    <asp:TextBox ID="txtInstitucionalImpacto" runat="server" CssClass="form-control" TextMode="MultiLine" Enabled="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Governo</h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblGovernoBeneficio" AssociatedControlID="txtGovernoBeneficio" Text="Benefício" runat="server" />
                                    <asp:TextBox ID="txtGovernoBeneficio" runat="server" CssClass="form-control" TextMode="MultiLine" Enabled="false" />
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblGovernoImpacto" AssociatedControlID="txtGovernoImpacto" Text="Impacto" runat="server" />
                                    <asp:TextBox ID="txtGovernoImpacto" runat="server" CssClass="form-control" TextMode="MultiLine" Enabled="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Setor Regulado</h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblSetorReguladoBeneficio" AssociatedControlID="txtSetorReguladoBeneficio" Text="Benefício" runat="server" />
                                    <asp:TextBox ID="txtSetorReguladoBeneficio" runat="server" CssClass="form-control" TextMode="MultiLine" Enabled="false" />
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblSetorReguladoImpacto" AssociatedControlID="txtSetorReguladoImpacto" Text="Impacto" runat="server" />
                                    <asp:TextBox ID="txtSetorReguladoImpacto" runat="server" CssClass="form-control" TextMode="MultiLine" Enabled="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function CarregarPopUp(UsuarioBeneficio, UsuarioImpacto,
                InstitucionalBeneficio, InstitucionalImpacto,
                GovernoBeneficio, GovernoImpacto,
                SetorReguladoBeneficio, SetorReguladoImpacto, DescricaoMeta) {

            debugger;

            Corpo_txtSociedadeUsuarioBeneficio.value = UsuarioBeneficio;
            Corpo_txtSociedadeUsuarioImpacto.value = UsuarioImpacto;
            Corpo_txtInstitucionalBeneficio.value = InstitucionalBeneficio;
            Corpo_txtInstitucionalImpacto.value = InstitucionalImpacto;
            Corpo_txtGovernoBeneficio.value = GovernoBeneficio;
            Corpo_txtGovernoImpacto.value = GovernoImpacto;
            Corpo_txtSetorReguladoBeneficio.value = SetorReguladoBeneficio;
            Corpo_txtSetorReguladoImpacto.value = SetorReguladoImpacto;
            document.getElementById("Corpo_lblTitulo").innerHTML = "Benefício/Impacto - Meta: " + DescricaoMeta;
        }
    </script>
</asp:Content>

