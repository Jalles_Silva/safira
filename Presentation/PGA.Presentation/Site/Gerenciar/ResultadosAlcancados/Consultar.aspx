﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Consultar.aspx.cs" Inherits="PGA.Presentation.Site.Gerenciar.ResultadosAlcancados.Consultar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel-heading">
                    <h3 class="panel-title">Consulta de Resultados Alcançados (Informações para Relatório Anual Consubstanciado)</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4">
                            <div class="form-group">
                                <asp:Label ID="lblAnoExercicio" AssociatedControlID="lblAnoExercicio" Text="Exercício" runat="server" />
                                <asp:DropDownList ID="ddlAnoExercicio" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <div class="form-group">
                                <asp:Label ID="lblArea" AssociatedControlID="ddlArea" Text="Área" runat="server" />
                                <asp:DropDownList ID="ddlArea" runat="server" CssClass="form-control"></asp:DropDownList>
                                <asp:HiddenField ID="hdfAreaUsuarioLogado" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-xs-12 col-sm-12">
                                <asp:LinkButton ID="btnPesquisar" runat="server" SkinID="btnPesquisar" CausesValidation="false" />
                                <asp:HyperLink ID="btnLimpar" runat="server" SkinID="btnLimpar" CausesValidation="false" NavigateUrl="~/Site/Gerenciar/ResultadosAlcancados/Consultar.aspx" />
                            </div>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="upPnlResultadoConsulta" runat="server">
                        <ContentTemplate>
                            <div class="area-table">
                                <asp:Repeater ID="rptResultadoAlcancado" runat="server">
                                    <HeaderTemplate>
                                        <table>
                                            <tr runat="server">
                                                <th >Tipo</th>
                                                <th>Descrição</th>
                                                <th>Ano</th>
                                                <th align="center">Resultados Alcançados Registrado </th>
                                                <th>Ações</th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="background-color: #C5D8CC !important; width: 50px;">
                                                <asp:Label ID="lblNomeUnidade" runat="server" Text='<%#Eval("Key") %>' />
                                            </td>
                                            <td colspan="4" style="background-color: #C5D8CC !important"></td>
                                        </tr>
                                        <asp:Repeater ID="rptResultadoAlcancadoPorUnidade" OnItemDataBound="rptResultadoAlcancado_ItemDataBound" OnItemCommand="rptResultadoAlcancadoPorUnidade_ItemCommand" runat="server" DataSource='<%#Eval("Value") %>'>
                                            <ItemTemplate>
                                                <tr class="<%# (Container.ItemIndex % 2 == 0 ? "" : "alternate") %>">
                                                    <td style="width: 15%">
                                                        <asp:Label ID="lblDescricaoTipo" runat="server" Text='<%#Eval("DescricaoTipo") %>' />
                                                    </td>
                                                    <td style="width: 55%">
                                                        <asp:Label ID="lblDescricaoObjetivoIniciativa" runat="server" Text='<%#Eval("DescricaoObjetivoIniciativa") %>' />
                                                    </td>
                                                    <td style="width: 3%"> 
                                                        <asp:Label ID="lblAnoExcercicio" runat="server" Text='<%#Eval("AnoExcercicio") %>' />
                                                    </td>
                                                    <td align="center" style="width: 20%">
                                                        <asp:Label ID="lblResultadoAlcancadoRegistrado" runat="server" Text='<%#Eval("ResultadoAlcancadoRegistrado") %>' />
                                                    </td>
                                                    <td style="width: 5%">
                                                        <asp:LinkButton ID="lnkDetalhar" runat="server" Target="_self" CommandArgument='<%#Eval("CodSeqResultadoAlcancado") +"|"+ Eval("CodSeqObjetivoIniciativa") +"|"+ Eval("CodTipo") +"|"+ Eval("CodigoUnidade") +"|"+ Eval("CodMeta") %>'
                                                            CommandName="Detalhar" CausesValidation="false" CssClass="glyphicon glyphicon-pencil" 
                                                            Style='<%# ((PGA.Common.FaseEnum)Convert.ToInt32(Eval("CodFase")) == PGA.Common.FaseEnum.Encerrado) ? "visibility:hidden;": "visibility:visible;" %>'/>

                                                        <asp:LinkButton ID="lnkVisualizar" runat="server" Target="_self" CommandArgument='<%#Eval("CodSeqResultadoAlcancado") +"|"+ Eval("CodSeqObjetivoIniciativa") +"|"+ Eval("CodTipo") +"|"+ Eval("CodigoUnidade") +"|"+ Eval("CodMeta") %>'
                                                            CommandName="Visuzalizar" CausesValidation="false" CssClass="glyphicon glyphicon-search" />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <tr>
                                            <td colspan="5">
                                                <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." /></td>
                                        </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="paginator">
                                <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
