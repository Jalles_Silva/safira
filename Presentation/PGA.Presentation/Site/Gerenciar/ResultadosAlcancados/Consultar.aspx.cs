﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SQFramework.Data.Pagging;


namespace PGA.Presentation.Site.Gerenciar.ResultadosAlcancados
{
    public partial class Consultar : CustomPageBase
    {
        #region [Properties]

        public bool UsuarioAdministrador
        {
            get
            {
                return VerificarPermissao("PGA_Administrador", true);
            }
        }

        public bool UsuarioCadastrador
        {
            get
            {
                return VerificarPermissao("PGA_Cadastrador", true);
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        private int AnoExercicio
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("AnoExercicio").ToInt32();
            }
        }

        private int Area
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Area").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            ddlAnoExercicio.SelectedIndexChanged += ddlAnoExercicio_SelectedIndexChanged;
            btnPesquisar.Click += btnPesquisar_Click;
        }

        protected void rptResultadoAlcancadoPorUnidade_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                int codSeqResultadoAlcancado = 0;
                int codSeqObjetivoIniciativa = 0;
                int codTipo = 0;
                int codUnidade = 0;
                int codMeta = 0;
                string[] parametro = null;

                switch (e.CommandName)
                {
                    case "Detalhar":
                        parametro = e.CommandArgument.ToString().Split('|');
                        codSeqResultadoAlcancado = int.Parse(parametro[0]);
                        codSeqObjetivoIniciativa = int.Parse(parametro[1]);
                        codTipo = int.Parse(parametro[2]);
                        codUnidade = int.Parse(parametro[3]);
                        codMeta = int.Parse(parametro[4]);

                        WebHelper.Redirect(string.Format("~/Site/Gerenciar/ResultadosAlcancados/Detalhar.aspx?CodSeqObjetivoIniciativa={0}&CodTipo={1}&CodUnidade={2}&PageIndex={3}&AnoExercicio={4}&Area={5}&CodSeqResultadoAlcancado={6}&CodMeta={7}", codSeqObjetivoIniciativa, codTipo, codUnidade, ucPaginatorConsulta.PageIndex, ddlAnoExercicio.Text, ddlArea.Text, codSeqResultadoAlcancado, codMeta));
                        break;
                    case "Visuzalizar":
                        parametro = e.CommandArgument.ToString().Split('|');
                        codSeqResultadoAlcancado = int.Parse(parametro[0]);
                        codSeqObjetivoIniciativa = int.Parse(parametro[1]);
                        codTipo = int.Parse(parametro[2]);
                        codUnidade = int.Parse(parametro[3]);
                        codMeta = int.Parse(parametro[4]);

                        WebHelper.Redirect(string.Format("~/Site/Gerenciar/ResultadosAlcancados/Visualizar.aspx?CodSeqObjetivoIniciativa={0}&CodTipo={1}&CodUnidade={2}&PageIndex={3}&AnoExercicio={4}&Area={5}&CodSeqResultadoAlcancado={6}&CodMeta={7}", codSeqObjetivoIniciativa, codTipo, codUnidade, ucPaginatorConsulta.PageIndex, ddlAnoExercicio.Text, ddlArea.Text, codSeqResultadoAlcancado, codMeta));
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }


        protected void rptResultadoAlcancado_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkDetalhar = (LinkButton)ProcuraControle(e.Item, "lnkDetalhar");
            LinkButton lnkVisualizar = (LinkButton)ProcuraControle(e.Item, "lnkVisualizar");

            lnkDetalhar.Visible = UsuarioAdministrador;
            lnkVisualizar.Visible = UsuarioAdministrador || UsuarioCadastrador;
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            ConsultarDados(e.NewPage - 1);
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            ConsultarDados(0);
        }

        private void ConsultarDados(int pageIndex)
        {
            try
            {
                Validate();

                if (IsValid)
                {
                    try
                    {
                        PageMessage<DTOResultadosAlcancados> dadosPesquisados = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService")
                            .ListarResultadosAlcancados(ddlArea.SelectedValue.ToInt32(), ddlAnoExercicio.SelectedValue.ToInt32(), pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize);

                        List<DTOResultadosAlcancados> source = dadosPesquisados.Entities.ToList();

                        List<string> unidades = source.Select(i => i.DescricaoUnidade).Distinct().ToList();
                        Dictionary<string, IList<DTOResultadosAlcancados>> lista = new Dictionary<string, IList<DTOResultadosAlcancados>>();

                        foreach (var item in unidades)
                        {
                            lista.Add(item, source.Where(i => i.DescricaoUnidade == item).OrderBy(x => x.DescricaoObjetivoIniciativa).ToList());
                        }

                        rptResultadoAlcancado.DataSource = lista;
                        rptResultadoAlcancado.DataBind();

                        ucPaginatorConsulta.Visible = (dadosPesquisados.RowsCount > 0);
                        ucPaginatorConsulta.TotalRecords = dadosPesquisados.RowsCount;
                        ucPaginatorConsulta.PageIndex = pageIndex + 1;
                        ucPaginatorConsulta.DataBind();
                    }
                    catch (Exception ex)
                    {
                        HandleException(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void ddlAnoExercicio_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarArea();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CarregarAnoExercicio();
                CarregarArea();
                CarregarStatusAnterior();
            }
        }

        #endregion

        #region [Methods]
        private void CarregarStatusAnterior()
        {
            try
            {
                if (AnoExercicio > 0)
                    ddlAnoExercicio.SelectedValue = AnoExercicio.ToString();

                if (Area > 0)
                    ddlArea.SelectedValue = Area.ToString();

                if (PageIndex > 0)
                    ConsultarDados(PageIndex - 1);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarAnoExercicio()
        {
            try
            {
                ddlAnoExercicio.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarExercicios().OrderByDescending(i => i.Ano);
                ddlAnoExercicio.DataTextField = "Ano";
                ddlAnoExercicio.DataValueField = "Ano";
                ddlAnoExercicio.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarArea()
        {
            try
            {
                ddlArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarCorporativoUnidadeAtivoPorExercicio(ddlAnoExercicio.SelectedValue.ToInt32());
                ddlArea.DataTextField = "DescricaoUnidade";
                ddlArea.DataValueField = "CodigoUnidade";
                ddlArea.DataBind();
                ddlArea.Items.Insert(0, new ListItem("Todas", "0"));
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        public static System.Web.UI.Control ProcuraControle(System.Web.UI.Control control, string id)
        {
            if (control == null)
                return null;

            System.Web.UI.Control ctrl = control.FindControl(id);

            if (ctrl == null)
            {
                foreach (System.Web.UI.Control child in control.Controls)
                {
                    ctrl = ProcuraControle(child, id);

                    if (ctrl != null)
                        break;
                }
            }
            return ctrl;
        }

        #endregion
    }
}