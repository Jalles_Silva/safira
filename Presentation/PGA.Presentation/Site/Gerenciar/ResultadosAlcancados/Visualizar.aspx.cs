﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.Services;
using PGA.Common;
using PGA.Services.Spec.DataTransferObjects;
using System.Collections.Generic;

namespace PGA.Presentation.Site.Gerenciar.ResultadosAlcancados
{
    public partial class Visualizar : CustomPageBase
    {
        #region [Properties]

        private int CodSeqResultadoAlcancado
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodSeqResultadoAlcancado").ToInt32();
            }
        }

        private int CodSeqObjetivoIniciativa
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodSeqObjetivoIniciativa").ToInt32();
            }
        }

        private int CodTipo
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodTipo").ToInt32();
            }
        }

        private int CodUnidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodUnidade").ToInt32();
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        private int AnoExercicio
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("AnoExercicio").ToInt32();
            }
        }

        private int Area
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Area").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
            gdvResultadoConsulta.RowCommand += new GridViewCommandEventHandler(gdvResultadoConsulta_RowCommand);
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(string.Format("~/Site/Gerenciar/ResultadosAlcancados/Consultar.aspx?PageIndex={0}&AnoExercicio={1}&Area={2}", PageIndex, AnoExercicio, Area));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void gdvResultadoConsulta_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Visualizar"))
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "$('#modalBeneficiosImpactados').modal('show');", true);
                    CarregarRegistroPopUp(e.CommandArgument);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion

        #region [Methods]

        private void CarregarRegistro()
        {
            try
            {
                if (CodSeqObjetivoIniciativa > 0 && CodTipo > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterResultadoAlcancadoCompleto(CodSeqResultadoAlcancado, CodSeqObjetivoIniciativa, CodTipo, AnoExercicio);

                    var objetivoIniciativa = "";
                    if (registro.Objetivo != null)
                        objetivoIniciativa = registro.Objetivo.NomeObjetivo;

                    if (registro.Iniciativa != null)
                        objetivoIniciativa = registro.Iniciativa.DescricaoIniciativa;

                    txtObjetivoIniciativa.Text = objetivoIniciativa;
                    txtAnoExercicio.Text = registro.Exercicio.Ano.ToString();
                    txtDescResultadosAlcancados.Text = registro.DescricaoResultadoAlcancado;
                    txtArea.Text = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterCorporativoUnidade(CodUnidade).DescricaoUnidade;

                    ConsultarDados(registro.Exercicio.Ano, 0);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void ConsultarDados(short ano, int pageIndex)
        {
            try
            {
                var dadosPesquisados = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListaMetasPorObjetivoIniciativa(CodSeqObjetivoIniciativa, ano, CodTipo, CodUnidade, pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize);

                gdvResultadoConsulta.Visible = (dadosPesquisados.RowsCount > 0);
                gdvResultadoConsulta.DataSource = dadosPesquisados.Entities.ToList();
                gdvResultadoConsulta.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistroPopUp(object codigoMeta)
        {
            try
            {
                if (int.Parse(codigoMeta.ToString()) > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterMeta(int.Parse(codigoMeta.ToString()));

                    System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp",
                            "<script>CarregarPopUp('"
                                + registro.DescricaoSociedadeUsuarioBeneficio + "', '"
                                + registro.DescricaoSociedadeUsuarioImpacto + "', '"
                                + registro.DescricaoInstitucionalBeneficio + "', '"
                                + registro.DescricaoInstitucionalImpacto + "', '"
                                + registro.DescricaoGovernoBeneficio + "', '"
                                + registro.DescricaoGovernoImpacto + "', '"
                                + registro.DescricaoSetorReguladoBeneficio + "', '"
                                + registro.DescricaoSetorReguladoImpacto + "', '"
                                + registro.DescricaoMeta
                            + "');</script>", false);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}