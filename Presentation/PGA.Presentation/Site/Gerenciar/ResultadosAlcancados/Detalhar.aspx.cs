﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.Services;
using PGA.Common;
using PGA.Services.Spec.DataTransferObjects;
using System.Collections.Generic;
using System.Web.UI;
using System.Text;
using System.Drawing;

namespace PGA.Presentation.Site.Gerenciar.ResultadosAlcancados
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodSeqResultadoAlcancado
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodSeqResultadoAlcancado").ToInt32();
            }
        }

        private int CodSeqObjetivoIniciativa
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodSeqObjetivoIniciativa").ToInt32();
            }
        }

        private int CodTipo
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodTipo").ToInt32();
            }
        }

        private int CodUnidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodUnidade").ToInt32();
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        private int AnoExercicio
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("AnoExercicio").ToInt32();
            }
        }

        private int Area
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Area").ToInt32();
            }
        }

        private int CodMeta
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodMeta").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            gdvResultadoConsulta.RowCommand += new GridViewCommandEventHandler(gdvResultadoConsulta_RowCommand);
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            PreSalvar();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Voltar",
                "Deseja retornar para a página de consulta?", MessageBoxButtons.YesNo, "Voltar");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    hdfCodigoSeqResultadoAlcancado.Value = "0";
                    hdfCodigoSeqObjetivo.Value = "0";
                    hdfCodigoSeqIniciativa.Value = "0";
                    hdfCodigoSeqExercicio.Value = "0";

                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Salvar":
                    if (e.Result == MessageBoxResult.Yes)
                        Salvar();
                    break;
                case "Detalhar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        var CodSeqResultadoAlcancado = e.Parameters["CodSeqResultadoAlcancado"];
                        WebHelper.Redirect(string.Format("~/Site/Gerenciar/ResultadosAlcancados/Detalhar.aspx?CodSeqObjetivoIniciativa={0}&CodTipo={1}&CodUnidade={2}&PageIndex={3}&AnoExercicio={4}&Area={5}&CodSeqResultadoAlcancado={6}", CodSeqObjetivoIniciativa, CodTipo, CodUnidade, PageIndex, AnoExercicio, Area, CodSeqResultadoAlcancado));
                    }
                    break;
                case "Voltar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                        WebHelper.Redirect(string.Format("~/Site/Gerenciar/ResultadosAlcancados/Consultar.aspx?PageIndex={0}&AnoExercicio={1}&Area={2}", PageIndex, AnoExercicio, Area));
                    break;
            }
        }

        protected void gdvResultadoConsulta_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName.Equals("Visualizar"))
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "$('#modalBeneficiosImpactados').modal('show');", true);
                    CarregarRegistroPopUp(e.CommandArgument);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion

        #region [Methods]

        private void CarregarRegistro()
        {
            try
            {
                if (CodSeqObjetivoIniciativa > 0 && CodTipo > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterResultadoAlcancadoCompleto(CodSeqResultadoAlcancado, CodSeqObjetivoIniciativa, CodTipo, AnoExercicio);

                    hdfCodigoSeqResultadoAlcancado.Value = registro.CodigoSeqResultadoAlcancado.ToString();
                    hdfCodigoSeqExercicio.Value = registro.Exercicio.CodigoSeqExercicio.ToString();

                    var objetivoIniciativa = "";
                    if (registro.Objetivo != null)
                    {
                        hdfCodigoSeqObjetivo.Value = registro.Objetivo.CodigoSeqObjetivo.ToString();
                        objetivoIniciativa = registro.Objetivo.NomeObjetivo;
                    }

                    if (registro.Iniciativa != null)
                    {
                        hdfCodigoSeqIniciativa.Value = registro.Iniciativa.CodigoSeqIniciativa.ToString();
                        objetivoIniciativa = registro.Iniciativa.DescricaoIniciativa;
                    }

                    txtObjetivoIniciativa.Text = objetivoIniciativa;
                    txtAnoExercicio.Text = registro.Exercicio.Ano.ToString();
                    txtDescResultadosAlcancados.Text = registro.DescricaoResultadoAlcancado;
                    txtArea.Text = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterCorporativoUnidade(CodUnidade).DescricaoUnidade;

                    ConsultarDados(0, registro.Exercicio.Ano);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void ConsultarDados(int pageIndex, short ano)
        {
            try
            {
                var dadosPesquisados = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListaMetasPorObjetivoIniciativa(CodSeqObjetivoIniciativa, ano, CodTipo, CodUnidade, pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize);

                gdvResultadoConsulta.Visible = (dadosPesquisados.RowsCount > 0);
                gdvResultadoConsulta.DataSource = dadosPesquisados.Entities.ToList();
                gdvResultadoConsulta.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void PreSalvar()
        {
            try
            {
                Validate();

                if (IsValid)
                {
                    if (string.IsNullOrEmpty(txtDescResultadosAlcancados.Text))
                    {
                        MessageBox.ShowInformationMessage("Favor informar Resultados Alcançados.", "Erro");
                        return;
                    }

                    MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Salvar",
                        "Deseja realmente salvar?", MessageBoxButtons.YesNo, "Salvar");
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void Salvar()
        {
            try
            {
                DTOResultadoAlcancadoRegistro registro = new DTOResultadoAlcancadoRegistro()
                {
                    Objetivo = new DTOObjetivo(),
                    Iniciativa = new DTOIniciativa(),
                    Exercicio = new DTOExercicio(),
                    CorporativoUnidade = new DTOCorporativoUnidade()
                };

                registro.CodigoSeqResultadoAlcancado = int.Parse(hdfCodigoSeqResultadoAlcancado.Value);
                registro.Objetivo.CodigoSeqObjetivo = int.Parse(hdfCodigoSeqObjetivo.Value);
                registro.Iniciativa.CodigoSeqIniciativa = int.Parse(hdfCodigoSeqIniciativa.Value);
                registro.Exercicio.CodigoSeqExercicio = int.Parse(hdfCodigoSeqExercicio.Value);
                registro.Exercicio.CodigoSeqExercicio = int.Parse(hdfCodigoSeqExercicio.Value);
                registro.CorporativoUnidade.CodigoUnidade = CodUnidade;
                registro.DescricaoResultadoAlcancado = txtDescResultadosAlcancados.Text;
                registro.Registrado = true;
                registro.Ativo = true;

                int codSeqResultadoAlcancado = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarResultadosAlcancados(registro);

                var param = new Dictionary<string, object>();
                param.Add("CodSeqResultadoAlcancado", codSeqResultadoAlcancado);

                MessageBox.ShowInformationMessage("Registro salvo com sucesso.", "Detalhar", param);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistroPopUp(object codigoMeta)
        {
            try
            {
                if (int.Parse(codigoMeta.ToString()) > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterMeta(int.Parse(codigoMeta.ToString()));

                    System.Web.UI.ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "temp",
                            "<script>CarregarPopUp('"
                                + registro.DescricaoSociedadeUsuarioBeneficio + "', '"
                                + registro.DescricaoSociedadeUsuarioImpacto + "', '"
                                + registro.DescricaoInstitucionalBeneficio + "', '"
                                + registro.DescricaoInstitucionalImpacto + "', '"
                                + registro.DescricaoGovernoBeneficio + "', '"
                                + registro.DescricaoGovernoImpacto + "', '"
                                + registro.DescricaoSetorReguladoBeneficio + "', '"
                                + registro.DescricaoSetorReguladoImpacto + "', '"
                                + registro.DescricaoMeta
                            + "');</script>", false);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}