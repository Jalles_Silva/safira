﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Web;
using PGA.Presentation.Util;

namespace PGA.Presentation.Site
{
    public partial class Default : CustomPageBase
    {
        protected override void OnInit(EventArgs e)
        {
            this.Load += Default_Load;

            base.OnInit(e);
        }

        void Default_Load(object sender, EventArgs e)
        {
            //WebHelper.Redirect("~/Site/Meta/Consultar.aspx");
        }
    }
}