﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using SQFramework.Core.Reflection;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using PGA.Common;
using SCA.WebControls;

namespace PGA.Presentation.Site.Acompanhar.Acao
{
    public partial class Consultar : CustomPageBase
    {
        #region [Properties]

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private int AnoExercicio
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("AnoExercicio").ToInt32();
            }
        }

        private int Meta
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Meta").ToInt32();
            }
        }

        private string Status
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Status");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            btnPesquisar.Click += new EventHandler(btnPesquisar_Click);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            ddlArea.SelectedIndexChanged += new EventHandler(ddlArea_SelectedIndexChanged);
            ddlAnoExercicio.SelectedIndexChanged += ddlAnoExercicio_SelectedIndexChanged;
        }

        void ddlAnoExercicio_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CarregarArea();
                CarregarMeta();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }  
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarControles();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Recarregar":
                    if (e.Result == MessageBoxResult.OK)
                        btnPesquisar_Click(btnPesquisar, null);
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void btnPesquisar_Click(object sender, EventArgs e)
        {
            try
            {
                ConsultarDados(0);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            try
            {
                ConsultarDados(e.NewPage - 1);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void rptAcao_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            int codigoFaseExercicio = ((HiddenField)ProcuraControle(e.Item, "hdfCodigoFaseExercicio")).Value.ToInt32();
            int codigoUnidade = ((HiddenField)ProcuraControle(e.Item, "hdfNomeUnidade")).Value.ToInt32();
            LinkButton lnkDetalhar = (LinkButton)ProcuraControle(e.Item, "lnkDetalhar");

            if (((codigoFaseExercicio == (int)FaseEnum.AcompanhamentoPrimeiroTrimestre
                    || codigoFaseExercicio == (int)FaseEnum.AcompanhamentoSegundoTrimestre
                    || codigoFaseExercicio == (int)FaseEnum.AcompanhamentoTerceiroTrimestre
                    || codigoFaseExercicio == (int)FaseEnum.AcompanhamentoQuartoTrimestre)
                && (UsuarioAdministrador || (UsuarioCadastrador && codigoUnidade == hdfAreaUsuarioLogado.Value.ToInt32())))
                || (codigoFaseExercicio == (int)FaseEnum.PreEncerramento && UsuarioAdministrador))
            {
                lnkDetalhar.CssClass = "glyphicon glyphicon-pencil";
                lnkDetalhar.Enabled = true;
            }
            else
            {
                lnkDetalhar.CssClass = "glyphicon glyphicon-ban-circle";
                lnkDetalhar.Enabled = false;
            }
            
        }

        protected void rptAcaoPorMetaEUnidade_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Detalhar")
                WebHelper.Redirect(String.Format("~/Site/Acompanhar/Acao/Detalhar.aspx?CodigoSeqAcao={0}&AnoExercicio={1}&Unidade={2}&Meta={3}&Status={4}&PageIndex={5}", e.CommandArgument, ddlAnoExercicio.SelectedValue, ddlArea.SelectedValue, ddlDescricaoMeta.SelectedValue, ddlStatus.SelectedValue, ucPaginatorConsulta.PageIndex));
        }

        #endregion

        #region [Methods]

        private void ConsultarDados(int pageIndex)
        {
            Validate();

            if (IsValid)
            {
                try
                {
                    DTOFiltroControle filtro = new DTOFiltroControle();
                    filtro.CodigoMeta = ddlDescricaoMeta.SelectedValue.ToInt32();
                    filtro.Unidade = ddlArea.SelectedItem.Text;
                    filtro.Status = ddlStatus.SelectedValue;
                    filtro.StartIndex = (pageIndex * ucPaginatorConsulta.PageSize);
                    filtro.PageSize = ucPaginatorConsulta.PageSize;
                    filtro.AnoExercicio = ddlAnoExercicio.SelectedValue.ToInt32();

                    var dadosPesquisados = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService")
                        .ListarAcoesPorFiltros(filtro);

                    List<DTOAcaoDetalhado> source = dadosPesquisados.Entities.ToList();

                    var metas = source.Select(i => new { DescricaoMeta = i.DescricaoMeta, UnidadeCorporativa = i.UnidadeCorporativa }).Distinct().ToList();

                    Dictionary<string, IList<DTOAcaoDetalhado>> lista = new Dictionary<string, IList<DTOAcaoDetalhado>>();

                    foreach (var item in metas)
                    {
                        lista.Add(item.UnidadeCorporativa + " - " + item.DescricaoMeta, source.Where(i => i.UnidadeCorporativa == item.UnidadeCorporativa
                            && i.DescricaoMeta == item.DescricaoMeta).OrderBy(x => x.DataInicioAcao).ToList());
                    }

                    rptAcao.DataSource = lista;

                    rptAcao.DataBind();

                    ucPaginatorConsulta.Visible = (dadosPesquisados.RowsCount > 0);
                    ucPaginatorConsulta.TotalRecords = dadosPesquisados.RowsCount;
                    ucPaginatorConsulta.PageIndex = pageIndex + 1;
                    ucPaginatorConsulta.DataBind();
                    upPnlResultadoConsulta.Update();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        private void CarregarControles()
        {
            ddlAnoExercicio.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarExercicioExcetoPlanejamento();
            ddlAnoExercicio.DataTextField = "Ano";
            ddlAnoExercicio.DataValueField = "Ano";
            ddlAnoExercicio.DataBind();
            ddlAnoExercicio.Items.Insert(0, new ListItem("Todos", ""));

            var ExercicioAtual = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterExercicioAtual();

            if (ExercicioAtual != null)
            {
                ddlAnoExercicio.SelectedValue = ExercicioAtual.Ano.ToString();
            }

            CarregarArea();

            CarregarMeta();

            if (UsuarioCadastrador)
            {
                if (SCAApplicationContext.Usuario.CodigoSuperintendencia == 0)
                {
                    MessageBox.ShowConfirmationMessage(MessageBoxType.Error, "Erro",
                         "Não foi possível carregar a superintêndencia, favor logar novamente.", MessageBoxButtons.OK, "Deslogar");
                    return;
                }
                else
                {
                    hdfAreaUsuarioLogado.Value = SCAApplicationContext.Usuario.CodigoSuperintendencia.ToString();
                }
            }

            if (AnoExercicio > 0)
                ddlAnoExercicio.SelectedValue = AnoExercicio.ToString();
            if (Unidade > 0)
                ddlArea.SelectedValue = Unidade.ToString();
            if (Meta > 0)
                ddlDescricaoMeta.SelectedValue = Meta.ToString();
            if (!String.IsNullOrEmpty(Status))
                ddlStatus.SelectedValue = Status;
            if (PageIndex > 0)
                ConsultarDados(PageIndex - 1);
        }

        private void CarregarArea()
        {
            ddlArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarCorporativoUnidadePorExercicio(ddlAnoExercicio.SelectedValue.ToInt32());
            ddlArea.DataTextField = "DescricaoUnidade";
            ddlArea.DataValueField = "CodigoUnidade";
            ddlArea.DataBind();
            ddlArea.Items.Insert(0, new ListItem("Todas", ""));
        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarMeta();   
        }

        public static Control ProcuraControle(Control control, string id)
        {
            if (control == null)
                return null;

            Control ctrl = control.FindControl(id);

            if (ctrl == null)
            {
                foreach (Control child in control.Controls)
                {
                    ctrl = ProcuraControle(child, id);

                    if (ctrl != null)
                        break;
                }
            }
            return ctrl;
        }

        private void CarregarMeta()
        {
            ddlDescricaoMeta.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarMetaSimplificadaPorUnidadeEAno(ddlArea.SelectedValue.ToInt32(), ddlAnoExercicio.SelectedValue.ToInt32());
            ddlDescricaoMeta.DataTextField = "DescricaoMeta";
            ddlDescricaoMeta.DataValueField = "CodSeqMeta";
            ddlDescricaoMeta.DataBind();
            ddlDescricaoMeta.Items.Insert(0, new ListItem("Todas", ""));
        }

        #endregion


    }
}