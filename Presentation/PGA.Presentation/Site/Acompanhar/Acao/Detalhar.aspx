﻿<%@ Page Title="SAFIRA - Acao" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false"
    CodeBehind="Detalhar.aspx.cs" Inherits="PGA.Presentation.Site.Acompanhar.Acao.Detalhar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnSalvar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Acompanhar Atividade</h3>
                    </div>
                    <div class="panel-body">
                         <div class="row">
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblAnoExercicio" AssociatedControlID="txtAnoExercicio" Text="Exercício" runat="server" />
                                    <asp:TextBox ID="txtAnoExercicio" runat="server" Enabled="false" CssClass="form-control" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="txtArea" Text="Área" runat="server" />
                                    <asp:TextBox ID="txtArea" runat="server" Enabled="false" CssClass="form-control" ></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoMeta" AssociatedControlID="txtDescricaoMeta" Text="Meta" runat="server" />
                                    <asp:TextBox ID="txtDescricaoMeta" runat="server" ToolTip="Descrição Meta" CssClass="form-control" Enabled="false" TextMode="MultiLine" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoAtividade" AssociatedControlID="txtDescricaoAtividade" Text="Atividade" runat="server" />
                                    <asp:TextBox ID="txtDescricaoAtividade" runat="server" ToolTip="Descrição Atividade" CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoEstrategia" AssociatedControlID="txtDescricaoEstrategia" Text="Estratégia" runat="server" />
                                    <asp:TextBox ID="txtDescricaoEstrategia" runat="server" ToolTip="Descrição Estratégia " CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblDataInicioAcao" AssociatedControlID="txtDataInicioAcao" Text="Data Início" runat="server" />
                                    <asp:TextBox ID="txtDataInicioAcao" runat="server" ToolTip="Data Início da atividade" CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblDataFimAcao" AssociatedControlID="txtDataFimAcao" Text="Data Fim" runat="server" />
                                    <asp:TextBox ID="txtDataFimAcao" runat="server" ToolTip="Data Fim da atividade" CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default" runat="server" id="Reporte">
                            <div class="panel-heading">
                                <h3 class="panel-title">Reporte de Execução de Atividade</h3>
                            </div>
                            <div class="panel-body">
                                <asp:UpdatePanel ID="upPnlControleAcao" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="col-xs-4 col-sm-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblDataReporte" AssociatedControlID="txtDataReporte" Text="Data Reporte*" runat="server" />
                                                    <asp:LinkButton ID="lnkAjudalDataReporte" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaDataReporte" ForeColor="#034623" />
                                                    <asp:TextBox ID="txtDataReporte" runat="server" CssClass="form-control" />
                                                    <asp:RequiredFieldValidator ID="rfvDataReporte" runat="server" Display="None" ControlToValidate="txtDataReporte" ErrorMessage="Por gentileza preencha os campos obrigatórios." ValidationGroup="Salvar" />
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblValorPercentualReporte" AssociatedControlID="txtPercentualReporte" Text="Percentual de Execução na Data do Reporte*" runat="server" />
                                                    <asp:LinkButton ID="lnkAjudaValorPercentualReporte" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaValorPercentualReporte" ForeColor="#034623" />
                                                    <asp:TextBox ID="txtPercentualReporte" runat="server" CssClass="form-control" MaxLength="4" AutoPostBack="true" />
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4">
                                                <div class="form-group">
                                                    <asp:Label ID="lblRecursoFinanceiroUtilizado" AssociatedControlID="txtRecursoFinanceiroUtilizado" Text="Recurso Financeiro Utilizado" runat="server" />
                                                    <asp:LinkButton ID="lnkRecursoFinanceiroUtilizado" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaRecursoFinanceiroUtilizado" ForeColor="#034623" />
                                                    <asp:TextBox ID="txtRecursoFinanceiroUtilizado" runat="server" CssClass="form-control" Style="text-align: right"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblDescricaoObservacao" AssociatedControlID="txtDescricaoObservacao" Text="Fato *" runat="server" />
                                                    <asp:LinkButton ID="lnkAjudaObservacao" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaObservacao" ForeColor="#034623" />
                                                    <asp:TextBox ID="txtDescricaoObservacao" runat="server" ToolTip="DescricaoObservacao" CssClass="form-control" MaxLength="1800" TextMode="MultiLine" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblDescricaoCausa" AssociatedControlID="txtDescricaoCausa" Text="Causa" runat="server" />
                                                    <asp:LinkButton ID="lnkAjudaCausa" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaCausa" ForeColor="#034623" />
                                                    <asp:TextBox ID="txtDescricaoCausa" runat="server" ToolTip="DescricaoCausa" CssClass="form-control" MaxLength="1800" TextMode="MultiLine" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblDescricaoAcao" AssociatedControlID="txtDescricaoAcao" Text="Ação" runat="server" />
                                                    <asp:LinkButton ID="lnkAjudaAcao" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaAcao" ForeColor="#034623" />
                                                    <asp:TextBox ID="txtDescricaoAcao" runat="server" ToolTip="DescricaoAcao" CssClass="form-control" MaxLength="1800" TextMode="MultiLine" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="area-table">
                                                    <asp:GridView ID="gdvControleAcao" runat="server"
                                                        Width="100%" AutoGenerateColumns="False"
                                                        EmptyDataText="Nenhum registro encontrado.">
                                                        <EmptyDataTemplate>Nenhum registro encontrado.</EmptyDataTemplate>
                                                        <AlternatingRowStyle CssClass="alternate" />
                                                        <Columns>
                                                            <asp:BoundField HeaderText="Data do Reporte" DataField="DataReporte" SortExpression="DataReporte" DataFormatString="{0:dd/MM/yyyy}" />
                                                            <asp:BoundField HeaderText="Percentual de Execução na Data do Reporte" DataField="ValorPercentualReporte" SortExpression="ValorPercentualReporte" ItemStyle-HorizontalAlign="Right"/>
                                                            <asp:BoundField HeaderText="Fato" DataField="DescricaoObservacao" SortExpression="DescricaoObservacao" />
                                                            <asp:BoundField HeaderText="Causa" DataField="DescricaoCausa" SortExpression="DescricaoCausa" />
                                                            <asp:BoundField HeaderText="Acao" DataField="DescricaoAcao" SortExpression="DescricaoAcao" />
                                                            <asp:TemplateField HeaderText="Ações" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkDetalhar" runat="server" CommandArgument='<%# Eval("CodigoSeqControleAcao") %>'
                                                                        CommandName="Detalhar" CausesValidation="false" CssClass="glyphicon glyphicon-pencil"
                                                                        Visible='<%# Convert.ToInt32(Eval("CodigoSeqControleAcao")) == 0 ? false : true %>' />
                                                                    <asp:LinkButton ID="lnkExcluir" runat="server" CommandArgument='<%# Eval("CodigoSeqControleAcao") %>'
                                                                        CommandName="Excluir" CausesValidation="false" CssClass="glyphicon glyphicon-remove" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="50px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div class="paginator">
                                                    <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="updBotoes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnSalvar" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="Salvar" />
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaDataReporte" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Data Reporte</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Corresponde à data de atualização da atividade.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaValorPercentualReporte" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Percentual de Execução na Data do Reporte</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Corresponde ao percentual de execução da atividade referente ao(s) trimestre(s) em avaliação.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <div id="modalAjudaRecursoFinanceiroUtilizado" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Recurso Financeiro Utilizado</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        Valor do recurso financeiro utilizado desde a última atualização.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaObservacao" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Fato</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        É a situação no momento da análise (real e atual) considerada indesejada (problema) e que impactou sobre o desempenho do indicador ou da execução do projeto. 
                                        A descrição adequada do "Fato" deve abordar as condições em que o problema foi evidenciado, evitando que o analista realize suposições ou extrapolações
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaCausa" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Causa</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        É o conjunto de motivos que desencadeou o problema. Existem uma sucessão de causas quando se analisa um determinado problema (chamada de cadeia de falhas), 
                                        sendo mais adequada a citação da causa fundamental (ou causa-raiz). A Ferramenta "5-Por que?" é bastante útil para encontrar a causa-raiz, 
                                        cabendo repetir 5 vezes a pergunta "Por quê?" cada causa do problema ocorreu
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaAcao" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Acao</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        É o esforço que deve ser desenvolvimento para atuar sobre a causa e resolver o problema. Para uma adequada descrição da ação, 
                                        sugere-se considerar a resposta a 3 questões: Para que serve a ação (Por que)? Quais ações serão realizadas (O Que)? Como essas ações serão executadas (Como)? 
                                        Quem será responsável por cada ação a ser realizada (Quem)? Quais os prazos associados a cada uma das ações (Quando)?)
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            FuncaoTela();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            FuncaoTela();
        });


        function FuncaoTela() {
            $("#<%= txtDataReporte.ClientID %>").datepicker();
            $("#<%= txtDataReporte.ClientID %>").mask("99/99/9999").val();
            $("#<%= txtPercentualReporte.ClientID %>").mask('##9%', { reverse: true, maxlength: true });

            $("#<%= txtPercentualReporte.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text != null) {
                    if (text.replace('%', '') <= 100) {
                        $("#<%= txtPercentualReporte.ClientID %>").val();
                    } else {
                        $("#<%= txtPercentualReporte.ClientID %>").val(text.substring(0, 2) + "%");
                    }
                }
            });

            $("#<%= txtDescricaoObservacao.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoObservacao.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtDescricaoObservacao.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoObservacao.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });

            $("#<%= txtDescricaoCausa.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoCausa.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtDescricaoCausa.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoCausa.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });

            $("#<%= txtDescricaoAcao.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoAcao.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtDescricaoAcao.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoAcao.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });

            $("#<%= txtRecursoFinanceiroUtilizado.ClientID %>").mask('000.000.000.000,00', { reverse: true, maxlength: true });
        }
    </script>
</asp:Content>
