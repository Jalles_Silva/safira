﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using PGA.Common;

namespace PGA.Presentation.Site.Acompanhar.Acao
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqAcaoParameter
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqAcao").ToInt32();
            }
        }

        private int CodigoSeqControleAcaoParameter
        {
            get
            {
                return ViewState["CodigoSeqControleAcaoParameter"].ToInt32();
            }
            set
            {
                ViewState["CodigoSeqControleAcaoParameter"] = value;
            }
        }

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private int AnoExercicio
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("AnoExercicio").ToInt32();
            }
        }

        private int Meta
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Meta").ToInt32();
            }
        }

        private string Status
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Status");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
            gdvControleAcao.RowCommand += new GridViewCommandEventHandler(gdvResultadoConsulta_RowCommand);
            gdvControleAcao.RowDataBound += gdvControleAcao_RowDataBound;
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Voltar",
                "Deseja retornar para a página de consulta (dados não salvos serão perdidos)?", MessageBoxButtons.YesNo, "Voltar");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CarregarControles();
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Salvar":
                    if (e.Result == MessageBoxResult.Yes)   
                        Salvar();
                    break;
                case "Recarregar":
                    Recarregar();
                    break;
                case "Voltar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                        WebHelper.Redirect(String.Format("~/Site/Acompanhar/Acao/Consultar.aspx?CodigoSeqAcao={0}&AnoExercicio={1}&Unidade={2}&Meta={3}&Status={4}&PageIndex={5}", CodigoSeqAcaoParameter, AnoExercicio, Unidade, Meta, Status, PageIndex));
                    break;
                case "Remover":
                    if (e.Result == MessageBoxResult.Yes)
                        Remover(e.Parameters["CodigoSeqControleAcao"].ToInt32());
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            CarregarControlesAcao(e.NewPage - 1, "DataReporte", false);
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            PreSalvar();
        }

        protected void gdvResultadoConsulta_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Excluir":
                    PreRemove(e.CommandArgument.ToInt32());
                    break;
                case "Detalhar":
                    CarregarRegistro(e.CommandArgument.ToInt32());
                    break;
                default:
                    break;
            }
        }

        protected void gdvControleAcao_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[5].Visible = UsuarioAlteraAcompanhamento;
        }

        #endregion

        #region [Methods]

        private void CarregarControles()
        {
            try
            {
                CarregarRegistro();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void ValidaPermissao(string CodigoUnidade)
        {
            if (UsuarioAdministrador)
            {
                Reporte.Visible = true;
                btnSalvar.Visible = UsuarioAlteraAcompanhamento;
            }
            else if (UsuarioCadastrador)
            {
                if (SCAApplicationContext.Usuario.CodigoSuperintendencia == 0)
                {
                    MessageBox.ShowConfirmationMessage(MessageBoxType.Error, "Erro",
                         "Não foi possível carregar a superintêndencia, favor logar novamente.", MessageBoxButtons.OK, "Deslogar");
                    return;
                }

                if (SCAApplicationContext.Usuario.CodigoSuperintendencia.ToString() == CodigoUnidade)
                {
                    Reporte.Visible = true;
                    btnSalvar.Visible = UsuarioAlteraAcompanhamento;
                }
                else
                {
                    Reporte.Visible = btnSalvar.Visible = false;
                }
            }
            else
            {
                Reporte.Visible = btnSalvar.Visible = false;
            }
        }

        private void PreSalvar()
        {
            Validate();

            if (IsValid)
            {
                if (String.IsNullOrEmpty(txtPercentualReporte.Text))
                {
                    MessageBox.ShowInformationMessage("Por gentileza preencha os campos obrigatórios.", "Erro");
                    return;
                }

                if (String.IsNullOrEmpty(txtDescricaoObservacao.Text))
                {
                    MessageBox.ShowInformationMessage("Por gentileza preencha os campos obrigatórios.", "Erro");
                    return;
                }

                MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Salvar",
                    "Deseja realmente salvar?", MessageBoxButtons.YesNo, "Salvar");
            }
        }

        private void Salvar()
        {
            try
            {
                DTOControleAcao registro;
                if (CodigoSeqControleAcaoParameter > 0)
                {
                    registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterControleAcao(CodigoSeqControleAcaoParameter);
                }
                else
                {
                    registro = new DTOControleAcao();
                }

                registro.DataReporte = txtDataReporte.Text.ToDateTime("dd/MM/yyyy", null);
                registro.ValorPercentualReporte = txtPercentualReporte.Text.Replace("%", "").ToInt16();
                registro.DescricaoObservacao = (txtDescricaoObservacao.Text.Length > 1800) ? txtDescricaoObservacao.Text.Substring(0, 1800) : txtDescricaoObservacao.Text;
                registro.DescricaoCausa = (txtDescricaoCausa.Text.Length > 1800) ? txtDescricaoCausa.Text.Substring(0, 1800) : txtDescricaoCausa.Text;
                registro.DescricaoAcao = (txtDescricaoAcao.Text.Length > 1800) ? txtDescricaoAcao.Text.Substring(0, 1800) : txtDescricaoAcao.Text;
                if (!String.IsNullOrEmpty(txtRecursoFinanceiroUtilizado.Text))
                {
                    registro.ValorRecursoFinanceiro = txtRecursoFinanceiroUtilizado.Text.ToDecimal();
                }
                
                registro.CodigoSeqAcao = CodigoSeqAcaoParameter;
                registro.NomeUsuario = SCAApplicationContext.Usuario.Nome;
                registro.UsuarioAdministrador = UsuarioAdministrador;

                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarControleAcao(registro);

                Recarregar();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void PreRemove(int codigoControleAcao)
        {
            var parametros = new Dictionary<string, object>();
            parametros.Add("CodigoSeqControleAcao", codigoControleAcao);
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente excluir o registro?",
                "Deseja realmente excluir o registro?", MessageBoxButtons.YesNo, "Remover", parametros);
        }

        private void Remover(int codigoSeqControleAcao)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarControleAcao(codigoSeqControleAcao, UsuarioAdministrador);

                Recarregar();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqAcaoParameter > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterAcao(CodigoSeqAcaoParameter);
                    txtDescricaoMeta.Text = registro.DescricaoMeta;
                    txtDescricaoAtividade.Text = registro.DescricaoAtividadeAcao;
                    txtDescricaoEstrategia.Text = registro.DescricaoEstrategiaAcao;

                    txtAnoExercicio.Text = registro.AnoExercicio.ToString();
                    txtArea.Text = registro.UnidadeCorporativa;

                    if (registro.DataInicioAcao.HasValue)
                        txtDataInicioAcao.Text = registro.DataInicioAcao.Value.ToString("dd/MM/yyyy");
                    if (registro.DataFimAcao.HasValue)
                        txtDataFimAcao.Text = registro.DataFimAcao.Value.ToString("dd/MM/yyyy");

                    CarregarControlesAcao(0, "DataReporte", false);

                    ValidaPermissao(registro.CodigoUnidade.ToString());
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro(int codigoControleAcao)
        {
            var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterControleAcao(codigoControleAcao);
            txtDataReporte.Text = registro.DataReporte.ToString("dd/MM/yyyy");
            txtPercentualReporte.Text = registro.ValorPercentualReporte.ToString() + "%";
            txtPercentualReporte.Enabled = true;
            
            txtDescricaoObservacao.Text = registro.DescricaoObservacao;
            txtDescricaoCausa.Text = registro.DescricaoCausa;
            txtDescricaoAcao.Text = registro.DescricaoAcao;
            txtRecursoFinanceiroUtilizado.Text = String.Format("{0:N2}", registro.ValorRecursoFinanceiro);
            CodigoSeqControleAcaoParameter = registro.CodigoSeqControleAcao;
        }

        private void CarregarControlesAcao(int pageIndex, string sortExpression, bool sortAscending)
        {
            var dados = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarControlesPorAcao(CodigoSeqAcaoParameter, pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize, sortExpression, sortAscending);

            if (dados.Count() > 0)
            {
                gdvControleAcao.DataSource = dados.Entities.ToList();

                gdvControleAcao.DataSource = from a in dados
                                              select new
                                              {
                                                  DataReporte = a.DataReporte,
                                                  ValorPercentualReporte = !string.IsNullOrEmpty(a.ValorPercentualReporte.ToString()) ? a.ValorPercentualReporte + "%" : string.Empty,
                                                  DescricaoObservacao = a.DescricaoObservacao,
                                                  DescricaoCausa = a.DescricaoCausa,
                                                  DescricaoAcao = a.DescricaoAcao,
                                                  CodigoSeqControleAcao = a.CodigoSeqControleAcao
                                              };
                gdvControleAcao.DataBind();

                gdvControleAcao.Attributes["SortExpression"] = sortExpression;
                gdvControleAcao.Attributes["SortAscending"] = sortAscending.ToString();

                ucPaginatorConsulta.Visible = (dados.RowsCount > 0);
                ucPaginatorConsulta.TotalRecords = dados.RowsCount;
                ucPaginatorConsulta.PageIndex = pageIndex + 1;
                ucPaginatorConsulta.DataBind();
                upPnlControleAcao.Update();
            }
        }

        private void Recarregar()
        {
            if (PageIndex == 0)
                WebHelper.Redirect("~/Site/Acompanhar/Acao/Detalhar.aspx?CodigoSeqAcao=" + CodigoSeqAcaoParameter);
            else
                WebHelper.Redirect(String.Format("~/Site/Acompanhar/Acao/Detalhar.aspx?CodigoSeqAcao={0}&AnoExercicio={1}&Unidade={2}&Meta={3}&Status={4}&PageIndex={5}", CodigoSeqAcaoParameter, AnoExercicio, Unidade, Meta, Status, PageIndex));
        }

        #endregion


    }
}
