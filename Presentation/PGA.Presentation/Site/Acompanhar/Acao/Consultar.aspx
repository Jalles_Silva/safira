﻿<%@ Page Title="SAFIRA - Consultar Acao" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false"
    CodeBehind="Consultar.aspx.cs" Inherits="PGA.Presentation.Site.Acompanhar.Acao.Consultar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnPesquisar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Consultar Atividades</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                           <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblAnoExercicio" AssociatedControlID="lblAnoExercicio" Text="Exercício" runat="server" />
                                    <asp:DropDownList ID="ddlAnoExercicio" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="ddlArea" Text="Área" runat="server" />
                                    <asp:DropDownList ID="ddlArea" runat="server" CssClass="form-control"  OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    <asp:HiddenField ID="hdfAreaUsuarioLogado" runat="server" />
                                </div>
                            </div>
                            <div class="col-xs-5 col-sm-5">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoMeta" AssociatedControlID="ddlDescricaoMeta" Text="Meta" runat="server" />
                                    <asp:DropDownList ID="ddlDescricaoMeta" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-3 col-sm-3">
                                <div class="form-group">
                                    <asp:Label ID="lblStatus" AssociatedControlID="ddlStatus" Text="Status" runat="server" />
                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="Todas" Value="Todas" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Concluída" Value="Concluida"></asp:ListItem>
                                        <asp:ListItem Text="Concluída com atraso" Value="ConcluidaComAtraso"></asp:ListItem>
                                        <asp:ListItem Text="Concluída no prazo" Value="ConcluidaNoPrazo"></asp:ListItem>
                                        <asp:ListItem Text="Não Concluída" Value="NaoConcluida"></asp:ListItem>
                                        <asp:ListItem Text="Não concluída - em atraso" Value="NaoConcluidaEmAtraso"></asp:ListItem>
                                        <asp:ListItem Text="Não concluída - ainda no prazo" Value="NaoConcluidaAindaNoPrazo"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row pull-right">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <asp:LinkButton ID="btnPesquisar" runat="server" SkinID="btnPesquisar" CausesValidation="false"
                                        ValidationGroup="Pesquisar" />
                                    <asp:HyperLink ID="btnLimpar" runat="server" SkinID="btnLimpar" CausesValidation="false"
                                        NavigateUrl="~/Site/Acompanhar/Acao/Consultar.aspx" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <asp:UpdatePanel ID="upPnlResultadoConsulta" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="area-table">
                                            <asp:Repeater ID="rptAcao" runat="server">
                                                <HeaderTemplate>
                                                    <table>
                                                        <tr runat="server">
                                                            <th colspan="2">Exercício</th>
                                                            <th>Descrição da Atividade</th>
                                                            <th>Estratégia</th>
                                                            <th>Data Início</th>
                                                            <th>Data Fim</th>
                                                            <th>Data do Último Reporte de Execução</th>
                                                            <th>Percentual de Execução</th>
                                                            <th>Ações</th>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td colspan="9" style="background-color: #C5D8CC !important; width: 50px;">
                                                            <asp:Label ID="lblNomeUnidade" runat="server" Text='<%#Eval("Key") %>' />
                                                        </td>
                                                    </tr>
                                                    <asp:Repeater ID="rptAtividadePorMetaEUnidade" OnItemDataBound="rptAcao_ItemDataBound" OnItemCommand="rptAcaoPorMetaEUnidade_ItemCommand" runat="server" DataSource='<%#Eval("Value") %>'>
                                                        <ItemTemplate>
                                                            <tr class="<%# (Container.ItemIndex % 2 == 0 ? "" : "alternate") %>">
                                                                <td style="width: 10px">
                                                                    <asp:HiddenField ID="hdfNomeUnidade" runat="server" Value='<%#Eval("CodigoUnidade") %>' />
                                                                    <asp:HiddenField ID="hdfCodigoFaseExercicio" runat="server" Value='<%#Eval("CodigoFaseExercicio") %>' />
                                                                </td>
                                                                <td style="width: 50px">
                                                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("AnoExercicio") %>' />
                                                                </td>
                                                                <td style="width: 30%">
                                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("DescricaoAtividadeAcao") %>' />
                                                                </td>
                                                                <td style="width: 30%">
                                                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("DescricaoEstrategiaAcao") %>' />
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("DataInicioAcao", "{0:dd/MM/yyyy}") %>' />
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("DataFimAcao", "{0:dd/MM/yyyy}") %>' />
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("DataUltimoReporte", "{0:dd/MM/yyyy}") %>' />
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("PercentualExecucao") %>' />
                                                                </td>
                                                                <td style="width: 50px">
                                                                     <asp:LinkButton ID="lnkDetalhar" runat="server" Target="_self" CommandArgument='<%# Eval("CodigoSeqAcao") %>'
                                                                        CommandName="Detalhar" CausesValidation="false" CssClass="glyphicon glyphicon-pencil" />
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <tr>
                                                        <td colspan="9">
                                                            <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." /></td>
                                                    </tr>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div class="paginator">
                                            <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnPesquisar" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

</asp:Content>
