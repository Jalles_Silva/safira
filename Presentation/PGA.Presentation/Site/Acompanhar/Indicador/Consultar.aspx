﻿<%@ Page Title="SAFIRA - Consultar Indicador" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false"
    CodeBehind="Consultar.aspx.cs" Inherits="PGA.Presentation.Site.Acompanhar.Indicador.Consultar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnPesquisar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Consultar Indicador de Contribuição</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblAnoExercicio" AssociatedControlID="lblAnoExercicio" Text="Exercício" runat="server" />
                                    <asp:DropDownList ID="ddlAnoExercicio" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="ddlArea" Text="Área" runat="server" />
                                    <asp:DropDownList ID="ddlArea" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlArea_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    <asp:HiddenField ID="hdfAreaUsuarioLogado" runat="server" />
                                </div>
                            </div>
                            <div class="col-xs-5 col-sm-5">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoMeta" AssociatedControlID="ddlDescricaoMeta" Text="Meta" runat="server" />
                                    <asp:DropDownList ID="ddlDescricaoMeta" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-3 col-sm-3">
                                <div class="form-group">
                                    <asp:Label ID="lblStatus" AssociatedControlID="ddlStatus" Text="Status" runat="server" />
                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="Todos" Value="Todos" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Atingido" Value="Atingido"></asp:ListItem>
                                        <asp:ListItem Text="Atingido com atraso" Value="AtingidoComAtraso"></asp:ListItem>
                                        <asp:ListItem Text="Atingido no prazo" Value="AtingidoNoPrazo"></asp:ListItem>
                                        <asp:ListItem Text="Não Atingido" Value="NaoAtingido"></asp:ListItem>
                                        <asp:ListItem Text="Não Atingido – em atraso" Value="NaoAtingidoEmAtraso"></asp:ListItem>
                                        <asp:ListItem Text="Não Atingido – ainda no prazo" Value="NaoAtingidoAindaNoPrazo"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row pull-right">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <asp:LinkButton ID="btnPesquisar" runat="server" SkinID="btnPesquisar" CausesValidation="false"
                                        ValidationGroup="Pesquisar" />
                                    <asp:HyperLink ID="btnLimpar" runat="server" SkinID="btnLimpar" CausesValidation="false"
                                        NavigateUrl="~/Site/Acompanhar/Indicador/Consultar.aspx" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <asp:UpdatePanel ID="upPnlResultadoConsulta" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="area-table">
                                            <asp:Repeater ID="rptIndicadores" runat="server">
                                                <HeaderTemplate>
                                                    <table>
                                                        <tr runat="server">
                                                            <th colspan="2">Exercício</th>
                                                            <th>Indicador</th>
                                                            <th>Valor Alvo</th>
                                                            <th>Data Alvo</th>
                                                            <th>Data do Último Valor Informado</th>
                                                            <th>Resultado do Indicador</th>
                                                            <th>Resultado do Indicador em Relação ao Valor Alvo</th>
                                                            <th>Ações</th>
                                                        </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td colspan="9" style="background-color: #C5D8CC !important; width: 50px;">
                                                            <asp:Label ID="lblNomeUnidade" runat="server" Text='<%#Eval("Key") %>' />
                                                        </td>
                                                    </tr>
                                                    <asp:Repeater ID="rptIndicadorPorMetaEUnidade" OnItemDataBound="rptIndicadores_ItemDataBound" OnItemCommand="rptIndicadorPorMetaEUnidade_ItemCommand" runat="server" DataSource='<%#Eval("Value") %>'>
                                                        <ItemTemplate>
                                                            <tr class="<%# (Container.ItemIndex % 2 == 0 ? "" : "alternate") %>">
                                                                <td style="width: 10px">
                                                                    <asp:HiddenField ID="hdfNomeUnidade" runat="server" Value='<%#Eval("CodigoUnidade") %>' />
                                                                    <asp:HiddenField ID="hdfCodigoFaseExercicio" runat="server" Value='<%#Eval("CodigoFaseExercicio") %>' />
                                                                </td>
                                                                <td style="width: 50px">
                                                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("AnoExercicio") %>' />
                                                                </td>
                                                                <td style="width: 30%">
                                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("NoIndicador") %>' />
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("ValorAlvo") %>' />
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("DataAlvo", "{0:MM/yyyy}") %>' />
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("DataUltimoValor", "{0:MM/yyyy}") %>' />
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("ResultadoIndicador") %>' />
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("PercentualAtingimento") %>' />
                                                                </td>
                                                                <td style="width: 50px">
                                                                    <asp:LinkButton ID="lnkDetalhar" runat="server" Target="_self" CommandArgument='<%# Eval("CodigoSeqIndicador") %>'
                                                                        CommandName="Detalhar" CausesValidation="false" CssClass="glyphicon glyphicon-pencil" />
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <tr>
                                                        <td colspan="9">
                                                            <asp:Label ID="lblEmptyData" runat="server" Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." /></td>
                                                    </tr>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div class="paginator">
                                            <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnPesquisar" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

</asp:Content>
