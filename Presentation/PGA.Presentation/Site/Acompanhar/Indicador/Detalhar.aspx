﻿<%@ Page Title="SAFIRA - Indicador" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false"
    CodeBehind="Detalhar.aspx.cs" Inherits="PGA.Presentation.Site.Acompanhar.Indicador.Detalhar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnSalvar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Acompanhar Indicadores de Contribuição</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblAnoExercicio" AssociatedControlID="txtAnoExercicio" Text="Exercício" runat="server" />
                                    <asp:TextBox ID="txtAnoExercicio" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="txtArea" Text="Área" runat="server" />
                                    <asp:TextBox ID="txtArea" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoMeta" AssociatedControlID="txtDescricaoMeta" Text="Meta" runat="server" />
                                    <asp:TextBox ID="txtDescricaoMeta" runat="server" ToolTip="Descrição da Meta" CssClass="form-control" Enabled="false" TextMode="MultiLine"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblNomeIndicador" AssociatedControlID="txtNomeIndicador" Text="Indicador de Contribuição" runat="server" />
                                    <asp:TextBox ID="txtNomeIndicador" runat="server" ToolTip="Nome do Indicador de Distribuição" CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                              <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoValor" AssociatedControlID="ddlTipoValor" Text="Tipo Valor" runat="server" />
                                    <asp:DropDownList ID="ddlTipoValor" runat="server" CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblLinhaBase" AssociatedControlID="txtLinhaBase" Text="Linha de Base" runat="server" />
                                    <asp:TextBox ID="txtLinhaBase" runat="server" ToolTip="Linha de Base" CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblMesReferenciaLinhaBase" AssociatedControlID="txtMesReferenciaLinhaBase" Text="Mês de Referência da Linha de Base" runat="server" />
                                    <asp:TextBox ID="txtMesReferenciaLinhaBase" runat="server" ToolTip="Mês de Referência da Linha de Base" CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblDescricaoFormula" AssociatedControlID="txtDescricaoFormula" Text="Fórmula" runat="server" />
                                    <asp:LinkButton ID="LinkButton6" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaFormula" ForeColor="#034623" />
                                    <asp:TextBox ID="txtDescricaoFormula" runat="server" ToolTip="Descricao Formula" Enabled="false" CssClass="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblValorAlvo" AssociatedControlID="txtValorAlvo" Text="Valor Alvo" runat="server" />
                                    <asp:TextBox ID="txtValorAlvo" runat="server" ToolTip="Valor Alvo" CssClass="form-control" Enabled="false" Style="text-align: right" />
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblDataAlvo" AssociatedControlID="txtDataAlvo" Text="Data Alvo" runat="server" />
                                    <asp:TextBox ID="txtDataAlvo" runat="server" ToolTip="Data Alvo" CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoResultado" AssociatedControlID="ddlTipoResultado" Text="Tipo Resultado" runat="server" />
                                    <asp:LinkButton ID="LinkButton11" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaTipoResultado" ForeColor="#034623" />
                                    <asp:DropDownList ID="ddlTipoResultado" runat="server" CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default" runat="server" id="Reporte">
                            <div class="panel-heading">
                                <h3 class="panel-title">Histórico de Indicadores de Contribuição</h3>
                            </div>
                            <div class="panel-body">
                                <asp:UpdatePanel ID="upPnlControleIndicador" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6">
                                                <div class="form-group">
                                                    <asp:Label ID="lblDataIndicador" AssociatedControlID="txtDataIndicador" Text="Data Indicador *" runat="server" />
                                                    <asp:LinkButton ID="lnkAjudalDataIndicador" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaDataIndicador" ForeColor="#034623" />
                                                    <asp:TextBox ID="txtDataIndicador" runat="server" ToolTip="Data Indicador" CssClass="form-control" />
                                                    <asp:RequiredFieldValidator ID="rfvDataIndicador" runat="server" Display="None" ControlToValidate="txtDataIndicador" ErrorMessage="Por gentileza preencha os campos obrigatórios." ValidationGroup="Salvar" />
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6">
                                                <div class="form-group">
                                                    <asp:Label ID="lblValorIndicador" AssociatedControlID="txtValorIndicador" Text="Valor Indicador *" runat="server" />
                                                    <asp:LinkButton ID="lnkValorIndicador" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaValorIndicador" ForeColor="#034623" />
                                                    <asp:TextBox ID="txtValorIndicador" runat="server" ToolTip="Valor Indicador" CssClass="form-control"></asp:TextBox>
                                                    <asp:TextBox ID="txtValorIndicadorPercentual" runat="server" ToolTip="Valor Indicador" CssClass="form-control"></asp:TextBox>
                                                   <%-- <ajaxToolkit:MaskedEditExtender ID="maksEdit" runat="server" TargetControlID="txtValorIndicadorPercentual" Mask="999,99%" MessageValidatorTip="false" MaskType="Number" InputDirection="RightToLeft" AcceptNegative="None" ClearTextOnInvalid="true" />
                                                    <%-- ClearMaskOnLostFocus="false" 
                                                    <ajaxToolkit:MaskedEditValidator runat="server"
    ControlExtender="maksEdit"
    ControlToValidate="txtValorIndicadorPercentual" 
    IsValidEmpty="False" 
    MaximumValue="100" 
    EmptyValueMessage="Number is required"
    InvalidValueMessage="Number is invalid" 
    MaximumValueMessage="MEnsagem para quando for acima do valor máximo"
    MinimumValueMessage="Mensagem para quando for abaixo co valor mínimo" 
    MinimumValue="1111111111" ValidationGroup="Demo1"
    Display="Dynamic" 
    TooltipMessage="coloque a aqui a mensagem que você se apareça como dica"/>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblDescricaoObservacao" AssociatedControlID="txtDescricaoObservacao" Text="Fato *" runat="server" />
                                                    <asp:LinkButton ID="lnkObservacao" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaObservacao" ForeColor="#034623" />
                                                    <asp:TextBox ID="txtDescricaoObservacao" runat="server" ToolTip="DescricaoObservacao" CssClass="form-control" MaxLength="250" TextMode="MultiLine" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblDescricaoCausa" AssociatedControlID="txtDescricaoCausa" Text="Causa" runat="server" />
                                                    <asp:LinkButton ID="lnkCausa" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaCausa" ForeColor="#034623" />
                                                    <asp:TextBox ID="txtDescricaoCausa" runat="server" ToolTip="DescricaoCausa" CssClass="form-control" MaxLength="1800" TextMode="MultiLine" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <asp:Label ID="lblDescricaoAcao" AssociatedControlID="txtDescricaoAcao" Text="Ação" runat="server" />
                                                    <asp:LinkButton ID="lnkAjudaAcao" runat="server" CausesValidation="false" CssClass="glyphicon glyphicon-question-sign" data-toggle="modal" data-target="#modalAjudaAcao" ForeColor="#034623" />
                                                    <asp:TextBox ID="txtDescricaoAcao" runat="server" ToolTip="DescricaoAcao" CssClass="form-control" MaxLength="1800" TextMode="MultiLine" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <div class="area-table">
                                                    <asp:GridView ID="gdvControleIndicador" runat="server"
                                                        Width="100%" AutoGenerateColumns="False"
                                                        EmptyDataText="Nenhum registro encontrado.">
                                                        <EmptyDataTemplate>Nenhum registro encontrado.</EmptyDataTemplate>
                                                        <AlternatingRowStyle CssClass="alternate" />
                                                        <Columns>
                                                            <asp:BoundField HeaderText="Data Indicador" DataField="DataIndicador" SortExpression="DataIndicador" DataFormatString="{0:MM/yyyy}" />
                                                            <asp:BoundField HeaderText="Valor Indicador" DataField="ValorIndicador" ItemStyle-HorizontalAlign="Right" SortExpression="ValorIndicador" />
                                                            <asp:BoundField HeaderText="Fato" DataField="DescricaoObservacao" SortExpression="DescricaoObservacao" />
                                                            <asp:BoundField HeaderText="Causa" DataField="DescricaoCausa" SortExpression="DescricaoCausa" />
                                                            <asp:BoundField HeaderText="Acao" DataField="DescricaoAcao" SortExpression="DescricaoAcao" />
                                                            <asp:TemplateField HeaderText="Ações" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkDetalhar" runat="server" CommandArgument='<%# Eval("CodigoSeqControleIndicador") %>'
                                                                        CommandName="Detalhar" CausesValidation="false" CssClass="glyphicon glyphicon-pencil"
                                                                        Visible='<%# Convert.ToInt32(Eval("CodigoSeqControleIndicador")) == 0 ? false : true %>' />
                                                                    <asp:LinkButton ID="lnkExcluir" runat="server" CommandArgument='<%# Eval("CodigoSeqControleIndicador") %>'
                                                                        CommandName="Excluir" CausesValidation="false" CssClass="glyphicon glyphicon-remove" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="50px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div class="paginator">
                                                    <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="updBotoes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnSalvar" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="Salvar" />
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaDataIndicador" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Data do Indicador</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        A data do indicador refere-se a data de atualização do indicador de contribuição.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaValorIndicador" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Valor do Indicador</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        O valor do indicador corresponde ao valor que está sendo atualizado na data correspondente.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaObservacao" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Fato</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        É a situação no momento da análise (real e atual) considerada indesejada (problema) e que impactou sobre o desempenho do indicador ou da execução do projeto. 
                                        A descrição adequada do "Fato" deve abordar as condições em que o problema foi evidenciado, evitando que o analista realize suposições ou extrapolações
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaCausa" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Causa</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        É o conjunto de motivos que desencadeou o problema. Existem uma sucessão de causas quando se analisa um determinado problema (chamada de cadeia de falhas), 
                                        sendo mais adequada a citação da causa fundamental (ou causa-raiz). A Ferramenta "5-Por que?" é bastante útil para encontrar a causa-raiz, 
                                        cabendo repetir 5 vezes a pergunta "Por quê?" cada causa do problema ocorreu
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaAcao" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Acao</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        É o esforço que deve ser desenvolvimento para atuar sobre a causa e resolver o problema. Para uma adequada descrição da ação, 
                                        sugere-se considerar a resposta a 3 questões: Para que serve a ação (Por que)? Quais ações serão realizadas (O Que)? Como essas ações serão executadas (Como)? 
                                        Quem será responsável por cada ação a ser realizada (Quem)? Quais os prazos associados a cada uma das ações (Quando)?)
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaTipoResultado" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Tipo Resultado</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <p>Corresponde a forma de acompanhamento da evolução do indicador de contribuição: </p>
                                        <p>Acumulado: Indicador representado pela soma dos valores informados; </p>
                                        <p>Média: Média dos valores informados para o indicador.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalAjudaFormula" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Fórmula</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-12">
                                        <p>Este campo apresenta o método de cálculo do indicador, com definição precisa dos elementos que o compõem, numerador e denominador.</p>
                                        <p>Exemplos:</p>
                                        <p>a) ('Quantitativo de Competências Existentes' vezes o 'Grau de Proficiência da Competência') dividido por ('Quantitativo de Competências Necessárias' vezes o 'Grau de Proficiência da Competência')</p>
                                        <p>b) ('Número de Servidores com Titulação' vezes 100) dividido por 'Número Total de Servidores'</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            FuncaoTela();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function ()
        {
            FuncaoTela();
        });

        function FuncaoTela() {
            $("#<%= txtDataIndicador.ClientID %>").mask("00/0000", { placeholder: "" }).val();
            $("#<%= txtDataIndicador.ClientID %>").datepicker({ dateFormat: "mm/yy" }).val();

            $("#<%= txtValorIndicador.ClientID %>").mask('000.000.000,00', { reverse: true });
            $("#<%= txtValorIndicadorPercentual.ClientID %>").mask('000,00%', { reverse: true });
            
           <%-- $("#<%= txtValorIndicadorPercentual.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text != null) {
                    text = text.replace('%', '').split(',');
                    var valorInteiro = text[0];
                    var valorDecimal = text[1] != undefined ? completaZeroEsquerda(text[1], 2, '0') : '';
                    if (valorInteiro <= 100 && valorInteiro > 0) {
                        $("#<%= txtValorIndicadorPercentual.ClientID %>").val();
                    } else {
                        if (valorInteiro.substring(0, 2) > 10) {
                            $("#<%= txtValorIndicadorPercentual.ClientID %>").val(valorInteiro.substring(0, 2) + "," + valorDecimal + "%");
                        } else if (valorInteiro.substring(0, 1) == 1) {
                            $("#<%= txtValorIndicadorPercentual.ClientID %>").val(valorInteiro.substring(0, 3) + "," + valorDecimal + "%");
                        } else {
                            var value = valorInteiro.substring(0, 2) + (text[1] != undefined ? "," + valorDecimal : "") + "%";
                            $("#<%= txtValorIndicadorPercentual.ClientID %>").val(value);
                        }
                    }
                }
            });--%>

            $("#<%= txtDescricaoObservacao.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoObservacao.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtDescricaoObservacao.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoObservacao.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });

            $("#<%= txtDescricaoCausa.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoCausa.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtDescricaoCausa.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoCausa.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });

            $("#<%= txtDescricaoAcao.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoAcao.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtDescricaoAcao.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoAcao.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });
        }
    </script>
</asp:Content>
