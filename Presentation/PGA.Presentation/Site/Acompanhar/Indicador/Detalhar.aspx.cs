﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using PGA.Common;

namespace PGA.Presentation.Site.Acompanhar.Indicador
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqIndicadorParameter
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqIndicador").ToInt32();
            }
        }

        private int CodigoSeqControleIndicadorParameter
        {
            get
            {
                return ViewState["CodigoSeqControleIndicadorParameter"].ToInt32();
            }
            set
            {
                ViewState["CodigoSeqControleIndicadorParameter"] = value;
            }
        }

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private int AnoExercicio
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("AnoExercicio").ToInt32();
            }
        }

        private int Meta
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Meta").ToInt32();
            }
        }

        private string Status
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Status");
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
            gdvControleIndicador.RowCommand += new GridViewCommandEventHandler(gdvControleIndicador_RowCommand);
            gdvControleIndicador.RowDataBound += gdvControleIndicador_RowDataBound;
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Voltar",
                "Deseja retornar para a página de consulta (dados não salvos serão perdidos)?", MessageBoxButtons.YesNo, "Voltar");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CarregarControles();
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Salvar":
                    if (e.Result == MessageBoxResult.Yes)
                        Salvar();
                    break;
                case "Recarregar":
                    Recarregar();
                    break;
                case "Voltar":
                     if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                         WebHelper.Redirect(String.Format("~/Site/Acompanhar/Indicador/Consultar.aspx?CodigoSeqIndicador={0}&AnoExercicio={1}&Unidade={2}&Meta={3}&Status={4}&PageIndex={5}", CodigoSeqIndicadorParameter, AnoExercicio, Unidade, Meta, Status, PageIndex));
                    break;
                case "Remover":
                    if (e.Result == MessageBoxResult.Yes)
                        Remover(e.Parameters["CodigoSeqControleIndicador"].ToInt32());
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            CarregarControlesIndicador(e.NewPage - 1, "DataIndicador", false);
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            PreSalvar();
        }

        protected void gdvControleIndicador_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Excluir":
                    PreRemove(e.CommandArgument.ToInt32());
                    break;
                case "Detalhar":
                    CarregarRegistro(e.CommandArgument.ToInt32());
                    break;
                default:
                    break;
            }
        }

        protected void gdvControleIndicador_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[5].Visible = UsuarioAlteraAcompanhamento;
        }

        #endregion

        #region [Methods]

        private void CarregarControles()
        {
            try
            {
                CarregarRegistro();
                ValidaValorReportado();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }

            
        }

        private void ValidaValorReportado()
        {
            try
            {
                if (ddlTipoValor.SelectedValue.ToInt32() == 2)
                {
                    txtValorIndicador.Visible = false;
                    txtValorIndicadorPercentual.Visible = true;
                }
                else if (ddlTipoValor.SelectedValue.ToInt32() == 1)
                {
                    txtValorIndicador.Visible = true;
                    txtValorIndicadorPercentual.Visible = false;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void ValidaPermissao(string CodigoUnidade)
        {
            if (UsuarioAdministrador)
            {
                Reporte.Visible = true;
                btnSalvar.Visible = UsuarioAlteraAcompanhamento;
            }
            else if (UsuarioCadastrador)
            {
                if (SCAApplicationContext.Usuario.CodigoSuperintendencia == 0)
                {
                    MessageBox.ShowConfirmationMessage(MessageBoxType.Error, "Erro",
                         "Não foi possível carregar a superintêndencia, favor logar novamente.", MessageBoxButtons.OK, "Deslogar");
                    return;
                }

                if (SCAApplicationContext.Usuario.CodigoSuperintendencia.ToString() == CodigoUnidade)
                {
                    Reporte.Visible = true;
                    btnSalvar.Visible = UsuarioAlteraAcompanhamento;
                }
                else
                {
                    Reporte.Visible = btnSalvar.Visible = false;
                }
            }
            else
            {
                Reporte.Visible = btnSalvar.Visible = false;
            }
        }

        private void PreSalvar()
        {
            Validate();

            if (IsValid)
            {
                if (ddlTipoValor.SelectedValue.ToInt32() == 2 && String.IsNullOrEmpty(txtValorIndicadorPercentual.Text))
                {
                    MessageBox.ShowInformationMessage("Por gentileza preencha os campos obrigatórios.", "Erro");
                    return;
                }
                else if (ddlTipoValor.SelectedValue.ToInt32() == 1 && String.IsNullOrEmpty(txtValorIndicador.Text))
                {
                    MessageBox.ShowInformationMessage("Por gentileza preencha os campos obrigatórios.", "Erro");
                    return;
                }

                if (String.IsNullOrEmpty(txtDescricaoObservacao.Text))
                {
                    MessageBox.ShowInformationMessage("Por gentileza preencha os campos obrigatórios.", "Erro");
                    return;
                }


                MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Salvar",
                    "Deseja realmente salvar?", MessageBoxButtons.YesNo, "Salvar");
            }
        }

        private void Salvar()
        {
            try
            {
                DTOControleIndicador registro;
                if (CodigoSeqControleIndicadorParameter > 0)
                {
                    registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterControleIndicador(CodigoSeqControleIndicadorParameter);
                }
                else
                {
                    registro = new DTOControleIndicador();
                }

                if (ddlTipoValor.SelectedValue.ToInt32() == 2)
                {
                    txtValorIndicador.Visible = false;
                    txtValorIndicadorPercentual.Visible = true;

                    registro.ValorIndicador = decimal.Parse(txtValorIndicadorPercentual.Text.Replace("%", ""));
                }
                else if (ddlTipoValor.SelectedValue.ToInt32() == 1)
                {
                    txtValorIndicador.Visible = true;
                    txtValorIndicadorPercentual.Visible = false;

                    registro.ValorIndicador = decimal.Parse(txtValorIndicador.Text);
                }

                registro.DataIndicador = txtDataIndicador.Text.ToDateTime("MM/yyyy", null);
                registro.DescricaoObservacao = (txtDescricaoObservacao.Text.Length > 1800) ? txtDescricaoObservacao.Text.Substring(0, 1800) : txtDescricaoObservacao.Text;
                registro.DescricaoCausa = (txtDescricaoCausa.Text.Length > 1800) ? txtDescricaoCausa.Text.Substring(0, 1800) : txtDescricaoCausa.Text;
                registro.DescricaoAcao = (txtDescricaoAcao.Text.Length > 1800) ? txtDescricaoAcao.Text.Substring(0, 1800) : txtDescricaoAcao.Text;
                registro.CodigoSeqIndicador = CodigoSeqIndicadorParameter;
                registro.NomeUsuario = SCAApplicationContext.Usuario.Nome;
                registro.UsuarioAdministrador = UsuarioAdministrador;

                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarControleIndicador(registro);

                Recarregar();
                
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void PreRemove(int codigoControleIndicador)
        {
            var parametros = new Dictionary<string, object>();
            parametros.Add("CodigoSeqControleIndicador", codigoControleIndicador);
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente excluir o registro?",
                "Deseja realmente excluir o registro?", MessageBoxButtons.YesNo, "Remover", parametros);
        }

        private void Remover(int codigoControleIndicador)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarControleIndicador(codigoControleIndicador, UsuarioAdministrador);

                Recarregar();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqIndicadorParameter > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterIndicador(CodigoSeqIndicadorParameter);

                    if (!registro.MesReferencia.HasValue || !registro.DataAlvo.HasValue || registro.CodigoSeqTipoValor == 0)
                    {
                        MessageBox.ShowInformationMessage("Dados do Indicador de Contribuição estão incompletos.", "Voltar");
                        return;
                    }

                    txtAnoExercicio.Text = registro.AnoExercicio.ToString();
                    txtArea.Text = registro.UnidadeCorporativa;

                    txtDescricaoMeta.Text = registro.DescricaoMeta;
                    txtNomeIndicador.Text = registro.NoIndicador;
                    txtLinhaBase.Text = registro.ValorLinhaBase.ToString();
                    txtDescricaoFormula.Text = registro.DescricaoFormula;
                    txtMesReferenciaLinhaBase.Text = registro.MesReferencia.Value.ToString("MM/yyyy");
                    txtDataAlvo.Text = registro.DataAlvo.Value.ToString("MM/yyyy");
                    txtValorAlvo.Text = ApresentaValores(registro.ValorAlvo, registro.CodigoSeqTipoValor.ToInt32(), false).ToString();//registro.ValorAlvo.ToString().Replace(",00", "%");
                    CarregarTipoValor();
                    ddlTipoValor.SelectedValue = registro.CodigoSeqTipoValor.ToString();
                    CarregarTipoResultado();
                    ddlTipoResultado.SelectedValue = registro.TipoResultado.Codigo.ToString();

                    CarregarControlesIndicador(0, "DataIndicador", false);
                    ValidaPermissao(registro.CodigoUnidade.ToString());
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro(int codigoControleIndicador)
        {
            var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterControleIndicador(codigoControleIndicador);
            txtDataIndicador.Text = registro.DataIndicador.ToString("MM/yyyy");


            if (ddlTipoValor.SelectedValue.ToInt32() == 2)
            {
                txtValorIndicador.Visible = false;
                txtValorIndicadorPercentual.Visible = true;

                txtValorIndicadorPercentual.Text = registro.ValorIndicador.ToString()+"%";
            }
            else if (ddlTipoValor.SelectedValue.ToInt32() == 1)
            {
                txtValorIndicador.Visible = true;
                txtValorIndicadorPercentual.Visible = false;

                txtValorIndicador.Text = registro.ValorIndicador.ToString();
            }

            txtDescricaoObservacao.Text = registro.DescricaoObservacao;
            txtDescricaoCausa.Text = registro.DescricaoCausa;
            txtDescricaoAcao.Text = registro.DescricaoAcao;
            CodigoSeqControleIndicadorParameter = registro.CodigoSeqControleIndicador;
        }

        private void CarregarControlesIndicador(int pageIndex, string sortExpression, bool sortAscending)
        {
            var dados = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarControlesPorIndicador(CodigoSeqIndicadorParameter, pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize, sortExpression, sortAscending);

            if (dados.Count() > 0)
            {
                gdvControleIndicador.DataSource = dados.Entities.ToList();

                gdvControleIndicador.DataSource = from a in dados
                                             select new
                                             {
                                                 DataIndicador = a.DataIndicador.ToString("MM/yyyy"),
                                                 ValorIndicador = ApresentaValores(a.ValorIndicador,(int)a.CodigoSeqTipoValor,false).ToString(),
                                                 DescricaoObservacao = a.DescricaoObservacao,
                                                 DescricaoCausa = a.DescricaoCausa,
                                                 DescricaoAcao = a.DescricaoAcao,
                                                 CodigoSeqTipoValor = a.CodigoSeqTipoValor,
                                                 CodigoSeqControleIndicador = a.CodigoSeqControleIndicador
                                             };

                gdvControleIndicador.DataBind();

                gdvControleIndicador.Attributes["SortExpression"] = sortExpression;
                gdvControleIndicador.Attributes["SortAscending"] = sortAscending.ToString();

                ucPaginatorConsulta.Visible = (dados.RowsCount > 0);
                ucPaginatorConsulta.TotalRecords = dados.RowsCount;
                ucPaginatorConsulta.PageIndex = pageIndex + 1;
                ucPaginatorConsulta.DataBind();
                upPnlControleIndicador.Update();


                //lblNomeTotal.Text = (txtDataAlvo.Text.ToDateTime("dd/MM/yyyy", null) > DateTime.Now) ? "Parcial: " : "Acumulado: ";
                //txtTotal.Text = dados.Entities.Sum(i => i.ValorIndicador).ToString() + ((ddlTipoValor.SelectedValue == "2") ? "%" : String.Empty);

            }
        }
        
        private void CarregarTipoValor()
        {
            ddlTipoValor.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTipoValores();
            ddlTipoValor.DataTextField = "DescricaoTipo";
            ddlTipoValor.DataValueField = "CodigoSeqTipoValor";
            ddlTipoValor.DataBind();
            ddlTipoValor.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void CarregarTipoResultado()
        {
            ddlTipoResultado.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTipoResultado();
            ddlTipoResultado.DataTextField = "Descricao";
            ddlTipoResultado.DataValueField = "Codigo";
            ddlTipoResultado.DataBind();
            ddlTipoResultado.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void Recarregar()
        {
            if (PageIndex == 0)
                WebHelper.Redirect("~/Site/Acompanhar/Indicador/Detalhar.aspx?CodigoSeqIndicador=" + CodigoSeqIndicadorParameter);
            else
                WebHelper.Redirect(String.Format("~/Site/Acompanhar/Indicador/Detalhar.aspx?CodigoSeqIndicador={0}&AnoExercicio={1}&Unidade={2}&Meta={3}&Status={4}&PageIndex={5}", CodigoSeqIndicadorParameter, AnoExercicio, Unidade, Meta, Status, PageIndex));
        }

        #endregion
    }
}
