﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;

namespace PGA.Presentation.Site.Acompanhar.Encaminhamento
{
    public partial class Visualizar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqEncaminhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqEncaminhamento").ToInt32();
            }
        }

        private int TipoAcompanhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoAcompanhamento").ToInt32();
            }
        }

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private int Status
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Status").ToInt32();
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            lnkBaixarAnexo.Click += new EventHandler(lnkBaixarAnexo_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Acompanhar/Encaminhamento/Consultar.aspx?TipoAcompanhamento={0}&Unidade={1}&Status={2}&PageIndex={3}", TipoAcompanhamento, Unidade, Status, PageIndex));
        }

        protected void lnkBaixarAnexo_Click(object sender, EventArgs e)
        {
            var lnk = (LinkButton)sender;

            WebHelper.DownloadFile(System.IO.File.ReadAllBytes(lnk.Attributes["CaminhoAnexo"]), lblAnexo.Text);
        }

        #endregion

        #region [Methods]

        private void CarregarReportesEncaminhamentos()
        {
            try
            {
                var reportes = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarReportesDoEncaminhamento(CodigoSeqEncaminhamento);

                grdReportes.DataSource = reportes;
                grdReportes.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqEncaminhamento > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterEncaminhamento(CodigoSeqEncaminhamento);

                    if (registro.Acompanhamento != null)
                    {
                        if (registro.Acompanhamento.DataAcompanhamento != null)
                            lblDataAcompanhamento.Text = registro.Acompanhamento.DataAcompanhamento.ToShortDateString();

                        if (registro.Acompanhamento.Exercicio != null)
                            lblAnoExercicio.Text = registro.Acompanhamento.Exercicio.Ano.ToString();

                        if (registro.Acompanhamento.TipoAcompanhamento != null)
                            lblTipoAcompanhamento.Text = registro.Acompanhamento.TipoAcompanhamento.DescricaoTipoAcompanhamento;

                        if (registro.Acompanhamento.Anexo != null)
                        {
                            lblAnexo.Text = registro.Acompanhamento.Anexo.DescricaoAnexo;
                            lnkBaixarAnexo.Attributes["CaminhoAnexo"] = registro.Acompanhamento.Anexo.CaminhoAnexo;
                        }
                        else
                        {
                            lnkBaixarAnexo.Text = "Sem Anexo";
                            lnkBaixarAnexo.Enabled = false;
                        }

                        if (registro.Acompanhamento.StatusAcompanhamento != null)
                            lblStatusAcompanhamento.Text = registro.Acompanhamento.StatusAcompanhamento.DescricaoStatusAcompanhamento;
                    }

                    if (registro.CorporativoUnidade != null)
                        lblArea.Text = registro.CorporativoUnidade.DescricaoUnidade;

                    if (registro.Meta != null)
                        lblMeta.Text = registro.Meta.DescricaoMeta;

                    if (registro.DescricaoEncaminhamento != null)
                        lblEncaminhamento.Text = registro.DescricaoEncaminhamento;

                    if (registro.PrazoAtendimento != null)
                        lblPrazoEncaminhamento.Text = registro.PrazoAtendimento.ToShortDateString();

                    if (registro.StatusEncaminhamento != null)
                        lblStatusEncaminhamento.Text = registro.StatusEncaminhamento.DescricaoStatusEncaminhamento;

                    CarregarReportesEncaminhamentos();
                }
                else
                    WebHelper.Redirect("~/Site/Acompanhar/Encaminhamento/Consultar.aspx");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        #endregion
    }
}