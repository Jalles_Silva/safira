﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;

namespace PGA.Presentation.Site.Acompanhar.Encaminhamento
{
    public partial class Reportar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqEncaminhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqEncaminhamento").ToInt32();
            }
        }

        private int TipoAcompanhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoAcompanhamento").ToInt32();
            }
        }

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private int Status
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Status").ToInt32();
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            lnkBaixarAnexo.Click += new EventHandler(lnkBaixarAnexo_Click);
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Remover":
                    if (e.Result == MessageBoxResult.Yes)
                        Remover(e.Parameters["codigoSeqReporteEncaminhamento"].ToInt32());
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            CarregarReportesEncaminhamentos(e.NewPage - 1);
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            Validate();

            if (IsValid)
            {
                try
                {
                    DTOReporteEncaminhamento registro = new DTOReporteEncaminhamento();
                    registro.CodigoSeqReporteEncaminhamento = !String.IsNullOrEmpty(hdfCodReporteEncaminhamento.Value) ? Int32.Parse(hdfCodReporteEncaminhamento.Value) : 0;
                    registro.DescricaoReporteEncaminhamento = txtComentario.Text;
                    registro.DataReporte = DateTime.Now;
                    registro.Usuario = Context.User.Identity.Name;

                    registro.Encaminhamento = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterEncaminhamento(CodigoSeqEncaminhamento);

                    if (registro.Encaminhamento.StatusEncaminhamento.CodigoSeqStatusEncaminhamento != drpStatusEncaminhamento.SelectedValue.ToInt32())
                        this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").AlterarStatusEncaminhamento(CodigoSeqEncaminhamento, drpStatusEncaminhamento.SelectedValue.ToInt32());

                    if (!String.IsNullOrEmpty(registro.DescricaoReporteEncaminhamento))
                        this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarReporteEncaminhamento(registro);

                    MessageBox.ShowInformationMessage("Registro salvo com sucesso.");

                    hdfCodReporteEncaminhamento.Value =
                    txtComentario.Text = "";
                    CarregarReportesEncaminhamentos(ucPaginatorConsulta.PageIndex - 1);
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Acompanhar/Encaminhamento/Consultar.aspx?TipoAcompanhamento={0}&Unidade={1}&Status={2}&PageIndex={3}", TipoAcompanhamento, Unidade, Status, PageIndex));
        }

        protected void lnkBaixarAnexo_Click(object sender, EventArgs e)
        {
            var lnk = (LinkButton)sender;

            WebHelper.DownloadFile(System.IO.File.ReadAllBytes(lnk.Attributes["CaminhoAnexo"]), lblAnexo.Text);
        }

        protected void grdReportes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Excluir":
                    {
                        var parametros = new Dictionary<string, object>();
                        parametros.Add("codigoSeqReporteEncaminhamento", e.CommandArgument);

                        MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente excluir o registro?",
                            "Deseja realmente excluir o registro?", MessageBoxButtons.YesNo, "Remover", parametros);
                    }
                    break;
                case "Detalhar":
                    CarregarReporteEncaminhamento(e.CommandArgument.ToInt32());
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region [Methods]

        private void CarregarReportesEncaminhamentos(int pageIndex)
        {
            try
            {
                var reportes = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarReportesPorEncaminhamento(CodigoSeqEncaminhamento, pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize, "CodigoSeqReporteEncaminhamento", true);

                grdReportes.DataSource = reportes;
                grdReportes.DataBind();

                drpStatusEncaminhamento.Enabled =
                ucPaginatorConsulta.Visible = (reportes.RowsCount > 0);
                ucPaginatorConsulta.TotalRecords = reportes.RowsCount;
                ucPaginatorConsulta.PageIndex = pageIndex + 1;
                ucPaginatorConsulta.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqEncaminhamento > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterEncaminhamento(CodigoSeqEncaminhamento);

                    if (registro.Acompanhamento != null)
                    {
                        if (registro.Acompanhamento.DataAcompanhamento != null)
                            txtDataAcompanhamento.Text = registro.Acompanhamento.DataAcompanhamento.ToShortDateString();

                        if (registro.Acompanhamento.Exercicio != null)
                            txtAnoExercicio.Text = registro.Acompanhamento.Exercicio.Ano.ToString();

                        if (registro.Acompanhamento.TipoAcompanhamento != null)
                            txtTipoAcompanhamento.Text = registro.Acompanhamento.TipoAcompanhamento.DescricaoTipoAcompanhamento;

                        if (registro.Acompanhamento.Anexo != null)
                        {
                            lblAnexo.Text = registro.Acompanhamento.Anexo.DescricaoAnexo;
                            lnkBaixarAnexo.Attributes["CaminhoAnexo"] = registro.Acompanhamento.Anexo.CaminhoAnexo;
                        }
                        else
                        {
                            lnkBaixarAnexo.Text = "Sem Anexo";
                            lnkBaixarAnexo.Enabled = false;
                        }
                    }

                    if (registro.CorporativoUnidade != null)
                        txtArea.Text = registro.CorporativoUnidade.DescricaoUnidade;

                    if (registro.Meta != null)
                        txtMeta.Text = registro.Meta.DescricaoMeta;

                    if (registro.DescricaoEncaminhamento != null)
                        txtEncaminhamento.Text = registro.DescricaoEncaminhamento;

                    if (registro.PrazoAtendimento != null)
                        txtPrazoEncaminhamento.Text = registro.PrazoAtendimento.ToShortDateString();

                    CarregarReportesEncaminhamentos(ucPaginatorConsulta.PageIndex - 1);
                }
                else
                    WebHelper.Redirect("~/Site/Acompanhar/Encaminhamento/Consultar.aspx");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarReporteEncaminhamento(int codigoSeqReporteEncaminhamento)
        {
            try
            {
                if (codigoSeqReporteEncaminhamento > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterReporteEncaminhamento(codigoSeqReporteEncaminhamento);

                    hdfCodReporteEncaminhamento.Value = registro.CodigoSeqReporteEncaminhamento.ToString();
                    txtComentario.Text = registro.DescricaoReporteEncaminhamento;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void Remover(int codigoSeqReporteEncaminhamento)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarReporteEncaminhamento(codigoSeqReporteEncaminhamento);
                MessageBox.ShowInformationMessage("Registro excluido com sucesso!");
                CarregarReportesEncaminhamentos(ucPaginatorConsulta.PageIndex - 1);
            }
            catch
            {
                MessageBox.ShowErrorMessage("Não foi possível excluir o registro devido ao fato de ele já ter vinculação.");
            }
        }

        #endregion
    }
}