﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;

namespace PGA.Presentation.Site.Acompanhar.Encaminhamento
{
    public partial class Consultar : CustomPageBase
    {
        #region [Properties]

        private int TipoAcompanhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoAcompanhamento").ToInt32();
            }
        }

        private int Unidade
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Unidade").ToInt32();
            }
        }

        private int Status
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Status").ToInt32();
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            btnPesquisar.Click += BtnPesquisar_Click;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarTipoAcompanhamento();
                    CarregarArea();

                    if (TipoAcompanhamento > 0)
                        drpTipoAcompanhamento.SelectedValue = TipoAcompanhamento.ToString();
                    if (Unidade > 0)
                        drpArea.SelectedValue = Unidade.ToString();
                    if (Status > 0)
                        rblStatus.SelectedValue = Status.ToString();
                    if (PageIndex > 0)
                        ConsultarDados(PageIndex - 1);
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            ConsultarDados(e.NewPage - 1);
        }

        private void BtnPesquisar_Click(object sender, EventArgs e)
        {
            ConsultarDados(0);
        }

        protected void rptEncaminhamentoPorUnidade_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Reportar":
                    WebHelper.Redirect(String.Format("~/Site/Acompanhar/Encaminhamento/Reportar.aspx?CodigoSeqEncaminhamento={0}&TipoAcompanhamento={1}&Unidade={2}&Status={3}&PageIndex={4}", e.CommandArgument, drpTipoAcompanhamento.SelectedValue, drpArea.SelectedValue, rblStatus.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                case "Visuzalizar":
                    WebHelper.Redirect(String.Format("~/Site/Acompanhar/Encaminhamento/Visualizar.aspx?CodigoSeqEncaminhamento={0}&TipoAcompanhamento={1}&Unidade={2}&Status={3}&PageIndex={4}", e.CommandArgument, drpTipoAcompanhamento.SelectedValue, drpArea.SelectedValue, rblStatus.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region [Methods]

        private void CarregarTipoAcompanhamento()
        {
            drpTipoAcompanhamento.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTiposAcompanhamento();
            drpTipoAcompanhamento.DataTextField = "DescricaoTipoAcompanhamento";
            drpTipoAcompanhamento.DataValueField = "CodigoSeqTipoAcompanhamento";
            drpTipoAcompanhamento.DataBind();
            drpTipoAcompanhamento.Items.Insert(0, new ListItem("Todos", "0"));
        }

        private void CarregarArea()
        {
            drpArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarCorporativoUnidadePorExercicio(0);
            drpArea.DataTextField = "DescricaoUnidade";
            drpArea.DataValueField = "CodigoUnidade";
            drpArea.DataBind();
            drpArea.Items.Insert(0, new ListItem("Todas", "0"));
        }

        private void ConsultarDados(int pageIndex)
        {
            Validate();

            if (IsValid)
            {
                try
                {
                    var encaminhamentos = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarEncaminhamentosPorTipoUnidadeEStatus(drpTipoAcompanhamento.SelectedValue.ToInt32(), drpArea.SelectedItem.Text, rblStatus.SelectedValue.ToInt32(), pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize, "CorporativoUnidade.DescricaoUnidade", false);

                    List<DTOEncaminhamento> source = encaminhamentos.Entities.ToList();

                    List<string> unidades = source.OrderBy(u => u.CorporativoUnidade.DescricaoUnidade).Select(i => i.CorporativoUnidade.DescricaoUnidade + " - " + i.Acompanhamento.TipoAcompanhamento.DescricaoTipoAcompanhamento).Distinct().ToList();
                    Dictionary<string, IList<DTOEncaminhamento>> lista = new Dictionary<string, IList<DTOEncaminhamento>>();

                    foreach (var item in unidades)
                    {
                        lista.Add(item, source.Where(i => i.CorporativoUnidade.DescricaoUnidade + " - " + i.Acompanhamento.TipoAcompanhamento.DescricaoTipoAcompanhamento == item).OrderBy(x => x.DescricaoEncaminhamento).ToList());
                    }

                    rptEncaminhamento.DataSource = lista;
                    rptEncaminhamento.DataBind();

                    ucPaginatorConsulta.Visible = (encaminhamentos.RowsCount > 0);
                    ucPaginatorConsulta.TotalRecords = encaminhamentos.RowsCount;
                    ucPaginatorConsulta.PageIndex = pageIndex + 1;
                    ucPaginatorConsulta.DataBind();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        #endregion
    }
}