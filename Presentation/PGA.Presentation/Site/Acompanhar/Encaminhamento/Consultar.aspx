﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Consultar.aspx.cs" Inherits="PGA.Presentation.Site.Acompanhar.Encaminhamento.Consultar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Pesquisar Encaminhamento do SAFIRA</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoAcompanhamento" AssociatedControlID="drpTipoAcompanhamento" Text="Acompanhamento do SAFIRA" runat="server" />
                                    <asp:DropDownList ID="drpTipoAcompanhamento" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4">
                                <div class="form-group">
                                    <asp:Label ID="lblArea" AssociatedControlID="drpArea" Text="Área" runat="server" />
                                    <asp:DropDownList ID="drpArea" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <asp:Label ID="lblStatus" AssociatedControlID="rblStatus" Text="Status" runat="server" />
                                    <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal" CssClass="radioListHoriz">
                                        <asp:ListItem Value="0" Text="Todos" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Pendente"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Concluído"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-12">
                                    <asp:LinkButton ID="btnPesquisar" runat="server" SkinID="btnPesquisar" CausesValidation="false" />
                                    <asp:HyperLink ID="btnLimpar" runat="server" SkinID="btnLimpar" CausesValidation="false"
                                        NavigateUrl="~/Site/Acompanhar/Encaminhamento/Consultar.aspx" />
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="upPnlResultadoConsulta" runat="server">
                            <ContentTemplate>
                                <div class="area-table">
                                    <asp:Repeater ID="rptEncaminhamento" runat="server">
                                        <HeaderTemplate>
                                            <table>
                                                <tr>
                                                    <th>Encaminhamento</th>
                                                    <th>Meta</th>
                                                    <th>Exercício</th>
                                                    <th>Data Acompanhamento</th>
                                                    <th>Prazo Atendimento</th>
                                                    <th>Status</th>
                                                    <th>Ações</th>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td colspan="7" style="background-color: #C5D8CC !important;">
                                                    <asp:Label ID="lblNomeUnidade" runat="server" Text='<%#Eval("Key") %>' />
                                                </td>
                                            </tr>
                                            <asp:Repeater ID="rptEncaminhamentoPorUnidade" OnItemCommand="rptEncaminhamentoPorUnidade_ItemCommand" runat="server" DataSource='<%#Eval("Value") %>'>
                                                <ItemTemplate>
                                                    <tr class="<%# (Container.ItemIndex % 2 == 0 ? "" : "alternate") %>">
                                                        <td>
                                                            <div style="margin-left:13px;">
                                                                <asp:Label ID="lblEncaminhamento" runat="server" Text='<%#Eval("DescricaoEncaminhamento")%>' />
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblMeta" runat="server" Text='<%#Eval("Meta.DescricaoMeta")%>' />
                                                        </td>
                                                        <td style="width: 60px; text-align:center;">
                                                            <asp:Label ID="lblExercicio" runat="server" Text='<%#Eval("Acompanhamento.Exercicio.Ano")%>' />
                                                        </td>
                                                        <td style="width: 110px;">
                                                            <asp:Label ID="lblDataAcompanhamento" runat="server" Text='<%#Eval("Acompanhamento.DataAcompanhamento", "{0:dd/MM/yyyy}")%>' />
                                                        </td>
                                                        <td style="width: 100px;">
                                                            <asp:Label ID="lblPrazoAtendimento" runat="server" Text='<%#Eval("PrazoAtendimento", "{0:dd/MM/yyyy}")%>' />
                                                        </td>
                                                        <td style="width: 60px; text-align:center;">
                                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("StatusEncaminhamento.DescricaoStatusEncaminhamento")%>' />
                                                        </td>
                                                        <td style="width: 60px">
                                                            <asp:LinkButton ID="lnkReportar" runat="server" Target="_self" CommandArgument='<%# Eval("CodigoSeqEncaminhamento")%>'
                                                                CommandName="Reportar" CausesValidation="false" CssClass="glyphicon glyphicon-edit"
                                                                Visible='<%# Convert.ToInt32(Eval("StatusEncaminhamento.CodigoSeqStatusEncaminhamento")) == 2 ? false : true %>' />

                                                            <asp:LinkButton ID="lnkVisualizar" runat="server" Target="_self" CommandArgument='<%# Eval("CodigoSeqEncaminhamento")%>'
                                                                CommandName="Visuzalizar" CausesValidation="false" CssClass="glyphicon glyphicon-search" />

                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <tr>
                                                <td colspan="7"><asp:Label ID="lblEmptyData" runat="server"  Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." /></td>
                                            </tr>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                                <div class="paginator">
                                    <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
