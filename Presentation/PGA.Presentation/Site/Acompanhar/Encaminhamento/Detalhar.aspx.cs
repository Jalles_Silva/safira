﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Core;
using SQFramework.Core.Enums;
using SQFramework.Web;
using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;

namespace PGA.Presentation.Site.Acompanhar.Encaminhamento
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodigoSeqAcompanhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("CodigoSeqAcompanhamento").ToInt32();
            }
        }

        private int TipoAcompanhamento
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("TipoAcompanhamento").ToInt32();
            }
        }

        private int AnoExercicio
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("AnoExercicio").ToInt32();
            }
        }

        private int Status
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Status").ToInt32();
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            drpArea.SelectedIndexChanged += drpArea_SelectedIndexChanged;
            lnkBaixarAnexo.Click += new EventHandler(lnkBaixarAnexo_Click);
            btnAdicionar.Click += new EventHandler(btnAdicionar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    CarregarArea();
                    CarregarRegistro();
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Remover":
                    if (e.Result == MessageBoxResult.Yes)
                        Remover(e.Parameters["CodigoSeqEncaminhamento"].ToInt32());
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void drpArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarregarMetas();
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            CarregarEncaminhamentos(e.NewPage - 1);
        }

        protected void btnAdicionar_Click(object sender, EventArgs e)
        {
            Validate();

            if (IsValid)
            {
                if (String.IsNullOrEmpty(txtEncaminhamento.Text))
                {
                    MessageBox.ShowInformationMessage("Favor informar o Encaminhamento.", "Erro");
                    return;
                }

                if (String.IsNullOrEmpty(drpArea.SelectedValue))
                {
                    MessageBox.ShowInformationMessage("Favor informar Área do encaminhamento.", "Erro");
                    return;
                }

                if (String.IsNullOrEmpty(txtPrazoEncaminhamento.Text))
                {
                    MessageBox.ShowInformationMessage("Favor informar o Prazo do encaminhamento.", "Erro");
                    return;
                }

                Salvar();
            }
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            WebHelper.Redirect(String.Format("~/Site/Cadastro/Acompanhamento/Detalhar.aspx?CodigoSeqAcompanhamento={0}&TipoAcompanhamento={1}&AnoExercicio={2}&Status={3}&PageIndex={4}", CodigoSeqAcompanhamento, TipoAcompanhamento, AnoExercicio, Status, PageIndex));
        }

        protected void lnkBaixarAnexo_Click(object sender, EventArgs e)
        {
            var lnk = (LinkButton)sender;

            WebHelper.DownloadFile(System.IO.File.ReadAllBytes(lnk.Attributes["CaminhoAnexo"]), lblAnexo.Text);
        }

        protected void rptEncaminhamentoPorUnidade_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Excluir":
                    {
                        var parametros = new Dictionary<string, object>();
                        parametros.Add("CodigoSeqEncaminhamento", e.CommandArgument);

                        MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente excluir o registro?",
                            "Deseja realmente excluir o registro?", MessageBoxButtons.YesNo, "Remover", parametros);
                    }
                    break;
                case "Detalhar":
                    CarregarEncaminhamento(e.CommandArgument.ToInt32());
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region [Methods]

        private void CarregarArea()
        {
            drpArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarCorporativoUnidadeAtivo();
            drpArea.DataTextField = "DescricaoUnidade";
            drpArea.DataValueField = "CodigoUnidade";
            drpArea.DataBind();
            drpArea.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void CarregarMetas()
        {
            drpMeta.Items.Clear();

            if (!String.IsNullOrEmpty(drpArea.SelectedValue))
            {
                drpMeta.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarMetaSimplificadaPorUnidadeEAno(drpArea.SelectedValue.ToInt32(), 0);
                drpMeta.DataTextField = "DescricaoMeta";
                drpMeta.DataValueField = "CodSeqMeta";
            }

            drpMeta.DataBind();
            drpMeta.Items.Insert(0, new ListItem("Selecione", ""));
        }

        private void CarregarEncaminhamentos(int pageIndex)
        {
            try
            {
                var encaminhamentos = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarEncaminhamentosPorAcompanhamento(CodigoSeqAcompanhamento, pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize, "CorporativoUnidade.DescricaoUnidade", true);

                List<DTOEncaminhamento> source = encaminhamentos.Entities.ToList();

                List<string> unidades = source.Select(i => i.CorporativoUnidade.DescricaoUnidade).Distinct().ToList();
                Dictionary<string, IList<DTOEncaminhamento>> lista = new Dictionary<string, IList<DTOEncaminhamento>>();

                foreach (var item in unidades)
                {
                    lista.Add(item, source.Where(i => i.CorporativoUnidade.DescricaoUnidade == item).OrderBy(x => x.DescricaoEncaminhamento).ToList());
                }

                rptEncaminhamento.DataSource = lista;
                rptEncaminhamento.DataBind();

                ucPaginatorConsulta.Visible = (encaminhamentos.RowsCount > 0);
                ucPaginatorConsulta.TotalRecords = encaminhamentos.RowsCount;
                ucPaginatorConsulta.PageIndex = pageIndex + 1;
                ucPaginatorConsulta.DataBind();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void Salvar()
        {
            try
            {
                DTOEncaminhamento registro = new DTOEncaminhamento();
                registro.CodigoSeqEncaminhamento = !String.IsNullOrEmpty(hdfCodEncaminhamento.Value) ? Int32.Parse(hdfCodEncaminhamento.Value) : 0;
                registro.DescricaoEncaminhamento = txtEncaminhamento.Text;
                registro.PrazoAtendimento = DateTime.Parse(txtPrazoEncaminhamento.Text);

                var acompanhamento = new DTOAcompanhamento();
                acompanhamento.CodigoSeqAcompanhamento = CodigoSeqAcompanhamento;
                registro.Acompanhamento = acompanhamento;

                var unidade = new DTOCorporativoUnidade();
                unidade.CodigoUnidade = drpArea.SelectedValue.ToInt32();
                registro.CorporativoUnidade = unidade;

                if (!String.IsNullOrEmpty(drpMeta.SelectedValue))
                {
                    var meta = new DTOMeta();
                    meta.CodSeqMeta = drpMeta.SelectedValue.ToInt32();
                    registro.Meta = meta;
                }

                registro.StatusEncaminhamento = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterStatusEncaminhamento(Common.EnumStatusEncaminhamento.Pendente);

                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarEncaminhamento(registro);

                MessageBox.ShowInformationMessage("Registro salvo com sucesso.");

                hdfCodEncaminhamento.Value =
                txtEncaminhamento.Text =
                drpArea.SelectedValue =
                drpMeta.SelectedValue =
                txtPrazoEncaminhamento.Text = "";
                CarregarEncaminhamentos(ucPaginatorConsulta.PageIndex - 1);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                if (CodigoSeqAcompanhamento > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterAcompanhamento(CodigoSeqAcompanhamento);

                    txtDataAcompanhamento.Text = registro.DataAcompanhamento.ToShortDateString();

                    if (registro.Exercicio != null)
                        txtAnoExercicio.Text = registro.Exercicio.Ano.ToString();

                    if (registro.TipoAcompanhamento != null)
                        txtTipoAcompanhamento.Text = registro.TipoAcompanhamento.DescricaoTipoAcompanhamento;

                    if (registro.Anexo != null)
                    {
                        lblAnexo.Text = registro.Anexo.DescricaoAnexo;
                        lnkBaixarAnexo.Attributes["CaminhoAnexo"] = registro.Anexo.CaminhoAnexo;
                    }
                    else
                    {
                        lnkBaixarAnexo.Text = "Sem Anexo";
                        lnkBaixarAnexo.Enabled = false;
                    }

                    CarregarEncaminhamentos(ucPaginatorConsulta.PageIndex - 1);
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarEncaminhamento(int codigoSeqEncaminhamento)
        {
            try
            {
                if (codigoSeqEncaminhamento > 0)
                {
                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterEncaminhamento(codigoSeqEncaminhamento);

                    hdfCodEncaminhamento.Value = registro.CodigoSeqEncaminhamento.ToString();
                    txtEncaminhamento.Text = registro.DescricaoEncaminhamento;
                    txtPrazoEncaminhamento.Text = registro.PrazoAtendimento.ToShortDateString();

                    if (registro.CorporativoUnidade != null && drpArea.Items.Contains(new ListItem(registro.CorporativoUnidade.DescricaoUnidade, registro.CorporativoUnidade.CodigoUnidade.ToString())))
                        drpArea.SelectedValue = registro.CorporativoUnidade.CodigoUnidade.ToString();

                    CarregarMetas();
                    if (registro.Meta != null && drpMeta.Items.Contains(new ListItem(registro.Meta.DescricaoMeta, registro.Meta.CodSeqMeta.ToString())))
                        drpMeta.SelectedValue = registro.Meta.CodSeqMeta.ToString();
                }
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void Remover(int CodigoSeqEncaminhamento)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarEncaminhamento(CodigoSeqEncaminhamento);
                MessageBox.ShowInformationMessage("Registro excluido com sucesso!");
                CarregarEncaminhamentos(ucPaginatorConsulta.PageIndex - 1);
            }
            catch
            {
                MessageBox.ShowErrorMessage("Não foi possível excluir o registro devido ao fato de ele já ter vinculação.");
            }
        }

        #endregion
    }
}