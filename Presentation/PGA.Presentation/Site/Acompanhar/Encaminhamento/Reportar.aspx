﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Reportar.aspx.cs" Inherits="PGA.Presentation.Site.Acompanhar.Encaminhamento.Reportar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Acompanhar Encaminhamento do SAFIRA</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoAcompanhamento" AssociatedControlID="txtTipoAcompanhamento" Text="Acompanhamento do SAFIRA" runat="server" />
                                    <asp:TextBox ID="txtTipoAcompanhamento" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-1 col-sm-1">
                                <div class="form-group">
                                    <asp:Label ID="lblAnoExercicio" AssociatedControlID="txtAnoExercicio" Text="Exercício" runat="server" />
                                    <asp:TextBox ID="txtAnoExercicio" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblDataAcompanhamento" AssociatedControlID="txtDataAcompanhamento" Text="Data do Acompanhamento" runat="server" />
                                    <asp:TextBox ID="txtDataAcompanhamento" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label AssociatedControlID="lnkBaixarAnexo" Text="Anexo" runat="server" />
                                </div>
                                <div style="margin-top:-10px">
                                    <asp:LinkButton ID="lnkBaixarAnexo" ToolTip="Realizar o download do arquivo" runat="server" CaminhoAnexo="">
                                        <asp:Label ID="lblAnexo" runat="server" />   <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <br />
                        <asp:UpdatePanel ID="upPnlEncaminhamento" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group">
                                            <asp:Label ID="lblArea" AssociatedControlID="txtArea" Text="Área" runat="server" />
                                            <asp:TextBox ID="txtArea" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6">
                                        <div class="form-group">
                                            <asp:Label ID="lblMeta" AssociatedControlID="txtMeta" Text="Meta" runat="server" />
                                            <asp:TextBox ID="txtMeta" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6 col-sm-6">
                                        <div class="form-group">
                                            <asp:Label ID="lblEncaminhamento" AssociatedControlID="txtEncaminhamento" Text="Encaminhamento" runat="server" />
                                            <asp:TextBox ID="txtEncaminhamento" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group">
                                            <asp:Label ID="lblPrazoEncaminhamento" AssociatedControlID="txtPrazoEncaminhamento" Text="Prazo de Atendimento" runat="server" />
                                            <asp:TextBox ID="txtPrazoEncaminhamento" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group">
                                            <asp:Label ID="lblStatusEncaminhamento" AssociatedControlID="drpStatusEncaminhamento" Text="Status" runat="server" />
                                            <asp:DropDownList ID="drpStatusEncaminhamento" runat="server" CssClass="form-control" Enabled="false">
                                                <asp:ListItem Value="1" Text="Pendente" Selected="True" />
                                                <asp:ListItem Value="2" Text="Concluído" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:HiddenField ID="hdfCodReporteEncaminhamento" runat="server" />
                                            <asp:Label ID="lblComentario" AssociatedControlID="txtComentario" Text="Comentário" runat="server" />
                                            <asp:TextBox ID="txtComentario" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row pull-right">
                                    <div class="col-xs-12">
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                        <asp:LinkButton ID="btnSalvar" runat="server" SkinID="btnSalvar" CausesValidation="false" />
                                    </div>
                                </div>
                        
                                <br />
                                <asp:UpdatePanel ID="upPnlResultadoConsulta" runat="server">
                                    <ContentTemplate>
                                        <div class="area-table col-xs-12">
                                            <asp:Label Text="Comentários" runat="server" CssClass="h4" />
                                            <asp:GridView ID="grdReportes" runat="server" AutoGenerateColumns="False" EmptyDataText="Nenhum registro encontrado." OnRowCommand="grdReportes_RowCommand" Style="margin-top:10px">
                                                <EmptyDataTemplate>Nenhum registro encontrado.</EmptyDataTemplate>
                                                <AlternatingRowStyle CssClass="alternate" />
                                                <Columns>
                                                    <asp:BoundField HeaderText="Data do Reporte" DataField="DataReporte" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="7%" />
                                                    <asp:BoundField HeaderText="Comentário" DataField="DescricaoReporteEncaminhamento" />
                                                    <asp:BoundField HeaderText="Responsável" DataField="Usuario" ItemStyle-Width="10%" />
                                                    <asp:TemplateField HeaderText="Ações" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDetalhar" runat="server" CommandArgument='<%# Eval("CodigoSeqReporteEncaminhamento") %>'
                                                                CommandName="Detalhar" CausesValidation="false" CssClass="glyphicon glyphicon-pencil" />
                                                            <asp:LinkButton ID="lnkExcluir" runat="server" CommandArgument='<%# Eval("CodigoSeqReporteEncaminhamento") %>'
                                                                CommandName="Excluir" CausesValidation="false" CssClass="glyphicon glyphicon-remove" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div class="paginator">
                                            <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
