﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Detalhar.aspx.cs" Inherits="PGA.Presentation.Site.Acompanhar.Encaminhamento.Detalhar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cadastro Encaminhamento do SAFIRA</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6">
                                <div class="form-group">
                                    <asp:Label ID="lblTipoAcompanhamento" AssociatedControlID="txtTipoAcompanhamento" Text="Acompanhamento do SAFIRA" runat="server" />
                                    <asp:TextBox ID="txtTipoAcompanhamento" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-1 col-sm-1">
                                <div class="form-group">
                                    <asp:Label ID="lblAnoExercicio" AssociatedControlID="txtAnoExercicio" Text="Exercício" runat="server" />
                                    <asp:TextBox ID="txtAnoExercicio" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label ID="lblDataAcompanhamento" AssociatedControlID="txtDataAcompanhamento" Text="Data do Acompanhamento" runat="server" />
                                    <asp:TextBox ID="txtDataAcompanhamento" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2 col-sm-2">
                                <div class="form-group">
                                    <asp:Label AssociatedControlID="lnkBaixarAnexo" Text="Anexo" runat="server" />
                                </div>
                                <div style="margin-top:-10px">
                                    <asp:LinkButton ID="lnkBaixarAnexo" ToolTip="Realizar o download do arquivo" runat="server" CaminhoAnexo="">
                                        <asp:Label ID="lblAnexo" runat="server" />   <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        
                        <br />
                        <asp:UpdatePanel ID="upPnlEncaminhamento" runat="server">
                            <ContentTemplate>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">Encaminhamento do Acompanhamento</h5>
                                        <asp:HiddenField ID="hdfCodEncaminhamento" runat="server" />
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-2 col-sm-2">
                                                <div class="form-group">
                                                    <asp:Label ID="lblArea" AssociatedControlID="drpArea" Text="Área *" runat="server" />
                                                    <asp:DropDownList ID="drpArea" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-xs-8 col-sm-8">
                                                <div class="form-group">
                                                    <asp:Label ID="lblMeta" AssociatedControlID="drpMeta" Text="Meta" runat="server" />
                                                    <asp:DropDownList ID="drpMeta" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8 col-sm-8">
                                                <div class="form-group">
                                                    <asp:Label ID="lblEncaminhamento" AssociatedControlID="txtEncaminhamento" Text="Encaminhamento *" runat="server" />
                                                    <asp:TextBox ID="txtEncaminhamento" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-xs-2 col-sm-2">
                                                <div class="form-group">
                                                    <asp:Label ID="lblPrazoEncaminhamento" AssociatedControlID="txtPrazoEncaminhamento" Text="Prazo de Atendimento *" runat="server" />
                                                    <asp:TextBox ID="txtPrazoEncaminhamento" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row pull-right">
                                            <div class="col-xs-12">
                                                <asp:LinkButton ID="btnAdicionar" runat="server" SkinID="btnAdicionar" CausesValidation="false" /> 
                                            </div>
                                        </div>
                                
                                        <br /><br /><br />
                                        <asp:UpdatePanel ID="upPnlResultadoConsulta" runat="server">
                                            <ContentTemplate>
                                                <div class="area-table" style="width:100%;">
                                                    <asp:Label Text="Encaminhamentos" runat="server" CssClass="h4" />
                                                    <asp:Repeater ID="rptEncaminhamento" runat="server">
                                                        <HeaderTemplate>
                                                            <table Style="margin-top:10px">
                                                                <tr>
                                                                    <th>Encaminhamento</th>
                                                                    <th>Meta</th>
                                                                    <th>Prazo Atendimento</th>
                                                                    <th>Ações</th>
                                                                </tr>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="4" style="background-color: #C5D8CC !important;">
                                                                    <asp:Label ID="lblNomeUnidade" runat="server" Text='<%#Eval("Key") %>' />
                                                                </td>
                                                            </tr>
                                                            <asp:Repeater ID="rptEncaminhamentoPorUnidade"  runat="server" OnItemCommand="rptEncaminhamentoPorUnidade_ItemCommand" DataSource='<%#Eval("Value") %>'>
                                                                <ItemTemplate>
                                                                    <tr class="<%# (Container.ItemIndex % 2 == 0 ? "" : "alternate") %>">
                                                                        <td>
                                                                            <div style="margin-left:13px;">
                                                                                <asp:Label ID="lblEncaminhamento" runat="server" Text='<%#Eval("DescricaoEncaminhamento")%>' />
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblMeta" runat="server" Text='<%#Eval("Meta.DescricaoMeta")%>' />
                                                                        </td>
                                                                        <td style="width: 150px;">
                                                                            <asp:Label ID="lblPrazoAtendimento" runat="server" Text='<%#Eval("PrazoAtendimento", "{0:dd/MM/yyyy}")%>' />
                                                                        </td>
                                                                        <td style="width: 60px">
                                                                            <asp:LinkButton ID="lnkDetalhar" runat="server" Target="_self" CommandArgument='<%# Eval("CodigoSeqEncaminhamento")%>'
                                                                                CommandName="Detalhar" CausesValidation="false" CssClass="glyphicon glyphicon-pencil" />
                                                                            <asp:LinkButton ID="lnkExcluir" runat="server" CommandArgument='<%# Eval("CodigoSeqEncaminhamento") %>'
                                                                                CommandName="Excluir" CausesValidation="false" CssClass="glyphicon glyphicon-remove" />
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <tr>
                                                                <td colspan="4"><asp:Label ID="lblEmptyData" runat="server"  Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." /></td>
                                                            </tr>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </div>
                                                <div class="paginator">
                                                    <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </asp:Panel>
    <script type="text/javascript">
        $(document).ready(function () {
            FuncaoTela();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            FuncaoTela();
        });

        function FuncaoTela() {
            $("#txtPrazoEncaminhamento").datepicker();
            $("#txtPrazoEncaminhamento").mask("99/99/9999");
        };
    </script>
</asp:Content>
