﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" CodeBehind="Visualizar.aspx.cs" Inherits="PGA.Presentation.Site.Acompanhar.Encaminhamento.Visualizar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Consulta/Validação</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>Acompanhamento do SAFIRA</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-1 col-sm-1">
                                        <div class="form-group">
                                            <asp:Label Text="EXERCÍCIO: " runat="server" />
                                            <asp:Label ID="lblAnoExercicio" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="ACOMPANHAMENTO: " runat="server" />
                                            <asp:Label ID="lblTipoAcompanhamento" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group">
                                            <asp:Label Text="DATA DO ACOMPANHAMENTO: " runat="server" />
                                            <asp:Label ID="lblDataAcompanhamento" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-1 col-sm-1">
                                        <div class="form-group">
                                            <asp:Label Text="STATUS: " runat="server" />
                                            <asp:Label ID="lblStatusAcompanhamento" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6">
                                        <div>
                                            <asp:Label Text="ANEXO: " runat="server" />
                                            <asp:LinkButton ID="lnkBaixarAnexo" ToolTip="Realizar o download do arquivo" runat="server" CaminhoAnexo="">
                                                <asp:Label ID="lblAnexo" runat="server" />   <i aria-hidden="true" class="glyphicon glyphicon-download-alt"></i>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><strong>Encaminhamento do SAFIRA</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group">
                                            <asp:Label Text="ÁREA: " runat="server" />
                                            <asp:Label ID="lblArea" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="META: " runat="server" />
                                            <asp:Label ID="lblMeta" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="ENCAMINHAMENTO: " runat="server" />
                                            <asp:Label ID="lblEncaminhamento" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="form-group">
                                            <asp:Label Text="PRAZO DE ATENDIMENTO: " runat="server" />
                                            <asp:Label ID="lblPrazoEncaminhamento" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <asp:Label Text="STATUS: " runat="server" />
                                            <asp:Label ID="lblStatusEncaminhamento" runat="server" />
                                        </div>
                                    </div>
                                </div>

                                <br />
                                <div class="area-table col-xs-12">
                                    <asp:Label ID="lblComentarios" Text="Comentários" runat="server" CssClass="h4" />
                                    <asp:GridView ID="grdReportes" runat="server" AutoGenerateColumns="False" EmptyDataText="Nenhum registro encontrado." Style="margin-top:10px">
                                        <EmptyDataTemplate>Nenhum registro encontrado.</EmptyDataTemplate>
                                        <AlternatingRowStyle CssClass="alternate" />
                                        <Columns>
                                            <asp:BoundField HeaderText="Data do Reporte" DataField="DataReporte" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-Width="7%" />
                                            <asp:BoundField HeaderText="Comentário" DataField="DescricaoReporteEncaminhamento" />
                                            <asp:BoundField HeaderText="Responsável" DataField="Usuario" ItemStyle-Width="10%" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="row pull-right">
                            <div class="col-xs-12">
                                <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
