﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SCA.WebControls;
using PGA.Presentation.Util;
using SQFramework.Web;

namespace PGA.Presentation.Site
{
    public partial class PortalSistemas : CustomPageBase
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(PortalSistemas_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Voltar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                    {
                        WebHelper.Redirect("~/Site/Login/Login.aspx");
                    }
                    break;
            }
        }

        protected void PortalSistemas_Load(object sender, EventArgs e)
        {
            MessageBox.ShowInformationMessage("Usuário não possui acesso ao SAFIRA.", "Voltar");
        }
    }
}