﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="false"
    CodeBehind="PortalSistemas.aspx.cs" Inherits="PGA.Presentation.Site.PortalSistemas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Corpo" runat="server">
    <div class="container geral">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Acesso Negado</h3>
                </div>
                <div class="panel-body">
                    <asp:Literal ID="litMensagem" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
