﻿using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using SCA.Dominio.DataTransferObject;
using SCA.Servicos.Spec.Servicos;
using SCA.WebControls;
using SCL.Servicos.Spec.Servicos;
using SQFramework.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PGA.Presentation.Site.Login
{
    public partial class Login1 : CustomPageBase
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.btnEntrar.Click += btnEntrar_Click;

            if(Request.QueryString["Usr"] != null && Request.QueryString["Usr"] == "logado")
            {
                if (ValidarUsuario("usuario.sistemico"))
                {
                    if (this.Autenticar("usuario.sistemico", "Antt123@#"))
                    {
                        SQFramework.Web.WebHelper.Redirect("~/Default.aspx");
                    }
                }
            }
        }

        protected void btnEntrar_Click(object sender, EventArgs e)
        {
            this.Validate();
            if (!IsValid)
                return;

            if (ValidarUsuario(txtCpf.Text))
            {
                if (this.Autenticar(txtCpf.Text, txtSenha.Text))
                {
                    SQFramework.Web.WebHelper.Redirect("~/Default.aspx");
                }
            }
        }

        private bool Autenticar(string identificador, string senha)
        {
            try
            {
                var scaServiceV2Usuarios = GetService<ISCAServiceV2Usuarios>(Common.Constants.SCAServiceV2Usuarios);

                DTOInformacoesUsuario usuarioContract = null;

                this.TryRemoteOperation(() => usuarioContract = scaServiceV2Usuarios.Autenticar(identificador, senha, this.ObtemObjetoAuditoriaSLA(identificador)));

                if (usuarioContract != null)
                {
                    Utilitarios.Web.WebHelper.Autenticar(usuarioContract.Usuario, true);

                    var sgrh = GetService<ISGRHService>("antt.servicos/IntegracaoSGRH");
                    var user = sgrh.ObterEmpregadoPorIdentificador(identificador);
                    //var user = sgrh.ObterEmpregadoPorCodigo(usuario.Codigo);

                    if (user == null)
                    {
                        var colaborador = sgrh.ObterColaboradores(user.Codigo.ToString().PadLeft(11, '0'), null, null, null);
                        var usu = sgrh.ObterLotacaoDoEmpregado((int)user.Codigo);

                        usuarioContract.Usuario.CodigoSuperintendencia = Convert.ToInt32(usu.CodigoUnidadeOrganizacional);
                    }
                    else
                    {
                        usuarioContract.Usuario.CodigoSuperintendencia = Convert.ToInt32(user.CodigoUnidade);
                    }

                    if (usuarioContract.Usuario != null)
                    {
                        SCAApplicationContext.Usuario = usuarioContract.Usuario;

                        if (usuarioContract.Permissoes != null)
                            SCAApplicationContext.Permissoes = usuarioContract.Permissoes;

                        SCAApplicationContext.AdicionarUsuarioLogado();
                    }
                    else
                    {
                        WebHelper.Logoff();
                        return false;
                    }
                }
                else
                {
                    MessageBox.ShowErrorMessage("Usuário ou senha inválidos.");
                    return false;
                }
            }
            catch
            {
                MessageBox.ShowErrorMessage("Não foi possível autenticar o usuário.");
                return false;
            }

            return true;
        }

        private bool ValidarUsuario(string identificador)
        {
            var valido = true;
            var erros = new List<string>();

            var scaService = GetService<ISCAService>(Common.Constants.SCAService);
            var scaServiceV2Usuarios = GetService<ISCAServiceV2Usuarios>(Common.Constants.SCAServiceV2Usuarios);
            DTOUsuario usuario = null;

            try
            {
                usuario = scaService.ObterUsuarioPorIdentificador(identificador);
            }
            catch
            {
                erros.Add("O sistema de autenticação está off-line. Favor tentar acesso mais tarde.");
                valido = false;
            }

            try
            {
                if (!ValidarUnidadesDasMetas())
                {
                    erros.Add("Existem metas cadastradas no exercício corrente ligadas a uma unidade inválida. Contate o gestor do sistema para corrigir este problema.");
                    valido = false;
                }
            }
            catch(Exception)
            {
                erros.Add("O sistema de autenticação está off-line. Favor tentar acesso mais tarde.");
                valido = false;
            }

            if (valido)
            {
                if (usuario == null)
                {
                    erros.Add("Usuário não encontrado.");
                    valido = false;
                }
                else
                {
                    var usuarioSenha = scaService.ObterUsuario(usuario.Codigo);

                    if (usuarioSenha == null)
                    {
                        erros.Add("Usuário não encontrado.");
                        valido = false;
                    }
                }
            }

            if (!valido)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var erro in erros)
                {
                    sb.AppendLine(erro.ToString());
                }

                MessageBox.ShowInformationMessage(sb.ToString());
            }

            return valido;
        }

        private bool ValidarUnidadesDasMetas()
        {
            var unidadesAtivas = GetService<ISGRHService>("antt.servicos/IntegracaoSGRH")
                .ListarSubUnidadesAtivasPorUnidadeAdministrativa(10000)
                .Where(w => w.TipoUnidade == SCL.Comum.TipoUnidadeAdministrativa.UnidadeOrganizacional)
                .Select(i => new DTOCorporativoUnidade(){CodigoUnidade = i.Codigo, CodigoUnidadePai = i.CodigoUnidadeSuperior, DescricaoUnidade = i.Sigla})
                .ToList();

            this.ObterPGAService().AtualizarUnidadesAtivas(unidadesAtivas);
            return this.ObterPGAService().ValidarUnidadesAtivasDasMetas(unidadesAtivas);
        }
    }
}