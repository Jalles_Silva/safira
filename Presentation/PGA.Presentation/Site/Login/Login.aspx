﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="PGA.Presentation.Site.Login.Login1" %>

<!DOCTYPE html>
<%@ Register Src="~/UserControls/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc" %>
<%@ Register Src="~/UserControls/Progress.ascx" TagName="Progress" TagPrefix="uc" %>

<html class="loginHtml" xmlns="http://www.w3.org/1999/xhtml">
<head id="head" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="Wings4Cloud - Squadra Tecnologia" />
    <link rel="stylesheet" href="~/Scripts/jquery-ui/jquery-ui.min.css" />
    <link rel="stylesheet" href="~/Scripts/jquery-chosen/chosen.min.css" />
    <link rel="shortcut icon" type="image/x-icon" href="~/favicon.ico" />
    <title></title>
</head>
<body class="loginBody">
    <section id="sistemasHeader">
        <div class="container-fluid content">
            <div class="rowHeader">
                <div class="container-fluid">
                    <div class="col-sm-6 margin-top-10">
                        <h1 class="col-xs-12 no-margin no-padding pull-left margin-top-10 tituloLogin bold">SAFIRA</h1>
                        <h2 class="col-xs-12 no-margin no-padding pull-left tituloLogin">Sistema de Gestão Estratégica e Anual</h2>
                    </div>
                    <div class="col-sm-6 margin-top-10">
                         <h1 class="col-xs-12 no-margin no-padding pull-right">
                             <a href="#" title="logoSutec">
                                <img 
                                    class="pull-right" 
                                    src="../../App_Themes/Default/images/LogoSUTEC_Branco.png" 
                                    alt="LOGO SUTEC"
                                    style="max-height:80px" />
                             </a>
                         </h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form id="form" class="form-horizontal validateForm submeter" role="form" runat="server">
        <asp:ScriptManager ID="ScriptManagerMain" runat="server" EnablePageMethods="true"
            ScriptMode="Release" AsyncPostBackTimeout="300" EnablePartialRendering="true"
            EnableScriptGlobalization="true" EnableScriptLocalization="true">
            <Scripts>
                <asp:ScriptReference Path="~/Scripts/jquery-1.11.2.min.js" />
                <asp:ScriptReference Path="~/Scripts/jquery-ui/jquery-ui.min.js" />
                <asp:ScriptReference Path="~/Scripts/jquery-ui/datepicker-pt-BR.js" />
                <asp:ScriptReference Path="~/Scripts/bootstrap.min.js" />
                <asp:ScriptReference Path="~/Scripts/jquery.mask.js" />
                <asp:ScriptReference Path="~/Scripts/updateProgress.js" />
                <asp:ScriptReference Path="~/Scripts/util.js" />
                <asp:ScriptReference Path="~/Scripts/custom.js" />
                <asp:ScriptReference Path="~/Scripts/custom.min.js" />
                <asp:ScriptReference Path="~/Scripts/jquery-chosen/chosen.jquery.min.js" />
            </Scripts>
        </asp:ScriptManager>
        <div class="content-login col-sm-5 no-padding">
            <span class="login">
                <img src="../../App_Themes/Default/images/logo.png" alt="login" /></span>
            <div>
                <div class="form-group ico-content no-margin-left no-margin-right">
                    <span class="glyphicon glyphicon-user"></span>
                    <label class="hide" for="usuario">Digite o usuário</label>
                    <%--<input id="usuario" name="usuario" class="form-control" required="required" type="text" placeholder="Usuário" />--%>
                    <asp:TextBox ID="txtCpf" CssClass="form-control" runat="server" Width="100%" MaxLength="44" />
                    <asp:RequiredFieldValidator ID="rfvCpf" runat="server" Display="None" ControlToValidate="txtCpf" ErrorMessage="Favor informar o usuário." Text="*" ValidationGroup="Entrar" />
                </div>
                <div class="form-group ico-content no-margin-left no-margin-right">
                    <span class="glyphicon glyphicon-lock"></span>
                    <label class="hide" for="senha">Digite a senha</label>
                    <%--<input id="senha" name="senha" type="password" required="required" class="form-control" placeholder="Senha" />--%>
                    <asp:TextBox CssClass="form-control" ID="txtSenha" runat="server" Width="100%" TextMode="Password" MaxLength="44" />
                    <asp:RequiredFieldValidator ID="rfvSenha" runat="server" Display="None" ControlToValidate="txtSenha" ErrorMessage="Favor informar a senha." Text="*" ValidationGroup="Entrar" />
                </div>
                <div class="form-group no-margin-left no-margin-right">
                    <div class="col-sm col-sm-12 no-padding">
                        <%-- <input type="submit" class="btn btn-block btn-primary logar" value="Logar" />--%>
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="updateBtnEntrar">
                            <ContentTemplate>
                                <asp:LinkButton ID="btnEntrar" runat="server" CssClass="btn btn-block btn-primary logar" Text=" Acessar" CausesValidation="false" ValidationGroup="Entrar" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnEntrar" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <%-- <div class="form-group recuperar-senha margin-top-10 no-margin-left no-margin-right no-margin-bottom no-padding-bottom">
                  <a href="#" class="pull-left">Solicitar Senha</a> 
                  <a href="#" data-toggle="modal" data-target=".alterarSenha" class="pull-right">Alterar Senha</a>
              </div>--%>
            </div>
        </div>

        <input type="hidden" id="validateLogin" value="true" />

        <asp:UpdatePanel ID="UpdatePanelMain" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <uc:MessageBox ID="MessageBox" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <uc:Progress ID="Progress" runat="server" />
    </form>

    <footer class="loginFooter container-fluid">
        <div class="content center-block">
            <p class="loginFooterP pull-right">Versão 2.0.8</p>
        </div>
    </footer>

</body>
</html>
