﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site/MasterPage/Site.Master" AutoEventWireup="true" 
    CodeBehind="Detalhar.aspx.cs" Inherits="PGA.Presentation.Site.Processos.Detalhar" %>

<asp:Content ID="Content" ContentPlaceHolderID="Corpo" runat="server">
    <asp:Panel ID="Panel" runat="server" DefaultButton="btnSalvar">
        <div class="container geral">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cadastro de Processos</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <asp:Label ID="lblNomeDoProcesso" AssociatedControlID="txtNomeDoProcesso" Text="Nome do Processo" runat="server" />
                                            <asp:TextBox ID="txtNomeDoProcesso" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <asp:Label ID="lblArea" AssociatedControlID="drpArea" Text="Área" runat="server" />
                                            <asp:DropDownList ID="drpArea" runat="server" CssClass="form-control"></asp:DropDownList>                                    
                                        </div>
                                    </div>
                                </div>
                                <div class="row">                            
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <asp:Label ID="lblMacroprocesso" AssociatedControlID="drpMacroprocesso" Text="Macroprocesso" runat="server" />
                                            <asp:DropDownList ID="drpMacroprocesso" runat="server" CssClass="form-control"></asp:DropDownList>                                    
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <asp:Label ID="lblProcessoSei" AssociatedControlID="txtProcessoSei" Text="Processo Sei" runat="server" />
                                            <asp:TextBox ID="txtProcessoSei" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <asp:Label ID="lblSituacao" AssociatedControlID="rblSituacao" Text="Situação" runat="server" />
                                            <asp:RadioButtonList ID="rblSituacao" runat="server" RepeatDirection="Horizontal" CssClass="radioListHoriz">                                        
                                                <asp:ListItem Value="1" Text="Ativo"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Inativo"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                    <%--<div class="col-xs-1 col-sm-3">
                                        <div class="form-group">
                                            <asp:Label ID="lblMotivoInativo" AssociatedControlID="txtMotivoInativo" Text="Motivo Inativo" runat="server" />
                                            <asp:TextBox ID="txtMotivoInativo" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>--%>
                                    <div class="col-xs-2 col-sm-3">
                                        <div class="form-group">
                                            <asp:Label ID="lblProcessoPriorizadoParaGr" AssociatedControlID="rblProcessoPriorizadoParaGr" Text="Processo priorizado para GR" runat="server" />
                                            <asp:RadioButtonList ID="rblProcessoPriorizadoParaGr" runat="server" RepeatDirection="Horizontal" CssClass="radioListHoriz">                                        
                                                <asp:ListItem Value="1" Text="Sim"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Não"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                </div>
                                <%--processo--%>
                            </div>
                        </div>
                        <%--Anexos--%>                        
                        <%--<div class="panel panel-cadastro">
                            <div class="panel-heading">
                                <h3 class="panel-title">Anexos</h3>
                            </div>
                                <div class="row">               
                                    <div class="col-lg-12">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <asp:Label ID="Label2" AssociatedControlID="txtNomeAnexo" Text="Nome Anexo" runat="server" />
                                                <asp:TextBox ID="txtNomeAnexo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <asp:Label ID="Label1" AssociatedControlID="drpTipoAnexo" Text="Tipo Anexo" runat="server" />
                                                <asp:DropDownList ID="drpTipoAnexo" runat="server" CssClass="form-control"></asp:DropDownList>                                    
                                            </div>
                                        </div> 
                                        <br />
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <asp:UpdatePanel ID="fileUpload_PanelAnexo" runat="server">
                                                    <ContentTemplate>
                                                        
                                                        <asp:LinkButton ID="btnUpload" runat="server" SkinID="btnUpload" CausesValidation="false" ValidationGroup="Upload" />                                        
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                       
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <asp:UpdatePanel ID="updBotao" runat="server">
                                                    <ContentTemplate>
                                                        
                                                        <asp:LinkButton ID="btnAdicionar" runat="server" SkinID="btnAdicionar" CausesValidation="false" ValidationGroup="Adicionar" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>

                                    </div>                            
                                </div>
                                <div class="row">
                                <%--Tabela--%>
                                    <%--<div class="col-lg-12">                                        
                                            <ContentTemplate>
                                                <div class="area-table">                                                    
                                                        
                                                            <table>
                                                                <tr>
                                                                    <th>Anexo</th>
                                                                    <th>Tipo Anexo</th>
                                                                    <th>Data</th>
                                                                    <th>Excluir</th>
                                                                </tr>
                                                        
                                                                <tr>
                                                                    <td colspan="1" style="background-color: #C5D8CC !important; width: 50px;">Anexo 1</td>
                                                                    <td colspan="1" style="background-color: #C5D8CC !important; width: 50px;">Anexo teste</td>
                                                                    <td colspan="1" style="background-color: #C5D8CC !important; width: 50px;">12/01/2021</td>
                                                                    <td colspan="1" style="background-color: #C5D8CC !important; width: 50px;">X</td>
                                                                </tr>                                                        
                                                            </table>
                                                </div>
                                            </ContentTemplate>                                                                                
                                    </div>--%>
                                <%--Fim Tabela--%>
                              <%--  </div>
                        </div>--%>
                        <%--Fim Anexos--%>

                        <%--Tabela Anexos--%>
                         <br />
                        <asp:UpdatePanel ID="upPnlAnexo" runat="server">
                            <ContentTemplate>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">Anexar Processos</h5>
                                        <asp:HiddenField ID="hdfCodAnexo" runat="server" />
                                    </div>
                                    <div class="panel-body">                                       
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" AssociatedControlID="txtNomeAnexo" Text="Nome Anexo" runat="server" />
                                                        <asp:TextBox ID="txtNomeAnexo" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <asp:Label ID="Label1" AssociatedControlID="drpTipoAnexo" Text="Tipo Anexo" runat="server" />
                                                    <asp:DropDownList ID="drpTipoAnexo" runat="server" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>

                                                 <div class="row pull-right">
                                                    <div class="col-lg-offset-1 col-lg-1">
                                                        <asp:LinkButton ID="btnAdicionar" runat="server" SkinID="btnAdicionar" CausesValidation="false" /> 
                                                    </div>
                                                </div>

                                                <%--<div class="row pull-right">
                                                    <div class="col-lg-1">
                                                        <asp:LinkButton ID="btnUpload" runat="server" SkinID="btnUpload" CausesValidation="false" ValidationGroup="Upload" />
                                                    </div>
                                                </div>--%>

                                                 <div class="row">
                                                    <div class="col-xs-2 col-sm-2">
                                                        <div class="form-group">
                                                            <asp:UpdatePanel ID="updPanel" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:Label Text="Anexo" runat="server"  />
                                                                    <asp:FileUpload ID="flpAnexo" runat="server" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>                                                       
                                                    </div>
                                                </div>

                                               
                                            </div>
                                        </div>
                                        <br /><br /><br />
                                        <asp:UpdatePanel ID="upPnlResultadoConsulta" runat="server">
                                            <ContentTemplate>
                                                <div class="area-table" style="width:100%;">
                                                    <asp:Label Text="Processos Anexos " runat="server" CssClass="h4" />
                                                    <asp:Repeater ID="rptEncaminhamento" runat="server">
                                                        <HeaderTemplate>
                                                            <table Style="margin-top:10px">
                                                                <tr>
                                                                    <th>Anexo</th>
                                                                    <th>Tipo Anexo</th>
                                                                    <th>Data</th>
                                                                    <th>Excluir</th>
                                                                </tr>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td colspan="4" style="background-color: #C5D8CC !important;">
                                                                    <asp:Label ID="lblDescricaoAnexo" runat="server" Text='<%#Eval("Key") %>' />
                                                                </td>
                                                            </tr>
                                                            <asp:Repeater ID="rptAnexoProcesso"  runat="server" OnItemCommand="rptProcessoAnexo_ItemCommand" DataSource='<%#Eval("Value") %>'>
                                                                <ItemTemplate>
                                                                    <tr class="<%# (Container.ItemIndex % 2 == 0 ? "" : "alternate") %>">
                                                                        <td>
                                                                            <div style="margin-left:13px;">
                                                                                <asp:Label ID="lbl" runat="server" Text='<%#Eval("Anexo.DescricaoAnexo")%>' />
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblTipoAnexo" runat="server" Text='<%#Eval("Anexo.DescricaoAnexo")%>' />
                                                                        </td>
                                                                        <td style="width: 150px;">
                                                                            <asp:Label ID="lblDataInclusao" runat="server" Text='<%#Eval("Anexo.dataInclusao", "{0:dd/MM/yyyy}")%>' />
                                                                        </td>
                                                                        <td style="width: 60px">
                                                                            <asp:LinkButton ID="lnkDetalhar" runat="server" Target="_self" CommandArgument='<%# Eval("Cd_Seq_Processo_Anexo")%>'
                                                                                CommandName="Detalhar" CausesValidation="false" CssClass="glyphicon glyphicon-pencil" />
                                                                            <asp:LinkButton ID="lnkExcluir" runat="server" CommandArgument='<%# Eval("Anexo.CodigoSeqAnexo") %>'
                                                                                CommandName="Excluir" CausesValidation="false" CssClass="glyphicon glyphicon-remove" />
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <tr>
                                                                <td colspan="4"><asp:Label ID="lblEmptyData" runat="server"  Visible='<%# ((Repeater)Container.NamingContainer).Items.Count == 0 %>' Text="Nenhum registro encontrado." /></td>
                                                            </tr>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </div>
                                                <div class="paginator">
                                                    <uc:Paginator ID="ucPaginatorConsulta" runat="server" Visible="false" PageSize="10" />
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <%--Tabela Anexos--%>
                    

                        <div class="row">
                            <div class="col-xs-12">
                                <asp:UpdatePanel ID="updBotoes" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnSalvar" runat="server" SkinID="btnSalvar" CausesValidation="false" ValidationGroup="Salvar" />
                                        <asp:LinkButton ID="btnVoltar" runat="server" SkinID="btnVoltar" CausesValidation="false" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">
        !function ($) {

            "use strict"; // jshint ;_

            /* FILEUPLOAD PUBLIC CLASS DEFINITION
             * ================================= */

            var Fileupload = function (element, options) {
                this.$element = $(element)
                this.type = this.$element.data('uploadtype') || (this.$element.find('.thumbnail').length > 0 ? "image" : "file")

                this.$input = this.$element.find(':file')
                if (this.$input.length === 0) return

                this.name = this.$input.attr('name') || options.name

                this.$hidden = this.$element.find('input[type=hidden][name="' + this.name + '"]')
                if (this.$hidden.length === 0) {
                    this.$hidden = $('<input type="hidden" />')
                    this.$element.prepend(this.$hidden)
                }

                this.$preview = this.$element.find('.fileupload-preview')
                var height = this.$preview.css('height')
                if (this.$preview.css('display') != 'inline' && height != '0px' && height != 'none') this.$preview.css('line-height', height)

                this.original = {
                    'exists': this.$element.hasClass('fileupload-exists'),
                    'preview': this.$preview.html(),
                    'hiddenVal': this.$hidden.val()
                }

                this.$remove = this.$element.find('[data-dismiss="fileupload"]')

                this.$element.find('[data-trigger="fileupload"]').on('click.fileupload', $.proxy(this.trigger, this))

                this.listen()
            }

            Fileupload.prototype = {

                listen: function () {
                    this.$input.on('change.fileupload', $.proxy(this.change, this))
                    $(this.$input[0].form).on('reset.fileupload', $.proxy(this.reset, this))
                    if (this.$remove) this.$remove.on('click.fileupload', $.proxy(this.clear, this))
                },

                change: function (e, invoked) {
                    var file = e.target.files !== undefined ? e.target.files[0] : (e.target.value ? { name: e.target.value.replace(/^.+\\/, '') } : null)
                    if (invoked === 'clear') return

                    if (!file) {
                        this.clear()
                        return
                    }

                    this.$hidden.val('')
                    this.$hidden.attr('name', '')
                    this.$input.attr('name', this.name)

                    if (this.type === "image" && this.$preview.length > 0 && (typeof file.type !== "undefined" ? file.type.match('image.*') : file.name.match('\\.(gif|png|jpe?g)$')) && typeof FileReader !== "undefined") {
                        var reader = new FileReader()
                        var preview = this.$preview
                        var element = this.$element

                        reader.onload = function (e) {
                            preview.html('<img src="' + e.target.result + '" ' + (preview.css('max-height') != 'none' ? 'style="max-height: ' + preview.css('max-height') + ';"' : '') + ' />')
                            element.addClass('fileupload-exists').removeClass('fileupload-new')
                        }

                        reader.readAsDataURL(file)
                    } else {
                        this.$preview.text(file.name)
                        this.$element.addClass('fileupload-exists').removeClass('fileupload-new')
                    }
                },

                clear: function (e) {
                    this.$hidden.val('')
                    this.$hidden.attr('name', this.name)
                    this.$input.attr('name', '')

                    //ie8+ doesn't support changing the value of input with type=file so clone instead
                    if ($.browser.msie) {
                        var inputClone = this.$input.clone(true);
                        this.$input.after(inputClone);
                        this.$input.remove();
                        this.$input = inputClone;
                    } else {
                        this.$input.val('')
                    }

                    this.$preview.html('')
                    this.$element.addClass('fileupload-new').removeClass('fileupload-exists')

                    if (e) {
                        this.$input.trigger('change', ['clear'])
                        e.preventDefault()
                    }
                },

                reset: function (e) {
                    this.clear()

                    this.$hidden.val(this.original.hiddenVal)
                    this.$preview.html(this.original.preview)

                    if (this.original.exists) this.$element.addClass('fileupload-exists').removeClass('fileupload-new')
                    else this.$element.addClass('fileupload-new').removeClass('fileupload-exists')
                },

                trigger: function (e) {
                    this.$input.trigger('click')
                    e.preventDefault()
                }
            }


            /* FILEUPLOAD PLUGIN DEFINITION
             * =========================== */

            $.fn.fileupload = function (options) {
                return this.each(function () {
                    var $this = $(this)
                        , data = $this.data('fileupload')
                    if (!data) $this.data('fileupload', (data = new Fileupload(this, options)))
                    if (typeof options == 'string') data[options]()
                })
            }

            $.fn.fileupload.Constructor = Fileupload


            /* FILEUPLOAD DATA-API
             * ================== */

            $(function () {
                $('body').on('click.fileupload.data-api', '[data-provides="fileupload"]', function (e) {
                    var $this = $(this)
                    if ($this.data('fileupload')) return
                    $this.fileupload($this.data())

                    var $target = $(e.target).is('[data-dismiss=fileupload],[data-trigger=fileupload]') ?
                        $(e.target) : $(e.target).parents('[data-dismiss=fileupload],[data-trigger=fileupload]').first()
                    if ($target.length > 0) {
                        $target.trigger('click.fileupload')
                        e.preventDefault()
                    }
                })
            })

        }(window.jQuery);
        
    </script>
    <%--<script type="text/javascript">
        $(document).ready(function () {
            FuncaoTela();
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            FuncaoTela();
        });


        function FuncaoTela() {
            $("#<%= txtDataReporte.ClientID %>").datepicker();
            $("#<%= txtDataReporte.ClientID %>").mask("99/99/9999").val();
            $("#<%= txtPercentualReporte.ClientID %>").mask('##9%', { reverse: true, maxlength: true });

            $("#<%= txtPercentualReporte.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text != null) {
                    if (text.replace('%', '') <= 100) {
                        $("#<%= txtPercentualReporte.ClientID %>").val();
                    } else {
                        $("#<%= txtPercentualReporte.ClientID %>").val(text.substring(0, 2) + "%");
                    }
                }
            });

            $("#<%= txtDescricaoObservacao.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoObservacao.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtDescricaoObservacao.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoObservacao.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });

            $("#<%= txtDescricaoCausa.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoCausa.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtDescricaoCausa.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoCausa.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });

            $("#<%= txtDescricaoAcao.ClientID %>").keypress(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoAcao.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });
            $("#<%= txtDescricaoAcao.ClientID %>").keyup(function () {
                var text = $(this).val();
                if (text.length > 1800) {
                    $("#<%= txtDescricaoAcao.ClientID %>").val(text.substring(0, 1800));
                    return false;
                }
                else {
                    return true;
                }
            });

            $("#<%= txtRecursoFinanceiroUtilizado.ClientID %>").mask('000.000.000.000,00', { reverse: true, maxlength: true });
        }
    </script>--%>
</asp:Content>

