﻿using PGA.Presentation.Util;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using SCA.WebControls;
using SQFramework.Core;
using SQFramework.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utilitarios.Extensions;

namespace PGA.Presentation.Site.Processos
{
    public partial class Detalhar : CustomPageBase
    {
        #region [Properties]

        private int CodSeqProcesso;


        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            btnSalvar.Click += new EventHandler(btnSalvar_Click);
            btnVoltar.Click += new EventHandler(btnVoltar_Click);
            CodSeqProcesso = 0;
            //gdvControleAcao.RowCommand += new GridViewCommandEventHandler(gdvResultadoConsulta_RowCommand);
            //gdvControleAcao.RowDataBound += gdvControleAcao_RowDataBound;
            //ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
        }

        protected void btnVoltar_Click(object sender, EventArgs e)
        {
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Voltar",
                "Deseja retornar para a página de consulta (dados não salvos serão perdidos)?", MessageBoxButtons.YesNo, "Voltar");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (rblSituacao.SelectedValue == string.Empty)
                rblSituacao.SelectedValue = "1";            
            if (rblProcessoPriorizadoParaGr.SelectedValue == string.Empty)
                rblProcessoPriorizadoParaGr.SelectedValue = "0";

            if (!IsPostBack)
            {
                CarregarControles();
                CarregarArea();
                CarregarObjtivos();
                CarregarTipoAnexo();
            }

        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch (e.Command)
            {
                case "Salvar":
                    if (e.Result == MessageBoxResult.Yes)
                        Salvar();
                    break;
                case "AdicionarAnexo":
                    if (e.Result == MessageBoxResult.Yes)
                        AdicionarAnexos();
                    break;
                case "Recarregar":
                    Recarregar();
                    break;
                case "Voltar":
                    if (e.Result == MessageBoxResult.Yes || e.Result == MessageBoxResult.OK)
                        WebHelper.Redirect(String.Format("~/Site/Processos/Consultar.aspx"));
                    break;
                case "Remover":
                    if (e.Result == MessageBoxResult.Yes)
                        Remover(e.Parameters["CodigoSeqControleAcao"].ToInt32());
                    break;
                case "Deslogar":
                    if (e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        public List<DTOAnexo> listAnexos;

        private void AdicionarAnexos()
        {
            try
            {
                if (flpAnexo.HasFile)
                {
                    listAnexos.Add(new DTOAnexo()
                    {
                        DescricaoAnexo = flpAnexo.FileName,
                        Conteudo = flpAnexo.FileBytes,
                        DataInclusao = DateTime.Now,
                        TipoAnexo = Convert.ToInt32(drpTipoAnexo.SelectedItem.Value),
                        CaminhoAnexo = System.Configuration.ConfigurationManager.AppSettings["diretorioAnexosAcompanhamento"],
                    });
                }                
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        protected void rptProcessoAnexo_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Excluir":
                    {
                        var parametros = new Dictionary<string, object>();
                        parametros.Add("CodigoSeqEncaminhamento", e.CommandArgument);

                        MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente excluir o registro?",
                            "Deseja realmente excluir o registro?", MessageBoxButtons.YesNo, "Remover", parametros);
                    }
                    break;
                case "Detalhar":
                    //CarregarTipoAnexo(e.CommandArgument.ToInt32());
                    break;
                default:
                    break;
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            CarregarControlesAcao(e.NewPage - 1, "DataReporte", false);
        }

        protected void btnSalvar_Click(object sender, EventArgs e)
        {
            PreSalvar();
        }

        protected void gdvResultadoConsulta_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Excluir":
                    PreRemove(e.CommandArgument.ToInt32());
                    break;
                case "Detalhar":
                    CarregarRegistro(e.CommandArgument.ToInt32());
                    break;
                default:
                    break;
            }
        }

        protected void gdvControleAcao_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            e.Row.Cells[5].Visible = UsuarioAlteraAcompanhamento;
        }

        #endregion

        #region [Methods]

        private void CarregarControles()
        {
            try
            {
                CarregarRegistro();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }
        
        private void CarregarArea()
        {
            drpArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService")
                .ListarCorporativoUnidadeAtivoPorExercicio(drpArea.SelectedValue.ToInt32());

            drpArea.DataTextField = "DescricaoUnidade";
            drpArea.DataValueField = "CodigoUnidade";
            drpArea.DataBind();
            drpArea.Items.Insert(0, new ListItem("Selecionar", ""));
        }

        private void CarregarObjtivos()
        {
            drpMacroprocesso.DataSource = this.ServiceLocator.GetService<IPGAService>
                ("antt.servicos/PGAService")
                .ListarObjetivos();
            
            drpMacroprocesso.DataTextField = "NomeObjetivo";
            drpMacroprocesso.DataValueField = "CodigoSeqObjetivo";
            drpMacroprocesso.DataBind();
            drpMacroprocesso.Items.Insert(0, new ListItem("Selecionar", ""));
        }

        private void CarregarTipoAnexo()
        {
            var tipoAnexo = new List<DTOTipoAnexo>();
            tipoAnexo = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTipoAnexo().ToList();

                 tipoAnexo.Add(
                new DTOTipoAnexo() 
                { 
                    Cd_Seq_Tipo_Anexo = 1,
                    Ds_Tipo_Anexo = "Teste 1"
                });


            drpTipoAnexo.DataSource =  this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarTipoAnexo();

            drpTipoAnexo.DataTextField = "Ds_Tipo_Anexo";
            drpTipoAnexo.DataValueField = "Cd_Seq_Tipo_Anexo";
            drpTipoAnexo.DataBind();
            drpTipoAnexo.Items.Insert(0, new ListItem("Selecionar", ""));
        }

        private void ValidaPermissao(string CodigoUnidade)
        {
            if (UsuarioAdministrador)
            {
                //Reporte.Visible = true;
                btnSalvar.Visible = UsuarioAlteraAcompanhamento;
            }
            else if (UsuarioCadastrador)
            {
                if (SCAApplicationContext.Usuario.CodigoSuperintendencia == 0)
                {
                    MessageBox.ShowConfirmationMessage(MessageBoxType.Error, "Erro",
                         "Não foi possível carregar a superintêndencia, favor logar novamente.", MessageBoxButtons.OK, "Deslogar");
                    return;
                }

                if (SCAApplicationContext.Usuario.CodigoSuperintendencia.ToString() == CodigoUnidade)
                {
                    //Reporte.Visible = true;
                    btnSalvar.Visible = UsuarioAlteraAcompanhamento;
                }
                else
                {
                    //Reporte.Visible = btnSalvar.Visible = false;
                }
            }
            else
            {
                //Reporte.Visible = btnSalvar.Visible = false;
            }
        }

        private void PreSalvar()
        {
            Validate();

            if (IsValid)
            {
                if (String.IsNullOrEmpty(txtNomeDoProcesso.Text))
                {
                    MessageBox.ShowInformationMessage("Por gentileza preencha os campos obrigatórios.", "Erro");
                    return;
                }

                if (String.IsNullOrEmpty(drpArea.SelectedValue))
                {
                    MessageBox.ShowInformationMessage("Favor informar Área do encaminhamento.", "Erro");
                    return;
                }

                MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Salvar",
                    "Deseja realmente salvar?", MessageBoxButtons.YesNo, "Salvar");
            }
        }

        private void Salvar()
        {
            try
            {
                string usuario = "";
                if (SCAApplicationContext.Usuario != null)
                    usuario = SCAApplicationContext.Usuario.Identificador;
                
                DTOProcesso processos = new DTOProcesso() 
                {                    
                    No_Processo = txtNomeDoProcesso.Text,
                    Cd_Corporativo_Unidade = Convert.ToInt32(drpArea.SelectedItem.Value),
                    Cd_Objetivo = Convert.ToInt32(drpMacroprocesso.SelectedItem.Value),
                    No_Usuario_Atualizacao = usuario,
                    No_Usuario_Criacao = usuario,
                    Ds_Processo_Sei = txtProcessoSei.Text,
                    St_Ativo = rblSituacao.SelectedValue.Contains("1") ? true : false,
                    St_Excluido = rblProcessoPriorizadoParaGr.SelectedValue.Contains("1") ? true : false,
                    Dt_Atualizacao = DateTime.Now,
                    Dt_Criacao = DateTime.Now,
                };

                CodSeqProcesso = this.ServiceLocator.GetService < IPGAService >
                    ("antt.servicos/PGAService").SalvarProcesso(processos);


                //listAnexos.ForEach(t=> 
                //    var test = new DTOAnexo() 
                //    { 
                //        CaminhoAnexo = t.CaminhoAnexo
                //    },

                //codigo
                //    ProAnexo 

                //);

                //listAnexos.salvar

                List<DTOProcessoAnexo> registroProAnexo = new List<DTOProcessoAnexo>();

                registroProAnexo.Add(new DTOProcessoAnexo()
                {
                    Cd_Processo = CodSeqProcesso,
                    Cd_Anexo = 2,

                    No_Usuario_Criacao = usuario,
                    St_Ativo = rblSituacao.SelectedValue.Contains("1") ? true : false,
                    Dt_Atualizacao = DateTime.Now,
                    Dt_Criacao = DateTime.Now,
                    Ds_Motivo_Inativacao = null,
                    No_Usuario_Atualizado = usuario,
                });

                //if (listAnexos.Count>0)
                //{
                //    listAnexos.ForEach(
                        
                //        pX => new DTOAnexo()
                //        {
                //           CaminhoAnexo ="",
                //           Cd_Tipo_Anexo = 2
                //        }


                //    );
                //}

                
                                
                //var CodSeqProcesso = this.ServiceLocator.GetService<IPGAService>
                //   ("antt.servicos/PGAService").SalvarProcesso(registroProAnexo);
                //var CodSeqAcompanhamento = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarAcompanhamento(registro);

                var param = new Dictionary<string, object>();
                param.Add("CodSeqProcesso", CodSeqProcesso);

                MessageBox.ShowInformationMessage("Registro salvo com sucesso.", "Detalhar", param);


                //var exercicio = new DTOExercicio();
                //exercicio.CodigoSeqExercicio = drpAnoExercicio.SelectedValue.ToInt32();
                //registro.Exercicio = exercicio;

                //var tipo = new DTOTipoAcompanhamento();
                //tipo.CodigoSeqTipoAcompanhamento = drpTipoAcompanhamento.SelectedValue.ToInt32();
                //registro.TipoAcompanhamento = tipo;

                //registro.StatusAcompanhamento = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterStatusAcompanhamento(Common.EnumStatusAcompanhamento.Pendente);

                //if (flpAnexo.HasFile)
                //{
                //    var anexo = new DTOAnexo();

                //    anexo.DescricaoAnexo = flpAnexo.FileName;
                //    anexo.Conteudo = flpAnexo.FileBytes;
                //    anexo.CaminhoAnexo = System.Configuration.ConfigurationManager.AppSettings["diretorioAnexosAcompanhamento"];

                //    registro.Anexo = anexo;
                //}

                //var CodSeqAcompanhamento = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").SalvarAcompanhamento(registro);

                //var param = new Dictionary<string, object>();
                //param.Add("CodSeqAcompanhamento", CodSeqAcompanhamento);

                //MessageBox.ShowInformationMessage($"Registro salvo com sucesso. Processo = {CodSeqProcesso}", "Detalhar");
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void PreRemove(int codigoControleAcao)
        {
            var parametros = new Dictionary<string, object>();
            parametros.Add("CodigoSeqControleAcao", codigoControleAcao);
            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente excluir o registro?",
                "Deseja realmente excluir o registro?", MessageBoxButtons.YesNo, "Remover", parametros);
        }

        private void Remover(int codigoSeqControleAcao)
        {
            try
            {
                //this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarControleAcao(codigoSeqControleAcao, UsuarioAdministrador);

                Recarregar();
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro()
        {
            try
            {
                //if (CodSeqProcesso > 0)
                //{
                    CarregarArea();
                    CarregarObjtivos();

                    var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterProcesso(1);
                    txtNomeDoProcesso.Text = registro.Processo.No_Processo;
                    txtProcessoSei.Text = registro.Processo.Ds_Processo_Sei;
                    txtNomeAnexo.Text = registro.Anexos.DescricaoAnexo;

                    

                    //txtAnoExercicio.Text = registro.AnoExercicio.ToString();
                    //txtArea.Text = registro.UnidadeCorporativa;

                    //if (registro.DataInicioAcao.HasValue)
                    //    txtDataInicioAcao.Text = registro.DataInicioAcao.Value.ToString("dd/MM/yyyy");
                    //if (registro.DataFimAcao.HasValue)
                    //    txtDataFimAcao.Text = registro.DataFimAcao.Value.ToString("dd/MM/yyyy");

                    //CarregarControlesAcao(0, "DataReporte", false);

                    //ValidaPermissao(registro.CodigoUnidade.ToString());
                //}
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private void CarregarRegistro(int codigoControleAcao)
        {
            //var registro = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ObterControleAcao(codigoControleAcao);
            //txtDataReporte.Text = registro.DataReporte.ToString("dd/MM/yyyy");
            //txtPercentualReporte.Text = registro.ValorPercentualReporte.ToString() + "%";
            //txtPercentualReporte.Enabled = true;

            //txtDescricaoObservacao.Text = registro.DescricaoObservacao;
            //txtDescricaoCausa.Text = registro.DescricaoCausa;
            //txtDescricaoAcao.Text = registro.DescricaoAcao;
            //txtRecursoFinanceiroUtilizado.Text = String.Format("{0:N2}", registro.ValorRecursoFinanceiro);
            //CodigoSeqControleAcaoParameter = registro.CodigoSeqControleAcao;
        }

        private void CarregarControlesAcao(int pageIndex, string sortExpression, bool sortAscending)
        {
            //var dados = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarControlesPorAcao(CodigoSeqAcaoParameter, pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize, sortExpression, sortAscending);

            //if (dados.Count() > 0)
            //{
                //gdvControleAcao.DataSource = dados.Entities.ToList();

                //gdvControleAcao.DataSource = from a in dados
                //                             select new
                //                             {
                //                                 DataReporte = a.DataReporte,
                //                                 ValorPercentualReporte = !string.IsNullOrEmpty(a.ValorPercentualReporte.ToString()) ? a.ValorPercentualReporte + "%" : string.Empty,
                //                                 DescricaoObservacao = a.DescricaoObservacao,
                //                                 DescricaoCausa = a.DescricaoCausa,
                //                                 DescricaoAcao = a.DescricaoAcao,
                //                                 CodigoSeqControleAcao = a.CodigoSeqControleAcao
                //                             };
                //gdvControleAcao.DataBind();

                //gdvControleAcao.Attributes["SortExpression"] = sortExpression;
                //gdvControleAcao.Attributes["SortAscending"] = sortAscending.ToString();

                //ucPaginatorConsulta.Visible = (dados.RowsCount > 0);
                //ucPaginatorConsulta.TotalRecords = dados.RowsCount;
                //ucPaginatorConsulta.PageIndex = pageIndex + 1;
                //ucPaginatorConsulta.DataBind();
                //upPnlControleAcao.Update();
            //}
        }

        private void Recarregar()
        {
            if (PageIndex == 0)
                WebHelper.Redirect("~/Site/Processos/Detalhar.aspx?Cd_Seq_Processo_Anexo=" + CodSeqProcesso);
            //else
                //WebHelper.Redirect(String.Format("~/Site/Acompanhar/Acao/Detalhar.aspx?Cd_Seq_Processo_Anexo={0}&AnoExercicio={1}&Unidade={2}&Meta={3}&Status={4}&PageIndex={5}", CodSeqProcesso, AnoExercicio, Unidade, Meta, Status, PageIndex));
        }

        #endregion


    }
}