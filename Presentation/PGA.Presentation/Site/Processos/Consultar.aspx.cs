﻿using PGA.Presentation.Util;
using PGA.Services.Spec.Services;
using SQFramework.Core;
using SQFramework.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PGA.Presentation.Site.Processos
{
    public partial class Consultar : CustomPageBase
    {
        #region [Properties]

        private int Area
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("drpArea").ToInt32();
            }
        }

        private int Situacao
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("Situacao").ToInt32();
            }
        }

        private int PageIndex
        {
            get
            {
                return QueryStringHelper.GetQueryStringParameter("PageIndex").ToInt32();
            }
        }

        #endregion

        #region [Events]

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Load += new EventHandler(Page_Load);
            MessageBox.Click += new MessageBoxClickHandler(MessageBox_Click);
            //ucPaginatorConsulta.GoToPage += new SQFramework.Web.Controls.PaginatorEventHandler(ucPaginatorConsulta_GoToPage);
            btnPesquisar.Click += BtnPesquisar_Click;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                try
                {
                    CarregarArea();

                    if(Area > 0)
                        drpArea.SelectedValue = Area.ToString();                    
                    if(Situacao > 0)
                        rblSituacao.SelectedValue = Situacao.ToString();
                    if(PageIndex > 0)
                        ConsultarDados(PageIndex - 1);
                }
                catch(Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        protected void MessageBox_Click(object sender, MessageBoxEventArgs e)
        {
            switch(e.Command)
            {
                case "Remover":
                    if(e.Result == MessageBoxResult.Yes)
                        Remover(e.Parameters["CodigoSeqProcessos"].ToInt32());
                    break;
                case "Concluir":
                    if(e.Result == MessageBoxResult.Yes)
                        Concluir(e.Parameters["CodigoSeqProcessos"].ToInt32());
                    break;
                case "Deslogar":
                    if(e.Result == MessageBoxResult.OK)
                        WebHelper.Logoff();
                    break;
            }
        }

        protected void ucPaginatorConsulta_GoToPage(object sender, SQFramework.Web.Controls.PaginatorEventArgs e)
        {
            ConsultarDados(e.NewPage - 1);
        }

        private void BtnPesquisar_Click(object sender, EventArgs e)
        {
            ConsultarDados(0);
        }

        protected void grdAcompanhamento_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch(e.CommandName)
            {
                case "Excluir":
                    {
                        var parametros = new Dictionary<string, object>();
                        parametros.Add("CodigoSeqProcesso", e.CommandArgument);

                        MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente excluir o processo?",
                            "Deseja realmente excluir o processo?", MessageBoxButtons.YesNo, "Remover", parametros);
                    }
                    break;
                case "Concluir":
                    {
                        var encaminhamentos = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarEncaminhamentosDoAcompanhamento(e.CommandArgument.ToInt32());

                        if(encaminhamentos.Where(x => x.StatusEncaminhamento.CodigoSeqStatusEncaminhamento == (int)Common.EnumStatusAcompanhamento.Pendente).Count() == 0)
                        {
                            var parametros = new Dictionary<string, object>();
                            parametros.Add("CodigoSeqProcesso", e.CommandArgument);

                            MessageBox.ShowConfirmationMessage(MessageBoxType.Question, "Deseja realmente concluir o Processo?",
                            "Deseja realmente concluir o processo?", MessageBoxButtons.YesNo, "Concluir", parametros);
                        }
                        else
                            MessageBox.ShowErrorMessage("Não é possível concluir o Processo por existir pendente(s)");
                    }
                    break;
                case "Detalhar":
                    WebHelper.Redirect(String.Format("~/Site/Cadastro/Acompanhamento/Detalhar.aspx?CodigoSeqAcompanhamento={0}&TipoAcompanhamento={1}&AnoExercicio={2}&Status={3}&PageIndex={4}", e.CommandArgument, txtNomeDoProcesso, drpArea.SelectedValue, rblSituacao.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                case "Visuzalizar":
                    WebHelper.Redirect(String.Format("~/Site/Cadastro/Acompanhamento/Visualizar.aspx?CodigoSeqAcompanhamento={0}&TipoAcompanhamento={1}&AnoExercicio={2}&Status={3}&PageIndex={4}", e.CommandArgument, txtNomeDoProcesso, drpArea.SelectedValue, rblSituacao.SelectedValue, ucPaginatorConsulta.PageIndex));
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region [Methods]
        private void CarregarArea()
        {
            drpArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarCorporativoUnidadeAtivoPorExercicio(drpArea.SelectedValue.ToInt32());
            drpArea.DataTextField = "DescricaoUnidade";
            drpArea.DataValueField = "CodigoUnidade";
            drpArea.DataBind();
            drpArea.Items.Insert(0, new ListItem("Todas", ""));
        }

        private void ConsultarDados(int pageIndex)
        {
            Validate();

            if(IsValid)
            {
                try
                {
                    var processos = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService")
                        .ListarProcessoNomeAreaSituacao(
                            txtNomeDoProcesso.ToString(),
                            drpArea.SelectedValue.ToInt16(),
                            rblSituacao.SelectedValue.ToInt32(),
                        pageIndex * ucPaginatorConsulta.PageSize, ucPaginatorConsulta.PageSize,
                            "CodigoSeqProcessos", true);

                    //ucPaginatorConsulta.Visible = (processos.RowsCount > 0);
                    //ucPaginatorConsulta.TotalRecords = processos.RowsCount;
                    //ucPaginatorConsulta.PageIndex = pageIndex + 1;
                    //ucPaginatorConsulta.DataBind();
                }
                catch(Exception ex)
                {
                    HandleException(ex);
                }
            }
        }
        //private void CarregarArea()
        //{
        //    //if(UsuarioCadastrador || UsuarioAdministrador || UsuarioValidador)
        //    //    drpArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarProcessos().OrderByDescending(i => i.NomeProcesso);
        //    //valida rotina para validar usuario
        //    //else if(UsuarioConsultor)
        //    //    drpArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarExercicioExcetoPlanejamento().OrderByDescending(i => i.Ano);

        //    drpArea.DataSource = this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").ListarProcessos().OrderByDescending(i => i.Cd_Corporativo_Unidade);

        //    drpArea.DataTextField = "Area";
        //    drpArea.DataBind();
        //    drpArea.Items.Insert(0, new ListItem("Todos", "0"));
        //}

        private void Remover(int CodigoSeqAcompanhamento)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").DeletarAcompanhamento(CodigoSeqAcompanhamento);
                MessageBox.ShowInformationMessage("Registro excluido com sucesso!");
                //ConsultarDados(ucPaginatorConsulta.PageIndex - 1);
            }
            catch
            {
                MessageBox.ShowErrorMessage("Não foi possível excluir o registro devido ao fato de ele já ter vinculação.");
            }
        }

        private void Concluir(int CodigoSeqAcompanhamento)
        {
            try
            {
                this.ServiceLocator.GetService<IPGAService>("antt.servicos/PGAService").AlterarStatusAcompanhamento(CodigoSeqAcompanhamento, (int)Common.EnumStatusAcompanhamento.Concluido);
                MessageBox.ShowInformationMessage("Acompanhamento concluído com sucesso!");
                //ConsultarDados(ucPaginatorConsulta.PageIndex - 1);
            }
            catch(Exception e)
            {
                MessageBox.ShowErrorMessage(e.Message);
            }
        }
        #endregion
    }
}