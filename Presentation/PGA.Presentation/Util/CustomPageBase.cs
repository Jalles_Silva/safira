﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Logging;
using SCA.WebControls;
using SCA.Servicos.Spec.Servicos;

namespace PGA.Presentation.Util
{
    public class CustomPageBase : SCAPageBase
    {
        protected bool TryRemoteOperation(Action remoteOperation)
        {
            if (remoteOperation != null)
            {
                try
                {
                    remoteOperation();
                    return true;
                }
                catch (Exception ex)
                {
                    RegistraLog(ex);
                    MessageBox.ShowErrorMessage("Desculpe, não foi possível se comunicar com os serviços do sistema.");
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public void RegistraLog(Exception ex)
        {
            ILog logger = LogManager.GetLogger("ASPNET.LOG");

            logger.ErrorFormat("{0} Página: {1} - {2} ", ex.Message, this.Request.Url.AbsoluteUri, ex.ToString());
        }

        public bool PopUp
        {
            get
            {
                return ((PGA.Presentation.Site.MasterPage.Site)this.Page.Master).PopUp;
            }
            set
            {
                ((PGA.Presentation.Site.MasterPage.Site)this.Page.Master).PopUp = value;
            }
        }

        public bool UsuarioAdministrador
        {
            get
            {
                return VerificarPermissao("PGA_Administrador", true);
            }
        }

        public bool UsuarioAlteraAcompanhamento
        {
            get
            {
                return VerificarPermissao("AlterarAcompanhamento", true);
            }
        }

        public bool UsuarioConsultor
        {
            get
            {
                return VerificarPermissao("PGA_Consultor", true);
            }
        }

        public bool UsuarioValidador
        {
            get
            {
                return VerificarPermissao("PGA_Validador", true);
            }
        }

        public bool UsuarioCadastrador
        {
            get
            {
                return VerificarPermissao("PGA_Cadastrador", true);
            }
        }

        public bool UsuarioPublico
        {
            get
            {
                return VerificarPermissao("PGA_Publico", true);
            }
        }

    }
}