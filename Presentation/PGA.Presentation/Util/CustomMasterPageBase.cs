﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SQFramework.Web;

namespace PGA.Presentation.Util
{
    public class CustomMasterPageBase : MasterPageBase
    {
        public new CustomPageBase PageBase
        {
            get
            {
                return this.Page as CustomPageBase;
            }
        }
    }
}