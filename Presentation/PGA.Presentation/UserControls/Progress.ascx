﻿<%@ Control Language="C#" CodeBehind="Progress.ascx.cs" Inherits="PGA.Presentation.UserControls.Progress" %>
<script type="text/javascript" language="javascript">
    var UpdateProgress = '<%= UpdateProgress.ClientID %>';
    var DivProgress = '<%= DivProgress.ClientID %>';
</script>
<div id="DivProgress" runat="server" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="divModalLabel"
    aria-hidden="true" data-backdrop="false">
    <asp:UpdateProgress ID="UpdateProgress" DisplayAfter="0" runat="server">
        <ProgressTemplate>
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        &nbsp;
                    </div>
                    <div class="modal-body text-center">
                        <asp:Image ID="imgLoading" runat="server" ImageUrl="~/App_Themes/Default/images/ajax-loader.gif" />
                        Processando... 
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</div>
