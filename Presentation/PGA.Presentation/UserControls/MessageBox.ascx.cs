﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SQFramework.Web;
using PGA.Presentation.Util;

namespace PGA.Presentation.UserControls
{
    public partial class MessageBox : CustomUserControlBase, IMessageBox
    {
        #region Constantes

        private const string VS_BOTOES_MENSAGEM = "_BOTOES_MENSAGEM_SELECIONADO";
        private const string VS_COMANDO_RETORNO = "_COMANDO_RETORNO";

        #endregion

        #region Eventos

        public event MessageBoxClickHandler Click;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.ButtonOk.Click += new EventHandler(ButtonOk_Click);
            this.ButtonCancelar.Click += new EventHandler(ButtonCancelar_Click);

            if (PageBase != null && PageBase.MessageBoxPostBackForce)
                ScriptManager.GetCurrent(Page).RegisterPostBackControl(ButtonOk);
        }

        protected void ButtonOk_Click(object sender, EventArgs e)
        {
            if (Click != null)
                Click(sender, this.ObterArgumento(true));
        }

        protected void ButtonCancelar_Click(object sender, EventArgs e)
        {
            if (Click != null)
                Click(sender, this.ObterArgumento(false));
        }

        #endregion

        #region Propriedades

        private MessageBoxButtons BotaoSelecionado
        {
            get
            {
                MessageBoxButtons botaoSelecionado = MessageBoxButtons.OK;

                if (ViewState[VS_BOTOES_MENSAGEM] != null)
                    botaoSelecionado = (MessageBoxButtons)ViewState[VS_BOTOES_MENSAGEM];

                return botaoSelecionado;
            }
            set
            {
                ViewState[VS_BOTOES_MENSAGEM] = value;
            }
        }

        private string ComandoRetorno
        {
            get
            {
                string comandoRetorno = null;

                if (ViewState["ComandoRetorno"] != null)
                    comandoRetorno = ViewState["ComandoRetorno"].ToString();

                return comandoRetorno;
            }
            set
            {
                ViewState["ComandoRetorno"] = value;
            }
        }

        private Dictionary<string, object> Parameters
        {
            get
            {
                if (ViewState["Parameters"] == null)
                    ViewState["Parameters"] = new Dictionary<string, object>();

                return ViewState["Parameters"] as Dictionary<string, object>;
            }
            set
            {
                ViewState["Parameters"] = value;
            }
        }

        #endregion

        #region Metodos


        private void ControlarBotoes(MessageBoxButtons botoes)
        {
            switch (botoes)
            {
                case MessageBoxButtons.OK:
                    ButtonOk.Text = "Ok";
                    ButtonCancelar.Visible = false;
                    break;
                case MessageBoxButtons.OKCancel:
                    ButtonCancelar.Visible = true;
                    ButtonCancelar.Text = "Cancelar";
                    ButtonOk.Text = "Ok";
                    break;
                case MessageBoxButtons.YesNo:
                    ButtonCancelar.Visible = true;
                    ButtonCancelar.Text = "Não";
                    ButtonOk.Text = "Sim";
                    break;
            }
        }

        public void Mostrar(MessageBoxType tipoMensagem, string titulo, string mensagem, MessageBoxButtons botoes, string comandoRetorno)
        {
            this.Mostrar(tipoMensagem, titulo, mensagem, botoes, comandoRetorno, null);
        }

        public void Mostrar(MessageBoxType tipoMensagem, string titulo, string mensagem, MessageBoxButtons botoes, string comandoRetorno, params KeyValuePair<string, object>[] parameters)
        {
            this.Mostrar(tipoMensagem, titulo, mensagem, botoes, comandoRetorno, parameters, null, null);
        }

        private MessageBoxEventArgs ObterArgumento(bool botaoOk)
        {
            MessageBoxEventArgs args = null;

            switch (this.BotaoSelecionado)
            {
                case MessageBoxButtons.OK:
                    args = new MessageBoxEventArgs(this.PageBase, MessageBoxResult.OK, this.ComandoRetorno, this.Parameters);
                    break;
                case MessageBoxButtons.OKCancel:
                    if (botaoOk)
                        args = new MessageBoxEventArgs(this.PageBase, MessageBoxResult.OK, this.ComandoRetorno, this.Parameters);
                    else
                        args = new MessageBoxEventArgs(this.PageBase, MessageBoxResult.Cancel, this.ComandoRetorno, this.Parameters);
                    break;
                case MessageBoxButtons.YesNo:
                    if (botaoOk)
                        args = new MessageBoxEventArgs(this.PageBase, MessageBoxResult.Yes, this.ComandoRetorno, this.Parameters);
                    else
                        args = new MessageBoxEventArgs(this.PageBase, MessageBoxResult.No, this.ComandoRetorno, this.Parameters);
                    break;
                default:
                    args = new MessageBoxEventArgs(this.PageBase, MessageBoxResult.OK, this.ComandoRetorno, this.Parameters);
                    break;
            }

            return args;
        }

        public void ShowInformationMessage(string mensagem)
        {
            this.ShowInformationMessage(mensagem, null, null);
        }

        public void ShowInformationMessage(string mensagem, string comandoRetorno)
        {
            this.ShowInformationMessage(mensagem, comandoRetorno, null);
        }

        public void ShowInformationMessage(string mensagem, string comandoRetorno, Dictionary<string, object> parametros)
        {
            List<KeyValuePair<string, object>> par = new List<KeyValuePair<string, object>>();
            if (parametros != null)
                foreach (KeyValuePair<string, object> keys in parametros)
                    par.Add(keys);

            this.Mostrar(MessageBoxType.Information, "Informação", mensagem, MessageBoxButtons.OK, comandoRetorno, par.ToArray());
        }

        public void ShowErrorMessage(string mensagem)
        {
            this.ShowErrorMessage(mensagem, null);
        }

        public void ShowErrorMessage(string mensagem, string comandoRetorno)
        {
            this.Mostrar(MessageBoxType.Error, "Erro", mensagem, MessageBoxButtons.OK, comandoRetorno);
        }

        public void ShowConfirmationMessage(MessageBoxType tipoMensagem, string titulo, string messagem, MessageBoxButtons botoes, string comandoRetorno)
        {
            this.ShowConfirmationMessage(tipoMensagem, titulo, messagem, botoes, comandoRetorno, null);
        }

        public void ShowConfirmationMessage(MessageBoxType tipoMensagem, string titulo, string messagem, MessageBoxButtons botoes, string comandoRetorno, Dictionary<string, object> parametros)
        {
            List<KeyValuePair<string, object>> par = new List<KeyValuePair<string, object>>();
            if (parametros != null)
                foreach (KeyValuePair<string, object> keys in parametros)
                    par.Add(keys);

            this.Mostrar(tipoMensagem, titulo, messagem, botoes, comandoRetorno, par.ToArray());
        }

        public void MostrarMensagemConfirmacao(MessageBoxType tipoMensagem, string titulo,
            string messagem, MessageBoxButtons botoes, string comandoRetorno, Dictionary<string, object> parametros,
            string subtitulo, Dictionary<string, string> dadosComplementares)
        {
            List<KeyValuePair<string, object>> par = new List<KeyValuePair<string, object>>();
            if (parametros != null)
                foreach (KeyValuePair<string, object> keys in parametros)
                    par.Add(keys);

            this.Mostrar(tipoMensagem, titulo, messagem, botoes, comandoRetorno, par.ToArray(), subtitulo, dadosComplementares);
        }

        private void Mostrar(MessageBoxType tipoMensagem, string titulo, string mensagem, MessageBoxButtons botoes,
            string comandoRetorno, KeyValuePair<string, object>[] parameters, string subtitulo, Dictionary<string, string> dadosComplementares)
        {
            mensagem = mensagem.Replace("\r\n", "<BR />");

            if (parameters != null)
            {
                foreach (KeyValuePair<string, object> key in parameters)
                    if (!this.Parameters.ContainsKey(key.Key))
                        this.Parameters.Add(key.Key, key.Value);
                    else
                        this.Parameters[key.Key] = key.Value;
            }

            if (dadosComplementares != null && dadosComplementares.Count > 0)
            {
                pnlDadosComplementares.Visible = true;
                mensagem = "<BR />" + mensagem;
                lblSubtitulo.Text = subtitulo;
                rptDadosComplementares.DataSource = dadosComplementares;
                rptDadosComplementares.DataBind();
            }
            else
            {
                pnlDadosComplementares.Visible = false;
            }

            this.BotaoSelecionado = botoes;
            this.ControlarBotoes(botoes);
            this.ComandoRetorno = comandoRetorno;
            LabelMensagem.Text = mensagem;
            lblTitulo.Text = titulo;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "$('#divMensagem').modal();", true);

            ButtonOk.Focus();

            UpdatePanel updatePanelMessageBox = this.PageBase.UpdatePanelMessageBox;

            if (updatePanelMessageBox != null)
                updatePanelMessageBox.Update();
        }

        public void MostrarMensagemSucesso(string mensagem)
        {
            this.MostrarMensagemSucesso(mensagem, null, null);
        }

        public void MostrarMensagemSucesso(string mensagem, string comandoRetorno)
        {
            this.MostrarMensagemSucesso(mensagem, comandoRetorno, null);
        }

        public void MostrarMensagemSucesso(string mensagem, string comandoRetorno, Dictionary<string, object> parametros)
        {
            List<KeyValuePair<string, object>> par = new List<KeyValuePair<string, object>>();
            if (parametros != null)
                foreach (KeyValuePair<string, object> keys in parametros)
                    par.Add(keys);

            this.Mostrar(MessageBoxType.Information, "Sucesso", mensagem, MessageBoxButtons.OK, comandoRetorno, par.ToArray());
        }

        public void MostrarMensagemAlerta(string mensagem)
        {
            this.MostrarMensagemAlerta(mensagem, null);
        }

        public void MostrarMensagemAlerta(string mensagem, string comandoRetorno)
        {
            this.Mostrar(MessageBoxType.Warning, "Aviso", mensagem, MessageBoxButtons.OK, comandoRetorno);
        }
        #endregion
    }
}