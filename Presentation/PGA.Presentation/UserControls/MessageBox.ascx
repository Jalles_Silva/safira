﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessageBox.ascx.cs" Inherits="PGA.Presentation.UserControls.MessageBox" %>
<asp:Label ID="LabelAtiva" runat="server" EnableViewState="false"></asp:Label>
<!-- Modal -->
<div class="modal fade" id="divMensagem" tabindex="-1" role="dialog" aria-labelledby="divModalLabel"
    aria-hidden="true" data-backdrop="static" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="divModalLabel">
                    <asp:Label ID="lblTitulo" runat="server" EnableViewState="false" />
                </h4>
            </div>
            <div class="modal-body">
                <asp:Panel ID="pnlDadosComplementares" runat="server" CssClass="pnlGroup" Width="83%"
                    BackColor="#F4F4F4" Visible="false">
                    <div class="TituloModulo">
                        <asp:Label ID="lblSubtitulo" runat="server" />
                        <br />
                        <hr />
                    </div>
                    <asp:Repeater ID="rptDadosComplementares" runat="server">
                        <HeaderTemplate>
                            <table class="TblCampos">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="ColunaEsquerda" style="width: 100px;">
                                    <asp:Label ID="lblKey" runat="server" Text='<%#Eval("Key") %>' />:
                                </td>
                                <td class="ColunaDireita">
                                    <asp:Label ID="lblValue" runat="server" Text='<%#Eval("Value") %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </asp:Panel>
                <ajaxToolkit:RoundedCornersExtender ID="rceDadosComplementares" runat="server" Color="244, 244, 244"
                    Enabled="True" TargetControlID="pnlDadosComplementares" BorderColor="244, 244, 244"
                    Corners="All" Radius="15">
                </ajaxToolkit:RoundedCornersExtender>
                <span class="ext-mb-text">
                    <asp:Label ID="LabelMensagem" EnableViewState="false" runat="server" />
                </span>
            </div>
            <div class="modal-footer">
                <asp:Button ID="ButtonOk" runat="server" Text="Ok" CssClass="btn btn-default btn-antt" CausesValidation="False"
                    UseSubmitBehavior="False" EnableViewState="false"  data-dismiss="modal" />
                <asp:Button ID="ButtonCancelar" runat="server" Text="Cancelar" CssClass="btn btn-default btn-antt"
                    CausesValidation="False" UseSubmitBehavior="False" EnableViewState="false" data-dismiss="modal" />
            </div>
        </div>
    </div>
</div>
