﻿using PGA.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Domain.Entities
{
    public partial class ControleAcao
    {
        public virtual bool UsuarioAdministrador { get; set; }

        public override void Save()
        {

            if (!UsuarioAdministrador)
            {
                if (this.DataReporte.Month < this.Acao.Meta.Exercicio.Fase.MesInicio)
                    throw new Exception("Impossível registrar acompanhamento devido à Data de Reporte ser de um período não iniciado.");
            }

            if (this.DataReporte.Year != this.Acao.Meta.Exercicio.Ano)
                throw new Exception("Impossível registrar acompanhamento devido à Data de Reporte não fazer parte deste exercício.");

            base.Save();
        }

        public override void Delete()
        {
            var acao = this.Acao.ControlesAcao.Where(i => i.CodigoSeqControleAcao == this.CodigoSeqControleAcao).FirstOrDefault();

            if (acao.DataReporte.Year != this.Acao.Meta.Exercicio.Ano)
            {
                throw new Exception("Não é possível excluir ações de trimestres fechados");
            }

            if (!UsuarioAdministrador && acao.DataReporte.Month < this.Acao.Meta.Exercicio.Fase.MesInicio)
            {
                throw new Exception("Não é possível excluir ações de trimestres fechados");
            }

            base.Delete();           
        }
    }
}
