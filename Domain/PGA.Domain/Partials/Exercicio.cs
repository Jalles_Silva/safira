﻿using PGA.Common;
using PGA.Integration.Spec.ValueObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using iTextSharp.text;
using PGA.Domain.Utils;
using SCL.Servicos.Spec.Servicos;
using Utilitarios.Services;
using SQFramework.Core;

namespace PGA.Domain.Entities
{
    public partial class Exercicio
    {
        private const string CORSTATUS_VERDE = "verde";
        private const string CORSTATUS_AMARELO = "amarelo";
        private const string CORSTATUS_ALARANJADO = "alaranjado";
        private const string CORSTATUS_VERMELHO = "vermelho";

        private const string CORINSTRUMETOS_AMARELO = "amarelo";
        private const string CORINSTRUMETOS_AZUL = "azul";
        private const string CORINSTRUMETOS_VERDE = "verde";
        private const string CORINSTRUMETOS_VERMELHO = "vermelho";
        private const string CORINSTRUMETOS_LARANJA = "alaranjado";
        private const string CORINSTRUMETOS_CINZA = "cinza";
        private const string CORINSTRUMETOS_NEUTRO = "branco";

        public override void Save()
        {
            base.Save();
            LogExercicio log = new LogExercicio();
            log.AnoExercicio = this.Ano;
            log.CodigoFase = this.Fase.CodigoSeqFase;
            log.DataCadastro = this.DataCadastro;
            log.NomeUsuario = this.NomeUsuario;
            log.Save();
        }

        public virtual void AbrirExercicio()
        {
            //if (Exercicio.GetRepository().ObterExercicioEmPlanejamento() == null)
                this.Save();
            //else
            //    throw new Exception("Impossível criar um novo Exercício, já existe um exercício na fase de Planejamento.");
        }

        public virtual void PromoverFase(string nomeUsuario)
        {
            Fase novaFase = Fase.GetRepository().ObterProximaFase(this.Fase.Ordem);
            if (novaFase != null)
            {
                if (novaFase.CodigoSeqFase == (int)FaseEnum.AcompanhamentoPrimeiroTrimestre)
                {
                    if (Exercicio.GetRepository().ObterExercicioEmAcompanhamento() != null)
                        throw new Exception("Impossível promover o Exercício, já existe um exercício na fase de Acompanhamento.");
                }
                this.SetFase(novaFase);
                this.NomeUsuario = nomeUsuario;
                this.DataCadastro = DateTime.Now;
                this.Save();
            }
        }

        public virtual byte[] GerarRelatorioAcompanhamento(string Area, int codigoPeriodo, bool periodoAcumulado, int codigoTipoMeta, bool alinhamnetoPE, bool alinhamnetoPPA, bool alinhamnetoMI, int codigoTipoInstrumento, int metaInterna, Func<List<VOCalculoAcumuladoTrimestre>, byte[]> gerarImagemGrafico)
        {
            IList<Meta> metas = Meta.GetRepository().ListarMetasAcompanhamento(
                Area,
                Ano,
                codigoTipoMeta,
                alinhamnetoPE,
                alinhamnetoPPA,
                alinhamnetoMI,
                codigoTipoInstrumento,
                metaInterna
            );

            var unidades = metas.Select(i => i.CorporativoUnidade.CodigoUnidade).Distinct();
            Dictionary<int, string> chefes = new Dictionary<int, string>();

            if (unidades != null && unidades.Any())
            {
                using (var serviceLocator = new ServiceLocator())
                {
                    var sclService = serviceLocator.GetService<ISGRHService>("antt.servicos/IntegracaoSGRH");

                    foreach (var item in unidades)
                    {
                        var dadosOrgao = sclService.ObterCamposOrgaoPorCodigo(item.ToString());
                        if (dadosOrgao != null)
                        {
                            var servidor = sclService.ObterEmpregadoPorCodigo(dadosOrgao.CodEmpregado.ToLong());
                            chefes.Add(item, servidor?.Nome);
                        }
                    }
                }
            }

            //Caso seja selecionado TODOS no filtro Tipo de Meta 
            //É necessário recuperar a descrição de todos os Tipos de Metas para incluir na capa do relatório
            string todosTiposMetas = "";
            if (codigoTipoMeta == 0)
            {
                var listTipoMetas = TipoMeta.GetRepository().ListAll();
                for (int i = 0; i < listTipoMetas.Count; i++)
                {
                    if (i > 0 && i < listTipoMetas.Count)
                    {
                        todosTiposMetas += ", ";
                    }

                    todosTiposMetas += listTipoMetas[i].DescricaoTipoMeta;
                }
            }

            //Caso seja selecionado TODOS no filtro Tipo de Alinhamento 
            //É necessário recuperar a descrição de todos os Tipos de Alinhamento para incluir na capa do relatório
            //string todosTiposAlinhamento = "";
            //if (codigoTipoAlinhamento == 0)
            //{
            //    var listTipoAlinhamento = Enum.GetValues(typeof(TiposAlinhamento)).Cast<TiposAlinhamento>().ToList();

            //    for (int i = 0; i < listTipoAlinhamento.Count; i++)
            //    {
            //        if (i > 0 && i < listTipoAlinhamento.Count)
            //        {
            //            todosTiposAlinhamento += ", ";
            //        }

            //        todosTiposAlinhamento += listTipoAlinhamento[i].GetDescription();

            //    }
            //}

            //Caso seja selecionado TODOS no filtro Tipo de Instrumento 
            //É necessário recuperar a descrição de todos os Tipos de Instrumento para incluir na capa do relatório
            string todosTiposInstrumento = "";
            if (codigoTipoInstrumento == 0)
            {
                var listTipoInstrumento = Enum.GetValues(typeof(TiposInstrumento)).Cast<TiposInstrumento>().ToList();

                for (int i = 0; i < listTipoInstrumento.Count; i++)
                {
                    if (i > 0 && i < listTipoInstrumento.Count)
                    {
                        todosTiposInstrumento += ", ";
                    }

                    todosTiposInstrumento += listTipoInstrumento[i].GetDescription();

                }
            }

            var relatorio = new VORelatorioAcompanhamento
            {
                Exercicio = Ano.ToString(),
                Periodo = codigoPeriodo == 0
                    ? "Acumulado no Exercício"
                    : string.Format("{0}º Trimestre{1}",
                        codigoPeriodo,
                        periodoAcumulado ? " (Acumulado)" : ""
                    ),
                TipoMetas = codigoTipoMeta == 0 ? todosTiposMetas : TipoMeta.GetRepository().Get(codigoTipoMeta).DescricaoTipoMeta,
                //TipoAlinhamentos = codigoTipoAlinhamento == 0 ? todosTiposAlinhamento : ((TiposAlinhamento)codigoTipoAlinhamento).GetDescription(),
                TipoInstrumentos = codigoTipoInstrumento == 0 ? todosTiposInstrumento : ((TiposInstrumento)codigoTipoInstrumento).GetDescription()
            };


            VOMetaAcompanhamento metaAcompanhamento;
            DateTime dataUltimoAcompanhamento;
            string nomeGestor;

            var count = 0;

            foreach (var item in metas)
            {
                // [codigoPeriodo]
                // Período (trimestre)
                // Ações: Considerar o período (inicio e fim da ação)
                //        - Nos comentários filtrar também
                //        - Nos controles, o valor do último lançamento deve ser válido
                //          como o primeiro para um controle que esteja em um trimestre sem lançamentos
                //          de controles
                DateTime dataInicioPeriodo;
                DateTime dataInicioPeriodoComentarios;
                DateTime dataFinalPeriodo;
                {
                    var numeroTrimestre = codigoPeriodo == 0
                        ? 4  // Sendo [0] quer dizer que é o período completo, ou seja, 4 trimestre
                        : codigoPeriodo;
                    var numeroMesInicioTrimestre = (numeroTrimestre * 3) - 2;
                    var numeroMesFinalTrimestre = (numeroTrimestre * 3);

                    dataInicioPeriodo = new DateTime((int)ano, 1, 1); // 01/01/{ano}
                    dataInicioPeriodoComentarios = periodoAcumulado
                        ? dataInicioPeriodo
                        : new DateTime((int)ano, numeroMesInicioTrimestre, 1); // 01/{mes}/{ano}

                    var ultimoDiaDoMes = DateTime.DaysInMonth((int)ano, numeroMesFinalTrimestre);

                    // {ultimo_dia}/{mes}/{ano} 23:59:59
                    dataFinalPeriodo = new DateTime((int)ano, numeroMesFinalTrimestre, ultimoDiaDoMes, 23, 59, 59);
                }

                metaAcompanhamento = new VOMetaAcompanhamento();
                metaAcompanhamento.CodigoMeta = item.CodSeqMeta;
                metaAcompanhamento.DescricaoMeta = item.DescricaoMeta;
                metaAcompanhamento.StatusMetaInterna = item.StatusMetaInterna == false ? "Não" : "Sim";
                metaAcompanhamento.NomeArea = item.CorporativoUnidade.DescricaoUnidade;
                metaAcompanhamento.NomeCoordenador = item.NoCoordenacao;

                if (metas[count].GestaoDeRiscos != null)
                {
                    metaAcompanhamento.CorStatusGestaoRisco = CORINSTRUMETOS_AMARELO;

                }
                else
                {
                    metaAcompanhamento.CorStatusGestaoRisco = CORINSTRUMETOS_NEUTRO;
                }

                if (metas[count].AgendaRegulatoria != null)
                {
                    metaAcompanhamento.CorStatusAgendaRegulatoria = CORINSTRUMETOS_AZUL;

                }
                else
                {
                    metaAcompanhamento.CorStatusAgendaRegulatoria = CORINSTRUMETOS_NEUTRO;
                }

                if (metas[count].Desburocratizacao != null)
                {
                    metaAcompanhamento.CorStatusDesburocratizacao = CORINSTRUMETOS_VERDE;

                }
                else
                {
                    metaAcompanhamento.CorStatusDesburocratizacao = CORINSTRUMETOS_NEUTRO;
                }

                if (metas[count].Integridade != null)
                {
                    metaAcompanhamento.CorStatusIntegridade = CORINSTRUMETOS_VERMELHO;

                }
                else
                {
                    metaAcompanhamento.CorStatusIntegridade = CORINSTRUMETOS_NEUTRO;
                }

                if (metas[count].IniciativaPe != null)
                {
                    metaAcompanhamento.CorStatusIniciativaEstrategica = CORINSTRUMETOS_LARANJA;

                }
                else
                {
                    metaAcompanhamento.CorStatusIniciativaEstrategica = CORINSTRUMETOS_NEUTRO;
                }

                if (metas[count].DescricaoOutros != "")
                {
                    metaAcompanhamento.CorStatusOutros = CORINSTRUMETOS_CINZA;

                }
                else
                {
                    metaAcompanhamento.CorStatusOutros = CORINSTRUMETOS_NEUTRO;
                }

                chefes.TryGetValue(item.CorporativoUnidade.CodigoUnidade, out nomeGestor);
                metaAcompanhamento.NomeGestorEstrategico = nomeGestor;

                metaAcompanhamento.Indicadores = new List<VOIndicadorAcompanhamento>();

                foreach (var indicador in item.Indicadores)
                {
                    var voIndicador = new VOIndicadorAcompanhamento();

                    voIndicador.CodigoIndicador = indicador.CodigoSeqIndicador;
                    voIndicador.DescricaoIndicador = indicador.NoIndicador;
                    voIndicador.ValorAlvo = indicador.ValorAlvo;
                    indicador.DataAlvo = indicador.DataAlvo != DateTime.MinValue ? indicador.DataAlvo.Value.AddMonths(1).AddDays(-1) : DateTime.Now;
                    voIndicador.DataAlvo = indicador.DataAlvo.Value.ToShortDateString();
                    voIndicador.MesReferencia = indicador.MesReferencia.HasValue ? indicador.MesReferencia.Value.ToString("MM/yyyy") : "";
                    voIndicador.TipoResultado = indicador.TipoResultado.Descricao;

                    var controlesValidos = indicador.ControlesIndicador
                        .Where(w => w.DataIndicador >= dataInicioPeriodo &&
                                    w.DataIndicador <= dataFinalPeriodo)
                        .ToList();

                    // TODO: Se controlesValidos for vazio?
                    //       - Mas indicador.ControlesIndicador não for vazio.
                    //       - Quer dizer que somente não há controle para o período
                    //       - Nesse caso devemos considerar o controle do último período?
                    //       - ** Pegar o LastItem de indicador.ControlesIndicador?
                    if (controlesValidos.Count() > 0)
                    {
                        // Calculamos os totais em cada trimestre, iniciando todos com 0 (zero)
                        // para garantir que sempre haverá os 4 trimestres do ano independente
                        // dos valores cadastrados. Em caso de não haver informação para um dado
                        // trimestre, esse simplesmente conterá o valor 0 (zero).
                        {
                            var totaisTrimestres = new Dictionary<int, VOCalculoAcumuladoTrimestre>(4);

                            // Inicializa os totais
                            for (int i = 1; i <= 4; i++)
                            {
                                totaisTrimestres.Add(i, new VOCalculoAcumuladoTrimestre((EnumTipoResultado)indicador.TipoResultado.Codigo, i, indicador.ValorAlvo));
                            }

                            // Método que acumula os totais para os trimestres
                            Action<int, decimal> acumularTotal = (trimestre, valor) =>
                            {
                                totaisTrimestres[trimestre].TotalItens++;
                                totaisTrimestres[trimestre].ValorAcumulado += valor;
                            };

                            controlesValidos.ForEach((controle) => acumularTotal(
                                    // Numero do trimestre
                                    Convert.ToInt32(Math.Ceiling(controle.DataIndicador.Month / 3.0)), controle.ValorIndicador));                            

                            voIndicador.DadosGrafico.AddRange(totaisTrimestres.Values);
                        }

                        dataUltimoAcompanhamento = controlesValidos.Max(i => i.DataIndicador);

                        if (indicador.TipoResultado.Codigo == (int)EnumTipoResultado.Acumulado)
                        {
                            voIndicador.ValorAcumuladoPeriodo = controlesValidos.Sum(i => i.ValorIndicador).ToInt32();
                        }
                        else
                        {
                            voIndicador.ValorAcumuladoPeriodo = (controlesValidos.Sum(i => i.ValorIndicador) / controlesValidos.Count()).ToInt32();
                        }

                        if (voIndicador.ValorAcumuladoPeriodo >= indicador.ValorAlvo)
                        {
                            if (dataUltimoAcompanhamento > indicador.DataAlvo)
                            {
                                voIndicador.Status = "Atingido com atraso";
                                voIndicador.CorStatus = CORSTATUS_AMARELO;
                            }
                            else
                            {
                                voIndicador.Status = "Atingido no prazo";
                                voIndicador.CorStatus = CORSTATUS_VERDE;
                            }
                        }
                        else
                        {
                            if (indicador.DataAlvo < dataUltimoAcompanhamento || indicador.DataAlvo < DateTime.Now)
                            {
                                voIndicador.Status = "Não Atingido - em atraso";
                                voIndicador.CorStatus = CORSTATUS_VERMELHO;
                            }
                            else
                            {
                                voIndicador.Status = "Não Atingido - ainda no prazo";
                                voIndicador.CorStatus = CORSTATUS_ALARANJADO;
                            }
                        }

                        foreach (var c in controlesValidos.Where(w =>
                            w.DataIndicador >= dataInicioPeriodoComentarios &&
                            w.DataIndicador <= dataFinalPeriodo))
                        {
                            if (!String.IsNullOrEmpty(c.DescricaoObservacao))
                                voIndicador.Comentarios.Add(new VOComentario
                                {
                                    Data = c.DataIndicador,
                                    Fato = c.DescricaoObservacao,
                                    Causa = c.DescricaoCausa,
                                    Acao = c.DescricaoAcao
                                });
                        }
                    }
                    else
                    {
                        voIndicador.Status = "NÃO REP.";
                        voIndicador.CorStatus = CORSTATUS_VERMELHO;
                    }

                    decimal valorFinalPeriodo = 0;
                    int trimestreConsiderado = 0;
                    var trimestreReferencia = indicador.MesReferencia.HasValue ? (indicador.MesReferencia.Value.Month + 2) / 3 : 1;
                    if (voIndicador.DadosGrafico.Count > 0)
                    {
                        decimal agrupaValorFinal = 0;
                        foreach (var dadoTrimestre in voIndicador.DadosGrafico)
                        {
                            if (indicador.TipoResultado.Codigo == (int)EnumTipoResultado.Acumulado)
                            {
                                agrupaValorFinal += dadoTrimestre.ValorFinal;

                                dadoTrimestre.SomatorioFinal = agrupaValorFinal;
                            }
                            else
                            {
                                if (dadoTrimestre.NumeroTrimestre >= trimestreReferencia)
                                {
                                    agrupaValorFinal += dadoTrimestre.ValorFinal;
                                    trimestreConsiderado++;
                                    dadoTrimestre.SomatorioFinal = agrupaValorFinal / trimestreConsiderado;
                                }
                                else
                                    dadoTrimestre.SomatorioFinal = 0;
                            }
                        }

                        //caso o codigoPeriodo seja 0, trazer o valor do último trimestre.
                        trimestreConsiderado = codigoPeriodo > 0 ? codigoPeriodo : 4;
                        valorFinalPeriodo = voIndicador.DadosGrafico.Where(i => i.NumeroTrimestre == trimestreConsiderado).FirstOrDefault().ValorFinal;
                    }

                    if (indicador.TipoValor == null || indicador.TipoValor.CodigoSeqTipoValor == (int)EnumTipoValor.Absoluto)
                    {
                        voIndicador.ValorAcumuladoPeriodoFormatado = voIndicador.ValorAcumuladoPeriodo.ToString();
                        voIndicador.ValorAlvoFormatado = String.Format("{0:N2}", voIndicador.ValorAlvo);
                        voIndicador.ValorPeriodoFormatado = String.Format("{0:N2}", valorFinalPeriodo);

                    }
                    else
                    {
                        voIndicador.ValorAcumuladoPeriodoFormatado = voIndicador.ValorAcumuladoPeriodo.ToString() + "%";
                        voIndicador.ValorAlvoFormatado = String.Format("{0:N2}", voIndicador.ValorAlvo) + "%";
                        voIndicador.ValorPeriodoFormatado = String.Format("{0:N2}", valorFinalPeriodo) + "%";
                    }

                    if (item.Indicadores.Count > 1 && indicador != item.Indicadores.LastOrDefault())
                        voIndicador.PularPagina = "pularPagina";

                    metaAcompanhamento.Indicadores.Add(voIndicador);
                }

                metaAcompanhamento.Acoes = new List<VOAcaoAcompanhamento>();

                foreach (var acao in item.Acoes.Where(w =>
                    (w.DataInicioAcao == null || (w.DataInicioAcao >= dataInicioPeriodo && w.DataInicioAcao <= dataFinalPeriodo)) ||
                    (w.DataFimAcao == null || (w.DataFimAcao <= dataFinalPeriodo && w.DataFimAcao >= dataInicioPeriodo))))
                {
                    var voAcao = new VOAcaoAcompanhamento();

                    voAcao.CodigoAcao = acao.CodigoSeqAcao;
                    voAcao.DescricaoAcao = acao.DescricaoAtividadeAcao;
                    voAcao.DataInicio = acao.DataInicioAcao;
                    voAcao.DataFim = acao.DataFimAcao;

                    var controlesValidos = acao.ControlesAcao
                       .Where(w => w.DataReporte >= dataInicioPeriodo &&
                                   w.DataReporte <= dataFinalPeriodo)
                       .ToList();

                    // TODO: Se controlesValidos for vazio?
                    //       - Mas acao.ControlesAcao não for vazio.
                    //       - Quer dizer que somente não há controle para o período
                    //       - Nesse caso devemos considerar o controle do último período?
                    //       - ** Pegar o LastItem de acao.ControlesAcao?

                    ControleAcao ultimoAcompanhamento = controlesValidos.OrderByDescending(i => i.DataReporte).FirstOrDefault();

                    if (ultimoAcompanhamento != null)
                    {
                        voAcao.Percentual = ultimoAcompanhamento.ValorPercentualReporte;

                        if (ultimoAcompanhamento.ValorPercentualReporte >= 100)
                        {
                            if (ultimoAcompanhamento.DataReporte > acao.DataFimAcao)
                            {
                                voAcao.Status = "Concluída com atraso";
                                voAcao.CorStatus = CORSTATUS_AMARELO;
                            }
                            else
                            {
                                voAcao.Status = "Concluída no prazo";
                                voAcao.CorStatus = CORSTATUS_VERDE;
                            }
                        }
                        else
                        {
                            if (DateTime.Today > acao.DataFimAcao)
                            {
                                voAcao.Status = "Não concluída - em atraso";
                                voAcao.CorStatus = CORSTATUS_VERMELHO;
                            }
                            else
                            {
                                voAcao.Status = "Não concluída - ainda no prazo";
                                voAcao.CorStatus = CORSTATUS_ALARANJADO;
                            }
                        }
                    }
                    else
                    {
                        voAcao.Status = "NÃO REP.";
                        voAcao.CorStatus = CORSTATUS_VERMELHO;
                    }

                    foreach (var c in controlesValidos.Where(w =>
                        w.DataReporte >= dataInicioPeriodoComentarios && w.DataReporte <= dataFinalPeriodo)
                        .OrderBy(o => o.DataReporte))
                    {
                        var comentario = new VOComentario();

                        comentario.Data = c.DataReporte;
                        comentario.Fato = !String.IsNullOrEmpty(c.DescricaoObservacao) ? c.DescricaoObservacao : "-";
                        comentario.Causa = !String.IsNullOrEmpty(c.DescricaoCausa) ? c.DescricaoCausa : "-";
                        comentario.Acao = !String.IsNullOrEmpty(c.DescricaoAcao) ? c.DescricaoAcao : "-";

                        voAcao.Comentarios.Add(comentario);
                    }

                    metaAcompanhamento.Acoes.Add(voAcao);
                }

                count++;

                relatorio.Metas.Add(metaAcompanhamento);
            }

            var htmlText = GerarRelatorioAcompanhamentoHTML(relatorio, gerarImagemGrafico);

            var resultBytes = new HTMLToiTextSharpPDFParser(htmlText)
                .ParseToBytes();

            return resultBytes;
        }

        private string GerarRelatorioAcompanhamentoHTML(VORelatorioAcompanhamento relatorio, Func<List<VOCalculoAcumuladoTrimestre>, byte[]> gerarImagemGrafico)
        {
            /* Estrutura do template do relatório
             * ------------------------------------------------
             * <META_ITEM>
             * * |
             *   |-----<CONTRACAPA>
             *   |
             *   |-----<INDICADORES>
             *   |       |--<INDICADOR_ITEM_VAZIO>
             *   |       |--<INDICADOR_ITEM>
             *   |       \--<COMENTARIOS>
             *   |              |---<COMENTARIO_ITEM>
             *   |              \---<COMENTARIO_ITEM_VAZIO>
             *   |
             *   \-----<ACOES>
             *   |       |--<ACAO_ITEM_VAZIO>
             *   |       \--<ACAO_ITEM>
             *   |              |---<ACAO_COMENTARIO_ITEM>
             *   |              \---<ACAO_COMENTARIO_ITEM_VAZIO>
             */
            TextTemplate relatorioTmpl = null;

            using (var templateStream = EmbeddedResourceUtils.LoadResourceFromPath("ReportsHTML/RelatorioDeAcompanhamento.html"))
            {
                relatorioTmpl = new TextTemplate(templateStream)
                    .Apply(relatorio, prefix: "Relatorio");
            }

            var metaItemTmpl = relatorioTmpl.GetSection("META_ITEM");
            var metaItensConteudo = new StringBuilder();

            var listMetas = relatorio.Metas.OrderBy(x => x.NomeArea).ToList();

            string nomeArea = "";

            foreach (VOMetaAcompanhamento meta in listMetas)
            {                
                var metaItemAtualTmpl = metaItemTmpl.Apply(meta, prefix: "Meta");

                //Inclusão de uma contra capa antes de cada área que for inserida no relatório
                if (nomeArea != meta.NomeArea)
                {
                    nomeArea = meta.NomeArea;
                }
                else
                {
                    metaItemAtualTmpl.ReplaceSection("CONTRACAPA", string.Empty);
                }


                // Indicadores
                {
                    
                    var indicadoresTmpl = metaItemAtualTmpl.GetSection("INDICADORES");
                    var indicadorItemVazioTmpl = metaItemAtualTmpl.GetSection("INDICADOR_ITEM_VAZIO");
                    var indicadorItemTmpl = metaItemAtualTmpl.GetSection("INDICADOR_ITEM");
                    var indicadorItensConteudo = new StringBuilder();

                    if (meta.Indicadores.Any())
                    {
                        indicadoresTmpl.ReplaceSection("INDICADOR_ITEM_VAZIO", string.Empty);

                        var indicadorNumero = 1;
                        var totalIndicadores = meta.Indicadores.Count();

                        foreach (VOIndicadorAcompanhamento indicador in meta.Indicadores)
                        {
                            var indicadorItemAtualTmpl = indicadorItemTmpl
                                .Apply(indicador, prefix: "Indicador")

                                // Gráfico
                                .Apply(new
                                {
                                    ImgSource = indicador.DadosGrafico.Any()
                                        ? string.Format("data:image/png;base64,{0}", Convert.ToBase64String(gerarImagemGrafico(indicador.DadosGrafico)))
                                        : string.Empty
                                }, "Grafico")

                                .Apply(new
                                {
                                    IndicadorNumero = indicadorNumero,
                                    TotalIndicadores = totalIndicadores
                                }, prefix: "Indicador");

                            // Comentários
                            var comentariosTmpl = indicadorItemAtualTmpl.GetSection("COMENTARIOS");
                            var comentarioItemVazioTmpl = comentariosTmpl.GetSection("COMENTARIO_ITEM_VAZIO");
                            var comentarioItemTmpl = comentariosTmpl.GetSection("COMENTARIO_ITEM");
                            var comentarioItensConteudo = new StringBuilder();

                            if (indicador.Comentarios.Any())
                            {
                                comentariosTmpl.ReplaceSection("COMENTARIO_ITEM_VAZIO", string.Empty);

                                foreach (var comentario in indicador.Comentarios.OrderBy(o => o.Data))
                                {
                                    var comentarioItemAtualTmpl = comentarioItemTmpl
                                        .Apply(new
                                        {
                                            Data = comentario.Data.ToString("MM/yyyy"),
                                            Fato = comentario.Fato,
                                            Causa = comentario.Causa,
                                            Acao = comentario.Acao
                                        }, prefix: "Comentario");

                                    comentarioItensConteudo.AppendLine(comentarioItemAtualTmpl.ToString());
                                }
                            }

                            comentariosTmpl.ReplaceSection("COMENTARIO_ITEM", comentarioItensConteudo.ToString());
                            indicadorItemAtualTmpl.ReplaceSection("COMENTARIOS", comentariosTmpl.ToString());

                            indicadorItensConteudo.AppendLine(indicadorItemAtualTmpl.ToString());
                            indicadorNumero++;
                        }
                    }

                    // Finalizando os indicadores
                    indicadoresTmpl.ReplaceSection("INDICADOR_ITEM", indicadorItensConteudo.ToString());
                    metaItemAtualTmpl.ReplaceSection("INDICADORES", indicadoresTmpl.ToString());                    
                }

                // Ações
                {
                    var acoesTmpl = metaItemAtualTmpl
                        .GetSection("ACOES")
                        .Apply(new { TotalAcoes = meta.Acoes.Count() }, prefix: "Acoes");

                    var acaoItemVazioTmpl = metaItemAtualTmpl.GetSection("ACAO_ITEM_VAZIO");
                    var acaoItemTmpl = metaItemAtualTmpl.GetSection("ACAO_ITEM");
                    var acaoItensConteudo = new StringBuilder();

                    if (meta.Acoes.Any())
                    {
                        acoesTmpl.ReplaceSection("ACAO_ITEM_VAZIO", string.Empty);

                        foreach (VOAcaoAcompanhamento acao in meta.Acoes)
                        {
                            var acaoItemAtualTmpl = acaoItemTmpl
                                .Apply(acao, prefix: "Acao");

                            // Comentários
                            var comentariosTmpl = acaoItemAtualTmpl.GetSection("ACAO_COMENTARIOS");
                            var comentarioItemVazioTmpl = comentariosTmpl.GetSection("ACAO_COMENTARIO_ITEM_VAZIO");
                            var comentarioItemTmpl = comentariosTmpl.GetSection("ACAO_COMENTARIO_ITEM");
                            var comentarioItensConteudo = new StringBuilder();

                            if (acao.Comentarios.Any())
                            {
                                comentariosTmpl.ReplaceSection("ACAO_COMENTARIO_ITEM_VAZIO", string.Empty);

                                foreach (var comentario in acao.Comentarios.OrderBy(o => o.Data))
                                {
                                    var comentarioItemAtualTmpl = comentarioItemTmpl
                                        .Apply(new
                                        {
                                            Data = comentario.Data.ToString("MM/yyyy"),
                                            Fato = comentario.Fato,
                                            Causa = comentario.Causa,
                                            Acao = comentario.Acao
                                        }, prefix: "Acao");

                                    comentarioItensConteudo.AppendLine(comentarioItemAtualTmpl.ToString());
                                }
                            }

                            comentariosTmpl.ReplaceSection("ACAO_COMENTARIO_ITEM", comentarioItensConteudo.ToString());
                            acaoItemAtualTmpl.ReplaceSection("ACAO_COMENTARIOS", comentariosTmpl.ToString());

                            acaoItensConteudo.AppendLine(acaoItemAtualTmpl.ToString());
                        }
                    }

                    // Finalizando as ações
                    acoesTmpl.ReplaceSection("ACAO_ITEM", acaoItensConteudo.ToString());
                    metaItemAtualTmpl.ReplaceSection("ACOES", acoesTmpl.ToString());
                }

                // Finalizando a meta
                metaItensConteudo.AppendLine(metaItemAtualTmpl.ToString());
            }

            relatorioTmpl.ReplaceSection("META_ITEM", metaItensConteudo.ToString());

            return relatorioTmpl.ToString();
        }
    }
}
