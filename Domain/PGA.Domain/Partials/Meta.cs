﻿using PGA.Common;
using PGA.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Domain.Entities
{
    public partial class Meta
    {
        public virtual StatusMeta StatusMeta
        {
            get
            {
                return this.Situacoes.OrderByDescending(i => i.DataAlteracao).FirstOrDefault().StatusMeta;
            }
        }

        public virtual SituacaoMeta SituacaoMeta
        {
            get
            {
                return this.Situacoes.OrderByDescending(i => i.DataAlteracao).FirstOrDefault();
            }
        }
    }
}
