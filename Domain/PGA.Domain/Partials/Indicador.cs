﻿using PGA.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Domain.Entities
{
    public partial class Indicador
    {
        public override void Delete()
        {
            if (this.Meta.StatusMeta.CodigoSeqStatusMeta == (int)StatusMetaEnum.EmCadastro)
            {
                base.Delete();
            }
            else
                throw new Exception(String.Format("Não é possível excluir o indicador contido em uma meta com o status {0}.", this.Meta.StatusMeta.DescricaoStatus));
        }
    }

}
