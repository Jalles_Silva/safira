﻿using PGA.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Domain.Entities
{
    public partial class ControleIndicador
    {
        public virtual bool UsuarioAdministrador { get; set; }

        public override void Save()
        {
            if (!UsuarioAdministrador)
            {
                if (this.DataIndicador.Month < this.Indicador.Meta.Exercicio.Fase.MesInicio)
                    throw new Exception("Impossível registrar acompanhamento devido à Data de Reporte ser de um período não iniciado.");
            }

            if (this.DataIndicador.Year != this.Indicador.Meta.Exercicio.Ano)
                throw new Exception("Impossível registrar acompanhamento devido à Data de Reporte não fazer parte deste exercício.");

            base.Save();
        }

        public override void Delete()
        {
            var indicador = this.Indicador.ControlesIndicador.Where(i => i.CodigoSeqControleIndicador == this.CodigoSeqControleIndicador).FirstOrDefault();

            //TODO: Remover regra do usúario adm. Incluido para remoçao de dado em prod.
            if (!UsuarioAdministrador && indicador.DataIndicador.Year != this.Indicador.Meta.Exercicio.Ano)
            {
                throw new Exception("Não é possível excluir indicadores de trimestres fechados");
            }

            if (!UsuarioAdministrador && indicador.DataIndicador.Month < this.Indicador.Meta.Exercicio.Fase.MesInicio)
            {
                throw new Exception("Não é possível excluir indicadores de trimestres fechados");
            }

            base.Delete();
        }
    }
}
