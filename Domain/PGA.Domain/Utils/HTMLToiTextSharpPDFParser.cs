﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.html;
using iTextSharp.tool.xml.parser;
using iTextSharp.tool.xml.pipeline.css;
using iTextSharp.tool.xml.pipeline.end;
using iTextSharp.tool.xml.pipeline.html;
using System;
using System.IO;

namespace PGA.Domain.Utils
{
    public class HTMLToiTextSharpPDFParser
    {
        // https://developers.itextpdf.com/examples/xml-worker-itext5/html-images

        private readonly TextReader _input;
        private readonly Base64iTextSharpImageProvider _base64ImageProvider;
        private readonly Rectangle _pageSize;
        private readonly float _margin;

        public HTMLToiTextSharpPDFParser(string input)
            : this(new StringReader(input), PageSize.A4, 40)
        { }

        public HTMLToiTextSharpPDFParser(TextReader input)
            : this(input, PageSize.A4, 40)
        { }

        public HTMLToiTextSharpPDFParser(string input, Rectangle pageSize, float margin)
            : this(new StringReader(input), pageSize, margin)
        { }

        public HTMLToiTextSharpPDFParser(TextReader input, Rectangle pageSize, float margin)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            _input = input;
            _pageSize = pageSize;
            _margin = margin;
            _base64ImageProvider = new Base64iTextSharpImageProvider();
        }

        public byte[] ParseToBytes()
        {
            byte[] result = null;

            using (var output = new MemoryStream())
            using (var document = new Document(_pageSize, _margin, _margin, _margin, _margin))
            using (var writer = PdfWriter.GetInstance(document, output))
            {
                document.Open();
                {
                    var cssResolver = XMLWorkerHelper.GetInstance().GetDefaultCssResolver(true);
                    var htmlContext = new HtmlPipelineContext(null);

                    htmlContext.SetTagFactory(Tags.GetHtmlTagProcessorFactory());
                    htmlContext.SetImageProvider(_base64ImageProvider);

                    // Pipelines
                    var pdf = new PdfWriterPipeline(document, writer);
                    var html = new HtmlPipeline(htmlContext, pdf);
                    var css = new CssResolverPipeline(cssResolver, html);

                    // XML Worker
                    var worker = new XMLWorker(css, true);
                    var parser = new XMLParser(worker);

                    parser.Parse(_input);
                }
                document.Close();

                result = output.ToArray();
            }

            return result;
        }
    }
}
