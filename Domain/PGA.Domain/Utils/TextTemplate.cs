﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PGA.Domain.Utils
{
    public class TextTemplate
    {
        private const string BEGIN_SECTION_TMPL = "<!--[{0}]-->";
        private const string END_SECTION_TMPL = "<!--[/{0}]-->";

        private string _template;

        public TextTemplate(Stream template)
        {
            if (template == null)
            {
                throw new ArgumentNullException(nameof(template));
            }

            using (var reader = new StreamReader(template))
            {
                _template = reader.ReadToEnd();
            }
        }

        public TextTemplate(string template)
        {
            if (template == null)
            {
                throw new ArgumentNullException(nameof(template));
            }

            _template = template;
        }

        public TextTemplate GetSection(string section)
        {
            var beginText = string.Format(BEGIN_SECTION_TMPL, section);
            var endText = string.Format(END_SECTION_TMPL, section);

            var beginIndexOf = _template.IndexOf(beginText);
            var endIndexOf = _template.IndexOf(endText);

            if (beginIndexOf < 0 || endIndexOf < 0 || beginIndexOf > endIndexOf)
            {
                return null;
            }

            beginIndexOf += beginText.Length;

            var sectionText = _template.Substring(beginIndexOf, endIndexOf - beginIndexOf);

            return new TextTemplate(sectionText);
        }

        public TextTemplate ReplaceSection(string section, TextTemplate value)
        {
            return ReplaceSection(section, value.ToString());
        }

        public TextTemplate ReplaceSection(string section, string value)
        {
            var beginText = string.Format(BEGIN_SECTION_TMPL, section);
            var endText = string.Format(END_SECTION_TMPL, section);

            var beginIndexOf = _template.IndexOf(beginText);
            var endIndexOf = _template.IndexOf(endText);

            if (beginIndexOf < 0 || endIndexOf < 0 || beginIndexOf > endIndexOf)
            {
                return this;
            }

            endIndexOf += endText.Length;

            _template = new StringBuilder()
                .Append(_template.Substring(0, beginIndexOf))
                .Append(value)
                .Append(_template.Substring(endIndexOf))
                .ToString();

            return this;
        }

        public override string ToString()
        {
            return _template;
        }

        public TextTemplate Apply(object context, string prefix)
        {
            var newTmpl = ToString(context, prefix);

            return new TextTemplate(newTmpl);
        }

        public string ToString(object context, string prefix)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            var data = ToString();
            var ctxType = context.GetType();

            foreach (var propInfo in ctxType.GetProperties())
            {
                string propTag = "{" + prefix + "." + propInfo.Name + "}";

                if (data.IndexOf(propTag) < 0)
                {
                    continue;
                }

                var propValue = propInfo.GetValue(context, null)?.ToString();

                data = data.Replace(propTag, propValue);
            }

            return data;
        }
    }
}
