﻿using iTextSharp.text;
using System;
using System.IO;

namespace PGA.Domain.Utils
{
    class Base64iTextSharpImageProvider : iTextSharp.tool.xml.pipeline.html.AbstractImageProvider
    {
        public override Image Retrieve(string src)
        {
            var pos = src.IndexOf("base64,");

            try
            {
                if (src.StartsWith("data") && pos > 0)
                {
                    byte[] imgBytes = Convert.FromBase64String(src.Substring(pos + 7));
                    return Image.GetInstance(imgBytes);
                }
                else
                {
                    return Image.GetInstance(src);
                }
            }
            catch (BadElementException)
            {
                return null;
            }
            catch (IOException)
            {
                return null;
            }
        }

        public override string GetImageRootPath()
        {
            return null;
        }
    }
}
