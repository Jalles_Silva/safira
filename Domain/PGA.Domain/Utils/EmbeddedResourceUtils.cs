﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PGA.Domain.Utils
{
    public static class EmbeddedResourceUtils
    {
        public static Stream LoadResourceFromPath(string resourcePath)
        {
            var thisAssembly = typeof(EmbeddedResourceUtils).Assembly;
            var resourceName = string.Format("{0}.{1}",
                thisAssembly.GetName().Name,
                resourcePath.Replace("/", ".")
            );

            return thisAssembly.GetManifestResourceStream(resourceName);
        }
    }
}
