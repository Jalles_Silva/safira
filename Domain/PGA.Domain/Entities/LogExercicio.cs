﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class LogExercicio : DomainBase<LogExercicio, ILogExercicioRepository<LogExercicio>>
    {
        public LogExercicio()
        {
        }

        protected int codigoSeqLogExercicio;
        protected int codigoFase;
        protected short anoExercicio;
        protected string nomeUsuario;
        protected DateTime dataCadastro;

        public virtual int CodigoSeqLogExercicio { get { return codigoSeqLogExercicio; } }
        public virtual int CodigoFase { get { return codigoFase; } set { codigoFase = value; } }
        public virtual short AnoExercicio { get { return anoExercicio; } set { anoExercicio = value; } }
        public virtual string NomeUsuario { get { return nomeUsuario; } set { nomeUsuario = value; } }
        public virtual DateTime DataCadastro { get { return dataCadastro; } set { dataCadastro = value; } }
    }
}