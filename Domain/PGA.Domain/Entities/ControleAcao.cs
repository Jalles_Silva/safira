﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class ControleAcao : DomainBase<ControleAcao, IControleAcaoRepository<ControleAcao>>
    {
        protected ControleAcao()
        {
        }

        public ControleAcao(Acao acao)
        {
            this.SetAcao(acao);
        }

        protected int codigoSeqControleAcao;
        protected DateTime dataReporte;
        protected short valorPercentualReporte;
        protected string descricaoAcao;
        protected string descricaoCausa;
        protected string descricaoObservacao;
        protected string nomeUsuario;
        protected DateTime dataCadastro;
        protected decimal? valorRecursoFinanceiro;

        protected Acao acao;

        public virtual int CodigoSeqControleAcao { get { return codigoSeqControleAcao; } }
        public virtual DateTime DataReporte { get { return dataReporte; } set { dataReporte = value; } }
        public virtual short ValorPercentualReporte { get { return valorPercentualReporte; } set { valorPercentualReporte = value; } }
        public virtual string DescricaoAcao { get { return descricaoAcao; } set { descricaoAcao = value; } }
        public virtual string DescricaoCausa { get { return descricaoCausa; } set { descricaoCausa = value; } }
        public virtual string DescricaoObservacao { get { return descricaoObservacao; } set { descricaoObservacao = value; } }
        public virtual string NomeUsuario { get { return nomeUsuario; } set { nomeUsuario = value; } }
        public virtual DateTime DataCadastro { get { return dataCadastro; } set { dataCadastro = value; } }
        public virtual decimal? ValorRecursoFinanceiro { get { return valorRecursoFinanceiro; } set { valorRecursoFinanceiro = value; } }

        public virtual Acao Acao { get { return acao; } }

        public virtual void SetAcao(Acao acao)
        {
            this.acao = acao;
        }
    }
}