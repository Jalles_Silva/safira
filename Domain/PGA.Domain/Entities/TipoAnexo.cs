﻿using PGA.Integration.Spec;
using SQFramework.Spring.Domain;

namespace PGA.Domain.Entities
{
    public partial class TipoAnexo : DomainBase<TipoAnexo, ITipoAnexoRepository<TipoAnexo>>
    {
        public TipoAnexo()
        {
        }

        protected int cd_Seq_Tipo_Anexo;
        protected string ds_Tipo_Anexo;

        public virtual int Cd_Seq_Tipo_Anexo { get { return cd_Seq_Tipo_Anexo; } set { cd_Seq_Tipo_Anexo = value; } }
        public virtual string Ds_Tipo_Anexo { get { return ds_Tipo_Anexo; } set { ds_Tipo_Anexo = value; } }
    }
}
