﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class Instrumento : DomainBase<Instrumento, IInstrumentoRepository<Instrumento>>
    {
        public Instrumento()
        {
        }

        protected int codigoSeqInstrumento;
        protected string descricaoInstrumento;
        protected bool ativo;

        protected Objetivo objetivo;
        protected Objetivo objetivoEixoTematico;
        protected CorporativoUnidade corporativoUnidade;
        protected TipoInstrumento tipoInstrumento;

        public virtual int CodigoSeqInstrumento { get { return codigoSeqInstrumento; } }
        public virtual string DescricaoInstrumento { get { return descricaoInstrumento; } set { descricaoInstrumento = value; } }
        public virtual bool Ativo { get { return ativo; } set { ativo = value; } }

        public virtual Objetivo Objetivo { get { return objetivo; } set { objetivo = value; } }
        public virtual Objetivo ObjetivoEixoTematico { get { return objetivoEixoTematico; } set { objetivoEixoTematico = value; } }
        public virtual CorporativoUnidade CorporativoUnidade { get { return corporativoUnidade; } set { corporativoUnidade = value; } }
        public virtual TipoInstrumento TipoInstrumento { get { return tipoInstrumento; } set { tipoInstrumento = value; } }
    }
}
