﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class StatusEncaminhamento : DomainBase<StatusEncaminhamento, IStatusEncaminhamentoRepository<StatusEncaminhamento>>
    {
        public StatusEncaminhamento()
        {
        }

        protected int codigoSeqStatusEncaminhamento;
        protected string descricaoStatusEncaminhamento;

        public virtual int CodigoSeqStatusEncaminhamento { get { return codigoSeqStatusEncaminhamento; } }
        public virtual string DescricaoStatusEncaminhamento { get { return descricaoStatusEncaminhamento; } set { descricaoStatusEncaminhamento = value; } }
    }
}
