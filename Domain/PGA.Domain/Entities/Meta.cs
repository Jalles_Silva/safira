﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class Meta : DomainBase<Meta, IMetaRepository<Meta>>
    {
        protected Meta()
        {
        }

        public Meta(TipoMeta tipoMeta)
        {
            this.SetTipoMeta(tipoMeta);
        }

        protected int codSeqMeta;
        protected string descricaoMeta;
        protected string descricaoJustificativa;
        protected string descricaoResultadoEsperado;
        protected string descricaoPremissa;
        protected string descricaoRestricao;
        protected string noCoordenacao;
        protected bool? statusIniciativaEstrategica;
        protected string descricaoOutros;
        protected bool statusMetaInterna;

        protected string descricaoSociedadeUsuarioBeneficio;
        protected string descricaoSociedadeUsuarioImpacto;
        protected string descricaoInstitucionalBeneficio;
        protected string descricaoInstitucionalImpacto;
        protected string descricaoGovernoBeneficio;
        protected string descricaoGovernoImpacto;
        protected string descricaoSetorReguladoBeneficio;
        protected string descricaoSetorReguladoImpacto;

        protected Iniciativa iniciativaPe;
        protected Iniciativa iniciativaPpa;
        protected Iniciativa iniciativaMissao;
        protected TipoMeta tipoMeta;
        protected CorporativoUnidade corporativoUnidade;
        protected Exercicio exercicio;
        protected Instrumento gestaoDeRiscos;
        protected Instrumento agendaRegulatoria;
        protected Instrumento desburocratizacao;
        protected Instrumento integridade;

        protected IList<Acao> acoes;
        protected IList<Indicador> indicadores;
        protected IList<SituacaoMeta> situacoes;

        public virtual int CodSeqMeta { get { return codSeqMeta; } }
        public virtual string DescricaoMeta { get { return descricaoMeta; } set { descricaoMeta = value; } }
        public virtual string DescricaoJustificativa { get { return descricaoJustificativa; } set { descricaoJustificativa = value; } }
        public virtual string DescricaoResultadoEsperado { get { return descricaoResultadoEsperado; } set { descricaoResultadoEsperado = value; } }
        public virtual string DescricaoPremissa { get { return descricaoPremissa; } set { descricaoPremissa = value; } }
        public virtual string DescricaoRestricao { get { return descricaoRestricao; } set { descricaoRestricao = value; } }
        public virtual string NoCoordenacao { get { return noCoordenacao; } set { noCoordenacao = value; } }
        public virtual bool? StatusIniciativaEstrategica { get { return statusIniciativaEstrategica; } set { statusIniciativaEstrategica = value; } }
        public virtual string DescricaoOutros { get { return descricaoOutros; } set { descricaoOutros = value; } }


        public virtual string DescricaoSociedadeUsuarioBeneficio { get { return descricaoSociedadeUsuarioBeneficio; } set { descricaoSociedadeUsuarioBeneficio = value; } }
        public virtual string DescricaoSociedadeUsuarioImpacto { get { return descricaoSociedadeUsuarioImpacto; } set { descricaoSociedadeUsuarioImpacto = value; } }
        
        public virtual string DescricaoInstitucionalBeneficio { get { return descricaoInstitucionalBeneficio; } set { descricaoInstitucionalBeneficio = value; } }
        public virtual string DescricaoInstitucionalImpacto { get { return descricaoInstitucionalImpacto; } set { descricaoInstitucionalImpacto = value; } }

        public virtual string DescricaoGovernoBeneficio { get { return descricaoGovernoBeneficio; } set { descricaoGovernoBeneficio = value; } }
        public virtual string DescricaoGovernoImpacto { get { return descricaoGovernoImpacto; } set { descricaoGovernoImpacto = value; } }

        public virtual string DescricaoSetorReguladoBeneficio { get { return descricaoSetorReguladoBeneficio; } set { descricaoSetorReguladoBeneficio = value; } }
        public virtual string DescricaoSetorReguladoImpacto { get { return descricaoSetorReguladoImpacto; } set { descricaoSetorReguladoImpacto = value; } }


        public virtual Iniciativa IniciativaPe { get { return iniciativaPe; } set { iniciativaPe = value; } }
        public virtual Iniciativa IniciativaPpa { get { return iniciativaPpa; } set { iniciativaPpa = value; } }
        public virtual Iniciativa IniciativaMissao { get { return iniciativaMissao; } set { iniciativaMissao = value; } }
        public virtual TipoMeta TipoMeta { get { return tipoMeta; } }
        public virtual CorporativoUnidade CorporativoUnidade { get { return corporativoUnidade; } }
        public virtual Exercicio Exercicio { get { return exercicio; } }
        public virtual Instrumento GestaoDeRiscos { get { return gestaoDeRiscos; } set { gestaoDeRiscos = value; } }
        public virtual Instrumento AgendaRegulatoria { get { return agendaRegulatoria; } set { agendaRegulatoria = value; } }
        public virtual Instrumento Desburocratizacao { get { return desburocratizacao; } set { desburocratizacao = value; } }
        public virtual Instrumento Integridade { get { return integridade; } set { integridade = value; } }

        public virtual IList<Acao> Acoes { get { return (acoes ?? (acoes = new List<Acao>())); } }
        public virtual IList<Indicador> Indicadores { get { return (indicadores ?? (indicadores = new List<Indicador>())); } }
        public virtual IList<SituacaoMeta> Situacoes { get { return (situacoes ?? (situacoes = new List<SituacaoMeta>())); } }
        public virtual bool StatusMetaInterna  { get { return statusMetaInterna; } set { statusMetaInterna = value; } }

        public virtual void SetTipoMeta(TipoMeta tipoMeta)
        {
            this.tipoMeta = tipoMeta;
        }

        public virtual void SetCorporativoUnidade(CorporativoUnidade corporativoUnidade)
        {
            this.corporativoUnidade = corporativoUnidade;
        }

        public virtual void SetExercicio(Exercicio exercicio)
        {
            this.exercicio = exercicio;
        }
    }
}