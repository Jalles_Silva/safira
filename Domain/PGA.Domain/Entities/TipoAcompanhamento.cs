﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class TipoAcompanhamento : DomainBase<TipoAcompanhamento, ITipoAcompanhamentoRepository<TipoAcompanhamento>>
    {
        public TipoAcompanhamento()
        {
        }

        protected int codigoSeqTipoAcompanhamento;
        protected string descricaoTipoAcompanhamento;
        protected bool ativo;

        public virtual int CodigoSeqTipoAcompanhamento { get { return codigoSeqTipoAcompanhamento; } }
        public virtual string DescricaoTipoAcompanhamento { get { return descricaoTipoAcompanhamento; } set { descricaoTipoAcompanhamento = value; } }
        public virtual bool Ativo { get { return ativo; } set { ativo = value; } }
    }
}
