﻿using PGA.Integration.Spec;
using SQFramework.Spring.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Domain.Entities
{
    public partial class Processo : DomainBase<Processo, IProcessoRepository<Processo>>
    {
        public Processo()
        {
        }

        protected int cd_Seq_Processo;
        protected int cd_Corporativo_Unidade;
        protected int? cd_Objetivo;
        protected string no_Processo;
        protected string ds_Processo_Sei;
        protected string no_Usuario_Criacao;
        protected string no_Usuario_Atualizacao;
        protected string ds_Motivo_Inativacao;
        protected bool st_Ativo;
        protected bool? st_Excluido;
        protected DateTime dt_Criacao;
        protected DateTime dt_Atualizacao;

        public virtual int Cd_Seq_Processo { get { return cd_Seq_Processo; } set { cd_Seq_Processo = value; } }
        public virtual int Cd_Corporativo_Unidade { get { return cd_Corporativo_Unidade; } set { cd_Corporativo_Unidade = value; } }
        public virtual int? Cd_Objetivo { get { return cd_Objetivo; } set { cd_Objetivo = value; } }
        public virtual string No_Processo { get { return no_Processo; } set { no_Processo = value; } }
        public virtual string Ds_Processo_Sei { get { return ds_Processo_Sei; } set { ds_Processo_Sei = value; } }
        public virtual string No_Usuario_Criacao { get { return no_Usuario_Criacao; } set { no_Usuario_Criacao = value; } }
        public virtual DateTime Dt_Criacao { get { return dt_Criacao; } set { dt_Criacao = value; } }
        public virtual string No_Usuario_Atualizacao { get { return no_Usuario_Atualizacao; } set { no_Usuario_Atualizacao = value; } }
        public virtual DateTime Dt_Atualizacao { get { return dt_Atualizacao; } set { dt_Atualizacao = value; } }
        public virtual bool St_Ativo { get { return st_Ativo; } set { st_Ativo = value; } }
        public virtual string Ds_Motivo_Inativacao { get { return ds_Motivo_Inativacao; } set { ds_Motivo_Inativacao = value; } }
        public virtual bool? St_Excluido { get { return st_Excluido; } set { st_Excluido = value; } }
    }
}
