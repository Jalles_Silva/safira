﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class TipoResultado : DomainBase<TipoResultado, ITipoResultadoRepository<TipoResultado>>
    {
        public TipoResultado()
        {
        }

        protected int codigo;
        protected string descricao;

        public virtual int Codigo { get { return codigo; } }
        public virtual string Descricao { get { return descricao; } set { descricao = value; } }

    }
}