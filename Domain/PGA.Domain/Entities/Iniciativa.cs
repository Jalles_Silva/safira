﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class Iniciativa : DomainBase<Iniciativa, IIniciativaRepository<Iniciativa>>
    {
        public Iniciativa()
        {
        }

        protected int codigoSeqIniciativa;
        protected int codigoObjetivo;
        protected string descricaoIniciativa;
        protected string programaTematico;
        protected string tpIniciativaProjeto;
        protected bool ativo;

        protected CorporativoUnidade corporativoUnidade;
        protected TipoIniciativa tipoIniciativa;
        protected Anexo anexo;

        public virtual int CodigoSeqIniciativa { get { return codigoSeqIniciativa; } set { codigoSeqIniciativa = value; } }
        public virtual int CodigoObjetivo { get { return codigoObjetivo; } set { codigoObjetivo = value; } }
        public virtual string DescricaoIniciativa { get { return descricaoIniciativa; } set { descricaoIniciativa = value; } }
        public virtual string ProgramaTematico { get { return programaTematico; } set { programaTematico = value; } }
        public virtual string TpIniciativaProjeto { get { return tpIniciativaProjeto; } set { tpIniciativaProjeto = value; } }
        public virtual Anexo Anexo { get { return anexo; } set { anexo = value; } }
        public virtual bool Ativo { get { return ativo; } set { ativo = value; } }

        public virtual CorporativoUnidade CorporativoUnidade { get { return corporativoUnidade; } set { corporativoUnidade = value; } }
        public virtual TipoIniciativa TipoIniciativa { get { return tipoIniciativa; } set { tipoIniciativa = value; } }
    }
}