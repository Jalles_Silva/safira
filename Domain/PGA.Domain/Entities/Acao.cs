﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class Acao : DomainBase<Acao, IAcaoRepository<Acao>>
    {
        protected Acao()
        {
        }

        public Acao(Meta meta)
        {
            this.SetMeta(meta);
        }

        protected int codigoSeqAcao;
        protected string descricaoAtividadeAcao;
        protected string descricaoEstrategiaAcao;
        protected string descricaoResponsavel;
        protected DateTime? dataInicioAcao;
        protected DateTime? dataFimAcao;
        protected int peso;

        protected int? statusAcao;
        protected int? codigoSeqAnexo;
        protected string descricaoJustificativaCancelamento;

        protected Meta meta;
        protected Anexo anexo;

        protected IList<Recurso> recursos;
        protected IList<ControleAcao> controlesAcao;

        public virtual int CodigoSeqAcao { get { return codigoSeqAcao; } }
        public virtual string DescricaoAtividadeAcao { get { return descricaoAtividadeAcao; } set { descricaoAtividadeAcao = value; } }
        public virtual string DescricaoEstrategiaAcao { get { return descricaoEstrategiaAcao; } set { descricaoEstrategiaAcao = value; } }
        public virtual string DescricaoResponsavel { get { return descricaoResponsavel; } set { descricaoResponsavel = value; } }
        public virtual DateTime? DataInicioAcao { get { return dataInicioAcao; } set { dataInicioAcao = value; } }
        public virtual DateTime? DataFimAcao { get { return dataFimAcao; } set { dataFimAcao = value; } }
        public virtual int Peso { get { return peso; } set { peso = value; } }

        public virtual int? StatusAcao { get { return statusAcao; } set { statusAcao = value; } }
        public virtual int? CodigoSeqAnexo { get { return codigoSeqAnexo; } set { codigoSeqAnexo = value; } }
        public virtual string DescricaoJustificativaCancelamento { get { return descricaoJustificativaCancelamento; } set { descricaoJustificativaCancelamento = value; } }

        public virtual Meta Meta { get { return meta; } }
        public virtual Anexo Anexo { get { return anexo; } }

        public virtual IList<Recurso> Recursos { get { return (recursos ?? (recursos = new List<Recurso>())); } }
        public virtual IList<ControleAcao> ControlesAcao { get { return (controlesAcao ?? (controlesAcao = new List<ControleAcao>())); } }

        public virtual void SetMeta(Meta meta)
        {
            this.meta = meta;
        }
        public virtual void SetAnexo(Anexo anexo)
        {
            this.anexo = anexo;
        }
    }
}