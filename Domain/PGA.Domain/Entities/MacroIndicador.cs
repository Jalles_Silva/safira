﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class MacroIndicador : DomainBase<MacroIndicador, IMacroIndicadorRepository<MacroIndicador>>
    {
        public MacroIndicador()
        {
        }

        protected int codigoSeqMacroInd;
        protected string descricaoMacroInd;

        protected CorporativoUnidade corporativoUnidade;
        protected Objetivo objetivo;

        public virtual int CodigoSeqMacroInd { get { return codigoSeqMacroInd; } }
        public virtual string DescricaoMacroInd { get { return descricaoMacroInd; } set { descricaoMacroInd = value; } }

        public virtual CorporativoUnidade CorporativoUnidade { get { return corporativoUnidade; } }
        public virtual Objetivo Objetivo { get { return objetivo; } }

        public virtual void SetCorporativoUnidade(CorporativoUnidade corporativoUnidade)
        {
            this.corporativoUnidade = corporativoUnidade;
        }

        public virtual void SetObjetivo(Objetivo objetivo)
        {
            this.objetivo = objetivo;
        }

        public override void Save()
        {
            if (CodigoSeqMacroInd <= 0)
            {
                //TODO: Implementar regra para geração da chave primária
                throw new NotImplementedException();
            }

            base.Save();
        }
    }
}