﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class IniciativaObjetivo : DomainBase<IniciativaObjetivo, IIniciativaObjetivoRepository<IniciativaObjetivo>>
    {
        public IniciativaObjetivo()
        {
        }

        protected int codigoSeqIniciativaObjetivo;
        protected decimal percentualParticipacao;

        protected Iniciativa iniciativa;
        protected Objetivo objetivo;

        public virtual int CodigoSeqIniciativaObjetivo { get { return codigoSeqIniciativaObjetivo; } }
        public virtual decimal PercentualParticipacao { get { return percentualParticipacao; } set { percentualParticipacao = value; } }

        public virtual Iniciativa Iniciativa { get { return iniciativa; } set { iniciativa = value; } }
        public virtual Objetivo Objetivo { get { return objetivo; } set { objetivo = value; } }
    }
}
