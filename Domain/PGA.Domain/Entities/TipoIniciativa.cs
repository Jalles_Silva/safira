﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class TipoIniciativa : DomainBase<TipoIniciativa, ITipoIniciativaRepository<TipoIniciativa>>
    {
        public TipoIniciativa()
        {
        }

        protected int codigoSeqTipoIniciativa;
        protected string descricaoTipoIniciativa;

        public virtual int CodigoSeqTipoIniciativa { get { return codigoSeqTipoIniciativa; } }
        public virtual string DescricaoTipoIniciativa { get { return descricaoTipoIniciativa; } set { descricaoTipoIniciativa = value; } }
    }
}
