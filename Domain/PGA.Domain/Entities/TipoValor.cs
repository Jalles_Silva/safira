﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class TipoValor : DomainBase<TipoValor, ITipoValorRepository<TipoValor>>
    {
        public TipoValor()
        {
        }

        protected int codigoSeqTipoValor;
        protected string descricaoTipo;

        protected IList<Indicador> indicadores;

        public virtual int CodigoSeqTipoValor { get { return codigoSeqTipoValor; } }
        public virtual string DescricaoTipo { get { return descricaoTipo; } set { descricaoTipo = value; } }

        public virtual IList<Indicador> Indicadores { get { return (indicadores ?? (indicadores = new List<Indicador>())); } }
    }
}