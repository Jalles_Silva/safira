﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class Acompanhamento : DomainBase<Acompanhamento, IAcompanhamentoRepository<Acompanhamento>>
    {
        public Acompanhamento()
        {
        }

        protected int codigoSeqAcompanhamento;
        protected TipoAcompanhamento tipoAcompanhamento;
        protected StatusAcompanhamento statusAcompanhamento;
        protected Exercicio exercicio;
        protected Anexo anexo;
        protected DateTime dataAcompanhamento;

        public virtual int CodigoSeqAcompanhamento { get { return codigoSeqAcompanhamento; } }
        public virtual TipoAcompanhamento TipoAcompanhamento { get { return tipoAcompanhamento; } set { tipoAcompanhamento = value; } }
        public virtual StatusAcompanhamento StatusAcompanhamento { get { return statusAcompanhamento; } set { statusAcompanhamento = value; } }
        public virtual Exercicio Exercicio { get { return exercicio; } set { exercicio = value; } }
        public virtual Anexo Anexo { get { return anexo; } set { anexo = value; } }
        public virtual DateTime DataAcompanhamento { get { return dataAcompanhamento; } set { dataAcompanhamento = value; } }
    }
}
