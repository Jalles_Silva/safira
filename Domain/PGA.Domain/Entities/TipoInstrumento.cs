﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class TipoInstrumento : DomainBase<TipoInstrumento, ITipoInstrumentoRepository<TipoInstrumento>>
    {
        public TipoInstrumento()
        {
        }

        protected int codigoSeqTipoInstrumento;
        protected string descricaoTipoInstrumento;

        public virtual int CodigoSeqTipoInstrumento { get { return codigoSeqTipoInstrumento; } }
        public virtual string DescricaoTipoInstrumento { get { return descricaoTipoInstrumento; } set { descricaoTipoInstrumento = value; } }
    }
}
