﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class CorporativoUnidade : DomainBase<CorporativoUnidade, ICorporativoUnidadeRepository<CorporativoUnidade>>
    {
        public CorporativoUnidade()
        {
        }

        public CorporativoUnidade(int codigo)
        {
            codigoUnidade = codigo;
        }

        protected int codigoUnidade;
        protected int? codigoUnidadePai;
        protected string descricaoUnidade;
        protected bool ativo;
        protected IList<Meta> metas;

        public virtual int CodigoUnidade { get { return codigoUnidade; } }
        public virtual int? CodigoUnidadePai { get { return codigoUnidadePai; } set { codigoUnidadePai = value; } }
        public virtual string DescricaoUnidade { get { return descricaoUnidade; } set { descricaoUnidade = value; } }
        public virtual bool Ativo { get { return ativo; } set { ativo = value; } }
        public virtual IList<Meta> Metas { get { return (metas ?? (metas = new List<Meta>())); } }

        public override void Save()
        {
            if (CodigoUnidade <= 0)
            {
                //TODO: Implementar regra para geração da chave primária
                throw new NotImplementedException();
            }

            base.Save();
        }
    }
}