﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class Recurso : DomainBase<Recurso, IRecursoRepository<Recurso>>
    {
        public Recurso()
        {
        }

        protected int codigoSeqRecurso;
        protected string descricaoRecurso;
        protected decimal quantidadeRecurso;

        protected TipoRecurso tipoRecurso;
        protected Acao acao;

        public virtual int CodigoSeqRecurso { get { return codigoSeqRecurso; } }
        public virtual string DescricaoRecurso { get { return descricaoRecurso; } set { descricaoRecurso = value; } }
        public virtual decimal QuantidadeRecurso { get { return quantidadeRecurso; } set { quantidadeRecurso = value; } }

        public virtual TipoRecurso TipoRecurso { get { return tipoRecurso; } }
        public virtual Acao Acao { get { return acao; } }

        public virtual void SetTipoRecurso(TipoRecurso tipoRecurso)
        {
            this.tipoRecurso = tipoRecurso;
        }

        public virtual void SetAcao(Acao acao)
        {
            this.acao = acao;
        }
    }
}