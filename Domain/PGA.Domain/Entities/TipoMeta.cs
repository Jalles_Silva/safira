﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class TipoMeta : DomainBase<TipoMeta, ITipoMetaRepository<TipoMeta>>
    {
        public TipoMeta()
        {
        }

        protected int codigoSeqTipoMeta;
        protected string descricaoTipoMeta;

        protected IList<Meta> metas;

        public virtual int CodigoSeqTipoMeta { get { return codigoSeqTipoMeta; } }
        public virtual string DescricaoTipoMeta { get { return descricaoTipoMeta; } set { descricaoTipoMeta = value; } }

        public virtual IList<Meta> Metas { get { return (metas ?? (metas = new List<Meta>())); } }

        public override void Save()
        {
            if (CodigoSeqTipoMeta <= 0)
            {
                //TODO: Implementar regra para geração da chave primária
                throw new NotImplementedException();
            }

            base.Save();
        }
    }
}