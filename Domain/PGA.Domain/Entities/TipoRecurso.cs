﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class TipoRecurso : DomainBase<TipoRecurso, ITipoRecursoRepository<TipoRecurso>>
    {
        public TipoRecurso()
        {
        }

        protected int codigoSeqTipoRecurso;
        protected string descricaoTipoRecurso;

        protected IList<Recurso> recursos;

        public virtual int CodigoSeqTipoRecurso { get { return codigoSeqTipoRecurso; } }
        public virtual string DescricaoTipoRecurso { get { return descricaoTipoRecurso; } set { descricaoTipoRecurso = value; } }

        public virtual IList<Recurso> Recursos { get { return (recursos ?? (recursos = new List<Recurso>())); } }

        public override void Save()
        {
            if (CodigoSeqTipoRecurso <= 0)
            {
                //TODO: Implementar regra para geração da chave primária
                throw new NotImplementedException();
            }

            base.Save();
        }
    }
}