﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class TipoObjetivo : DomainBase<TipoObjetivo, ITipoObjetivoRepository<TipoObjetivo>>
    {
        public TipoObjetivo()
        {
        }

        protected int codigoSeqTipoObjetivo;
        protected string descricaoTipoObjetivo;

        public virtual int CodigoSeqTipoObjetivo { get { return codigoSeqTipoObjetivo; } }
        public virtual string DescricaoTipoObjetivo { get { return descricaoTipoObjetivo; } set { descricaoTipoObjetivo = value; } }
    }
}
