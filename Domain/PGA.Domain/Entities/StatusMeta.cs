﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class StatusMeta : DomainBase<StatusMeta, IStatusMetaRepository<StatusMeta>>
    {
        public StatusMeta()
        {
        }

        protected int codigoSeqStatusMeta;
        protected string descricaoStatus;

        protected IList<SituacaoMeta> situacaoMetas;

        public virtual int CodigoSeqStatusMeta { get { return codigoSeqStatusMeta; } }
        public virtual string DescricaoStatus { get { return descricaoStatus; } set { descricaoStatus = value; } }

        public virtual IList<SituacaoMeta> SituacaoMetas { get { return (situacaoMetas ?? (situacaoMetas = new List<SituacaoMeta>())); } }
    }
}