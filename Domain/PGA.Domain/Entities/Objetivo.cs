﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class Objetivo : DomainBase<Objetivo, IObjetivoRepository<Objetivo>>
    {
        public Objetivo()
        {
        }

        protected int codigoSeqObjetivo;
        protected string nuObjetivo;
        protected string objetivo;
        protected string descricaoObjetivo;
        protected bool ativo;

        protected Tema tema;
        protected TipoObjetivo tipoObjetivo;

        public virtual int CodigoSeqObjetivo { get { return codigoSeqObjetivo; } set { codigoSeqObjetivo = value; } }
        public virtual string NuObjetivo { get { return nuObjetivo; } set { nuObjetivo = value; } }
        public virtual string NomeObjetivo { get { return objetivo; } set { objetivo = value; } }
        public virtual string DescricaoObjetivo { get { return descricaoObjetivo; } set { descricaoObjetivo = value; } }
        public virtual bool Ativo { get { return ativo; } set { ativo = value; } }

        public virtual Tema Tema { get { return tema; } set { tema = value; } }
        public virtual TipoObjetivo TipoObjetivo { get { return tipoObjetivo; } set { tipoObjetivo = value; } }
    }
}