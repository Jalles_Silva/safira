﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class StatusIndicador : DomainBase<StatusIndicador, IStatusIndicadorRepository<StatusIndicador>>
    {
        public StatusIndicador()
        {
        }

        protected int codigoSeqStatusIndicador;
        protected string descricaoStatusIndicador;
        protected bool statusAtivo;

        public virtual int CodigoSeqStatusIndicador { get { return codigoSeqStatusIndicador; } }
        public virtual string DescricaoStatusIndicador { get { return descricaoStatusIndicador; } set { descricaoStatusIndicador = value; } }
        public virtual bool StatusAtivo { get { return statusAtivo; } set { statusAtivo = value; } }
    }
}