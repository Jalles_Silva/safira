﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class Encaminhamento : DomainBase<Encaminhamento, IEncaminhamentoRepository<Encaminhamento>>
    {
        public Encaminhamento()
        {
        }

        protected int codigoSeqEncaminhamento;
        protected Acompanhamento acompanhamento;
        protected CorporativoUnidade corporativoUnidade;
        protected StatusEncaminhamento statusEncaminhamento;
        protected Meta meta;
        protected string descricaoEncaminhamento;
        protected DateTime prazoAtendimento;

        public virtual int CodigoSeqEncaminhamento { get { return codigoSeqEncaminhamento; } }
        public virtual Acompanhamento Acompanhamento { get { return acompanhamento; } set { acompanhamento = value; } }
        public virtual CorporativoUnidade CorporativoUnidade { get { return corporativoUnidade; } set { corporativoUnidade = value; } }
        public virtual StatusEncaminhamento StatusEncaminhamento { get { return statusEncaminhamento; } set { statusEncaminhamento = value; } }
        public virtual Meta Meta { get { return meta; } set { meta = value; } }
        public virtual string DescricaoEncaminhamento { get { return descricaoEncaminhamento; } set { descricaoEncaminhamento = value; } }
        public virtual DateTime PrazoAtendimento { get { return prazoAtendimento; } set { prazoAtendimento = value; } }
    }
}
