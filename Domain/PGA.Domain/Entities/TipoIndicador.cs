﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class TipoIndicador : DomainBase<TipoIndicador, ITipoIndicadorRepository<TipoIndicador>>
    {
        public TipoIndicador()
        {
        }

        protected int codigoSeqTipoIndicador;
        protected string descricaoTipoIndicador;

        protected IList<Indicador> indicadores;

        public virtual int CodigoSeqTipoIndicador { get { return codigoSeqTipoIndicador; } }
        public virtual string DescricaoTipoIndicador { get { return descricaoTipoIndicador; } set { descricaoTipoIndicador = value; } }

        public virtual IList<Indicador> Indicadores { get { return (indicadores ?? (indicadores = new List<Indicador>())); } }

        public override void Save()
        {
            if (CodigoSeqTipoIndicador <= 0)
            {
                //TODO: Implementar regra para geração da chave primária
                throw new NotImplementedException();
            }

            base.Save();
        }
    }
}