﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class StatusAcao : DomainBase<StatusAcao, IStatusAcaoRepository<StatusAcao>>
    {
        public StatusAcao()
        {
        }

        protected int codigoSeqStatusAcao;
        protected string descricaoStatusAcao;
        protected bool statusAtivo; 

        public virtual int CodigoSeqStatusAcao { get { return codigoSeqStatusAcao; } }
        public virtual string DescricaoStatusAcao { get { return descricaoStatusAcao; } set { descricaoStatusAcao = value; } }

        public virtual bool StatusAtivo { get { return statusAtivo; } set { statusAtivo = value; } }

    }
}