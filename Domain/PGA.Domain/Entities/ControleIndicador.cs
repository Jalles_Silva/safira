﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class ControleIndicador : DomainBase<ControleIndicador, IControleIndicadorRepository<ControleIndicador>>
    {
        protected ControleIndicador()
        {
        }

        public ControleIndicador(Indicador indicador)
        {
            this.SetIndicador(indicador);
        }

        protected int codigoSeqControleIndicador;
        protected decimal valorIndicador;
        protected DateTime dataIndicador;
        protected string descricaoAcao;
        protected string descricaoCausa;
        protected string descricaoObservacao;
        protected string nomeUsuario;
        protected DateTime dataCadastro;

        protected Indicador indicador;

        public virtual int CodigoSeqControleIndicador { get { return codigoSeqControleIndicador; } }
        public virtual decimal ValorIndicador { get { return valorIndicador; } set { valorIndicador = value; } }
        public virtual DateTime DataIndicador { get { return dataIndicador; } set { dataIndicador = value; } }
        public virtual string DescricaoAcao { get { return descricaoAcao; } set { descricaoAcao = value; } }
        public virtual string DescricaoCausa { get { return descricaoCausa; } set { descricaoCausa = value; } }
        public virtual string DescricaoObservacao { get { return descricaoObservacao; } set { descricaoObservacao = value; } }
        public virtual string NomeUsuario { get { return nomeUsuario; } set { nomeUsuario = value; } }
        public virtual DateTime DataCadastro { get { return dataCadastro; } set { dataCadastro = value; } }

        public virtual Indicador Indicador { get { return indicador; } }

        public virtual void SetIndicador(Indicador indicador)
        {
            this.indicador = indicador;
        }
    }
}