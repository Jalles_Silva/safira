﻿using System;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class RelatorioConsubstanciado : DomainBase<RelatorioConsubstanciado, IRelatorioConsubstanciadoRepository<RelatorioConsubstanciado>>
    {
        public RelatorioConsubstanciado()
        {

        }

        protected int codigoSeqRelatorioConsubstanciado;
        protected string descricaoRelatorioConsubstanciado;
        protected bool ativo;
        protected DateTime dataInclusao;

        protected Exercicio exercicio;
        protected TipoTexto tipoTexto;

        public virtual int CodigoSeqRelatorioConsubstanciado { get { return codigoSeqRelatorioConsubstanciado; }  set { codigoSeqRelatorioConsubstanciado = value; } }
        public virtual string DescricaoRelatorioConsubstanciado { get { return descricaoRelatorioConsubstanciado; } set { descricaoRelatorioConsubstanciado = value; } }
        public virtual bool Ativo { get { return ativo; } set { ativo = value; } }
        public virtual DateTime DataInclusao { get { return dataInclusao; } set { dataInclusao = value; } }

        public virtual Exercicio Exercicio { get { return exercicio; } set { exercicio = value; } }
        public virtual TipoTexto TipoTexto { get { return tipoTexto; } set { tipoTexto = value; } }
    }
}