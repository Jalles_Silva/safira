﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class Perspectiva : DomainBase<Perspectiva, IPerspectivaRepository<Perspectiva>>
    {
        public Perspectiva()
        {
        }

        protected int codigoSeqPerspectiva;
        protected string nuPerspectiva;
        protected string descricaoPerspectiva;
        protected bool ativo;

        public virtual int CodigoSeqPerspectiva { get { return codigoSeqPerspectiva; } set { codigoSeqPerspectiva = value; } }
        public virtual string NuPerspectiva { get { return nuPerspectiva; } set { nuPerspectiva = value; } }
        public virtual string DescricaoPerspectiva { get { return descricaoPerspectiva; } set { descricaoPerspectiva = value; } }
        public virtual bool Ativo { get { return ativo; } set { ativo = value; } }
    }
}