﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class Fase : DomainBase<Fase, IFaseRepository<Fase>>
    {
        public Fase()
        {
        }

        protected int codigoSeqFase;
        protected short ordem;
        protected string descricaoFase;
        protected int mesInicio;
        protected int mesFim;

        protected IList<Exercicio> exercicios;

        public virtual int CodigoSeqFase { get { return codigoSeqFase; } }
        public virtual short Ordem { get { return ordem; } set { ordem = value; } }
        public virtual string DescricaoFase { get { return descricaoFase; } set { descricaoFase = value; } }
        public virtual int MesInicio { get { return mesInicio; } set { mesInicio = value; } }
        public virtual int MesFim { get { return mesFim; } set { mesFim = value; } }

        public virtual IList<Exercicio> Exercicios { get { return (exercicios ?? (exercicios = new List<Exercicio>())); } }
    }
}