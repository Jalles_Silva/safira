﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class ReporteEncaminhamento : DomainBase<ReporteEncaminhamento, IReporteEncaminhamentoRepository<ReporteEncaminhamento>>
    {
        public ReporteEncaminhamento()
        {
        }

        protected int codigoSeqReporteEncaminhamento;
        protected Encaminhamento encaminhamento;
        protected DateTime dataReporte;
        protected string descricaoReporteEncaminhamento;
        protected string usuario;

        public virtual int CodigoSeqReporteEncaminhamento { get { return codigoSeqReporteEncaminhamento; } }
        public virtual Encaminhamento Encaminhamento { get { return encaminhamento; } set { encaminhamento = value; } }
        public virtual DateTime DataReporte { get { return dataReporte; } set { dataReporte = value; } }
        public virtual string DescricaoReporteEncaminhamento { get { return descricaoReporteEncaminhamento; } set { descricaoReporteEncaminhamento = value; } }
        public virtual string Usuario { get { return usuario; } set { usuario = value; } }
    }
}
