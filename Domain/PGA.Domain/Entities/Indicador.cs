﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class Indicador : DomainBase<Indicador, IIndicadorRepository<Indicador>>
    {
        protected Indicador()
        {
        }

        public Indicador(TipoIndicador tipoIndicador)
        {
            this.SetTipoIndicador(tipoIndicador);
        }

        protected int codigoSeqIndicador;
        protected string noIndicador;
        protected string descricaoIndicador;
        protected decimal valorLinhaBase;
        protected string descricaoFormula;
        protected decimal valorAlvo;
        protected DateTime? dataAlvo;
        protected DateTime? mesReferencia;
        protected bool objetivoEstrategico;

        protected TipoIndicador tipoIndicador;
        protected Meta meta;
        protected TipoValor tipoValor;
        protected TipoResultado tipoResultado;
        protected double? nuLinhabase;
        protected Anexo anexo;

        protected int? statusIndicador;
        protected int? codigoSeqAnexo;
        protected string descricaoJustificativaCancelamento;

        protected IList<ControleIndicador> controlesIndicador;

        public virtual int CodigoSeqIndicador { get { return codigoSeqIndicador; } }
        public virtual string NoIndicador { get { return noIndicador; } set { noIndicador = value; } }
        public virtual string DescricaoIndicador { get { return descricaoIndicador; } set { descricaoIndicador = value; } }
        public virtual decimal ValorLinhaBase { get { return valorLinhaBase; } set { valorLinhaBase = value; } }
        public virtual string DescricaoFormula { get { return descricaoFormula; } set { descricaoFormula = value; } }
        public virtual decimal ValorAlvo { get { return valorAlvo; } set { valorAlvo = value; } }
        public virtual DateTime? DataAlvo { get { return dataAlvo; } set { dataAlvo = value; } }
        public virtual DateTime? MesReferencia { get { return mesReferencia; } set { mesReferencia = value; } }
		public virtual double? NuLinhabase { get { return nuLinhabase; } set { nuLinhabase = value; } }
		public virtual bool ObjetivoEstrategico { get { return objetivoEstrategico; } set { objetivoEstrategico = value; } }

        public virtual TipoIndicador TipoIndicador { get { return tipoIndicador; } }
        public virtual Meta Meta { get { return meta; } }
        public virtual TipoValor TipoValor { get { return tipoValor; } }
		public virtual TipoResultado TipoResultado { get { return tipoResultado; } }
        public virtual Anexo Anexo { get { return anexo; } }

        public virtual int? StatusIndicador { get { return statusIndicador; } set { statusIndicador = value; } }
        public virtual int? CodigoSeqAnexo { get { return codigoSeqAnexo; } set { codigoSeqAnexo = value; } }
        public virtual string DescricaoJustificativaCancelamento { get { return descricaoJustificativaCancelamento; } set { descricaoJustificativaCancelamento = value; } }

        public virtual IList<ControleIndicador> ControlesIndicador { get { return (controlesIndicador ?? (controlesIndicador = new List<ControleIndicador>())); } }

        public virtual void SetTipoIndicador(TipoIndicador tipoIndicador)
        {
            this.tipoIndicador = tipoIndicador;
        }

        public virtual void SetTipoValor(TipoValor tipoValor)
        {
            this.tipoValor = tipoValor;
        }

        public virtual void SetTipoResultado(TipoResultado tipoResultado)
        {
            this.tipoResultado = tipoResultado;
        }

        public virtual void SetMeta(Meta meta)
        {
            this.meta = meta;
        }
        public virtual void SetAnexo(Anexo anexo)
        {
            this.anexo = anexo;
        }
    }
}