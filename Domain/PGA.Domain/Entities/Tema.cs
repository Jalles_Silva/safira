﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class Tema : DomainBase<Tema, ITemaRepository<Tema>>
    {
        public Tema()
        {
        }

        protected int codigoSeqTema;
        protected string nuTema;
        protected string descricaoTema;
        protected bool ativo;

        protected Perspectiva perspectiva;

        public virtual int CodigoSeqTema { get { return codigoSeqTema; } set { codigoSeqTema = value; } }
        public virtual string NuTema { get { return nuTema; } set { nuTema = value; } }
        public virtual string DescricaoTema { get { return descricaoTema; } set { descricaoTema = value; } }
        public virtual bool Ativo { get { return ativo; } set { ativo = value; } }
        public virtual Perspectiva Perspectiva { get { return perspectiva; } set { perspectiva = value; } }
    }
}