﻿using PGA.Integration.Spec;
using SQFramework.Spring.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Domain.Entities
{
    public partial class ProcessoAnexo : DomainBase<ProcessoAnexo, IProcessoAnexoRepository<ProcessoAnexo>>
    {
        public ProcessoAnexo()
        {
        }

        protected int cd_Seq_Processo_Anexo;
        protected int cd_Processo;
        protected int? cd_Anexo;
        protected string no_Usuario_Criacao;
        protected DateTime dt_Criacao;
        protected string no_Usuario_Atualizacao;
        protected DateTime dt_Atualizacao;
        protected bool st_Ativo;
        protected string ds_Motivo_Inativacao;

        protected Processo processo;
        protected Anexo anexo;

        public virtual int Cd_Seq_Processo_Anexo { get { return cd_Seq_Processo_Anexo; } set { cd_Seq_Processo_Anexo = value; } }
        public virtual int Cd_Processo { get { return cd_Processo; } set { cd_Processo = value; } }
        public virtual int? Cd_Anexo { get { return cd_Anexo; } set { cd_Anexo = value; } }
        public virtual string No_Usuario_Criacao { get { return no_Usuario_Criacao; } set { no_Usuario_Criacao = value; } }
        public virtual DateTime Dt_Criacao { get { return dt_Criacao; } set { dt_Criacao = value; } }
        public virtual string No_Usuario_Atualizacao { get { return no_Usuario_Atualizacao; } set { no_Usuario_Atualizacao = value; } }
        public virtual DateTime Dt_Atualizacao { get { return dt_Atualizacao; } set { dt_Atualizacao = value; } }
        public virtual bool St_Ativo { get { return st_Ativo; } set { st_Ativo = value; } }
        public virtual string Ds_Motivo_Inativacao { get { return ds_Motivo_Inativacao; } set { ds_Motivo_Inativacao = value; } }

        //public virtual Processo Processo { get { return processo; } set { processo = value; } }
        //public virtual Anexo Anexo { get { return anexo; } set { anexo = value; } }

    }
}

