﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class Anexo : DomainBase<Anexo, IAnexoRepository<Anexo>>
    {
        public Anexo()
        {
        }

        protected int codigoSeqAnexo;
        protected string descricaoAnexo;
        protected string caminhoAnexo;
        protected DateTime dataInclusao;
        protected string observacoes;
        protected int cdTipoAnexo;

        public virtual int CodigoSeqAnexo { get { return codigoSeqAnexo; } }
        public virtual string DescricaoAnexo { get { return descricaoAnexo; } set { descricaoAnexo = value; } }
        public virtual string CaminhoAnexo { get { return caminhoAnexo; } set { caminhoAnexo = value; } }
        public virtual DateTime DataInclusao { get { return dataInclusao; } set { dataInclusao = value; } }
        public virtual string Observacoes { get { return observacoes; } set { observacoes = value; } }
        public virtual int CdTipoAnexo { get { return cdTipoAnexo; } set { cdTipoAnexo = value; } }
    }
}
