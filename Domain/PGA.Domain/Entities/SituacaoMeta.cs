﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class SituacaoMeta : DomainBase<SituacaoMeta, ISituacaoMetaRepository<SituacaoMeta>>
    {
        protected SituacaoMeta()
        {
        }

        public SituacaoMeta(StatusMeta statusMeta, Meta meta)
        {
            this.SetStatusMeta(statusMeta);
            this.SetMeta(meta);
        }

        protected int codigoSeqSituacaoMeta;
        protected string noUsuario;
        protected DateTime dataAlteracao;
        protected int? codigoSeqAnexo;
        protected string descricaoJustificativaCancelamento;

        protected StatusMeta statusMeta;
        protected Meta meta;
        protected Anexo anexo;

        public virtual int CodigoSeqSituacaoMeta { get { return codigoSeqSituacaoMeta; } }
        public virtual string NoUsuario { get { return noUsuario; } set { noUsuario = value; } }
        public virtual DateTime DataAlteracao { get { return dataAlteracao; } set { dataAlteracao = value; } }
        public virtual int? CodigoSeqAnexo { get { return codigoSeqAnexo; } set { codigoSeqAnexo = value; } }

        public virtual Anexo Anexo { get { return anexo; } set { anexo = value; } }
        public virtual string DescricaoJustificativaCancelamento { get { return descricaoJustificativaCancelamento; } set { descricaoJustificativaCancelamento = value; } }

        public virtual StatusMeta StatusMeta { get { return statusMeta; } }
        public virtual Meta Meta { get { return meta; } }

        public virtual void SetStatusMeta(StatusMeta statusMeta)
        {
            this.statusMeta = statusMeta;
        }

        public virtual void SetMeta(Meta meta)
        {
            this.meta = meta;
        }
    }
}