﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class ResultadoAlcancado : DomainBase<ResultadoAlcancado, IResultadoAlcancadoRepository<ResultadoAlcancado>>
    {
        public ResultadoAlcancado()
        {

        }

        protected int codigoSeqResultadoAlcancado;
        protected string descricaoResultadoAlcancado;
        protected bool registrado;
        protected bool ativo;
        protected DateTime dataInclusao;

        protected Exercicio exercicio;
        protected Objetivo objetivo;
        protected Iniciativa iniciativa;
        protected CorporativoUnidade corporativoUnidade;

        public virtual int CodigoSeqResultadoAlcancado { get { return codigoSeqResultadoAlcancado; }  set { codigoSeqResultadoAlcancado = value; } }
        public virtual string DescricaoResultadoAlcancado { get { return descricaoResultadoAlcancado; } set { descricaoResultadoAlcancado = value; } }
        public virtual bool Registrado { get { return registrado; } set { registrado = value; } }
        public virtual bool Ativo { get { return ativo; } set { ativo = value; } }
        public virtual DateTime DataInclusao { get { return dataInclusao; } set { dataInclusao = value; } }

        public virtual Exercicio Exercicio { get { return exercicio; } set { exercicio = value; } }
        public virtual Objetivo Objetivo { get { return objetivo; } set { objetivo = value; } }
        public virtual Iniciativa Iniciativa { get { return iniciativa; } set { iniciativa = value; } }
        public virtual CorporativoUnidade CorporativoUnidade { get { return corporativoUnidade; } set { corporativoUnidade = value; } }
    }
}