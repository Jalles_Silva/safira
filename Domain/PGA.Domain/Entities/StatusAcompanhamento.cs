﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class StatusAcompanhamento : DomainBase<StatusAcompanhamento, IStatusAcompanhamentoRepository<StatusAcompanhamento>>
    {
        public StatusAcompanhamento()
        {
        }

        protected int codigoSeqStatusAcompanhamento;
        protected string descricaoStatusAcompanhamento;

        public virtual int CodigoSeqStatusAcompanhamento { get { return codigoSeqStatusAcompanhamento; } }
        public virtual string DescricaoStatusAcompanhamento { get { return descricaoStatusAcompanhamento; } set { descricaoStatusAcompanhamento = value; } }
    }
}
