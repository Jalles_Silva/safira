﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class TipoTexto : DomainBase<TipoTexto, ITipoTextoRepository<TipoTexto>>
    {
        public TipoTexto()
        {

        }

        protected int codigoSeqTipoTexto;
        protected string descricaoTipoTexto;
        protected bool ativo;
        protected DateTime dataInclusao;

        public virtual int CodigoSeqTipoTexto { get { return codigoSeqTipoTexto; } }
        public virtual string DescricaoTipoTexto { get { return descricaoTipoTexto; } set { descricaoTipoTexto = value; } }
        public virtual bool Ativo { get { return ativo; } set { ativo = value; } }
        public virtual DateTime DataInclusao { get { return dataInclusao; } set { dataInclusao = value; } }
    }
}