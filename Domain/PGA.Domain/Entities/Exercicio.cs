﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQFramework.Spring.Domain;
using PGA.Integration.Spec;

namespace PGA.Domain.Entities
{
    public partial class Exercicio : DomainBase<Exercicio, IExercicioRepository<Exercicio>>
    {
        protected Exercicio()
        {
        }

        public Exercicio(Fase fase)
        {
            this.SetFase(fase);
        }

        protected int codigoSeqExercicio;
        protected short ano;
        protected string nomeUsuario;
        protected DateTime dataCadastro;

        protected Fase fase;

        public virtual int CodigoSeqExercicio { get { return codigoSeqExercicio; } }
        public virtual short Ano { get { return ano; } set { ano = value; } }
        public virtual string NomeUsuario { get { return nomeUsuario; } set { nomeUsuario = value; } }
        public virtual DateTime DataCadastro { get { return dataCadastro; } set { dataCadastro = value; } }

        public virtual Fase Fase { get { return fase; } }

        public virtual void SetFase(Fase fase)
        {
            this.fase = fase;
        }
    }
}