﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PGA.Common
{
    public static class Constants
    {
        public const string SCAService = "antt.servicos/SCAService";

        public const string SCAServiceV2Usuarios = "antt.servicos/SCAServiceV2Usuarios";
    }
}