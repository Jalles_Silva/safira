﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using SQFramework.Core.Enums;

namespace PGA.Common
{
    [TypeConverter(typeof(EnumConverter<StatusMetaEnum>))]
    public enum StatusMetaEnum
    {
        [StringValue("1", "Em cadastro")]
        EmCadastro = 1,
        [StringValue("2", "Em validação")]
        EmValidacao = 2,
        [StringValue("3", "Validada")]
        Validada = 3,
        [StringValue("4", "Cancelada")]
        Cancelada = 4
    }

    [TypeConverter(typeof(EnumConverter<FaseEnum>))]
    public enum FaseEnum
    {
        [StringValue("1", "Planejamento")]
        Planejamento = 1,
        [StringValue("2", "Acompanhamento do 1° Trimestre")]
        AcompanhamentoPrimeiroTrimestre = 2,
        [StringValue("3", "Acompanhamento do 2° Trimestre")]
        AcompanhamentoSegundoTrimestre = 3,
        [StringValue("4", "Acompanhamento do 3° Trimestre")]
        AcompanhamentoTerceiroTrimestre = 4,
        [StringValue("5", "Acompanhamento do 4° Trimestre")]
        AcompanhamentoQuartoTrimestre = 5,
        [StringValue("6", "Encerrado")]
        Encerrado = 6,
        [StringValue("7", "Pré-Encerramento")]
        PreEncerramento = 7
    }

    [TypeConverter(typeof(EnumConverter<TiposAlinhamento>))]
    public enum TiposAlinhamento
    {
        [StringValue("1", "Alinhamento PE")]
        Alinhamento_PE = 1,
        [StringValue("2", "Alinhamento PPA")]
        Alinhamento_PPA = 2,
        [StringValue("3", "Alinhamento Missão Institucional")]
        Alinhamento_MI = 3
    }

    [TypeConverter(typeof(EnumConverter<TiposInstrumento>))]
    public enum TiposInstrumento
    {
        [StringValue("1", "Agenda Regulatória")]
        AgendaRegulatoria = 1,
        [StringValue("2", "Desburocratização")]
        Desburocratizacao = 2,
        [StringValue("3", "Gestão de Riscos")]
        GestaoRiscos = 3,
        [StringValue("4", "Integridade")]
        Integridade = 4,
        [StringValue("5", "Iniciativa Estratégica")]
        IniciativaEstrategica = 5
    }

    [TypeConverter(typeof(EnumConverter<TiposDeMeta>))]
    public enum TiposDeMeta
    {
        [StringValue("1", "Todos")]
        Todos = 1,
        [StringValue("10", "Administrativa")]
        Administrativa = 10,
        [StringValue("30", "De Fiscalização")]
        De_Fiscalizacao = 30,
        [StringValue("40", "De Regulação")]
        De_Regulacao = 40
    }

    [TypeConverter(typeof(EnumConverter<FasePeriodo>))]
    public enum FasePeriodo
    {
        [StringValue("0", "Acumulado no Exercício")]
        AcumuloExercicio = 0
        , [StringValue("1", "PLANEJAMENTO")]
        Planejamento = 1,
        [StringValue("2", "1° Trimestre")]
        PrimeiroTrimestre = 2,
        [StringValue("3", "2° Trimestre")]
        SegundoTrimestre = 3,
        [StringValue("4", "3° Trimestre")]
        TerceiroTrimestre = 4,
        [StringValue("5", "4° Trimestre")]
        QuartoTrimestre = 5
    }

    [TypeConverter(typeof(EnumConverter<TiposDeMeta>))]
    public enum DetalhesDeMetas
    {
        [StringValue("0", "TipoMetaAlinhamento")]
        TipoMetaAlinhamento = 0,
        [StringValue("1", "TipoMeta")]
        TipoMeta = 1,
        [StringValue("2", "TipoAlinhamento")]
        TipoAlinhamento = 2
    }

    [TypeConverter(typeof(EnumConverter<EnumTipoResultado>))]
    public enum EnumTipoResultado
    {
        [StringValue("1", "Acumulado")]
        Acumulado = 1,
        [StringValue("2", "Média")]
        Media = 2
    }

    [TypeConverter(typeof(EnumConverter<EnumTipoValor>))]
    public enum EnumTipoValor
    {
        [StringValue("1", "Absoluto")]
        Absoluto = 1,
        [StringValue("2", "Percentual")]
        Percentual = 2
    }

    [TypeConverter(typeof(EnumConverter<EnumTipoValor>))]
    public enum EnumTipoObjetivo
    {
        [StringValue("1", "Aspectos de Integridade")]
        Aspectos = 1,
        [StringValue("2", "Eixos Temáticos")]
        Eixos = 2,
        [StringValue("3", "Macroprocessos")]
        Macroprocessos = 3,
        [StringValue("4", "Objetivos Estratégicos")]
        Objetivos = 4
    }

    [TypeConverter(typeof(EnumConverter<EnumStatusAcompanhamento>))]
    public enum EnumStatusAcompanhamento
    {
        [StringValue("1", "Pendente")]
        Pendente = 1,
        [StringValue("2", "Concluído")]
        Concluido = 2
    }

    [TypeConverter(typeof(EnumConverter<EnumStatusEncaminhamento>))]
    public enum EnumStatusEncaminhamento
    {
        [StringValue("1", "Pendente")]
        Pendente = 1,
        [StringValue("2", "Concluído")]
        Concluido = 2
    }

    [TypeConverter(typeof(EnumConverter<EnumTipoResultadoAlcancado>))]
    public enum EnumTipoResultadoAlcancado
    {
        [StringValue("1", "Objetivos Estratégicos")]
        ObjetivoEstrategico = 1,
        [StringValue("2", "Missão Institucional")]
        MissaoInstitucional = 2
    }

    [TypeConverter(typeof(EnumConverter<EnumTipoTexto>))]
    public enum EnumTipoTexto
    {
        [StringValue("1", "Sumário Executivo")]
        SumarioExecutivo = 1,
        [StringValue("2", "Apresentação")]
        Apresentacao = 2,
        [StringValue("3", "Plano Estratégico e Plano de Gestão Anual")]
        PlanoEstrategicoPlanoGestaoAnual = 3
    }
}