﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PGA.Services.Spec.Services;
using SQFramework.Core.Reflection;

namespace PGA.Services.Impl.Services
{
    public class ServiceBase : IServiceBase
    {
        public string GetServiceVersion()
        {
            return AssemblyHelper.GetAssemblyVersion(GetType());
        }
    }
}