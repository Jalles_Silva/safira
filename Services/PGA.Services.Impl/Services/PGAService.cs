﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using SQFramework.Core;
using SQFramework.Data.Pagging;
using SQFramework.Web.Report;
using Spring.Transaction.Interceptor;
using Spring.Transaction;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Services.Spec.Services;
using PGA.Domain.Entities;
using SQFramework.Spring;
using PGA.Common;
using PGA.Integration.Spec.ValueObjects;
using System.IO;
using System.Collections;

namespace PGA.Services.Impl.Services
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.PerSession)]
    public class PGAService : ServiceBase, IPGAService
    {
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] ExportReportRelatorioGlobalAtividade(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending)
        {
            var items = ListaGlobalAtividade(alinhadasPE, alinhadasPPA, alinhadasMI, AnoExercicio, Unidade, codigoPeriodo,
                codigoTipoMeta, MetaInterna, TipoInstrumento, 0, int.MaxValue);

            return ReportViewerHelper.ExportReport(reportType, @"Reports\RelatorioGlobalAtividade.rdlc", items);
        }
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] ExportReportRelatorioGlobalAtividadeSimples(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending)
        {
            var items = ListaGlobalAtividade(alinhadasPE, alinhadasPPA, alinhadasMI, AnoExercicio, Unidade, codigoPeriodo,
                codigoTipoMeta, MetaInterna, TipoInstrumento, 0, int.MaxValue);

            return ReportViewerHelper.ExportReport(reportType, @"Reports\RelatorioGlobalAtividadeSimples.rdlc", items);
        }
        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] ExportReportRelatorioDetalheAtividade(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending)
        {
            var items = ListaDetalheAtividade(alinhadasPE, alinhadasPPA, alinhadasMI, AnoExercicio, Unidade, codigoPeriodo,
                codigoTipoMeta, MetaInterna, TipoInstrumento, 0, int.MaxValue);

            return ReportViewerHelper.ExportReport(reportType, @"Reports\RelatorioDetalheAtividade.rdlc", items);
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] ExportReportRelatorioDetalheAtividadeSimples(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending)
        {
            var items = ListaDetalheAtividade(alinhadasPE, alinhadasPPA, alinhadasMI, AnoExercicio, Unidade, codigoPeriodo,
                codigoTipoMeta, MetaInterna, TipoInstrumento, 0, int.MaxValue);

            return ReportViewerHelper.ExportReport(reportType, @"Reports\RelatorioDetalheAtividadeSimples.rdlc", items);
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] ExportReportRelatorioDetalheIndicador(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending)
        {
            var items = ListaDetalheIndicador(alinhadasPE, alinhadasPPA, alinhadasMI, AnoExercicio, Unidade, codigoPeriodo,
                codigoTipoMeta, MetaInterna, TipoInstrumento, 0, int.MaxValue);

            return ReportViewerHelper.ExportReport(reportType, @"Reports\RelatorioDetalheIndicador.rdlc", items);
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] ExportReportRelatorioDetalheIndicadorSimples(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending)
        {
            var items = ListaDetalheIndicador(alinhadasPE, alinhadasPPA, alinhadasMI, AnoExercicio, Unidade, codigoPeriodo,
                codigoTipoMeta, MetaInterna, TipoInstrumento, 0, int.MaxValue);

            return ReportViewerHelper.ExportReport(reportType, @"Reports\RelatorioDetalheIndicadorSimples.rdlc", items);
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] ExportReportRelatorioGlobalIndicador(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending)
        {
            var items = ListaGlobalIndicador(alinhadasPE, alinhadasPPA, alinhadasMI, AnoExercicio, Unidade, codigoPeriodo,
                codigoTipoMeta, MetaInterna, TipoInstrumento, 0, int.MaxValue);

            return ReportViewerHelper.ExportReport(reportType, @"Reports\RelatorioGlobalIndicador.rdlc", items);
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] ExportReportRelatorioGlobalIndicadorSimples(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending)
        {
            var items = ListaGlobalIndicador(alinhadasPE, alinhadasPPA, alinhadasMI, AnoExercicio, Unidade, codigoPeriodo,
                codigoTipoMeta, MetaInterna, TipoInstrumento, 0, int.MaxValue);

            return ReportViewerHelper.ExportReport(reportType, @"Reports\RelatorioGlobalIndicadorSimples.rdlc", items);
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] ExportReportRelatorioDetalheMeta(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int codigoTipoMeta,
            string Unidade, int AnoExercicio, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending)
        {
            var items = ListaDetalheMeta(alinhadasPE, alinhadasPPA, alinhadasMI, codigoTipoMeta, Unidade, AnoExercicio, MetaInterna, TipoInstrumento, 0, int.MaxValue);
            return ReportViewerHelper.ExportReport(reportType, @"Reports\RelatorioDetalheMeta.rdlc", items);
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] ExportReportRelatorioDetalheMetaSimples(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int codigoTipoMeta,
            string Unidade, int AnoExercicio, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending)
        {
            var items = ListaDetalheMeta(alinhadasPE, alinhadasPPA, alinhadasMI, codigoTipoMeta, Unidade, AnoExercicio, MetaInterna, TipoInstrumento, 0, int.MaxValue);
            return ReportViewerHelper.ExportReport(reportType, @"Reports\RelatorioDetalheMetaSimples.rdlc", items);
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] ExportReportRelatorioGlobalMeta(bool VerAlinhadasPE, bool VerAlinhadasPPA, bool VerAlinhadasMI, bool VerAdministratiVas,
            bool VerFiscalizacoes, bool VerRegulacoes, string Unidade, int AnoExercicio, int CodigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType,
            string orderProperty, bool orderAscending)
        {
            var items = ListaGlobalMeta(VerAlinhadasPE, VerAlinhadasPPA, VerAlinhadasMI, VerAdministratiVas, VerFiscalizacoes, VerRegulacoes, Unidade,
                AnoExercicio, CodigoTipoMeta, MetaInterna, TipoInstrumento, 0, int.MaxValue);
            return ReportViewerHelper.ExportReport(reportType, @"Reports\RelatorioGlobalMeta.rdlc", items);
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] ExportReportRelatorioGlobalMetaSimples(bool VerAlinhadasPE, bool VerAlinhadasPPA, bool VerAlinhadasMI, bool VerAdministratiVas,
            bool VerFiscalizacoes, bool VerRegulacoes, string Unidade, int AnoExercicio, int CodigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType,
            string orderProperty, bool orderAscending)
        {
            var items = ListaGlobalMeta(VerAlinhadasPE, VerAlinhadasPPA, VerAlinhadasMI, VerAdministratiVas, VerFiscalizacoes, VerRegulacoes, Unidade,
                AnoExercicio, CodigoTipoMeta, MetaInterna, TipoInstrumento, 0, int.MaxValue);
            return ReportViewerHelper.ExportReport(reportType, @"Reports\RelatorioGlobalMetaSimples.rdlc", items);
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] ExportReportRelatorioAcompanhamentos(int CodigoSeqAcompanhamento, ReportViewerHelper.ReportType reportType)
        {
            var item = Encaminhamento.GetRepository().ListAll().Where(a => a.Acompanhamento.CodigoSeqAcompanhamento == CodigoSeqAcompanhamento).TransformList<DTOAcompanhamentoRelatorio>();

            if (item.Count <= 0)
            {
                var encaminhamento = new Encaminhamento();
                encaminhamento.Acompanhamento = Acompanhamento.GetRepository().Get(CodigoSeqAcompanhamento);
                var e = encaminhamento.Transform<DTOAcompanhamentoRelatorio>();
                e.PrazoAtendimento = null;
                item.Add(e);
            }

            return ReportViewerHelper.ExportReport(reportType, @"Reports\RelatorioAcompanhamentosPGA.rdlc", item);
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] ExportReportRelatorioConsubstanciadoAnual(int anoExercicio, ReportViewerHelper.ReportType reportType)
        {
            Dictionary<string, IEnumerable> dataSources = new Dictionary<string, IEnumerable>();

            IList<DTORelatorioConsubstanciadoUnico> dtoConsubstanciadoSources = new List<DTORelatorioConsubstanciadoUnico>();
            DTORelatorioConsubstanciadoUnico dtoRelatorioConsubstanciado = new DTORelatorioConsubstanciadoUnico();

            foreach (var item in ListarRelatorioConsubstanciado(ObterFaseExercicioPorAno(short.Parse(anoExercicio.ToString())).CodigoSeqExercicio))
            {
                if (item.TipoTexto.CodigoSeqTipoTexto == (int)EnumTipoTexto.SumarioExecutivo)
                {
                    dtoRelatorioConsubstanciado.CodigoSumarioExecutivo = item.CodigoSeqRelatorioConsubstanciado;
                    dtoRelatorioConsubstanciado.DescricaoSumarioExecutivo = item.DescricaoRelatorioConsubstanciado;
                }
                if (item.TipoTexto.CodigoSeqTipoTexto == (int)EnumTipoTexto.Apresentacao)
                {
                    dtoRelatorioConsubstanciado.CodigoApresentacao = item.CodigoSeqRelatorioConsubstanciado;
                    dtoRelatorioConsubstanciado.DescricaoApresentacao = item.DescricaoRelatorioConsubstanciado;
                }
                if (item.TipoTexto.CodigoSeqTipoTexto == (int)EnumTipoTexto.PlanoEstrategicoPlanoGestaoAnual)
                {
                    dtoRelatorioConsubstanciado.CodigoPlanoEstrategicoPlanoGestaoAnual = item.CodigoSeqRelatorioConsubstanciado;
                    dtoRelatorioConsubstanciado.DescricaoPlanoEstrategicoPlanoGestaoAnual = item.DescricaoRelatorioConsubstanciado;
                }

            }
            dtoConsubstanciadoSources.Add(dtoRelatorioConsubstanciado);

            dataSources.Add("RelatorioConsubstanciado", dtoConsubstanciadoSources);
            dataSources.Add("ResultadosAlcancados", ListarResultadosAlcancadosPorExercicio(anoExercicio));
            dataSources.Add("ObjetivosEstrategicos", ListarObjetivoEstrategico(anoExercicio));
            dataSources.Add("Recursos", ListarRecursos(anoExercicio));
            dataSources.Add("Ano", AnoExercicio(anoExercicio));

            return ReportViewerHelper.ExportReport(reportType, @"Reports\RelatorioConsubstanciado.rdlc", dataSources);
        }

        [Transaction(TransactionPropagation.Required)]
        public void AbrirExercicio(string nomeUsuario)
        {
            short anoExercicio = 1;
            var ultimoExercicio = Exercicio.GetRepository().ObterUltimoExercicioEmPlanejamento();

            if (ultimoExercicio != null)
                anoExercicio += ultimoExercicio.Ano;
            else
                anoExercicio += Exercicio.GetRepository().ObterExercicioAtual().Ano;

            Fase fase = Fase.GetRepository().Get((int)FaseEnum.Planejamento);
            Exercicio novoExercicio = new Exercicio(fase);
            novoExercicio.Ano = anoExercicio;
            novoExercicio.NomeUsuario = nomeUsuario;
            novoExercicio.DataCadastro = DateTime.Now;
            novoExercicio.AbrirExercicio();
        }

        [Transaction(TransactionPropagation.Required)]
        public void PromoverFaseExercicio(int ano, string nomeUsuario)
        {
            Exercicio exercicio = Exercicio.GetRepository().ObterExercicioPorAno(ano.ToShort());
            exercicio.PromoverFase(nomeUsuario);
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOExercicio ObterExercicioAtual()
        {
            return Exercicio.GetRepository().ObterExercicioAtual().Transform<DTOExercicio>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOExercicio> ListarExercicios()
        {
            return Exercicio.GetRepository().ListAll().TransformList<DTOExercicio>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOTipoValor> ListarTipoValores()
        {
            return TipoValor.GetRepository().ListAll().TransformList<DTOTipoValor>();
        }

        [Transaction(TransactionPropagation.Required)]
        public DTOControleIndicador SalvarControleIndicador(DTOControleIndicador dto)
        {
            ControleIndicador controleIndicador = null;

            var indicador = Indicador.GetRepository().Get(dto.CodigoSeqIndicador);

            if (dto.CodigoSeqControleIndicador > 0)
            {
                controleIndicador = ControleIndicador.GetRepository().Get(dto.CodigoSeqControleIndicador);

                if (controleIndicador == null)
                    throw new Exception("ControleIndicador não encontrado(a)!");

                dto.Transform<ControleIndicador>(controleIndicador);

                controleIndicador.SetIndicador(indicador);
            }
            else
            {
                controleIndicador = new ControleIndicador(indicador);
                dto.Transform<ControleIndicador>(controleIndicador);
            }

            controleIndicador.DataCadastro = DateTime.Now;
            controleIndicador.Save();

            return controleIndicador.Transform<DTOControleIndicador>();
        }



        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOControleIndicador ObterControleIndicador(int codigoSeqControleIndicador)
        {
            return ControleIndicador.GetRepository().Get(codigoSeqControleIndicador).Transform<DTOControleIndicador>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarControleIndicador(int codigoSeqControleIndicador, bool usuarioAdministrador)
        {
            var item = ControleIndicador.GetRepository().Get(codigoSeqControleIndicador);
            item.UsuarioAdministrador = usuarioAdministrador;
            item.Delete();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOControleIndicador> ListarControlesPorIndicador(int codigoIndicador, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            return ControleIndicador.GetRepository().ListarControlesPorIndicador(codigoIndicador, startIndex, pageSize, orderProperty, orderAscending)
                .Transform<PageMessage<DTOControleIndicador>>();
        }

        [Transaction(TransactionPropagation.Required)]
        public DTOControleAcao SalvarControleAcao(DTOControleAcao dto)
        {
            ControleAcao controleAcao = null;

            var acao = Acao.GetRepository().Get(dto.CodigoSeqAcao);

            if (dto.CodigoSeqControleAcao > 0)
            {
                controleAcao = ControleAcao.GetRepository().Get(dto.CodigoSeqControleAcao);

                if (controleAcao == null)
                    throw new Exception("ControleAcao não encontrado(a)!");

                dto.Transform<ControleAcao>(controleAcao);

                controleAcao.SetAcao(acao);
            }
            else
            {
                controleAcao = new ControleAcao(acao);
                dto.Transform<ControleAcao>(controleAcao);
            }


            controleAcao.DataCadastro = DateTime.Now;
            controleAcao.Save();

            return controleAcao.Transform<DTOControleAcao>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOControleAcao ObterControleAcao(int codigoSeqControleAcao)
        {
            return ControleAcao.GetRepository().Get(codigoSeqControleAcao).Transform<DTOControleAcao>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarControleAcao(int codigoSeqControleAcao, bool usuarioAdministrador)
        {
            var item = ControleAcao.GetRepository().Get(codigoSeqControleAcao);
            item.UsuarioAdministrador = usuarioAdministrador;
            item.Delete();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOControleAcao> ListarControlesPorAcao(int codigoAcao, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            return ControleAcao.GetRepository().ListarControlesPorAcao(codigoAcao, startIndex, pageSize, orderProperty, orderAscending)
                .Transform<PageMessage<DTOControleAcao>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOControleIndicadorDetalhado> ListarIndicadoresPorFiltros(DTOFiltroControle filtro)
        {
            return Indicador.GetRepository().ListarIndicadoresPorFiltros(filtro.Transform<VOFiltroControle>())
                .Transform<PageMessage<DTOControleIndicadorDetalhado>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOAcaoDetalhado> ListarAcoesPorFiltros(DTOFiltroControle filtro)
        {
            return Acao.GetRepository().ListarAcoesPorFiltros(filtro.Transform<VOFiltroControle>())
                .Transform<PageMessage<DTOAcaoDetalhado>>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void SalvarAcao(DTOAcao dto)
        {
            Acao acao = null;
            if (dto.CodigoSeqAcao > 0)
            {
                acao = Acao.GetRepository().Get(dto.CodigoSeqAcao);
                if (acao == null)
                    throw new Exception("Acao não encontrado(a)!");
            }
            else
            {
                if (dto.CodigoSeqMeta == 0)
                    throw new Exception("Meta não encontrada.");
                var meta = Meta.GetRepository().Get(dto.CodigoSeqMeta);
                if (meta == null)
                    throw new Exception("Meta não encontrada.");
                acao = new Acao(meta);
            }

            acao.DescricaoAtividadeAcao = dto.DescricaoAtividadeAcao;
            acao.DescricaoEstrategiaAcao = dto.DescricaoEstrategiaAcao;
            acao.DescricaoResponsavel = dto.DescricaoResponsavel;
            acao.DataInicioAcao = dto.DataInicioAcao;
            acao.DataFimAcao = dto.DataFimAcao;
            acao.Peso = dto.Peso;
            acao.StatusAcao = dto.StatusAcao;

            acao.Recursos.Clear();

            Recurso recurso;
            foreach (var item in dto.Recursos)
            {
                recurso = new Recurso();
                recurso.DescricaoRecurso = item.DescricaoRecurso;
                recurso.QuantidadeRecurso = item.QuantidadeRecurso;
                recurso.SetTipoRecurso(TipoRecurso.GetRepository().Get(item.CodigoSeqTipoRecurso));
                recurso.SetAcao(acao);
                recurso.Save();
                acao.Recursos.Add(recurso);
            }
            acao.Save();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOAcao ObterAcao(int codigoSeqAcao)
        {
            return Acao.GetRepository().Get(codigoSeqAcao).Transform<DTOAcao>();
        }

        public void AlterarStatusAcao(int codigoSeqAcao, int status)
        {
            var item = Acao.GetRepository().Get(codigoSeqAcao);

            item.StatusAcao = item.StatusAcao == 1 ? 2 : 1;

            item.Save();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarAcao(int codigoSeqAcao)
        {
            var item = Acao.GetRepository().Get(codigoSeqAcao);
            item.Delete();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOAcao> ListarAcoesPorMeta(int codigoSeqMeta, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            return Acao.GetRepository().ListarAcoesPorMeta(codigoSeqMeta, startIndex, pageSize, orderProperty, orderAscending)
                .Transform<PageMessage<DTOAcao>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOTipoIndicador> ListarTipoIndicadores()
        {
            return TipoIndicador.GetRepository().ListAll().TransformList<DTOTipoIndicador>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOTipoRecurso> ListarTipoRecursos()
        {
            return TipoRecurso.GetRepository().ListAll().TransformList<DTOTipoRecurso>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void SalvarIndicador(DTOIndicador dto)
        {
            Indicador indicador = null;

            var tipoIndicador = TipoIndicador.GetRepository().Get(dto.CodigoSeqTipoIndicador);

            if (dto.CodigoSeqIndicador > 0)
            {
                indicador = Indicador.GetRepository().Get(dto.CodigoSeqIndicador);
                if (indicador == null)
                    throw new Exception("Indicador não encontrado.");
                indicador.SetTipoIndicador(tipoIndicador);
            }
            else
            {
                indicador = new Indicador(tipoIndicador);
                if (dto.CodigoSeqMeta == 0)
                    throw new Exception("Meta não encontrada.");
                Meta meta = Meta.GetRepository().Get(dto.CodigoSeqMeta);
                if (meta == null)
                    throw new Exception("Meta não encontrada.");
                indicador.SetMeta(meta);
            }

            var tipoValor = TipoValor.GetRepository().Get(dto.CodigoSeqTipoValor);
            if (tipoValor != null)
                indicador.SetTipoValor(tipoValor);

            indicador.NoIndicador = dto.NoIndicador;
            indicador.DescricaoFormula = dto.DescricaoFormula;
            indicador.DescricaoIndicador = dto.DescricaoIndicador;
            indicador.ValorLinhaBase = dto.ValorLinhaBase;
            indicador.ValorAlvo = dto.ValorAlvo;
            indicador.DataAlvo = dto.DataAlvo;
            indicador.MesReferencia = dto.MesReferencia;
            indicador.ObjetivoEstrategico = dto.ObjetivoEstrategico;
            indicador.StatusIndicador = dto.StatusIndicador;

            var tipoResultado = TipoResultado.GetRepository().Get(dto.TipoResultado.Codigo);
            if (tipoResultado != null)
                indicador.SetTipoResultado(tipoResultado);

            indicador.Save();

            //Caso esse indicador tenha sido setado como Objetivo Estratégico ele verifica se existem outros indicadores com a mesma opção e desabilita deixando apenas um setado como true.
            if (indicador.ObjetivoEstrategico && dto.CodigoSeqMeta > 0)
            {
                Meta meta = Meta.GetRepository().Get(dto.CodigoSeqMeta);

                foreach (var item in meta.Indicadores.Where(i => i.CodigoSeqIndicador != indicador.CodigoSeqIndicador && i.ObjetivoEstrategico))
                {
                    item.ObjetivoEstrategico = false;
                    item.Save();
                }
            }
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOIndicador ObterIndicador(int codigoSeqIndicador)
        {
            return Indicador.GetRepository().Get(codigoSeqIndicador).Transform<DTOIndicador>();
        }
        [Transaction(TransactionPropagation.Required)]
        public void AlterarStatusIndicador(int codigoSeqIndicador, int status)
        {
            var item = Indicador.GetRepository().Get(codigoSeqIndicador);

            item.StatusIndicador = item.StatusIndicador == 1 ? 2 : 1;

            item.Save();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarIndicador(int codigoSeqIndicador)
        {
            var item = Indicador.GetRepository().Get(codigoSeqIndicador);
            item.Delete();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOIndicador> ListarIndicadoresPorMeta(int codigoSeqMeta, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            return Indicador.GetRepository().ListarIndicadoresPorMeta(codigoSeqMeta, startIndex, pageSize, orderProperty, orderAscending)
                .Transform<PageMessage<DTOIndicador>>();
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarMeta(DTOMeta dto)
        {
            Meta meta;

            if (dto.CodigoUnidade == 0)
                throw new Exception("Não foi possível encontrar a superintêndencia.");

            if (dto.CodigoSeqTipoMeta == 0)
                throw new Exception("Tipo de Meta não informado.");

            var tipoMeta = TipoMeta.GetRepository().Get(dto.CodigoSeqTipoMeta);

            if (dto.CodSeqMeta > 0)
            {
                meta = Meta.GetRepository().Get(dto.CodSeqMeta);
                if (meta == null)
                    throw new Exception("Meta não encontrada.");
                meta.SetTipoMeta(tipoMeta);
            }
            else
                meta = new Meta(tipoMeta);

            if (dto.IniciativaPe != null && dto.IniciativaPe.CodigoSeqIniciativa > 0)
                meta.IniciativaPe = Iniciativa.GetRepository().Get(dto.IniciativaPe.CodigoSeqIniciativa);
            else
                meta.IniciativaPe = null;

            if (dto.IniciativaPpa != null && dto.IniciativaPpa.CodigoSeqIniciativa > 0)
                meta.IniciativaPpa = Iniciativa.GetRepository().Get(dto.IniciativaPpa.CodigoSeqIniciativa);
            else
                meta.IniciativaPpa = null;

            if (dto.IniciativaMissao != null && dto.IniciativaMissao.CodigoSeqIniciativa > 0)
                meta.IniciativaMissao = Iniciativa.GetRepository().Get(dto.IniciativaMissao.CodigoSeqIniciativa);
            else
                meta.IniciativaMissao = null;

            if (dto.CodigoSeqGestaoDeRiscos > 0)
                meta.GestaoDeRiscos = Instrumento.GetRepository().Get(dto.CodigoSeqGestaoDeRiscos);
            else
                meta.GestaoDeRiscos = null;

            if (dto.CodigoSeqAgendaRegulatoria > 0)
                meta.AgendaRegulatoria = Instrumento.GetRepository().Get(dto.CodigoSeqAgendaRegulatoria);
            else
                meta.AgendaRegulatoria = null;

            if (dto.CodigoSeqDesburocratizacao > 0)
                meta.Desburocratizacao = Instrumento.GetRepository().Get(dto.CodigoSeqDesburocratizacao);
            else
                meta.Desburocratizacao = null;

            if (dto.CodigoSeqIntegridade > 0)
                meta.Integridade = Instrumento.GetRepository().Get(dto.CodigoSeqIntegridade);
            else
                meta.Integridade = null;

            if (dto.CodigoSeqExercicio > 0)
            {
                var exercicio = Exercicio.GetRepository().Get(dto.CodigoSeqExercicio);
                meta.SetExercicio(exercicio);
            }
            else
            {
                meta.SetExercicio(null);
            }

            var unidade = CorporativoUnidade.GetRepository().Get(dto.CodigoUnidade);
            meta.SetCorporativoUnidade(unidade);

            meta.DescricaoMeta = dto.DescricaoMeta;
            meta.DescricaoPremissa = dto.DescricaoPremissa;
            meta.DescricaoRestricao = dto.DescricaoRestricao;
            meta.DescricaoJustificativa = dto.DescricaoJustificativa;
            meta.DescricaoResultadoEsperado = dto.DescricaoResultadoEsperado;
            meta.NoCoordenacao = dto.NoCoordenacao;
            meta.StatusIniciativaEstrategica = dto.StatusIniciativaEstrategica;
            meta.DescricaoOutros = dto.DescricaoOutros;
            meta.DescricaoSociedadeUsuarioBeneficio = dto.DescricaoSociedadeUsuarioBeneficio;
            meta.DescricaoSociedadeUsuarioImpacto = dto.DescricaoSociedadeUsuarioImpacto;
            meta.DescricaoInstitucionalBeneficio = dto.DescricaoInstitucionalBeneficio;
            meta.DescricaoInstitucionalImpacto = dto.DescricaoInstitucionalImpacto;
            meta.DescricaoGovernoBeneficio = dto.DescricaoGovernoBeneficio;
            meta.DescricaoGovernoImpacto = dto.DescricaoGovernoImpacto;
            meta.DescricaoSetorReguladoBeneficio = dto.DescricaoSetorReguladoBeneficio;
            meta.DescricaoSetorReguladoImpacto = dto.DescricaoSetorReguladoImpacto;
            meta.StatusMetaInterna = dto.StatusMetaInterna;

            meta.Save();

            if (meta.Situacoes == null || meta.Situacoes.Count == 0)
            {
                StatusMeta status = StatusMeta.GetRepository().Get((int)StatusMetaEnum.EmCadastro);
                Domain.Entities.SituacaoMeta situacao = new Domain.Entities.SituacaoMeta(status, meta);
                situacao.DataAlteracao = DateTime.Now;
                situacao.NoUsuario = dto.NomeUsuario;
                situacao.Save();
            }
            return meta.CodSeqMeta;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOMeta ObterMeta(int codSeqMeta)
        {
            return Meta.GetRepository().Get(codSeqMeta).Transform<DTOMeta>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOMetaDetalhada ObterMetaDetalhada(int codSeqMeta)
        {
            DTOMetaDetalhada meta = Meta.GetRepository().Get(codSeqMeta).Transform<DTOMetaDetalhada>();
            return meta;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOSituacaoMeta ObterSituacaoMetaAtual(DTOMetaDetalhada meta)
        {
            int CodSeqMeta = meta.CodSeqMeta;
            DTOSituacaoMeta situacaoMeta = Domain.Entities.SituacaoMeta.GetRepository().Get(CodSeqMeta).Transform<DTOSituacaoMeta>();
            return situacaoMeta;
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarMeta(int codSeqMeta)
        {
            var item = Meta.GetRepository().Get(codSeqMeta);
            item.Delete();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOAcaoRelatorio> ListaGlobalAtividade(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize)
        {
            return Acao.GetRepository().ListaGlobalAtividade(alinhadasPE, alinhadasPPA, alinhadasMI, AnoExercicio,
                Unidade, codigoPeriodo, codigoTipoMeta, MetaInterna, TipoInstrumento, startIndex, pageSize).Transform<PageMessage<DTOAcaoRelatorio>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOAcaoRelatorio> ListaDetalheAtividade(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize)
        {
            return Acao.GetRepository().ListaDetalheAtividade(alinhadasPE, alinhadasPPA, alinhadasMI, AnoExercicio,
                Unidade, codigoPeriodo, codigoTipoMeta, MetaInterna, TipoInstrumento, startIndex, pageSize).Transform<PageMessage<DTOAcaoRelatorio>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOIndicadorRelatorio> ListaGlobalIndicador(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize)
        {
            return Indicador.GetRepository().ListaGlobalIndicador(alinhadasPE, alinhadasPPA, alinhadasMI, AnoExercicio,
                Unidade, codigoPeriodo, codigoTipoMeta, MetaInterna, TipoInstrumento, startIndex, pageSize).Transform<PageMessage<DTOIndicadorRelatorio>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOIndicadorRelatorio> ListaDetalheIndicador(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize)
        {
            return Indicador.GetRepository().ListaDetalheIndicador(alinhadasPE, alinhadasPPA, alinhadasMI, AnoExercicio,
                Unidade, codigoPeriodo, codigoTipoMeta, MetaInterna, TipoInstrumento, startIndex, pageSize).Transform<PageMessage<DTOIndicadorRelatorio>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOMetaRelatorio> ListaGlobalMeta(bool VerAlinhadasPE, bool VerAlinhadasPPA, bool VerAlinhadasMI, bool VerAdministratiVas,
            bool VerFiscalizacoes, bool VerRegulacoes, string unidade, int AnoExercicio, int CodigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize)
        {
            return Meta.GetRepository().ListaGlobalMeta(VerAlinhadasPE, VerAlinhadasPPA, VerAlinhadasMI, VerAdministratiVas,
            VerFiscalizacoes, VerRegulacoes, unidade, AnoExercicio, CodigoTipoMeta, MetaInterna, TipoInstrumento, startIndex, pageSize).Transform<PageMessage<DTOMetaRelatorio>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOMetaRelatorio> ListaDetalheMeta(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int codigoTipoMeta,
            string Unidade, int AnoExercicio, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize)
        {
            return Meta.GetRepository().ListaDetalheMeta(alinhadasPE, alinhadasPPA, alinhadasMI, codigoTipoMeta, Unidade,
                AnoExercicio, MetaInterna, TipoInstrumento, startIndex, pageSize).Transform<PageMessage<DTOMetaRelatorio>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOMeta> ListarMetas(int startIndex, int pageSize)
        {
            return Meta.GetRepository().ListarMetasPorUnidadeMeta("", 0, 0, 0, startIndex, pageSize)
                .Transform<PageMessage<DTOMeta>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOMeta> ListarMetasPorUnidadeMeta(string Unidade, int AnoExercicio, int MetaInterna, int StatusMeta, int startIndex, int pageSize)
        {
            return Meta.GetRepository().ListarMetasPorUnidadeMeta(Unidade, AnoExercicio, MetaInterna, StatusMeta, startIndex, pageSize)
                .Transform<PageMessage<DTOMeta>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOMeta> ListarMetasPorUnidade(int codigoUnidade, int AnoExercicio, int startIndex, int pageSize)
        {
            return Meta.GetRepository().ListarMetasPorUnidade(codigoUnidade, AnoExercicio, startIndex, pageSize)
                .Transform<PageMessage<DTOMeta>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOTipoMeta> ListarTipoMetas()
        {
            return TipoMeta.GetRepository().ListAll().TransformList<DTOTipoMeta>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOStatusMeta> ListarStatusMetas()
        {
            return StatusMeta.GetRepository().ListAll().TransformList<DTOStatusMeta>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOIniciativa> ListarIniciativaPorTipo(int tipoIniciativa)
        {
            return Iniciativa.GetRepository().ListAll().Where(i => i.TipoIniciativa.CodigoSeqTipoIniciativa == tipoIniciativa).TransformList<DTOIniciativa>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOIniciativa> ListarIniciativaPorTipoEUnidadeAtivos(TiposAlinhamento tipoIniciativa, int codigoUnidade)
        {
            return Iniciativa.GetRepository().ListarIniciativaPorTipoEUnidadeAtivos((int)tipoIniciativa, codigoUnidade).TransformList<DTOIniciativa>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOCorporativoUnidade ObterCorporativoUnidade(int codigoUnidade)
        {
            return CorporativoUnidade.GetRepository().Get(codigoUnidade).Transform<DTOCorporativoUnidade>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOIniciativa ObterIniciativaPorCodigo(int codigoIniciativa)
        {
            return Iniciativa.GetRepository().Get(codigoIniciativa).Transform<DTOIniciativa>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void AlterarStatusMeta(int codSeqMeta, int codigoStatus, string nomeUsuario, string descricaoJustificativaCancelamento, DTOAnexo anexo)
        {
            var meta = Meta.GetRepository().Get(codSeqMeta);
            StatusMeta status = StatusMeta.GetRepository().Get(codigoStatus);

            Domain.Entities.SituacaoMeta situacao = new Domain.Entities.SituacaoMeta(status, meta);
            
            situacao.DataAlteracao = DateTime.Now;
            situacao.NoUsuario = nomeUsuario;
            situacao.DescricaoJustificativaCancelamento = descricaoJustificativaCancelamento;
            foreach (var item in situacao.Meta.Acoes)
            {
                if (codigoStatus == 4)
                {
                    item.StatusAcao = 2;
                }
                else if (codigoStatus == 3)
                {
                    item.StatusAcao = 1;
                }
            }

            foreach (var item in situacao.Meta.Indicadores)
            {
                if (codigoStatus == 4)
                {
                    item.StatusIndicador = 2;
                }
                else if (codigoStatus == 3)
                {
                    item.StatusIndicador = 1;
                }
            }

            if (anexo != null)
            {
                var caminho = anexo.CaminhoAnexo + codSeqMeta + "\\";
                var caminhoCompleto = Path.Combine(caminho, anexo.DescricaoAnexo);

                CriarPasta(caminho);
                GravaArquivo(caminhoCompleto, anexo.Conteudo);

                anexo.CaminhoAnexo = caminhoCompleto;

                var CodSeqAnexo = SalvarAnexo(anexo);

                situacao.Anexo = Anexo.GetRepository().Get(CodSeqAnexo);
            }

            situacao.Save();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarAnexoMeta(int CodigoSeqMeta)
        {
            var item = Meta.GetRepository().Get(CodigoSeqMeta);

            if (item.SituacaoMeta.Anexo != null)
                DeletarAnexo(item.SituacaoMeta.Anexo.CodigoSeqAnexo);

            item.SituacaoMeta.Anexo = null;

            item.Save();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOSituacaoMeta> GetSituacaoMeta(int codSeqMeta)
        {
            return Domain.Entities.SituacaoMeta.GetRepository().GetSituacaoMeta(codSeqMeta).TransformList<DTOSituacaoMeta>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOCorporativoUnidade> ListarCorporativoUnidadeAtivo()
        {
            return CorporativoUnidade.GetRepository().ListarCorporativoUnidadeAtivo().TransformList<DTOCorporativoUnidade>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOFase> ListarFase()
        {
            return Fase.GetRepository().ListAll().TransformList<DTOFase>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOMetaSimplificada> ListarMetaSimplificada()
        {
            return Meta.GetRepository().ListAll().TransformList<DTOMetaSimplificada>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOMetaSimplificada> ListarMetaSimplificadaPorUnidadeEAno(int codigoUnidade, int ano)
        {
            return Meta.GetRepository().ListarMetaSimplificadaPorUnidadeEAno(codigoUnidade, ano.ToShort()).TransformList<DTOMetaSimplificada>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOExercicio> ListarExercicioEmAcompanhamento()
        {
            return Exercicio.GetRepository().ListarExercicioEmAcompanhamento().TransformList<DTOExercicio>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOExercicio> ListarExercicioEmPlanejamento()
        {
            return Exercicio.GetRepository().ListarExercicioEmPlanejamento().TransformList<DTOExercicio>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOExercicio> ListarExercicioExcetoPlanejamento()
        {
            return Exercicio.GetRepository().ListarExercicioExcetoPlanejamento().TransformList<DTOExercicio>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOExercicio> ListarAnoExercicio()
        {
            return Exercicio.GetRepository().ListarAnoExercicio().TransformList<DTOExercicio>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOExercicio ObterFaseExercicioPorAno(Int16 ano)
        {
            return Exercicio.GetRepository().ObterFaseExercicioPorAno(ano).Transform<DTOExercicio>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOTipoResultado> ListarTipoResultado()
        {
            return TipoResultado.GetRepository().ListAll().TransformList<DTOTipoResultado>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOCorporativoUnidade> ListarCorporativoUnidadeAtivoPorExercicio(int ano)
        {
            Exercicio exercicio = Exercicio.GetRepository().ObterExercicioPorAno(ano.ToShort());
            if (exercicio != null && exercicio.Fase.CodigoSeqFase == (int)FaseEnum.Encerrado)
            {
                return CorporativoUnidade.GetRepository().ListarCorporativoUnidadePorExercicio(ano.ToShort()).TransformList<DTOCorporativoUnidade>();
            }
            else
            {
                return CorporativoUnidade.GetRepository().ListarCorporativoUnidadeAtivo().TransformList<DTOCorporativoUnidade>();
            }
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOCorporativoUnidade> ListarCorporativoUnidadePorExercicio(int ano)
        {
            var unids = CorporativoUnidade.GetRepository().ListarCorporativoUnidadePorExercicio(ano.ToShort()).TransformList<DTOCorporativoUnidade>();

            foreach (var u in unids)
                if (!u.Ativo)
                    if (unids.Where(un => (un.DescricaoUnidade == u.DescricaoUnidade && un.Ativo) || un.DescricaoUnidade == u.DescricaoUnidade + " - Inativa").Count() > 0)
                        u.DescricaoUnidade = "Remover";
                    else
                        u.DescricaoUnidade = u.DescricaoUnidade + " - Inativa";

            unids = unids.Where(u => u.DescricaoUnidade != "Remover").ToList();

            return unids;
        }

        [Transaction(TransactionPropagation.Required)]
        public void AtualizarUnidadesAtivas(List<DTOCorporativoUnidade> unidadesAtivas)
        {
            var unidades = CorporativoUnidade.GetRepository().ListarCorporativoUnidadeAtivo();
            foreach (var item in unidades)
            {
                item.Ativo = false;
                item.Save();
            }

            unidades = CorporativoUnidade.GetRepository().ListarCorporativoUnidadePorCodigos(unidadesAtivas.Select(i => i.CodigoUnidade).ToList());
            foreach (var item in unidades)
            {
                item.Ativo = true;
                item.Save();
            }

            if (unidadesAtivas.Count() > unidades.Count())
            {
                foreach (var item in unidadesAtivas)
                {
                    if (!unidades.Any(i => i.CodigoUnidade == item.CodigoUnidade))
                    {
                        CorporativoUnidade novaUnidade = new CorporativoUnidade(item.CodigoUnidade);
                        novaUnidade.CodigoUnidadePai = item.CodigoUnidadePai;
                        novaUnidade.DescricaoUnidade = item.DescricaoUnidade;
                        novaUnidade.Ativo = true;
                        novaUnidade.Save();
                    }
                }
            }
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public bool ValidarUnidadesAtivasDasMetas(List<DTOCorporativoUnidade> unidadesAtivas)
        {
            var exercicios = Exercicio.GetRepository().ListAll();
            foreach (var exercicio in exercicios)
            {
                if (exercicio.Fase.CodigoSeqFase != (int)FaseEnum.Encerrado)
                {
                    var unidadesUtilizadas = CorporativoUnidade.GetRepository().ListarCorporativoUnidadePorExercicio(exercicio.Ano);
                    foreach (var item in unidadesUtilizadas.Where(u => u.Ativo))
                    {
                        if (!unidadesAtivas.Any(i => i.CodigoUnidade == item.CodigoUnidade))
                            return false;
                    }
                }
            }

            return true;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public byte[] GerarRelatorioAcompanhamento(int anoExercicio, string Area, int codigoPeriodo, bool periodoAcumulado, int codigoTipoMeta, bool alinhamnetoPE, bool alinhamnetoPPA, bool alinhamnetoMI, int codigoTipoInstrumento, int metaInterna)
        {
            Func<List<VOCalculoAcumuladoTrimestre>, byte[]> gerarImagemGrafico = (dados) =>
            {
                // Aqui nós adicionamos +2 registros vazios para representar o trimestre 0 (zero)
                // e o trimestre 5. Ambos não são trimestres válidos, mas são necessários
                // para que a linha no gráfico que representa o valor alvo cubra toda a proporção
                // das barras. Assim sempre haverá uma barra no início e no fim que tem seus
                // valores zerados, e as barras reais ficarão entre, criando assim uma margem
                // que faz com que o gráfico fique mais "assimétrico"!
                var items = new List<VOCalculoAcumuladoTrimestre>(dados);
                //{
                //    var copia = dados.First();

                //    items.Add(new VOCalculoAcumuladoTrimestre(EnumTipoResultado.Acumulado, 0, copia.ValorAlvo));
                //    items.Add(new VOCalculoAcumuladoTrimestre(EnumTipoResultado.Acumulado, 5, copia.ValorAlvo));

                //    items.AddRange(dados);
                //};

                var ds = new Dictionary<string, System.Collections.IEnumerable>();
                {
                    ds.Add("dsDadosGrafico", items.OrderBy(o => o.NumeroTrimestre));
                }

                var reportPath = @"Reports\RelatorioAcompanhamentoGrafico.rdlc";
                var bytes = ReportViewerHelper.ExportReport(ReportViewerHelper.ReportType.Image, reportPath, ds);

                return bytes;
            };

            Exercicio exercicio = Exercicio.GetRepository().ObterExercicioPorAno(anoExercicio.ToShort());

            return exercicio.GerarRelatorioAcompanhamento(Area, codigoPeriodo, periodoAcumulado, codigoTipoMeta, alinhamnetoPE, alinhamnetoPPA, alinhamnetoMI, codigoTipoInstrumento, metaInterna, gerarImagemGrafico);
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]

        public PageMessage<DTOMeta> ListarInstrumentosPorUnidade(int codigoUnidade, int AnoExercicio, int startIndex, int pageSize)
        {
            return Meta.GetRepository().ListarMetasPorUnidade(codigoUnidade, AnoExercicio, startIndex, pageSize)
                .Transform<PageMessage<DTOMeta>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOTipoObjetivo> ListarTiposObjetivo()
        {
            return TipoObjetivo.GetRepository().ListAll().TransformList<DTOTipoObjetivo>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOObjetivo> ListarObjetivos()
        {
            return Objetivo.GetRepository().ListAll().TransformList<DTOObjetivo>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOObjetivo> ListarObjetivosPorTipo(EnumTipoObjetivo tipo)
        {
            return Objetivo.GetRepository().ListAll().Where(o => o.TipoObjetivo.CodigoSeqTipoObjetivo == (int)tipo && o.Ativo == true).TransformList<DTOObjetivo>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOObjetivo> ListarObjetivosPorTipoESituacao(int tipoObjetivo, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            return Objetivo.GetRepository().ListarObjetivosPorTipo(tipoObjetivo, ativo, startIndex, pageSize, orderProperty, orderAscending).Transform<PageMessage<DTOObjetivo>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOPerspectiva> ListarPerspectivas()
        {
            return Perspectiva.GetRepository().ListAll().TransformList<DTOPerspectiva>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOPerspectiva> ListarPerspectivasPorSituacao(string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            return Perspectiva.GetRepository().ListarPerspectivasPorSituacao(ativo, startIndex, pageSize, orderProperty, orderAscending).Transform<PageMessage<DTOPerspectiva>>();
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarPerspectiva(DTOPerspectiva dto)
        {
            Perspectiva Perspectiva;

            if (dto.CodigoSeqPerspectiva > 0)
            {
                Perspectiva = Perspectiva.GetRepository().Get(dto.CodigoSeqPerspectiva);
                if (Perspectiva == null)
                    throw new Exception("Perspectiva não encontrado.");
            }
            else
            {
                Perspectiva = new Perspectiva();
                Perspectiva.CodigoSeqPerspectiva = Perspectiva.GetRepository().ListAll().OrderBy(o => o.CodigoSeqPerspectiva).LastOrDefault().CodigoSeqPerspectiva + 1;
            }

            Perspectiva.NuPerspectiva = dto.NuPerspectiva;
            Perspectiva.DescricaoPerspectiva = dto.DescricaoPerspectiva;
            Perspectiva.Ativo = dto.Ativo;

            Perspectiva.Save();

            return Perspectiva.CodigoSeqPerspectiva;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOPerspectiva ObterPerspectiva(int CodigoSeqPerspectiva)
        {
            return Perspectiva.GetRepository().Get(CodigoSeqPerspectiva).Transform<DTOPerspectiva>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarPerspectiva(int CodigoSeqPerspectiva)
        {
            var item = Perspectiva.GetRepository().Get(CodigoSeqPerspectiva);
            item.Delete();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOTema> ListarTemas()
        {
            return Tema.GetRepository().ListAll().TransformList<DTOTema>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOTema> ListarTemasPorPerspectiva(int codPerspectiva)
        {
            return Tema.GetRepository().ListAll().Where(t => t.Perspectiva.CodigoSeqPerspectiva == codPerspectiva).TransformList<DTOTema>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOTema> ListarTemasPorPerspectivasESituacao(int perspectiva, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            return Tema.GetRepository().ListarTemasPorPerspectivasESituacao(perspectiva, ativo, startIndex, pageSize, orderProperty, orderAscending).Transform<PageMessage<DTOTema>>();
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarTema(DTOTema dto)
        {
            Tema tema;

            if (dto.CodigoSeqTema > 0)
            {
                tema = Tema.GetRepository().Get(dto.CodigoSeqTema);
                if (tema == null)
                    throw new Exception("Tema não encontrado.");
            }
            else
            {
                tema = new Tema();
                tema.CodigoSeqTema = Tema.GetRepository().ListAll().OrderBy(o => o.CodigoSeqTema).LastOrDefault().CodigoSeqTema + 1;
            }

            if (dto.Perspectiva != null && dto.Perspectiva.CodigoSeqPerspectiva > 0)
                tema.Perspectiva = Perspectiva.GetRepository().Get(dto.Perspectiva.CodigoSeqPerspectiva);
            else
                throw new Exception("Não foi possível encontrar o tipo do objetivo.");

            tema.NuTema = dto.NuTema;
            tema.DescricaoTema = dto.DescricaoTema;
            tema.Ativo = dto.Ativo;

            tema.Save();

            return tema.CodigoSeqTema;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOTema ObterTema(int CodigoSeqTema)
        {
            return Tema.GetRepository().Get(CodigoSeqTema).Transform<DTOTema>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarTema(int CodigoSeqTema)
        {
            var item = Tema.GetRepository().Get(CodigoSeqTema);
            item.Delete();
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarObjetivo(DTOObjetivo dto)
        {
            Objetivo objetivo;

            if (dto.CodigoSeqObjetivo > 0)
            {
                objetivo = Objetivo.GetRepository().Get(dto.CodigoSeqObjetivo);
                if (objetivo == null)
                    throw new Exception("Objetivo não encontrado.");
            }
            else
            {
                objetivo = new Objetivo();
                objetivo.CodigoSeqObjetivo = Objetivo.GetRepository().ListAll().OrderBy(o => o.CodigoSeqObjetivo).LastOrDefault().CodigoSeqObjetivo + 1;
            }

            if (dto.TipoObjetivo != null && dto.TipoObjetivo.CodigoSeqTipoObjetivo > 0)
                objetivo.TipoObjetivo = TipoObjetivo.GetRepository().Get(dto.TipoObjetivo.CodigoSeqTipoObjetivo);
            else
                throw new Exception("Não foi possível encontrar o tipo do objetivo.");

            if (dto.Tema != null && dto.Tema.CodigoSeqTema > 0)
                objetivo.Tema = Tema.GetRepository().Get(dto.Tema.CodigoSeqTema);
            else
                objetivo.Tema = null;

            objetivo.NuObjetivo = dto.NuObjetivo;
            objetivo.NomeObjetivo = dto.NomeObjetivo;
            objetivo.DescricaoObjetivo = dto.DescricaoObjetivo;
            objetivo.Ativo = dto.Ativo;

            objetivo.Save();

            return objetivo.CodigoSeqObjetivo;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOObjetivo ObterObjetivo(int CodigoSeqObjetivo)
        {
            return Objetivo.GetRepository().Get(CodigoSeqObjetivo).Transform<DTOObjetivo>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarObjetivo(int CodigoSeqObjetivo)
        {
            var item = Objetivo.GetRepository().Get(CodigoSeqObjetivo);
            item.Delete();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOTipoIniciativa> ListarTiposIniciativa()
        {
            return TipoIniciativa.GetRepository().ListAll().TransformList<DTOTipoIniciativa>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOIniciativa> ListarTiposIniciativaFiltro(int tipoIniciativa)
        {
            return Iniciativa.GetRepository().ListarTiposIniciativaFiltro(tipoIniciativa).TransformList<DTOIniciativa>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOIniciativa> ListarIniciativaPorTipoEUnidade(int tipoIniciativa, string unidade, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var inis = Iniciativa.GetRepository().ListarIniciativaPorTipoEUnidade(tipoIniciativa, unidade, ativo, startIndex, pageSize, orderProperty, orderAscending).Transform<PageMessage<DTOIniciativa>>();

            foreach (var i in inis)
                if (!i.CorporativoUnidade.Ativo && !i.CorporativoUnidade.DescricaoUnidade.Contains("Inativa"))
                    i.CorporativoUnidade.DescricaoUnidade = i.CorporativoUnidade.DescricaoUnidade + " - Inativa";

            return inis;
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarIniciativa(DTOIniciativa dto)
        {
            Iniciativa iniciativa;

            if (dto.CodigoSeqIniciativa > 0)
            {
                iniciativa = Iniciativa.GetRepository().Get(dto.CodigoSeqIniciativa);
                if (iniciativa == null)
                    throw new Exception("Objetivo não encontrado.");
            }
            else
            {
                iniciativa = new Iniciativa();
                iniciativa.CodigoSeqIniciativa = Iniciativa.GetRepository().ListAll().OrderBy(i => i.CodigoSeqIniciativa).LastOrDefault().CodigoSeqIniciativa + 1;
            }

            if (dto.TipoIniciativa != null && dto.TipoIniciativa.CodigoSeqTipoIniciativa > 0)
                iniciativa.TipoIniciativa = TipoIniciativa.GetRepository().Get(dto.TipoIniciativa.CodigoSeqTipoIniciativa);
            else
                throw new Exception("Não foi possível encontrar o tipo da iniciativa.");

            if (dto.CorporativoUnidade != null && dto.CorporativoUnidade.CodigoUnidade > 0)
                iniciativa.CorporativoUnidade = CorporativoUnidade.GetRepository().Get(dto.CorporativoUnidade.CodigoUnidade);
            else
                throw new Exception("Não foi possível encontrar a área da iniciativa.");

            iniciativa.DescricaoIniciativa = dto.DescricaoIniciativa;
            iniciativa.ProgramaTematico = dto.ProgramaTematico;
            iniciativa.TpIniciativaProjeto = dto.TpIniciativaProjeto;
            iniciativa.Ativo = dto.Ativo;

            if (iniciativa.CodigoSeqIniciativa == 0 && dto.Anexo != null)
                iniciativa.Save();

            if (dto.Anexo != null && dto.Anexo.CodigoSeqAnexo <= 0)
            {
                var caminho = dto.Anexo.CaminhoAnexo + iniciativa.CodigoSeqIniciativa + "\\";
                var caminhoCompleto = Path.Combine(caminho, dto.Anexo.DescricaoAnexo);

                CriarPasta(caminho);
                GravaArquivo(caminhoCompleto, dto.Anexo.Conteudo);

                dto.Anexo.CaminhoAnexo = caminhoCompleto;

                var CodSeqAnexo = SalvarAnexo(dto.Anexo);

                iniciativa.Anexo = Anexo.GetRepository().Get(CodSeqAnexo);
            }

            iniciativa.Save();

            return iniciativa.CodigoSeqIniciativa;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOIniciativa ObterIniciativa(int CodigoSeqIniciativa)
        {
            return Iniciativa.GetRepository().Get(CodigoSeqIniciativa).Transform<DTOIniciativa>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarIniciativa(int CodigoSeqIniciativa)
        {
            var item = Iniciativa.GetRepository().Get(CodigoSeqIniciativa);
            item.Delete();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarAnexoIniciativa(int CodigoSeqIniciativa)
        {
            var item = Iniciativa.GetRepository().Get(CodigoSeqIniciativa);

            if (item.Anexo != null)
                DeletarAnexo(item.Anexo.CodigoSeqAnexo);

            item.Anexo = null;

            item.Save();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOInstrumento> ListarInstrumentoPorTipoAtivos(TiposInstrumento tipoInstrumento)
        {
            return Instrumento.GetRepository().ListAll().Where(i => i.TipoInstrumento.CodigoSeqTipoInstrumento == (int)tipoInstrumento && i.Ativo == true).TransformList<DTOInstrumento>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOInstrumento> ListarInstrumentoPorTipoEUnidadeAtivos(TiposInstrumento tipoInstrumento, int codigoUnidade)
        {
            return Instrumento.GetRepository().ListAll().Where(i => i.TipoInstrumento.CodigoSeqTipoInstrumento == (int)tipoInstrumento && i.CorporativoUnidade != null && i.CorporativoUnidade.CodigoUnidade == codigoUnidade && i.Ativo == true).TransformList<DTOInstrumento>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOTipoInstrumento> ListarTiposInstrumento()
        {
            return TipoInstrumento.GetRepository().ListAll().TransformList<DTOTipoInstrumento>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOInstrumento> ListarInstrumentoPorTipoEUnidade(int tipoInstrumento, int codigoUnidade, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            return Instrumento.GetRepository().ListarInstrumentoPorTipoEUnidade(tipoInstrumento, codigoUnidade, ativo, startIndex, pageSize, orderProperty, orderAscending).Transform<PageMessage<DTOInstrumento>>();
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarInstrumento(DTOInstrumento dto)
        {
            Instrumento instrumento;

            if (dto.CodigoSeqInstrumento > 0)
            {
                instrumento = Instrumento.GetRepository().Get(dto.CodigoSeqInstrumento);
                if (instrumento == null)
                    throw new Exception("Objetivo não encontrado.");
            }
            else
                instrumento = new Instrumento();

            if (dto.TipoInstrumento != null && dto.TipoInstrumento.CodigoSeqTipoInstrumento > 0)
                instrumento.TipoInstrumento = TipoInstrumento.GetRepository().Get(dto.TipoInstrumento.CodigoSeqTipoInstrumento);
            else
                throw new Exception("Não foi possível encontrar o tipo de Instrumento.");

            if (dto.CorporativoUnidade != null && dto.CorporativoUnidade.CodigoUnidade > 0)
                instrumento.CorporativoUnidade = CorporativoUnidade.GetRepository().Get(dto.CorporativoUnidade.CodigoUnidade);
            else
                throw new Exception("Não foi possível encontrar a área do Instrumento.");

            if (dto.Objetivo != null && dto.Objetivo.CodigoSeqObjetivo > 0)
                instrumento.Objetivo = Objetivo.GetRepository().Get(dto.Objetivo.CodigoSeqObjetivo);
            else
                instrumento.Objetivo = null;

            if (dto.ObjetivoEixoTematico != null && dto.ObjetivoEixoTematico.CodigoSeqObjetivo > 0)
                instrumento.ObjetivoEixoTematico = Objetivo.GetRepository().Get(dto.ObjetivoEixoTematico.CodigoSeqObjetivo);
            else
                instrumento.ObjetivoEixoTematico = null;

            instrumento.DescricaoInstrumento = dto.DescricaoInstrumento;
            instrumento.Ativo = dto.Ativo;

            instrumento.Save();

            return instrumento.CodigoSeqInstrumento;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOInstrumento ObterInstrumento(int CodigoSeqInstrumento)
        {
            return Instrumento.GetRepository().Get(CodigoSeqInstrumento).Transform<DTOInstrumento>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarInstrumento(int CodigoSeqInstrumento)
        {
            var item = Instrumento.GetRepository().Get(CodigoSeqInstrumento);
            item.Delete();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOTipoAcompanhamento> ListarTiposAcompanhamento()
        {
            return TipoAcompanhamento.GetRepository().ListAll().TransformList<DTOTipoAcompanhamento>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOTipoAcompanhamento> ListarTiposAcompanhamentoPorTipoESituacao(string tipoAcompanhamento, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            return TipoAcompanhamento.GetRepository().ListarTiposAcompanhamentoPorTipoESituacao(tipoAcompanhamento, ativo, startIndex, pageSize, orderProperty, orderAscending).Transform<PageMessage<DTOTipoAcompanhamento>>();
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarTipoAcompanhamento(DTOTipoAcompanhamento dto)
        {
            TipoAcompanhamento tipoAcompanhamento;

            if (dto.CodigoSeqTipoAcompanhamento > 0)
            {
                tipoAcompanhamento = TipoAcompanhamento.GetRepository().Get(dto.CodigoSeqTipoAcompanhamento);
                if (tipoAcompanhamento == null)
                    throw new Exception("Tipo de Acompanhamento não encontrado.");
            }
            else
                tipoAcompanhamento = new TipoAcompanhamento();

            tipoAcompanhamento.DescricaoTipoAcompanhamento = dto.DescricaoTipoAcompanhamento;
            tipoAcompanhamento.Ativo = dto.Ativo;

            tipoAcompanhamento.Save();

            return tipoAcompanhamento.CodigoSeqTipoAcompanhamento;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOTipoAcompanhamento ObterTipoAcompanhamento(int CodigoSeqTipoAcompanhamento)
        {
            return TipoAcompanhamento.GetRepository().Get(CodigoSeqTipoAcompanhamento).Transform<DTOTipoAcompanhamento>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarTipoAcompanhamento(int CodigoSeqTipoAcompanhamento)
        {
            var item = TipoAcompanhamento.GetRepository().Get(CodigoSeqTipoAcompanhamento);
            item.Delete();
        }

        public void CriarPasta(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public void GravaArquivo(string caminho, byte[] conteudo)
        {
            if (File.Exists(caminho))
                DeletaArquivo(caminho);

            System.IO.File.WriteAllBytes(caminho, conteudo);
        }

        public void DeletaArquivo(string caminho)
        {
            if (File.Exists(caminho))
            {
                File.Delete(caminho);
            }
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarAnexo(DTOAnexo dto)
        {
            Anexo anexo;

            if (dto.CodigoSeqAnexo > 0)
            {
                anexo = Anexo.GetRepository().Get(dto.CodigoSeqAnexo);
                if (anexo == null)
                    throw new Exception("Anexo não encontrado.");
            }
            else
                anexo = new Anexo();

            anexo.DescricaoAnexo = dto.DescricaoAnexo;
            anexo.CaminhoAnexo = dto.CaminhoAnexo;
            anexo.DataInclusao = DateTime.Now;

            anexo.Save();

            return anexo.CodigoSeqAnexo;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOAnexo ObterAnexo(int CodigoSeqAnexo)
        {
            return Anexo.GetRepository().Get(CodigoSeqAnexo).Transform<DTOAnexo>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarAnexo(int CodigoSeqAnexo)
        {
            var item = Anexo.GetRepository().Get(CodigoSeqAnexo);

            DeletaArquivo(item.CaminhoAnexo);

            item.Delete();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOStatusAcompanhamento ObterStatusAcompanhamento(EnumStatusAcompanhamento status)
        {
            return StatusAcompanhamento.GetRepository().Get((int)status).Transform<DTOStatusAcompanhamento>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOStatusEncaminhamento ObterStatusEncaminhamento(EnumStatusEncaminhamento status)
        {
            return StatusEncaminhamento.GetRepository().Get((int)status).Transform<DTOStatusEncaminhamento>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOAcompanhamento> ListarAcompanhamentosPorTipoAnoEStatus(int tipoAcompanhamento, short ano, int status, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            return Acompanhamento.GetRepository().ListarAcompanhamentosPorTipoAnoEStatus(tipoAcompanhamento, ano, status, startIndex, pageSize, orderProperty, orderAscending).Transform<PageMessage<DTOAcompanhamento>>();
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarAcompanhamento(DTOAcompanhamento dto)
        {
            Acompanhamento acompanhamento;

            if (dto.CodigoSeqAcompanhamento > 0)
            {
                acompanhamento = Acompanhamento.GetRepository().Get(dto.CodigoSeqAcompanhamento);
                if (acompanhamento == null)
                    throw new Exception("Acompanhamento não encontrado.");
            }
            else
                acompanhamento = new Acompanhamento();

            if (dto.TipoAcompanhamento != null && dto.TipoAcompanhamento.CodigoSeqTipoAcompanhamento > 0)
                acompanhamento.TipoAcompanhamento = TipoAcompanhamento.GetRepository().Get(dto.TipoAcompanhamento.CodigoSeqTipoAcompanhamento);
            else
                throw new Exception("Não foi possível encontrar o tipo do acompanhamento.");

            if (dto.Exercicio != null && dto.Exercicio.CodigoSeqExercicio > 0)
                acompanhamento.Exercicio = Exercicio.GetRepository().Get(dto.Exercicio.CodigoSeqExercicio);
            else
                throw new Exception("Não foi possível encontrar o Exercício.");

            if (dto.StatusAcompanhamento != null && dto.StatusAcompanhamento.CodigoSeqStatusAcompanhamento > 0)
                acompanhamento.StatusAcompanhamento = StatusAcompanhamento.GetRepository().Get(dto.StatusAcompanhamento.CodigoSeqStatusAcompanhamento);
            else
                throw new Exception("Não foi possível encontrar o Status do Acompanhamento.");

            acompanhamento.DataAcompanhamento = dto.DataAcompanhamento;

            acompanhamento.Save();

            if (dto.Anexo != null && dto.Anexo.CodigoSeqAnexo <= 0)
            {
                var caminho = dto.Anexo.CaminhoAnexo + acompanhamento.CodigoSeqAcompanhamento + "\\";
                var caminhoCompleto = Path.Combine(caminho, dto.Anexo.DescricaoAnexo);

                CriarPasta(caminho);
                GravaArquivo(caminhoCompleto, dto.Anexo.Conteudo);

                dto.Anexo.CaminhoAnexo = caminhoCompleto;

                var CodSeqAnexo = SalvarAnexo(dto.Anexo);

                acompanhamento.Anexo = Anexo.GetRepository().Get(CodSeqAnexo);

                acompanhamento.Save();
            }

            return acompanhamento.CodigoSeqAcompanhamento;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOAcompanhamento ObterAcompanhamento(int CodigoSeqAcompanhamento)
        {
            return Acompanhamento.GetRepository().Get(CodigoSeqAcompanhamento).Transform<DTOAcompanhamento>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void AlterarStatusAcompanhamento(int CodigoSeqAcompanhamento, int status)
        {
            var item = Acompanhamento.GetRepository().Get(CodigoSeqAcompanhamento);

            if (status > 0)
                item.StatusAcompanhamento = StatusAcompanhamento.GetRepository().Get(status);

            item.Save();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarAcompanhamento(int CodigoSeqAcompanhamento)
        {
            var item = Acompanhamento.GetRepository().Get(CodigoSeqAcompanhamento);
            item.Delete();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarAnexoAcompanhamento(int CodigoSeqAcompanhamento)
        {
            var item = Acompanhamento.GetRepository().Get(CodigoSeqAcompanhamento);

            if (item.Anexo != null)
                DeletarAnexo(item.Anexo.CodigoSeqAnexo);

            item.Anexo = null;

            item.Save();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOEncaminhamento> ListarEncaminhamentosDoAcompanhamento(int CodigoSeqAcompanhamento)
        {
            return Encaminhamento.GetRepository().ListAll().Where(e => e.Acompanhamento.CodigoSeqAcompanhamento == CodigoSeqAcompanhamento)
                .OrderBy(e => e.CorporativoUnidade.DescricaoUnidade).ThenBy(e => e.PrazoAtendimento).TransformList<DTOEncaminhamento>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOEncaminhamento> ListarEncaminhamentosPorAcompanhamento(int CodigoSeqAcompanhamento, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            return Encaminhamento.GetRepository().ListarEncaminhamentosPorAcompanhamento(CodigoSeqAcompanhamento, startIndex, pageSize, orderProperty, orderAscending).Transform<PageMessage<DTOEncaminhamento>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOEncaminhamento> ListarEncaminhamentosPorTipoUnidadeEStatus(int tipoAcompanhamento, string unidade, int status, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            var encs = Encaminhamento.GetRepository().ListarEncaminhamentosPorTipoUnidadeEStatus(tipoAcompanhamento, unidade, status, startIndex, pageSize, orderProperty, orderAscending).Transform<PageMessage<DTOEncaminhamento>>();
            
            foreach (var e in encs)
                if (!e.CorporativoUnidade.Ativo && !e.CorporativoUnidade.DescricaoUnidade.Contains("Inativa"))
                    e.CorporativoUnidade.DescricaoUnidade = e.CorporativoUnidade.DescricaoUnidade + " - Inativa";

            return encs;
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarEncaminhamento(DTOEncaminhamento dto)
        {
            Encaminhamento encaminhamento;

            if (dto.CodigoSeqEncaminhamento > 0)
            {
                encaminhamento = Encaminhamento.GetRepository().Get(dto.CodigoSeqEncaminhamento);
                if (encaminhamento == null)
                    throw new Exception("Encaminhamento não encontrado.");
            }
            else
                encaminhamento = new Encaminhamento();

            if (dto.Acompanhamento != null && dto.Acompanhamento.CodigoSeqAcompanhamento > 0)
                encaminhamento.Acompanhamento = Acompanhamento.GetRepository().Get(dto.Acompanhamento.CodigoSeqAcompanhamento);
            else
                throw new Exception("Não foi possível encontrar o acompanhamento do Encaminhamento.");

            if (dto.CorporativoUnidade != null && dto.CorporativoUnidade.CodigoUnidade > 0)
                encaminhamento.CorporativoUnidade = CorporativoUnidade.GetRepository().Get(dto.CorporativoUnidade.CodigoUnidade);
            else
                throw new Exception("Não foi possível encontrar a Área do encaminhamento.");

            if (dto.StatusEncaminhamento != null && dto.StatusEncaminhamento.CodigoSeqStatusEncaminhamento > 0)
                encaminhamento.StatusEncaminhamento = StatusEncaminhamento.GetRepository().Get(dto.StatusEncaminhamento.CodigoSeqStatusEncaminhamento);
            else
                throw new Exception("Não foi possível encontrar o Status do Encaminhamento.");

            if (dto.Meta != null && dto.Meta.CodSeqMeta > 0)
                encaminhamento.Meta = Meta.GetRepository().Get(dto.Meta.CodSeqMeta);

            encaminhamento.DescricaoEncaminhamento = dto.DescricaoEncaminhamento;
            encaminhamento.PrazoAtendimento = dto.PrazoAtendimento;

            encaminhamento.Save();

            return encaminhamento.CodigoSeqEncaminhamento;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOEncaminhamento ObterEncaminhamento(int CodigoSeqEncaminhamento)
        {
            return Encaminhamento.GetRepository().Get(CodigoSeqEncaminhamento).Transform<DTOEncaminhamento>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void AlterarStatusEncaminhamento(int CodigoSeqEncaminhamento, int status)
        {
            var item = Encaminhamento.GetRepository().Get(CodigoSeqEncaminhamento);

            if (status > 0)
                item.StatusEncaminhamento = StatusEncaminhamento.GetRepository().Get(status);

            item.Save();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarEncaminhamento(int CodigoSeqEncaminhamento)
        {
            var item = Encaminhamento.GetRepository().Get(CodigoSeqEncaminhamento);
            item.Delete();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOReporteEncaminhamento> ListarReportesDoEncaminhamento(int CodigoSeqEncaminhamento)
        {
            return ReporteEncaminhamento.GetRepository().ListAll().Where(r => r.Encaminhamento.CodigoSeqEncaminhamento == CodigoSeqEncaminhamento).TransformList<DTOReporteEncaminhamento>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOReporteEncaminhamento> ListarReportesPorEncaminhamento(int CodigoSeqEncaminhamento, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            return ReporteEncaminhamento.GetRepository().ListarReportesPorEncaminhamentos(CodigoSeqEncaminhamento, startIndex, pageSize, orderProperty, orderAscending).Transform<PageMessage<DTOReporteEncaminhamento>>();
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarReporteEncaminhamento(DTOReporteEncaminhamento dto)
        {
            ReporteEncaminhamento reporte;

            if (dto.CodigoSeqReporteEncaminhamento > 0)
            {
                reporte = ReporteEncaminhamento.GetRepository().Get(dto.CodigoSeqReporteEncaminhamento);
                if (reporte == null)
                    throw new Exception("Reporte do encaminhamento não encontrado.");
            }
            else
                reporte = new ReporteEncaminhamento();

            if (dto.Encaminhamento != null && dto.Encaminhamento.CodigoSeqEncaminhamento > 0)
                reporte.Encaminhamento = Encaminhamento.GetRepository().Get(dto.Encaminhamento.CodigoSeqEncaminhamento);
            else
                throw new Exception("Não foi possível encontrar o acompanhamento do Encaminhamento.");

            reporte.DescricaoReporteEncaminhamento = dto.DescricaoReporteEncaminhamento;
            reporte.DataReporte = dto.DataReporte;
            reporte.Usuario = dto.Usuario;

            reporte.Save();

            return reporte.CodigoSeqReporteEncaminhamento;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOReporteEncaminhamento ObterReporteEncaminhamento(int CodigoSeqReporteEncaminhamento)
        {
            return ReporteEncaminhamento.GetRepository().Get(CodigoSeqReporteEncaminhamento).Transform<DTOReporteEncaminhamento>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarReporteEncaminhamento(int CodigoSeqReporteEncaminhamento)
        {
            var item = ReporteEncaminhamento.GetRepository().Get(CodigoSeqReporteEncaminhamento);
            item.Delete();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOIniciativaObjetivo> ListarObjetivosPorIniciativa(int CodigoSeqIniciativa, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            return IniciativaObjetivo.GetRepository().ListarObjetivosPorIniciativa(CodigoSeqIniciativa, startIndex, pageSize, orderProperty, orderAscending).Transform<PageMessage<DTOIniciativaObjetivo>>();
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarIniciativaObjetivo(DTOIniciativaObjetivo dto)
        {
            IniciativaObjetivo objetivo;

            if (dto.CodigoSeqIniciativaObjetivo > 0)
            {
                objetivo = IniciativaObjetivo.GetRepository().Get(dto.CodigoSeqIniciativaObjetivo);
                if (objetivo == null)
                    throw new Exception("Objetivo não encontrado.");
            }
            else
                objetivo = new IniciativaObjetivo();

            if (dto.Iniciativa != null && dto.Iniciativa.CodigoSeqIniciativa > 0)
                objetivo.Iniciativa = Iniciativa.GetRepository().Get(dto.Iniciativa.CodigoSeqIniciativa);
            else
                throw new Exception("Não foi possível encontrar a Iniciativa.");

            if (dto.Objetivo != null && dto.Objetivo.CodigoSeqObjetivo > 0)
                objetivo.Objetivo = Objetivo.GetRepository().Get(dto.Objetivo.CodigoSeqObjetivo);
            else
                throw new Exception("Não foi possível encontrar o Objetivo.");

            objetivo.PercentualParticipacao = dto.PercentualParticipacao;

            objetivo.Save();

            return objetivo.CodigoSeqIniciativaObjetivo;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOIniciativaObjetivo ObterIniciativaObjetivo(int CodigoSeqIniciativaObjetivo)
        {
            return IniciativaObjetivo.GetRepository().Get(CodigoSeqIniciativaObjetivo).Transform<DTOIniciativaObjetivo>();
        }

        [Transaction(TransactionPropagation.Required)]
        public void DeletarIniciativaObjetivo(int CodigoSeqIniciativaObjetivo)
        {
            var item = IniciativaObjetivo.GetRepository().Get(CodigoSeqIniciativaObjetivo);
            item.Delete();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOResultadoAlcancadoRegistro ObterResultadoAlcancadoCompleto(int codSeqResultadoAlcancado, int codSeqObjetivoIniciativa, int codTipo, int anoExercicio)
        {
            DTOResultadoAlcancadoRegistro registro = null;

            if (codSeqResultadoAlcancado > 0)
                registro = ResultadoAlcancado.GetRepository().Get(codSeqResultadoAlcancado).Transform<DTOResultadoAlcancadoRegistro>();
            else
                registro = new DTOResultadoAlcancadoRegistro();

            if (codTipo == (int)EnumTipoResultadoAlcancado.ObjetivoEstrategico)
                registro.Objetivo = Objetivo.GetRepository().Get(codSeqObjetivoIniciativa).Transform<DTOObjetivo>();
            else if (codTipo == (int)EnumTipoResultadoAlcancado.MissaoInstitucional)
                registro.Iniciativa = Iniciativa.GetRepository().Get(codSeqObjetivoIniciativa).Transform<DTOIniciativa>();

            registro.Exercicio = Exercicio.GetRepository().ObterExercicioPorAno(short.Parse(anoExercicio.ToString())).Transform<DTOExercicio>();

            return registro;
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarResultadosAlcancados(DTOResultadoAlcancadoRegistro dto)
        {
            ResultadoAlcancado resultadoAlcancado;

            if (dto.CodigoSeqResultadoAlcancado > 0)
            {
                resultadoAlcancado = ResultadoAlcancado.GetRepository().Get(dto.CodigoSeqResultadoAlcancado);
                if (resultadoAlcancado == null)
                    throw new Exception("resultado Alcançado não encontrado.");
            }
            else
                resultadoAlcancado = new ResultadoAlcancado();

            resultadoAlcancado.Exercicio = Exercicio.GetRepository().Get(dto.Exercicio.CodigoSeqExercicio);
            resultadoAlcancado.CorporativoUnidade = CorporativoUnidade.GetRepository().Get(dto.CorporativoUnidade.CodigoUnidade);

            if (dto.Objetivo.CodigoSeqObjetivo > 0)
                resultadoAlcancado.Objetivo = Objetivo.GetRepository().Get(dto.Objetivo.CodigoSeqObjetivo);

            if (dto.Iniciativa.CodigoSeqIniciativa > 0)
                resultadoAlcancado.Iniciativa = Iniciativa.GetRepository().Get(dto.Iniciativa.CodigoSeqIniciativa);

            resultadoAlcancado.DescricaoResultadoAlcancado = dto.DescricaoResultadoAlcancado;
            resultadoAlcancado.Registrado = dto.Registrado;
            resultadoAlcancado.Ativo = dto.Ativo;
            resultadoAlcancado.DataInclusao = DateTime.Now;

            resultadoAlcancado.Save();

            return resultadoAlcancado.CodigoSeqResultadoAlcancado;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOResultadoAlcancadoRegistro ObterResultadoAlcancado(int codigoSeqResultadoAlcancado)
        {
            return ResultadoAlcancado.GetRepository().Get(codigoSeqResultadoAlcancado).Transform<DTOResultadoAlcancadoRegistro>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOResultadoAlcancadoRegistro ObterResultadosAlcancados()
        {
            return ResultadoAlcancado.GetRepository().Transform<DTOResultadoAlcancadoRegistro>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOResultadosAlcancados> ListarResultadosAlcancados(int codigoUnidade, int anoExercicio, int startIndex, int pageSize)
        {
            return ResultadoAlcancado.GetRepository().ListarResultadosAlcancados(codigoUnidade, anoExercicio, startIndex, pageSize).Transform<PageMessage<DTOResultadosAlcancados>>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public bool ObterResultadoAlcancadoPorAnoUnidade(int codigoUnidade, int anoExercicio)
        {
            bool resultadosInformados = false;

            foreach (var item in ResultadoAlcancado.GetRepository().ListarResultadosAlcancadosPorUnidade(codigoUnidade, anoExercicio).TransformList<DTOResultadosAlcancados>().Distinct())
            {
                if (!item.CodSeqResultadoAlcancado.Equals(0))
                    resultadosInformados = true;
                else
                    resultadosInformados = false;
            }

            return resultadosInformados;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOMetaResultadosAlcancados> ListaMetasPorObjetivoIniciativa(int codObjetivoIniciativa, short anoExercicio, int codTipo, int codUnidade, int startIndex, int pageSize)
        {
            var resultado = ResultadoAlcancado.GetRepository().ListaMetasPorObjetivoIniciativa(codObjetivoIniciativa, anoExercicio, codTipo, codUnidade, startIndex, pageSize).Transform<PageMessage<DTOMetaResultadosAlcancados>>();

            var lista = new List<DTOMetaResultadosAlcancados>();
            foreach (var item in resultado.Entities.ToList())
            {
                item.Executado = ObterResultadoAtividade(item.CodigoMeta);
                item.DesempenhoIndicador = ObterResultadoIndicador(item.CodigoMeta);
                lista.Add(item);
            }

            resultado.Entities = lista;
            return resultado;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public string ObterResultadoAtividade(int codigoMeta)
        {
            decimal desempenhoAtividade = 0;

            var atividade = RelatorioConsubstanciado.GetRepository().ListaResultadoAtividade(codigoMeta);

            foreach (var item in atividade)
            {
                desempenhoAtividade += item.PercentualExecucao;
            }

            desempenhoAtividade = desempenhoAtividade.Equals(0) ? 0 : (desempenhoAtividade / atividade.Count());

            return Math.Round(desempenhoAtividade) + "%";
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public string ObterResultadoIndicador(int codigoMeta)
        {
            decimal desempenhoIndicador = 0;

            var indicador = RelatorioConsubstanciado.GetRepository().ListaResultadoIndicador(codigoMeta);

            foreach (var item in indicador)
            {
                desempenhoIndicador += obterPercentualAtingimento(Convert.ToInt32(item.ResultadoIndicador), item.ValorAlvo, Convert.ToInt32(item.CodSeqTipoValor));
            }

            desempenhoIndicador = desempenhoIndicador.Equals(0) ? 0 : (desempenhoIndicador / indicador.Count());

            return Math.Round(desempenhoIndicador) + "%";
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public decimal obterPercentualAtingimento(int resultadoIndicador, decimal ValorAlvo, int CodTipoValor)
        {
            decimal valor = 0;

            if (resultadoIndicador > 0)
            {
                if (ValorAlvo > 0)
                {
                    if (CodTipoValor == 2)
                    {
                        decimal percentValorReportado = Convert.ToDecimal(resultadoIndicador) / 100;
                        decimal percentValorAlvo = ValorAlvo / 100;
                        int valorTotal = Convert.ToInt32((percentValorReportado / percentValorAlvo) * 100);
                        valor = decimal.Parse(valorTotal.ToString());
                    }
                    else
                    {
                        decimal valorReportado = Convert.ToDecimal(resultadoIndicador);
                        decimal valorAlvo = ValorAlvo;
                        decimal valorDivido = (valorReportado / valorAlvo);

                        long valorTotal = Convert.ToInt64(valorDivido * 100);
                        valor = decimal.Parse(valorTotal.ToString().Replace(",0", ""));
                    }
                }
            }

            return valor;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOObjetivos> ListarOjetivoEstrategicoEMissaoAdministrativa()
        {
            return RelatorioConsubstanciado.GetRepository().ListarOjetivoEstrategicoEMissaoAdministrativa().TransformList<DTOObjetivos>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOObjetivos> ListarOjetivoEstrategico(int codTipoInciativa)
        {
            return RelatorioConsubstanciado.GetRepository().ListarOjetivoEstrategico(codTipoInciativa).TransformList<DTOObjetivos>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOResultadosAlcancadosPorExercicio> ListarResultadosAlcancadosPorExercicio(int anoExercicio)
        {
            return RelatorioConsubstanciado.GetRepository().ListarResultadosAlcancadosPorExercicio(anoExercicio).TransformList<DTOResultadosAlcancadosPorExercicio>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOObjetivoEstrategicoDetalhado> ListarRecursos(int anoExercicio)
        {
            var dadosPesquisados = ListarObjetivoEstrategicoTotal(anoExercicio, 0, 0);

            IList<DTOObjetivoEstrategicoDetalhado> lista = new List<DTOObjetivoEstrategicoDetalhado>();
            List<string> objetivos = dadosPesquisados.OrderBy(x => x.DescricaoObjetivo).Select(i => i.DescricaoObjetivo).Distinct().ToList();

            if (objetivos.Count().Equals(0))
            {
                DTOObjetivoEstrategicoDetalhado dtoRelatorio = new DTOObjetivoEstrategicoDetalhado();
                dtoRelatorio.DescricaoUnidade = "-";
                lista.Add(dtoRelatorio);
            }
            else
            {
                foreach (var itemDescricaoObjetivo in objetivos)
                {
                    DTOObjetivoEstrategicoDetalhado dtoRelatorio = new DTOObjetivoEstrategicoDetalhado()
                    {
                        DescricaoObjetivo = itemDescricaoObjetivo,
                        RecursosPrevistosAssociados = dadosPesquisados.Where(x => x.DescricaoObjetivo == itemDescricaoObjetivo).Sum(x => x.TotalRecursoEstimado),
                        RecursosPrevistosRoteados = dadosPesquisados.Where(x => x.DescricaoObjetivo == itemDescricaoObjetivo).Sum(x => x.TotalRecursoEstimadoTotal),
                        RecursosUtilizadosAssociados = dadosPesquisados.Where(x => x.DescricaoObjetivo == itemDescricaoObjetivo).Sum(x => x.TotalRecursoOrcamentarioUtilizado),
                        RecursosUtilizadosRoteados = dadosPesquisados.Where(x => x.DescricaoObjetivo == itemDescricaoObjetivo).Sum(x => x.TotalRecursoOrcamentarioUtilizadoTotal)
                    };

                    lista.Add(dtoRelatorio);
                }
            }

            return lista.Distinct().OrderBy(x => x.DescricaoObjetivo).ToList();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTORelatorioConsubstanciadoDetalhado> ListarObjetivoEstrategico(int anoExercicio)
        {
            var dadosPesquisados = RelatorioConsubstanciado.GetRepository().ListarObjetivoEstrategico(anoExercicio).TransformList<DTORelatorioObjetivoEstrategico>();

            IList<DTORelatorioConsubstanciadoDetalhado> lista = new List<DTORelatorioConsubstanciadoDetalhado>();
            List<string> objetivos = dadosPesquisados.Select(i => i.DescricaoTipo).Distinct().ToList();

            if (objetivos.Count().Equals(0))
            {
                DTORelatorioConsubstanciadoDetalhado dtoRelatorio = new DTORelatorioConsubstanciadoDetalhado();
                dtoRelatorio.DescricaoObjetivo = "-";
                lista.Add(dtoRelatorio);
            }
            else
            {
                foreach (var itemObjetivo in objetivos)
                {
                    DTORelatorioConsubstanciadoDetalhado dto = new DTORelatorioConsubstanciadoDetalhado();

                    dto.DescricaoUnidade = dadosPesquisados.Where(x => x.DescricaoTipo == itemObjetivo).Select(i => i.DescricaoUnicade).FirstOrDefault();
                    dto.DescricaoObjetivo = itemObjetivo;

                    List<int> meta = dadosPesquisados.Where(x => x.DescricaoTipo == itemObjetivo).Select(i => i.CodSeqMeta).Distinct().ToList();

                    foreach (var cod in meta)
                    {
                        if (cod > 0)
                        {
                            ++dto.TotalMetas;

                            decimal resultadoIndicador = dadosPesquisados.Where(i => i.CodSeqMeta == cod && i.DescricaoTipo == itemObjetivo).Sum(x => x.ResultadoIndicador);
                            decimal percentualExecucao = dadosPesquisados.Where(i => i.CodSeqMeta == cod && i.DescricaoTipo == itemObjetivo).Sum(x => x.PercentualExecucao);

                            if (resultadoIndicador.Equals(100) && percentualExecucao.Equals(100))
                                ++dto.CemMetas;
                            else if ((resultadoIndicador >= 90 && resultadoIndicador < 100) && (percentualExecucao >= 90 && percentualExecucao < 100))
                                ++dto.NoventaMetas;
                        }
                    }

                    List<int> acao = dadosPesquisados.Where(i => i.DescricaoTipo == itemObjetivo).Select(i => i.CodSeqAcao).Distinct().ToList();

                    foreach (var cod in acao)
                    {
                        if (cod > 0)
                        {
                            ++dto.TotalAtividades;

                            decimal percentualExecucao = dadosPesquisados.Where(i => i.CodSeqAcao == cod && i.DescricaoTipo == itemObjetivo).Sum(x => x.PercentualExecucao);

                            if (percentualExecucao.Equals(100))
                                ++dto.CemAtividades;
                            else if (percentualExecucao >= 90 && percentualExecucao < 100)
                                ++dto.NoventaAtividades;
                        }
                    }

                    List<int> indicador = dadosPesquisados.Where(i => i.DescricaoTipo == itemObjetivo).Select(i => i.CodSeqIndicador).Distinct().ToList();

                    foreach (var cod in indicador)
                    {
                        if (cod > 0)
                        {
                            ++dto.TotalIndicadores;

                            decimal resultadoIndicador = dadosPesquisados.Where(i => i.CodSeqIndicador == cod && i.DescricaoTipo == itemObjetivo).Sum(x => x.ResultadoIndicador);

                            if (resultadoIndicador.Equals(100))
                                ++dto.CemIndicadores;
                            else if (resultadoIndicador >= 90 && resultadoIndicador < 100)
                                ++dto.NoventaIndicadores;
                        }
                    }

                    lista.Add(dto);
                }
            }

            return lista.Distinct().OrderBy(x => x.DescricaoObjetivo).ToList();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOAnoExercicio> AnoExercicio(int anoExercicio)
        {
            IList<DTOAnoExercicio> lista = new List<DTOAnoExercicio>();
            var dto = new DTOAnoExercicio();
            dto.AnoExercicio = anoExercicio;
            lista.Add(dto);

            return lista;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTORelatorioObjetivoEstrategico> ListarObjetivoEstrategicoPE(int anoExercicio, int codSeqObjectivo)
        {
            return RelatorioConsubstanciado.GetRepository().ListarObjetivoEstrategicoPE(anoExercicio, codSeqObjectivo).TransformList<DTORelatorioObjetivoEstrategico>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTORelatorioObjetivoEstrategico> ListarObjetivoEstrategicoPPA(int anoExercicio, int codSeqIniciativa)
        {
            return RelatorioConsubstanciado.GetRepository().ListarObjetivoEstrategicoPPA(anoExercicio, codSeqIniciativa).TransformList<DTORelatorioObjetivoEstrategico>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTORelatorioObjetivoEstrategico> ListarMeta(int anoExercicio, int codTipoMeta)
        {
            return RelatorioConsubstanciado.GetRepository().ListarMeta(anoExercicio, codTipoMeta).TransformList<DTORelatorioObjetivoEstrategico>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTORelatorioObjetivoEstrategico> ListarInstrumentos(int anoExercicio, int codTipoInstrumento)
        {
            return RelatorioConsubstanciado.GetRepository().ListarInstrumentos(anoExercicio, codTipoInstrumento).TransformList<DTORelatorioObjetivoEstrategico>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTORelatorioObjetivoEstrategico> ListarIniciativa(int anoExercicio, int codTipoIniciativa)
        {
            return RelatorioConsubstanciado.GetRepository().ListarIniciativa(anoExercicio, codTipoIniciativa).TransformList<DTORelatorioObjetivoEstrategico>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTORelatorioConsubstanciado> ListarRelatorioConsubstanciado(int codigoSeqExercicio)
        {
            return RelatorioConsubstanciado.GetRepository().ListarRelatorioConsubstanciado(codigoSeqExercicio).TransformList<DTORelatorioConsubstanciado>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOObjetivoEstrategicoTotalizadorDetalhadado> ListarObjetivoEstrategicoTotal(int anoExercicio, int codObjetivo, int codTipo)
        {
            var objEstrategicos = RelatorioConsubstanciado.GetRepository().ListarObjetivosEstrategicoDetalhado(anoExercicio, codObjetivo, codTipo);

            var lista = new List<DTOObjetivoEstrategicoTotalizadorDetalhadado>();
            foreach (var item in objEstrategicos)
            {
                var dto = new DTOObjetivoEstrategicoTotalizadorDetalhadado();
                var recurso = RelatorioConsubstanciado.GetRepository().ObterValorDeRecurso(item.CodSeqMeta, item.AnoExercicio);

                dto.AnoExercicio = item.AnoExercicio;
                dto.CodSeqObjetivo = item.CodSeqObjetivo;
                dto.DescricaoObjetivo = item.DescricaoObjetivo;
                dto.DescricaoUnidade = item.DescricaoUnidade;
                dto.TotalRecursoEstimado = recurso.Sum(x => x.TotalRecursoEstimado);
                dto.TotalRecursoOrcamentarioUtilizado = recurso.Sum(x => x.TotalRecursoOrcamentarioUtilizado);
                lista.Add(dto);
            }

            var objEstrategicosSoma = RelatorioConsubstanciado.GetRepository().ListarObjetivosEstrategicoDetalhado(anoExercicio, 0, codTipo);
            var listaSoma = new List<DTOObjetivoEstrategicoTotalizadorDetalhadado>();
            foreach (var item in objEstrategicosSoma)
            {
                var dto = new DTOObjetivoEstrategicoTotalizadorDetalhadado();
                var recurso = RelatorioConsubstanciado.GetRepository().ObterValorDeRecurso(item.CodSeqMeta, item.AnoExercicio);

                dto.TotalRecursoEstimado = recurso.Sum(x => x.TotalRecursoEstimado);
                dto.TotalRecursoOrcamentarioUtilizado = recurso.Sum(x => x.TotalRecursoOrcamentarioUtilizado);
                listaSoma.Add(dto);
            }

            var retorno = new List<DTOObjetivoEstrategicoTotalizadorDetalhadado>();
            foreach (var item in lista)
            {
                var dto = new DTOObjetivoEstrategicoTotalizadorDetalhadado();
                var recursoTotal = RelatorioConsubstanciado.GetRepository().ObterValorDeRecurso(0, item.AnoExercicio);

                dto.CodSeqObjetivo = item.CodSeqObjetivo;
                dto.DescricaoObjetivo = item.DescricaoObjetivo;
                dto.DescricaoUnidade = item.DescricaoUnidade;
                dto.TotalRecursoEstimado = item.TotalRecursoEstimado;
                dto.TotalRecursoOrcamentarioUtilizado = item.TotalRecursoOrcamentarioUtilizado;
                dto.TotalRecursoEstimadoTotal = CalcularTotalizadoresRecurso(item.TotalRecursoEstimado, listaSoma.Sum(x => x.TotalRecursoEstimado), recursoTotal.Sum(x => x.TotalRecursoEstimado));
                dto.TotalRecursoOrcamentarioUtilizadoTotal = CalcularTotalizadoresRecurso(item.TotalRecursoOrcamentarioUtilizado, listaSoma.Sum(x => x.TotalRecursoOrcamentarioUtilizado), recursoTotal.Sum(x => x.TotalRecursoOrcamentarioUtilizado));

                retorno.Add(dto);
            }

            return retorno;
        }

        private decimal CalcularTotalizadoresRecurso(decimal totalRecurso, decimal totalObjetivo, decimal totalProjeto)
        {
            decimal resultado = 0;

            if (totalRecurso > 0 && totalObjetivo > 0)
                resultado = (totalRecurso / totalObjetivo) * totalProjeto;

            return resultado;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOCorporativoResultadosAlcancados> ListarCorporativoResultadosAlcancados(int ano)
        {
            List<DTOCorporativoResultadosAlcancados> dtoCorporativoUnidadeRetorno = new List<DTOCorporativoResultadosAlcancados>();

            foreach (var item in CorporativoUnidade.GetRepository().ListarCorporativoUnidadeAtivo().TransformList<DTOCorporativoResultadosAlcancados>())
            {
                item.ResultadosInformados = ObterResultadoAlcancadoPorAnoUnidade(item.CodigoUnidade, ano);
                dtoCorporativoUnidadeRetorno.Add(item);
            }

            return dtoCorporativoUnidadeRetorno;
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarRelatorioConsubstanciado(DTORelatorioConsubstanciado dto)
        {
            RelatorioConsubstanciado relatorioConsubstanciado;

            if (dto.CodigoSeqRelatorioConsubstanciado > 0)
            {
                relatorioConsubstanciado = RelatorioConsubstanciado.GetRepository().Get(dto.CodigoSeqRelatorioConsubstanciado);
                if (relatorioConsubstanciado == null)
                    throw new Exception("relatório consubstanciado não encontrado.");
            }
            else
                relatorioConsubstanciado = new RelatorioConsubstanciado();

            relatorioConsubstanciado.Exercicio = Exercicio.GetRepository().ObterExercicioPorAno(dto.Exercicio.Ano);
            relatorioConsubstanciado.TipoTexto = TipoTexto.GetRepository().Get(dto.TipoTexto.CodigoSeqTipoTexto);

            relatorioConsubstanciado.DescricaoRelatorioConsubstanciado = dto.DescricaoRelatorioConsubstanciado;
            relatorioConsubstanciado.Ativo = true;
            relatorioConsubstanciado.DataInclusao = DateTime.Now;

            relatorioConsubstanciado.Save();

            return relatorioConsubstanciado.CodigoSeqRelatorioConsubstanciado;
        }

        [Transaction(TransactionPropagation.Required)]
        public int SalvarProcesso(DTOProcesso dto)
        {
            Processo processo = new Processo()
            {
                No_Processo = dto.No_Processo,
                Cd_Corporativo_Unidade = dto.Cd_Corporativo_Unidade,
                Cd_Objetivo = dto.Cd_Objetivo,
                Ds_Processo_Sei = dto.Ds_Processo_Sei,
                Dt_Atualizacao = dto.Dt_Atualizacao,
                No_Usuario_Atualizacao = dto.No_Usuario_Atualizacao,
                No_Usuario_Criacao = dto.No_Usuario_Criacao,
                Dt_Criacao = dto.Dt_Criacao,
                St_Ativo = dto.St_Ativo,
                St_Excluido = dto.St_Excluido
            };

            processo.Save();
            return processo.Cd_Seq_Processo;
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public PageMessage<DTOProcesso> ListarProcessoNomeAreaSituacao(string pNome, Int32 pArea, int pSituacao, int startIndex, int pageSize, string orderProperty, bool orderAscending)
        {
            return null;//Processo.GetRepository().ListarEncaminhamentosPorTipoUnidadeEStatus(tipoAcompanhamento, codigoUnidade, status, startIndex, pageSize, orderProperty, orderAscending).Transform<PageMessage<DTOEncaminhamento>>();
        }



        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public IList<DTOProcesso> ListarProcessos()
        {
            return Processo.GetRepository().ListAll().TransformList<DTOProcesso>();
        }

        public IList<DTOTipoAnexo> ListarTipoAnexo()
        {
            return TipoAnexo.GetRepository().ListAll().TransformList<DTOTipoAnexo>();
        }

        [Transaction(TransactionPropagation.Required, ReadOnly = true)]
        public DTOProcessoAnexo ObterProcesso(int pCd_Seq_Processo)
        {
            DTOProcesso processo = new DTOProcesso();

            processo = Processo.GetRepository().Get(pCd_Seq_Processo).Transform<DTOProcesso>();

            var teste = new DTOProcesso();

            teste = processo;

            DTOProcessoAnexo ldto = new DTOProcessoAnexo();

            var dto = new DTOProcessoAnexo();
            dto.Cd_Processo = processo.Cd_Seq_Processo;
            dto.Processo = teste;

            DTOProcessoAnexo resul = new DTOProcessoAnexo();

            var resultado = ProcessoAnexo.GetRepository().Get(1);

            //resul = resultado;
            

            return resul;
            //return ProcessoAnexo.GetRepository().Get(1).Transform<DTOProcessoAnexo>();
        }
        

        //[Transaction(TransactionPropagation.Required, ReadOnly = true)]
        //public IList<DTOTipoAnexo> ListarTipoAnexo()
        //{
        //    var teste = TipoAnexo.GetRepository().ListAll().TransformList<DTOTipoAnexo>();
        //    return teste;
        //}
    }
}