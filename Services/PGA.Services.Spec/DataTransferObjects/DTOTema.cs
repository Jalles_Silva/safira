﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOTema
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqTema { get; set; }

        [DataMember(), Required(), StringLength(10)]
        public string NuTema { get; set; }

        [DataMember(), Required(), StringLength(100)]
        public string DescricaoTema { get; set; }

        [DataMember()]
        public DTOPerspectiva Perspectiva { get; set; }

        [DataMember(), Required()]
        public bool Ativo { get; set; }
    }
}