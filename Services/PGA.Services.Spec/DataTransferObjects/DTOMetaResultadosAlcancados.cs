﻿using System;
using System.Runtime.Serialization;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOMetaResultadosAlcancados
    {
        [DataMember()]
        public int CodigoMeta { get; set; }

        [DataMember()]
        public string DescricaoMeta { get; set; }

        [DataMember()]
        public string TipoMeta { get; set; }

        [DataMember()]
        public string DescricaoTipo { get; set; }

        [DataMember()]
        public string ResultadoEsperadoMeta { get; set; }

        [DataMember()]
        public string ContantePGA { get; set; }

        [DataMember()]
        public string DescricaoAlinhamento { get; set; }

        [DataMember()]
        public string Executado { get; set; }

        [DataMember()]
        public string DesempenhoIndicador { get; set; }
    }
}
