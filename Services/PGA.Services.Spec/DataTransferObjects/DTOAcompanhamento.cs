﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOAcompanhamento
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqAcompanhamento { get; set; }

        [DataMember(), Required()]
        public DTOTipoAcompanhamento TipoAcompanhamento { get; set; }

        [DataMember(), Required()]
        public DTOExercicio Exercicio { get; set; }

        [DataMember()]
        public DTOAnexo Anexo { get; set; }

        [DataMember(), Required()]
        public DateTime DataAcompanhamento { get; set; }

        [DataMember(), Required()]
        public DTOStatusAcompanhamento StatusAcompanhamento { get; set; }

        [DataMember()]
        public bool AptoConclusao { get; set; }
    }
}
