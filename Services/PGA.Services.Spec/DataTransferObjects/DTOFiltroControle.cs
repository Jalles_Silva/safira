﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    public class DTOFiltroControle
    {
        [DataMember()]
        public int CodigoMeta { get; set; }
        [DataMember()]
        public int CodigoUnidade { get; set; }
        [DataMember()]
        public string Unidade { get; set; }
        [DataMember()]
        public string Status { get; set; }
        [DataMember()]
        public int StartIndex { get; set; }
        [DataMember()]
        public int PageSize { get; set; }
        [DataMember()]
        public string OrderProperty { get; set; }
        [DataMember()]
        public bool OrderAscending { get; set; }
        [DataMember()]
        public int AnoExercicio { get; set; }
    }
}
