﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOControleIndicador
    {
        [DataMember(), Key(), Required(), DisplayName("CodigoSeqControleIndicador")]
        public int CodigoSeqControleIndicador { get; set; }

        [DataMember(), Required(), DisplayName("DataCadastro")]
        public DateTime DataIndicador { get; set; }

        [DataMember(), Required(), DisplayName("ValorIndicador")]
        public decimal ValorIndicador { get; set; }

        [DataMember(), StringLength(1800), DisplayName("DescricaoAcao")]
        public string DescricaoAcao { get; set; }

        [DataMember(), StringLength(1800), DisplayName("DescricaoCausa")]
        public string DescricaoCausa { get; set; }

        [DataMember(), StringLength(1800), DisplayName("DescricaoObservacao")]
        public string DescricaoObservacao { get; set; }

        [DataMember(), Required(), StringLength(50), DisplayName("NoUsuario")]
        public string NomeUsuario { get; set; }

        [DataMember(), Required(), DisplayName("DataCadastro")]
        public DateTime DataCadastro { get; set; }

        [DataMember()]
        public int CodigoSeqIndicador { get; set; }

        [DataMember()]
        public bool UsuarioAdministrador { get; set; }

        [DataMember()]
        [Map("Indicador.TipoValor.CodigoSeqTipoValor")]
        public int CodigoSeqTipoValor { get; set; }
    }
}
