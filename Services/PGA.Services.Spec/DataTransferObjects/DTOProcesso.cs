﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOProcesso
    {
        [DataMember(), Key(), Required(), DisplayName("Cd_Seq_Processo")]
        public int Cd_Seq_Processo { get; set; }
        
        [DataMember(), Key(), DisplayName("Cd_Corporativo_Unidade")]
        public int Cd_Corporativo_Unidade { get; set; }
        
        [DataMember(), Key(), DisplayName("Cd_Objetivo")]
        public int? Cd_Objetivo { get; set; }

        [DataMember(), Required(), StringLength(500), DisplayName("No_Processo")]
        public string No_Processo { get; set; }

        [DataMember(), Required(), StringLength(20), DisplayName("Ds_Processo_Sei")]
        public string Ds_Processo_Sei { get; set; }

        [DataMember(), Required(), StringLength(30), DisplayName("No_Usuario_Criacao")]
        public string No_Usuario_Criacao { get; set; }

        [DataMember(), Required(), DisplayName("Dt_Criacao")]
        public DateTime Dt_Criacao { get; set; }

        [DataMember(), Required(), StringLength(30), DisplayName("No_Usuario_Atualizacao")]
        public string No_Usuario_Atualizacao { get; set; }

        [DataMember(), Required(), DisplayName("Dt_Atualizacao")]
        public DateTime Dt_Atualizacao { get; set; }

        [DataMember(), StringLength(1), DisplayName("St_Ativo")]
        public bool St_Ativo { get; set; }

        [DataMember(), Required(), DisplayName("Ds_Motivo_Inativacao")]
        public string Ds_Motivo_Inativacao { get; set; }

        [DataMember(), StringLength(1), DisplayName("St_Excluido")]
        public bool? St_Excluido { get; set; }
    }
}
