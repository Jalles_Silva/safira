﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOAnoExercicio
    {
        [DataMember()]
        public int AnoExercicio { get; set; }
    }
}
