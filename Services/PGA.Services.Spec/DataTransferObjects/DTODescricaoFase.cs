﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOFase
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqFase { get; set; }

        [DataMember(), Required()]
        public short Ordem { get; set; }

        [DataMember(), Required(), StringLength(50)]
        public string DescricaoFase { get; set; }

        [DataMember()]
        public int MesInicio { get; set; }

        [DataMember()]
        public int MesFim { get; set; }

        public string DescricaoFaseAlterada
        {
            get
            {
                return DescricaoFase.Replace("ACOMPANHAMENTO DO",""); ;
            }
        }
    }
}