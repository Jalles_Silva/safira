﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTORecurso
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqRecurso { get; set; }

        [DataMember(), Required(), StringLength(100)]
        public string DescricaoRecurso { get; set; }

        [DataMember(), Required()]
        public decimal QuantidadeRecurso { get; set; }

        [DataMember()]
        [Map("TipoRecurso.CodigoSeqTipoRecurso")]
        public int CodigoSeqTipoRecurso { get; set; }

        [DataMember()]
        [Map("TipoRecurso.DescricaoTipoRecurso")]
        public string DescricaoTipoRecurso { get; set; }

    }
}