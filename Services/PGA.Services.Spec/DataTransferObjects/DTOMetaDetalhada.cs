﻿using PGA.Common;
using SQFramework.Core.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOMetaDetalhada
    {
        [DataMember(), Key(), Required(), DisplayName("CodSeqMeta")]
        public int CodSeqMeta { get; set; }

        [DataMember(), Required(), DisplayName("DescricaoMeta")]
        public string DescricaoMeta { get; set; }

        [DataMember(), Required(), DisplayName("StatusMetaInterna")]
        public string StatusMetaInterna { get; set; }

        [DataMember]
        [Map("CorporativoUnidade.DescricaoUnidade")]
        public string DescricaoUnidade { get; set; }

        [DataMember(), DisplayName("DescricaoResultadoEsperado")]
        public string DescricaoResultadoEsperado { get; set; }

        [DataMember(), DisplayName("DescricaoPremissa")]
        public string DescricaoPremissa { get; set; }

        [DataMember(), DisplayName("DescricaoRestricao")]
        public string DescricaoRestricao { get; set; }

        [DataMember(), DisplayName("StatusAlinhamentoPe")]
        public bool? StatusAlinhamentoPe { get; set; }

        [DataMember(), DisplayName("StatusAlinhamentoPpa")]
        public bool? StatusAlinhamentoPpa { get; set; }

        [DataMember(), Required(), DisplayName("NoCoordenacao")]
        public string NoCoordenacao { get; set; }

        [DataMember()]
        public DTOIniciativa IniciativaPe { get; set; }

        [DataMember()]
        public DTOIniciativa IniciativaPpa { get; set; }

        [DataMember()]
        public DTOIniciativa IniciativaMissao { get; set; }

        [DataMember]
        public DTOTipoMeta TipoMeta { get; set; }

        [DataMember]
        public IList<DTOAcao> Acoes { get; set; }

        [DataMember]
        public IList<DTOIndicador> Indicadores { get; set; }

        [DataMember()]
        public DTOStatusMeta StatusMeta { get; set; }

        [DataMember()]
        public DTOSituacaoMeta SituacaoMeta { get; set; }

        [DataMember()]
        public bool? StatusAlinhamentoMissaoInstitucional { get; set; }

        [DataMember()]
        public string DescricaoAlinhamentoMissaoInstitucional { get; set; }

        [DataMember()]
        public DTOExercicio Exercicio { get; set; }
        
        [DataMember(), Required(), DisplayName("DescricaoJustificativa")]
        public string DescricaoJustificativa { get; set; }

        [DataMember]
        public DTOInstrumento GestaoDeRiscos { get; set; }

        [DataMember]
        public DTOInstrumento AgendaRegulatoria { get; set; }

        [DataMember]
        public DTOInstrumento Desburocratizacao { get; set; }

        [DataMember]
        public DTOInstrumento Integridade { get; set; }

        [DataMember(), DisplayName("DescricaoOutros")]
        public string DescricaoOutros { get; set; }
        
        [DataMember(), DisplayName("DescricaoSociedadeUsuarioBeneficio")]
        public string DescricaoSociedadeUsuarioBeneficio { get; set; }

        [DataMember(), DisplayName("DescricaoSociedadeUsuarioImpacto")]
        public string DescricaoSociedadeUsuarioImpacto { get; set; }

        [DataMember(), DisplayName("DescricaoInstitucionalBeneficio")]
        public string DescricaoInstitucionalBeneficio { get; set; }

        [DataMember(), DisplayName("DescricaoInstitucionalImpacto")]
        public string DescricaoInstitucionalImpacto { get; set; }

        [DataMember(), DisplayName("DescricaoGovernoBeneficio")]
        public string DescricaoGovernoBeneficio { get; set; }

        [DataMember(), DisplayName("DescricaoGovernoImpacto")]
        public string DescricaoGovernoImpacto { get; set; }

        [DataMember(), DisplayName("DescricaoSetorReguladoBeneficio")]
        public string DescricaoSetorReguladoBeneficio { get; set; }

        [DataMember(), DisplayName("DescricaoSetorReguladoImpacto")]
        public string DescricaoSetorReguladoImpacto { get; set; }

    }
}
