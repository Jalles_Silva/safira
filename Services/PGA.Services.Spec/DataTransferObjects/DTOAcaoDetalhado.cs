﻿using SQFramework.Core.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOAcaoDetalhado
    {
        [DataMember()]
        public int CodigoSeqAcao  { get; set; }
        
        [DataMember()]
        [Map("Meta.DescricaoMeta")]
        public string DescricaoMeta { get; set; }
        
        [DataMember()]
        public string DescricaoAtividadeAcao { get; set; }
        
        [DataMember()]
        public string DescricaoEstrategiaAcao { get; set; }

        [DataMember()]
        public DateTime? DataInicioAcao { get; set; }
        
        [DataMember()]
        public DateTime? DataFimAcao { get; set; }

        [DataMember()]
        public DateTime? DataUltimoReporte { get; set; }

        [DataMember()]
        [Map("PercentualExecucao")]
        public int? iPercentualExecucao  { get; set; }

        public string PercentualExecucao
        {
            get
            {
                return !string.IsNullOrEmpty(iPercentualExecucao.ToString()) ? iPercentualExecucao + "%" : string.Empty;
            }
        }

        [DataMember()]
        [Map("Meta.Exercicio.Ano")]
        public string AnoExercicio { get; set; }

        [DataMember()]
        [Map("Meta.CorporativoUnidade.DescricaoUnidade")]
        public string UnidadeCorporativa  { get; set; }

        [DataMember()]
        public string CodigoFaseExercicio { get; set; }

        [DataMember]
        [Map("Meta.CorporativoUnidade.CodigoUnidade")]
        public string CodigoUnidade  { get; set; }
    }
}
