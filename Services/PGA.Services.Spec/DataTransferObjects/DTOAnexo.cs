﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOAnexo
    {
        [DataMember(), Key(), Required()]
        public Int32 CodigoSeqAnexo { get; set; }

        [DataMember(), Required(), StringLength(100)]
        public String DescricaoAnexo { get; set; }

        [DataMember(), Required(), StringLength(1000)]
        public String CaminhoAnexo { get; set; }

        [DataMember(), Required()]
        public DateTime DataInclusao { get; set; }

        [DataMember(), StringLength(1800)]
        public String Observacoes { get; set; }

        [DataMember(), StringLength(1800)]
        public int TipoAnexo { get; set; }

        [DataMember(), Required()]
        public byte[] Conteudo { get; set; }
    }
}
