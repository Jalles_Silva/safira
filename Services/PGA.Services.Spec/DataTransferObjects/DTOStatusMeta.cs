﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOStatusMeta
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqStatusMeta { get; set; }

        [DataMember(), StringLength(100)]
        public string DescricaoStatus { get; set; }
    }
}