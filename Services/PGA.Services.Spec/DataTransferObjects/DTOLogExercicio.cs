﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOLogExercicio
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqLogExercicio { get; set; }

        [DataMember(), Required()]
        public int CodigoFase { get; set; }

        [DataMember(), Required()]
        public short AnoExercicio { get; set; }

        [DataMember(), Required(), StringLength(50)]
        public string NomeUsuario { get; set; }

        [DataMember(), Required()]
        public DateTime DataCadastro { get; set; }

    }
}