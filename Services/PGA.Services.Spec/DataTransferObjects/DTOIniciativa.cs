﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOIniciativa
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqIniciativa { get; set; }

        [DataMember(), Required(), StringLength(500)]
        public string DescricaoIniciativa { get; set; }

        [DataMember(), StringLength(1000)]
        public string ProgramaTematico { get; set; }

        [DataMember()]
        public DTOCorporativoUnidade CorporativoUnidade { get; set; }

        [DataMember()]
        public DTOTipoIniciativa TipoIniciativa { get; set; }

        [DataMember(), StringLength(1)]
        public string TpIniciativaProjeto { get; set; }

        [DataMember()]
        public DTOAnexo Anexo { get; set; }

        [DataMember(), Required()]
        public bool Ativo { get; set; }

        [DataMember()]
        public IList<DTOIniciativaObjetivo> Objetivos { get; set; }

        [DataMember()]
        public IList<DTOMeta> Metas { get; set; }
    }
}