﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PGA.Services.Spec.DataTransferObjects
{
    public class DTOMetaSimplificada
    {
        [DataMember()]
        public int CodSeqMeta { get; set; }

        [DataMember()]
        public string DescricaoMeta { get; set; }
    }
}
