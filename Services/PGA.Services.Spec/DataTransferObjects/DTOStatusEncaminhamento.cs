﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOStatusEncaminhamento
    {
        [DataMember(), Key(), Required()]
        public Int32 CodigoSeqStatusEncaminhamento { get; set; }

        [DataMember(), Required(), StringLength(50)]
        public String DescricaoStatusEncaminhamento { get; set; }
    }
}
