﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOControleAcao
    {
        [DataMember(), Key(), Required(), DisplayName("CodigoSeqControleAcao")]
        public int CodigoSeqControleAcao { get; set; }

        [DataMember(), Required(), DisplayName("DataReporte")]
        public DateTime DataReporte { get; set; }

        [DataMember(), Required(), DisplayName("ValorPercentualReporte")]
        public short ValorPercentualReporte { get; set; }

        [DataMember(), StringLength(1800), DisplayName("DescricaoAcao")]
        public string DescricaoAcao { get; set; }

        [DataMember(), StringLength(1800), DisplayName("DescricaoCausa")]
        public string DescricaoCausa { get; set; }

        [DataMember(), StringLength(1800), DisplayName("DescricaoObservacao")]
        public string DescricaoObservacao { get; set; }

        [DataMember(), Required(), StringLength(50), DisplayName("NoUsuario")]
        public string NomeUsuario { get; set; }

        [DataMember(), Required(), DisplayName("DataCadastro")]
        public DateTime DataCadastro { get; set; }

        [DataMember()]
        public int CodigoSeqAcao { get; set; }

        [DataMember()]
        public bool UsuarioAdministrador { get; set; }

        [DataMember()]
        public decimal? ValorRecursoFinanceiro { get; set; }

    }
}
