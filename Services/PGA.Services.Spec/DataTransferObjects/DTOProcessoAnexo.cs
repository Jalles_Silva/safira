﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOProcessoAnexo
    {
        [DataMember(), Key(), Required(), DisplayName("Cd_Seq_Processo_Anexo")]
        public int Cd_Seq_Processo_Anexo { get; set; }

        [DataMember(), DisplayName("Cd_Processo")]
        public int Cd_Processo { get; set; }

        [DataMember(), DisplayName("Cd_Anexo")]
        public int Cd_Anexo { get; set; }

        [DataMember(), Required(), StringLength(30), DisplayName("No_Usuario_Criacao")]
        public string No_Usuario_Criacao { get; set; }

        [DataMember(), Required(), DisplayName("Dt_Criacao")]
        public DateTime Dt_Criacao { get; set; }

        [DataMember(), Required(), StringLength(30), DisplayName("No_Usuario_Atualizado")]
        public string No_Usuario_Atualizado { get; set; }
        
        [DataMember(), Required(), DisplayName("Dt_Atualizacao")]
        public DateTime Dt_Atualizacao { get; set; }

        [DataMember(), DisplayName("St_Ativo")]
        public bool St_Ativo { get; set; }

        [DataMember(), Required(), DisplayName("Ds_Motivo_Inativacao")]
        public string Ds_Motivo_Inativacao { get; set; }

        [DataMember(), Required(), DisplayName("Anexo")]
        public DTOAnexo Anexos { get; set; }
        
        [DataMember(), Required(), DisplayName("Processo")]
        public DTOProcesso Processo { get; set; }
    }
}
