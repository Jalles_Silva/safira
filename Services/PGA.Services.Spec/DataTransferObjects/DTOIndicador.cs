﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOIndicador
    {
        [DataMember(), Key(), Required(), DisplayName("CodigoSeqIndicador")]
        public int CodigoSeqIndicador { get; set; }

        [DataMember(), Required(), StringLength(500), DisplayName("NoIndicador")]
        public string NoIndicador { get; set; }

        [DataMember(), Required(), StringLength(1000), DisplayName("DescricaoIndicador")]
        public string DescricaoIndicador { get; set; }

        [DataMember(), Required(), StringLength(1000), DisplayName("ValorLinhaBase")]
        public decimal ValorLinhaBase { get; set; }

        [DataMember(), StringLength(1000), DisplayName("DescricaoFormula")]
        public string DescricaoFormula { get; set; }
        
        [DataMember(), DisplayName("StatusIndicador")]
        public int? StatusIndicador { get; set; }

        [DataMember()]
        [Map("TipoIndicador.CodigoSeqTipoIndicador")]
        public int CodigoSeqTipoIndicador { get; set; }

        [DataMember()]
        [Map("TipoIndicador.DescricaoTipoIndicador")]
        public string DescricaoTipoIndicador { get; set; }

        [DataMember()]
        [Map("Meta.CodSeqMeta")]
        public int CodigoSeqMeta { get; set; }

        [DataMember()]
        [Map("Meta.DescricaoMeta")]
        public string DescricaoMeta { get; set; }

        [DataMember]
        [Map("Meta.CorporativoUnidade.CodigoUnidade")]
        public string CodigoUnidade { get; set; }

		[DataMember()]
        [Map("Meta.StatusMeta")]
        public DTOStatusMeta StatusMeta { get; set; }

        [DataMember(), Required(), DisplayName("ValorAlvo")]
        public decimal ValorAlvo { get; set; }

        [DataMember(), Required(), DisplayName("DataAlvo")]
        public DateTime? DataAlvo { get; set; }

        [DataMember()]
        [Map("TipoValor.CodigoSeqTipoValor")]
        public int CodigoSeqTipoValor { get; set; }

        [DataMember(), Required(), DisplayName("MesReferencia")]
        public DateTime? MesReferencia { get; set; }

		[DataMember()]
        public double? NuLinhabase { get; set; }
        
		[DataMember()]
        public DateTime? DataUltimoValor { get; set; }

        [DataMember()]
        public int? UltimoValor { get; set; }

        [DataMember()]
        public int? CodSeqTipoValor { get; set; }

        [DataMember()]
        [Map("Meta.Exercicio.Ano")]
        public int AnoExercicio { get; set; }

        [DataMember()]
        [Map("Meta.CorporativoUnidade.DescricaoUnidade")]
        public string UnidadeCorporativa { get; set; }

        [DataMember()]
        [Map("Meta.Exercicio.Fase.CodigoSeqFase")]
        public int CodigoFaseExercicio { get; set; }

		[DataMember()]
        public DTOTipoResultado TipoResultado { get; set; }

        [DataMember()]
        public bool ObjetivoEstrategico { get; set; }

        [DataMember()]
        public int ResultadoIndicador { get; set; }
    }
}