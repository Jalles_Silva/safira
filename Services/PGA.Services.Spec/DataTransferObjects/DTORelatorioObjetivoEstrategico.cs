﻿using System;
using System.Runtime.Serialization;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTORelatorioObjetivoEstrategico
    {
        [DataMember()]
        public int CodSeqIniciativa { get; set; }

        [DataMember()]
        public int CodSeqMeta { get; set; }

        [DataMember()]
        public int CodSeqIndicador { get; set; }

        [DataMember()]
        public int CodSeqAcao { get; set; }

        [DataMember()]
        public string DescricaoUnicade { get; set; }

        [DataMember()]
        public decimal PercentualExecucao { get; set; }

        [DataMember()]
        public decimal ResultadoIndicador { get; set; }

        [DataMember()]
        public string DescricaoTipo { get; set; }
    }
}
