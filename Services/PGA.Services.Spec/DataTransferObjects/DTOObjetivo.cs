﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOObjetivo
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqObjetivo { get; set; }

        [DataMember(), Required(), StringLength(10)]
        public string NuObjetivo { get; set; }

        [DataMember(), Required(), StringLength(100)]
        public string NomeObjetivo { get; set; }

        [DataMember(), Required(), StringLength(1000)]
        public string DescricaoObjetivo { get; set; }

        [DataMember(), Required()]
        public bool Ativo { get; set; }

        [DataMember()]
        public DTOTipoObjetivo TipoObjetivo { get; set; }

        [DataMember()]
        public DTOTema Tema { get; set; }
    }
}