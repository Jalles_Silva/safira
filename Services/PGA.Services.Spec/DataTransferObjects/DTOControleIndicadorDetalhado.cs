﻿using SQFramework.Core.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOControleIndicadorDetalhado
    {
        [DataMember()]
        public int CodigoSeqIndicador { get; set; }
        
        [DataMember()]
        public int? CodSeqTipoValor { get; set; }
        
        [DataMember()]
        public string NoIndicador { get; set; }

        [DataMember()]
        public string DescricaoMeta { get; set; }

        [DataMember()]
        public DateTime? DataAlvo { get; set; }

        [DataMember()]
        [Map("ValorAlvo")]
        public decimal dvalorAlvo { get; set; }

        [DataMember()]
        public DateTime? DataUltimoValor { get; set; }

        [DataMember()]
        [Map("Meta.Exercicio.Ano")]
        public int AnoExercicio { get; set; }

        [DataMember()]
        [Map("Meta.CorporativoUnidade.DescricaoUnidade")]
        public string UnidadeCorporativa { get; set; }

        [DataMember()]
        public int CodigoFaseExercicio { get; set; }

        [DataMember]
        [Map("Meta.CorporativoUnidade.CodigoUnidade")]
        public string CodigoUnidade { get; set; }

        [DataMember()]
        [Map("UltimoValor")]
        public int? ultimoValor { get; set; }

        [DataMember()]
        [Map("ResultadoIndicador")]
        public decimal dresultadoIndicador { get; set; }

        public string PercentualAtingimento 
        {
            get
            {
                return obterPercentualAtingimento(Convert.ToInt32(dresultadoIndicador), dvalorAlvo, Convert.ToInt32(CodSeqTipoValor));
            }
        }

        public string ValorAlvo
        {
            get
            {
                return ApresentaValores(dvalorAlvo, Convert.ToInt32(CodSeqTipoValor), false).ToString();
            }
        }

        public string ResultadoIndicador
        {
            get
            {
                return ApresentaValores(dresultadoIndicador, Convert.ToInt32(CodSeqTipoValor), false).ToString();
            }
        }
       
        public string obterPercentualAtingimento(int resultadoIndicador, decimal ValorAlvo, int CodTipoValor)
        {
            string valor;

            if (resultadoIndicador > 0)
            {
                if (ValorAlvo > 0)
                {
                    if (CodTipoValor == 2)
                    {
                        decimal percentValorReportado = Convert.ToDecimal(resultadoIndicador) / 100;
                        decimal percentValorAlvo = ValorAlvo / 100;
                        int valorTotal = Convert.ToInt32((percentValorReportado / percentValorAlvo) * 100);
                        valor = valorTotal.ToString() + "%";
                    }
                    else
                    {
                        decimal valorReportado = Convert.ToDecimal(resultadoIndicador);
                        decimal valorAlvo = ValorAlvo;
                        decimal valorDivido = (valorReportado / valorAlvo);

                        long valorTotal = Convert.ToInt64(valorDivido * 100);
                        valor = valorTotal.ToString().Replace(",0", "") + "%";
                    }
                }
                else
                {
                    valor = "0%";
                }
            }
            else
            {
                valor = "";
            }
            
            return valor;
        }

        public string ApresentaValores(decimal Valor, int CodTipoValor, bool RertornaVazio)
        {
            string valor;

            if (Valor > 0)
            {
                if (CodTipoValor == 2)
                {
                    valor = Valor.ToString() + "%";
                }
                else
                {
                    valor = Valor.ToString();
                }
            }
            else
            {
                if (RertornaVazio)
                {
                    valor = "";
                }
                else
                {
                     valor = "0" + (CodTipoValor == 2 ? "%" : "");
                }

            }

            return valor;
        }
    }
}
