﻿using SQFramework.Core.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOObjetivos
    {
        [DataMember()]
        public string CodSeqObjetivo { get; set; }
        
        [DataMember()]
        public string DescricaoObjetivo { get; set; }
    }
}
