﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOFechamento
    {
        [DataMember(), Required(), Key(), DisplayName("CodigoSeqFechamento")]
        public int CodigoSeqFechamento { get; set; }

        [DataMember(), Required(), DisplayName("DataFechamento")]
        public DateTime DataFechamento { get; set; }

        [DataMember(), Required(), StringLength(50), DisplayName("NomeUsuario")]
        public string NomeUsuario { get; set; }

        [DataMember(), Required(), DisplayName("DataCadastro")]
        public DateTime DataCadastro { get; set; }
    }
}