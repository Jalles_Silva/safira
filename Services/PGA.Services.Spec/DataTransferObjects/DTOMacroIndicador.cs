﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOMacroIndicador
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqMacroInd { get; set; }

        [DataMember(), Required(), StringLength(150)]
        public string DescricaoMacroInd { get; set; }

    }
}