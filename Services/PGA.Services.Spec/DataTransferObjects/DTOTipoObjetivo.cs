﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOTipoObjetivo
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqTipoObjetivo { get; set; }

        [DataMember(), Required(), StringLength(50)]
        public string DescricaoTipoObjetivo { get; set; }
    }
}
