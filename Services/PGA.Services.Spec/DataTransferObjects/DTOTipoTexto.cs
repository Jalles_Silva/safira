﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOTipoTexto
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqTipoTexto { get; set; }

        [DataMember(), Required(), StringLength(8000)]
        public string DescricaoTipoTexto { get; set; }

        [DataMember()]
        public bool Ativo { get; set; }

        [DataMember()]
        public DateTime DataInclusao { get; set; }
    }
}