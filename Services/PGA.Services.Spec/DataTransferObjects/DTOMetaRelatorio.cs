﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;
using PGA.Common;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOMetaRelatorio
    {
        [DataMember(), Key()]
        public string CodSeqMeta { get; set; }

        [DataMember()]
        public string DescricaoArea { get; set; }

        [DataMember()]
        public int AnoExcercicio { get; set; }

        [DataMember()]
        public int CodigoUnidade { get; set; }

        [DataMember()]
        public int QdtMetasCadastradas { get; set; }

        [DataMember()]
        public int QtdMetasAlinhadasPE { get; set; }

        [DataMember()]
        public int QtdMetasAlinhadasPPA { get; set; }

        [DataMember()]
        public int QtdMetasAlinhadasMI { get; set; }

        [DataMember()]
        public int QtdMetasAdministrativas { get; set; }

        [DataMember()]
        public int QtdMetasFiscalizacoes { get; set; }

        [DataMember()]
        public int QtdMetasRegulacoes { get; set; }
        
        [DataMember()]
        public bool VerAlinhadasPE { get; set; }

        [DataMember()]
        public bool VerAlinhadasPPA { get; set; }

        [DataMember()]
        public bool VerAlinhadasMI { get; set; }

        [DataMember()]
        public bool VerAdministrativas { get; set; }

        [DataMember()]
        public bool VerFiscalizacoes { get; set; }

        [DataMember()]
        public bool VerRegulacoes { get; set; }

        [DataMember()]
        public string TipoMeta { get; set; }

        [DataMember()]
        public string TipoAlinhamento { get; set; }

        [DataMember()]
        public string DescricaoMeta { get; set; }

        [DataMember()]
        public int? QtdMetasInternas { get; set; }

        [DataMember()]
        public string MetaInterna { get; set; }

        [DataMember()]
        public string Trimestre { get; set; }
    }
}
