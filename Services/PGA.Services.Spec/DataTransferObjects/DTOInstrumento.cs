﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOInstrumento
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqInstrumento { get; set; }

        [DataMember(), Required(), StringLength(1000)]
        public string DescricaoInstrumento { get; set; }
        
        [DataMember()]
        public DTOObjetivo Objetivo { get; set; }

        [DataMember()]
        public DTOObjetivo ObjetivoEixoTematico { get; set; }

        [DataMember()]
        public DTOCorporativoUnidade CorporativoUnidade { get; set; }

        [DataMember()]
        public DTOTipoInstrumento TipoInstrumento { get; set; }

        [DataMember(), Required()]
        public bool Ativo { get; set; }
    }
}
