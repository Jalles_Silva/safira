﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTORelatorioConsubstanciadoUnico
    {
        [DataMember()]
        public int CodigoSumarioExecutivo { get; set; }

        [DataMember()]
        public string DescricaoSumarioExecutivo { get; set; }

        [DataMember()]
        public int CodigoApresentacao { get; set; }

        [DataMember()]
        public string DescricaoApresentacao { get; set; }

        [DataMember()]
        public int CodigoPlanoEstrategicoPlanoGestaoAnual { get; set; }

        [DataMember()]
        public string DescricaoPlanoEstrategicoPlanoGestaoAnual { get; set; }
    }
}
