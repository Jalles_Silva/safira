﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOSituacaoMeta
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqSituacaoMeta { get; set; }

        [DataMember(), Required(), StringLength(50)]
        public string NoUsuario { get; set; }

        [DataMember(), Required()]
        public DateTime DataAlteracao { get; set; }

        [DataMember(), Required()]
        public DTOStatusMeta StatusMeta { get; set; }

        [DataMember(), Required()]
        public DTOMeta Meta { get; set; }
        
        [DataMember(), Required()]
        public DTOAnexo Anexo { get; set; }
        
        [DataMember(), Required()]
        public string DescricaoJustificativaCancelamento { get; set; }
    }
}