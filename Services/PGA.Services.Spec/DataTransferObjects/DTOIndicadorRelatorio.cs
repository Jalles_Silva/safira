﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOIndicadorRelatorio
    {
        [DataMember()]
        public int CodigoSeqIndicador { get; set; }

        [DataMember()]
        public string DescricaoArea { get; set; }

        [DataMember()]
        public int CodigoUnidade { get; set; }

        [DataMember()]
        public int TotalMetasCadastradas { get; set; }

        [DataMember()]
        public int IndicadoresCadastradosExercicio { get; set; }

        [DataMember()]
        public int IndicadoresCadastradasPeriodo { get; set; }

        [DataMember()]
        public decimal ResultadoIndicador { get; set; }

        [DataMember()]
        public decimal ValorAlvo { get; set; }

        [DataMember()]
        public int IndicadoresPrazoPeriodo { get; set; }

        [DataMember()]
        public int IndicadoresAlimentadosPeriodo { get; set; }

        [DataMember()]
        public int AnoExercicio { get; set; }

        [DataMember()]
        public string DescricaoIndicador { get; set; }

        [DataMember()]
        public int CodigoMeta { get; set; }

        [DataMember()]
        public string DescricaoMeta { get; set; }

        [DataMember()]
        public string LinhaBase { get; set; }

        [DataMember()]
        public DateTime MesReferencia { get; set; }

        [DataMember()]
        public DateTime DataAlvo { get; set; }

        [DataMember()]
        public DateTime? DataUltimaAtualizacao { get; set; }
        
        [DataMember()]
        public int CodigoTipoValor { get; set; }
        [DataMember()]
        public int QtdMetasInternas { get; set; }

        [DataMember()]
        public bool ObjetivoEstrategico { get; set; }

        public string ValorAlvoMascara
        {
            get
            {
                return obterTipoValor(Convert.ToInt32(ValorAlvo), Convert.ToInt32(CodigoTipoValor));
            }
        }

        public string ResultadoIndicadorPercentual
        {
            get
            {
                return obterTipoValor(Convert.ToInt32(ResultadoIndicador), Convert.ToInt32(CodigoTipoValor));
            }
        }

        public string obterTipoValor(int valorAlvo, int CodigoTipoValor)
        {
            string valor;

            if (valorAlvo > 0 && CodigoTipoValor > 0)
            {
                if (CodigoTipoValor == 2)
                    valor = valorAlvo.ToString() + "%";
                else
                    valor = valorAlvo.ToString();
            }
            else
                valor = "-";

            return valor;
        }

        public string ResultadoIndicadorValorAlvo
        {
            get
            {
                return obterResultadoIndicadorValorAlvo(Convert.ToDecimal(ResultadoIndicador), Convert.ToDecimal(ValorAlvo));
            }
        }

        public string obterResultadoIndicadorValorAlvo(decimal ResultadoIndicador, decimal ValorAlvo)
        {
            string valor = string.Empty;
            decimal valorDividido = 0;

            if (ResultadoIndicador > 0 && ValorAlvo > 0)
            {
                valorDividido = Convert.ToInt64(((ResultadoIndicador / ValorAlvo) * 100));
                valor = valorDividido.ToString() + "%";
            }
            else
                valor = "-";

            return valor;
        }


        public string PercentualDesempenhoIndicadoresPeriodo
        {
            get
            {
                return obterPercentualDesempenhoIndicadoresPeriodo(Convert.ToDecimal(ResultadoIndicador), Convert.ToDecimal(ValorAlvo));
            }
        }

        public string obterPercentualDesempenhoIndicadoresPeriodo(decimal ResultadoIndicador, decimal ValorAlvo)
        {
            string valor = string.Empty;
            decimal valorDividido = 0;

            if (ResultadoIndicador > 0 && ValorAlvo > 0)
            {
                valorDividido = Convert.ToInt64(((ResultadoIndicador / ValorAlvo) * 100));
                valor = valorDividido > 0 ? valorDividido.ToString() + "%" : "0";
            }
            else
                valor = "0";

            return valor;
        }

        public string IndicadorPrazoPeriodoMaisPercentual
        {
            get
            {
                return obterIndicadorPrazoPeriodoMaisPercentual(IndicadoresCadastradasPeriodo, IndicadoresPrazoPeriodo);
            }
        }

        private string obterIndicadorPrazoPeriodoMaisPercentual(int IndicadoresPrazoPeriodo, int IndicadoresCadastradasPeriodo)
        {
            string valor = string.Empty;
            decimal valorDividido = 0;

            if (IndicadoresPrazoPeriodo > 0 && IndicadoresCadastradasPeriodo > 0)
            {
                valorDividido = Convert.ToInt64(((IndicadoresPrazoPeriodo / IndicadoresCadastradasPeriodo) * 100));
                valor =  IndicadoresPrazoPeriodo.ToString() + " (" + valorDividido.ToString() + "%)";
            }
            else
                valor = "0 (0%)";

            return valor;
        }
    } 
}