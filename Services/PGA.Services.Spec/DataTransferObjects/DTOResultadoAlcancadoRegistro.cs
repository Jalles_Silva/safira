﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOResultadoAlcancadoRegistro
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqResultadoAlcancado { get; set; }

        [DataMember(), Required(), StringLength(300)]
        public string DescricaoResultadoAlcancado { get; set; }

        [DataMember(), Required()]
        public bool Registrado { get; set; }

        [DataMember(), Required()]
        public bool Ativo { get; set; }

        [DataMember(), Required()]
        public DateTime DataInclusao { get; set; }

        [DataMember(), Required()]
        public DTOExercicio Exercicio { get; set; }

        [DataMember()]
        public DTOObjetivo Objetivo { get; set; }

        [DataMember()]
        public DTOIniciativa Iniciativa { get; set; }

        [DataMember()]
        public DTOCorporativoUnidade CorporativoUnidade { get; set; }

    }
}
