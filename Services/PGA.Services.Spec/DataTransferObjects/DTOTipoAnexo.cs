﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOTipoAnexo
    {
        [DataMember(), Key(), Required()]
        public int Cd_Seq_Tipo_Anexo { get; set; }

        [DataMember(), Required(), StringLength(100)]
        public string Ds_Tipo_Anexo { get; set; }
    }
}
