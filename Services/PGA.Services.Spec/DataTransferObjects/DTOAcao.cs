﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOAcao
    {
        [DataMember(), Key(), Required(), DisplayName("CodigoSeqAcao")]
        public int CodigoSeqAcao { get; set; }

        [DataMember(), StringLength(100), DisplayName("DescricaoAtividadeAcao")]
        public string DescricaoAtividadeAcao { get; set; }

        [DataMember(), StringLength(100), DisplayName("DescricaoEstrategiaAcao")]
        public string DescricaoEstrategiaAcao { get; set; }

        [DataMember(), StringLength(200), DisplayName("DescricaoResponsavel")]
        public string DescricaoResponsavel { get; set; }

        [DataMember(), DisplayName("DataInicioAcao")]
        public DateTime? DataInicioAcao { get; set; }

        [DataMember(), DisplayName("DataFimAcao")]
        public DateTime? DataFimAcao { get; set; }

        [DataMember(), DisplayName("Peso")]
        public int Peso { get; set; }

        [DataMember(), DisplayName("StatusAcao")]
        public int? StatusAcao { get; set; }

        [DataMember()]
        [Map("Meta.CodSeqMeta")]
        public int CodigoSeqMeta { get; set; }

        [DataMember()]
        [Map("Meta.DescricaoMeta")]
        public string DescricaoMeta { get; set; }

        [DataMember]
        [Map("Meta.CorporativoUnidade.CodigoUnidade")]
        public string CodigoUnidade { get; set; }
        

        [DataMember()]
        [Map("Meta.StatusMeta")]
        public DTOStatusMeta StatusMeta { get; set; }

        [DataMember()]
        public IList<DTORecurso> Recursos { get; set; }

        [DataMember]
        public DateTime? DataUltimoReporte { get; set; }

        [DataMember]
        public int? PercentualExecucao { get; set; }

        [DataMember()]
        [Map("Meta.Exercicio.Ano")]
        public int AnoExercicio { get; set; }

        [DataMember()]
        [Map("Meta.CorporativoUnidade.DescricaoUnidade")]
        public string UnidadeCorporativa { get; set; }

        [DataMember()]
        [Map("Meta.Exercicio.Fase.CodigoSeqFase")]
        public int CodigoFaseExercicio { get; set; }
    }
}