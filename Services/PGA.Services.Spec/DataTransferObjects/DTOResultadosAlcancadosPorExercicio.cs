﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOResultadosAlcancadosPorExercicio
    {
        [DataMember()]
        public string DescricaoObjetivoIniciativa { get; set; }

        [DataMember()]
        public string ResultadoAlcancadoRegistrado { get; set; }

        [DataMember()]
        public int CodSeqExercicio { get; set; }

        [DataMember()]
        public int AnoExcercicio { get; set; }

        [DataMember()]
        public int CodigoUnidade { get; set; }

        [DataMember()]
        public string DescricaoUnidade { get; set; }

        [DataMember()]
        public int CodSeqResultadoAlcancado { get; set; }

        [DataMember()]
        public string ResultadoAlcancadoRegistradoUnidade { get; set; }
    }
}
