﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOResultadosAlcancados
    {
        [DataMember()]
        public int CodSeqObjetivoIniciativa { get; set; }

        [DataMember()]
        public int CodTipo { get; set; }

        [DataMember()]
        public string DescricaoTipo { get; set; }

        [DataMember()]
        public string DescricaoObjetivoIniciativa { get; set; }

        [DataMember()]
        public int AnoExcercicio { get; set; }

        [DataMember()]
        public string DescricaoUnidade { get; set; }

        [DataMember()]
        public int CodigoUnidade { get; set; }

        [DataMember()]
        public string ResultadoAlcancadoRegistrado { get; set; }

        [DataMember()]
        public int CodSeqResultadoAlcancado { get; set; }

        [DataMember()]
        public int CodFase { get; set; }

        [DataMember()]
        public int CodMeta { get; set; }
    }
}
