﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOEncaminhamento
    {
        [DataMember(), Key(), Required()]
        public Int32 CodigoSeqEncaminhamento { get; set; }

        [DataMember(), Required(), StringLength(300)]
        public String DescricaoEncaminhamento { get; set; }

        [DataMember(), Required()]
        public DTOAcompanhamento Acompanhamento { get; set; }

        [DataMember(), Required()]
        public DTOCorporativoUnidade CorporativoUnidade { get; set; }

        [DataMember(), Required()]
        public DTOMeta Meta { get; set; }

        [DataMember(), Required()]
        public DateTime PrazoAtendimento { get; set; }

        [DataMember(), Required()]
        public DTOStatusEncaminhamento StatusEncaminhamento { get; set; }
    }
}
