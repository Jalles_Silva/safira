﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOAcaoRelatorio
    {
        [DataMember()]
        public int CodigoSeqAcao { get; set; }

        [DataMember()]
        public string DescricaoArea { get; set; }

        [DataMember()]
        public int CodigoUnidade { get; set; }

        [DataMember()]
        public int CodigoMeta { get; set; }

        [DataMember()]
        public string DescricaoMeta { get; set; }

        [DataMember()]
        public string DescricaoAtividade { get; set; }

        [DataMember()]
        public string DescricaoEstrategia { get; set; }

        [DataMember()]
        public int PercentualExecucao { get; set; }

        [DataMember()]
        public decimal RecursoEstimado { get; set; }

        [DataMember()]
        public decimal RecursoOrcamentarioUtilizado { get; set; }

        [DataMember()]
        public DateTime? DataInicioAtividade { get; set; }

        [DataMember()]
        public DateTime? DataFimAtividade { get; set; }

        [DataMember()]
        public DateTime? DataUltimaAtualizacao { get; set; }

        [DataMember()]
        public int TotalMetasCadastradas { get; set; }

        [DataMember()]
        public int AtividadesCadastradasExercicio { get; set; }

        [DataMember()]
        public int AtividadesCadastradasPeriodo { get; set; }

        [DataMember()]
        public int AtividadesConcluidas { get; set; }

        [DataMember()]
        public int AtividadesPeriodoPrazo { get; set; }

        [DataMember()]
        public int AtividadesRecursoFinanceiro { get; set; }

        [DataMember()]
        public int AnoExercicio { get; set; }

        [DataMember()]
        public int AtividadesConcluidasPeriodo { get; set; }

        [DataMember()]
        public decimal RecursoFinanceiro { get; set; }
        [DataMember()]
        public int QtdMetasInternas { get; set; }

        public string AtividadesConcluidasPercentual
        {
            get
            {
                return obterAtividadePeriodo(Convert.ToDecimal(AtividadesConcluidas), Convert.ToDecimal(AtividadesCadastradasPeriodo));
            }
        }

        public string obterAtividadePeriodo(decimal AtividadesConcluidas, decimal AtividadesCadastradasPeriodo)
        {
            string valor = string.Empty;
            decimal valorDividido = 0;

            if (AtividadesConcluidas > 0 && AtividadesCadastradasPeriodo > 0)
            {
                valorDividido = Convert.ToInt32(((AtividadesConcluidas / AtividadesCadastradasPeriodo) * 100));
                valor = Convert.ToInt32(AtividadesConcluidas).ToString() + " (" + valorDividido.ToString() + "%)";
            }
            else
                valor = "0 (0%)";

            return valor;
        }

        public string PercentualExecucaoMascara
        {
            get
            {
                return PercentualExecucao.ToString() + "%";
            }
        }


        public string AtividadesPeriodoPrazoPercentual
        {
            get
            {
                return obterAtividadePrazo(Convert.ToDecimal(AtividadesPeriodoPrazo), Convert.ToDecimal(AtividadesCadastradasPeriodo));
            }
        }


        public string obterAtividadePrazo(decimal AtividadesPeriodoPrazo, decimal AtividadesCadastradasPeriodo)
        {
            string valor = string.Empty;
            decimal valorDividido = 0;

            if (AtividadesPeriodoPrazo > 0 && AtividadesCadastradasPeriodo > 0)
            {
                valorDividido = Convert.ToInt32(((AtividadesPeriodoPrazo / AtividadesCadastradasPeriodo) * 100));
                valor = Convert.ToInt32(AtividadesPeriodoPrazo).ToString() + " (" + valorDividido.ToString() + "%)";
            }
            else
                valor = "0 (0%)";

            return valor;
        }

        public string RecursoEstimadoMascara
        {
            get
            {
                return RecursoEstimado > 0 ? RecursoEstimado.ToString() : "-";
            }
        }

        public string RecursoOrcamentarioUtilizadoMascara
        {
            get
            {
                return RecursoOrcamentarioUtilizado > 0 ? RecursoOrcamentarioUtilizado.ToString() : "-";
            }
        }
    }
}