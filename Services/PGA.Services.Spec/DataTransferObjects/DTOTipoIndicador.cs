﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOTipoIndicador
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqTipoIndicador { get; set; }

        [DataMember(), Required(), StringLength(100)]
        public string DescricaoTipoIndicador { get; set; }
    }
}