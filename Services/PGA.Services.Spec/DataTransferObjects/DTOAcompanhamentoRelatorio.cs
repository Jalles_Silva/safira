﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOAcompanhamentoRelatorio
    {
        [DataMember()]
        [Map("Acompanhamento.CodigoSeqAcompanhamento")]
        public int CodigoSeqAcompanhamento { get; set; }

        [DataMember()]
        [Map("Acompanhamento.TipoAcompanhamento.DescricaoTipoAcompanhamento")]
        public string TipoAcompanhamento { get; set; }

        [DataMember()]
        [Map("Acompanhamento.Exercicio.Ano")]
        public int AnoExercicio { get; set; }

        [DataMember()]
        [Map("Acompanhamento.Anexo.DescricaoAnexo")]
        public string Anexo { get; set; }

        [DataMember()]
        [Map("Acompanhamento.DataAcompanhamento")]
        public DateTime DataAcompanhamento { get; set; }

        [DataMember()]
        [Map("Acompanhamento.StatusAcompanhamento.DescricaoStatusAcompanhamento")]
        public string StatusAcompanhamento { get; set; }

        [DataMember()]
        public int CodigoSeqEncaminhamento { get; set; }

        [DataMember()]
        public string DescricaoEncaminhamento { get; set; }

        [DataMember()]
        [Map("CorporativoUnidade.DescricaoUnidade")]
        public string CorporativoUnidade { get; set; }

        [DataMember()]
        public DateTime? PrazoAtendimento { get; set; }

        [DataMember()]
        [Map("StatusEncaminhamento.DescricaoStatusEncaminhamento")]
        public string StatusEncaminhamento { get; set; }
    }
}
