﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOTipoAcompanhamento
    {
        [DataMember(), Key(), Required()]
        public Int32 CodigoSeqTipoAcompanhamento { get; set; }

        [DataMember(), Required(), StringLength(150)]
        public String DescricaoTipoAcompanhamento { get; set; }

        [DataMember(), Required()]
        public Boolean Ativo { get; set; }
    }
}
