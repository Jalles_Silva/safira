﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTORelatorioConsubstanciado
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqRelatorioConsubstanciado { get; set; }

        [DataMember(), Required(), StringLength(8000)]
        public string DescricaoRelatorioConsubstanciado { get; set; }

        [DataMember(), Required()]
        public bool Ativo { get; set; }

        [DataMember(), Required()]
        public DateTime DataInclusao { get; set; }

        [DataMember(), Required()]
        public DTOExercicio Exercicio { get; set; }

        [DataMember()]
        public DTOTipoTexto TipoTexto { get; set; }
    }
}
