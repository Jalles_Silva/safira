﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOTipoIniciativa
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqTipoIniciativa { get; set; }

        [DataMember(), Required(), StringLength(50)]
        public string DescricaoTipoIniciativa { get; set; }
    }
}
