﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOReporteEncaminhamento
    {
        [DataMember(), Key(), Required()]
        public Int32 CodigoSeqReporteEncaminhamento { get; set; }

        [DataMember(), Required(), StringLength(5000)]
        public String DescricaoReporteEncaminhamento { get; set; }

        [DataMember(), Required()]
        public DTOEncaminhamento Encaminhamento { get; set; }

        [DataMember(), Required()]
        public DateTime DataReporte { get; set; }

        [DataMember(), Required(), StringLength(50)]
        public String Usuario { get; set; }
    }
}
