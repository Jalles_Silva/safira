﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DtoRelatorioObjetivoEstrategicoDetalhado
    {
        [DataMember()]
        public string DescricaoObjetivo { get; set; }

        [DataMember()]
        public string DescricaoUnidade { get; set; }

        [DataMember()]
        public int TotalMetas { get; set; }

        [DataMember()]
        public int TotalIndicadores { get; set; }

        [DataMember()]
        public int TotalAtividades { get; set; }

        [DataMember()]
        public decimal ResultadoMeta { get; set; }

        [DataMember()]
        public decimal ResultadoIndicador { get; set; }

        [DataMember()]
        public decimal ResultadoAtividade { get; set; }

        [DataMember()]
        public decimal ValorAlvoMeta { get; set; }

        [DataMember()]
        public decimal ValorAlvoIndicador { get; set; }

        [DataMember()]
        public decimal ValorAlvoAtividade { get; set; }

        [DataMember()]
        public decimal PencentualAtividadeMeta { get; set; }

        [DataMember()]
        public decimal PencentualAtividadeIndicador { get; set; }

        [DataMember()]
        public decimal PencentualAtividade { get; set; }

        [DataMember()]
        public string DescricaoTipo { get; set; }


        public decimal ResultadoMetaCalculado
        {
            get
            {
                return obterPercentualDesempenhoIndicadoresPeriodo(Convert.ToDecimal(ResultadoMeta), Convert.ToDecimal(ValorAlvoMeta));
            }
        }

        public decimal ResultadoIndicadorCalculado
        {
            get
            {
                return obterPercentualDesempenhoIndicadoresPeriodo(Convert.ToDecimal(ResultadoIndicador), Convert.ToDecimal(ValorAlvoIndicador));
            }
        }

        public decimal ResultadoAtividadeCalculado
        {
            get
            {
                return obterPercentualDesempenhoIndicadoresPeriodo(Convert.ToDecimal(ResultadoAtividade), Convert.ToDecimal(ValorAlvoAtividade));
            }
        }

        public decimal obterPercentualDesempenhoIndicadoresPeriodo(decimal Resultado, decimal ValorAlvo)
        {
            decimal valor = 0;
            decimal valorDividido = 0;

            if (Resultado > 0 && ValorAlvo > 0)
            {
                valorDividido = Convert.ToInt64(((Resultado / ValorAlvo) * 100));
                valor = valorDividido;
            }

            return valor;
        }

    }
}
