﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTORelatorioConsubstanciadoDetalhado
    {
        [DataMember()]
        public string DescricaoObjetivo { get; set; }

        [DataMember()]
        public string DescricaoTipo { get; set; }

        [DataMember()]
        public string DescricaoUnidade { get; set; }

        [DataMember()]
        public int TotalMetas { get; set; }

        [DataMember()]
        public int CemMetas { get; set; }

        [DataMember()]
        public int NoventaMetas { get; set; }

        [DataMember()]
        public int TotalIndicadores { get; set; }

        [DataMember()]
        public int CemIndicadores { get; set; }

        [DataMember()]
        public int NoventaIndicadores { get; set; }

        [DataMember()]
        public int TotalAtividades { get; set; }

        [DataMember()]
        public int CemAtividades { get; set; }

        [DataMember()]
        public int NoventaAtividades { get; set; }
    }
}
