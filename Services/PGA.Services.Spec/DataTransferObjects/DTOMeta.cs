﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SQFramework.Core.Reflection;
using PGA.Common;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOMeta
    {
        [DataMember(), Key(), Required(), DisplayName("CodSeqMeta")]
        public int CodSeqMeta { get; set; }

        [DataMember(), Required(), StringLength(100), DisplayName("DescricaoMeta")]
        public string DescricaoMeta { get; set; }

        [DataMember(), StringLength(4000), DisplayName("DescricaoJustificativa")]
        public string DescricaoJustificativa { get; set; }

        [DataMember(), StringLength(4000), DisplayName("DescricaoResultadoEsperado")]
        public string DescricaoResultadoEsperado { get; set; }

        [DataMember(), StringLength(4000), DisplayName("DescricaoPremissa")]
        public string DescricaoPremissa { get; set; }

        [DataMember(), StringLength(4000), DisplayName("DescricaoRestricao")]
        public string DescricaoRestricao { get; set; }

        [DataMember(), Required(), StringLength(500), DisplayName("NoCoordenacao")]
        public string NoCoordenacao { get; set; }

        [DataMember(), StringLength(100), DisplayName("DescricaoTipoMeta")]
        public string DescricaoTipoMeta { get; set; }

        [DataMember()]
        [Map("TipoMeta.CodigoSeqTipoMeta")]
        public int CodigoSeqTipoMeta { get; set; }

        [DataMember()]
        [Map("CorporativoUnidade.CodigoUnidade")]
        public int CodigoUnidade { get; set; }

        [DataMember()]
        [Map("Exercicio.CodigoSeqExercicio")]
        public int CodigoSeqExercicio { get; set; }

        [DataMember()]
        public string NomeUsuario { get; set; }

        [DataMember()]
        public DTOStatusMeta StatusMeta { get; set; }

        [DataMember()]
        public DTOCorporativoUnidade CorporativoUnidade { get; set; }

        [DataMember()]
        public DTOExercicio Exercicio { get; set; }

        [DataMember()]
        [Map("IniciativaPe.CodigoSeqIniciativa")]
        public int CodigoSeqIniciativaPE { get; set; }

        [DataMember()]
        public DTOIniciativa IniciativaPe { get; set; }

        [DataMember()]
        public DTOIniciativa IniciativaPpa { get; set; }

        [DataMember()]
        public DTOIniciativa IniciativaMissao { get; set; }

        [DataMember()]
        public string DescricaoUnidade { get; set; }

        [DataMember()]
        public string DescricaoStatusMeta { get; set; }

        [DataMember()]
        public string CodigoStatusMeta { get; set; }

        [DataMember()]
        public string AnoExercicio { get; set; }

        [DataMember]
        public IList<DTOAcaoRelatorio> Acoes { get; set; }

        [DataMember]
        public IList<DTOIndicadorRelatorio> Indicadores { get; set; }

        [DataMember()]
        [Map("Exercicio.Fase.CodigoSeqFase")]
        public int CodigoFaseExercicio { get; set; }
        
        [DataMember()]
        [Map("GestaoDeRiscos.CodigoSeqInstrumento")]
        public int CodigoSeqGestaoDeRiscos { get; set; }
        
        [DataMember()]
        [Map("AgendaRegulatoria.CodigoSeqInstrumento")]
        public int CodigoSeqAgendaRegulatoria { get; set; }
        
        [DataMember()]
        [Map("Desburocratizacao.CodigoSeqInstrumento")]
        public int CodigoSeqDesburocratizacao { get; set; }
        
        [DataMember()]
        [Map("Integridade.CodigoSeqInstrumento")]
        public int CodigoSeqIntegridade { get; set; }

        [DataMember(), DisplayName("IniciativaEstrategica")]
        public bool? StatusIniciativaEstrategica { get; set; }

        [DataMember(), StringLength(500), DisplayName("DescricaoOutros")]
        public string DescricaoOutros { get; set; }
        
        [DataMember(), StringLength(500), DisplayName("DescricaoSociedadeUsuarioBeneficio")]
        public string DescricaoSociedadeUsuarioBeneficio { get; set; }

        [DataMember(), StringLength(500), DisplayName("DescricaoSociedadeUsuarioImpacto")]
        public string DescricaoSociedadeUsuarioImpacto { get; set; }

        [DataMember(), StringLength(500), DisplayName("DescricaoInstitucionalBeneficio")]
        public string DescricaoInstitucionalBeneficio { get; set; }

        [DataMember(), StringLength(500), DisplayName("DescricaoInstitucionalImpacto")]
        public string DescricaoInstitucionalImpacto { get; set; }

        [DataMember(), StringLength(500), DisplayName("DescricaoGovernoBeneficio")]
        public string DescricaoGovernoBeneficio { get; set; }

        [DataMember(), StringLength(500), DisplayName("DescricaoGovernoImpacto")]
        public string DescricaoGovernoImpacto { get; set; }

        [DataMember(), StringLength(500), DisplayName("DescricaoSetorReguladoBeneficio")]
        public string DescricaoSetorReguladoBeneficio { get; set; }

        [DataMember(), StringLength(500), DisplayName("DescricaoSetorReguladoImpacto")]
        public string DescricaoSetorReguladoImpacto { get; set; }

        [DataMember(), DisplayName("StatusMetaInterna")]
        public bool StatusMetaInterna { get; set; }

    }
}
