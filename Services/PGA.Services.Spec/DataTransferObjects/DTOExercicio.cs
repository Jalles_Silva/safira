﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOExercicio
    {
        [DataMember(), Key(), Required(), DisplayName("CodigoSeqExercicio")]
        public int CodigoSeqExercicio { get; set; }

        [DataMember(), Required(), DisplayName("Ano")]
        public short Ano { get; set; }

        [DataMember(), Required(), StringLength(50), DisplayName("NomeUsuario")]
        public string NomeUsuario { get; set; }

        [DataMember(), Required(), DisplayName("DataCadastro")]
        public DateTime DataCadastro { get; set; }

        [DataMember(), Required(), DisplayName("Fase")]
        public DTOFase Fase { get; set; }
    }
}