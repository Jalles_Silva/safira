﻿using System;
using System.Runtime.Serialization;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOObjetivoEstrategicoTotalizadorDetalhadado
    {
        [DataMember()]
        public int AnoExercicio { get; set; }

        [DataMember()]
        public string DescricaoUnidade { get; set; }

        [DataMember()]
        public string DescricaoObjetivo { get; set; }

        [DataMember()]
        public int CodSeqObjetivo { get; set; }

        [DataMember()]
        public decimal TotalRecursoEstimado { get; set; }

        [DataMember()]
        public decimal TotalRecursoEstimadoTotal { get; set; }

        [DataMember()]
        public decimal TotalRecursoOrcamentarioUtilizado { get; set; }

        [DataMember()]
        public decimal TotalRecursoOrcamentarioUtilizadoTotal { get; set; }
    }
}
