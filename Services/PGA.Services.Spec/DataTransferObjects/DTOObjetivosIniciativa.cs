﻿using System;
using System.Runtime.Serialization;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOObjetivosIniciativa
    {
        [DataMember()]
        public string DescricaoTipo { get; set; }

        [DataMember()]
        public int CodigoObjetivo { get; set; }

        [DataMember()]
        public int CodigoIniciativa { get; set; }

        [DataMember()]
        public int QtdRecurso { get; set; }

        [DataMember()]
        public int QtdReporte { get; set; }
    }
}
