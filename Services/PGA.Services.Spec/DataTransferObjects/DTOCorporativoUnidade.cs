﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOCorporativoUnidade
    {
        [DataMember(), Key(), Required()]
        public int CodigoUnidade { get; set; }

        [DataMember()]
        public int? CodigoUnidadePai { get; set; }

        [DataMember(), StringLength(100)]
        public string DescricaoUnidade { get; set; }

        [DataMember(), Required()]
        public bool Ativo { get; set; }
    }
}