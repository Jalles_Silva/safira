﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOObjetivoEstrategicoDetalhado
    {
        [DataMember()]
        public string DescricaoObjetivo { get; set; }

        [DataMember()]
        public string DescricaoUnidade { get; set; }

        [DataMember()]
        public decimal RecursosPrevistosAssociados { get; set; }

        [DataMember()]
        public decimal RecursosPrevistosRoteados { get; set; }

        [DataMember()]
        public decimal RecursosUtilizadosAssociados { get; set; }

        [DataMember()]
        public decimal RecursosUtilizadosRoteados { get; set; }

    }
}
