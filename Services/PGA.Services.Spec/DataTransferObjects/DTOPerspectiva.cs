﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace PGA.Services.Spec.DataTransferObjects
{
    [DataContract()]
    [Serializable()]
    public class DTOPerspectiva
    {
        [DataMember(), Key(), Required()]
        public int CodigoSeqPerspectiva { get; set; }

        [DataMember(), Required(), StringLength(10)]
        public string NuPerspectiva { get; set; }

        [DataMember(), Required(), StringLength(100)]
        public string DescricaoPerspectiva { get; set; }

        [DataMember(), Required()]
        public bool Ativo { get; set; }
    }
}