﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using SQFramework.Spring;
using SQFramework.Data.Pagging;
using SQFramework.Web.Report;
using PGA.Services.Spec.DataTransferObjects;
using PGA.Common;

namespace PGA.Services.Spec.Services
{
    [ServiceContract]
    [ObjectMap("PGAService", true)]
    public interface IPGAService : IServiceBase
    {
        [OperationContract]
        byte[] ExportReportRelatorioGlobalAtividade(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending);

        [OperationContract]
        byte[] ExportReportRelatorioGlobalAtividadeSimples(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending);

        [OperationContract]
        byte[] ExportReportRelatorioDetalheAtividade(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending);

        [OperationContract]
        byte[] ExportReportRelatorioDetalheAtividadeSimples(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending);

        [OperationContract]
        byte[] ExportReportRelatorioDetalheIndicador(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending);

        [OperationContract]
        byte[] ExportReportRelatorioDetalheIndicadorSimples(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending);

        [OperationContract]
        byte[] ExportReportRelatorioGlobalIndicador(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending);

        [OperationContract]
        byte[] ExportReportRelatorioGlobalIndicadorSimples(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending);

        [OperationContract]
        byte[] ExportReportRelatorioDetalheMeta(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int codigoTipoMeta,
            string Unidade, int AnoExercicio, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending);

        [OperationContract]
        byte[] ExportReportRelatorioDetalheMetaSimples(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int codigoTipoMeta,
            string Unidade, int AnoExercicio, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, bool orderAscending);

        [OperationContract]
        byte[] ExportReportRelatorioGlobalMeta(bool verAlinhadasPE, bool verAlinhadasPPA, bool verAlinhadasMI, bool verAdministrativas,
            bool verFiscalizacoes, bool verRegulacoes, string Unidade, int AnoExercicio, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty, 
            bool orderAscending);

        [OperationContract]
        byte[] ExportReportRelatorioGlobalMetaSimples(bool verAlinhadasPE, bool verAlinhadasPPA, bool verAlinhadasMI, bool verAdministrativas,
            bool verFiscalizacoes, bool verRegulacoes, string Unidade, int AnoExercicio, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, ReportViewerHelper.ReportType reportType, string orderProperty,
            bool orderAscending);

        [OperationContract]
        byte[] ExportReportRelatorioAcompanhamentos(int CodigoSeqAcompanhamento, ReportViewerHelper.ReportType reportType);

        [OperationContract]
        byte[] ExportReportRelatorioConsubstanciadoAnual(int anoExercicio, ReportViewerHelper.ReportType reportType);

        [OperationContract]
        void AbrirExercicio(string nomeUsuario);

        [OperationContract]
        void PromoverFaseExercicio(int ano, string nomeUsuario);

        [OperationContract]
        DTOExercicio ObterExercicioAtual();

        [OperationContract]
        int SalvarRelatorioConsubstanciado(DTORelatorioConsubstanciado dto);

        [OperationContract]
        IList<DTOExercicio> ListarExercicios();

        [OperationContract]
        IList<DTOTipoValor> ListarTipoValores();

        [OperationContract]
        DTOControleIndicador SalvarControleIndicador(DTOControleIndicador dto);

        [OperationContract]
        DTOControleIndicador ObterControleIndicador(int codigoSeqControleIndicador);

        [OperationContract]
        void DeletarControleIndicador(int codigoSeqControleAcao, bool usuarioAdministrador);

        [OperationContract]
        PageMessage<DTOMetaResultadosAlcancados> ListaMetasPorObjetivoIniciativa(int codObjetivoIniciativa, short anoExercicio, int codTipo, int codUnidade, int startIndex, int pageSize);

        [OperationContract]
        IList<DTORelatorioConsubstanciado> ListarRelatorioConsubstanciado(int codigoSeqExercicio);

        [OperationContract]
        IList<DTOObjetivoEstrategicoTotalizadorDetalhadado> ListarObjetivoEstrategicoTotal(int anoExercicio, int codObjetivo, int codTipo);

        [OperationContract]
        PageMessage<DTOControleIndicador> ListarControlesPorIndicador(int codigoIndicador, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        IList<DTOResultadosAlcancadosPorExercicio> ListarResultadosAlcancadosPorExercicio(int anoExercicio);

        [OperationContract]
        DTOControleAcao SalvarControleAcao(DTOControleAcao dto);

        [OperationContract]
        DTOResultadoAlcancadoRegistro ObterResultadoAlcancado(int codigoSeqResultadoAlcancado);

        [OperationContract]
        DTOControleAcao ObterControleAcao(int codigoSeqControleAcao);

        [OperationContract]
        void DeletarControleAcao(int codigoSeqControleAcao, bool usuarioAdministrador);

        [OperationContract]
        PageMessage<DTOControleAcao> ListarControlesPorAcao(int codigoAcao, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        PageMessage<DTOControleIndicadorDetalhado> ListarIndicadoresPorFiltros(DTOFiltroControle filtro);

        [OperationContract]
        PageMessage<DTOAcaoDetalhado> ListarAcoesPorFiltros(DTOFiltroControle filtro);

        [OperationContract]
        void SalvarAcao(DTOAcao dto);

        [OperationContract]
        DTOAcao ObterAcao(int codigoSeqAcao);
        
        [OperationContract]
        void AlterarStatusIndicador(int codigoSeqAcao, int statusAcao);
        
        [OperationContract]
        void DeletarAcao(int codigoSeqAcao);

        [OperationContract]
        PageMessage<DTOAcao> ListarAcoesPorMeta(int codigoSeqMeta, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        IList<DTOTipoIndicador> ListarTipoIndicadores();

        [OperationContract]
        DTOResultadoAlcancadoRegistro ObterResultadoAlcancadoCompleto(int codSeqResultadoAlcancado , int codSeqObjetivoIniciativa, int codTipo, int AnoExercicio);

        [OperationContract]
        IList<DTOTipoRecurso> ListarTipoRecursos();

        [OperationContract]
        void SalvarIndicador(DTOIndicador dto);

        [OperationContract]
        DTOIndicador ObterIndicador(int codigoSeqIndicador);

        [OperationContract]
        void DeletarIndicador(int codigoSeqIndicador);
        [OperationContract]
        void AlterarStatusAcao(int codigoSeqIndicador, int statusIndicador);

        [OperationContract]
        PageMessage<DTOIndicador> ListarIndicadoresPorMeta(int codigoSeqMeta, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        int SalvarResultadosAlcancados(DTOResultadoAlcancadoRegistro registro);

        [OperationContract]
        int SalvarMeta(DTOMeta dto);

        [OperationContract]
        DTOMeta ObterMeta(int codSeqMeta);

        [OperationContract]
        DTOMetaDetalhada ObterMetaDetalhada(int codSeqMeta);

        [OperationContract]
        DTOSituacaoMeta ObterSituacaoMetaAtual(DTOMetaDetalhada meta);

        [OperationContract]
        void DeletarMeta(int codSeqMeta);

        [OperationContract]
        PageMessage<DTOMeta> ListarMetas(int startIndex, int pageSize);

        [OperationContract]
        PageMessage<DTOMetaRelatorio> ListaGlobalMeta(bool verAlinhadasPE, bool verAlinhadasPPA, bool verAlinhadasMI, bool verAdministrativas,
            bool verFiscalizacoes, bool verRegulacoes, string unidade, int AnoExercicio, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize);

        [OperationContract]
        PageMessage<DTOMetaRelatorio> ListaDetalheMeta(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int codigoTipoMeta,
            string Unidade, int AnoExercicio, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize);

        [OperationContract]
        DTOResultadoAlcancadoRegistro ObterResultadosAlcancados();

        [OperationContract]
        PageMessage<DTOIndicadorRelatorio> ListaGlobalIndicador(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize);

        [OperationContract]
        PageMessage<DTOIndicadorRelatorio> ListaDetalheIndicador(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize);

        [OperationContract]
        PageMessage<DTOAcaoRelatorio> ListaGlobalAtividade(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize);

        [OperationContract]
        PageMessage<DTOAcaoRelatorio> ListaDetalheAtividade(bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int AnoExercicio,
            string Unidade, int codigoPeriodo, int codigoTipoMeta, int MetaInterna, int TipoInstrumento, int startIndex, int pageSize);

        [OperationContract]
        PageMessage<DTOMeta> ListarMetasPorUnidadeMeta(string Unidade, int AnoExercicio, int MetaInterna, int StatusMeta, int startIndex, int pageSize);

        [OperationContract]
        PageMessage<DTOMeta> ListarMetasPorUnidade(int codigoUnidade, int AnoExercicio, int startIndex, int pageSize);

        [OperationContract]
        PageMessage<DTOResultadosAlcancados> ListarResultadosAlcancados(int codigoUnidade, int AnoExercicio, int startIndex, int pageSize);

        [OperationContract]
        IList<DTOObjetivos> ListarOjetivoEstrategicoEMissaoAdministrativa();

        [OperationContract]
        IList<DTOObjetivos> ListarOjetivoEstrategico(int codTipoInciativa);

        [OperationContract]
        IList<DTOTipoMeta> ListarTipoMetas();
        
        [OperationContract]
        IList<DTOStatusMeta> ListarStatusMetas();

        [OperationContract]
        IList<DTOIniciativa> ListarIniciativaPorTipo(int tipoIniciativa);

        [OperationContract]
        IList<DTOIniciativa> ListarIniciativaPorTipoEUnidadeAtivos(TiposAlinhamento tipoIniciativa, int codigoUnidade);

        [OperationContract]
        DTOCorporativoUnidade ObterCorporativoUnidade(int codigoUnidade);

        [OperationContract]
        DTOIniciativa ObterIniciativaPorCodigo(int codigoIniciativa);

        [OperationContract]
        void AlterarStatusMeta(int codSeqMeta, int codigoStatus, string nomeUsuario, string descricaoJustificativaCancelamento, DTOAnexo anexo);

        [OperationContract]
        void DeletarAnexoMeta(int CodigoSeqMeta);

        [OperationContract]
        IList<DTOSituacaoMeta> GetSituacaoMeta(int codSeqMeta);

        [OperationContract]
        IList<DTOCorporativoUnidade> ListarCorporativoUnidadeAtivo();

        [OperationContract]
        IList<DTOFase> ListarFase();

        [OperationContract]
        IList<DTOMetaSimplificada> ListarMetaSimplificada();

        [OperationContract]
        IList<DTOMetaSimplificada> ListarMetaSimplificadaPorUnidadeEAno(int codigoUnidade, int ano);

        [OperationContract]
        IList<DTOExercicio> ListarExercicioEmAcompanhamento();

        [OperationContract]
        IList<DTOExercicio> ListarExercicioEmPlanejamento();

        [OperationContract]
        IList<DTOExercicio> ListarExercicioExcetoPlanejamento();

        [OperationContract]
        IList<DTOExercicio> ListarAnoExercicio();

        [OperationContract]
        DTOExercicio ObterFaseExercicioPorAno(Int16 AnoExercicio);

        [OperationContract]
        IList<DTOTipoResultado> ListarTipoResultado();

        [OperationContract]
        IList<DTOCorporativoUnidade> ListarCorporativoUnidadeAtivoPorExercicio(int ano);

        [OperationContract]
        IList<DTOCorporativoUnidade> ListarCorporativoUnidadePorExercicio(int ano);

        [OperationContract]
        IList<DTOCorporativoResultadosAlcancados> ListarCorporativoResultadosAlcancados(int ano);

        [OperationContract]
        void AtualizarUnidadesAtivas(List<DTOCorporativoUnidade> unidadesAtivas);

        [OperationContract]
        bool ValidarUnidadesAtivasDasMetas(List<DTOCorporativoUnidade> unidadesAtivas);

        [OperationContract]
        byte[] GerarRelatorioAcompanhamento(int anoExercicio, string Area, int codigoPeriodo, bool periodoAcumulado, int codigoTipoMeta, bool alinhadasPE, bool alinhadasPPA, bool alinhadasMI, int codigoTipoInstrumento, int MetaInterna);
        
        [OperationContract]
        PageMessage<DTOMeta> ListarInstrumentosPorUnidade(int codigoUnidade, int AnoExercicio, int startIndex, int pageSize);

        [OperationContract]
        IList<DTOTipoObjetivo> ListarTiposObjetivo();

        [OperationContract]
        IList<DTOObjetivo> ListarObjetivos();

        [OperationContract]
        IList<DTOObjetivo> ListarObjetivosPorTipo(EnumTipoObjetivo tipo);

        [OperationContract]
        PageMessage<DTOObjetivo> ListarObjetivosPorTipoESituacao(int tipoObjetivo, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        IList<DTOPerspectiva> ListarPerspectivas();

        [OperationContract]
        PageMessage<DTOPerspectiva> ListarPerspectivasPorSituacao(string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        int SalvarPerspectiva(DTOPerspectiva dto);

        [OperationContract]
        DTOPerspectiva ObterPerspectiva(int CodigoSeqInstrumento);

        [OperationContract]
        void DeletarPerspectiva(int CodigoSeqInstrumento);

        [OperationContract]
        IList<DTOTema> ListarTemas();

        [OperationContract]
        IList<DTOTema> ListarTemasPorPerspectiva(int codPerspectiva);

        [OperationContract]
        PageMessage<DTOTema> ListarTemasPorPerspectivasESituacao(int perspectiva, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        int SalvarTema(DTOTema dto);

        [OperationContract]
        DTOTema ObterTema(int CodigoSeqInstrumento);

        [OperationContract]
        void DeletarTema(int CodigoSeqInstrumento);

        [OperationContract]
        int SalvarObjetivo(DTOObjetivo dto);

        [OperationContract]
        DTOObjetivo ObterObjetivo(int CodigoSeqObjetivo);

        [OperationContract]
        void DeletarObjetivo(int CodigoSeqObjetivo);

        [OperationContract]
        IList<DTOTipoIniciativa> ListarTiposIniciativa();

        [OperationContract]
        IList<DTOIniciativa> ListarTiposIniciativaFiltro(int tipoIniciativa);

        [OperationContract]
        PageMessage<DTOIniciativa> ListarIniciativaPorTipoEUnidade(int tipoIniciativa, string unidade, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        int SalvarIniciativa(DTOIniciativa dto);

        [OperationContract]
        DTOIniciativa ObterIniciativa(int CodigoSeqIniciativa);

        [OperationContract]
        void DeletarIniciativa(int CodigoSeqIniciativa);

        [OperationContract]
        void DeletarAnexoIniciativa(int CodigoSeqIniciativa);

        [OperationContract]
        IList<DTOInstrumento> ListarInstrumentoPorTipoAtivos(TiposInstrumento tipoInstrumento);

        [OperationContract]
        IList<DTOInstrumento> ListarInstrumentoPorTipoEUnidadeAtivos(TiposInstrumento tipoInstrumento, int codigoUnidade);

        [OperationContract]
        IList<DTOTipoInstrumento> ListarTiposInstrumento();

        [OperationContract]
        PageMessage<DTOInstrumento> ListarInstrumentoPorTipoEUnidade(int tipoInstrumento, int codigoUnidade, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        int SalvarInstrumento(DTOInstrumento dto);

        [OperationContract]
        DTOInstrumento ObterInstrumento(int CodigoSeqInstrumento);

        [OperationContract]
        void DeletarInstrumento(int CodigoSeqInstrumento);

        [OperationContract]
        IList<DTOTipoAcompanhamento> ListarTiposAcompanhamento();

        [OperationContract]
        PageMessage<DTOTipoAcompanhamento> ListarTiposAcompanhamentoPorTipoESituacao(string tipoAcompanhamento, string ativo, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        int SalvarTipoAcompanhamento(DTOTipoAcompanhamento dto);

        [OperationContract]
        DTOTipoAcompanhamento ObterTipoAcompanhamento(int CodigoSeqTipoAcompanhamento);

        [OperationContract]
        void DeletarTipoAcompanhamento(int CodigoSeqTipoAcompanhamento);

        [OperationContract]
        int SalvarAnexo(DTOAnexo dto);

        [OperationContract]
        DTOAnexo ObterAnexo(int CodigoSeqAnexo);

        [OperationContract]
        DTOStatusAcompanhamento ObterStatusAcompanhamento(EnumStatusAcompanhamento status);

        [OperationContract]
        DTOStatusEncaminhamento ObterStatusEncaminhamento(EnumStatusEncaminhamento status);

        [OperationContract]
        void DeletarAnexo(int CodigoSeqAnexo);

        [OperationContract]
        PageMessage<DTOAcompanhamento> ListarAcompanhamentosPorTipoAnoEStatus(int tipoAcompanhamento, short ano, int status, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        int SalvarAcompanhamento(DTOAcompanhamento dto);

        [OperationContract]
        DTOAcompanhamento ObterAcompanhamento(int CodigoSeqAcompanhamento);

        [OperationContract]
        void AlterarStatusAcompanhamento(int CodigoSeqAcompanhamento, int status);

        [OperationContract]
        void DeletarAcompanhamento(int CodigoSeqAcompanhamento);

        [OperationContract]
        void DeletarAnexoAcompanhamento(int CodigoSeqAcompanhamento);

        [OperationContract]
        IList<DTOEncaminhamento> ListarEncaminhamentosDoAcompanhamento(int CodigoSeqAcompanhamento);

        [OperationContract]
        PageMessage<DTOEncaminhamento> ListarEncaminhamentosPorAcompanhamento(int CodigoSeqAcompanhamento, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        PageMessage<DTOEncaminhamento> ListarEncaminhamentosPorTipoUnidadeEStatus(int tipoAcompanhamento, string unidade, int status, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        int SalvarEncaminhamento(DTOEncaminhamento dto);

        [OperationContract]
        DTOEncaminhamento ObterEncaminhamento(int CodigoSeqEncaminhamento);

        [OperationContract]
        void DeletarEncaminhamento(int CodigoSeqEncaminhamento);

        [OperationContract]
        void AlterarStatusEncaminhamento(int CodigoSeqEncaminhamento, int status);

        [OperationContract]
        IList<DTORelatorioObjetivoEstrategico> ListarObjetivoEstrategicoPE(int anoExercicio, int codSeqObjectivo);

        [OperationContract]
        IList<DTORelatorioObjetivoEstrategico> ListarObjetivoEstrategicoPPA(int anoExercicio, int codSeqObjectivo);

        [OperationContract]
        IList<DTORelatorioObjetivoEstrategico> ListarMeta(int anoExercicio, int codTipoMeta);

        [OperationContract]
        IList<DTORelatorioObjetivoEstrategico> ListarInstrumentos(int anoExercicio, int codTipoInstrumento);

        [OperationContract]
        IList<DTORelatorioObjetivoEstrategico> ListarIniciativa(int anoExercicio, int codTipoIniciativa);

        [OperationContract]
        IList<DTOReporteEncaminhamento> ListarReportesDoEncaminhamento(int CodigoSeqEncaminhamento);

        [OperationContract]
        PageMessage<DTOReporteEncaminhamento> ListarReportesPorEncaminhamento(int CodigoSeqEncaminhamento, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        int SalvarReporteEncaminhamento(DTOReporteEncaminhamento dto);

        [OperationContract]
        DTOReporteEncaminhamento ObterReporteEncaminhamento(int CodigoSeqReporteEncaminhamento);

        [OperationContract]
        void DeletarReporteEncaminhamento(int CodigoSeqReporteEncaminhamento);

        [OperationContract]
        PageMessage<DTOIniciativaObjetivo> ListarObjetivosPorIniciativa(int CodigoSeqIniciativa, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        int SalvarIniciativaObjetivo(DTOIniciativaObjetivo dto);

        [OperationContract]
        DTOIniciativaObjetivo ObterIniciativaObjetivo(int CodigoSeqIniciativaObjetivo);
        
        [OperationContract]
        void DeletarIniciativaObjetivo(int CodigoSeqIniciativaObjetivo);

        [OperationContract]
        int SalvarProcesso(DTOProcesso dto);

        [OperationContract]
        PageMessage<DTOProcesso> ListarProcessoNomeAreaSituacao(string pNome, Int32 pArea, int pSituacao, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        //[OperationContract]
        //PageMessage<DTOCorporativoUnidade> ListarArea(string pNome, short pArea, int pSituacao, int startIndex, int pageSize, string orderProperty, bool orderAscending);

        [OperationContract]
        IList<DTOProcesso> ListarProcessos();

        [OperationContract]
        IList<DTOTipoAnexo> ListarTipoAnexo();

        [OperationContract]
        DTOProcessoAnexo ObterProcesso(int pCd_Seq_Processo);
    }
}